# Uncomment the next line to define a global platform for your project
platform :ios, '11.0'
require_relative '../node_modules/@react-native-community/cli-platform-ios/native_modules'

target 'OneAssist-Swift' do
  # Comment the next line if you're not using Swift and don't want to use dynamic frameworks
  use_frameworks!
  inhibit_all_warnings!
  
  # Pods for OneAssist-Swift
  pod 'Alamofire', '~> 5.3'
  pod 'IQKeyboardManagerSwift', '~> 6.5'
  pod 'Kingfisher', '~> 5.15'
  pod "KingfisherWebP"
  pod 'GoogleMaps', '~> 4.0'
  pod 'GoogleAnalytics', '~> 3.17'
  pod 'NewRelicAgent', '~> 6.10'
  pod 'MMMaterialDesignSpinner', '~> 0.2'
  pod 'WebEngage', '~> 5.2'
  pod 'GSKStretchyHeaderView', '~> 1.0'
  pod 'Cosmos', '~> 23.0'
  pod 'KMPlaceholderTextView', '~> 1.4.0'
  pod 'AppsFlyerFramework', '~> 6.0'
  pod 'DeviceKit', '~> 4.2'

  #Firebase pods
  pod 'Firebase/MLVision'
  pod 'Firebase/MLVisionTextModel'
  pod 'Firebase/MLVisionBarcodeModel'
  pod 'Firebase/RemoteConfig'
  pod 'Firebase/DynamicLinks', '~> 6.14'
  pod 'Firebase/Analytics'
  pod 'Firebase/Firestore'
  pod 'Firebase/Storage'
  pod 'Firebase/Crashlytics'
  pod 'Firebase/Database'
  
  pod 'TensorFlowLiteSwift'
  pod 'ReachabilityManager'
  pod 'SSZipArchive'
  
  # Pods for Chat
  pod 'XMPPFramework', '~> 4.0', :inhibit_warnings => true
  pod 'FBLazyVector', :path => "../node_modules/react-native/Libraries/FBLazyVector"
  pod 'FBReactNativeSpec', :path => "../node_modules/react-native/Libraries/FBReactNativeSpec"
  pod 'RCTRequired', :path => "../node_modules/react-native/Libraries/RCTRequired"
  pod 'RCTTypeSafety', :path => "../node_modules/react-native/Libraries/TypeSafety"
  pod 'React', :path => '../node_modules/react-native/'
  pod 'React-Core', :path => '../node_modules/react-native/'
  pod 'React-CoreModules', :path => '../node_modules/react-native/React/CoreModules'
  pod 'React-Core/DevSupport', :path => '../node_modules/react-native/'
  pod 'React-RCTActionSheet', :path => '../node_modules/react-native/Libraries/ActionSheetIOS'
  pod 'React-RCTAnimation', :path => '../node_modules/react-native/Libraries/NativeAnimation'
  pod 'React-RCTBlob', :path => '../node_modules/react-native/Libraries/Blob'
  pod 'React-RCTImage', :path => '../node_modules/react-native/Libraries/Image'
  pod 'React-RCTLinking', :path => '../node_modules/react-native/Libraries/LinkingIOS'
  pod 'React-RCTNetwork', :path => '../node_modules/react-native/Libraries/Network'
  pod 'React-RCTSettings', :path => '../node_modules/react-native/Libraries/Settings'
  pod 'React-RCTText', :path => '../node_modules/react-native/Libraries/Text'
  pod 'React-RCTVibration', :path => '../node_modules/react-native/Libraries/Vibration'
  pod 'React-Core/RCTWebSocket', :path => '../node_modules/react-native/'
  
  pod 'React-cxxreact', :path => '../node_modules/react-native/ReactCommon/cxxreact'
  pod 'React-jsi', :path => '../node_modules/react-native/ReactCommon/jsi'
  pod 'React-jsiexecutor', :path => '../node_modules/react-native/ReactCommon/jsiexecutor'
  pod 'React-jsinspector', :path => '../node_modules/react-native/ReactCommon/jsinspector'
  pod 'ReactCommon/jscallinvoker', :path => "../node_modules/react-native/ReactCommon"
  pod 'ReactCommon/turbomodule/core', :path => "../node_modules/react-native/ReactCommon"
  pod 'Yoga', :path => '../node_modules/react-native/ReactCommon/yoga'
  
  pod 'DoubleConversion', :podspec => '../node_modules/react-native/third-party-podspecs/DoubleConversion.podspec'
  pod 'glog', :podspec => '../node_modules/react-native/third-party-podspecs/glog.podspec'
  pod 'Folly', :podspec => '../node_modules/react-native/third-party-podspecs/Folly.podspec'
  
  pod 'react-native-document-picker', :path => '../node_modules/react-native-document-picker'
  pod 'react-native-image-picker', :path => '../node_modules/react-native-image-picker'
  pod 'RNVectorIcons', :path => '../node_modules/react-native-vector-icons'
  pod 'RNSVG', :path => '../node_modules/react-native-svg'
  pod 'RNStoreReview', :path => '../node_modules/react-native-store-review/ios'
  pod 'BVLinearGradient', :path => '../node_modules/react-native-linear-gradient'
  pod 'RNDeviceInfo', :path => '../node_modules/react-native-device-info'
  
  pod 'CodePush', :path => '../node_modules/react-native-code-push'
  pod 'RNFS', :path => '../node_modules/react-native-fs'
  
  target 'OneAssist-SwiftTests' do
    inherit! :search_paths
    # Pods for testing
  end
  
  target 'OneAssist-SwiftUITests' do
    inherit! :search_paths
    # Pods for testing
  end
  
  use_native_modules!
end

target 'NotificationService' do
  use_frameworks!
  pod 'WebEngageBannerPush'
end

target 'NotificationViewController' do
  use_frameworks!
  pod 'WebEngageAppEx/ContentExtension'
end

# Use change_lines_in_file for changeing/adding any file during pod install
def change_lines_in_file(file_path, &change)
  print "Fixing file at #{file_path}\n"
  contents = []
  
  file = File.open(file_path, 'r')
  file.each_line do | line |
    contents << line
  end
  file.close

  File.open(file_path, 'w') do |f|
    f.puts(change.call(contents))
  end
end

def find_and_replace(dir, findstr, replacestr)
  Dir[dir].each do |name|
      text = File.read(name)
      replace = text.gsub(findstr,replacestr)
      if text != replace
          puts "Fix: " + name
          File.open(name, "w") { |file| file.puts replace }
          STDOUT.flush
      end
  end
  Dir[dir + '*/'].each(&method(:find_and_replace))
end

post_install do |installer|
  
  ## Fix for XCode 12.5 beta
  find_and_replace("../node_modules/react-native/React/CxxBridge/RCTCxxBridge.mm",
  "_initializeModules:(NSArray<id<RCTBridgeModule>> *)modules", "_initializeModules:(NSArray<Class> *)modules")
  
  find_and_replace("../node_modules/react-native/ReactCommon/turbomodule/core/platform/ios/RCTTurboModuleManager.mm",
        "RCTBridgeModuleNameForClass(module))", "RCTBridgeModuleNameForClass(Class(module)))")
  
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings.delete 'IPHONEOS_DEPLOYMENT_TARGET'
        end
        if ['BVLinearGradient', 'RNDateTimePicker', 'RNDeviceInfo', 'RNGestureHandler', 'RNStoreReview', 'RNSVG', 'RNVectorIcons', 'react-native-document-picker', 'react-native-image-picker', 'react-native-netinfo', 'react-native-safe-area-context', 'react-native-viewpager', 'react-native-webengage', 'ReactNativeWebPFormat', 'react-native-webview', 'toolbar-android'].include?(target.name) #list all affected target in the array
            target.build_phases.each do |build_phases|
                if build_phases.display_name == 'Frameworks'
                    file_ref = installer.pods_project.new(Xcodeproj::Project::Object::PBXFileReference)
                    file_ref.path = 'React.framework'
                    file_ref.source_tree = 'BUILT_PRODUCTS_DIR'
                    build_phases.add_file_reference(file_ref)
                end
            end
        end
    end
    
    change_lines_in_file('../node_modules/react-native/Libraries/Image/RCTUIImageViewAnimated.m') do |lines|
      lines.map do |line|
        if line.include?("layer.contents = (__bridge id)_currentFrame.CGImage;") then 'layer.contents = (__bridge id)_currentFrame.CGImage ; } else { [super displayLayer:layer];'
        else line
        end
      end
    end
    
    find_and_replace("../node_modules/react-native/React/CxxBridge/RCTCxxBridge.mm", "_initializeModules:(NSArray<id<RCTBridgeModule>> *)modules", "_initializeModules:(NSArray<Class> *)modules")
    find_and_replace("../node_modules/react-native/ReactCommon/turbomodule/core/platform/ios/RCTTurboModuleManager.mm", "RCTBridgeModuleNameForClass(module))", "RCTBridgeModuleNameForClass(Class(module)))")
    
 end

def find_and_replace(dir, findstr, replacestr)
  Dir[dir].each do |name|
      text = File.read(name)
      replace = text.gsub(findstr,replacestr)
      if text != replace
          puts "Fix: " + name
          File.open(name, "w") { |file| file.puts replace }
          STDOUT.flush
      end
  end
  Dir[dir + '*/'].each(&method(:find_and_replace))
end
