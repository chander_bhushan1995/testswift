var entities =
[{
  "id": 1,
  "typeString": "protocol",
  "name": "HAFindPlanScenePresentationLogic"
},{
  "id": 2,
  "typeString": "class",
  "properties": [
    {
  "name": "var presenter: HAFindPlanScenePresentationLogic?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    5
  ],
  "name": "HAFindPlanSceneInteractor",
  "superClass": 11
},{
  "id": 3,
  "typeString": "protocol",
  "name": "HAFindPlanSceneDisplayLogic",
  "superClass": 12
},{
  "id": 4,
  "typeString": "class",
  "properties": [
    {
  "name": "var viewController: HAFindPlanSceneDisplayLogic?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    1
  ],
  "name": "HAFindPlanScenePresenter",
  "superClass": 13
},{
  "id": 5,
  "typeString": "protocol",
  "name": "HAFindPlanSceneBusinessLogic"
},{
  "id": 6,
  "typeString": "class",
  "properties": [
    {
  "name": "var interactor: HAFindPlanSceneBusinessLogic?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var tableViewObj: UITableView! required",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "viewDidLoad()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "initializeView()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "HAFindPlanSceneVC",
  "superClass": 14,
  "extensions": [
    7,
    8,
    9,
    10
  ]
},{
  "id": 11,
  "typeString": "class",
  "name": "BaseInteractor"
},{
  "id": 12,
  "typeString": "class",
  "name": "class"
},{
  "id": 13,
  "typeString": "class",
  "name": "BasePresenter"
},{
  "id": 14,
  "typeString": "class",
  "name": "BaseVC"
},{
  "id": 15,
  "typeString": "protocol",
  "name": "UITableViewDataSource"
},{
  "id": 16,
  "typeString": "protocol",
  "name": "WhcInfoCellDelegate"
},{
  "id": 17,
  "typeString": "protocol",
  "name": "FindAPlanCellDelegate"
},{
  "id": 7,
  "typeString": "extension",
  "methods": [
    {
  "name": "tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    15
  ]
},{
  "id": 8,
  "typeString": "extension",
  "methods": [
    {
  "name": "clickedWhcInfoPrimaryAction()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    16
  ]
},{
  "id": 9,
  "typeString": "extension",
  "methods": [
    {
  "name": "findAPlanCell(_ cell: FindAPlanCell, clickedBtnFindAPlan sender: Any)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "findAPlanCell(_ cell: FindAPlanCell, clickedBtnViewBenefits sender: Any)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    17
  ]
},{
  "id": 10,
  "typeString": "extension",
  "protocols": [
    3
  ]
}]
;

var renderedEntities = [];

var useCentralNode = true;

var templates = {
  case: undefined,
  property: undefined,
  method: undefined,
  entity: undefined,
  data: undefined,

  setup: function() {
    this.case = document.getElementById("case").innerHTML;
    this.property = document.getElementById("property").innerHTML;
    this.method = document.getElementById("method").innerHTML;
    this.entity = document.getElementById("entity").innerHTML;
    this.data = document.getElementById("data").innerHTML;

    Mustache.parse(this.case)
    Mustache.parse(this.property);
    Mustache.parse(this.method);
    Mustache.parse(this.entity);
    Mustache.parse(this.data);
  }
}

var colorSuperClass = { color: "#848484", highlight: "#848484", hover: "#848484" }
var colorProtocol = { color: "#9a2a9e", highlight: "#9a2a9e", hover: "#9a2a9e" }
var colorExtension = { color: "#2a8e9e", highlight: "#2a8e9e", hover: "#2a8e9e" }
var colorContainedIn = { color: "#99AB22", highlight: "#99AB22", hover: "#99AB22" }
var centralNodeColor = "rgba(0,0,0,0)";
var centralEdgeLengthMultiplier = 1;
var network = undefined;

function bindValues() {
    templates.setup();

    for (var i = 0; i < entities.length; i++) {
        var entity = entities[i];
        var entityToBind = {
            "name": entity.name == undefined ? entity.typeString : entity.name,
            "type": entity.typeString,
            "props": renderTemplate(templates.property, entity.properties),
            "methods": renderTemplate(templates.method, entity.methods),
            "cases": renderTemplate(templates.case, entity.cases)
        };
        var rendered = Mustache.render(templates.entity, entityToBind);
        var txt = rendered;
        document.getElementById("entities").innerHTML += rendered;
    }

    setSize();
    setTimeout(startCreatingDiagram, 100);
}

function renderTemplate(template, list) {
    if (list != undefined && list.length > 0) {
        var result = "";
        for (var i = 0; i < list.length; i++) {
            var temp = Mustache.render(template, list[i]);
            result += temp;
        }
        return result;
    }
    return undefined;
}

function getElementSizes() {
  var strings = [];
  var elements = $("img");

  for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      
      var elementData = {
        width: element.offsetWidth,
        height: element.offsetHeight
      };
      strings.push(elementData);
  }
  return strings;
}

function renderEntity(index) {
  if (index >= entities.length) {
    // create the diagram
    $("#entities").html("");
    setTimeout(createDiagram, 100);
    return;
  }
  html2canvas($(".entity")[index], {
    onrendered: function(canvas) {
      var data = canvas.toDataURL();
      renderedEntities.push(data);
      var img = Mustache.render(templates.data, {data: data}); 
      $(document.body).append(img);

      renderEntity(index + 1);
    }
  });
}

function startCreatingDiagram() {
  renderedEntities = [];
  renderEntity(0);
}

function createDiagram() {
  var entitySizes = getElementSizes();

  var nodes = [];
  var edges = [];

  var edgesToCentral = [];
  var maxEdgeLength = 0;
  for (var i = 0; i < entities.length; i++) {
    var entity = entities[i];
    var data = entitySizes[i];
    var length = Math.max(data.width, data.height) * 1.5;
    var hasDependencies = false;

    maxEdgeLength = Math.max(maxEdgeLength, length);

    nodes.push({id: entity.id, label: undefined, image: renderedEntities[i], shape: "image", shapeProperties: {useImageSize: true } });
    if (entity.superClass != undefined && entity.superClass > 0) {
      edges.push({from: entity.superClass, to: entity.id, length: length, color: colorSuperClass, label: "inherits", arrows: {from: true} });
      
      hasDependencies = true;
    }

    var extEdges = getEdges(entity.id, entity.extensions, length, "extends", colorExtension, {from: true});
    var proEdges = getEdges(entity.id, entity.protocols, length, "conforms to", colorProtocol, {to: true});
    var conEdges = getEdges(entity.id, entity.containedEntities, length, "contained in", colorContainedIn, {from: true});

    hasDependencies = hasDependencies && extEdges.length > 0 && proEdges.length > 0 && conEdges.length > 0;

    edges = edges.concat(extEdges);
    edges = edges.concat(proEdges);
    edges = edges.concat(conEdges);

    if (!hasDependencies && useCentralNode)
    {
      edgesToCentral.push({from: entity.id, to: -1, length: length * centralEdgeLengthMultiplier, color: centralNodeColor, arrows: {from: true} });
    }
  }

  if (edgesToCentral.length > 1) {
    edges = edges.concat(edgesToCentral);
    nodes.push({id: -1, label: undefined, shape: "circle", color: centralNodeColor });
  }

  var container = document.getElementById("classDiagram");
  var dataToShow = {
      nodes: nodes,
      edges: edges
  };
  var options = {
      "edges": { "smooth": false },
      "physics": {
        "barnesHut": {
          "gravitationalConstant": -7000,
          "springLength": maxEdgeLength,
          "avoidOverlap": 1
        }
      },
      //configure: true
  };
  network = new vis.Network(container, dataToShow, options);

  $("#entities").html("");
  $("img").remove();

  setTimeout(disablePhysics, 200);
}

function disablePhysics()
{
  var options = {
      "edges": { "smooth": false },
      "physics": { "enabled": false }
  };
  network.setOptions(options);
  $(".loading-overlay").fadeOut("fast");
}

function getEdges(entityId, arrayToBind, edgeLength, label, color, arrows) {
  var result = [];
  if (arrayToBind != undefined && arrayToBind.length > 0) {
      for (var i = 0; i < arrayToBind.length; i++) {
        result.push({from: entityId, to: arrayToBind[i], length: edgeLength, color: color, label: label, arrows: arrows });
      }
  }
  return result;   
}

function setSize() {
  var width = $(window).width();
  var height = $(window).height();

  $("#classDiagram").width(width - 5);
  $("#classDiagram").height(height - 5);
}