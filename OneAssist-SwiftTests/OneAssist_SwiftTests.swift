//
//  OneAssist_SwiftTests.swift
//  OneAssist-SwiftTests
//
//  Created by Mukesh on 7/19/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import XCTest
import Foundation
@testable import OneAssist_Swift

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}
class OneAssist_SwiftTests: XCTestCase {
    
    var addCardVC:AddCardsViewVC!
    override func setUp() {
        super.setUp()
        addCardVC = AddCardsViewVC()
        _ = addCardVC?.view
        
        // Put setup code here. This method is called before the invocation of each test method in the class.

import Alamofire
@testable import OneAssist_Swift

class AuthSpy: AuthenticationRemoteDataProtocol {
    
    func loginUser(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        // dummy response
        return nil
    }
    
    func responsePath(url: String, request: BaseRequestDTO?, completionHandler: @escaping(Data?, URLResponse?, Error?) -> Void) -> Request? {
        
        return nil
    }
    
    func verifyOTP(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseDuplicateIMEI(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseVoucherRequest(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseLogout(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseGetCustomerDetails(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseUpdateCustomerDetails(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseUpdateSession(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
    
    func responseUpdatePayment(url:String ,request: BaseRequestDTO? , completionHandler:@escaping(Data?,URLResponse?,Error?)->Void) -> Request? {
        
        return nil
    }
}

class OneAssist_SwiftTests: XCTestCase {
    
    var usecase: LoginRequestUseCase!
    var dummyRequest: LoginMobileRequestDTO!
    var dummyResponse: LoginMobileResponseDTO!
    var dummyError: Error!
    
    override func setUp() {
        super.setUp()
        //usecase = LoginRequestUseCase()
        usecase.remoteDataSource = AuthSpy()
        dummyRequest = LoginMobileRequestDTO(mobileNo: "8527148663")
        
                // Put setup code here. This method is called before the invocation of each test method in the class.

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        super.tearDown()
    }
    
    func testSearchAutocompleteEntries() {
        addCardVC?.issuerArray = ["pnb","hdfc","brt","rbl","pnh"]
        let resultArray:[String] = ["pnb","pnh"]
        addCardVC?.searchAutocompleteEntries(withSubstring: "pn")
        XCTAssert((addCardVC.autocompleteIssuer.containsSameElements(as: resultArray)), "Same Elements")
    }
    
    func testInitializeIssuserArray()
    {
        addCardVC.initializeIssuserArray()
        XCTAssert(addCardVC.issuerArray.count > 0,"does not return coredata objects" )
    }
    
    func testValidateCardDetails()
    {
        /*
         XCTAssertThrowsError(try addCardVC.validateCardDetails(issuerName: "fghf", cardNumber: "1234567890123456", cardName: "hi")) { (error) in
         
         let validationError = error as! AddCardError
         
         var errorReason = ""
         
         switch validationError {
         case .invalidIssuerName(let error):
         //XCTAssertEqual(error, Strings.Addcards.Error.invalidIssuerName,"does not match issuername")
         errorReason = error
         break
         
         case .invalidCardNumber(let error):
         //XCTAssertEqual(error, Strings.Addcards.Error.invalidCardnumber)
         errorReason = error
         break
         //cardNumberTextField.setError(error)
         
         case .invalidCardName(let error):
         //XCTAssertEqual(error, Strings.Addcards.Error.invalidCardName)
         errorReason = error
         break
         //cardNameTextField.setError(error)
         
         }
         
         
         XCTAssert(false, errorReason)
         
         }
         */
        XCTAssertNoThrow(try addCardVC.validateCardDetails(issuerName: "fthht", cardNumber: "1234567890123456", cardName: "hi"), "error has been caught")
    }
    
    
    func testTextFieldViewDidBeginEditing(){
        let textField:TextFieldView = TextFieldView()
        textField.fieldText = "huhhuuu"
        addCardVC.textFieldViewDidBeginEditing(textField)
        XCTAssert((addCardVC.autocompleteTableView?.isHidden)!,"is not hidden")
    }
    
    func testtextFieldViewShouldEndEditing()
    {
       let _ = addCardVC.textFieldViewShouldEndEditing(TextFieldView())
        XCTAssert((addCardVC.autocompleteTableView?.isHidden)!,"is not hidden")
    }
    
    func testtextFieldViewDidEndEditing()
    {
        addCardVC.textFieldViewDidEndEditing(TextFieldView())
        XCTAssert((addCardVC.autocompleteTableView?.isHidden)!,"is not hidden")
        
        dummyRequest = nil
        super.tearDown()
    }
    
    func testLogin() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let _ = LoginRequestUseCase.service(requestDTO: dummyRequest) { (usecase, response, error) in
            XCTAssertNil(error, "is success")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func testSetupDependencyConfigurator()
    {
        addCardVC.setupDependencyConfigurator()
        XCTAssert(addCardVC.interactor is AddCardsInteractor,"interactor does not set")
    }
    
    func testGetCardType()
    {
        let cardNumber = "33345678901234"
        let interactor = AddCardsInteractor()
        let answer:String = "DINERS"
        XCTAssert(interactor.getCardType(fromCardNumber: cardNumber) == answer,"no matching")
    }
    
    // presenter Test Cases
    func testPresentAddCard()
    {
        let presentor = BasePresenter()
        XCTAssertThrowsError(try presentor.checkError(nil, error: nil),"error has been caught")
    }
    
    func testAddCardAPI()
    {/*
        let interactor  = AddCardsInteractor()
        interactor.presenter = TestingAddCardsPresentor()
        interactor.addCard(issuerName: "ANZ Bank", cardNumber: "457273344344443", cardName: "f", typeOfCard:BankCardType(dictionary: [:]) , memId:"23523232323")
        */
        
        
        let customerWalletDetails = CustomerWalletDetails(dictionary: [:])
        customerWalletDetails.custId = "10278213"
       // customerWalletDetails.contentType = typeOfCard.cardTypeCode
        customerWalletDetails.contentNo = "457273344344443"
        customerWalletDetails.nameOnCard = "f"
        customerWalletDetails.issuerName =  "ANZ Bank"
        customerWalletDetails.assetName = ""
        customerWalletDetails.association = "VISA"
        //      customerWalletDetails.walletId = NSNumber()
        let addCardRequestDTO = AddcardRequestDTO()
        addCardRequestDTO.wallet = customerWalletDetails
        addCardRequestDTO.custId = UserCoreDataStore.currentUser?.cusId
        addCardRequestDTO.memId = "10278213"
        
        let Expectation = expectation(description: "Wait for getting the response")
        
      _ = AddCardRequestUseCase.service(requestDTO:addCardRequestDTO){
            (service,response,error) in
           // self?.presenter?.presentAddCard(response,error:error,request:(self?.addCardRequestDTO)!)
            XCTAssert(response?.status == Constants.ResponseConstants.success)
            Expectation.fulfill()
        }
        waitForExpectations(timeout: 1.0)

        
    }
    
    class TestingAddCardsPresentor:BasePresenter,AddCardPresentationLogic
    {
        func presentAddCard(_ response: AddcardResponseDTO?, error: Error?, request: AddcardRequestDTO) {
                        XCTAssertThrowsError(try checkError(response, error: error))
        }
        
        /*
        func presentAddCard(_ response: AddcardResponseDTO?, error: Error?, request: AddcardRequestDTO)
        {
            XCTAssertThrowsError(try checkError(response, error: error))
        }*/
    }
}

