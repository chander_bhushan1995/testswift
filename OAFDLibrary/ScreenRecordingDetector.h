//
//  ScreenRecordingDetector.h
//  OAFDController
//
//  Created by Ankur Batham on 09/09/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

/*
 ScreenRecordingDetector checks for screen capturing as well as airplay mirroring
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *kScreenRecordingDetectorRecordingStatusChangedNotification;

@interface ScreenRecordingDetector : NSObject
    
+(instancetype)sharedInstance;
    
    /*
     Start timer to detect Screen Recording
     */
+ (void)triggerDetectorTimer;
    
    /*
     Stop timer at time app become background/inactive or view dismiss
     */
+ (void)stopDetectorTimer;
    
    /*
     check if recording or mirroring on available version 11 and above
     */
- (BOOL)isRecording;
    
    @end
