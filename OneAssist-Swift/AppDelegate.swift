//
//  AppDelegate.swift
//  LandingPage
//
//  Created by Mukesh on 7/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase
import FirebaseDynamicLinks
import UserNotifications
import FirebaseCrashlytics
import WebEngage
import AppsFlyerLib
import Kingfisher
import KingfisherWebP

var sharedAppDelegate: AppDelegate? { return UIApplication.shared.delegate as?  AppDelegate }
var rootNavVC: BaseNavigationController? { return sharedAppDelegate?.window?.rootViewController as? BaseNavigationController }
var rootTabVC: TabVC? { return rootNavVC?.viewControllers.first as? TabVC }
var rootTopVC: ReactMainVC? { return rootNavVC?.topViewController as? ReactMainVC }

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var countForBackgroundApi = 0
    var alertShown = false
    var appOpenedWithLink: String = Constants.AppConstants.organic
    var recoCatalystProduct: String? = nil
    var recoCatalystService: String? = nil
    var recoCatalystCommSubcat: String? = nil
    var recoCatalystRID: String? = nil
    var recoCatalystUIComponentID: String? = nil
    var recoCatalystRCID: String? = nil
    var isSessionStarted: Bool = false
    var isShowBlurView: Bool = true
    var forInternalDeeplink = false
    var finalTabForInternalDeeplink: TabIndex = .newHome
    private var blurEffectView:UIVisualEffectView?
    var onDownloadingCompleteNotification: ( ()-> Void )?
    var backgroundTaskId: UIBackgroundTaskIdentifier = .invalid
    
    let Alarm_Invoice = "Alarm_Invoice"
    
    static var questionsResponse: GetQuestionAnswerResponseDTO?
    var pushPermissionStatus: Bool?
    let inAppNotificationHandler : InAppNotificationManager = InAppNotificationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        checkAndHandleAppUpdate()
        addDependencies(application: application, launchOptions: launchOptions)
        onAppStart()
        
        if RemoteConfigManager.shared.migrateDataInAppDelegate {
            DataMigrationManager(modelName: cdModelName, storeURL: cdStoreURL, modelVersions: cdModelVersions).checkAndPerformMigration()
        }
        
        #if DEBUG
        checkAndHandleEnvironmentChange()
        #elseif TEST
        checkAndHandleEnvironmentChange()
        #endif
        
        window = UIWindow()
        window?.backgroundColor = UIColor.backgroundWhiteBlue
        window?.makeKeyAndVisible()
        
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        if UIAccessibility.isInvertColorsEnabled {
            // enabled
        }
        UIView.appearance().accessibilityIgnoresInvertColors = true
        
        let weUser: WEGUser = WebEngage.sharedInstance().user
        weUser.setAttribute(Event.UserAttribute.appInstallDate, withDateValue: Constants.appInstallDate)
        
        //save the core date for notifications
        if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            savePushNotification(userInfo: userInfo, applicationState:application.applicationState)
            if application.applicationState == .active {
                Publisher.NewPushNotificationReceived.publish()
            } else {
                DeepLinkManager.handlePushNotification(userInfo)
            }
            confirmNotificationReceived(userInfo)
        }
        getSubCategories()
        getUserGroup()
        window?.rootViewController = BaseNavigationController(rootViewController: SplashScreenVC())
        
        setStatusBarBackgroundColor(color: UIColor.clear)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        addBlurView()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        removeBlurView()
        // resume download tasks if not error occured while downloading in background
        if TFLiteModelsDownloadManager.sharedInstance.currentlyOccuredError == nil {
            OADownloadManager.sharedInstance.session.getAllTasks { (tasks) in
                tasks.forEach({$0.resume()})
            }
        }
        #if DEBUG
        checkAndHandleEnvironmentChange()
        #elseif TEST
        checkAndHandleEnvironmentChange()
        #endif
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.longRunningTask()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        removeBlurView()
        getPushNotificationPermission{}
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (timer) in
            self.appopenEvent()
        }
        NotificationCenter.default.post(name: .appDidBecomeActive, object: nil)
        
        if let custID = UserCoreDataStore.currentUser?.cusId {
            Utilities.setAppsFlyerUser(customerId: custID)
        }
        AppsFlyerLib.shared().start()
        applicationState()
    }
    
    func applicationState() {
        let state = UIApplication.shared.applicationState
        if state == .background {
            print("background")
        } else if state == .active {
            print("action")
        }else if state == .inactive {
            print("inactive")
        }else {
            print("not determine")
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        XMPPManager.shared.disconnect()
        
        //invalidate background session
        OADownloadManager.sharedInstance.session.invalidateAndCancel()
    }
    
    func appopenEvent() {
        if !isSessionStarted {
            isSessionStarted = true
            setupAppOpenEvent()
            
        }
    }
    
    private func setupAppOpenEvent() {
        getPushNotificationPermission {
            var dict:[Event.EventKeys: Any] = [.link: self.appOpenedWithLink, .pushPermission: (self.pushPermissionStatus ?? false) ? "True" : "False"]
            EventTracking.shared.eventTracking(name: .appOpen, dict)
            EventTracking.shared.eventTracking(name: .afReengage)
        }
    }
    
    private func setUpAppsFlyer(){
        // 1 - Get AppsFlyer preferences from .plist file
        guard let propertiesPath = Bundle.main.path(forResource: "afdevkey", ofType: "plist"),
              let properties = NSDictionary(contentsOfFile: propertiesPath) as? [String:String] else {
            fatalError("Cannot find `afdevkey`")
        }
        guard let appsFlyerDevKey = properties["appsFlyerDevKey"],
              let appleAppID = properties["appleAppID"] else {
            fatalError("Cannot find `appsFlyerDevKey` or `appleAppID` key")
        }
        
        // 2 - Replace 'appsFlyerDevKey', `appleAppID` with your DevKey, Apple App ID
        AppsFlyerLib.shared().appsFlyerDevKey = appsFlyerDevKey
        AppsFlyerLib.shared().appleAppID = appleAppID
        AppsFlyerLib.shared().delegate = self
        AppsFlyerLib.shared().currencyCode = Constants.SchemeVariables.currency
        AppsFlyerLib.shared().oneLinkCustomDomains = ["c.1oa.in"]

        //  Set isDebug to true to see AppsFlyer debug logs
        #if DEBUG
        AppsFlyerLib.shared().isDebug = true
        AppsFlyerLib.shared().useUninstallSandbox = true
        #endif
    }
    
    func getPushNotificationPermission(_ completionHandler:  @escaping ()->() ){
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
            OperationQueue.main.addOperation {
                switch settings.authorizationStatus {
                case .notDetermined:
                    sharedAppDelegate?.isShowBlurView = false
                    break;
                case .authorized, .provisional:
                    self.pushPermissionStatus = true
                    self.enablePushNotification(UIApplication.shared)
                case .denied:
                    self.pushPermissionStatus = false
                @unknown default:
                    self.pushPermissionStatus = false
                }
                ChatBridge.sendNotification(withName: "pushStatusUpdated", body: ["isPushEnabled": self.pushPermissionStatus ?? false])
                completionHandler()
            }
        })
    }
    
    
    
    private func addDependencies(application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        // remote config
        FirebaseApp.configure()
        
        DataDiskStogare.deleteOfferbanksData()
        
        
        WebEngage.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions, notificationDelegate: inAppNotificationHandler,autoRegister : false)
        // It is necessary to register push notification otherwise the app will not behave properly(and crash) when the app is launched from push notification.
        //enablePushNotification(application)
        
        // google maps
        GMSServices.provideAPIKey("AIzaSyDSnsi3_Ky1fds7XFTg6-IV0abypqiSlVc")
        
        // keyboard manager
        addKeyboardManager()
        
        RemoteConfigManager.shared.setup(isDeveloperMode: !Constants.isProdRelease, expirationTime: Constants.isProdRelease ? Constants.RemoteConfig.defaultExpirationTime : 0)
        
        CloudFirestoreManager.shared.setup()
        
        RemoteConfigManager.shared.fetchRemoteData()
        enableAnalytics()
        
        initializeNewRelicAgent()
        setUpAppsFlyer()
    }
    
    private func onAppStart() {
        Utilities.updateMHCDataOnServer()
        UIApplication.shared.makeStatusBarView(with: .white)
        AppTheme.global.setThemes()
        
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.homeLocationPermissionNotNow.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.hideBuyback.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.appUpdateed.rawValue)
        TFLiteModelsDownloadManager.sharedInstance.checkModelDeletionTimeExceeded()
        TFLiteModelsDownloadManager.sharedInstance.oaDownloadManager.cancelAllOutStandingTasks()
        AppUserDefaults.isShowPushPromptOnTab = false
        UserDefaults.standard.synchronize()
        
        KingfisherManager.shared.defaultOptions += [
          .processor(WebPProcessor.default),
          .cacheSerializer(WebPSerializer.default)
        ]
        UIApplication.shared.setMinimumBackgroundFetchInterval(60*30)
    }
    
    //MARK:- WebEngage
    func WEGHandleDeeplink(_ deeplink: String, userData data: [AnyHashable: Any]) {
        DeepLinkManager.handlePushNotification(data)
        confirmNotificationReceived(data)
    }
    
    //MARK:- Push notification
    func enablePushNotification(_ application: UIApplication, onAuthorization: (()->())? = nil) {
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        center.requestAuthorization(options: options) { (granted, error) in
            DispatchQueue.main.async {
                self.pushPermissionStatus = granted
                application.registerForRemoteNotifications()
                onAuthorization?()
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        debugPrint("didReceiveRemoteNotification userInfo \(userInfo)" )
        AppsFlyerLib.shared().handlePushNotification(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        debugPrint("didReceiveRemoteNotification userInfo \(userInfo)" )
        AppsFlyerLib.shared().handlePushNotification(userInfo)
    }
    
    // This method will be called when app received push notifications in foreground
    var pushIdentifier : String?
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let application = UIApplication.shared
        let userInfo = notification.request.content.userInfo
        pushIdentifier = notification.request.identifier
        savePushNotification(userInfo: userInfo,identifier : notification.request.identifier ,applicationState: application.applicationState)
        Publisher.NewPushNotificationReceived.publish()
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application = UIApplication.shared
        let state = application.applicationState
        let userInfo = response.notification.request.content.userInfo
        
        if response.notification.request.identifier == Alarm_Invoice {
            if state == .active {
                self.showAlertForLocalNotificationWithString(message: response.notification.request.content.body, buttonTitle: "Try Again", isUpload: true)
                return
            }
        }
        
        if response.notification.request.identifier == TFLiteModelsDownloadManager.sharedInstance.pushNotificationIdentifier {
            onDownloadingCompleteNotification?()
            onDownloadingCompleteNotification = nil
        }
        
        //Below line is checking if user opens the
        if let identifier = pushIdentifier, identifier == response.notification.request.identifier {
            if let list : [NotificationsCD] = CoreDataStack.sharedStack.fetchObject(predicateString: "identifier == '\(identifier)'", inManagedObjectContext: CoreDataStack.sharedStack.mainContext), list.count > 0 {
                list[0].read = NSNumber(value: true)
                CoreDataStack.sharedStack.saveMainContext()
                Publisher.NewPushNotificationReceived.publish()
            }
        } else {
            savePushNotification(userInfo: userInfo,identifier : response.notification.request.identifier , applicationState: application.applicationState)
        }
        DeepLinkManager.handlePushNotification(userInfo)
        confirmNotificationReceived(userInfo)
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let registrationToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Device_Token :    \(registrationToken)")
        
        let savedRegistrationToken = UserDefaults.standard.string(forKey: UserDefaultsKeys.deviceToken.rawValue) ?? nil
        
        UserDefaults.standard.set(registrationToken, forKey: UserDefaultsKeys.deviceToken.rawValue)
        UserDefaults.standard.synchronize()
        
        if savedRegistrationToken != registrationToken {
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.appInstalled.rawValue)
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.custIdRegistered.rawValue)
            UserDefaults.standard.synchronize()
        }
        self.registrationDetail()
        AppsFlyerLib.shared().registerUninstall(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        self.registrationDetail()
    }
    
    func registrationDetail() {
        let registrationToken = UserDefaults.standard.string(forKey: UserDefaultsKeys.deviceToken.rawValue) ?? " "
        var isAppInstallApiUsed = UserDefaults.standard.bool(forKey:  UserDefaultsKeys.appInstalled.rawValue) // for first installed check
        let savedAppVersion = UserDefaults.standard.string(forKey: UserDefaultsKeys.registeredAppVersion.rawValue)
        
        if let appVersion = savedAppVersion,  appVersion != UIApplication.appVersion  { // check if user update there app
            isAppInstallApiUsed = false
            UserDefaults.standard.set("true", forKey: UserDefaultsKeys.appUpdateed.rawValue)
            EventTracking.shared.eventTracking(name: .afUpdate)
        }
        
        
        let customerId = UserCoreDataStore.currentUser?.cusId
        let isCustIdRegistered = UserDefaults.standard.bool(forKey: UserDefaultsKeys.custIdRegistered.rawValue)
        if customerId != nil  && !isCustIdRegistered { // add cust id in to DB
            isAppInstallApiUsed = false
        }
        
        if isAppInstallApiUsed == false {
            let uploadTokenRequest = DeviceRegistrationDetailsRequestDTO(custId: customerId, registrationId: registrationToken)
            let _ = DeviceRegistrationDetailsRequestUseCase.service(requestDTO: uploadTokenRequest, completionBlock: { (usecase, response, error) in
                print(error ?? "Uploaded Successfully Token")
                if let status = response?.status, status.contains("success") {
                    UserDefaults.standard.set(UIApplication.appVersion, forKey: UserDefaultsKeys.registeredAppVersion.rawValue)
                    UserDefaults.standard.set(true, forKey: UserDefaultsKeys.appInstalled.rawValue)
                    if customerId != nil {
                        UserDefaults.standard.set(true, forKey: UserDefaultsKeys.custIdRegistered.rawValue)
                    }
                    UserDefaults.standard.synchronize()
                }
            })
        }
    }
    
    private func savePushNotification(userInfo : [AnyHashable:Any] , identifier : String? = nil , applicationState : UIApplication.State){
        if let aps:[String:Any] = userInfo["aps"] as? [String : Any] ,let alert:[String : Any] = aps["alert"] as? [String : Any] , let title = alert["title"], let message = alert["body"] {
            saveNotification(title: title as! String, message: message as! String,userInfo: userInfo,identifier : identifier, state: applicationState)
        }else if let message = userInfo["message"] ,let title = userInfo["title"] {
            saveNotification(title: title as! String, message: message as! String,userInfo: userInfo,identifier : identifier, state: applicationState)
        }
    }
    private func saveNotification(title : String , message : String ,userInfo:[AnyHashable : Any],identifier : String?, state:UIApplication.State){
        if (userInfo["customData"] as? [[String: String]])?.filter({ $0["key"] == "persistable" }).first?["value"] == "false" {
            // if persistable is coming false then these notification are not be saved in inbox
            return
        }
        let managedObject = NotificationsCD(context: CoreDataStack.sharedStack.mainContext)
        managedObject.message = message
        managedObject.title = title
        managedObject.date = Date().string(with: DateFormatter.dayMonth)
        if state != .active {
            managedObject.read = NSNumber(value: true)
        }
        managedObject.userInfo = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        managedObject.identifier = identifier
        CoreDataStack.sharedStack.saveMainContext()
    }
    private func confirmNotificationReceived(_ userInfo: [AnyHashable : Any]) {
        if let commId = userInfo["commId"] as? String {
            let request = NotificationStatusRequestDTO(commId: commId)
            let _ = NotificationStatusRequestUseCase.service(requestDTO: request, completionBlock: { (usecase, response, error) in
                print(error ?? "Notification Status Updated Successfully")
            })
        }
    }
    
    // MARK:- Analytics methods
    private func enableAnalytics() {
        
        //GOOGLE ANALYTICS
        guard let gai = GAI.sharedInstance() else {
            return
        }
        
        gai.tracker(withTrackingId: Constants.LicenseKeys.gaKey)
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true
        
        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose;
    }
    
    private func addKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 20
    }
    
    private func getSubCategories() {
        
        let _ = ProductCategoryUseCase.service(requestDTO: nil) { (usecase, response, error) in
            CacheManager.shared.productCategories = response?.subCategoryList
        }
    }
    
    private func getUserGroup() {
        if let custId = UserCoreDataStore.currentUser?.cusId, !custId.isEmpty {
            let _ = UserGroupUseCase.service(requestDTO: UserGroupRequestDTO()) { (_, _, _) in
            }
        }
    }
    
    // MARK:- Campaign Tracking
    private func handleCampaign(url: URL?) {
        
        guard let url = url else {
            return
        }
        
        let tracker = GAI.sharedInstance().defaultTracker
        let hitParameter = GAIDictionaryBuilder()
        hitParameter.setCampaignParametersFromUrl(url.absoluteString)
        
        if hitParameter.get(kGAICampaignSource) == nil && url.host != nil {
            hitParameter.set("referrer", forKey: kGAICampaignMedium)
            hitParameter.set(url.host, forKey: kGAICampaignSource)
        }
        
        let finalDictionary = hitParameter.build()
        
        tracker?.set(kGAIScreenName, value: "screen name")
        tracker?.send(GAIDictionaryBuilder.createScreenView().setAll((finalDictionary as! [AnyHashable : Any])).build() as! [AnyHashable : Any]?)
    }
    
    // MARK:- New Relic Agent
    private func initializeNewRelicAgent() {
        NewRelicAgent.start(withApplicationToken: Constants.SchemeVariables.newRelicToken)
    }
    
    // MARK:- Deeplink methods
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        handleCampaign(url: url)
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            DeepLinkManager.handleLink(dynamicLink.url, usedIn: false)
            return true
        }
        AppsFlyerLib.shared().handleOpen(url, options: options)
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        handleCampaign(url: url)
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            DeepLinkManager.handleLink(dynamicLink.url, usedIn: false)
            return true
        }
        AppsFlyerLib.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
        return false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let url = userActivity.webpageURL {
            if url.absoluteString.contains("oneassist.onelink.me") ||
                url.absoluteString.contains("c.1oa.in") {
                AppsFlyerLib.shared().continue(userActivity, restorationHandler: nil)
                return true
            }else if url.absoluteString.contains("1oa."){
                if(Constants.isProdRelease && url.absoluteString.contains("1oa.1atesting")){return false}
                let oneOaUrl = url.absoluteString + "?redirect=false"
                let urlRedirectionPatch = RemoteConfigManager.shared.shortUrlRedirectionPatch
                
                var loaderCount = 0
                Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
                let request = GetLongURLRequestDTO(url: oneOaUrl)
                let _ = GetLongURLUseCase.service(requestDTO: request) {(usecase, response, error) in
                    Utilities.removeLoader(count: &loaderCount)
                    if let longUrlStr = response?.longUrl, let longUrl = URL(string: longUrlStr) {
                        var defalutUse = true
                        urlRedirectionPatch.forEach { value in
                            let useLongUrl = value["useLongUrl"] ?? "false"
                            let urlContaints = value["urlContaints"] ?? ""
                            if longUrlStr.contains(urlContaints) {
                                defalutUse = false
                                var redirectUrl = ((useLongUrl == "false") ? url.absoluteString : longUrlStr)
                                
                                if let redirectUrlValue = URL(string: redirectUrl),
                                   redirectUrlValue.queryItems.count > 0 {
                                    redirectUrl = redirectUrl + "&redirect=true"
                                }else {
                                    redirectUrl = redirectUrl + "?redirect=true"
                                }
                                
                                
                                self.handle1oaLinkRedirection(oneOaUrl: redirectUrl)
                                return
                            }
                        }
                        
                        if defalutUse {
                            let queryItems = longUrl.queryItems
                            if let llDeeplinkUrl = queryItems["ll_deeplink_url"] , let deeplinkUrl = URL(string: llDeeplinkUrl){
                                DeepLinkManager.handleLink(deeplinkUrl, usedIn: false, appOpenedWithLink: url.absoluteString)
                                
                            }else {
                                DeepLinkManager.handleLink(longUrl, usedIn: false)
                            }
                        }
                    }
                }
               
            }else{
                let isFromFirebase = DynamicLinks.dynamicLinks().handleUniversalLink(url, completion: {[weak self] (link, error) in
                    self?.handleCampaign(url: link?.url)
                    DeepLinkManager.handleLink(link?.url, usedIn: false)
                })
                if !isFromFirebase {
                    DeepLinkManager.handleLink(url, usedIn: false)
                }
            }
            return true
        }
        return false
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        OADownloadManager.sharedInstance.backgroundCompletionHandler = completionHandler
    }
    
    func handle1oaLinkRedirection(oneOaUrl: String){
        let remoteDataSource = RemoteDataSource()
        var loaderCount = 0
        Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
        let _ = remoteDataSource.responseGet(url:oneOaUrl,request: nil) { (data, urlResponse, error) in
            DispatchQueue.main.sync {
                Utilities.removeLoader(count: &loaderCount)
                if let urlData = urlResponse?.url {
                    let queryItems = urlData.queryItems
                    if let llDeeplinkUrl = queryItems["ll_deeplink_url"] , let deeplinkUrl = URL(string: llDeeplinkUrl){
                        DeepLinkManager.handleLink(deeplinkUrl, usedIn: false, appOpenedWithLink: urlData.absoluteString)
                        
                    }else {
                        DeepLinkManager.handleLink(urlData, usedIn: false)
                    }
                }
            }
        }
    }
    
    // MARK:- Background Running UseCases
    
    func uploadInvoice(docKey: String?, mem_id: NSNumber?, productCode: String?, filename_file: Data?) {
        
        guard countForBackgroundApi <= 3 else {
            countForBackgroundApi = 0
            
            if !alertShown {
                alertShown = true
                setUpLocalNotificationWithTime(fireTime: Date.currentDate(), AlertDesc: "Invoice upload failed, try again later", AlarmName: Alarm_Invoice)
            }
            return
        }
        
        let req = UploadInvoiceRequestDTO(docKey: docKey, mem_id: mem_id, productCode: productCode, filename_file: filename_file)
        let _ = UploadInvoiceRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            
            do {
                try Utilities.checkError(response, error: error)
                
                if(docKey == Constants.RequestKeys.invoice_image) {
                    UserDefaults.standard.setValue(nil, forKey: UserDefaultsKeys.invoiceImageFirst.rawValue)
                } else if(docKey == Constants.RequestKeys.additional_invoice_image) {
                    UserDefaults.standard.setValue(nil, forKey: UserDefaultsKeys.invoiceImageSecond.rawValue)
                }
                
                UserDefaults.standard.setValue(nil, forKey: UserDefaultsKeys.invoiceProductCode.rawValue)
                UserDefaults.standard.setValue(nil, forKey: UserDefaultsKeys.membershipIdInvoice.rawValue)
            } catch {
                self?.countForBackgroundApi = (self?.countForBackgroundApi ?? 0) + 1
                self?.uploadInvoice(docKey: req.docKey, mem_id: mem_id, productCode: req.productCode, filename_file: req.filename_file)
            }
        }
    }
    
    // MARK:- MHC
    func setStatusBarBackgroundColor(color: UIColor) {
        UIApplication.shared.makeStatusBarView(with: color)
    }
    
    func setUpLocalNotificationWithTime(fireTime : Date, AlertDesc : String, AlarmName name : String) {
        
        let content = UNMutableNotificationContent()
        content.body = NSString.localizedUserNotificationString(forKey: AlertDesc, arguments: nil)
        content.sound = .default
        content.categoryIdentifier = "com.oneAssist.oneAssist"
        UNUserNotificationCenter.current().add(UNNotificationRequest(identifier: name, content: content, trigger: UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)))
    }
    
    func showAlertForLocalNotificationWithString(message : String, buttonTitle : String, isUpload: Bool = false) {
        self.window?.rootViewController?.showAlert(title: "Message", message: message, primaryButtonTitle: buttonTitle, nil, primaryAction: {
            self.alertActionForLocalNotificationForbuttonTitle(buttonTitle : buttonTitle, isUpload: isUpload)
        }, nil)
    }
    
    func alertActionForLocalNotificationForbuttonTitle(buttonTitle : String, isUpload: Bool = false) {
        
        if isUpload {
            countForBackgroundApi = 0
            alertShown = true
            
            if let imgData = UserDefaults.standard.value(forKey: UserDefaultsKeys.invoiceImageFirst.rawValue) as? Data {
                
                let custId = UserDefaults.standard.string(forKey: UserDefaultsKeys.membershipIdInvoice.rawValue)
                let mem_id = NSNumber(value: Int(custId ?? "") ?? 0)
                let product_code = UserDefaults.standard.string(forKey: UserDefaultsKeys.invoiceProductCode.rawValue)
                
                self.uploadInvoice(docKey: Constants.RequestKeys.invoice_image, mem_id: mem_id, productCode: product_code, filename_file: imgData)
                
                if let additionalImgData = UserDefaults.standard.value(forKey: UserDefaultsKeys.invoiceImageSecond.rawValue) as? Data {
                    self.uploadInvoice(docKey: Constants.RequestKeys.additional_invoice_image, mem_id: mem_id, productCode: product_code, filename_file: additionalImgData)
                }
            }
            
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [Alarm_Invoice])
            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
        
        if buttonTitle == "OK" {
            //Do nothing
            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlertForCancel(alertController : UIAlertController) {
        // not in used
        self.window?.rootViewController?.presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    @objc func showAlertInMHC(title: String = "", message: String, primaryButtonTitle: String, secondaryButtonTitle: String?, primaryAction: @escaping AlertAction, secondaryAction: AlertAction?) {
        self.window?.rootViewController?.showAlert(title: title, message: message, primaryButtonTitle: primaryButtonTitle, secondaryButtonTitle, isCrossEnable: false, primaryAction: primaryAction, secondaryAction)
    }
    
    @objc func dismissViewController() {
        self.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    func longRunningTask() {
        self.backgroundTaskId = UIApplication.shared.beginBackgroundTask(
            withName: Constants.SessionIDs.modelDownloadSessionID,
          expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskId)
        })
      }
}

extension AppDelegate: ResponseFilter {
    func filter<Response>(response: Response) -> Response? where Response : BaseResponseDTO {
        
        if response.message == "WS.POST.LOGIN.INVALID.SESSION" {
            
            if UIApplication.shared.isIgnoringInteractionEvents {
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            
            Utilities.loaderView.removeFromSuperview()
            window?.rootViewController?.showAlert(message: response.responseMessage ?? "", primaryButtonTitle: Strings.Global.okay, nil, primaryAction: {
                _ = LogoutRequestUseCase.service(requestDTO: LogoutRequestDTO(), completionBlock: { (usecase, response, error) in
                    
                    Utilities.clearDataAndLogout()
                    
                    let weUser: WEGUser = WebEngage.sharedInstance().user
                    weUser.logout()
                })
            }, nil)
            return nil
        }
        
        return response
    }
    
    func checkAndHandleAppUpdate(){
        let currentAppVersion = UserDefaults.standard.string(forKey: UserDefaultsKeys.currentAppVersion.rawValue) ?? "0"
        if currentAppVersion != UIApplication.appVersion{ // App is upgraded
            // encrypt session token if current version is less than mentioned version
            // only handeled bellow 6.1 version #warning dont change 6.1
            if currentAppVersion.compare("6.1", options: .numeric, range: nil, locale: nil) == .orderedAscending {
                if let unEnryptedToken = UserDefaults.standard.string(forKey: UserDefaultsKeys.sessionToken.rawValue){
                    AppUserDefaults.sessionToken = unEnryptedToken
                }
            }
            UserDefaults.standard.set(UIApplication.appVersion, forKey: UserDefaultsKeys.currentAppVersion.rawValue)
        }
    }
    
    func checkAndHandleEnvironmentChange() {
        if EnvironmentSetting.currentEnviornment != EnvironmentSetting.previousEnviornment {
            EnvironmentSetting.previousEnviornment = EnvironmentSetting.currentEnviornment
            Utilities.forceUserLogout()
            exit(0)
        }
    }
}

extension AppDelegate: AppsFlyerLibDelegate {
    // Handle Organic/Non-organic installation
    func onConversionDataSuccess(_ data: [AnyHashable: Any]) {
        print("onConversionDataSuccess data:")
        for (key, value) in data {
            print(key, ":", value)
        }
        if let status = data["af_status"] as? String {
            if (status == "Non-organic") {
                if let sourceID = data["media_source"],
                   let campaign = data["campaign"] {
                    print("This is a Non-Organic install. Media source: \(sourceID)  Campaign: \(campaign)")
                }
            } else {
                print("This is an organic install.")
            }
            if let is_first_launch = data["is_first_launch"] as? Bool,
               is_first_launch {
                print("First Launch")
                goToDeepLinkManagerWithParams(data)
            } else {
                print("Not First Launch")
            }
        }
    }
    func onConversionDataFail(_ error: Error) {
        print("\(error)")
    }
    // Handle Deeplink
    func onAppOpenAttribution(_ attributionData: [AnyHashable: Any]) {
        goToDeepLinkManagerWithParams(attributionData)
    }
    
    func onAppOpenAttributionFailure(_ error: Error) {
        print("\(error)")
    }
    
    private func goToDeepLinkManagerWithParams(_ attributionData: [AnyHashable: Any]) {
        if let deeplinkURL = attributionData["ll_deeplink_url"] as? String, let url = URL(string: deeplinkURL){
            var queryParams: [String: String?] = [:]
            for key in appsFlyerEventQueryParams {
                if let value = attributionData[key] as? String {
                    queryParams[key] = value
                }
            }
            let link = URL(string: "https://oneassist.onelink.me")?.appendQueryParams(queryParams)
            DeepLinkManager.handleLink(url, usedIn: false, appOpenedWithLink: link?.absoluteString)
        }
    }
}

// For adding blur effect on entering and removing blur effect on exiting background mode
extension AppDelegate {
    func addBlurView() {
        if self.isShowBlurView {
            let blurEffect: UIBlurEffect
            if #available(iOS 13.0, *) {
                blurEffect = UIBlurEffect(style: .systemUltraThinMaterial)
            } else {
                blurEffect = UIBlurEffect(style: .light)
            }
            blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView?.frame = window?.bounds ?? UIScreen.main.bounds
            blurEffectView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            window?.addSubview(blurEffectView!)
        }
    }
    
    func removeBlurView() {
        self.isShowBlurView = true
        blurEffectView?.removeFromSuperview()
        blurEffectView = nil
    }
}
