//
//  TemporaryCustomerInfoModel.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 16/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class TemporaryCustomerInfoModel {
    var customerDetails: [TempCustomerDetail]?
    var enableFDFlow: Bool?
    var paymentDone: Bool?
    var preBoardingTaskStage: String?
    var task: [Task]?
    
    // can add extra fields from membership's view model
    var activationCode: String?
    
    func setupTempCustomerData(tempCustomer: TemporaryCustomerInfoResponseDTO?) {
        self.customerDetails = tempCustomer?.customerDetails
        self.enableFDFlow = tempCustomer?.enableFDFlow?.boolValue
        self.paymentDone = tempCustomer?.paymentDone
        self.preBoardingTaskStage = tempCustomer?.preBoardingTaskStage
        self.task = tempCustomer?.task
    }
    
    func setupPlanDetailAndSRModel(planDetailAndSRModel: PlanDetailAndSRModel) {
        self.customerDetails = planDetailAndSRModel.tempCustomerDetail
        self.enableFDFlow = planDetailAndSRModel.enableFDFlow
        self.paymentDone = planDetailAndSRModel.paymentDone
        self.preBoardingTaskStage = planDetailAndSRModel.preBoardingTaskStage
        self.task = planDetailAndSRModel.task
        
        self.activationCode = planDetailAndSRModel.activationCode
    }
    
    func getFDTestType() -> FDTestType {
        var fdTestType: FDTestType = .SDT
        guard let task = task, let firstObj = task.first, let fdType = firstObj.fdTestType else {
            return fdTestType
        }
        fdTestType = fdType == "MT" ? .MT : .SDT
        return fdTestType
    }
    
    func setFDTestType(_ type: FDTestType) {
        guard let _ = task, let _ = task?.first else {
            return
        }
        self.task?.first?.fdTestType = type.getAPIValue()
    }
}
