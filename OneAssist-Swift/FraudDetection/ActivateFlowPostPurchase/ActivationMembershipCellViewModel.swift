//
//  ActivationMembershipCellViewModel.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 06/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

enum ActivationMemCellStage {
    case notYetStarted
    case started
    case completed
}

enum FDActivationStep {
    case iMEIFetch
    case basicDetailFill
    case sMSSending
    case mirrorText
    case secondaryDevice
}

class ActivationMembershipCellViewModel {
    var stage: ActivationMemCellStage = .notYetStarted
    var activationStep: FDActivationStep = .basicDetailFill
    var title = ""
    var subTitle = ""
    var showButton = false
    var buttonText = ""
    
    init(stage: ActivationMemCellStage = .notYetStarted, title: String = "", subtitle: String = "", showButton: Bool = false, buttonText: String = "", activationStep: FDActivationStep) {
        self.stage = stage
        self.title = title
        self.subTitle = subtitle
        self.showButton = showButton
        self.buttonText = buttonText
        self.activationStep = activationStep
    }
}
