//
//  MembershipActivationHeader.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 06/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MembershipActivationHeader: UIView {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var nameLabel: BodyTextBoldBlackLabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUIElement(with tempCustInfoModel: TemporaryCustomerInfoModel?) {
        nameLabel.text = nil
        instructionLabel.text = nil
        
        guard let tempCustInfo = tempCustInfoModel else {
            return
        }
        
        let tempCustDetails = tempCustInfo.customerDetails?.first
        let coverAmmount = tempCustDetails?.coverAmount?.int64Value ?? 0
        let firstName = tempCustInfo.customerDetails?[0].firstName ?? ""
        
        if tempCustInfoModel?.getFDTestType() == .MT {
            nameLabel.text = Strings.FDetectionConstants.hi + (firstName.count>0 ? (" " + firstName + "!") :"!"  ) + "\n" + Strings.FDetectionConstants.activationHeaderText
            instructionLabel.text = String(format:Strings.FDetectionConstants.coverAmmountText, String(coverAmmount))
            
        }else {
            var strLastDateRaiseAClaim = ""
            if let dateStr = tempCustDetails?.uploadDocsLastDate {
                if let date = DateFormatter.dayMonthAndYear.date(from: dateStr) {
                    let dayMon = date.string(with: .dayMonth).split{$0 == " "}.map(String.init)
                    strLastDateRaiseAClaim = "\(dayMon[0])\(String(describing: (date.getSuffixFromDay(dayOfMonth: Int(dayMon[0]) ?? -1)))) \(dayMon[1])"
                }
            }
            
            instructionLabel.text = String(format: Strings.Global.welcomeAndInstructionMsg, firstName, strLastDateRaiseAClaim)
        }
        
        nameLabel.setLineSpacing(lineSpacing: 2, lineHeightMultiple: 1.5)
        instructionLabel.setLineSpacing(lineSpacing: 3, lineHeightMultiple: 1)
    }
}
