//
//  MembershipDetailsFraudDetectionVC.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 04/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import Photos

protocol MembershipDetailsFDBusinessLogic {
    func getStateCityInfo(pincode: String)
    func getTempCustomerInfo(for activationCode: String)
    
    /// - Parameter docId: mongoDB id of image
    func getDocumentImage(docId: String?, for invoiceName: String)
    
    func uploadFDDocument(for invoiceName: String, activityRefId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?)
    
    func submitPincodeAndAddress(with pincode: String, address: String, orderId: String, deviceModel: String, activationCode: String)
    
    func refundOrder(orderId: String)
}

let purchaseInvoice = "INVOICE_IMAGE"
let additionalInvoice = "ADDITIONAL_INVOICE_DOCUMENT"

class MembershipDetailsFraudDetectionVC: BaseVC {
    var interactor: MembershipDetailsFDBusinessLogic?
    var addressInfo:  StateCityResponseDTO?
    var countLabels = [UIView]()
    var selectedInputField: AnyObject?
    var refundView: RefundBottomView? = nil
    
    // outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var labelHeading: UILabel!
    
    @IBOutlet weak var deviceDetailTopView: UIView!
    @IBOutlet weak var deviceDetailHeadingLabel: UILabel!
    @IBOutlet weak var purchaseAmountFieldView: TextFieldView!
    @IBOutlet weak var purchaseDateFieldView: TextFieldView!
    @IBOutlet weak var imeiFieldView: TextFieldView!
    @IBOutlet weak var personalDetailHeadingLabel: UILabel!
    
    @IBOutlet weak var addressLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var addressTV: UITextView!
    @IBOutlet weak var labelError: SupportingTextRegularBlackLabel!
    @IBOutlet weak var fieldViewPincode: TextFieldView!
    
    @IBOutlet weak var buttonSubmit: UIButton!
        
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        title = Strings.MembershipDetailsFraudDetection.memDetails
        addTap()
        
        stepLabel.text = Strings.MembershipDetailsFraudDetection.firstStep
        stepLabel.font = DLSFont.supportingText.bold
        stepLabel.textColor = UIColor.bodyTextGray
        
        labelHeading.text = Strings.MembershipDetailsFraudDetection.heading
        labelHeading.font = DLSFont.h3.bold
        
        var addingText = ""
        if let maximumValue = FDCacheManager.shared.maxInsuranceValue,
           let minimumValue = FDCacheManager.shared.minInsuranceValue {
            addingText = " (\(Strings.Global.rupeeSymbol) \(minimumValue) to \(Strings.Global.rupeeSymbol) \(maximumValue) )"
        }
        purchaseAmountFieldView.descriptionText = Strings.iOSFraudDetection.purchaseAmountText + addingText
        purchaseAmountFieldView.fieldType = CurrencyFieldType.self
        purchaseAmountFieldView.prefixText = Strings.Global.rupeeSymbol
        purchaseAmountFieldView.delegate = self
        
        purchaseDateFieldView.descriptionText = Strings.iOSFraudDetection.purchaseDateText
        purchaseDateFieldView.isSetRightImageAction = true
        purchaseDateFieldView.delegate = self
        purchaseDateFieldView.enableFieldViewButton(true, showRightImage:  true, rightIconImage: UIImage(named: "calender"))
        
        fieldViewPincode.descriptionText = Strings.MembershipDetailsFraudDetection.pincode
        fieldViewPincode.fieldType = OTPFieldType.self
        fieldViewPincode.delegate = self
        
        buttonSubmit.isEnabled = false
        
        addressTV.delegate = self
        addressTV.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
        labelError.textColor = UIColor.errorTextFieldBorder
        labelError.isHidden = true
        
        countLabels.append(addBarButtons())
                
        self.deviceDetailHeadingLabel.isHidden = !FDCacheManager.shared.isShowFullForm
        self.purchaseAmountFieldView.isHidden = !FDCacheManager.shared.isShowFullForm
        self.purchaseDateFieldView.isHidden = !FDCacheManager.shared.isShowFullForm
        self.personalDetailHeadingLabel.isHidden = !FDCacheManager.shared.isShowFullForm
        
        if FDCacheManager.shared.fdTestType == .MT {
            self.imeiFieldView.isHidden = false
            self.stepLabel.isHidden = true
            imeiFieldView.descriptionText = Strings.iOSFraudDetection.IMEInumberText
            if let saveObject = SaveFDDataMode.getSaveData(FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue) {
                imeiFieldView.fieldText = saveObject.imei
            }
            imeiFieldView.setError(Strings.iOSFraudDetection.IMEInumberTextDesc)
            imeiFieldView.isUserInteractionEnabled = false
            buttonSubmit.setTitle(Strings.MembershipDetailsFraudDetection.continue, for: .normal)
            
        }else {
            self.imeiFieldView.isHidden = true
            self.stepLabel.isHidden = false
            buttonSubmit.setTitle(Strings.MembershipDetailsFraudDetection.proceedToNextStep, for: .normal)
        }
        
        if !FDCacheManager.shared.isShowFullForm, self.imeiFieldView.isHidden {
            self.deviceDetailTopView.isHidden = true
        }else {
            self.deviceDetailTopView.isHidden = false
        }
        
        Utilities.addLoader(onView: view,count: &loaderCount, isInteractionEnabled: false)
        interactor?.getTempCustomerInfo(for: FDCacheManager.shared.tempCustInfoModel?.activationCode ?? "")
    }
    
    private func addBarButtons() -> UIView {
        let chatNotificationView = Utilities.getBadgeView1(target: self,image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: #selector(self.chatClicked(_:)))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView)]
        return chatNotificationView
    }
    
    @objc func chatClicked(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .chat, [.location: Event.EventParams.basicDetails, .mode: FDCacheManager.shared.fdTestType.rawValue])
        present(ChatVC(), animated: true, completion: nil)
    }
    
    private func validateForm() throws {
        try Validation.validatePincode(text: fieldViewPincode.fieldText)
    }
    
    // MARK:- Action Methods
    @IBAction func clickedButtonSubmit(_ sender: Any) {
        if !validateEmptyForm(){
            return
        }
        do {
            EventTracking.shared.eventTracking(name: .postPurchaseContinue ,[.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
            
            try validateForm()
            
            let pincode = fieldViewPincode.fieldText
            let address = addressTV.text
            
            FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].pinCode = pincode
            FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].addrLine1 = address
            if FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].deviceModel == nil {
               FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].deviceModel = UIDevice.currentDeviceModel
            }
            
            if FDCacheManager.shared.isShowFullForm {
                if let purchaseDate = self.purchaseDateFieldView.fieldText,
                   let date = DateFormatter.dayMonthAndYear.date(from: purchaseDate) {
                    FDCacheManager.shared.purchaseDate = String(date.millisecondsSince1970)
                }
                if let purchasePrice = self.purchaseAmountFieldView.fieldText {
                    FDCacheManager.shared.purchaseAmount = purchasePrice
                }
            }
            
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
            
            let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? ""
            
            interactor?.submitPincodeAndAddress(with: pincode ?? "", address: address ?? "", orderId: orderId, deviceModel: FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].deviceModel ?? UIDevice.currentDeviceModel, activationCode: FDCacheManager.shared.tempCustInfoModel?.activationCode ?? "")
            
        } catch let exception as ValidationError {
            
            switch exception {
            case .invalidPincode(let error):
                fieldViewPincode.setError(error)
            default:
                break
            }
            
        } catch {}
    }
}

// MARK:- Display Logic Conformance
extension MembershipDetailsFraudDetectionVC: MembershipDetailsFDDisplayLogic {
    
     func displayImageSuccess(image: UIImage?, with name: String) { }
     func displayImageFailure(error: String?, with name: String) { }
     func displayUploadedImageSuccess(with name: String, data: String?) { }
     func displayUploadedFailure(with name: String, error: String?) { }
    
    fileprivate func handleAutoPopulation() {
        if let pincode = FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].pinCode {
            fieldViewPincode.fieldText = pincode
            interactor?.getStateCityInfo(pincode: (fieldViewPincode.fieldText ?? ""))
        }
        
        if let customerDetail = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first {
            addressTV.text = "\(customerDetail.addrLine1 ?? "")"
        }
        checkForButtonSubmit()
    }
    
    func addressInfoRecieved(addressInfo: StateCityResponseDTO) {
        self.addressInfo = addressInfo
        if addressInfo.addressTmp?.pinCode == fieldViewPincode.fieldText {
            fieldViewPincode.suffixText = "\(addressInfo.addressTmp?.cityName ?? ""), \(addressInfo.addressTmp?.stateName ?? "")"
        }
    }
    
    func successfullyUploadedDetails(getDocsToUploadResponse: GetDocsToUploadResponseDTO?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        // Refreshing Membership Tab
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        NotificationCenter.default.post(name: Notification.Name.membershipActivationFDVC, object: nil)
        
        // Get the current authorization state.
        if FDCacheManager.shared.fdTestType == .MT {
            routeToMirrorTestVideoGuide()
        }else {
            if (Permission.checkGalleryPermission() == PHAuthorizationStatus.authorized) {
                routeToVerifyIMEIScreen()
                
            } else {
                routeToAllowPermission()
             
            }
        }
        
        if let docsResponse = getDocsToUploadResponse {
            FDCacheManager.shared.getDocsToUploadModel = docsResponse
        }
    }
    
    func uploadDetailsFailure(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func setRefreshedTempCustomerResponse(with tempCustomerInfoModel: TemporaryCustomerInfoModel) {
        // new response set now
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        tempCustomerInfoModel.activationCode = FDCacheManager.shared.tempCustInfoModel?.activationCode
        FDCacheManager.shared.tempCustInfoModel = tempCustomerInfoModel
        
        handleAutoPopulation()
    }
    
    func errorRecieved(_ error: String) {
        fieldViewPincode.suffixText = nil
        if (fieldViewPincode.fieldText?.count ?? 0) == 6 {
            fieldViewPincode.setError("please enter valid pincode")
        }
    }
    
    func displayTempCustomerError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func successfullyRefundOrder(txnID: String?) {
        Utilities.removeLoader(count: &loaderCount)
        Utilities.topMostPresenterViewController.dismiss(animated: true) {
            self.routeToRefundSuccessScree(txnId: txnID ?? (FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? ""))
        }
    }
    
    func refundOrderFailure(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        showAlert(message: error)
    }
}

extension MembershipDetailsFraudDetectionVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if let selectedField = self.selectedInputField as? TextFieldView {
            if selectedField == self.purchaseAmountFieldView, selectedField != textView, !selectedField.isEmpty() {
                return self.validatePurchaseAmount()
            }
        }
        selectedInputField = textView
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        labelError.isHidden = true
        addressTV.styleTextView(with: .white, borderColor: UIColor.buttonTitleBlue, shadowColor: UIColor.buttonTitleBlue)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        addressTV.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
        
        if let selectedField = self.selectedInputField as? UITextView,
           selectedField == textView {
            selectedInputField = nil
        }
        
        checkForButtonSubmit()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let str = textView.text as NSString
        let strNew = str.replacingCharacters(in: range, with: text)
        
        if strNew.count == 0{
            return true
        }else{
            return true
        }
    }
}

extension MembershipDetailsFraudDetectionVC: TextFieldViewDelegate {
    func textFieldViewShouldBeginEditing(_ textFieldView: TextFieldView) -> Bool {
        if let selectedField = self.selectedInputField as? TextFieldView {
            if selectedField == self.purchaseAmountFieldView, selectedField != textFieldView, !selectedField.isEmpty() {
                return self.validatePurchaseAmount()
            }
        }
        selectedInputField = textFieldView
        return true
    }

    func textFieldViewDidEndEditing(_ textFieldView: TextFieldView) {
        if textFieldView == self.purchaseAmountFieldView, !textFieldView.isEmpty()  {
            _ = self.validatePurchaseAmount()
            
        }else if textFieldView == self.fieldViewPincode, !textFieldView.isEmpty() {
            if self.validatePincode() {
                self.interactor?.getStateCityInfo(pincode: textFieldView.fieldText ?? "")
            }
        }
        
        if let selectedField = self.selectedInputField as? TextFieldView,
           selectedField == textFieldView {
                selectedInputField = nil
        }
        
        self.checkForButtonSubmit()
    }
    
    func textViewBtnClicked(_ textFieldView: TextFieldView) {
        if self.refundView == nil {
            selectedInputField = textFieldView
            textFieldView.animateTextField(state: .normal)
            presentDatePicker()
        }
    }
}

extension MembershipDetailsFraudDetectionVC: OADatePickerDelegate {
    
    func dateSelected(_ date: Date) {
        purchaseDateFieldView.fieldText = date.string(with: .dayMonthAndYear)
        purchaseDateFieldView.animateTextField(state: .normal)
        Utilities.topMostPresenterViewController.dismiss(animated: true) {
            _ = self.validatePurchaseDate()
            self.checkForButtonSubmit()
        }
    }
    
    @objc func presentDatePicker() {
        let datePicker = OADatePicker()
        var date: Date = .currentDate()
        if let transactionDate = FDCacheManager.shared.transactionDate {
            date = Date.getDate(from: transactionDate)
        }
        datePicker.minimumDate = date.addingTimeInterval(-365*5*24*60*60)
        datePicker.maximumDate = .currentDate()
        if let inputDate = purchaseDateFieldView.fieldText,
           let value = DateFormatter.dayMonthAndYear.date(from: inputDate){
            date =  value.addingTimeInterval(TimeInterval(19800))
        }
        datePicker.date = date
        
        datePicker.delegate = self
        Utilities.presentPopover(view: datePicker, height: Utilities.datePickerHeight)
    }
}

// Validation for empty form elements
extension MembershipDetailsFraudDetectionVC{
    
    fileprivate func checkForButtonSubmit() {
        var isDataFill = true
        if FDCacheManager.shared.isShowFullForm {
            
            if self.purchaseDateFieldView.fieldText != "" && self.purchaseAmountFieldView.fieldText != "" {
                isDataFill = true
            }else {
                isDataFill = false
            }
        }
        if  fieldViewPincode.fieldText != "" && addressTV.text != "" && isDataFill == true
        {
            buttonSubmit.isEnabled = true
        } else {
            buttonSubmit.isEnabled = false
        }
    }
    
    fileprivate func validateEmptyAddress()->Bool{
        if (!addressTV.isHidden && (addressTV == nil || addressTV.text == "")){
            addressTV.styleTextView(with: .white, borderColor: UIColor.errorTextFieldBorder, shadowColor: UIColor.errorTextFieldBorderShadow)
            labelError.text = Strings.EmptyTextFieldError.emptyAddress
            labelError.isHidden = false
            return false
        }
        return true
    }
    
    fileprivate func validatePincode()->Bool {
        var isValid = true
        
        if (self.fieldViewPincode.isEmpty()){
            fieldViewPincode.setError(Strings.EmptyTextFieldError.emptySixDigitPin)
            isValid = false
            
        }else {
            let pincode =  self.fieldViewPincode.fieldText ?? ""
            if pincode.count < 6 {
                fieldViewPincode.setError(Strings.EmptyTextFieldError.emptySixDigitPin)
                isValid = false
            }
        }
        
        return isValid
    }
    
    fileprivate func validateEmptyForm()->Bool {
        var isValid = true
        
        if validateAmountDateForm() == false {
            isValid = false
        }
        
        else if validatePincode() == false {
            isValid = false
        }
        
        else if validateEmptyAddress() == false {
            isValid = false
        }
        
        return isValid
    }
    
    fileprivate func validateAmountDateForm()->Bool {
        var isValid = true
        if FDCacheManager.shared.isShowFullForm == true {
            if validatePurchaseAmount() == false {
                isValid = false
            }else if validatePurchaseDate()  ==  false {
                isValid = false
            }
        }
        return isValid
    }
    
    fileprivate func validatePurchaseAmount()->Bool {
        var isValid = true
        if (self.purchaseAmountFieldView.isEmpty()){
            purchaseAmountFieldView.setError(Strings.EmptyTextFieldError.emptyPurchaseAmount)
            isValid = false
        }else {
            let purchasePrice: Int = Int(self.purchaseAmountFieldView.fieldText ?? "0") ?? 0
            if let maximumValue = FDCacheManager.shared.maxInsuranceValue,
               let minimumValue = FDCacheManager.shared.minInsuranceValue,
               (purchasePrice >= minimumValue.intValue && purchasePrice <= maximumValue.intValue) {
                isValid = true
            }else {
                refundView = RefundBottomView()
                refundView?.buttonAction = {[unowned self] value in
                    self.refundView = nil
                    if value == "refund" {
                        self.refundOrder()
                       
                    }else {
                        Utilities.topMostPresenterViewController.dismiss(animated: true) {
                            self.purchaseAmountFieldView.textFieldObj.becomeFirstResponder()
                        }
                    }
                }
                let model = refundModel(for: "amount")
                refundView?.setupModel(model)
                Utilities.presentPopoverWithOutGesture(view: refundView!, height: RefundBottomView.height(forModel: model))
                isValid = false
            }
            
        }
        return isValid
    }
    
    
    fileprivate func validatePurchaseDate()->Bool {
        var isValid = true
        if (self.purchaseDateFieldView.isEmpty()){
            purchaseDateFieldView.setError(Strings.EmptyTextFieldError.emptyDate)
            isValid = false
        }else {
            
            if let purchaseDate = self.purchaseDateFieldView.fieldText,
               let date = DateFormatter.dayMonthAndYear.date(from: purchaseDate) {
              let dateinminiSecond = Double(date.millisecondsSince1970)
            
            if let purchaseDate = Double(FDCacheManager.shared.transactionDate ?? "0"),
               let minDeviceDate = Double(FDCacheManager.shared.minDeviceDate ?? "0"),
               (dateinminiSecond <= purchaseDate && dateinminiSecond >= minDeviceDate){
                isValid = true
                
            }else {
                refundView = RefundBottomView()
                refundView?.buttonAction = {[unowned self] value in
                    self.refundView = nil
                    if value == "refund" {
                        self.refundOrder()
                    }else {
                        Utilities.topMostPresenterViewController.dismiss(animated: true) {
                            self.presentDatePicker()
                        }
                    }
                }
                let model = refundModel(for: "date")
                refundView?.setupModel(model)
                Utilities.presentPopoverWithOutGesture(view: refundView!, height: RefundBottomView.height(forModel: model))
                isValid = false
            }
            }else {
                purchaseDateFieldView.setError(Strings.EmptyTextFieldError.emptyDate)
                isValid = false
            }
        }
        return isValid
    }
    
    func refundModel(for type: String) -> RefundModel {
        var model = RefundModel()
        let purchaseAmount = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.amount ?? 0
        let daysLimit = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.planValidity ?? 365
        
        if type == "date"{
            let purchaseDate = Date.getDateStr(from: FDCacheManager.shared.transactionDate as NSString? ?? "0", with: .dayMonthYear)
            model.title = Strings.RefundText.phoneDateOlder
            model.subTitle = String(format:Strings.RefundText.phoneDateOlderDetail,String(daysLimit.intValue), String(purchaseDate))
            
        }else if type == "amount"{
            let maximumValue = (FDCacheManager.shared.maxInsuranceValue?.intValue ?? 0)
            let minimumValue = (FDCacheManager.shared.minInsuranceValue?.intValue ?? 0)
            model.title = Strings.RefundText.amountmatcherror
            model.subTitle = String(format:Strings.RefundText.amountmatchDetail, String(minimumValue), String(maximumValue))
        }else {
            model.title = Strings.RefundText.amountPhoneError
            model.subTitle = Strings.RefundText.refundOpt
        }
        
        model.purchaseAmount = "\(Strings.Global.rupeeSymbol) \(purchaseAmount.intValue)"
        model.purchaseDate = Date.getDateStr(from: FDCacheManager.shared.transactionDate as NSString? ?? "0", with: .dayMonthYear)
        return model
    }
    
    
    func refundOrder() {
        Utilities.addLoader(message: Strings.RatingFeedback.pleaseWait, count: &loaderCount, isInteractionEnabled: false)
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? ""
        interactor?.refundOrder(orderId: orderId)
    }
}

// MARK:- Configuration Logic
extension MembershipDetailsFraudDetectionVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MembershipDetailsFDInteractor()
        let presenter = MembershipDetailsFDPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension MembershipDetailsFraudDetectionVC {
    func routeToAllowPermission() {
        let vc = PermissionScreenVC()
        vc.delegate = self
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToVerifyIMEIScreen() {
        let vc = IMEIVerificationVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToMirrorTestVideoGuide() {
        let vc = VideoGuideViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToRefundSuccessScree(txnId: String) {
        let purchaseAmount = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.amount ?? 0
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        if let rootVC = self.popToRootView() as? TabVC {
            rootVC.routeToRefundSuccessScreen(txnId, purchaseAmount: "\(Strings.Global.rupeeSymbol) \(purchaseAmount.intValue)")
        }
    }
}

extension MembershipDetailsFraudDetectionVC: PermissionScreenVCDelegate {
    func routeToIMEIVerifyScreen() {
        EventTracking.shared.eventTracking(name: .galleryPermission ,[.location: "Mobile Fraud Detection", .action: "Allow",.mode: FDCacheManager.shared.fdTestType.rawValue])
        routeToVerifyIMEIScreen()
    }
}
