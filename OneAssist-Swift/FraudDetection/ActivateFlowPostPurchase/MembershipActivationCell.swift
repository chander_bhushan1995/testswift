//
//  MembershipActivationCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 06/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

let completeStageImage = "fd_complete"

protocol MembershipActivationCellDelegate: class {
    func clickedButtonViewDetails(_ cell: MembershipActivationCell, clickedActionBtn sender: Any)
}

class MembershipActivationCell: UITableViewCell,ReuseIdentifier,NibLoadableView {
    @IBOutlet weak var stageLabel: UILabel!
    @IBOutlet weak var stageLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var stageLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet var dashedLineView: UIView!
    
    @IBOutlet var stackViewBottomConstraint: NSLayoutConstraint!
    
    weak var delegate: MembershipActivationCellDelegate?
    var totalCellCount: Int = 1
    var fdTestType: FDTestType = .SDT
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Here you can access UI elements and set their properties
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // You can clear fields if not setting every time
        imgView.isHidden = true
        stageLabel.isHidden = true
        subTitle.isHidden = false
        actionBtn.isHidden = true
        stackViewBottomConstraint.constant = 16
        stageLabelWidthConstraint.constant = 26
        stageLabelHeightConstraint.constant = 26
        title.font = DLSFont.bodyText.regular
        dashedLineView.isHidden = false
    }
    
    private func setLabelOrImageForSDT(from stage: ActivationMemCellStage, and indexPath: IndexPath) {
        title.font = DLSFont.h3.regular
        if indexPath.row == (totalCellCount-1) {
            dashedLineView.isHidden = true
        }
        
        if stage == .completed {
            stackViewBottomConstraint.constant = 50
            imgView.isHidden = false
            subTitle.isHidden = true
            imgView.image = UIImage(named: completeStageImage) ?? UIImage()
        } else {
            stageLabel.isHidden = false
            if stage == .notYetStarted {
                stageLabel.circledLabel(with: String(indexPath.row + 1), textColor: UIColor.dashedViewColor, cornerRadius: 13, borderWidth: 1, borderColor: UIColor.dashedViewColor, backgroundColor: UIColor.clear)
            } else if stage == .started {
                title.font = DLSFont.h3.bold
                stageLabel.circledLabel(with: String(indexPath.row + 1), textColor: UIColor.white, cornerRadius: 13, borderWidth: 1, borderColor: UIColor.buttonTitleBlue, backgroundColor: UIColor.buttonTitleBlue)
            }
        }
    }
    
    private func setLabelOrImageForMT(from stage: ActivationMemCellStage, and indexPath: IndexPath) {
        title.font = DLSFont.bodyText.bold
        stackViewBottomConstraint.constant = 40
        stageLabelWidthConstraint.constant = 12
        stageLabelHeightConstraint.constant = 12
        
        if indexPath.row == (totalCellCount-1) {
            dashedLineView.isHidden = true
        }
        
        if stage == .completed {
            imgView.isHidden = false
            subTitle.isHidden = true
            imgView.image = UIImage(named: completeStageImage) ?? UIImage()
            
        } else {
            stageLabel.isHidden = false
            if stage == .notYetStarted {
                stageLabel.circledLabel(with: "", textColor: UIColor.dashedViewColor, cornerRadius: 6, borderWidth: 1, borderColor: UIColor.dashedViewColor, backgroundColor: UIColor.clear)
            } else if stage == .started {
                stageLabel.circledLabel(with: "", textColor: UIColor.buttonTitleBlue, cornerRadius: 6, borderWidth: 1, borderColor: UIColor.buttonTitleBlue, backgroundColor: UIColor.clear)
            }
        }
    }
    
    func setViewModel(with viewModel: ActivationMembershipCellViewModel, and indexPath: IndexPath) {
        if fdTestType == .MT {
             setLabelOrImageForMT(from: viewModel.stage, and: indexPath)
            
        }else {
            setLabelOrImageForSDT(from: viewModel.stage, and: indexPath)
        }
        
        title.text = viewModel.title
        subTitle.text = viewModel.subTitle
        actionBtn.isHidden = !viewModel.showButton
        actionBtn.setTitle(viewModel.buttonText, for: .normal)
        layoutIfNeeded()
    }
    
    @IBAction func btnTapped(_ sender: Any) {
        delegate?.clickedButtonViewDetails(self, clickedActionBtn: sender)
    }
    
}
