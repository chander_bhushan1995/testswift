//
//  MembershipDetailsFDInteractor.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 10/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MembershipDetailsFDPresentationLogic {
    func responseRecieved(response: StateCityResponseDTO?, error: Error?)
    func tempCustomerResponseRecieved(response: TemporaryCustomerInfoResponseDTO?, error: Error?)
    
    func boardingImage(of name: String, response: GetClaimDocumentResponseDTO?, error: Error?)
    
    func uploadResponseReceived(for invoiceName: String, response: UploadInvoiceFDResponseDTO?, error: Error?)
    func recievedUpdateDetailsResponse(updateDetailsResponse: UpdatePostPaymentDetailsResponseDTO?, getDocsToUploadResponse: GetDocsToUploadResponseDTO?, getDocsToUploadError: Error?, updateDetailsError: Error?)
    
    func refundOrderResponse(response: RefundOrderResponseDTO?, error: Error?)
}

class MembershipDetailsFDInteractor: BaseInteractor {
    var presenter: MembershipDetailsFDPresentationLogic?
    
}

extension MembershipDetailsFDInteractor: MembershipDetailsFDBusinessLogic {
    
    func getTempCustomerInfo(for activationCode: String) {
        let req = TemporaryCustomerInfoRequestDTO()
        req.activationCode = activationCode
        
        let _ = TemporaryCustomerInfoRequestUseCase.service(requestDTO: req) { [weak self] (usecase, response, error) in
            self?.presenter?.tempCustomerResponseRecieved(response: response, error: error)
        }
    }
    
    // MARK: Business Logic Conformance
    func getStateCityInfo(pincode: String) {
        let request = StateCityRequestDTO(pinCode: pincode)
        let _ = StateCityRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.responseRecieved(response: response, error: error)
        }
    }
    
    func getDocumentImage(docId: String?, for invoiceName: String) {
        let req = GetClaimDocumentRequestDTO(documentId: docId, claimDocType: .boardingThumbnail)
        let _ = GetBankDocumentRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.boardingImage(of: invoiceName, response: response, error: error)
        }
    }
    
    func uploadFDDocument(for invoiceName: String, activityRefId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?) {
        
        let req = UploadInvoiceFDRequestDTO(activityReferenceId: activityRefId, name: name, fileName: fileName, docFileContentType: docFileContentType, displayName: displayName, docMandatory: docMandatory, uploadedFile: uploadedFile)
        
        let _ = UploadInvoiceFDUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.uploadResponseReceived(for: invoiceName, response: response, error: error)
        }
    }
    
    func submitPincodeAndAddress(with pincode: String, address: String, orderId: String, deviceModel: String, activationCode: String){
        
        let addrInfo = AddressInfo(addressLine1: address, pincode: pincode)
        var imei1: String? = nil
        var imei2: String? = nil
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            imei1 = saveObject.imei
            imei2 = saveObject.imei2
        }
        
        let req = UpdatePostPaymentDetailsRequestDTO(addressInfo: addrInfo, deviceModel: deviceModel, orderId: orderId, imei: imei1, imei2: imei2, purchaseAmount: FDCacheManager.shared.purchaseAmount, purchaseDate: NSNumber(value: FDCacheManager.shared.purchaseDate))
        
        let _ = UpdatePostPaymentDetailUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in

            let reqs = GetDocsToUploadRequestDTO()
            reqs.activationCode = activationCode ?? ""
            let _ = GetDocsToUploadUseCase.service(requestDTO: reqs) { [weak self] (usecase, docResposne, getDocError) in
                self?.presenter?.recievedUpdateDetailsResponse(updateDetailsResponse: response,getDocsToUploadResponse: docResposne,getDocsToUploadError: getDocError, updateDetailsError: error)
            }
        }
    }
    
    func refundOrder(orderId: String) {
        let req = RefundOrderRequestDTO(orderId: orderId)
        
        let _ = RefundOrderUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.refundOrderResponse(response: response, error: error)
        }
    }
}
