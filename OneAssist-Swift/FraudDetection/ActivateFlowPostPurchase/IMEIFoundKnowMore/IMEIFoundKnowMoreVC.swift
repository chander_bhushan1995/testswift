//
//  IMEIFoundKnowMoreVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIFoundKnowMoreBusinessLogic {
    func getKnowMoreData()

}

class IMEIFoundKnowMoreVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var dataSourceObject: IMEIFoundKnowMoreModel?
    fileprivate var interactor: IMEIFoundKnowMoreBusinessLogic?
    var expendSectionIndex: Int = 0
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiliazeView()
        // Do any additional setup after loading the view.
    }

    
    private func initiliazeView() {
        tableView.register(cell: KnowMoreTableViewCell.self)
        
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.KnowMoreHeaderView)
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.KnowMoreFooterView)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 48
        
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 24
        
        tableView.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.white
        
        tableView.tableFooterView = UIView()
        
        self.interactor?.getKnowMoreData()
    }
    
    // MARK: - UIButton Action
    @IBAction func onClickBackAction(_ sender: UIButton?) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Navigation
}


// MARK:- Table DataSource
extension IMEIFoundKnowMoreVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if let section = self.dataSourceObject {
            return section.imeiKnowMoreData?.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == self.expendSectionIndex {
            if let sectionObjeect = self.dataSourceObject {
                if let cell = sectionObjeect.imeiKnowMoreData?[self.expendSectionIndex] {
                    return cell.values?.count ?? 0
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let section = self.dataSourceObject {
            if let cellData = section.imeiKnowMoreData?[self.expendSectionIndex] {
                let cell: KnowMoreTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
                let model = cellData.values?[indexPath.row]
                cell.setupModel(model)
                return cell
            }
        }
        return UITableViewCell()
    }
}

// MARK:- Table Delegate
extension IMEIFoundKnowMoreVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let sectionData = self.dataSourceObject {
            if let model = sectionData.imeiKnowMoreData?[section] {
                let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.KnowMoreHeaderView) as! KnowMoreHeaderView
                headerView.setupModel(model, section: section)
                headerView.delegate = self
                headerView.toggleButtonState(self.expendSectionIndex == section)
                return headerView
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == self.expendSectionIndex {
            return 12.0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == self.expendSectionIndex {
            let footerVew = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.KnowMoreFooterView) as! KnowMoreFooterView
            return footerVew
        }
        return nil
    }
}


// MARK: - KnowMoreHeaderDelegate
extension IMEIFoundKnowMoreVC: KnowMoreHeaderDelegate {
    func didSelectSection(_ index: Int) {
        var indexes = [self.expendSectionIndex]
        if self.expendSectionIndex != index {
            indexes.append(index)
            self.expendSectionIndex = index
            self.tableView.beginUpdates()
            self.tableView.reloadSections(IndexSet(indexes), with: .automatic)
            self.tableView.endUpdates()
        }
        
    }
}
    



// MARK:- Display Logic Conformance
extension IMEIFoundKnowMoreVC: IMEIFoundKnowMoreDisplayLogic {
    func sendKnowMoreResponse(_ response: IMEIFoundKnowMoreModel?, error: Error?) {
        if response != nil {
            self.dataSourceObject = response
        }
        
        self.tableView.reloadData()
    }
    
    func displayError(_ error: String?) {
        self.tableView.reloadData()
    }
}

// MARK:- Configuration Logic
extension IMEIFoundKnowMoreVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = IMEIFoundKnowMoreInterector()
        let presenter = IMEIFoundKnowMorePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

