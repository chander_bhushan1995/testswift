//
//  IMEIFoundKnowMoreModel.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation


class IMEIFoundKnowMoreModel: BaseResponseDTO {
    var imeiKnowMoreData: [KnowMoreSectionData]?
}

class KnowMoreSectionData: BaseResponseDTO {
    var category: String?
    var headerValue: String?
    var values: [CellData]?
}

class CellData : ParsableModel {
    var title : String?
    var image : String?
}
