//
//  IMEIFoundKnowMorePresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

protocol IMEIFoundKnowMoreDisplayLogic: class {
    func sendKnowMoreResponse(_ response: IMEIFoundKnowMoreModel?, error: Error?)
    func displayError(_ error: String?)
}

class IMEIFoundKnowMorePresenter: BasePresenter {
    weak var viewController: IMEIFoundKnowMoreDisplayLogic?
    
}

extension IMEIFoundKnowMorePresenter: IMEIFoundKnowMorePresentationLogic {
    
    func knowMoreDataReceived(_ resposne: IMEIFoundKnowMoreModel?, error: Error?) {
        if error != nil {
            viewController?.displayError(error?.localizedDescription)
        }else {
            viewController?.sendKnowMoreResponse(resposne, error: error)
        }
    }
}
