//
//  IMEIFoundKnowMoreInterector.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

protocol IMEIFoundKnowMorePresentationLogic {
    func knowMoreDataReceived(_ resposne: IMEIFoundKnowMoreModel?, error: Error?)
}

class IMEIFoundKnowMoreInterector: BaseInteractor {
    var presenter: IMEIFoundKnowMorePresentationLogic?
    
}

extension IMEIFoundKnowMoreInterector: IMEIFoundKnowMoreBusinessLogic {
    func getKnowMoreData() {
        let obj:KnowMoreRequestDTO = KnowMoreRequestDTO()
        
        let _ = IMEIFoundKnowMoreUseCase.service(requestDTO: obj) {  [weak self] (service,response,error) in
            self?.presenter?.knowMoreDataReceived(response,error:error)
        }
    }
  
}

