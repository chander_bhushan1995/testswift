//
//  IMEIFoundKnowMoreUseCase.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class KnowMoreRequestDTO: BaseRequestDTO {
    
}

class IMEIFoundKnowMoreUseCase : BaseRequestUseCase<KnowMoreRequestDTO, IMEIFoundKnowMoreModel> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    override func getRequest(requestDto: KnowMoreRequestDTO?, completionHandler: @escaping (IMEIFoundKnowMoreModel?, Error?) -> Void) {
        responseDataProvider.getIMEIKnowMoreResponse(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
