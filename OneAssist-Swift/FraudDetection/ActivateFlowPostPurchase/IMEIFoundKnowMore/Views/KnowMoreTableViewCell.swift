//
//  KnowMoreTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 25/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class KnowMoreTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var detailLabel: H2RegularLabel!
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupModel(_ modelObject: CellData?){
        self.setupStepsText(modelObject?.title)
        detailImage.isHidden = true
        if let image = modelObject?.image {
            detailImage.isHidden = false
            detailImage.image = UIImage(named: image)
        }
        
    }
    
    
    private func setupStepsText(_ title: String?) {
        if let textValue = title {
            let bullet = "\u{2022}\t"
            
            let paragraphStyle = NSMutableParagraphStyle()
            let nonOptions = [NSTextTab.OptionKey: Any]()
            paragraphStyle.tabStops = [
                NSTextTab(textAlignment: .left, location: 16, options: nonOptions)]
            paragraphStyle.defaultTabInterval = 16
            paragraphStyle.lineSpacing = 5
            paragraphStyle.headIndent = 16
            
            let textAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.charcoalGrey, NSAttributedString.Key.paragraphStyle : paragraphStyle]
            let bulletAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: DLSFont.bodyText.bold, NSAttributedString.Key.foregroundColor: UIColor.charcoalGrey, NSAttributedString.Key.paragraphStyle : paragraphStyle]
            
            let bulletAttribute = NSMutableAttributedString(string: bullet, attributes: bulletAttributes)
            let titleString = NSMutableAttributedString(string: textValue, attributes: textAttributes)
            
            bulletAttribute.append(titleString)
            
            self.detailLabel.attributedText = bulletAttribute
        }else {
            self.detailLabel.text = nil
        }
    }
    
    
    static func createParagraphAttribute(_ lineSpacing: CGFloat = 10.0) ->NSParagraphStyle {
        var paragraphStyle: NSMutableParagraphStyle
        paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 15, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 15
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.headIndent = 15
        return paragraphStyle
    }
    
    func setViewModel(_ model: Facts?){
        containerView.backgroundColor = UIColor.white
        detailImage.isHidden = true
        detailLabel.attributedText = KnowMoreTableViewCell.getAttributedText(model)
    }
    
    static func getAttributedText(_ model: Facts?) ->NSMutableAttributedString {
           let attributesDictionary = [NSAttributedString.Key.font : DLSFont.bodyText.medium]
           let fullAttributedString = NSMutableAttributedString(string: "", attributes: attributesDictionary as [NSAttributedString.Key : Any])
           for points in model?.points ?? []{
               let bulletPoint: String = "\u{2022}\t"
               let formattedString: String = "\(bulletPoint) \(points)\n"
               let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: formattedString, attributes: [.foregroundColor:UIColor.black, .font: DLSFont.bodyText.regular])
            let paragraphStyle = KnowMoreTableViewCell.createParagraphAttribute(model?.lineSpacing ?? 10.0)
               attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributedString.length))
               fullAttributedString.append(attributedString)
           }
        return fullAttributedString
    }
    
    
    class func heightForBottomSheetModel(_ model: Facts?, totalWidth: CGFloat) -> CGFloat {
        let widthConstraints: CGFloat = 32
        let collectiveHeigthConstains: CGFloat = 16
        let getText =  KnowMoreTableViewCell.getAttributedText(model)
        let titleHeight = getText.height(containerWidth: totalWidth - widthConstraints)
        return collectiveHeigthConstains + titleHeight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
