//
//  KnowMoreHeaderView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 25/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol KnowMoreHeaderDelegate: class {
    func didSelectSection(_ index: Int)
}

class KnowMoreHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var headerTitle: BodyTextBoldBlackLabel!
    @IBOutlet weak var arrawButton: UIButton!

    weak var delegate: KnowMoreHeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        arrawButton.contentHorizontalAlignment = .right
    }
    
    func setupModel(_ modelObject: KnowMoreSectionData?, section: Int) {
        self.headerTitle.text = modelObject?.headerValue
        arrawButton.tag = 100 + section
    }
    
    
    func toggleButtonState(_ selected: Bool) {
        arrawButton.isSelected = selected
        let angle: CGFloat = selected ? .pi : 0
        arrawButton.imageView?.transform = CGAffineTransform(rotationAngle: angle)
    }
    
    
    //MARK: UIButton Action
    @IBAction func onClickExpendAction(_ sender: UIButton?){
        self.delegate?.didSelectSection((sender?.tag ?? 100) - 100)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
