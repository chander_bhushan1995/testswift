//
//  FDBottomSheetView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 06/04/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class FDBottomSheetView: UIView {
    

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: H2BoldLabel!
    @IBOutlet var bottomSheetView: UIView!
    var buttonAction:((String) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("FDBottomSheetView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        
    }
    
    @IBAction func onClickCrossAction(_ sender: UIButton?) {
        self.buttonAction?("dismiss")
    }
    
    @IBAction func onClickAgreeAction(_ sender: UIButton?) {
        self.buttonAction?("agree")
    }

    
    func height() -> CGFloat {
        //12+30+20+height+30+height+12+20+48+20
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
        
        titleLabel.setNeedsLayout()
        titleLabel.layoutIfNeeded()
        
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        let widthConstraints: CGFloat = 40.0
        let titleHeight = Int(titleLabel.text?.height(withWidth: screensize.width - widthConstraints, font: titleLabel.font) ?? 0.0)
        var subHeight = 0
        for lable in stackView.subviews {
            let staticLable = lable as! UILabel
            subHeight = subHeight + Int((staticLable.text?.height(withWidth: screensize.width - widthConstraints, font: staticLable.font) ?? 0)) + 4
        }
        let subViewCount = stackView.subviews.count
        let stackheight = subHeight + 16 * (subViewCount - 1)
        let height = 12 + 30 + 20 + titleHeight + 30 + stackheight + 12 + 20 + 48 + 20
        return CGFloat(height) + bottomSafeArea
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
