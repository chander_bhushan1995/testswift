//
//  MembershipDetailsFDPresenter.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 10/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MembershipDetailsFDDisplayLogic: class {
    func addressInfoRecieved(addressInfo: StateCityResponseDTO)
    func errorRecieved(_ error: String)
    
    func displayTempCustomerError(_ error: String)
    func setRefreshedTempCustomerResponse(with tempCustomerInfoModel: TemporaryCustomerInfoModel)
    
    func displayImageSuccess(image: UIImage?, with name: String)
    func displayImageFailure(error: String?, with name: String)
    
    func displayUploadedImageSuccess(with name: String, data: String?)
    func displayUploadedFailure(with name: String, error: String?)
    
    func successfullyUploadedDetails(getDocsToUploadResponse: GetDocsToUploadResponseDTO?)
    func uploadDetailsFailure(_ error: String)
    
    func successfullyRefundOrder(txnID: String?)
    func refundOrderFailure(_ error: String)
    
}

class MembershipDetailsFDPresenter: BasePresenter {
    weak var viewController: MembershipDetailsFDDisplayLogic?
    
}

extension MembershipDetailsFDPresenter: MembershipDetailsFDPresentationLogic {
    
    func tempCustomerResponseRecieved(response: TemporaryCustomerInfoResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            
            guard let response = response else {
                return
            }
            
            let tempCustomerInfoModel = TemporaryCustomerInfoModel()
            tempCustomerInfoModel.setupTempCustomerData(tempCustomer: response)
            
            viewController?.setRefreshedTempCustomerResponse(with: tempCustomerInfoModel)
        } catch {
            viewController?.displayTempCustomerError(error.localizedDescription)
        }
    }
    
    private func setNonAddressValue(response: StateCityResponseDTO) -> StateCityResponseDTO {
        let response = response
        response.addressTmp = AddressTmp(dictionary: [:])
        response.addressTmp?.city = ""
        response.addressTmp?.cityName = Strings.AddressRegistrationScene.others
        response.addressTmp?.stateCode = ""
        response.addressTmp?.stateName = Strings.AddressRegistrationScene.others
        return response
    }
    
    // MARK: Presentation Logic Conformance
    func responseRecieved(response: StateCityResponseDTO?, error: Error?) {
        if let error = error {
            viewController?.errorRecieved(error.localizedDescription)
        } else if response?.status == Constants.ResponseConstants.success {
            viewController?.addressInfoRecieved(addressInfo: response!)
        } else if response?.status == Constants.ResponseConstants.failure {
            viewController?.errorRecieved(Strings.Global.somethingWrong)
        } else if let _ = response?.responseMessage {
            viewController?.addressInfoRecieved(addressInfo: setNonAddressValue(response: response!))
        } else {
            viewController?.errorRecieved(Strings.Global.somethingWrong)
        }
    }
    
    func boardingImage(of name: String, response: GetClaimDocumentResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayImageSuccess(image: response?.image, with: name)
        } catch {
            viewController?.displayImageFailure(error: error.localizedDescription, with: name)
        }
    }
    
    func uploadResponseReceived(for invoiceName: String, response: UploadInvoiceFDResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUploadedImageSuccess(with: invoiceName, data: response?.data)
        } catch {
            viewController?.displayUploadedFailure(with: invoiceName, error: error.localizedDescription)
        }
    }
    
    func recievedUpdateDetailsResponse(updateDetailsResponse: UpdatePostPaymentDetailsResponseDTO?, getDocsToUploadResponse: GetDocsToUploadResponseDTO?, getDocsToUploadError: Error?, updateDetailsError: Error?) {
        do {
            try Utilities.checkError(updateDetailsResponse, error: updateDetailsError)
            try Utilities.checkError(getDocsToUploadResponse, error: getDocsToUploadError)
            viewController?.successfullyUploadedDetails(getDocsToUploadResponse: getDocsToUploadResponse)
        } catch {
            viewController?.uploadDetailsFailure(error.localizedDescription)
        }
    }
    
    func refundOrderResponse(response: RefundOrderResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.successfullyRefundOrder(txnID: response?.data?.txnId)
        } catch {
            viewController?.refundOrderFailure(error.localizedDescription)
        }
    }
}
