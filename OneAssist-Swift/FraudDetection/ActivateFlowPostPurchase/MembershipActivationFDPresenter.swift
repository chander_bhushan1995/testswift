//
//  MembershipActivationFDPresenter.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 06/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit


protocol MembershipActivationFDDisplayLogic: class {
    func displayMembershipActivationCells(with activationCellViewModel: [ActivationMembershipCellViewModel])
    func setGetDocsToUploadResponse(with response: GetDocsToUploadResponseDTO?)
    func setUpdatedGetTempCustomerResponse(with response: TemporaryCustomerInfoModel?)
    func setGetTempCustomerAndGetDocsToUploadResponse(_ tempCustomerresponse: TemporaryCustomerInfoModel?, _ getDocsToUploadResponse: GetDocsToUploadResponseDTO?)
    func displayError(_ error: String)
}


class MembershipActivationFDPresenter: BasePresenter, MembershipActivationFDPresentationLogic {
    weak var viewController: MembershipActivationFDDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func prepareViewModelOfMembershipActivationCells(from tempCustInfoModel: TemporaryCustomerInfoModel?) {
        // When cell is in completed state then don't show subtitle and change the title
        
        guard let tempCustInfo = tempCustInfoModel else {
            return
        }
        
        guard let task = tempCustInfo.task?.first else {
            return
        }
        
        let fdTestType = tempCustInfo.getFDTestType()
        var firstStage: ActivationMemCellStage = .started,
            secondStage: ActivationMemCellStage = .notYetStarted,
            thirdStage: ActivationMemCellStage = .notYetStarted,
            fourthStage: ActivationMemCellStage = .notYetStarted
        
        var firstStageTitle = Strings.MembershipActivationCell.StageOne.title,
            secondStageTitle = Strings.MembershipActivationCell.StageTwo.title,
            thirdStageTitle = Strings.MembershipActivationCell.StageThree.title
        
        let fourthStageTitle =  Strings.MembershipActivationCell.StageFour.title
        
        var secondStageShowButton = false, thirdStageShowButton = false
        
        if fdTestType == .MT {
           firstStageTitle = Strings.MembershipActivationCell.StageOne.mtTitle
           secondStageTitle = Strings.MembershipActivationCell.StageTwo.mtTitle
           thirdStageTitle = Strings.MembershipActivationCell.StageThree.mtTitle
            
            if let orderId = tempCustInfo.customerDetails?.first?.orderId?.stringValue, let _ = SaveFDDataMode.getSaveData(orderId) {
                firstStage = .completed;
                secondStage = .started
            }
            
            if task.status == Strings.PEScene.FraudDetection.Status.strPostDtlPending {
                // no need to do anything here. All default values
            } else if task.status == Strings.PEScene.FraudDetection.Status.strComplete ||
                task.status == Strings.PEScene.FraudDetection.Status.strReupload {
                secondStage = .completed
                
                if let orderId = tempCustInfo.customerDetails?.first?.orderId?.stringValue, let _ = SaveFDDataMode.getSaveData(orderId) {
                    firstStage = .completed;
                    thirdStage = .started
                }
            }
            
            let firstStageCellVM = ActivationMembershipCellViewModel(stage: firstStage,
                                                                     title: firstStageTitle,
                                                                     activationStep: .iMEIFetch)
            
            let secondStageCellVM = ActivationMembershipCellViewModel(stage: secondStage,
                                                                      title: secondStageTitle,
                                                                      activationStep: .basicDetailFill)
            
            let thirdStageCellVM = ActivationMembershipCellViewModel(stage: thirdStage,
                                                                     title: thirdStageTitle,
                                                                     activationStep: .mirrorText)
                        
            // This is a View Model
            let activationCellsViewModel = [firstStageCellVM, secondStageCellVM, thirdStageCellVM]
            
            viewController?.displayMembershipActivationCells(with: activationCellsViewModel)
            
        }else {
            if task.status == Strings.PEScene.FraudDetection.Status.strPostDtlPending {
                // no need to do anything here. All default values
            } else if task.status == Strings.PEScene.FraudDetection.Status.strComplete ||
                task.status == Strings.PEScene.FraudDetection.Status.strReupload {
                // Here we need to check whether user has done IMEI verification or not. Based on whether user has done IMEI verification or not, we need to decide between Stage 2 and Stage 3
                firstStageTitle = Strings.MembershipActivationCell.StageOne.completedTitle
                firstStage = .completed;
                
                if let orderId = tempCustInfo.customerDetails?.first?.orderId?.stringValue, let _ = SaveFDDataMode.getSaveData(orderId) {
                    // It means IMEI verification has been done. User will be re-directed to 3rd stage
                    secondStage = .completed;
                    thirdStage = .started
                    secondStageTitle = Strings.MembershipActivationCell.StageTwo.completedTitle
                    thirdStageShowButton = true
                    
                } else {
                    secondStage = .started;
                    secondStageShowButton = true
                }
                
            }
            
            let firstStageCellVM = ActivationMembershipCellViewModel(stage: firstStage,
                                                                     title: firstStageTitle,
                                                                     subtitle: Strings.MembershipActivationCell.StageOne.subTitle,
                                                                     showButton: false,
                                                                     buttonText: "",
                                                                     activationStep: .basicDetailFill)
            
            let secondStageCellVM = ActivationMembershipCellViewModel(stage: secondStage,
                                                                      title: secondStageTitle,
                                                                      subtitle: Strings.MembershipActivationCell.StageTwo.subTitle,
                                                                      showButton: secondStageShowButton,
                                                                      buttonText: Strings.MembershipActivationCell.StageTwo.buttonText,
                                                                      activationStep: .iMEIFetch)
            
            let thirdStageCellVM = ActivationMembershipCellViewModel(stage: thirdStage,
                                                                     title: thirdStageTitle,
                                                                     subtitle: Strings.MembershipActivationCell.StageThree.subTitle,
                                                                     showButton: thirdStageShowButton,
                                                                     buttonText: Strings.MembershipActivationCell.StageThree.buttonText,
                                                                     activationStep: .sMSSending)
            
            let fourthStageCellVM = ActivationMembershipCellViewModel(stage: fourthStage,
                                                                      title: fourthStageTitle,
                                                                      subtitle: Strings.MembershipActivationCell.StageFour.subTitle,
                                                                      showButton: false,
                                                                      buttonText: "",
                                                                      activationStep: .secondaryDevice)
            
            // This is a View Model
            let activationCellsViewModel = [firstStageCellVM, secondStageCellVM, thirdStageCellVM, fourthStageCellVM]
            
            viewController?.displayMembershipActivationCells(with: activationCellsViewModel)
            
        }
    }
    
    func getDocsToUploadResponseRecieved(response: GetDocsToUploadResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            self.viewController?.setGetDocsToUploadResponse(with: response)
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
    
    func getTempcustomerAndDocsToUploadResponseRecieved(docResposne: GetDocsToUploadResponseDTO?,tempCustomerResponse: TemporaryCustomerInfoResponseDTO? , getDocError: Error?, getTempError: Error?) {
        
        do {
            try checkError(tempCustomerResponse, error: getTempError)
            try checkError(docResposne, error: getDocError)
            
            let tempCustomerInfoModel = TemporaryCustomerInfoModel()
            tempCustomerInfoModel.setupTempCustomerData(tempCustomer: tempCustomerResponse)
            
            self.viewController?.setGetTempCustomerAndGetDocsToUploadResponse(tempCustomerInfoModel, docResposne)
           
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
    func getupdatedTempCustomerResponseRecieved(response: TemporaryCustomerInfoResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            let tempCustomerInfoModel = TemporaryCustomerInfoModel()
            tempCustomerInfoModel.setupTempCustomerData(tempCustomer: response)
            
            self.viewController?.setUpdatedGetTempCustomerResponse(with: tempCustomerInfoModel)
            
        } catch {
           // self.viewController?.displayError(error.localizedDescription)
        }
    }
}
