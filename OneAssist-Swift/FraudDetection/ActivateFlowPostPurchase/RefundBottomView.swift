//
//  RefundBottomView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 30/12/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct RefundModel {
    var title: String?
    var subTitle: String?
    var purchaseAmount: String?
    var purchaseDate: String?
    var hintText: String? = Strings.RefundText.hintText
}

class RefundBottomView: UIView {

    @IBOutlet var bottomSheetView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hintView: DLSCurvedView!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var refundButton: UIButton!
    var buttonAction:((String) -> ())?
    
    var refundModel: RefundModel?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("RefundBottomView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        
        hintView.layer.cornerRadius = 3.0
        hintView.layer.borderWidth = 1.0
        hintView.backgroundColor = UIColor.backgroundInProgress
        hintView.layer.borderColor = UIColor.sunglow.cgColor
    }
    
    
    func setupModel(_ model: RefundModel){
        self.headingLabel.text = model.title
        self.detailLabel.text = model.subTitle
        self.amountLabel.text = model.purchaseAmount
        self.dateLabel.text = model.purchaseDate
        self.hintLabel.text = model.hintText
    }
    
    
    
    @IBAction func onClickRefundAction(_ sender: UIButton?){
        self.buttonAction?("refund")
    }
    
    @IBAction func onClickCrossAction(_ sender: UIButton?){
        self.buttonAction?("dismiss")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func height(forModel model:RefundModel) -> CGFloat {
        let widthConstraints: CGFloat = 40.0
        let imageHeight: CGFloat = 50 + 34 + 16 + 8 + 24 + 21 + 10 + 21 + 32 + 12 + 12 + 16 + 48 + 20
        //50 + 34 + 16 + <text height> + 8 + <text height> + 24 + 21 + 10 + 21 + 32 + 12 + <text height> + 12 + 16 + 48 + 20
        let titleHeight = model.title?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.h4.regular) ?? 0.0
        let descriptionHeight = model.subTitle?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.h3.regular) ?? 0.0
        let hintHight = model.hintText?.height(withWidth: screensize.width - widthConstraints - 24 , font: DLSFont.bodyText.regular) ?? 0.0
        return   imageHeight + titleHeight + descriptionHeight + hintHight + 10
    }
}
