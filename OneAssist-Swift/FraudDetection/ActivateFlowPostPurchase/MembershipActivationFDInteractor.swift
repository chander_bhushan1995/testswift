//
//  MembershipActivationFDInteractor.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 06/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MembershipActivationFDPresentationLogic {
    func prepareViewModelOfMembershipActivationCells(from tempCustInfoModel: TemporaryCustomerInfoModel?)
    func getDocsToUploadResponseRecieved(response: GetDocsToUploadResponseDTO?, error: Error?)
    func getTempcustomerAndDocsToUploadResponseRecieved(docResposne: GetDocsToUploadResponseDTO?,tempCustomerResponse: TemporaryCustomerInfoResponseDTO? , getDocError: Error?, getTempError: Error?)
    func getupdatedTempCustomerResponseRecieved(response: TemporaryCustomerInfoResponseDTO?, error: Error?)
}

class MembershipActivationFDInterator: BaseInteractor, MembershipActivationFraudDetectionBusinessLogic {    
    var presenter: MembershipActivationFDPresentationLogic?
    
    var tempCustomerResponse: TemporaryCustomerInfoResponseDTO?
    var tempCustomerError: Error?
    
    var getDocToUploadResponse: GetDocsToUploadResponseDTO?
    var getDocToUploadError: Error?
    
    // MARK: Business Logic Conformance
    func getViewModelOfMembershipActivationCells(from tempCustInfoModel: TemporaryCustomerInfoModel?) {
        presenter?.prepareViewModelOfMembershipActivationCells(from: tempCustInfoModel)
    }
    
    func getDocsToUpload(for activationCode: String?) {
        let req = GetDocsToUploadRequestDTO()
        req.activationCode = activationCode ?? ""
        
        let _ = GetDocsToUploadUseCase.service(requestDTO: req) { [weak self] (usecase, response, error) in
            self?.presenter?.getDocsToUploadResponseRecieved(response: response, error: error)
        }
    }
    
    func getTempCustomerWithDocsToUpload(for activationCode: String?) {
        let group = DispatchGroup()
        
        //Temp Customer
        group.enter()
        let req = TemporaryCustomerInfoRequestDTO()
        req.activationCode = activationCode ?? ""
        let _ = TemporaryCustomerInfoRequestUseCase.service(requestDTO: req) { [weak self] (usecase, response, error) in
            
            self?.tempCustomerResponse = response
            self?.tempCustomerError = error
            group.leave()
        }
        
        //Get DocsToUpload
        group.enter()
        let reqs = GetDocsToUploadRequestDTO()
        reqs.activationCode = activationCode ?? ""
        let _ = GetDocsToUploadUseCase.service(requestDTO: reqs) { [weak self] (usecase, response, error) in
            
            self?.getDocToUploadResponse = response
            self?.getDocToUploadError = error
            group.leave()
        }
        
        group.notify(queue: .main) {
            self.presenter?.getTempcustomerAndDocsToUploadResponseRecieved(docResposne: self.getDocToUploadResponse, tempCustomerResponse: self.tempCustomerResponse, getDocError: self.getDocToUploadError, getTempError: self.tempCustomerError)
        }
    }
    
    func getUpdatedTempCustomer(for activationCode: String?) {
        let req = TemporaryCustomerInfoRequestDTO()
        req.activationCode = activationCode ?? ""
        let _ = TemporaryCustomerInfoRequestUseCase.service(requestDTO: req) { [weak self] (usecase, response, error) in
            self?.presenter?.getupdatedTempCustomerResponseRecieved(response: response, error: error)
            
        }
    }
    
}
