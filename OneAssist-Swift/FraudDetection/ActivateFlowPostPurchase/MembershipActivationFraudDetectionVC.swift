//
//  MembershipActivationFraudDetectionVC.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 04/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import Photos

protocol MembershipActivationFraudDetectionBusinessLogic {
    func getViewModelOfMembershipActivationCells(from tempCustInfoModel: TemporaryCustomerInfoModel?)
    
    func getDocsToUpload(for activationCode: String?)

    func getUpdatedTempCustomer(for activationCode: String?)

    func getTempCustomerWithDocsToUpload(for activationCode: String?)
}

class MembershipActivationFraudDetectionVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var letsGoButon: OAPrimaryButton!
    @IBOutlet weak var stickyView: OAStickyView!
    @IBOutlet weak var stickyViewHeightConstraint: NSLayoutConstraint!
    
    var interactor: MembershipActivationFraudDetectionBusinessLogic?
    var dataSource: [ActivationMembershipCellViewModel] = []
    
    // It is populated when tempCustInfoModel is not present. It will be used when user is coming from payment success screen
    var activationCode: String?
    var isAutoRedirect: Bool = false
    var countLabels = [UIView]()
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- Action Methods
    @objc func handleTempCustomerRefresh() {
        Utilities.addLoader(onView: view,count: &loaderCount, isInteractionEnabled: true)
        interactor?.getUpdatedTempCustomer(for: FDCacheManager.shared.tempCustInfoModel?.activationCode)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        stickyView.isHidden = false
        stickyViewHeightConstraint.constant = 80
        
        // Here need to decide whether to redirect further or load table view by populating the view model from tempCustInfoModel
        interactor?.getViewModelOfMembershipActivationCells(from: FDCacheManager.shared.tempCustInfoModel)
    }

    
    private func addBarButtons() -> UIView {
        let chatNotificationView = Utilities.getBadgeView1(target: self,image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: #selector(self.chatClicked(_:)))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView)]
        return chatNotificationView
    }
    
    func setBottomButton() {
        self.letsGoButon.setTitle("", for: .normal)
        if FDCacheManager.shared.tempCustInfoModel != nil {
            if FDCacheManager.shared.fdTestType == .MT {
               self.letsGoButon.setTitle(Strings.ServiceRequest.startPlanctivation, for: .normal)
           }else {
               self.letsGoButon.setTitle(Strings.ServiceRequest.letsBegin, for: .normal)
            }
       }
    }
    
    @objc func chatClicked(_ sender: Any) {
        present(ChatVC(), animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .membershipActivationFDVC, object: nil)
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        title = Strings.Global.membershipActivation
        
        tableView.register(cell: MembershipActivationCell.self)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 100
        
        tableView.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.white
        
        /* To remove the default padding for grouped table view. */
        tableView.removeDefaultPaddingForGrouped()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleTempCustomerRefresh), name: Notification.Name.membershipActivationFDVC, object: nil)
        
        countLabels.append(addBarButtons())
        
        Utilities.addLoader(onView: view,count: &loaderCount, isInteractionEnabled: false)
        if let tempCustomer = FDCacheManager.shared.tempCustInfoModel {
            setInitliseEvent()
            interactor?.getDocsToUpload(for: tempCustomer.activationCode)
        }else {
            interactor?.getTempCustomerWithDocsToUpload(for: self.activationCode)
        }
        setBottomButton()
    }
    
    func setInitliseEvent() {
        if self.fromScreen == .PEPaymentSuccessScreen {
            EventTracking.shared.eventTracking(name: .continueActivation ,[.type: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
        }
    }
    
    
    @IBAction func beginButtonPressed(_ sender: Any) {
        let startedModel = dataSource.filter({$0.stage == .started}).first
        routeLogicForViewModel(startedModel: startedModel)
    }
}

extension MembershipActivationFraudDetectionVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MembershipActivationCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.delegate = self
        cell.totalCellCount = dataSource.count
        cell.fdTestType = FDCacheManager.shared.fdTestType
        cell.setViewModel(with: dataSource[indexPath.row], and: indexPath)
        return cell
    }
}

extension MembershipActivationFraudDetectionVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !dataSource.isEmpty {
            let headerView = Bundle.main.loadNibNamed(Constants.NIBNames.membershipActivationHeader, owner: nil, options: nil)?.first as? MembershipActivationHeader
            headerView?.setUIElement(with: FDCacheManager.shared.tempCustInfoModel)
            return headerView
        }
        return UIView()
    }
}

extension MembershipActivationFraudDetectionVC: MembershipActivationFDDisplayLogic {
    func setGetDocsToUploadResponse(with response: GetDocsToUploadResponseDTO?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        FDCacheManager.shared.getDocsToUploadModel = response
    }
    
    func setGetTempCustomerAndGetDocsToUploadResponse(_ tempCustomerresponse: TemporaryCustomerInfoModel?, _ getDocsToUploadResponse: GetDocsToUploadResponseDTO?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        FDCacheManager.shared.tempCustInfoModel = tempCustomerresponse
        FDCacheManager.shared.tempCustInfoModel?.activationCode = activationCode
        FDCacheManager.shared.getDocsToUploadModel = getDocsToUploadResponse
        setBottomButton()
        setInitliseEvent()
        // Here need to decide whether to redirect further or load table view by populating the view model from tempCustInfoModel
        interactor?.getViewModelOfMembershipActivationCells(from: FDCacheManager.shared.tempCustInfoModel)
    }
    
    func setUpdatedGetTempCustomerResponse(with response: TemporaryCustomerInfoModel?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        let activationCode = FDCacheManager.shared.tempCustInfoModel?.activationCode
        FDCacheManager.shared.tempCustInfoModel = response
        FDCacheManager.shared.tempCustInfoModel?.activationCode = activationCode
        setBottomButton()
        // Here need to decide whether to redirect further or load table view by populating the view model from tempCustInfoModel
        interactor?.getViewModelOfMembershipActivationCells(from: FDCacheManager.shared.tempCustInfoModel)
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
    
    func displayMembershipActivationCells(with activationCellViewModel: [ActivationMembershipCellViewModel]) {
        self.dataSource = activationCellViewModel
        
        if FDCacheManager.shared.fdTestType != .MT {
            if dataSource.first?.stage == .completed {
               stickyView.isHidden = true
               stickyViewHeightConstraint.constant = 0
           }
        }
        
        if tableView != nil {
            tableView.reloadData()
        }
        
        if isAutoRedirect {
            isAutoRedirect = false
            let startedModel = dataSource.filter({$0.stage == .started}).first
            routeLogicForViewModel(startedModel: startedModel)
        }
    }
    
}

extension MembershipActivationFraudDetectionVC: MembershipActivationCellDelegate {
    
    // Implement Cell methods
    func clickedButtonViewDetails(_ cell: MembershipActivationCell, clickedActionBtn sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            self.routeLogicForViewModel(startedModel: dataSource[indexPath.row])
        }
    }
    
}

// MARK:- Configuration Logic
extension MembershipActivationFraudDetectionVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MembershipActivationFDInterator()
        let presenter = MembershipActivationFDPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension MembershipActivationFraudDetectionVC: PermissionScreenVCDelegate {
    func routeToIMEIVerifyScreen() {
        routeToVerifyIMEIScreen()
    }
}

// MARK:- Routing Logic
extension MembershipActivationFraudDetectionVC {
    
    func routeLogicForViewModel(startedModel: ActivationMembershipCellViewModel?) {
        if startedModel?.activationStep == .iMEIFetch {
            routeToGetIMEI()
            
        }else if startedModel?.activationStep == .basicDetailFill {
            
            let keyName = (FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].orderId?.stringValue)! + "_key"
            if UserDefaults.standard.value(forKey: keyName) != nil ||
            FDCacheManager.shared.fdTestType == .MT {
                EventTracking.shared.eventTracking(name: .activationWelcome ,[.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
                routeToRegistrationScreen()
                
            }else {
                let bottomSheet = FDBottomSheetView()
                bottomSheet.buttonAction = {[unowned self] value in
                    Utilities.topMostPresenterViewController.dismiss(animated: true)
                    print(value)
                    if value == "agree" {
                        EventTracking.shared.eventTracking(name: .InvoiceDeclaration, [.mode: FDCacheManager.shared.fdTestType.rawValue])
                        EventTracking.shared.eventTracking(name: .activationWelcome ,[.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
                        self.routeToRegistrationScreen()
                        UserDefaults.standard.setValue("agreed", forKey: keyName)
                        UserDefaults.standard.synchronize()
                    }
                }
                Utilities.presentPopover(view: bottomSheet, height: bottomSheet.height())
            }
        } else if startedModel?.activationStep == .sMSSending {
            routeToSendSMSScreen()
            
        } else if startedModel?.activationStep == .mirrorText {
            routeToMirrorTestVideoGuide()
        }
    }
    
    func routeToGetIMEI() {
        if (Permission.checkGalleryPermission() == PHAuthorizationStatus.authorized) {
            routeToVerifyIMEIScreen()
            
        } else {
            routeToAllowPermission()
        }
    }
    
    func routeToAllowPermission() {
        let vc = PermissionScreenVC()
        vc.delegate = self
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToVerifyIMEIScreen() {
        let vc = IMEIVerificationVC()
        if FDCacheManager.shared.fdTestType == .MT {
            vc.completionHandler = {[unowned self] in
                self.isAutoRedirect = true
                vc.navigationController?.popViewController(animated: false)
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToSendSMSScreen() {
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?[0].orderId?.stringValue
        let vc = SendSMSScreenVC()
        vc.imeiNumber = SaveFDDataMode.getSaveData(orderId)?.imei
        vc.imeiNumber2 = SaveFDDataMode.getSaveData(orderId)?.imei2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToRegistrationScreen() {
        let membershipDetailsFDVC = MembershipDetailsFraudDetectionVC()
        navigationController?.pushViewController(membershipDetailsFDVC, animated: true)
    }
    
    func routeToMirrorTestVideoGuide() {
        let vc = VideoGuideViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}
