//
//  StartActivationBottomSheet.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 08/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

protocol StartActivationBottomSheetDelegate {
    func didTappedStartActivation(activationCode: String?, activityRefId: String?)
}

class StartActivationBottomSheet: UIView {

    @IBOutlet var rootView: UIView!
    @IBOutlet weak var headingLabel: H4BoldLabel!
    @IBOutlet weak var subHeadingLabel: H4BoldLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularBlackLabel!
    
    
    var activationCode: String?
    var activityRefId: String?
    var delegate: StartActivationBottomSheetDelegate?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed(Constants.NIBNames.startActivationBottomSheet , owner: self, options: nil)
        addSubview(rootView)
        rootView.frame = self.bounds
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        rootView.roundCornersWithBorderAndShadow(UIRectCorner(arrayLiteral: [.topLeft,.topRight]), radius: 20, with: 0, and: nil, with: nil, and: nil, and: nil, and: nil)
    }
    
    func setViewModel(coverAmount: String, brand: String) {
        headingLabel.text = String(format: Strings.Global.userSalutation, UserCoreDataStore.currentUser?.userName ?? "")
        subHeadingLabel.text = String(format: Strings.Global.activatePE_ADLD_Plan, brand)
        if !coverAmount.isEmpty, let coverAmount = coverAmount.numberWithComma() {
            descriptionLabel.text = String(format: Strings.Global.unlockPlanWithCoverAmt, coverAmount)
        } else {
            descriptionLabel.text = Strings.Global.unlockPlanBenefits
        }
    }
    
    @IBAction func didTappedStartActivation(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .startPlanActivation, [.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
        delegate?.didTappedStartActivation(activationCode: self.activationCode, activityRefId: self.activityRefId)
    }
    
    func height() -> CGFloat {
        var totalHeight: CGFloat = 0
        let titleHeight = (headingLabel.text?.height(withWidth: screensize.width - 48, font: DLSFont.h4.bold) ?? 0.0) + 50 // height + top is added
        let subTitleHeight = (subHeadingLabel.text?.height(withWidth: screensize.width - 48, font: DLSFont.h4.bold) ?? 0.0) + 18 // height + top is added
        let descriptionHeight = (descriptionLabel.text?.height(withWidth: screensize.width - 48, font: DLSFont.bodyText.bold) ?? 0.0) + 18 // height + top is added
        let buttonHeight: CGFloat = 48 + 50 + 20 // height + top + bottom is added
        totalHeight += titleHeight + subTitleHeight + descriptionHeight + buttonHeight
        return totalHeight
    }
    
}
