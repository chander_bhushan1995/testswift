//
//  DownloadingStatusPresenter.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 08/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

protocol DownloadingStatusDisplayLogic: class {
    func showInitialData(_ data: ModelDownloadingStatusScreenData?)
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?)
    func showFDTestTypeChangeSuccess()
}

class DownloadingStatusPresenter: BasePresenter {
    weak var viewController: DownloadingStatusDisplayLogic?
}

extension DownloadingStatusPresenter: DownloadingStatusPresenterLogic {

    func presentChangedFDType(_ response: BaseResponseDTO?, _ error: Error?, reasonToChange: String?) {
        do {
            try checkError(response, error: error)
            
            // Success case
            EventTracking.shared.eventTracking(name: .moveToSDT, [.mode: FDCacheManager.shared.fdTestType.rawValue, .reason: reasonToChange ?? ""])
            viewController?.showFDTestTypeChangeSuccess()
        } catch let error as LogicalError {
            viewController?.showChangeFDTestTypeError(error.errorDescription!, reasonToChange: reasonToChange)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
    
    func presentInitialLoadedData(_ data: ModelDownloadingStatusScreenData?) {
        viewController?.showInitialData(data)
    }
}


