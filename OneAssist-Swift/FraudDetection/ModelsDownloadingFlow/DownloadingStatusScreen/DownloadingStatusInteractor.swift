//
//  DownloadingStatusInteractor.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 08/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

protocol DownloadingStatusPresenterLogic {
    func presentInitialLoadedData(_ data: ModelDownloadingStatusScreenData?)
    func presentChangedFDType(_ response: BaseResponseDTO?,_ error: Error?, reasonToChange: String?)
}

class DownloadingStatusInteractor {
    var presenter: DownloadingStatusPresenterLogic?
}

extension DownloadingStatusInteractor: DownloadingStatusBusinessLogic {
    
    func changeFDFlowType(testType: FDTestType, activityReferenceId: String?, reasonToChange: String?) {
        let request = FDTestTypeChangeRequestDTO(modelType: testType.getAPIValue(), reasonValue: reasonToChange, activityReferenceId: activityReferenceId)
        FDTestTypeChangeUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.presentChangedFDType(response, error, reasonToChange: reasonToChange)
        }
    }
    
    func loadinitialData() {
        let _ = ModelDownloadingStatusScreenUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.presentInitialLoadedData(response)
        }
    }
}
