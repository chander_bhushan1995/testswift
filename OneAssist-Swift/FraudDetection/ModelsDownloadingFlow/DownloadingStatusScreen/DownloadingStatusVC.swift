//
//  DownloadingStatusVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 08/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit


//MARK: Interector Methods
protocol DownloadingStatusBusinessLogic {
    func loadinitialData();
    func changeFDFlowType(testType: FDTestType, activityReferenceId: String?, reasonToChange: String?)
}

class DownloadingStatusVC: BaseVC {
    
    @IBOutlet weak var closeIconImageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var downloadRemainingTimeLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var collectionViewHeight:
        NSLayoutConstraint!
    @IBOutlet weak var titleLabel: H2BoldLabel!
    
    fileprivate var interactor: DownloadingStatusBusinessLogic?
    
    private var screenViewModel: ModelDownloadingStatusScreenData?
    private var SECTION_INSET: CGFloat = 8
    private var downloadManager = TFLiteModelsDownloadManager.sharedInstance
    
    var onDownloadCompletion: (()->())? = nil
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = ""
        pageControl.tintColor = .clear
        pageControl.backgroundColor = .clear
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .dodgerBlue
        pageControl.numberOfPages = screenViewModel?.cards?.count ?? 0
        
        progressView.progress = 0
        progressView.trackTintColor = .progressTrackTint
        progressView.progressTintColor = .buttonTitleBlue
        progressView.setCornerRadius(cornerRadius: 2.5)
        downloadRemainingTimeLabel.text = Strings.Global.pleaseWhileSetupDownloading
        
        collectionView.register(cell: PEadldBenefitsCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.clear
        
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(didTappedOnClose(_:)))
        closeIconImageView.isUserInteractionEnabled = true
        closeIconImageView.addGestureRecognizer(tapGuesture)
        
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        interactor?.loadinitialData()
        
        TFLiteModelsDownloadManager.sharedInstance.progressView = progressView
        TFLiteModelsDownloadManager.sharedInstance.estimatedTimeLabel = downloadRemainingTimeLabel
        
        EventTracking.shared.eventTracking(name: .activationSetupScreen, [.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let error = TFLiteModelsDownloadManager.sharedInstance.currentlyOccuredError {
            self.onError(error: error)
        }
    }
    
    @IBAction func didTappedOnClose(_ sender: UITapGestureRecognizer) {
        Utilities.topMostPresenterViewController.dismiss(animated: true)
        rootTabVC?.showDownloadingStatusStrip()
    }
    
    func getMaxCellHeight() -> CGFloat {
        if let _ = screenViewModel {
            var maxHeight: CGFloat = 0
            for (index,card) in (screenViewModel?.cards ?? []).enumerated() {
                let cell: PEadldBenefitsCollectionViewCell = collectionView.dequeueReusableCell(indexPath: IndexPath(item: index, section: 0))
                cell.setViewModel(card)
                maxHeight = max(cell.height(), maxHeight)
            }
            return maxHeight
        }
        return 0
    }
    
    
    @IBAction func pageControlSelectionAction(_ sender: UIPageControl) {
        let page: Int? = sender.currentPage
        if let currentPage = page {
            var frame: CGRect = self.collectionView.frame
            frame.origin.x = frame.size.width * CGFloat(currentPage)
            frame.origin.y = 0
            self.collectionView.scrollRectToVisible(frame, animated: true)
        }
    }
    
    deinit {
        TFLiteModelsDownloadManager.sharedInstance.progressView = nil
        TFLiteModelsDownloadManager.sharedInstance.estimatedTimeLabel = nil
    }
}

// MARK:- DEPENDENCY CONFIGURATION
extension DownloadingStatusVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = DownloadingStatusInteractor()
        let presenter = DownloadingStatusPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}


// MARK:- CollectionView DataSource
extension DownloadingStatusVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return screenViewModel?.cards?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PEadldBenefitsCollectionViewCell = collectionView.dequeueReusableCell(indexPath: indexPath)
        cell.setViewModel(screenViewModel?.cards?[indexPath.item])
        return cell
    }
}

// MARK:- CollectionView Delegate
extension DownloadingStatusVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        return CGSize(width: screensize.width - (SECTION_INSET + (layout.minimumLineSpacing/2)), height: getMaxCellHeight())
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: SECTION_INSET, bottom: 0, right: SECTION_INSET)
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludeingSpacing = screensize.width - (SECTION_INSET + (layout.minimumLineSpacing/2))
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludeingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludeingSpacing - scrollView.contentInset.left, y: scrollView.contentInset.top)
        pageControl.currentPage = Int(roundedIndex)
        targetContentOffset.pointee = offset
    }
}

// MARK:- DownloadingStatusDisplayLogic
extension DownloadingStatusVC: DownloadingStatusDisplayLogic {
    
    func showInitialData(_ data: ModelDownloadingStatusScreenData?) {
        self.screenViewModel = data
        titleLabel.text = data?.title
        collectionView.reloadData()
        collectionViewHeight.constant = getMaxCellHeight()
        pageControl.numberOfPages = screenViewModel?.cards?.count ?? 0
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.showAlert(title: "", message: message, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
            self.interactor?.changeFDFlowType(testType: .SDT, activityReferenceId: TFLiteModelsDownloadManager.sharedInstance.activityRefId, reasonToChange: reasonToChange)
        }) {
            self.downloadManager.resetEverything()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showFDTestTypeChangeSuccess() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        FDCacheManager.shared.changeTestType(to: .SDT)
        if let activationCode = TFLiteModelsDownloadManager.sharedInstance.activationCode {
            if let navVC = UIApplication.shared.delegate?.window??.rootViewController as? BaseNavigationController {
                if let presentedViewController = navVC.topViewController?.presentedViewController {
                    presentedViewController.dismiss(animated: false, completion: {
                        let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
                        membershipActivationFDVC.activationCode = activationCode
                        membershipActivationFDVC.fromScreen = TFLiteModelsDownloadManager.sharedInstance.fromScreen
                        navVC.pushViewController(membershipActivationFDVC, animated: true)
                    })
                }
            }
        }
        TFLiteModelsDownloadManager.sharedInstance.resetEverything()
    }
}

// MARK: - TFLiteModelsDownloadManagerDelegate
extension DownloadingStatusVC: TFLiteModelsDownloadManagerDelegate {
    
    func showActivationErrorScreen(for error: Error) {
        let errorVC = MirrorTestErrorScreenVC()
        errorVC.errorScreenFor = .NetworkIssue
        errorVC.currentError = error
        errorVC.delegate = self
        self.presentOverFullScreen(errorVC, animated: true, completion: nil)
    }
    
    func onError(error: TFLiteModelsDownloadError) {
        if error == .ModelsLoadingError || error == .RetryExceeded { // SDT Flow Directly
            let errorReason = error == .ModelsLoadingError ? downloadManager.mirrorTestErrorMessages?.modelLoadingRetryExceeded : downloadManager.mirrorTestErrorMessages?.downloadingRetryExceeded
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
            self.interactor?.changeFDFlowType(testType: .SDT, activityReferenceId: self.downloadManager.activityRefId, reasonToChange: errorReason)
        } else {
            if !(Utilities.topMostPresenterViewController is MirrorTestErrorScreenVC) {
                showActivationErrorScreen(for: error)
            }
        }
    }
    
    func hideNoInternetErrorView() {
        if let _ = Utilities.topMostPresenterViewController as? MirrorTestErrorScreenVC {
            Utilities.topMostPresenterViewController.dismiss(animated: false, completion: nil)
        }
    }
    
    func startActivation(for activationCode: String?, with activityRefId: String?, fromScreen:FromScreen, openFromNotification: Bool = false) {
        if let _ = onDownloadCompletion { // basically usefull for reupload case and models not downloaded
            Utilities.dismissAllPresentedVC { [weak self] in
                self?.onDownloadCompletion?()
            }
            return
        }
        if let activationCode = activationCode {
            if let navVC = UIApplication.shared.delegate?.window??.rootViewController as? BaseNavigationController {
                if let presentedViewController = navVC.topViewController?.presentedViewController {
                    presentedViewController.dismiss(animated: false, completion: {
                        let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
                        membershipActivationFDVC.activationCode = activationCode
                        membershipActivationFDVC.fromScreen = fromScreen
                        navVC.pushViewController(membershipActivationFDVC, animated: true)
                    })
                }
            }
        }
    }
}

// MARK: - MirrorTestErrorScreenDelegate
extension DownloadingStatusVC: MirrorTestErrorScreenDelegate {
    func onRetry(with error: Error?) {
        downloadManager.handleRetry(with: error)
    }
}
