//
//  PE_ADLD_BenefitsCollectionViewCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 08/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

class PEadldBenefitsCollectionViewCell: UICollectionViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var containerView: DLSCurvedView!
    @IBOutlet weak var headingLabel: H3BoldLabel!
    @IBOutlet weak var benefitsListView: UIStackView!
    
    var model: NotesData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 8
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.cellBlueBorder.cgColor
    }
    
    func setViewModel(_ data: NotesData?){
        if let data = data {
            model = data
            headingLabel.text = data.title
            benefitsListView.isHidden = false
            if benefitsListView.arrangedSubviews.count <= 0 {
                for benefit in data.listPoints ?? [] {
                    let benefitView = LeftImageRightTextView()
                    benefitView.setViewModel(model: benefit)
                    benefitsListView.addArrangedSubview(benefitView)
                }
            }
        }
    }
    
    func height() -> CGFloat {
        var totalHeight: CGFloat = 0
        let titleHeight = (model?.title?.height(withWidth: screensize.width - 48 - 16, font: DLSFont.h3.bold) ?? 0.0) + 20 // height + top is added
        var listModel = ListViewModel()
         listModel.listFont = DLSFont.bodyText.regular
        listModel.list = model?.listPoints?.compactMap({$0.title}) ?? []
        var listViewHeight: CGFloat = 48 // 24 top 24 bottom
        for (index, benefit) in (model?.listPoints ?? []).enumerated() {
            let benefitTop: CGFloat = 20
            let benefitViewHeight = max(32, benefit.title?.height(withWidth: (screensize.width - 16 - 48 - 8), font: DLSFont.bodyText.regular) ?? 0) // 32 image height,
            listViewHeight += (benefitViewHeight + benefitTop)
        }
        totalHeight += titleHeight + listViewHeight
        return totalHeight
    }
}
