//
//  LeftImageRightTextView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 10/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

class LeftImageRightTextView: UIView {

    @IBOutlet var rootView: UIView!
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightText: BodyTextRegularBlackLabel!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() -> LeftImageRightTextView {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed(Constants.NIBNames.leftImageRightTextView, owner: self, options: nil)
        addSubview(rootView)
        rootView.frame = self.bounds
        return self
    }

    func setViewModel(model: ListPoint) {
        leftImageView.setImageWithUrlString(model.image)
        rightText.text = model.title
    }
    
}
