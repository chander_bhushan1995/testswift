//
//  MirrorTestErrorScreenVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 10/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

enum ErrorScreen {
    case NetworkIssue
    case StartSDT
}

@objc protocol MirrorTestErrorScreenDelegate: AnyObject {
    func onRetry(with error: Error?)
    @objc optional func onStartSDTtapped()
}

class MirrorTestErrorScreenVC: BaseVC {
    
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var headingLabel: H2BoldLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var secondaryButton: OASecondaryButton!
    @IBOutlet weak var primaryButton: OAPrimaryButton!
    
    var deviceModel: String?
    var errorScreenFor: ErrorScreen = .NetworkIssue
    var currentError: Error?
    weak var delegate: MirrorTestErrorScreenDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch errorScreenFor {
        case .NetworkIssue:
            headingLabel.text = Strings.MirrorTestErrorScreen.badNetwork
            descriptionLabel.text = Strings.MirrorTestErrorScreen.makeSureInternet
            errorImageView.image = #imageLiteral(resourceName: "internetError")
            primaryButton.setTitle(Strings.MirrorTestErrorScreen.retryButton, for: .normal)
            secondaryButton.isHidden = true
            let downloadRate = TFLiteModelsDownloadManager.sharedInstance.oaDownloadManager.downloadRateAndRemainingTime()?.downloadRate ?? ""
            let downloadedPercentage: Int = Int(TFLiteModelsDownloadManager.sharedInstance.oaDownloadManager.overallProgress() * 100)
            EventTracking.shared.eventTracking(name: .poorInternetScreenview, [.mode: FDCacheManager.shared.fdTestType.rawValue, .downloadSpeed: downloadRate,.downloadCompleted: "\(downloadedPercentage)%"])
        case .StartSDT:
            headingLabel.text = Strings.MirrorTestErrorScreen.setUpFailed
            descriptionLabel.text = String(format: Strings.MirrorTestErrorScreen.needAnotherPhone, deviceModel ?? UIDevice.deviceModel)
            errorImageView.image = UIImage(named: "MHC_test_fail")
            primaryButton.setTitle(Strings.MirrorTestErrorScreen.showMeHow, for: .normal)
            secondaryButton.setTitle(Strings.MirrorTestErrorScreen.retryButton, for: .normal)
            secondaryButton.isHidden = false
            EventTracking.shared.eventTracking(name: .moveTOSDTScreenView, [.mode: FDCacheManager.shared.fdTestType.rawValue])
        }
    }
    
    @IBAction func didTappedSecondaryButton(_ sender: Any) {
        self.dismiss(animated: true) { [weak self] in
            self?.delegate?.onRetry(with: self?.currentError)
        }
    }
    
    @IBAction func didTappedPrimaryButton(_ sender: Any) {
        if errorScreenFor == .NetworkIssue, TFLiteModelsDownloadManager.sharedInstance.isInternetGone { // if internet is not available
            EventTracking.shared.eventTracking(name: .noInternetPopup, [.location: FDCacheManager.shared.fdTestType.rawValue])
            self.showAlert(title: Strings.Global.noInternetConnection, message: Strings.Global.noInternetMessage, primaryButtonTitle: Strings.Global.okay, nil, isCrossEnable: true, primaryAction: {
            }, nil)
            return
        }
        self.dismiss(animated: true) { [weak self] in
            switch self?.errorScreenFor {
            case .NetworkIssue:
                EventTracking.shared.eventTracking(name: .poorInternetRetry, [.mode: FDCacheManager.shared.fdTestType.rawValue])
                self?.delegate?.onRetry(with: self?.currentError)
            case .StartSDT:
                self?.delegate?.onRetry(with: self?.currentError)
                self?.delegate?.onStartSDTtapped?()
            default:
                break
            }
        }
    }
    
}
