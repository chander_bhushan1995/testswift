//
//  OATextToSpeech.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 21/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation
import Speech

protocol OATextToSpeechDelegate: class {
    func speechDidStart()
    func speechDidFinish()
}

class OATextToSpeech: NSObject {
    weak var speechSynthesizerDelegate:OATextToSpeechDelegate?
    
    //MARK:- Speech Synthesizer Variables
    private var speechSynthesizer: AVSpeechSynthesizer!
    var rate: Float = 0.0
    var pitch: Float = 0.0
    var volume: Float = 0.0
    var delay: TimeInterval = Double(0.0)
    var preferredVoiceLanguageCode: String!
    var text:String = ""
    
    //MARK:- Main Funcs
    override init() {
        super.init()
        speechSynthesizer = AVSpeechSynthesizer()
        setupTextToSpeech()
    }
    
    
    // MARK:- AVSpeechSynthesizer
    private func setupTextToSpeech() {
        
        if !loadSettings() {
            registerDefaultSettings()
        }
        
        speechSynthesizer.delegate = self
        
    }
    
    private func registerDefaultSettings() {
        rate = AVSpeechUtteranceDefaultSpeechRate
        pitch = 1.0
        volume = 1.0
        
        let defaultSpeechSettings = ["rate": rate, "pitch": pitch, "volume": volume]
        
        UserDefaults.standard.register(defaults: defaultSpeechSettings)
    }
    
    
    private func loadSettings() -> Bool {
        let userDefaults = UserDefaults.standard as UserDefaults
        
        if let theRate: Float = userDefaults.value(forKey: "rate") as? Float {
            rate = theRate
            pitch = userDefaults.value(forKey:"pitch") as! Float
            volume = userDefaults.value(forKey:"volume") as! Float
            
            return true
        }
        
        return false
    }
    
    
    func speak() {
        if speechSynthesizer.isSpeaking  {
            self.stopSpeech()
        }
        let speechUtterance = AVSpeechUtterance(string: text)
        speechUtterance.rate = rate
        speechUtterance.pitchMultiplier = pitch
        // speechUtterance.volume = volume
        speechUtterance.preUtteranceDelay = delay/1000
        
        if let voiceLanguageCode = preferredVoiceLanguageCode {
            let voice = AVSpeechSynthesisVoice(language: voiceLanguageCode)
            speechUtterance.voice = voice
        }
        speechSynthesizer.speak(speechUtterance)
        
    }
    
    
    func pauseSpeech() {
        speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
    }
    
    func stopSpeech() {
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }

}

// MARK: AVSpeechSynthesizerDelegate method implementation
extension OATextToSpeech: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        print("AVSpeechSynthesizerDelegate: didFinish")
        speechSynthesizerDelegate?.speechDidFinish()
    }
    
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        print("AVSpeechSynthesizerDelegate: didStart")
        speechSynthesizerDelegate?.speechDidStart()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didPause utterance: AVSpeechUtterance) {
      print("AVSpeechSynthesizerDelegate: didPause")
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didContinue utterance: AVSpeechUtterance) {
      print("AVSpeechSynthesizerDelegate: didContinue")
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
      print("AVSpeechSynthesizerDelegate: didCancel")
    }

}




