//
//  ScreenshotCapture.swift
//  iOSFraudDetection
//
//  Created by Ankur Batham on 21/08/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//
import UIKit
import Photos
import CallKit

public protocol ScreenshotCaptureDelegate: class {
    func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didDetect screenshot: UIImage)
    func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didFailWith error: ScreenshotCapture.Error)
}

open class ScreenshotCapture: NSObject {
    
    public enum Error: Swift.Error {
        case unauthorized(status: PHAuthorizationStatus)
        case fetchFailure
        case loadFailure
        case tryAgain
        case unKnown
    }
    
    private var screenshotObserver: Any?
    private weak var delegate: ScreenshotCaptureDelegate?
    private let notificationCenter: NotificationCenter = .default
    private let application: UIApplication = .shared
    fileprivate let photoLibrary: PHPhotoLibrary = .shared()
    private let imageManager: PHImageManager = .default()
    
    private var prePhotos: PHFetchResult<PHAsset>!
    private var newPhotos: PHFetchResult<PHAsset>!
    
    private let callObserver = CXCallObserver()
    fileprivate var didDetectOutgoingCall = false
    fileprivate var takeScreenShot = false
    fileprivate var isApplicationInActiveState: Bool?
    private var screenShotCaptureDate: Date = Date()
    var isContinuousCapture: Bool = false
    
    public init(delegate: ScreenshotCaptureDelegate) {
        self.delegate = delegate
        super.init()
        
        requestPhotosAuthorization()
        callObserver.setDelegate(self, queue: DispatchQueue.main)
        self.didDetectOutgoingCall = false
        self.setupPhotoAsset()
    }
    
    deinit {
        unregisterScreenShotObserver()
        self.photoLibrary.unregisterChangeObserver(self)
    }
    
    // check photo Authorization permission
    func requestPhotosAuthorization(andThen f:(()->())? = nil) {
        sharedAppDelegate?.isShowBlurView = false
        PHPhotoLibrary.requestAuthorization { authorizationStatus in
            OperationQueue.main.addOperation {
                switch authorizationStatus {
                case .authorized:
                    f?()
                    break
                case .denied, .notDetermined, .restricted:
                    self.fail(with: .unauthorized(status: authorizationStatus))
                @unknown default:
                    self.fail(with: .unKnown)
                    fatalError()
                }
            }
        }
    }
    
    /*
     get previous screen shot for validation
     */
    fileprivate func setupPhotoAsset() {
        let prePhotosOptions = PHFetchOptions()
        prePhotosOptions.fetchLimit = 1
        prePhotosOptions.includeAssetSourceTypes = [.typeUserLibrary]
        prePhotosOptions.predicate = NSPredicate(format: "(mediaSubtype & %d) != 0", PHAssetMediaSubtype.photoScreenshot.rawValue)
        prePhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        self.prePhotos = PHAsset.fetchAssets(with: prePhotosOptions)
        
        prePhotosOptions.fetchLimit = 2
        self.newPhotos = PHAsset.fetchAssets(with: prePhotosOptions)
    }
    
    func registerApplicationStateObserver() {
        unregisterApplicationStateObserver()
        NotificationCenter.default.addObserver(self, selector: #selector(handleApplicationActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleApplicationResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    func unregisterApplicationStateObserver() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    
    func registerScreenShotObserver() {
        unregisterScreenShotObserver()
        screenshotObserver = notificationCenter.addObserver(self, selector: #selector(ScreenshotCapture.userTookScreenshot(_:)), name: UIApplication.userDidTakeScreenshotNotification, object: application)
    }
    
    func unregisterScreenShotObserver() {
        takeScreenShot = false
        if screenshotObserver != nil {
            NotificationCenter.default.removeObserver(self, name: UIApplication.userDidTakeScreenshotNotification, object: nil)
            screenshotObserver = nil
        }
    }
    
    @objc fileprivate func handleApplicationActive() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            if self.takeScreenShot == false, self.isApplicationInActiveState == false {
                self.fail(with: .fetchFailure)
            }
        }
        if !isContinuousCapture {
            unregisterScreenShotObserver()
        }
        isApplicationInActiveState = false
    }
    
    @objc fileprivate func handleApplicationResignActive() {
        isApplicationInActiveState = true
        registerScreenShotObserver()
    }
    
    @objc private func userTookScreenshot(_ notification: Notification) {
        OAAlertVC.forceRemoveAlertView()
        takeScreenShot = true
        let state = application.applicationState
         if state != .background{
            //unregister observer when taking screen shot
            self.unregisterScreenShotObserver()
            self.unregisterApplicationStateObserver()
            
            requestPhotosAuthorization {[unowned self] in
                self.screenShotCaptureDate = Date()
                self.photoLibrary.register(self)
            }
         }
    }
    
    
    fileprivate func findScreenShot(_ screenshot: PHAsset?) {
        if let imeiscreenshot = screenshot {
            self.imageManager.requestImage(for: imeiscreenshot, targetSize: PHImageManagerMaximumSize, contentMode: .default, options: PHImageRequestOptions.highQualitySynchronousLocalOptions(), resultHandler: { [weak self] (image, info) in
                print("info: \(String(describing: info))")
                OperationQueue.main.addOperation {
                    guard let strongSelf = self else { return }
                    if let fileName = (info?["PHImageFileURLKey"] as? URL) {
                        if fileName.absoluteString.contains("/Mutations/"){ //check if screenshot has edited
                            strongSelf.fail(with: .loadFailure); return
                        }
                    }
                    guard let image = image else { strongSelf.fail(with: .loadFailure); return }
                    strongSelf.succeed(with: image)
                }
            })
            
        }else {
            self.fail(with: .fetchFailure)
        }
    }
    
    fileprivate func validateCaptureScreenShot(_ screenshot: PHAsset?) ->PHAsset? {
        if let captureScreenShot = screenshot {
            let assetCreationTime = screenshot?.creationDate?.timeIntervalSince1970
            let assetModificationTime = screenshot?.modificationDate?.timeIntervalSince1970
            let screenCaptureTime: TimeInterval = screenShotCaptureDate.timeIntervalSince1970
            
            if (assetModificationTime! - assetCreationTime! < 10) && //check modify and creation time of screenshot
                (assetCreationTime! - screenCaptureTime < 10) {  ////check creation and take screenshot'time
                return captureScreenShot
            }else {
                return nil
            }
        }
        return nil
    }
    
    private func succeed(with image: UIImage) {
        delegate?.screenshotCapture(self, didDetect: image)
    }
    
    private func fail(with error: Error) {
        delegate?.screenshotCapture(self, didFailWith: error)
    }
}


private extension PHImageRequestOptions {
    static func highQualitySynchronousLocalOptions() -> PHImageRequestOptions {
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        return options
    }
}

extension ScreenshotCapture: PHPhotoLibraryChangeObserver {
    
    // MARK: - PHPhotoLibraryChangeObserver
    public func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return }
            if self.didDetectOutgoingCall == false {
                if !self.isContinuousCapture {
                    self.photoLibrary.unregisterChangeObserver(self)
                }
                let fetchPreChangeDetails = changeInstance.changeDetails(for: self.prePhotos)
                guard (fetchPreChangeDetails) != nil else {
                    print("No change in fetchResultChangeDetails")
                    return;
                }
                
                let fetchResultBeforeChanges: PHFetchResult = (fetchPreChangeDetails?.fetchResultBeforeChanges)!
                var beforeChangesPhotoAssetIds: [String] = []
                fetchResultBeforeChanges.enumerateObjects{(object: AnyObject!,
                    count: Int,
                    stop: UnsafeMutablePointer<ObjCBool>) in
                    if object is PHAsset{
                        let asset = object as! PHAsset
                        beforeChangesPhotoAssetIds.append(asset.localIdentifier)
                    }
                }
                
                let fetchNewChangeDetails = changeInstance.changeDetails(for: self.newPhotos)
                let fetchResultAfterChanges: PHFetchResult = (fetchNewChangeDetails?.fetchResultAfterChanges)!
                var afterChangesPhotoAssetIds: [String] = []
                fetchResultAfterChanges.enumerateObjects{(object: AnyObject!,
                    count: Int,
                    stop: UnsafeMutablePointer<ObjCBool>) in
                    if object is PHAsset{
                        let asset = object as! PHAsset
                        afterChangesPhotoAssetIds.append(asset.localIdentifier)
                    }
                }
                let diffrentAssetIds = afterChangesPhotoAssetIds.filter{
                    let id = $0
                    return !beforeChangesPhotoAssetIds.contains{ id == $0 }
                }
                
                let addedItems = fetchNewChangeDetails!.insertedObjects
                if diffrentAssetIds.count == 1, addedItems.count == 1 {
                    let newId = diffrentAssetIds.first
                    let addedAsset = addedItems.first
                    
                    if addedAsset?.localIdentifier == newId {
                        self.findScreenShot(addedAsset)
                    }
                }else {
                    self.findScreenShot(nil)
                }
            }
            self.setupPhotoAsset()
        }
    }
}

extension ScreenshotCapture: CXCallObserverDelegate {
    public func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.hasEnded == true {
            print("Disconnected")
            didDetectOutgoingCall = false
        }
        if call.isOutgoing == true && call.hasConnected == false {
            print("Dialing")
            didDetectOutgoingCall = false
        }
        if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
            didDetectOutgoingCall = true
            print("Incoming")
        }
        
        if call.hasConnected == true && call.hasEnded == false {
            print("Connected")
            didDetectOutgoingCall = false
        }
        
    }
}
