//
//  OAContactModel.swift
//  OACOntactPicker
//
//  Created by Ankur Batham on 02/09/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import UIKit
import Contacts

class OAContactModel {
    
    open var firstName: String
    open var lastName: String
    open var thumbnailProfileImage: UIImage?
    open var contactId: String?
    open var displayValue: String?
    open var email: String?
    open var identifire: String?
    
    public init (contact: CNContact, displayValue: String?) {
        firstName = contact.givenName
        lastName = contact.familyName
        contactId = contact.identifier
        identifire = contact.identifier
        
        if let thumbnailImageData = contact.thumbnailImageData {
            thumbnailProfileImage = UIImage(data:thumbnailImageData)
        }
       
        self.displayValue = displayValue
    }
    
    open func displayName() -> String {
        return firstName + " " + lastName
    }
    
    open func contactInitials() -> String {
        var initials = String()
        
        if let firstNameFirstChar = firstName.first {
            initials.append(firstNameFirstChar)
        }
        
        if let lastNameFirstChar = lastName.first {
            initials.append(lastNameFirstChar)
        }
        
        return initials
    }
    
}
