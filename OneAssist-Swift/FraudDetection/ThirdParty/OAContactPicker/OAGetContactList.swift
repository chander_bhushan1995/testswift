//
//  OAGetContactList.swift
//  OACOntactPicker
//
//  Created by Ankur Batham on 02/09/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import Foundation
import Contacts

typealias ContactsListHandler = (_ status: CNAuthorizationStatus,  _ error : Error?) -> Void

class  OAGetContactList {
    var contactsStore: CNContactStore?
    
    var orderedContacts = [String: [OAContactModel]]()
    var sortedContactKeys = [String]()
    
    var allContact = [OAContactModel]()
    var filteredContacts = [OAContactModel]()
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        return [
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor,
        ]
    }
    
    func getContacts(for type:SubtitleValueType,_ completion:  @escaping ContactsListHandler) {
        if contactsStore == nil {
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "OAContactPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        let status = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch status {
        case .denied, .restricted:
            completion(status, error)
            
        case .notDetermined:
            //This case means the user is prompted for the first time for allowing contacts
            sharedAppDelegate?.isShowBlurView = false
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ){
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(status, error)
                    })
                }
                else{
                    self.getContacts(for: type, completion)
                }
            })
            
        case  .authorized:
            //Authorization granted by user for this app.
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            self.resetData()
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    
                    switch type {
                    case .phoneNumber:
                        if contact.phoneNumbers.count >= 1 {
                            var useContacts = [OAContactModel]()
                            
                            for ContctNumVar: CNLabeledValue in contact.phoneNumbers {
                                let FulMobNumVar  = ContctNumVar.value
                                // let MccNamVar = FulMobNumVar.value(forKey: "countryCode") as? String
                                let MobNumVar = FulMobNumVar.value(forKey: "digits") as? String
                                let useContact = OAContactModel(contact: contact, displayValue: MobNumVar)
                                useContacts.append(useContact)
                            }
                            self.allContact += useContacts
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            var contacts = [OAContactModel]()
                            if let segregatedContact = self.orderedContacts[key] {
                                contacts = segregatedContact
                            }
                            contacts += useContacts
                            self.orderedContacts[key] = contacts
                        }
                        
                    case .email:
                        if contact.emailAddresses.count >= 1 {
                            var useContacts = [OAContactModel]()
                            
                            for ContctNumVar: CNLabeledValue in contact.emailAddresses {
                                let emailIdVar  = ContctNumVar.value
                                let useContact = OAContactModel(contact: contact, displayValue: emailIdVar as String)
                                useContacts.append(useContact)
                            }
                            self.allContact += useContacts
                            
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            var contacts = [OAContactModel]()
                            if let segregatedContact = self.orderedContacts[key] {
                                contacts = segregatedContact
                            }
                            contacts += useContacts
                            self.orderedContacts[key] = contacts
                        }
                    }
                    
                    
                })
                self.sortedContactKeys = Array(self.orderedContacts.keys).sorted(by: <)
                if self.sortedContactKeys.first == "#" {
                    self.sortedContactKeys.removeFirst()
                    self.sortedContactKeys.append("#")
                }
                completion(status, nil)
            }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
        @unknown default: break
          //  fatalError()
        }
    }
    
    func filterContactList(_ searchText: String) {
        self.filteredContacts = self.allContact.filter{ $0.displayName().contains(searchText)}
        print("\(filteredContacts.count) count")
    }
    
    func resetData() {
        orderedContacts.removeAll()
        sortedContactKeys.removeAll()
        allContact.removeAll()
        filteredContacts.removeAll()
    }
    
}
