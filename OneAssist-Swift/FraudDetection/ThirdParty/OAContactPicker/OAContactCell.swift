//
//  OAContactCell.swift
//  OACOntactPicker
//
//  Created by Ankur Batham on 02/09/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import UIKit

class OAContactCell: UITableViewCell {

    @IBOutlet weak var contactTextLabel: UILabel!
    @IBOutlet weak var contactDetailTextLabel: UILabel!
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var contactInitialLabel: UILabel!
    @IBOutlet weak var contactImageContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contactImageContainerView.layer.masksToBounds = true
        contactImageContainerView.layer.cornerRadius = contactImageContainerView.frame.size.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
    func updateInitialsColorForIndexPath(_ indexpath: IndexPath) {
        let colorArray = [#colorLiteral(red: 0.6078431373, green: 0.3490196078, blue: 0.7137254902, alpha: 1),#colorLiteral(red: 0.4980392157, green: 0.5490196078, blue: 0.5529411765, alpha: 1),#colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1),#colorLiteral(red: 0.2039215686, green: 0.5960784314, blue: 0.8588235294, alpha: 1),#colorLiteral(red: 0.7529411765, green: 0.2235294118, blue: 0.168627451, alpha: 1),#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)]
        let randomValue = (indexpath.row + indexpath.section) % colorArray.count
        contactInitialLabel.backgroundColor = colorArray[randomValue]
    }
    
    func updateContactsinUI(_ contact: OAContactModel, indexPath: IndexPath) {
        self.contactTextLabel?.text = contact.displayName()
        if contact.thumbnailProfileImage != nil {
            self.contactImageView?.image = contact.thumbnailProfileImage
            self.contactImageView.isHidden = false
            self.contactInitialLabel.isHidden = true
        } else {
            self.contactInitialLabel.text = contact.contactInitials()
            updateInitialsColorForIndexPath(indexPath)
            self.contactImageView.isHidden = true
            self.contactInitialLabel.isHidden = false
        }
        
        self.contactDetailTextLabel.text = contact.displayValue
    }
    
}
