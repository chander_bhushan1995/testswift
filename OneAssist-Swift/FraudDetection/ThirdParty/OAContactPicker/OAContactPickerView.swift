//
//  OAContactPickerView.swift
//  OACOntactPicker
//
//  Created by Ankur Batham on 02/09/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import UIKit
import Contacts

protocol OAContactPickerDelegate: class {
    func oaContactPickerView(_: OAContactPickerView, didContactFetchFailed error: Error?)
    func oaContactPickerView(_: OAContactPickerView, didCancel error: Error?)
    func oaContactPickerView(_: OAContactPickerView, didSelectContact contact: OAContactModel)
}

extension OAContactPickerDelegate {
    func oaContactPickerView(_: OAContactPickerView, didContactFetchFailed error: Error?) { }
    func oaContactPickerView(_: OAContactPickerView, didCancel error: Error?) { }
    func oaContactPickerView(_: OAContactPickerView, didSelectContact contact: OAContactModel) { }
}


public enum SubtitleValueType{
    case phoneNumber
    case email
}


class OAContactPickerView: UITableViewController {
    // MARK: - Properties
    
    open weak var contactDelegate: OAContactPickerDelegate?
    var resultSearchController = UISearchController()
    var subtitleValueType = SubtitleValueType.phoneNumber
    var clObject = OAGetContactList()
    
    // MARK: - Initializers
    
    convenience public init(delegate: OAContactPickerDelegate?, subtitleValueType: SubtitleValueType) {
        self.init(style: .plain)
        contactDelegate = delegate
        self.subtitleValueType = subtitleValueType
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contacts"
        
        registerContactCell()
        inititlizeBarButtons()
        initializeSearchBar()
        reloadContacts()
    }
    
    fileprivate func registerContactCell() {
        tableView.register(UINib(nibName: "OAContactCell", bundle: nil), forCellReuseIdentifier: "OAContactCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        
    }
    
    fileprivate func inititlizeBarButtons() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(onTouchCancelButton))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    
    fileprivate func initializeSearchBar() {
        self.resultSearchController = ( {
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.hidesNavigationBarDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
    }

    // MARK: - Contact Operations
    
    open func reloadContacts() {
        clObject.getContacts(for: self.subtitleValueType) { [unowned self](status, error) in
            switch status {
            case .notDetermined, .denied, .restricted:
                let productName = "\(Bundle.main.infoDictionary!["CFBundleName"]!)" + Strings.Global.contactPermisionDeniedMessage
                
                self.showOpenSettingsAlert(title: Strings.Global.contactPermisionDenied, message: productName, {
                    self.dismiss(animated: true, completion: {
                        self.contactDelegate?.oaContactPickerView(self, didContactFetchFailed: error)
                    })
                })
    
            case .authorized:
                if (error == nil) {
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                        if self.clObject.allContact.count == 0 {
                            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
                            self.showAlertWithCrossAction(title: "\(productName)", message: Strings.Global.noContact, primaryButtonTitle: "OK", primaryAction: {
                                self.dismiss(animated: true, completion: {
                                self.contactDelegate?.oaContactPickerView(self, didContactFetchFailed: NSError(domain: "OAContactPickerErrorDomain", code: 4, userInfo: [ NSLocalizedDescriptionKey: "No Contact available"]))
                                })
                            }, {
                                self.dismiss(animated: true, completion: {
                                    self.contactDelegate?.oaContactPickerView(self, didContactFetchFailed: NSError(domain: "OAContactPickerErrorDomain", code: 4, userInfo: [ NSLocalizedDescriptionKey: "No Contact available"]))
                                })
                            })
                        }
                    })
                }
            @unknown default: break
               // fatalError()
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if resultSearchController.isActive { return 1 }
        return clObject.sortedContactKeys.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultSearchController.isActive { return clObject.filteredContacts.count }
        if let contactsForSection = clObject.orderedContacts[clObject.sortedContactKeys[section]] {
            return contactsForSection.count
        }
        return 0
    }
    
    // MARK: - Table View Delegates
    
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OAContactCell", for: indexPath) as! OAContactCell
        let contact = getContactModel(for: indexPath)
        guard let contactObject = contact else {
            return UITableViewCell()
        }
        cell.updateContactsinUI(contactObject, indexPath: indexPath)
        
        return cell
    }
    
    func getContactModel(for indexPath: IndexPath) -> OAContactModel? {
        let contact: OAContactModel?
        
        if resultSearchController.isActive {
            contact = clObject.filteredContacts[indexPath.row]
            
        } else {
            guard let contactsForSection = clObject.orderedContacts[clObject.sortedContactKeys[indexPath.section]] else {
                assertionFailure()
                return nil
            }
            
            contact = contactsForSection[indexPath.row]
        }
        return contact
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("selection")
        let contact = getContactModel(for: indexPath)
        guard let contactObject = contact else {
            return
        }
        resultSearchController.isActive = false
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
            self.contactDelegate?.oaContactPickerView(self, didSelectContact: contactObject)
        }
    }

    
    // MARK: - Button Actions
    
    @objc func onTouchCancelButton() {
        dismiss(animated: true, completion: {
            self.contactDelegate?.oaContactPickerView(self, didCancel: NSError(domain: "OAContactPickerErrorDomain", code: 2, userInfo: [ NSLocalizedDescriptionKey: "User Canceled Selection"]))
        })
    }

}


extension OAContactPickerView: UISearchResultsUpdating, UISearchBarDelegate {
    open func updateSearchResults(for searchController: UISearchController) {
        if let searchText = resultSearchController.searchBar.text , searchController.isActive {
            self.clObject.filterContactList(searchText)
            self.tableView.reloadData()
        }
    }
    
    open func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
    }
    
}
