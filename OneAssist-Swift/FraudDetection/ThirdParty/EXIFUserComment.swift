import Foundation
import ImageIO

#if os(iOS)
    import MobileCoreServices
#endif

// See CGImageMetadataCopyTagWithPath documentation for an explanation
let exifUserCommentPath = "\(kCGImageMetadataPrefixExif):\(kCGImagePropertyExifUserComment)" as CFString

/* Set EXIF UserComment */

// using metadata functions
func setEXIFUserComment(_ comment: String, using sourceURL: URL, destination destinationURL: URL) {

    guard let imageDestination = CGImageDestinationCreateWithURL(destinationURL as CFURL, kUTTypeJPEG, 1, nil)
        else { fatalError("Image destination not created") }

    guard let metadataTag = CGImageMetadataTagCreate(kCGImageMetadataNamespaceExif, kCGImageMetadataPrefixExif, kCGImagePropertyExifUserComment, .string, comment as CFString)
        else { fatalError("Metadata tag not created") }

    let metadata = CGImageMetadataCreateMutable()
    CGImageMetadataSetTagWithPath(metadata, nil, exifUserCommentPath, metadataTag)

    guard let imageSource = CGImageSourceCreateWithURL(sourceURL as CFURL, nil)
        else { fatalError("Image source not created") }

    guard let image = CGImageSourceCreateImageAtIndex(imageSource, 0, nil)
        else { fatalError("Image not created from source") }

    CGImageDestinationAddImageAndMetadata(imageDestination, image, metadata, nil)
    CGImageDestinationFinalize(imageDestination)
}

// using image property functions
func setEXIFUserComment2(_ comment: String, using sourceURL: URL, destination destinationURL: URL) {

    guard let outputImage = CGImageDestinationCreateWithURL(destinationURL as CFURL, kUTTypeJPEG, 1, nil)
        else { fatalError("Image destination not created") }

    guard let imageSource = CGImageSourceCreateWithURL(sourceURL as CFURL, nil)
        else { fatalError("Image source not created") }

    let exifDictionary: [NSString: AnyObject] = [ kCGImagePropertyExifUserComment: comment as CFString ]
    let properties: [NSString: AnyObject] = [ kCGImagePropertyExifDictionary: exifDictionary as CFDictionary ]

    CGImageDestinationAddImageFromSource(outputImage, imageSource, 0, properties as CFDictionary)
    CGImageDestinationFinalize(outputImage)
}

/* Get EXIF UserComment */

// using image metadata functions
func getEXIFUserComment(from image: URL) -> String? {

    guard let imageSource = CGImageSourceCreateWithURL(image as CFURL, nil)
        else { fatalError("Image source not created") }

    guard let metadata = CGImageSourceCopyMetadataAtIndex(imageSource, 0, nil)
        else { return nil }

    //    // Solution 1
    //
    ////    // Solution 1a
    ////    guard let userCommentTag = CGImageMetadataCopyTagWithPath(metadata, nil, exifUserCommentPath)
    ////        else { return nil }
    ////
    ////    // Solution 1b
    ////    guard let userCommentTag = CGImageMetadataCopyTagMatchingImageProperty(metadata, kCGImagePropertyExifDictionary, kCGImagePropertyExifUserComment)
    ////        else { return nil }
    //
    //    guard let userComment = CGImageMetadataTagCopyValue(userCommentTag) as? String
    //        else { return nil }

    // Solution 2
    guard let userComment = CGImageMetadataCopyStringValueWithPath(metadata, nil, exifUserCommentPath) as String?
        else { return nil }
    
    return userComment
}

// using image property functions
func getEXIFUserComment2(from image: URL) -> String? {

    guard let imageSource = CGImageSourceCreateWithURL(image as CFURL, nil)
        else { fatalError("Image source not created") }

    guard let properties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as? [NSString: AnyObject],
          let exifDictonary = properties[kCGImagePropertyExifDictionary],
          let userComment = exifDictonary[kCGImagePropertyExifUserComment] as? String
        else { return nil }

    return userComment
}

/* Debug */

func getProperties(from image: URL) -> [NSString: AnyObject]? {

    guard let source = CGImageSourceCreateWithURL(image as CFURL, nil)
        else { fatalError("Image source not created") }

    return CGImageSourceCopyPropertiesAtIndex(source, 0, nil) as? [NSString: AnyObject]
}

func getMetadata(from image: URL) -> [[String: Any]]? {

    guard let imageSource = CGImageSourceCreateWithURL(image as CFURL, nil)
        else { fatalError("Image source not created") }

    guard let metadata = CGImageSourceCopyMetadataAtIndex(imageSource, 0, nil)
        else { return nil }

    guard let tags = CGImageMetadataCopyTags(metadata) as? [CGImageMetadataTag]
        else { return nil }

    return tags.map { tag in

        var info = [String: Any]()

        if let namespace = CGImageMetadataTagCopyNamespace(tag) as String? {
            info["namespance"] = namespace
        }

        if let prefix = CGImageMetadataTagCopyPrefix(tag) as String? {
            info["prefix"] = prefix
        }

        if let name = CGImageMetadataTagCopyName(tag) as String? {
            info["name"] = name
        }

        if let value = CGImageMetadataTagCopyValue(tag) as AnyObject? {
            info["value"] = value
        }

        info["type"] =  CGImageMetadataTagGetType(tag).rawValue

        if let qualifiers = CGImageMetadataTagCopyQualifiers(tag) as? [CGImageMetadataTag] {
            info["qualifiers"] = qualifiers
        }
        
        return info
    }
}
