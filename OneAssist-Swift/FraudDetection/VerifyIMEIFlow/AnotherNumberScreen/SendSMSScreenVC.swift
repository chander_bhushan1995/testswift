//
//  SendSMSScreenVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 05/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol SendSMSScreenBusinessLogic {
    func sendSMS(_ mobileNumber: String?, orderId: String)
}

class SendSMSScreenVC: BaseVC {
    @IBOutlet weak var imeiDetailView: UIView!
    @IBOutlet weak var imeiTitleLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var imeiLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var imeiSecondLabel: SupportingTextRegularGreyLabel!
    
    @IBOutlet weak var stepLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var stepLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headingLabel: H3BoldLabel!
    @IBOutlet weak var headingTopConstraintToIMEIView: NSLayoutConstraint!
    @IBOutlet weak var subHeadingLabel: H3RegularBlackLabel!
    @IBOutlet weak var subheadingTopConstraintToStepLabel: NSLayoutConstraint!
    @IBOutlet weak var numberdescriptionLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var smsTextFieldView: TextFieldView!
    
    @IBOutlet var imeiBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var continueButton: OAPrimaryButton!
    
    var interactor: SendSMSScreenBusinessLogic?
    
    var imeiNumber: String?
    var imeiNumber2: String?
    var mobileNo: String?
    
    var countLabels = [UIView]()

    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }

    private func initializeView() {
        stepLabel.textColor = UIColor.bodyTextGray
        imeiTitleLabel.textColor = UIColor.white
        imeiLabel.textColor = UIColor.white
        imeiSecondLabel.textColor = UIColor.white
        smsTextFieldView.descriptionText = Strings.iOSFraudDetection.anotherNumber
        smsTextFieldView.fieldType = MobileFieldType.self
        smsTextFieldView.isSetRightImageAction = true
        smsTextFieldView.rightImage = UIImage(named: "phone-book")
        smsTextFieldView.delegate = self
        continueButton.setTitle("Send SMS", for: .normal)
        countLabels.append(addBarButtons())
        
        if let imei = imeiNumber {
           showImeiStage(imei)
            
        }else {
            showStepStage()
        }
        
        if let mobileNo = mobileNo {
            self.smsTextFieldView.fieldText = mobileNo
        }
    }
    
    private func showImeiStage(_ imei: String) { //it will display the imei in green view
        self.title = Strings.iOSFraudDetection.verifyImeiTitle
        let deviceName = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.deviceModel ?? UIDevice.currentDeviceModel
        let imeiTitle = Strings.iOSFraudDetection.awsomeText + deviceName + Strings.iOSFraudDetection.IMEInumberText
        self.imeiTitleLabel.text = imeiTitle
        var imeinumber = ""
        if let imeinumber2 = imeiNumber2 {
            imeinumber = "IMEI1: " + imei
            
            self.imeiSecondLabel.isHidden = false
            self.imeiBottomConstraint.priority = .defaultLow
            self.imeiSecondLabel.text = "IMEI2: " + imeinumber2
        } else {
            imeinumber = "IMEI: " + imei
            self.imeiSecondLabel.isHidden = true
            self.imeiBottomConstraint.priority = .defaultHigh
        }
        
        self.imeiLabel.text = imeinumber
        self.stepLabel.text = nil
        self.headingTopConstraintToIMEIView.isActive = true
        self.numberdescriptionLabel.text = nil
    }
    
    
    private func showStepStage() {
        self.title = Strings.iOSFraudDetection.sendInspectionLink
        self.imeiTitleLabel.text = nil
        self.imeiLabel.text = nil
        self.imeiDetailView.isHidden = true
        self.stepLabelTopConstraint.priority = UILayoutPriority(rawValue: 1000)
        self.stepLabelTopConstraint.isActive = true
        self.stepLabel.text = "STEP 3 of 4"
        self.headingLabel.text = nil
        self.subheadingTopConstraintToStepLabel.isActive = true
        let deviceName = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.deviceModel ?? UIDevice.currentDeviceModel
        self.numberdescriptionLabel.text = Strings.iOSFraudDetection.smsHintText + deviceName
    }
    
    private func addBarButtons() -> UIView {
        let chatNotificationView = Utilities.getBadgeView1(target: self,image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: #selector(self.chatClicked(_:)))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView)]
        return chatNotificationView
    }
    
    @objc func chatClicked(_ sender: Any) {
        present(ChatVC(), animated: true, completion: nil)
    }

    //MARK: - UIButton Action
    @IBAction func onClickSendSMSAction(_ sender: UIButton?){
        do {
            try validateForm()
            
             Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
            EventTracking.shared.eventTracking(name: .sendUploadLink ,[.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])

            interactor?.sendSMS(smsTextFieldView.fieldText, orderId: FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? "")
            
        } catch let exception as ValidationError {
            
            switch exception {
            case .invalidMobile(let error):
                smsTextFieldView.setError(error)
            default:
                break
            }
            
        } catch {}
    }
    
    private func validateForm() throws {
        try Validation.validateSecondaryMobileNumber(text: smsTextFieldView.fieldText, existingNumber: CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber ?? "")
    }
    
}

extension SendSMSScreenVC: TextFieldViewDelegate {
    func textViewRightBtnClicked(_ textFieldView: TextFieldView) {
        routeToContactPicker()
    }
}
 
 // MARK: - Navigation
extension SendSMSScreenVC {
    func routeToContactPicker() {
        let contactPickerScene = OAContactPickerView(delegate: self, subtitleValueType: .phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        self.presentInFullScreen(navigationController, animated: true, completion: nil)
    }
    
    func routeToSMSSuccessScreen() {
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
        
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            SaveFDDataMode.savedata(saveObject.imei, saveObject.imei2, orderId, smsTextFieldView.fieldText, saveObject.mtFrontPanelName, saveObject.mtBackPanelName, saveObject.mtFrontPanelRetryCount, saveObject.mtServerHashCode)
        } else {
            SaveFDDataMode.savedata(imeiNumber, imeiNumber2, orderId, smsTextFieldView.fieldText, nil, nil, 0)
        }

        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        if let rootVC = self.popToRootView() as? TabVC {
            rootVC.routeToSendSMSSuccessScreen(mobileNumber: smsTextFieldView.fieldText)
        }
    }
}

extension SendSMSScreenVC: OAContactPickerDelegate {
    
    func oaContactPickerView(_: OAContactPickerView, didContactFetchFailed error: Error?) {
        print("Failed with error \(error.debugDescription)")
    }
    
    func oaContactPickerView(_: OAContactPickerView, didCancel error: Error?) {
        print("User canceled the selection");
        
    }
    
    func oaContactPickerView(_: OAContactPickerView, didSelectContact contact: OAContactModel) {
        self.smsTextFieldView.textFieldObj.text = String((contact.displayValue?.suffix(10))!)
        smsTextFieldView.animateTextField(state: .normal)
    }
}


// MARK:- Display Logic Conformance
extension SendSMSScreenVC: SendSMSPresenterDisplayLogic {
    func displaySendSMSSuccess(_ response: SendSMSResponseDTO) {
        Utilities.removeLoader(count: &loaderCount)
        routeToSMSSuccessScreen()
    }
    
    func displaySendSMSFailure(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        showAlert(message: error)
    }
}

// MARK:- Configuration Logic
extension SendSMSScreenVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = SendSMSInteractor()
        let presenter = SendSMSPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
