//
//  SendSMSPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 05/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol SendSMSPresenterDisplayLogic: class {
    func displaySendSMSSuccess(_ response: SendSMSResponseDTO)
    func displaySendSMSFailure(_ error: String)
}

class SendSMSPresenter: BasePresenter {
    weak var viewController: SendSMSPresenterDisplayLogic?
    
}

extension SendSMSPresenter: SendSMSInteractorPresenterLogic {
    func sendSMSResponse(_ response: SendSMSResponseDTO?, error: Error?){
        if let error = error {
            viewController?.displaySendSMSFailure(error.localizedDescription)
        } else if response?.status == Constants.ResponseConstants.success {
            viewController?.displaySendSMSSuccess(response!)
        } else {
            viewController?.displaySendSMSFailure("Something went wrong")
        }
    }
}
