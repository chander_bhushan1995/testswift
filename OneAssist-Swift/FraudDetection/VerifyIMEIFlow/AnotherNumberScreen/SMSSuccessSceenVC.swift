//
//  SMSSuccessSceenVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 06/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class SMSSuccessSceenVC: BaseVC {
    @IBOutlet weak var mobileNumberLabel: H3BoldLabel!
    
    @IBOutlet weak var openImageLinkLabel: H3BoldLabel!
    @IBOutlet weak var uploadLinkDescLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var actionBtn: OAPrimaryButton!
    
    var mobileNumber: String?
    
    var isReuploadOfFrontPanel = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }
    
    private func initializeView() {
        let smsText =  Strings.iOSFraudDetection.linkSentSuccess;
        if let mobile = self.mobileNumber {
            self.mobileNumberLabel.text = smsText + "to " + mobile + " via SMS."
        }else {
            self.mobileNumberLabel.text = smsText
        }
        
        if let task = FDCacheManager.shared.tempCustInfoModel?.task?.first {
            if task.status == Strings.FDetectionConstants.strReupload, let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
                ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
                isReuploadOfFrontPanel = Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs)
            }
        }
        
        let deviceName = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.deviceModel ?? UIDevice.currentDeviceModel
        if !isReuploadOfFrontPanel {
            openImageLinkLabel.text = String(format: Strings.Global.openImageLinkBackPanel, deviceName)
            uploadLinkDescLabel.text = Strings.Global.uploadDescTextBackPanel
            actionBtn.setTitle(MhcStrings.Buttons.gotIt, for: .normal)
        } else {
            openImageLinkLabel.text = Strings.Global.openImageLink
            actionBtn.setTitle(Strings.Global.openIMEIScreen, for: .normal)
            uploadLinkDescLabel.text = Strings.Global.uploadDescText//String(format: Strings.Global.uploadDescText, deviceName)
        }
        
    }

   
    //Mark: - UIButton Action
    @IBAction func onClickOpenIMEIAction(_ sender: UIButton?){
        if !isReuploadOfFrontPanel {
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            self.dismiss(animated: true, completion: nil)
        } else {
            EventTracking.shared.eventTracking(name: .viewVerificationScreen ,[.location: "Send Link Success", .mode: FDCacheManager.shared.fdTestType.rawValue])
            
            self.dismiss(animated: false) {[unowned self] in
                if let rootVC = self.popToRootView() as? TabVC {
                    rootVC.routeToIMEIScreen()
                }
            }
        }
    }
    
    @IBAction func onClickCloseAction(_ sender: UIButton?){
        self.dismiss(animated: true, completion: nil)
    }
}
