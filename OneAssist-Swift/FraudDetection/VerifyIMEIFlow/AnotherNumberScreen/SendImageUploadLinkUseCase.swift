//
//  SendImageUploadLinkUseCase.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//


import Foundation


class SendSMSRequestDTO: BaseRequestDTO {
    var orderId: String
    var verificationNo: String?
    
    init(orderId: String, verificationNo: String?) {
        self.orderId = orderId
        self.verificationNo = verificationNo
    }
    
    override func createRequestParameters() -> [String : Any]? {
        var parameters = super.createRequestParameters()
        parameters?[Constants.RequestKeys.orderId] = orderId
        if let number = self.verificationNo {
            parameters?[Constants.RequestKeys.verificationNo] = number
        }
        return parameters
    }
}

class SendSMSResponseDTO: BaseResponseDTO {
    
}

class SendImageUploadLinkUseCase : BaseRequestUseCase<SendSMSRequestDTO, SendSMSResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: SendSMSRequestDTO?, completionHandler: @escaping (SendSMSResponseDTO?, Error?) -> Void) {
        responseDataProvider.sendImageUploadLink(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

