//
//  SendSMSInteractor.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 05/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol SendSMSInteractorPresenterLogic {
    func sendSMSResponse(_ response: SendSMSResponseDTO?, error: Error?)
}

class SendSMSInteractor: BaseInteractor {
    var presenter: SendSMSInteractorPresenterLogic?
}

extension SendSMSInteractor: SendSMSScreenBusinessLogic {
    func sendSMS(_ mobileNumber: String?, orderId: String) {
        let obj:SendSMSRequestDTO = SendSMSRequestDTO(orderId: orderId, verificationNo: mobileNumber)
        
        let _ = SendImageUploadLinkUseCase.service(requestDTO: obj) { [weak self] (service,response,error) in
            self?.presenter?.sendSMSResponse(response,error:error)
        }
    }
}
