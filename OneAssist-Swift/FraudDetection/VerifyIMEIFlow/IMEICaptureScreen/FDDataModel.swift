//
//  FDDataModel.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 15/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation


class FDDataModel: NSObject, NSCoding {
    var imei : String?
    var imei2: String?
    let orderId : String
    var smsNumber : String?
    var mtFrontPanelName: String?
    var mtBackPanelName: String?
    var mtFrontPanelRetryCount: Int?
    var mtServerHashCode: String?
    
    enum Key:String {
        case imei = "imei"
        case imei2 = "imei2"
        case orderId = "orderId"
        case smsNumber = "smsNumber"
        case mtFrontPanelName = "mtFrontPanelName"
        case mtBackPanelName = "mtBackPanelName"
        case mtFrontPanelRetryCount = "mtFrontPanelRetryCount"
        case mtServerHashCode = "mtServerHashCode"
    }
    
    init(_ id: String) {
        self.orderId = id
    }
    
    init(id:String, imei:String?, imei2: String?, smsNumber:String?, mtFrontPanelName: String?, mtBackPanelName: String?, mtFrontPanelRetryCount: Int?, mtServerHashCode: String? = nil) {
        self.orderId = id
        self.imei = imei
        self.imei2 = imei2
        self.smsNumber = smsNumber
        self.mtFrontPanelName = mtFrontPanelName
        self.mtBackPanelName = mtBackPanelName
        self.mtFrontPanelRetryCount = mtFrontPanelRetryCount
        self.mtServerHashCode = mtServerHashCode
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(imei, forKey: Key.imei.rawValue)
        aCoder.encode(imei2, forKey: Key.imei2.rawValue)
        aCoder.encode(orderId, forKey: Key.orderId.rawValue)
        aCoder.encode(smsNumber, forKey: Key.smsNumber.rawValue)
        aCoder.encode(mtFrontPanelName, forKey: Key.mtFrontPanelName.rawValue)
        aCoder.encode(mtBackPanelName, forKey: Key.mtBackPanelName.rawValue)
        aCoder.encode(mtFrontPanelRetryCount, forKey: Key.mtFrontPanelRetryCount.rawValue)
        aCoder.encode(mtServerHashCode, forKey: Key.mtServerHashCode.rawValue)
    }
    
    convenience required init?(coder aDecoder: NSCoder) {
        let imei = aDecoder.decodeObject(forKey: Key.imei.rawValue) as? String
        let imei2 = aDecoder.decodeObject(forKey: Key.imei2.rawValue) as? String
        let smsNumber = aDecoder.decodeObject(forKey: Key.smsNumber.rawValue) as? String
        let mtFrontPanelName = aDecoder.decodeObject(forKey: Key.mtFrontPanelName.rawValue) as? String
        let mtBackPanelName = aDecoder.decodeObject(forKey: Key.mtBackPanelName.rawValue) as? String
        let mtServerHashCode = aDecoder.decodeObject(forKey: Key.mtServerHashCode.rawValue) as? String
        let mtFrontPanelRetryCount = aDecoder.decodeObject(forKey: Key.mtFrontPanelRetryCount.rawValue) as? Int
        guard let orderId = aDecoder.decodeObject(forKey: Key.orderId.rawValue) as? String else { return nil }
        self.init(id: orderId, imei: imei, imei2: imei2, smsNumber: smsNumber, mtFrontPanelName: mtFrontPanelName, mtBackPanelName: mtBackPanelName, mtFrontPanelRetryCount: mtFrontPanelRetryCount, mtServerHashCode: mtServerHashCode)
    }
}


typealias SaveFDDataMode = FDDataModel

extension SaveFDDataMode {
    
    class func savedata(_ imei: String?, _ imei2: String?, _ orderId: String?, _ smsNumber: String?, _ mtFrontPanelName: String?, _ mtBackPanelName: String?, _ mtFrontPanelRetryCount: Int?, _ mtServerHashCode: String? = nil) {
        if let orderid = orderId {
            let key = "\(UIDevice.uuid)" + "_" + orderid
            if let saveValue = SaveFDDataMode.getSaveData(orderId) {
                if let iMEI = imei {
                    saveValue.imei = iMEI
                }
                
                if let iMEI2 = imei2 {
                    saveValue.imei2 = iMEI2
                }
                
                if let number = smsNumber {
                    saveValue.smsNumber = number
                }
                saveValue.mtFrontPanelName = mtFrontPanelName
                saveValue.mtBackPanelName = mtBackPanelName
                saveValue.mtFrontPanelRetryCount = mtFrontPanelRetryCount
                saveValue.mtServerHashCode = mtServerHashCode
                SaveSecureData.setSecureValue(saveValue, forKey: key)
                
            }else {
                
                let saveValue = FDDataModel(orderid)
                if let iMEI = imei {
                    saveValue.imei = iMEI
                }
                if let iMEI2 = imei2 {
                    saveValue.imei2 = iMEI2
                }
                if let number = smsNumber {
                    saveValue.smsNumber = number
                }
                saveValue.mtFrontPanelName = mtFrontPanelName
                saveValue.mtBackPanelName = mtBackPanelName
                saveValue.mtFrontPanelRetryCount = mtFrontPanelRetryCount
                saveValue.mtServerHashCode = mtServerHashCode
                SaveSecureData.setSecureValue(saveValue, forKey: key)
            }
        }
        
    }
    
    class func getSaveData(_ orderId: String?) -> SaveFDDataMode? {
        if let orderid = orderId {
            let key = "\(UIDevice.uuid)" + "_" + orderid
            if let getValue = SaveSecureData.secureValue(forKey: key) as? SaveFDDataMode {
                return getValue
            }
            return nil
        }
        return nil
    }
    
    class func getSMSNumber(_ orderId: String?) -> String? {
        if let orderid = orderId {
            let key = "\(UIDevice.uuid)" + "_" + orderid
            if let getValue = SaveSecureData.secureValue(forKey: key) as? SaveFDDataMode {
                return getValue.smsNumber
            }
            return nil
        }
        return nil
    }
    
    class func getMTFrontPanelRetryCount(_ orderId: String?) -> Int? {
        if let orderid = orderId {
            let key = "\(UIDevice.uuid)" + "_" + orderid
            if let getValue = SaveSecureData.secureValue(forKey: key) as? SaveFDDataMode {
                return getValue.mtFrontPanelRetryCount
            }
            return nil
        }
        return nil
    }
    
    class func getMTServerHashCode(_ orderId: String?) -> String? {
        if let orderid = orderId {
            let key = "\(UIDevice.uuid)" + "_" + orderid
            if let getValue = SaveSecureData.secureValue(forKey: key) as? SaveFDDataMode {
                return getValue.mtServerHashCode
            }
            return nil
        }
        return nil
    }
}
