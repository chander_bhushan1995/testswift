//
//  IMEIVerificationPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 05/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIVerificationDisplayLogic: class {
    func imeiNumber(_ imeis: [String], from snapShot: UIImage?)
    func fetchIMEILoader()
    func displayIMEIErrorAlert()
    
    func successfullyUploadedDetails(with imei: String, and imei2: String?)
    func uploadDetailsFailure(_ error: String)
    
    func displayUploadedImageSuccess(with name: String, data: String?)
    func displayUploadedFailure(with name: String, error: String?)
}

class IMEIVerificationPresenter: BasePresenter {
    weak var viewController: IMEIVerificationDisplayLogic?
    private var screenshotCapture: ScreenshotCapture?
    private lazy var testRecognizer = ImageTextAndBarCodeRecognizer()

    var tempCustInfoModel: TemporaryCustomerInfoModel?
    
    private var snapshot: UIImage?
    
    override init() {
        super.init()
        screenshotCapture = ScreenshotCapture(delegate: self)
    }
}

extension IMEIVerificationPresenter: IMEIVerificationPresenterLogic {
    func removeScreenShotObserver() {
        screenshotCapture?.unregisterApplicationStateObserver()
    }
    
    func takeScreenShot(with tempCustInfoModel: TemporaryCustomerInfoModel?) {
        self.tempCustInfoModel = tempCustInfoModel
        screenshotCapture?.requestPhotosAuthorization(andThen: {[unowned self] in
            self.callFeature()
            self.screenshotCapture?.registerScreenShotObserver()
            self.screenshotCapture?.registerApplicationStateObserver()
        })
    }
    
    func callFeature() {
//        ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: false, handler: {[weak self] (image, error) in
//            self?.snapshot = image;
//            self?.viewController?.fetchIMEILoader()
//            self?.startTextDetection()
//        })

        DispatchQueue.main.async {
            let ussdCode = "tel://*#06#"
            let app = UIApplication.shared
            let csCopy = CharacterSet(bitmapRepresentation: CharacterSet.urlPathAllowed.bitmapRepresentation)
            
            if let encoded = ussdCode.addingPercentEncoding(withAllowedCharacters: csCopy) {
                let u = encoded//"tel://\(encoded)"
                if let url = URL(string:u) {
                    if app.canOpenURL(url) {
                        sharedAppDelegate?.isShowBlurView = false
                        app.open(url, options: [:], completionHandler: { (finished) in
                            if finished{ }
                        })
                    }
                }
            }
        }
    }
    
    
    func startTextDetection() {
        print("\(#function)")
        testRecognizer.tempCustInfoModel = tempCustInfoModel
        testRecognizer.getTextFromImage(image: self.snapshot, readTextType: .IMEI_READ) { [unowned self](recognizTexts, snapshot, errorMessage) in
            DispatchQueue.main.async {[unowned self] in
                if let screenTexts = recognizTexts {
                    self.viewController?.imeiNumber(screenTexts, from: snapshot)
                }else {
                    self.viewController?.displayIMEIErrorAlert()
                }
            }
        }
    }
    
    func recievedUpdateDetailsResponse(with imei: String, and imei2: String?, response: UpdatePostPaymentDetailsResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.successfullyUploadedDetails(with: imei, and: imei2)
        } catch {
            viewController?.uploadDetailsFailure(error.localizedDescription)
        }
    }
    
    func uploadResponseReceived(for invoiceName: String, response: UploadInvoiceFDResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUploadedImageSuccess(with: invoiceName, data: response?.data)
        } catch {
            viewController?.displayUploadedFailure(with: invoiceName, error: error.localizedDescription)
        }
    }
    
    func validateBarCodeSupportValue(with resposne: FDbarcodeScannerResponseDTO?) {
        var isBarCodeScannerSupport = false
        var isBarCodeScannerValidate = false
        var isValueSaveToFireStore = false
        
        let osVersion = UIDevice.deviceVersion
        let osVersionArray = osVersion.components(separatedBy: ".")
        
        if let responseData = resposne {
            if osVersionArray.count > 0 {
                isValueSaveToFireStore = responseData.savetoFirestore ?? isValueSaveToFireStore
                isBarCodeScannerSupport = responseData.enableScannerOS?.contains(osVersionArray.first ?? "") ?? isBarCodeScannerSupport
                isBarCodeScannerValidate = responseData.barCodeValidationOS?.contains(osVersionArray.first ?? "") ?? isBarCodeScannerValidate
            }
        }
        
        testRecognizer.isBarCodeScannerSupport = isBarCodeScannerSupport
        testRecognizer.isBarCodeScannerValidate = isBarCodeScannerValidate
        testRecognizer.isValueSaveToFireStore = isValueSaveToFireStore
    }
}


extension IMEIVerificationPresenter: ScreenshotCaptureDelegate {
        func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didDetect screenshot: UIImage) {
            print("Function: \(#function), line: \(#line)")
            self.snapshot = screenshot;
            viewController?.fetchIMEILoader()
            self.startTextDetection()
            print(screenshot)
        }
        
        func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didFailWith error: ScreenshotCapture.Error) {
            print("Function: \(#function), line: \(#line)")
            print(error)
            viewController?.displayIMEIErrorAlert()
        }
}
