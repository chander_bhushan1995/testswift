//
//  IMEIVerificationVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIVerificationBusinessLogic {
    func getIMEINumder(with tempCustInfoModel: TemporaryCustomerInfoModel?)
    func removeObserver()
    
    func submitIMEI(with pincode: String, address: String, orderId: String, deviceModel: String,
                    imei: String, imei2: String?)
    
    func uploadFDDocument(for invoiceName: String, activityRefId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?)
    
    func loadFDBarcodeSupportData();
}

typealias IMEICallback = (String) -> ()

class IMEIVerificationVC: BaseVC {
    @IBOutlet weak var stepsLable: UILabel!
    @IBOutlet weak var verifyIMEIButton: OAPrimaryButton!
    
    var interactor: IMEIVerificationBusinessLogic?
    var isFromBuyback: Bool = false
    var imeiCallback: IMEICallback?
    
    var countLabels = [UIView]()
    
    var completionHandler: (() -> ())?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }
    
    private func initializeView() {
        self.title = Strings.iOSFraudDetection.verifyImeiTitle
        if isFromBuyback {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style: .plain, target: self, action: #selector(dismissVC))
        }
        
        countLabels.append(addBarButtons())
        setupStepsText()
        interactor?.loadFDBarcodeSupportData()
    }
    
    @objc func dismissVC() {
        dismissVC(withIMEI: "")
       }
    
    func dismissVC(withIMEI imei: String = "") {
        if isFromBuyback {
            self.navigationItem.leftBarButtonItem = nil
            imeiCallback?(imei)
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func setupStepsText() {
        let bullet = "\u{2022}\t"
        verifyIMEIButton.setTitle("Verify IMEI", for: .normal)
        let paragraphStyle = NSMutableParagraphStyle()
        let nonOptions = [NSTextTab.OptionKey: Any]()
        paragraphStyle.tabStops = [
            NSTextTab(textAlignment: .left, location: 16, options: nonOptions)]
        paragraphStyle.defaultTabInterval = 16
        paragraphStyle.lineSpacing = 14
        paragraphStyle.paragraphSpacing = 12
        
        let textAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.charcoalGrey, NSAttributedString.Key.paragraphStyle : paragraphStyle]
        let textBoldAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: DLSFont.bodyText.bold, NSAttributedString.Key.foregroundColor: UIColor.charcoalGrey, NSAttributedString.Key.paragraphStyle : paragraphStyle]
        let bulletAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: DLSFont.bodyText.bold, NSAttributedString.Key.foregroundColor: UIColor.charcoalGrey, NSAttributedString.Key.paragraphStyle : paragraphStyle]
        
        let bulletList = NSMutableAttributedString()
        
        func bulletPoints(_ firstString: String?=nil, _ boldString: String?=nil, _ secondString: String?=nil) {
            let bulletAttribute = NSMutableAttributedString(string: bullet, attributes: bulletAttributes)
            if let first = firstString {
                let firstString = NSMutableAttributedString(string: first, attributes: textAttributes)
                bulletAttribute.append(firstString)
            }
            if let bold = boldString {
                let  boldString = NSMutableAttributedString(string: bold, attributes: textBoldAttributes)
                bulletAttribute.append(boldString)
            }
            if let second = secondString {
                let  secondString = NSMutableAttributedString(string: second, attributes: textAttributes)
                bulletAttribute.append(secondString)
            }
            bulletList.append(bulletAttribute)
        }
        
        bulletPoints(Strings.iOSFraudDetection.IMEISteps.Step1.firstText,
                     Strings.iOSFraudDetection.IMEISteps.Step1.boldText,
                     Strings.iOSFraudDetection.IMEISteps.Step1.lastText)
        
        bulletPoints(Strings.iOSFraudDetection.IMEISteps.Step2.firstText,
                     Strings.iOSFraudDetection.IMEISteps.Step2.boldText,
                     Strings.iOSFraudDetection.IMEISteps.Step2.lastText)
        
        bulletPoints(Strings.iOSFraudDetection.IMEISteps.Step3.firstText,
                     Strings.iOSFraudDetection.IMEISteps.Step3.boldText)
        
        if #available(iOS 13.0, *) {
            bulletPoints(Strings.iOSFraudDetection.IMEISteps.Step4_13.firstText,
                         Strings.iOSFraudDetection.IMEISteps.Step4_13.boldText,
                         Strings.iOSFraudDetection.IMEISteps.Step4_13.lastText)
        }else {
            bulletPoints(Strings.iOSFraudDetection.IMEISteps.Step4.firstText,
                         Strings.iOSFraudDetection.IMEISteps.Step4.boldText,
                         Strings.iOSFraudDetection.IMEISteps.Step4.lastText)
        }
        
        
        
        self.stepsLable.attributedText = bulletList
    }
    
    private func addBarButtons() -> UIView {
        let chatNotificationView = Utilities.getBadgeView1(target: self,image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: #selector(self.chatClicked(_:)))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView)]
        return chatNotificationView
    }
    
    @objc func chatClicked(_ sender: Any) {
        present(ChatVC(), animated: true, completion: nil)
    }
    
    fileprivate func callUploadFraudDetectionDocAPI(with fileName: String, _ image: UIImage?) {
        
        guard let image = image else {
            return
        }
        if RemoteConfigManager.shared.isIMEIScreenShotSend {
            let name = "IMEI_SCREENSHOT"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)  {[weak self] in
                let uploadedFile = image.resizeToMB(5)
                let pathExtention = (fileName as NSString).pathExtension
                self?.interactor?.uploadFDDocument(for: name, activityRefId: FDCacheManager.shared.getDocsToUploadModel?.activityReferenceId, name: name, fileName: fileName, docFileContentType: "image/.\(pathExtention.lowercased())", displayName: "IMEI Screen shot", docMandatory: "O", uploadedFile: uploadedFile)
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func onClickVerifyIMEIAction(_ sender: UIButton?) {
        sender?.isUserInteractionEnabled = false
        EventTracking.shared.eventTracking(name: .verifyIMEI, [.mode: FDCacheManager.shared.fdTestType.rawValue])
        
        #if DEBUG
        interactor?.getIMEINumder(with: FDCacheManager.shared.tempCustInfoModel)
        #else
        if UIDevice.isDeviceCharging {
            self.showAlert(message: Strings.iOSFraudDetection.unPlugCharger)
        } else {
            interactor?.getIMEINumder(with: FDCacheManager.shared.tempCustInfoModel)
        }
        #endif
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (timer) in
            sender?.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func onClickTutorialAction(_ sender: UIButton?) {
        EventTracking.shared.eventTracking(name: .seeTutorial, [.mode: FDCacheManager.shared.fdTestType.rawValue])
        self.routeToTutorial()
    }
}

// MARK: - Navigation
extension IMEIVerificationVC {
    func routeToTutorial() {
        let vc = IMEIVerificationTutorialVC()
        presentOverFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToSMSScreen(_ imei: String?, and imei2: String?) {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        if let rootVC = self.popToRootView() as? TabVC {
            rootVC.routeToSendSMSScreen(imeinumber: imei, imeinumber2: imei2)
        }
    }
    
    
    func routeToSetBesicDetailScreen() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        if let completionBlock = self.completionHandler {
            completionBlock()
            
        }else {
            if let rootVC = self.popToRootView() as? TabVC {
                rootVC.routeToSetBasicDetailScreen()
            }
        }
    }
    
    func routeToMirrorTestVideoGuide() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        if let rootVC = self.popToRootView() as? TabVC {
            rootVC.routeToMirrorTestVideoGuide()
        }
    }
    
}

// MARK:- Display Logic Conformance
extension IMEIVerificationVC: IMEIVerificationDisplayLogic {
    func fetchIMEILoader() {
        DispatchQueue.main.async {[unowned self] in
            Utilities.addLoader(count: &self.loaderCount, isInteractionEnabled: false)
        }
    }
    
    func displayIMEIErrorAlert() {
        DispatchQueue.main.async {[unowned self] in
            Utilities.removeLoader(count: &self.loaderCount)
            if self.isFromBuyback {
                self.dismissVC(withIMEI: "")
            } else {
                self.showAlert(title: Strings.iOSFraudDetection.screenShotCaptureTitle, message: Strings.iOSFraudDetection.screenShotCaptureSubTitle, primaryButtonTitle: Strings.Global.gotIt, nil, primaryAction: {
                    EventTracking.shared.eventTracking(name: .imeiNotCaptured ,[.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
                }, nil)
            }
        }
    }
    
    func successfullyUploadedDetails(with imei: String, and imei2: String?) {
        DispatchQueue.main.async {[unowned self] in
            self.interactor?.removeObserver()
            Utilities.removeLoader(count: &self.loaderCount)
            let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
            if let saveObject = SaveFDDataMode.getSaveData(orderId) {
                SaveFDDataMode.savedata(imei, imei2, orderId, saveObject.smsNumber, saveObject.mtFrontPanelName, saveObject.mtBackPanelName, saveObject.mtFrontPanelRetryCount, saveObject.mtServerHashCode)
            }else {
                SaveFDDataMode.savedata(imei, imei2, orderId, nil, nil, nil, 0)
            }
            
            if FDCacheManager.shared.fdTestType == .MT {
                self.routeToMirrorTestVideoGuide()
                
            }else {
               self.routeToSMSScreen(imei, and: imei2)
                
            }
            
        }
    }
    
    func uploadDetailsFailure(_ error: String) {
        DispatchQueue.main.async {[unowned self] in
            Utilities.removeLoader(count: &self.loaderCount)
            self.showAlert(message: error)
        }
        
    }
    
    func imeiNumber(_ imeis: [String], from snapShot: UIImage?) {
        print("\(#function)")
        print("IMEI ==== \(imeis)")
        self.interactor?.removeObserver()
       // print("SNAPSHOT ==== \(snapShot)")
        
        if isFromBuyback {
            Utilities.removeLoader(count: &self.loaderCount)
            self.dismissVC(withIMEI: imeis.first ?? "")
        } else {
            self.callUploadFraudDetectionDocAPI(with: "imei.png", snapShot)
            
            let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
            
            //strPostDtlPending
            if FDCacheManager.shared.fdTestType == .MT,
                let task = FDCacheManager.shared.tempCustInfoModel?.task?.first, task.status ==  Strings.PEScene.FraudDetection.Status.strPostDtlPending {
                Utilities.removeLoader(count: &self.loaderCount)
                if let saveObject = SaveFDDataMode.getSaveData(orderId) {
                    SaveFDDataMode.savedata(imeis[0], (imeis.count == 2 ? imeis[1]: nil), orderId, saveObject.smsNumber, saveObject.mtFrontPanelName, saveObject.mtBackPanelName, saveObject.mtFrontPanelRetryCount, saveObject.mtServerHashCode)
                }else {
                    SaveFDDataMode.savedata(imeis[0], (imeis.count == 2 ? imeis[1]: nil), orderId, nil, nil, nil, 0)
                }
                
                routeToSetBesicDetailScreen()
                
            }else {
                var pincode = "", address = "", orderId = "", deviceModel = ""
                if let customerDetail = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first {
                    pincode = customerDetail.pinCode ?? ""
                    address = customerDetail.addrLine1 ?? ""
                    orderId = customerDetail.orderId?.stringValue ?? ""
                    deviceModel = customerDetail.deviceModel ?? UIDevice.currentDeviceModel
                }
                
                interactor?.submitIMEI(with: pincode, address: address, orderId: orderId, deviceModel: deviceModel, imei: imeis[0], imei2: (imeis.count == 2 ? imeis[1]: nil))
            }
        }
    }
    
    func displayUploadedImageSuccess(with name: String, data: String?) {
        
    }
    
    func displayUploadedFailure(with name: String, error: String?) {
        
       
    }
}

// MARK:- Configuration Logic
extension IMEIVerificationVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = IMEIVerificationInteractor()
        let presenter = IMEIVerificationPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

