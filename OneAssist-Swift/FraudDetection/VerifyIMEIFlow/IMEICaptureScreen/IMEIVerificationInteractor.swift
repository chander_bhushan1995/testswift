//
//  IMEIVerificationInteractor.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 05/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIVerificationPresenterLogic {
    func takeScreenShot(with tempCustInfoModel: TemporaryCustomerInfoModel?)
    func removeScreenShotObserver()
    
    func recievedUpdateDetailsResponse(with imei: String,and imei2: String?, response: UpdatePostPaymentDetailsResponseDTO?, error: Error?)
    func uploadResponseReceived(for invoiceName: String, response: UploadInvoiceFDResponseDTO?, error: Error?)
    
    func validateBarCodeSupportValue(with resposne: FDbarcodeScannerResponseDTO?)

}

class IMEIVerificationInteractor: BaseInteractor {
    var presenter: IMEIVerificationPresenterLogic?
    
}

extension IMEIVerificationInteractor: IMEIVerificationBusinessLogic {
    func getIMEINumder(with tempCustInfoModel: TemporaryCustomerInfoModel?) {
        presenter?.takeScreenShot(with: tempCustInfoModel)
    }
    
    func removeObserver() {
        presenter?.removeScreenShotObserver()
    }
    
    func submitIMEI(with pincode: String, address: String, orderId: String, deviceModel: String,
                    imei: String,imei2: String?) {
        let addrInfo = AddressInfo(addressLine1: address, pincode: pincode)
        let req = UpdatePostPaymentDetailsRequestDTO(addressInfo: addrInfo, deviceModel: deviceModel, orderId: orderId, imei: imei, imei2: imei2, purchaseAmount: FDCacheManager.shared.purchaseAmount, purchaseDate: NSNumber(value: FDCacheManager.shared.purchaseDate))
        
        let _ = UpdatePostPaymentDetailUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedUpdateDetailsResponse(with: imei, and: imei2, response: response, error: error)
        }
    }
    
    func uploadFDDocument(for invoiceName: String, activityRefId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?) {
        
        let req = UploadInvoiceFDRequestDTO(activityReferenceId: activityRefId, name: name, fileName: fileName, docFileContentType: docFileContentType, displayName: displayName, docMandatory: docMandatory, uploadedFile: uploadedFile)
        
        let _ = UploadInvoiceFDUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.uploadResponseReceived(for: invoiceName, response: response, error: error)
        }
    }
    
    func loadFDBarcodeSupportData() {
        let _ = FDbarcodeScannersupporUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.validateBarCodeSupportValue(with: response)
        }
    }
}


