//
//  IMEIScreenInteractor.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 16/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIScreenPresenterLogic {
    func sendSMSResponse(_ response: SendSMSResponseDTO?, error: Error?)
    func presentHashKey(_ response: FDHashKeyGenerationResDTO?, _ error: Error?)
}

class IMEIScreenInteractor: BaseInteractor {
    var presenter: IMEIScreenPresenterLogic?
}

extension IMEIScreenInteractor: IMEIScreenBusinessLogic {
    
    func sendSMS(_ mobileNumber: String?, orderId: String) {
        let obj:SendSMSRequestDTO = SendSMSRequestDTO(orderId: orderId, verificationNo: mobileNumber)
        
        let _ = SendImageUploadLinkUseCase.service(requestDTO: obj) { [weak self] (service,response,error) in
            self?.presenter?.sendSMSResponse(response,error:error)
        }
    }
    
    func getHashKey(activityReferenceId: String?) {
        let request = FDHashKeyGenerationReqDTO()
        request.activityReferenceId = activityReferenceId
        FDHashKeyGenerationUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.presentHashKey(response, error)
        }
    }
}
