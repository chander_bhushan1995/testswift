//
//  IMEIScreenPresenter.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 16/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIScreenDisplayLogic: class {
    func displaySendSMSSuccess(_ response: SendSMSResponseDTO)
    func displaySendSMSFailure(_ error: String)
    func updateFDHashKey(_ hashKey: String?)
    func showFDHashAPIError(_ error: String?)
}

class IMEIScreenPresenter: BasePresenter {
    weak var viewController: IMEIScreenDisplayLogic?
}

extension IMEIScreenPresenter: IMEIScreenPresenterLogic {
    func sendSMSResponse(_ response: SendSMSResponseDTO?, error: Error?){
        if let error = error {
            viewController?.displaySendSMSFailure(error.localizedDescription)
        } else if response?.status == Constants.ResponseConstants.success {
            viewController?.displaySendSMSSuccess(response!)
        } else {
            viewController?.displaySendSMSFailure("Something went wrong")
        }
    }
    
    func presentHashKey(_ response: FDHashKeyGenerationResDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            
            // Success case
            viewController?.updateFDHashKey(response?.data?.hashKey)
        } catch let error as LogicalError {
            viewController?.showFDHashAPIError(error.errorDescription)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
}
