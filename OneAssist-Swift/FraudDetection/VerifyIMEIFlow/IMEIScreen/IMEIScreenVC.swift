//
//  IMEIScreenVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 07/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol IMEIScreenBusinessLogic {
    func sendSMS(_ mobileNumber: String?, orderId: String)
    func getHashKey(activityReferenceId: String?)
}


class IMEIScreenVC: BaseVC, HideNavigationProtocol {
    
    // MARK: - Outlets
    @IBOutlet weak var imeiNumberLabel: H3RegularBlackLabel!
    @IBOutlet weak var hashCodeLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var imeiSecondLabel: H3RegularBlackLabel!
    @IBOutlet var extremeLabelConstraint: NSLayoutConstraint!
    @IBOutlet var hashTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreButton: UIButton!
    
    // MARK: - private properties
    private var delay = 0.8
    private var activityRefId: String? = FDCacheManager.shared.activityRefId
    private var changeHashCodeInterval: TimeInterval?
    private var callHashCodeAPIInterval: TimeInterval?
    private var hashCodeAPICallTimer: Timer?
    private var hashCodeChangeTimer: Timer?
    private var screenshotCapture: ScreenshotCapture?
    private lazy var mirrorTestManager: MirrorTestManager? = MirrorTestManager(.both)
    
    var interactor: IMEIScreenBusinessLogic?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        resetAll()
    }
    
    // MARK: - private methods
    private func setup() {
        self.addObserver()
        self.appDidBecomeActive()
        self.screenCaptureStatusChanged()
    }
    
    private func resetAll() {
        self.setUpPreviousBrightnessValue()
        self.removeObserver()
        self.applicationWillResignActive()
        stopAllTimer()
    }
    
    private func stopAllTimer(){
        self.hashCodeChangeTimer?.invalidate()
        self.hashCodeChangeTimer = nil
        self.hashCodeAPICallTimer?.invalidate()
        self.hashCodeAPICallTimer = nil
    }
    
    private func initializeView() {
        self.modalPresentationCapturesStatusBarAppearance = true
        if let saveObject = SaveFDDataMode.getSaveData(FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue) {
            
            if let imeiSecondNumber = saveObject.imei2 {
                imeiNumberLabel.text = "\(saveObject.imei ?? "")"
                imeiSecondLabel.isHidden = false
                extremeLabelConstraint.priority = .defaultLow
                hashTopConstraint.isActive = true
                imeiSecondLabel.text = "\(imeiSecondNumber)"
            } else {
                imeiNumberLabel.text = "\(saveObject.imei ?? "")"
                imeiSecondLabel.isHidden = true
                hashTopConstraint.isActive = false
                extremeLabelConstraint.priority = .defaultHigh
            }
        }
        let thresolds = TFLiteModelsDownloadManager.sharedInstance.mirrorTestThresolds?.front_panel_mirror_test
        changeHashCodeInterval = (thresolds?.changeHashCodeThresold?.doubleValue ?? 500) / 1000
        callHashCodeAPIInterval = (thresolds?.hashCodeAPICallThresold?.doubleValue ?? 900000) / 1000
        self.startAPICallForHashCode()
        self.screenshotCapture = ScreenshotCapture(delegate: self)
        self.screenshotCapture?.isContinuousCapture = true
        self.screenshotCapture?.requestPhotosAuthorization(andThen: {[unowned self] in
            self.screenshotCapture?.registerScreenShotObserver()
            self.screenshotCapture?.registerApplicationStateObserver()
        })
    }
    
    private func startAPICallForHashCode() {
        self.stopAllTimer()
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
        self.interactor?.getHashKey(activityReferenceId: self.activityRefId)
    }
    
    private func startTimerForHashCodeAPICall() {
        self.hashCodeAPICallTimer?.invalidate()
        self.hashCodeAPICallTimer = nil
        self.hashCodeAPICallTimer = Timer.scheduledTimer(withTimeInterval: callHashCodeAPIInterval ?? 900000, repeats: false, block: { [weak self] timer in
            timer.invalidate()
            guard let self = self else {return}
            self.startAPICallForHashCode()
        })
    }
    
    private func startChangeHashCodeTimer() {
        self.hashCodeChangeTimer?.invalidate()
        self.hashCodeChangeTimer = nil
        self.hashCodeChangeTimer = Timer.scheduledTimer(withTimeInterval: changeHashCodeInterval ?? 0.4, repeats: true, block: { [weak self] timer in
            self?.hashCodeLabel.text = self?.mirrorTestManager?.getDisplayableHashKey(with: nil)
        })
    }
    
    private func setupTimerValue() {
        let value = OAFDLibrary.getTimerValue(Int(FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? "") ?? 0)
        self.hashCodeLabel.text = value
    }

    //MARK:- UIButton Action
    @IBAction func onClickBackAction(_ sender: UIButton?) {
        // Refreshing Membership Tab
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickMoreAction(_ sender: UIButton?) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = moreButton // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = [Strings.iOSFraudDetection.resendSame,
                               Strings.iOSFraudDetection.resendNew,
                               Strings.iOSFraudDetection.chatForAssistance]
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            switch index {
            case 0:
                EventTracking.shared.eventTracking(name: .resendSMS ,[.location: "Same Number", .mode: FDCacheManager.shared.fdTestType.rawValue])
                Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
                self.interactor?.sendSMS(SaveFDDataMode.getSMSNumber(FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue), orderId: FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? "")
                break
            case 1:
                EventTracking.shared.eventTracking(name: .resendSMS ,[.location: "Different Number", .mode: FDCacheManager.shared.fdTestType.rawValue])
                self.routeToSMSScreen()
                break
            case 2:
                self.routeToChat()
                break
            default:
                break
            }
        }
        
        dropDown.width = 254
        dropDown.direction = .bottom
        dropDown.show(beforeTransform: nil, anchorPoint: CGPoint(x: 0.7, y: 0.2))
    }
    
    func routeToChat() {
        present(ChatVC(), animated: true, completion: nil)
    }
    
    func routeToSMSScreen() {
        self.dismiss(animated: false) {[unowned self] in
            // NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            if let rootVC = self.popToRootView() as? TabVC {
                rootVC.routeToSendSMSScreen(imeinumber: nil)
            }
        }
    }

}

// MARK:- Display Logic Conformance
extension IMEIScreenVC: IMEIScreenDisplayLogic {
    func displaySendSMSSuccess(_ response: SendSMSResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if let mobileNumber = SaveFDDataMode.getSMSNumber(FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue) {
            let message = Strings.iOSFraudDetection.smsSentMessage + mobileNumber + "."
            showAlert(title: Strings.iOSFraudDetection.smsSentTitle, message: message, primaryButtonTitle: "Ok", nil, isCrossEnable: false, logoImage: UIImage(named: "checkboxOutline"), primaryAction: {
            }, nil)
            
        }else {
           showAlert(message: Strings.iOSFraudDetection.smsSent)
        }
    }
    
    func displaySendSMSFailure(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func updateFDHashKey(_ hashKey: String?) { // update has key by stopping current process
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.hashCodeLabel.text = mirrorTestManager?.getDisplayableHashKey(with: hashKey)
        startTimerForHashCodeAPICall()
        startChangeHashCodeTimer()
    }
    
    func showFDHashAPIError(_ error: String?) {
        resetAll()
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.showAlert(title: "", message: error ?? "", primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
            self.interactor?.getHashKey(activityReferenceId: self.activityRefId)
        }) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}


// MARK:- Configuration Logic
extension IMEIScreenVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = IMEIScreenInteractor()
        let presenter = IMEIScreenPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK: - ScreenRecording Extension
typealias ScreenRecording = IMEIScreenVC
extension ScreenRecording {
    func showAlertIfScreenRecordingEnable() {
        let value = ScreenRecordingDetector.sharedInstance()?.isRecording()
        if value == true {
            self.showAlert(message: Strings.iOSFraudDetection.stopRecording, primaryButtonTitle: "Dismiss", nil, isCrossEnable: false, primaryAction: {
                self.onClickBackAction(nil)
            }, nil)
        }else {
            OAAlertVC.forceRemoveAlertView()
        }
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(screenCaptureStatusChanged), name: NSNotification.Name.screenRecordingDetectorRecordingStatusChanged, object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func appDidBecomeActive() {
        setUpNewBrightnessValue()
        ScreenRecordingDetector.triggerDetectorTimer()
    }
    
    @objc func applicationWillResignActive() {
        setUpPreviousBrightnessValue()
        ScreenRecordingDetector.stopTimer()
    }
    
    @objc func screenCaptureStatusChanged() {
        #if !DEBUG
        self.showAlertIfScreenRecordingEnable()
        #endif
    }
    
    func setUpNewBrightnessValue() {
        let preValue = UIScreen.main.brightness
        if preValue <= 0.5 {
            UserDefaults.standard.set(preValue, forKey: UserDefaultsKeys.previousBrightnessValue.rawValue)
            UIScreen.main.brightness = CGFloat(0.5)
        }
    }
    
    func setUpPreviousBrightnessValue() {
        if let preValue = UserDefaults.standard.value(forKey: UserDefaultsKeys.previousBrightnessValue.rawValue) as? Float {
            UIScreen.main.brightness = CGFloat(preValue)
        }
    }
}

// MARK: - ScreenshotCaptureDelegate methods
extension IMEIScreenVC: ScreenshotCaptureDelegate {
    
    func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didDetect screenshot: UIImage) {
        self.startAPICallForHashCode()
    }
    
    func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didFailWith error: ScreenshotCapture.Error) {
    }
}
