//
//  TutorialModel.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TutorialModel {
    var image: String?
    var title: String?
    var subTitle: String?
    var steps: Int = 1
    
    init(steps: Int) {
        self.steps = steps
        self.setUpData()
    }
    
    func setUpData() {
        switch self.steps {
        case 1:
            self.title = "STEP 1"
            self.subTitle = "Click on Verify IMEI button on the screen to start."
            self.image = "step1"
            break
        case 2:
            self.title = "STEP 2"
            self.subTitle = "Then, Press Call Button on the alert to open IMEI Screen."
            self.image = "step2"
            break
        case 3:
            self.title = "STEP 3"
            if !Utilities.checkPhysicalHomeButton() {
               self.subTitle = "Make sure you take screenshot of this screen.\nPress Power button and the Home button simultaneously."
            }else {
                self.subTitle = "Make sure you take screenshot of this screen.\nPress Power button and the Vol. up button simultaneously."
            }
            if #available(iOS 13.0, *) {
                self.image = "step3_13"
            }else {
               self.image = "step3"
            }
            break
        case 4:
            self.title = "STEP 4"
            if #available(iOS 13.0, *) {
                self.subTitle = "Click on Cancel button to go back to OneAssist App.\nAnd you’re done!"
                self.image = "step4_13"
            }else {
                self.subTitle = "Click on Dismiss button to go back to OneAssist App.\nAnd you’re done!"
                self.image = "step4"
            }
            break
        default:
            break
        }
    }
}
