//
//  TutorialStepVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TutorialStepVC: UIViewController, PagingProtocol {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var stepLabel: BodyTestBoldGreyLabel!
    @IBOutlet var titleLabel: H3BoldLabel!
    
    let model: TutorialModel
    var currentPage: Int = 0
    
    init(model: TutorialModel, page: Int,
         nibName nibNameOrNil: String?,
         bundle nibBundleOrNil: Bundle?) {
        self.model = model
        self.currentPage = page
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = model.image {
           imageView.image = UIImage(named: image)
        }
        
        stepLabel.text = model.title
        titleLabel.text = model.subTitle
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
