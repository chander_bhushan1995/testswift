//
//  IMEIVerificationTutorialVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol PagingProtocol {
    var currentPage: Int { get set }
}

class IMEIVerificationTutorialVC: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    private var pageViewController: UIPageViewController!
    fileprivate var currentIndex: Int = 0 {
        didSet {
            updatePageControl()
        }
    }
    
    let walkthroughs = [
        TutorialModel(steps: 1),
        TutorialModel(steps: 2),
        TutorialModel(steps: 3),
        TutorialModel(steps: 4)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updatePageControl()
    }
    
    private func initializeView() {
        containerView.clipsToBounds = true;
        containerView.layer.cornerRadius = 4.0
        pageControl.numberOfPages = 4
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.dataSource = self
        pageViewController.delegate = self
        let vc = self.viewController(at: 0)
        pageViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        pageViewController.view.frame = CGRect(x: 0.0, y: 0.0, width: containerView.bounds.width, height: containerView.bounds.height-20)
        addChild(pageViewController)
        containerView.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        currentIndex = 0
    }
    
    private func updatePageControl() {
        pageControl.currentPage = currentIndex
        pageControl.customPageControl(dotFillColor: .dodgerBlue, dotBorderColor: UIColor.dlsBorder, dotBorderWidth: 1)
    }
    
    
    // MARK: - UIButton Action
    
    @IBAction func onClickCrossAction(_ sender: UIButton?){
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK:- UIPageViewControllerDataSource
extension IMEIVerificationTutorialVC: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let index = (viewController as? PagingProtocol)?.currentPage, index < 3 else {
            return nil
        }
        
        return self.viewController(at: index + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = (viewController as? PagingProtocol)?.currentPage, index > 0 else {
            return nil
        }
        
        return self.viewController(at: index - 1)
    }
    
    func viewController(at index: Int) -> UIViewController {
        var viewController: UIViewController!
        let tutorialVC = TutorialStepVC(model: self.walkthroughs[index], page: index, nibName: "TutorialStepVC", bundle: nil)
        viewController = tutorialVC
        return viewController
    }
}

// MARK:- UIPageViewControllerDelegate
extension IMEIVerificationTutorialVC: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            return
        }
        
        let lastVC = pageViewController.viewControllers?.last as? PagingProtocol
        // pageControl.currentPage = lastVC?.currentPage ?? 0
        currentIndex = lastVC?.currentPage ?? 0
        
    }
}
