//
//  PermissionScreenVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol PermissionScreenVCDelegate: class {
    func routeToIMEIVerifyScreen()
}
typealias PermissionCallback = (Bool) -> ()

class PermissionScreenVC: BaseVC, HideNavigationProtocol {
    var isFromBuyback: Bool = false
    var callback: PermissionCallback?
    weak var delegate: PermissionScreenVCDelegate?
    
    @IBOutlet weak var buttonPermission: OAPrimaryButton!
    @IBOutlet weak var lablePermission: SupportingTextRegularGreyLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        checkPermission()
        // Do any additional setup after loading the view.
    }
    
    private func checkPermission() {
        switch Permission.checkGalleryPermission() {
            case .denied, .restricted:
                self.lablePermission.textColor = UIColor.errorText
                break
            default:
                self.lablePermission.textColor = UIColor.bodyTextGray
                break
        }
    }
    
    //Mark: - Button Action
    @IBAction func onClickAllowPermissionAction(_ sender: UIButton?){
        switch Permission.checkGalleryPermission() {
        case .notDetermined:
            Permission.requestPhotosAuthorization { (status) in
                switch status {
                    case .authorized:
                        self.lablePermission.textColor = UIColor.bodyTextGray
                        self.routeToVerifyIMEIScreen()
                        break
                    default:
                        EventTracking.shared.eventTracking(name: .galleryPermission ,[.location: "Mobile Fraud Detection", .action: "Deny", .mode: FDCacheManager.shared.fdTestType.rawValue])
                        
                        self.lablePermission.textColor = UIColor.errorText
                        break
                    
                }
            }
            break
        case .authorized:
            self.lablePermission.textColor = UIColor.bodyTextGray
            self.routeToVerifyIMEIScreen()
            break
        default:
            showPermissionAlert()
            break
        }
    }
    
    @IBAction func onClickCrossAction(_ sender: UIButton?){
        if isFromBuyback {
            navigationController?.popViewController(animated: true)
            callback?(false)
        } else {
            dismiss(animated: true, completion: {})
        }
    }
    
    private func showPermissionAlert() {
        self.showOpenSettingsAlert(title: Strings.Global.permissionDenied, message: Strings.Global.GalleryPermissionDenied)
    }
    
    
    //MARK: - Navigation
    
    func routeToVerifyIMEIScreen() {
        if isFromBuyback {
            navigationController?.popViewController(animated: true)
            callback?(true)
        } else {
            delegate?.routeToIMEIVerifyScreen()
            dismiss(animated: true, completion:{})
        }
    }

}
