//
//  ReuploadSuccessScreen.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 24/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class ReuploadSuccessScreen: BaseVC {
    
    @IBAction func btnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
