//
//  InvoiceReuploadFDInteractor.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol InvoiceReuploadFDPresentationLogic {
    func boardingImage(of name: String, response: GetClaimDocumentResponseDTO?, error: Error?)
    
    func uploadResponseReceived(for invoiceName: String, response: UploadInvoiceFDResponseDTO?, error: Error?)
    func submitDocsResponseReceived(response: SubmitDocResponseDTO?, error: Error?)
}

class InvoiceReuploadFDInteractor: BaseInteractor {
    var presenter: InvoiceReuploadFDPresentationLogic?
}

extension InvoiceReuploadFDInteractor: InvoiceReuploadFDBusinessLogic {
    
    func getDocumentImage(docId: String?, for invoiceName: String) {
        let req = GetClaimDocumentRequestDTO(documentId: docId, claimDocType: .boardingThumbnail)
        let _ = GetBankDocumentRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.boardingImage(of: invoiceName, response: response, error: error)
        }
    }
    
    func submitDocs(for activityRefId: String?) {
        let req = SubmitDocRequestDTO(activityRefId: activityRefId)
        
        let _ = SubmitDocUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.submitDocsResponseReceived(response: response, error: error)
        }
    }
    
    func uploadFDDocument(for invoiceName: String, activityRefId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?) {
        
        let req = UploadInvoiceFDRequestDTO(activityReferenceId: activityRefId, name: name, fileName: fileName, docFileContentType: docFileContentType, displayName: displayName, docMandatory: docMandatory, uploadedFile: uploadedFile)
        
        let _ = UploadInvoiceFDUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.uploadResponseReceived(for: invoiceName, response: response, error: error)
        }
    }
}
