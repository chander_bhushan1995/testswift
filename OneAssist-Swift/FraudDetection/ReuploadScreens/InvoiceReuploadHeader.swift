//
//  InvoiceReuploadHeader.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 24/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class InvoiceReuploadHeader: UIView {
    
    @IBOutlet weak var stepLabel: BodyTestBoldGreyLabel!
    @IBOutlet weak var uploadInfo: H3BoldLabel!
    
    @IBOutlet weak var topUploadConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        initializeView()
    }
    
    private func initializeView() {
        stepLabel.text = Strings.MembershipDetailsFraudDetection.firstStep
        stepLabel.isHidden = true
        topUploadConstraint.priority = .defaultHigh
        uploadInfo.text = Strings.MembershipDetailsFraudDetection.uploadInstruction
    }
    
    func setConditions(for reuploadCase: ReuploadCase?) {
        if let reuploadCase = reuploadCase {
            /*
            if reuploadCase == .reuploadAll {
                stepLabel.isHidden = false
                topUploadConstraint.priority = .defaultLow
            } else {
                
            } */
            stepLabel.isHidden = true
            topUploadConstraint.priority = .defaultHigh
        }
    }
    
}
