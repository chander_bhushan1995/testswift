//
//  InvoiceReuploadFDPresenter.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol InvoiceReuploadFDDisplayLogic: class {
    func displayImageSuccess(image: UIImage?, with name: String)
    func displayImageFailure(error: String?, with name: String)
    
    func displayUploadedImageSuccess(with name: String, data: String?)
    func displayUploadedFailure(with name: String, error: String?)
    
    func submitDocsSuccess()
    func displayError(_ error: String)
}

class InvoiceReuploadFDPresenter: BasePresenter {
    weak var viewController: InvoiceReuploadFDDisplayLogic?
    
}

extension InvoiceReuploadFDPresenter: InvoiceReuploadFDPresentationLogic {
    func boardingImage(of name: String, response: GetClaimDocumentResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayImageSuccess(image: response?.image, with: name)
        } catch {
            viewController?.displayImageFailure(error: error.localizedDescription, with: name)
        }
    }
    
    func uploadResponseReceived(for invoiceName: String, response: UploadInvoiceFDResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUploadedImageSuccess(with: invoiceName, data: response?.data)
        } catch {
            viewController?.displayUploadedFailure(with: invoiceName, error: error.localizedDescription)
        }
    }
    
    func submitDocsResponseReceived(response: SubmitDocResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.submitDocsSuccess()
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
}
