//
//  InvoiceReuploadFDVC.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import Photos

enum ReuploadCase: String {
    case reuploadInvoiceOrAdditionalInvoiceOnly
    case reuploadAll
}

protocol InvoiceReuploadFDBusinessLogic {
    
    /// - Parameter docId: mongoDB id of image
    func getDocumentImage(docId: String?, for invoiceName: String)
    
    func uploadFDDocument(for invoiceName: String, activityRefId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?)
    
    func submitDocs(for activityRefId: String?)
}

class InvoiceReuploadFDVC: BaseVC {
    
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var stickyBtn: OAPrimaryButton!
    
    var interactor: InvoiceReuploadFDBusinessLogic?
        
    var whichReuploadCase: ReuploadCase?
    
    // Ankur change
    //var deviceName: String?
    
    var isDownloadingFirstImage = false
    var isDownloadingSecondImage = false
    
    var firstImage: UIImage?
    var secondImage: UIImage?
    
    // I could not use above for this because when user tries to upload an image on the other image then above would not be nil but these would be false.
    var hasImageFirst = false
    var hasImageSecond = false
    
    var isReuploadCaseOfInvoice: Bool = false
    var isActualReuploadCaseOfInvoice: Bool = false
    var isReuploadCaseOfAdditionalInvoice: Bool = false

    var countLabels = [UIView]()
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        whichReuploadCase = getReuploadCase()
        initializeView()
    }
    
    private func getReuploadCase() -> ReuploadCase {
        let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments
        if Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs) || Utilities.isReuploadCase(of: Constants.RequestKeys.backPanel, with: verificationDocs) {
            return .reuploadAll
        }
        
        return .reuploadInvoiceOrAdditionalInvoiceOnly
    }
    
    private func addBarButtons() -> UIView {
        let chatNotificationView = Utilities.getBadgeView1(target: self,image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: #selector(self.chatClicked(_:)))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView)]
        return chatNotificationView
    }
    
    @objc func chatClicked(_ sender: Any) {
        present(ChatVC(), animated: true, completion: nil)
    }
    
    // MARK:- private methods
    private func initializeView() {
        if whichReuploadCase == .reuploadAll {
            title = Strings.Global.membershipActivation
            stickyBtn.setTitle(Strings.MembershipDetailsFraudDetection.continue, for: .normal)
        } else {
            title = Strings.Global.uploadImages
            stickyBtn.setTitle(Strings.MembershipDetailsFraudDetection.submit, for: .normal)
        }
        
        let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments
        isReuploadCaseOfInvoice = Utilities.isReuploadCaseOfInvoice(with: verificationDocs)
        isReuploadCaseOfAdditionalInvoice = Utilities.isReuploadCase(of: Constants.RequestKeys.additional_invoice_image, with: verificationDocs)
        isActualReuploadCaseOfInvoice = Utilities.isReuploadCase(of: Constants.RequestKeys.invoice_image, with: verificationDocs)
        
        stickyBtn.isEnabled = false
        
        hasImageFirst = !isActualReuploadCaseOfInvoice
        hasImageSecond = !isReuploadCaseOfAdditionalInvoice
        
        tableViewObj.register(cell: UploadDocumentsCell.self)
        
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 44
        
        tableViewObj.sectionHeaderHeight = UITableView.automaticDimension
        tableViewObj.estimatedSectionHeaderHeight = 44
        
        /* To remove the default padding for grouped table view. */
        tableViewObj.removeDefaultPaddingForGrouped()
        
        tableViewObj.dataSource = self
        tableViewObj.delegate = self
        
        countLabels.append(addBarButtons())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if firstImage == nil && !isActualReuploadCaseOfInvoice {
            if let value = getValueOfDocumentUploaded(with: purchaseInvoice) {
                isDownloadingFirstImage = true
                interactor?.getDocumentImage(docId: value, for: purchaseInvoice)
            }
        }
        
        if secondImage == nil && !isReuploadCaseOfAdditionalInvoice {
            if let value = getValueOfDocumentUploaded(with: additionalInvoice) {
                isDownloadingSecondImage = true
                interactor?.getDocumentImage(docId: value, for: additionalInvoice)
            }
        }
    }
    
    private func getValueOfDocumentUploaded(with name: String) -> String? {
        if let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
            if let value = verificationDocs.filter({ $0.name == name }).first?.value {
                return value
            }
        }
        return nil
    }
    
    @IBAction func tappedOnStickyBtn(_ sender: OAPrimaryButton) {
        if whichReuploadCase == .reuploadAll {
            
            EventTracking.shared.eventTracking(name: .invoiceReupload ,[.location: "Mobile", .type: "Invoice_Doc", .mode: FDCacheManager.shared.fdTestType.rawValue])
            
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            if let rootVC = self.popToRootView() as? TabVC {
                let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
                
                if FDCacheManager.shared.fdTestType == .MT {
                    let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
                    rootVC.navigationController?.pushViewController(membershipActivationFDVC, animated: true)
                }else {
                    if SaveFDDataMode.getSaveData(orderId) != nil || !Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
                        ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments) {
                        rootVC.routeToSendSMSScreen(imeinumber: nil, mobileNo: SaveFDDataMode.getSMSNumber(orderId))
                    } else {
                        if (Permission.checkGalleryPermission() == PHAuthorizationStatus.authorized) {
                            // Access has been granted.
                            rootVC.routeToVerifyIMEIScreen()
                        } else {
                            routeToAllowPermission()
                        }
                    }
                }
            }
        } else {
            EventTracking.shared.eventTracking(name: .invoiceReupload ,[.location: "Mobile", .type: "Only Invoice", .mode: FDCacheManager.shared.fdTestType.rawValue])
            Utilities.addLoader(onView: view,count: &loaderCount, isInteractionEnabled: false)
            interactor?.submitDocs(for: FDCacheManager.shared.getDocsToUploadModel?.activityReferenceId)
        }
    }
    
}

extension InvoiceReuploadFDVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isReuploadCaseOfInvoice {
            return 2
        }
        return 1
    }
    
    fileprivate func getFirstCellInfo() -> (String,String,Int,Int,Bool,Bool,Bool,UIImage?,Bool,Bool,Bool?) {
        let title = Strings.MobilePlanRegistrationScene.uploadInvoiceWithIMEI
        let subtitle = Strings.MobilePlanRegistrationScene.blurryImage
        let bottomConstant = 0
        let imageViewCentreConstant = -20
        let isHiddenButton = true
        let isHiddenDescriptionLabel = false
        let isHiddenCircularView = true
        
        let image = firstImage
        let isDownloading = isDownloadingFirstImage
        let isAttributedText = false
        
        return (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText,nil)
    }
    
    fileprivate func getSecondCellInfo() -> (String,String,Int,Int,Bool,Bool,Bool,UIImage?,Bool,Bool,Bool?) {
        let title = String(format: Strings.MobilePlanRegistrationScene.uploadDocumentWithIMEIOptional,Strings.Global.optional)
        let subtitle = ""
        let bottomConstant = 0
        let imageViewCentreConstant = -16
        let isHiddenButton = false
        let isHiddenDescriptionLabel = true
        let isHiddenCircularView = true
        
        let image = secondImage
        let isDownloading = isDownloadingSecondImage
        let isAttributedText = true
        var isEditButtonHidden: Bool?
        
        if let status = Utilities.reuploadCase(of: Constants.RequestKeys.additional_invoice_image, with: FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments), status == Strings.DocsUploadStatus.accepted {
            isEditButtonHidden = true
        }
        
        return (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText, isEditButtonHidden)
    }
    
    func getCellInfo(with indexPath: IndexPath) -> (String,String,Int,Int,Bool,Bool,Bool,UIImage?,Bool,Bool, Bool?) {
        var title = Strings.MobilePlanRegistrationScene.uploadInvoiceWithIMEI
        var subtitle: String = Strings.MobilePlanRegistrationScene.blurryImage
        var bottomConstant = 0
        var imageViewCentreConstant = -20
        var isHiddenButton = true
        var isHiddenDescriptionLabel = false
        var isHiddenCircularView = false
        var image: UIImage?
        var isDownloading = false
        var isAttributedText = false
        var isEditButtonHidden: Bool?
        
        if isReuploadCaseOfInvoice {
            if indexPath.row == 0 {
                (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText,isEditButtonHidden) = getFirstCellInfo()
            } else if indexPath.row == 1 {
                (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText,isEditButtonHidden) = getSecondCellInfo()
            }
        } else {
            (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText,isEditButtonHidden) = getSecondCellInfo()
        }
        
        return (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText,isEditButtonHidden)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let uploadCell: UploadDocumentsCell = tableView.dequeueReusableCell(indexPath: indexPath)
        uploadCell.delegate = self
        
        var title = Strings.MobilePlanRegistrationScene.uploadInvoiceWithIMEI
        var subtitle: String = Strings.MobilePlanRegistrationScene.blurryImage
        var bottomConstant = 0
        var imageViewCentreConstant = -20
        var isHiddenButton = true
        var isHiddenDescriptionLabel = false
        var isHiddenCircularView = false
        var image: UIImage?
        var isDownloading = false
        var isAttributedText = false
        var isEditButtonHidden: Bool?
        
        (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,image, isDownloading,isAttributedText, isEditButtonHidden) = getCellInfo(with: indexPath)
        
        uploadCell.updateUserInterface(title: title, subtitle: subtitle, image: image, isUploaded: (image != nil), isDownloading: isDownloading, bottomConstant: CGFloat(bottomConstant), imageViewCentreConstant: CGFloat(imageViewCentreConstant), isHiddenButton: isHiddenButton, isHiddenDescriptionLabel: isHiddenDescriptionLabel,isHiddenCircularView: isHiddenCircularView, isAttributedText: (isAttributedText ? true: nil),editButtonHidden: isEditButtonHidden)
        uploadCell.layoutIfNeeded()
        return uploadCell
    }
}

extension InvoiceReuploadFDVC: PermissionScreenVCDelegate {
    func routeToIMEIVerifyScreen() {
        if let rootVC = self.popToRootView() as? TabVC {
            rootVC.routeToVerifyIMEIScreen()
        }
    }
}

extension InvoiceReuploadFDVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed(Constants.NIBNames.invoiceReuploadHeader, owner: nil, options: nil)?.first as? InvoiceReuploadHeader
        
        headerView?.setConditions(for: whichReuploadCase)
        headerView?.layoutIfNeeded()
        return headerView
    }
}

// MARK:- Upload Document Cell Delegate
extension InvoiceReuploadFDVC: UploadDocumentsCellDelegate {
    
    fileprivate func checkForButtonSubmit() {
        
        if isDownloadingFirstImage || isDownloadingSecondImage {
            stickyBtn.isEnabled = false
        } else {
            stickyBtn.isEnabled = hasImageFirst && hasImageSecond
        }
        
        /*
         if isReuploadCaseOfInvoice {
         stickyBtn.isEnabled = hasImageFirst
         } else if isReuploadCaseOfAdditionalInvoice {
         stickyBtn.isEnabled = hasImageSecond
         } */
    }
    
    private func pickImage(of cell: UploadDocumentsCell) {
        
        // action sheet
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: Strings.Global.uploadImage, style: .default) { (action) in
            ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(of: cell, image, error, ImagePickerManager.shared.imageName)
            })
        }
        
        let action2 = UIAlertAction(title: Strings.Global.clickPhoto, style: .default) { (action2) in
            ImagePickerManager.shared.pickImageFromCamera(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(of: cell, image, error, ImagePickerManager.shared.imageName)
            })
        }
        
        let action3 = UIAlertAction(title: Strings.Global.cancel, style: .cancel) { (action3) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action)
        alertController.addAction(action2)
        alertController.addAction(action3)
        presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    private func getName(for image: UIImage, with indexPath: IndexPath?) -> String {
        var name = purchaseInvoice
        
        if isReuploadCaseOfInvoice {
            if let indexPath = indexPath {
                if indexPath.row == 0 {
                    name = purchaseInvoice
                    hasImageFirst = false
                    firstImage = image
                } else if indexPath.row == 1 {
                    name = additionalInvoice
                    hasImageSecond = false
                    secondImage = image
                }
            }
        } else {
            name = additionalInvoice
            hasImageSecond = false
            secondImage = image
        }
        
        return name
    }
    
    private func callUploadFraudDetectionDocAPI(with fileName: String, _ indexPath: IndexPath?, _ image: UIImage?) {
       
            guard let image = image else {
                return
            }
                        
            let name = self.getName(for: image, with: indexPath)
            
            var verificationDoc: VerificationDocument?
            
        if let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
                verificationDoc = verificationDocs.filter({ $0.name == name }).first
            }
            
            var verifiedFileName = fileName
            
            if verifiedFileName.isEmpty {
                verifiedFileName = "default.png"
            }
            
            let pathExtention = (verifiedFileName as NSString).pathExtension
            let uploadedFile = image.resizeToMB(5)
            self.interactor?.uploadFDDocument(for: name, activityRefId: FDCacheManager.shared.getDocsToUploadModel?.activityReferenceId, name: verificationDoc?.name, fileName: verifiedFileName, docFileContentType: "image/.\(pathExtention.lowercased())", displayName: verificationDoc?.displayName, docMandatory: verificationDoc?.docMandatory, uploadedFile: uploadedFile)
    }
    
    private func handlePickedImage(of cell: UploadDocumentsCell, _ image: UIImage?, _ error: Error?,_ imageName: String) {
        if let error = error {
            // Handle error
            print(error)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {[unowned self] in
                if let img = image, let dta = img.jpegData(compressionQuality: 0.9), let err = Utilities.isImageDataAceptableForFD(dta, imageName) {
                    self.showAlert(message: err.localizedDescription)
                    return
                }
                
                var title = Strings.MobilePlanRegistrationScene.uploadInvoiceWithIMEI
                var subtitle: String = Strings.MobilePlanRegistrationScene.blurryImage
                var bottomConstant = 0
                var imageViewCentreConstant = -20
                var isHiddenButton = true
                var isHiddenDescriptionLabel = false
                var isHiddenCircularView = false
                var isEditButtonHidden: Bool?
                
                let indexPath = self.tableViewObj.indexPath(for: cell)
                
                if let indexPath = indexPath {
                    (title,subtitle,bottomConstant,imageViewCentreConstant,isHiddenButton,isHiddenDescriptionLabel,isHiddenCircularView,_,_,_,isEditButtonHidden) = self.getCellInfo(with: indexPath)
                }
                
                self.callUploadFraudDetectionDocAPI(with: imageName, indexPath, image)
                
                cell.updateUserInterface(title: title, subtitle: subtitle, image: image, isUploaded: false, isUploading: true, bottomConstant: CGFloat(bottomConstant), imageViewCentreConstant: CGFloat(imageViewCentreConstant), isHiddenButton: isHiddenButton, isHiddenDescriptionLabel: isHiddenDescriptionLabel, isHiddenCircularView: isHiddenCircularView, editButtonHidden: isEditButtonHidden)
                
                self.checkForButtonSubmit()
            }
        }
    }
    
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clikedBtnEdit sender: Any) {
        pickImage(of: cell)
    }
    
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clickedBtnSelect sender: Any) {
        pickImage(of: cell)
    }
    
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clickedKnowMore sender: Any) {
        let vc = IMEIFoundKnowMoreVC()
        self.presentInFullScreen(vc, animated: true, completion: nil)
        
//        let moreView = MoreInfoView()
//        let basePickerVC = BasePickerVC(withView: moreView, height: UIScreen.main.bounds.height);
//        moreView.basePickerVC = basePickerVC
//        moreView.layoutIfNeeded()
//        Utilities.topMostPresenterViewController.presentInFullScreen(basePickerVC, animated: true)
    }
    
}


// MARK:- Display Logic Conformance
extension InvoiceReuploadFDVC: InvoiceReuploadFDDisplayLogic {
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
    
    func submitDocsSuccess() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        if let rootVC = self.popToRootView() as? TabVC {
            rootVC.routeToSuccessScreen()
        }
    }
    
    private func reloadTableSection() {
        tableViewObj.beginUpdates()
        tableViewObj.reloadSections(IndexSet([0]), with: .fade)
        tableViewObj.endUpdates()
    }
    
    func displayImageSuccess(image: UIImage?, with name: String) {
        if name == purchaseInvoice {
            firstImage = image
            isDownloadingFirstImage = false
            hasImageFirst = true
        } else if name == additionalInvoice {
            secondImage = image
            isDownloadingSecondImage = false
            hasImageSecond = true
        }
        
        reloadTableSection()
        checkForButtonSubmit()
    }
    
    func displayImageFailure(error: String?, with name: String) {
        if name == purchaseInvoice {
            isDownloadingFirstImage = false
        } else if name == additionalInvoice {
            isDownloadingSecondImage = false
        }
        
        reloadTableSection()
        checkForButtonSubmit()
        
    }
    
    func displayUploadedImageSuccess(with name: String, data: String?) {
        // This will help in fetching images later also
        if let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
            if let verificationDoc = verificationDocs.filter({ $0.name == name }).first {
                verificationDoc.value = data
            }
        }
        
        if name == purchaseInvoice {
            hasImageFirst = true
        } else if name == additionalInvoice {
            hasImageSecond = true
        }
        
        reloadTableSection()
        checkForButtonSubmit()
    }
    
    func displayUploadedFailure(with name: String, error: String?) {
        if name == purchaseInvoice {
            hasImageFirst = !isActualReuploadCaseOfInvoice
            firstImage = nil
        } else if name == additionalInvoice {
            hasImageSecond = !isReuploadCaseOfAdditionalInvoice
            secondImage = nil
        }
        reloadTableSection()
        checkForButtonSubmit()
        
        if let errorMsg = error {
            self.showAlert(message: errorMsg)
        }

    }
}


// MARK:- Configuration Logic
extension InvoiceReuploadFDVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = InvoiceReuploadFDInteractor()
        let presenter = InvoiceReuploadFDPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension InvoiceReuploadFDVC {
    
    func routeToAllowPermission() {
        let vc = PermissionScreenVC()
        vc.delegate = self
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}
