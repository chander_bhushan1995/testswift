//
//  MTDelegateConfirmance.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 01/06/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

protocol CommonMirrorTestDelegate: PanelPreviewDelegate, PreMirrorTestDelegate, MirrorTestViewDelegate {
}

extension CommonMirrorTestDelegate where Self: AnyObject {
    
    // Method decides whether PreMirorTestScreen Needs to present or not
    func decideFlow() {
        let flowsArray = Array<ImageCaptureType>([.CaptureBoth,.ReuploadBoth,.ReuploadFrontPanel])
        if flowsArray.contains(FDCacheManager.shared.imageCaptureType) {
            redirectToPreMirrorTestScreen()
        } else {
            self.redirectToMirrorTestScreen(isCaptureFront: false)
        }
    }
    
    func redirectToPreMirrorTestScreen(isRetake: Bool = false) {
        Permission.checkCameraPermission(eventLocation: nil, authorizationStatus: {[weak self] (status) in
            guard let self = self else {return}
            DispatchQueue.main.async {
                if status == .authorized {
                    let vc = PreMirrorTestViewController()
                    vc.delegate = self
                    vc.isRetake = isRetake
                    Utilities.topMostPresenterViewController.presentInFullScreen(vc, animated: false, completion: nil)
                } else {
                    Utilities.topMostPresenterViewController.showOpenSettingsAlert(title: MhcStrings.AlertMessage.cameraTitle, message: Strings.Global.CameraPermissionDenied)
                }
            }
        })
    }
    
    func redirectToMirrorTestScreen(isCaptureFront: Bool = true) {
        Permission.checkCameraPermission(eventLocation: nil, authorizationStatus: {[weak self] (status) in
            guard let self = self else {return}
            DispatchQueue.main.async {
                if status == .authorized {
                    let vc = MirrorTestViewController()
                    vc.isCaptureFront = isCaptureFront
                    vc.delegate = self
                    Utilities.topMostPresenterViewController.presentInFullScreen(vc, animated: false, completion: nil)
                } else {
                    Utilities.topMostPresenterViewController.showOpenSettingsAlert(title: MhcStrings.AlertMessage.cameraTitle, message: Strings.Global.CameraPermissionDenied)
                }
            }
        })
    }
}

// MARK: - PanelPreviewDelegate methods
extension CommonMirrorTestDelegate where Self: AnyObject {
    func retakePhoto(for: CameraType) {
        if `for` == .FrontPanel {
            self.redirectToPreMirrorTestScreen(isRetake: true)
        } else {
            self.redirectToMirrorTestScreen(isCaptureFront: false)
        }
    }
}

// MARK: - PreMirrorTestDelegate methods
extension CommonMirrorTestDelegate where Self: AnyObject {
    //child class will provide implementation as per their navigation to push or present
    func redirectToTextToSpeechScreen() {
        Utilities.dismissAllPresentedVC { [weak self] in
            if let vc = self as? VideoGuideViewController {
                let viewController = TextToSpeechViewController()
                vc.navigationController?.pushViewController(viewController, animated: true)
            }
            if self is TabVC {
                if let baseNavigationViewController = Utilities.topMostPresenterViewController as? BaseNavigationController {
                    let viewController = TextToSpeechViewController()
                    baseNavigationViewController.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    func onObjectDetected() {
        if let topVC = Utilities.topMostPresenterViewController as? PreMirrorTestViewController, topVC.isRetake {
            Utilities.topMostPresenterViewController.dismiss(animated: false) { [weak self] in
                self?.redirectToMirrorTestScreen()
            }
            return
        }
        self.redirectToMirrorTestScreen()
    }
}

// MARK: - MirrorTest View Delegate Methods
extension CommonMirrorTestDelegate where Self: AnyObject {
    
    func openPanelPreview(with image: UIImage?, for: CameraType) {
        if let viewController = Utilities.topMostPresenterViewController as? PanelPreviewViewController, `for` == viewController.imageCapturedFor {
            viewController.updateImageAfterRetake(image: image)
        } else {
            let vc = PanelPreviewViewController()
            vc.capturedImage = image
            vc.imageCapturedFor = `for`
            vc.delegate = self
            Utilities.topMostPresenterViewController.presentInFullScreen(vc, animated: false, completion: nil)
        }
    }
    
    func onFrontPanelClicked(image: UIImage?) {
        // if panel preview  view controller already in stack and current flow is retake flow
        let previousVC: PanelPreviewViewController? = Utilities.getPresentedVCInStack()
        previousVC?.setDefaultProperty()
        
        // common code for retake and non-retake
        Utilities.topMostPresenterViewController.dismiss(animated: false) { [weak self] in
            self?.openPanelPreview(with: image, for: .FrontPanel)
        }
    }
    
    func onBackPanelClicked(image: UIImage?) {
        Utilities.topMostPresenterViewController.dismiss(animated: false) { [weak self] in
            self?.openPanelPreview(with: image, for: .BackPanel)
        }
    }
}
