//
//  TextToSpeechViewController.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 16/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import MessageUI

protocol TextToSpeechBusinessLogic {
   func loadTextToSpeechData();
}

class TextToSpeechViewController: BaseVC, CommonMirrorTestDelegate {
    
    @IBOutlet weak var inputTextField: TextFieldView!
    @IBOutlet weak var voiceSupportButton: UIButton!
    @IBOutlet weak var withourVoiceSupportButton: UIButton!

    var interactor: TextToSpeechBusinessLogic?
    
    var dataSource: [LanguageData] = []
    var selectedLanguage: LanguageData?
    
    // MARK:- Object lifecycle
   override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
       super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
       setupDependencyConfigurator()
   }
   
   required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       setupDependencyConfigurator()
   }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.  
    }
    
    private func initializeView() {
        title = "Select Language"
        inputTextField.descriptionText = Strings.iOSFraudDetection.selectLanguage
        inputTextField.placeholderFieldText = Strings.iOSFraudDetection.selectLanguage
        inputTextField.fieldType = NameFieldType.self
        inputTextField.delegate = self
        inputTextField.enableFieldViewButton(true, showRightImage:  true)
        
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        interactor?.loadTextToSpeechData()
    }
    
    func setupLanguage() {
        inputTextField.fieldText = selectedLanguage?.language
    }
    
    func presentSelectLanguage() {
        let timePicker = MHCCategoryBottomSheetView()
        timePicker.setLanguageDataModel(dataSource, delegate: self)
        Utilities.presentPopover(view: timePicker, height: MHCCategoryBottomSheetView.languageHeight(forModel: dataSource))
    }
    
    //MARK: - UIButton Action
    @IBAction func onClickVoiceSupportAction(_ sender: UIButton?) {
        if let selected = selectedLanguage {
            EventTracking.shared.eventTracking(name: .enableVoiceSupport, [.mode: FDCacheManager.shared.fdTestType.rawValue, .language: selected.language ?? ""])
           FDCacheManager.shared.selectLanguageForMirrorTest = selected
        }
        self.decideFlow()
    }
    
    @IBAction func onClickVoiceNotSupportAction(_ sender: UIButton?) {
        FDCacheManager.shared.selectLanguageForMirrorTest = nil
        EventTracking.shared.eventTracking(name: .skipVoiceSupport, [.mode: FDCacheManager.shared.fdTestType.rawValue])
        self.decideFlow()
    }
    
    
}

// MARK: - TextFieldViewDelegate
extension TextToSpeechViewController: TextFieldViewDelegate {
    func textFieldViewShouldBeginEditing(_ textFieldView: TextFieldView) -> Bool {
        return false
    }
    
    func textViewBtnClicked(_ textFieldView: TextFieldView) {
        textFieldView.animateTextField(state: .normal)
        presentSelectLanguage()
    }
}

// MARK: - TextToSpechDisplayLogic
extension TextToSpeechViewController: TextToSpechDisplayLogic {
    func displayLanguageDataSuccess(with response: [LanguageData]) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.dataSource = response
        if self.dataSource.count > 0 {
            selectedLanguage = self.dataSource.first
        }
        setupLanguage()
    }
    
    func displayLanguageDataFailure(with error: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
    }
}

// MARK: - MHCCategoryBottomSheetViewDelegate
extension TextToSpeechViewController: MHCCategoryBottomSheetViewDelegate {
    
    func dismiss() {
           Utilities.topMostPresenterViewController.dismiss(animated: true)
       }
    
    func didSelectLanguage(language: LanguageData) {
        selectedLanguage = language
        setupLanguage()
    }
}

// MARK:- Configuration Logic
extension TextToSpeechViewController {
    fileprivate func setupDependencyConfigurator() {
        let interactor = TextToSpeechInterector()
        let presenter = TextToSpechPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

