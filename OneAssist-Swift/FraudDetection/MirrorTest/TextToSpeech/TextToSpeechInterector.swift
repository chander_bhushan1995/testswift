//
//  TextToSpeechInterector.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 16/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol TextToSpeechPresenterLogic {
       func validateSampleValue(for response: MirrorTestLanguageModelDTO?, error: Error?)
}

class TextToSpeechInterector: BaseInteractor {
    var presenter: TextToSpeechPresenterLogic?
}

extension TextToSpeechInterector: TextToSpeechBusinessLogic {
    func loadTextToSpeechData() {
        let _ = GetLanguageDataUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
          self.presenter?.validateSampleValue(for: response, error: error)
        }
    }
}
