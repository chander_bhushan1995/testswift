//
//  TextToSpechPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 16/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol TextToSpechDisplayLogic: class {
    func displayLanguageDataSuccess(with response: [LanguageData])
    func displayLanguageDataFailure(with error: String?)
}

class TextToSpechPresenter: BasePresenter {
     weak var viewController: TextToSpechDisplayLogic?
}

extension TextToSpechPresenter: TextToSpeechPresenterLogic {
    func validateSampleValue(for response: MirrorTestLanguageModelDTO?, error: Error?) {
        if error != nil {
            self.viewController?.displayLanguageDataFailure(with: error?.localizedDescription)
        }else {
             var dataSource: [LanguageData] = []
            if let resposnemodel = response?.lang_data?.filter({$0.languageCode != nil}){
                let sortedArr = resposnemodel.sorted { (id1, id2) -> Bool in
                    return id1.order.intValue < id2.order.intValue
                }
                dataSource = sortedArr
              }
              self.viewController?.displayLanguageDataSuccess(with: dataSource)
        }
    }
}
