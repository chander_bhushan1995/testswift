//
//  MirrorTestManager.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 20/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

// MARK: - TFLiteModelsDownloadError
enum MirrorTestError: Error {
    case ObjectDetectionFaild
    case ImageClassificationFailed // nomessage
    case ImageClassificationLowConfidence
    case ModelLoadingError // next frame capture
    case RuntimeError // next frame capture
    case ObstractImageDetected
    case ObjectIsFar
    case ObjectIsTooNear
    case OCRFailed
    case LeftRightTextOCRFailed
    case ImageIsDark
    
    func getRespectiveMessage() -> String? {
        let errorMessages = TFLiteModelsDownloadManager.sharedInstance.mirrorTestErrorMessages
        switch self {
        case .ObjectDetectionFaild:
            return errorMessages?.frontPanelErrorMessages?.objectDetectionFaild
        case .ImageClassificationLowConfidence:
            return errorMessages?.frontPanelErrorMessages?.okWithLowConfidence
        case .ObstractImageDetected:
            return errorMessages?.frontPanelErrorMessages?.obstractedImageDetected
        case .ObjectIsFar:
            return errorMessages?.frontPanelErrorMessages?.objectIsFar
        case .ObjectIsTooNear:
            return errorMessages?.frontPanelErrorMessages?.objectIsNear
        case .OCRFailed:
            return errorMessages?.frontPanelErrorMessages?.OCRFailed
        case .ImageIsDark:
            return errorMessages?.frontPanelErrorMessages?.darkImageDetected
        case .LeftRightTextOCRFailed:
            return errorMessages?.frontPanelErrorMessages?.OCRLeftRightFailed
        default:
            return "runtime error"
        }
    }
}

//MARK: - Enum For MirrorTestManager
enum MirrorTestManagerFor {
    case objectDetection
    case imageClassification
    case both
    case none
}

class MirrorTestManager {
    
    //MARK: - PRIVATE VARIABLES
    private var objectDetectionHandler: ModelDataHandler? = nil
    private var imageClassificationHandler: ModelDataHandler? = nil
    
    private var downloadManager = TFLiteModelsDownloadManager.sharedInstance
    private var thresolds = TFLiteModelsDownloadManager.sharedInstance.mirrorTestThresolds?.front_panel_mirror_test
    private var objectDetectionReqClassName = "cell phone"
    private var imageClassificationMendatoryClass = "ok"
    private var objectDetectionOtherClasses = ["tv", "laptop"]
    private var objectDetectionConfidence: Float = 0.5
    private var imageClassificationConfidence: Float = 0.5
    private var requiredMinLeftRightBrighteness: Double = 10.0 // used for dark logic
    private var requiredMinTopBottomBrighteness: Double = 5.0 // used for dark logic
    private var requiredStripSize: Float = 0.2 // used for dark logic in percentage
    private var requiredBoundingBoxPerUnit: Float = 0.11 // used for far logic in percentage
    private var requiredBoundingBoxPerUnitNear: Float = 0.19 // used for near logic in percentage
    private var digitsComparisonForIMEIocr: Int = 6
    
    private var primaryIMEI: Int64?
    private(set) var serverKey: Int64?
    private var lastIMEIandKeyAddition: Int64 = 0
    private var hashKeyMaxLength: Int8 = 19
    
    weak var logger: OALogger?
    
    init?(_ instance: MirrorTestManagerFor) {
        
        objectDetectionConfidence = thresolds?.object_detection_min_confidence?.floatValue ?? 0.5
        imageClassificationConfidence = thresolds?.image_classification_ok_confidence?.floatValue ?? 0.5
        requiredMinLeftRightBrighteness = thresolds?.LRMinBrightness?.doubleValue ?? 10
        requiredMinTopBottomBrighteness = thresolds?.TBMinBrightness?.doubleValue ?? 5
        requiredStripSize = thresolds?.stripSize?.floatValue ?? 0.2
        requiredBoundingBoxPerUnit = thresolds?.bounding_box_per_unit_percentage?.floatValue ?? 0.11
        requiredBoundingBoxPerUnitNear = thresolds?.bounding_box_per_unit_percentage_near?.floatValue ?? 0.19
        digitsComparisonForIMEIocr = thresolds?.imeiDigitsComparisonForOCR?.intValue ?? 6
        
        if let saveObject = SaveFDDataMode.getSaveData(FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue), let imei = saveObject.imei {
            primaryIMEI = Int64(imei)
        }
        
        switch instance {
        case .objectDetection:
            if let modelFile = downloadManager.models?[Strings.Global.objectDetectionModel], let labelFile = downloadManager.models?[Strings.Global.objectDetectionLabels] {
                objectDetectionHandler = ModelDataHandler(modelFileInfo: (path: modelFile, fileType: "tflite"), labelsFileInfo: (path: labelFile, fileType: "txtfile"), inputWidth: 300, inputHeight: 300)
                guard let _ = objectDetectionHandler else {
                    return nil
                }
            }
        case .both:
            if let objectModel = downloadManager.models?[Strings.Global.objectDetectionModel], let objectLabels = downloadManager.models?[Strings.Global.objectDetectionLabels],
               let iCModel = downloadManager.models?[Strings.Global.imageClassificationModel],
               let iCLabels = downloadManager.models?[Strings.Global.imageClassificationLabels] {
                objectDetectionHandler = ModelDataHandler(modelFileInfo: (path: objectModel, fileType: "tflite"), labelsFileInfo: (path: objectLabels, fileType: "txtfile"), inputWidth: 300, inputHeight: 300)
                imageClassificationHandler = ModelDataHandler(modelFileInfo: (path: iCModel, fileType: "tflite"), labelsFileInfo: (path: iCLabels, fileType: "txtfile"), inputWidth: 224, inputHeight: 224)
                guard let _ = objectDetectionHandler, let _ = imageClassificationHandler else {
                    return nil
                }
            }
        default:
            break;
        }
    }
    
    deinit {
        print("Mirror Test Manager deinit")
    }
    
    func resetAll() {
        objectDetectionHandler = nil
        imageClassificationHandler = nil
    }
    
    func runMirrorTest(pixelBuffer: CVPixelBuffer, originalImage: UIImage?, isSkipNonMendatoryClasses: Bool = false) throws -> (originalImage: UIImage?, croppedImage: UIImage?, detectedObjectRect: CGRect?)? {
        logger?.mirrorTestFBLogData.removeAll()
        if let objectDetectionResult = try runObjectDetectionModel(pixelBuffer: pixelBuffer, with: objectDetectionConfidence) {
            let detectedObjImage = originalImage?.cropImage(rect: objectDetectionResult.rect)
            // perform far or near logic
            let result = try checkIfFarOrNear(image: originalImage, croppedImage: detectedObjImage)
            if !result.far && !result.near {
                // perform dark logic
                let detectedObjCVBuffer = detectedObjImage?.pixelBufferFromImage()
                if !isDark(capturedImage: originalImage, croppedImageRect: objectDetectionResult.rect), let detectedObjCVBuffer = detectedObjCVBuffer{
                    if !isSkipNonMendatoryClasses {  // if image classification thresold not reached
                        if let results = try runImageClassification(pixelBuffer: detectedObjCVBuffer, isSkipNonMendatoryClasses: isSkipNonMendatoryClasses) {
                            if let result = results.filter({$0.label == imageClassificationMendatoryClass && $0.confidence >= imageClassificationConfidence}).first { // OK with required confidence
                                logger?.mirrorTestFBLogData["imageClassification"] = ["className": result.label, "confidence": result.confidence]
                                return (originalImage: originalImage, croppedImage: detectedObjImage, detectedObjectRect: objectDetectionResult.rect)
                            } else {
                                let resultWithMaxConfidence = results.max{ prev, next in prev.confidence < next.confidence }
                                logger?.mirrorTestFBLogData["imageClassification"] = ["className": resultWithMaxConfidence?.label ?? "", "confidence": resultWithMaxConfidence?.confidence ?? 0]
                                if resultWithMaxConfidence?.label == imageClassificationMendatoryClass { // low confidence with ok
                                    debugPrint("classification failed ImageClassificationLowConfidence")
                                    logger?.log(errorString: MirrorTestError.ImageClassificationLowConfidence.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: detectedObjImage)
                                    throw MirrorTestError.ImageClassificationLowConfidence
                                } else { // result found other than ok
                                    debugPrint("Obstracted Image Detected")
                                    logger?.log(errorString: MirrorTestError.ObstractImageDetected.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: detectedObjImage)
                                    throw MirrorTestError.ObstractImageDetected
                                }
                            }
                        }
                        debugPrint("classification failed")
                        logger?.log(errorString: MirrorTestError.ImageClassificationFailed.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: detectedObjImage)
                        throw MirrorTestError.ImageClassificationFailed // not found any result for image classification
                    } else { // if image classification skipped
                        logger?.log(errorString: "Classification Skipped With Reached Thresold")
                        debugPrint("Classification Skipped With Reached Thresold")
                        return (originalImage: originalImage, croppedImage: detectedObjImage, detectedObjectRect: objectDetectionResult.rect)
                    }
                }
                debugPrint("image is dark")
                logger?.log(errorString: MirrorTestError.ImageIsDark.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: detectedObjImage)
                throw MirrorTestError.ImageIsDark
            }
            debugPrint("object is far")
            let error = result.far ? MirrorTestError.ObjectIsFar : MirrorTestError.ObjectIsTooNear
            logger?.log(errorString: error.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: detectedObjImage)
            throw error // far logic failed
        }
        debugPrint("object detection failed")
        logger?.log(errorString: MirrorTestError.ObjectDetectionFaild.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: nil)
        throw MirrorTestError.ObjectDetectionFaild // object detection result not found
    }
    
    func runObjectDetectionModel(pixelBuffer: CVPixelBuffer, with confidence: Float) throws -> Inference? {
        let results = try self.objectDetectionHandler?.runObjectModel(onFrame: pixelBuffer)
        var finalResult: Inference?
        debugPrint("Runing object detection \(results)")
        logger?.log(errorString: "Object Detection Result \n \(String(describing: results))")
        if let matchedResult = results?.inferences.filter({$0.className == objectDetectionReqClassName && $0.confidence >= confidence}).first {
            finalResult = matchedResult
        } else {
            // in case cell phone is not in results we assume that system is assuming cell phone as laptop, tv etc.
            for inference in (results?.inferences ?? []) {
                if objectDetectionOtherClasses.contains(inference.className) && inference.confidence >= confidence {
                    finalResult = inference
                    break
                }
            }
        }
        logger?.mirrorTestFBLogData["objectDetection"] = ["className": finalResult?.className ?? "", "confidence": finalResult?.confidence ?? 0]
        return finalResult
    }
    
    private func runImageClassification(pixelBuffer: CVPixelBuffer, isSkipNonMendatoryClasses: Bool = false) throws -> [Inference]? {
        debugPrint("Runing image classfication")
        let results = try self.imageClassificationHandler?.runImageModel(onFrame: pixelBuffer)
        debugPrint("Result from image classification :- \n \(String(describing: results))")
        logger?.log(errorString: "Object Detection Result \n \(String(describing: results))")
        return results?.inferences
    }
    
    private func checkIfFarOrNear(image:UIImage?, croppedImage: UIImage?) throws -> (far: Bool,near: Bool) { // far logic
        debugPrint("Runing far logic")
        var result = (far: true, near: true)
        let areaOfActualImage = (image?.size.height ?? 0) * (image?.size.width ?? 0) // area of actual image
        let areaOfCroppedImage = (croppedImage?.size.height ?? 0) * (croppedImage?.size.width ?? 0) // area of cropped image
        
        let precentageAreaofCroppedInImage = areaOfCroppedImage/areaOfActualImage
        result.far = precentageAreaofCroppedInImage <= CGFloat(requiredBoundingBoxPerUnit)
        result.near = precentageAreaofCroppedInImage >= CGFloat(requiredBoundingBoxPerUnitNear)
        
        logger?.mirrorTestFBLogData["far"] = ["result": result.far, "percentageArea" : precentageAreaofCroppedInImage]
        logger?.mirrorTestFBLogData["near"] = ["result": result.near, "percentageArea" : precentageAreaofCroppedInImage]
    
        debugPrint("area of actual image \(areaOfActualImage) area of cropped image \(areaOfCroppedImage) precentageAreaofCroppedInImage \(precentageAreaofCroppedInImage)")
        return result
    }
    
    private func isDark(capturedImage: UIImage?, croppedImageRect: CGRect) -> Bool { // Dark logic
        var result = false
        if let image = capturedImage {
            let values = getBrightnessOutSideBoundingBox(originalImage: image, objectImageRect: croppedImageRect)
            debugPrint("dark values \(values)")
            logger?.log(errorString: "dark values \(values)")
            if values.left < requiredMinLeftRightBrighteness
                || values.right < requiredMinLeftRightBrighteness
                || values.top < requiredMinTopBottomBrighteness
                || values.bottom < requiredMinTopBottomBrighteness  {
                result = true
            }
            logger?.mirrorTestFBLogData["dark"] = ["result": result, "left": values.left,"right": values.right, "top": values.top, "bottom": values.bottom]
        }
        return result
    }
    
    private func getBrightnessOutSideBoundingBox(originalImage: UIImage, objectImageRect: CGRect) -> (left: Double,right: Double,top: Double, bottom: Double) {
        let stripeSize = CGFloat(requiredStripSize)
        let strippedSize = Int(objectImageRect.width * stripeSize)
        
        let leftBoxImageBrightness = getLeftBoxImageBrightness(originalImage: originalImage, objectImageRect: objectImageRect, strippedSize: strippedSize)
        
        let rightBoxImageBrightness = getRightBoxImageBrightness(originalImage: originalImage, objectImageRect: objectImageRect, strippedSize: strippedSize)
        
        let topBoxImageBrighness = getTopBoxImageBrightness(originalImage: originalImage, objectImageRect: objectImageRect, strippedSize: strippedSize)
        
        let bottomBoxImageBrightness = getBottomBoxImageBrightness(originalImage: originalImage, objectImageRect: objectImageRect, strippedSize: strippedSize)
        
        return (left: leftBoxImageBrightness, right: rightBoxImageBrightness,top: topBoxImageBrighness, bottom: bottomBoxImageBrightness)
    }
    
    func doOCROnRecognizedTexts(recognizedTexts: [String]? = []) -> (leftRightOCR: Bool, imeiOCR: Bool) {
        var result = false
        var imeiVarified = false
        if let texts = recognizedTexts, let imei = primaryIMEI?.description {
            let imeiSubTextToCompare = imei.prefix(digitsComparisonForIMEIocr)
            imeiVarified = texts.filter({$0.contains(imeiSubTextToCompare)}).first != nil
            
            if texts.contains(LeftLabelText), texts.contains(RightLabelText), imeiVarified {
                    result = true
            }
            logger?.mirrorTestFBLogData["ocr"] = ["imeiDetected": imeiVarified, "leftTextDetected": texts.contains(LeftLabelText), "rightTextDetected": texts.contains(RightLabelText), "ocrText": (recognizedTexts ?? []).joined(separator: "|")]
        }
        return (leftRightOCR: (imeiVarified && result), imeiOCR: imeiVarified)
    }
    
    func getDisplayableHashKey(with hashKey: String?) -> String? {
        if let imei = primaryIMEI {
            if let hashKey = hashKey { // if new hash key came reset and generate hash key to show
                lastIMEIandKeyAddition = imei
                serverKey = Int64(hashKey)
            }
            if let key = serverKey {
                lastIMEIandKeyAddition += key
                let octalString = String(lastIMEIandKeyAddition,radix: 8) // get octal representation
                // add padding
                let stringToShow = octalString.length < hashKeyMaxLength ? octalString.leftPadding(toLength: Int(hashKeyMaxLength), withPad: "0") : octalString
                return stringToShow
            }
        }
        return nil
    }
}

// MARK: - Helper Methods
extension MirrorTestManager {
    func getLeftBoxImageBrightness(originalImage: UIImage, objectImageRect: CGRect, strippedSize: Int) -> Double {
        let newX = -strippedSize + Int(objectImageRect.minX)
        if newX >= 0 {
            return originalImage.cropImage(rect: CGRect(x: newX, y: Int(objectImageRect.minY), width: strippedSize, height: Int(objectImageRect.height))).brightness
        }
        return 220 // default brightness
    }
    func getRightBoxImageBrightness(originalImage: UIImage, objectImageRect: CGRect, strippedSize: Int) -> Double {
        if (objectImageRect.maxX + CGFloat(strippedSize)) <= originalImage.size.width {
            return originalImage.cropImage(rect: CGRect(x: Int(objectImageRect.maxX), y: Int(objectImageRect.minY), width: strippedSize, height: Int(objectImageRect.height))).brightness
        }
        return 220
    }
    func getTopBoxImageBrightness(originalImage: UIImage, objectImageRect: CGRect, strippedSize: Int) -> Double {
        let newY = Int(objectImageRect.minY) - strippedSize
        if newY >= 0 {
            return originalImage.cropImage(rect: CGRect(x: Int(objectImageRect.minX), y: newY, width: Int(objectImageRect.width), height: strippedSize)).brightness
        }
        return 220
    }
    func getBottomBoxImageBrightness(originalImage: UIImage, objectImageRect: CGRect, strippedSize: Int) -> Double {
        if (CGFloat(strippedSize) + objectImageRect.maxY) <= originalImage.size.height {
            return originalImage.cropImage(rect: CGRect(x: Int(objectImageRect.minX), y: Int(objectImageRect.maxY), width: Int(objectImageRect.width), height: strippedSize)).brightness
        }
        return 220
    }
}
