//
//  TFLiteModelsDownloadManager.swift
//  ImageClassification
//
//  Created by Chandra Bhushan on 06/05/21.
//  Copyright © 2021 Y Media Labs. All rights reserved.
//

import UIKit
import ReachabilityManager

//MARK: - TFLiteModelsDownloadManagerDelegate
protocol TFLiteModelsDownloadManagerDelegate: AnyObject {
    func onError(error: TFLiteModelsDownloadError)
    func startActivation(for activationCode: String?, with activityRefId: String?, fromScreen:FromScreen, openFromNotification: Bool)
    func hideNoInternetErrorView()
    func onChangeRemainingTime(time: String)
}

//MARK: - TFLiteModelsDownloadManagerDelegate methods default implementation
extension TFLiteModelsDownloadManagerDelegate {
    func onError(error: TFLiteModelsDownloadError) {}
    func startActivation(for activationCode: String?, with activityRefId: String?, fromScreen:FromScreen, openFromNotification: Bool = false) {}
    func hideNoInternetErrorView() {}
    func onChangeRemainingTime(time: String){}
}

// MARK: - TFLiteModelsDownloadError
enum TFLiteModelsDownloadError: String, Error {
    case ModelsLoadingError
    case DownloadTaskError
    case InternetUnavialability
    case PoorInternet
    case RetryExceeded
}

// MARK: - TFLiteModelsDownloadManager
class TFLiteModelsDownloadManager {
    
    //pass the reference of views to update content
    
    private var downloadRemainingTimeTimer: Timer?
    private var checkPoorInternetTimer: Timer?
    private var objectDetectionHandler: ModelDataHandler? = nil
    private var imageClassificationHandler: ModelDataHandler? = nil
    private var deleteModelsAfterDays: Int = 45
    
    weak var progressView: UIProgressView? = nil {
        didSet { // initially set progress without waiting call in delegate method to update progress view
            progressView?.progress = oaDownloadManager.overallProgress()
        }
    }
    weak var estimatedTimeLabel: UILabel?
    weak var delegate: TFLiteModelsDownloadManagerDelegate?
    
    var oaDownloadManager: OADownloadManager = OADownloadManager.sharedInstance
    var activationCode: String?
    var activityRefId: String?
    var currentlyOccuredError: TFLiteModelsDownloadError? // this variable is used to handle if bg downloading is paused due to an error then not resumt tasks in appdelegate when app comes in foreground from background
    var fromScreen: FromScreen = .blank // used when user start activation from post purchase screen
    var pushNotificationIdentifier = "ModelsDownloadSuccess"
    var modelsInfo: MirrorFlowModelsInfoResponseDTO?
    var mirrorTestThresolds: MirrorTestThresoldsResponseDTO?
    var mirrorTestErrorMessages: MirrorTestErrorMessagesResDTO?
    var isInternetGone: Bool = false
    var models: [String: String]?
    var downloadStartTime: TimeInterval?
    
    static var sharedInstance: TFLiteModelsDownloadManager = TFLiteModelsDownloadManager()
    
    private init() {
        
        TFLiteModelsInfoUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.modelsInfo = response
            self.models = [Strings.Global.objectDetectionLabels: self.getModelWithName(name: response?.objectDetectionModelInfo?.labelFileName),
                           Strings.Global.objectDetectionModel: self.getModelWithName(name: response?.objectDetectionModelInfo?.modelFileName),
                           Strings.Global.imageClassificationLabels: self.getModelWithName(name: response?.classificationModelInfo?.labelFileName),
                           Strings.Global.imageClassificationModel: self.getModelWithName(name: response?.classificationModelInfo?.modelFileName)]
        }
        
        MirrorTestThresoldsUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.mirrorTestThresolds = response
        }
        
        MirrorTestErrorMessagesUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.mirrorTestErrorMessages = response
        }
        
        oaDownloadManager.uiDelegate = self
        oaDownloadManager.requestTimeoutSeconds = 30
    
        oaDownloadManager.onConnectionLost = { [weak self] in
            self?.isInternetGone = true
            self?.onError(error: .InternetUnavialability)
        }
        
        oaDownloadManager.onConnectionAppear = { [weak self] in
            self?.isInternetGone = false
            self?.runCheckPoorInternetTimer()
            self?.runDownloadRemainingTimeTimer()
            self?.delegate?.hideNoInternetErrorView()
            UIViewController.forceRemoveAlertView()
        }
    }
    
    private func getModelWithName(name: String?) -> String {
        if let name = name {
            return FileManager.documentsDir().appendingPathComponent(Strings.Global.directoryForTFLiteModals + "/" + name)
        }
        return ""
    }
    
    func startModelDownloading() {
        self.downloadStartTime = Date.currentDate().timeIntervalSince1970
        self.progressView?.progress = 0
        self.estimatedTimeLabel?.text = Strings.Global.pleaseWhileSetupDownloading
        self.currentlyOccuredError = nil
        downloadRemainingTimeTimer?.invalidate()
        checkPoorInternetTimer?.invalidate()
        removeDownloadedModels()
        if let modelsInfo = modelsInfo, let url = modelsInfo.modelURL, let fileName = modelsInfo.fileNameWithExt {
            oaDownloadManager.downloadBatch(downloadInformation: [["url": Constants.SchemeVariables.baseCDNUrl+url+fileName, "destination":Strings.Global.directoryForTFLiteModals+"/"+fileName]])
            runCheckPoorInternetTimer()
        }
    }
    
    func runDownloadRemainingTimeTimer() {
        // Timer is fruitfull only when we need to show remaining time on label and label instance is set
        downloadRemainingTimeTimer?.invalidate()
        downloadRemainingTimeTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateRemainingTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateRemainingTime() {
        if oaDownloadManager.overallProgress() > 0 {
            if let downloadRateAndRemaining = OADownloadManager.sharedInstance.downloadRateAndRemainingTime(){
                let remainingTime: String = downloadRateAndRemaining.remainingTime;
                self.estimatedTimeLabel?.text = String(format: "Time Left: %@", remainingTime)
                self.delegate?.onChangeRemainingTime(time: remainingTime)
            }
        }
    }
    
    func runCheckPoorInternetTimer() {
        let poorInterCheckTime: Double = Double((mirrorTestThresolds?.download?.min_time_to_check_download_speed?.intValue ?? 5000)/1000) //in seconds
        checkPoorInternetTimer?.invalidate()
        checkPoorInternetTimer = Timer.scheduledTimer(timeInterval: poorInterCheckTime, target: self, selector: #selector(checkInternetSpeed), userInfo: nil, repeats: true)
    }
    
    @objc private func checkInternetSpeed() {
        if let downloadRateAndRemaining = OADownloadManager.sharedInstance.downloadRateAndRemainingTime(), let maxDownloadTime = mirrorTestThresolds?.download?.model_download_threshold_time_in_ms {
            let maxDownloadTimeInSeconds: Int64 = Int64(maxDownloadTime.int64Value/1000)
            if (downloadRateAndRemaining.remainingTimeInSeconds) > maxDownloadTimeInSeconds {
                oaDownloadManager.suspendAllOngoingDownloads()
                self.onError(error: .PoorInternet)
            }
        }
    }
    
    func isModelsDownloaded() -> Bool {
        var isDownloaded = true
        guard let models = models, models.count > 0 else {
            return false
        }
        
        for (_,value) in models {
            if !FileManager.default.fileExists(atPath: value) {
                isDownloaded = false
                break
            }
        }
        return isDownloaded
    }
    
    func checkModelDeletionTimeExceeded() {
        if let date: Date = UserDefaultsKeys.modelsDownloadedOn.get(), let daysDifference = Date().days(from: date), daysDifference > deleteModelsAfterDays {
            TFLiteModelsDownloadManager.sharedInstance.removeDownloadedModels()
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.modelsDownloadedOn.rawValue)
        }
    }
    
    @discardableResult
    func removeDownloadedModels() -> Bool {
        let fileManager = FileManager.default
        let tfliteModelsContainerDir = FileManager.documentsDir().appendingPathComponent(Strings.Global.directoryForTFLiteModals)
        var isRemoved = true
        var isDir: ObjCBool = false
        if fileManager.fileExists(atPath: tfliteModelsContainerDir, isDirectory: &isDir) {
            if isDir.boolValue {
                do {
                    try fileManager.removeItem(atPath: tfliteModelsContainerDir)
                    isRemoved = true
                } catch {
                    isRemoved = false
                    print("error while removing TFLiteModels Directory")
                }
            }
        }
        return isRemoved
    }
    
    private func checkModelsCouldLoad() -> Bool {
        var result = true
        if let objectDetectionTFLite = models?[Strings.Global.objectDetectionModel], let objectDetectionLabels = models?[Strings.Global.objectDetectionLabels], let imageClassificationTFLite = models?[Strings.Global.imageClassificationModel], let imageClassificationLabels = models?[Strings.Global.imageClassificationLabels] {
            
            objectDetectionHandler = ModelDataHandler(modelFileInfo: (path: objectDetectionTFLite, fileType: "tflite"), labelsFileInfo: (path: objectDetectionLabels, fileType: "txtfile"), inputWidth: 300, inputHeight: 300)
            
            imageClassificationHandler = ModelDataHandler(modelFileInfo: (path: imageClassificationTFLite, fileType: "tflite"), labelsFileInfo: (path: imageClassificationLabels, fileType: "txtfile"), inputWidth: 224, inputHeight: 224)
            
            guard imageClassificationHandler != nil,
                  objectDetectionHandler != nil else {
                objectDetectionHandler = nil
                imageClassificationHandler = nil
                return false
            }
            result = true
        }
        objectDetectionHandler = nil
        imageClassificationHandler = nil
        return result
    }
    
    func fireDownloadSuccessPushNotification() {
        UserDefaultsKeys.modelsDownloadedOn.set(value: Date())
        if UIApplication.shared.applicationState == .background {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                DispatchQueue.main.async {
                    if settings.authorizationStatus == .authorized {
                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [self.pushNotificationIdentifier])
                        
                        let content = UNMutableNotificationContent()
                        content.title = Strings.Global.downloadSuccessPushHeading
                        content.body = Strings.Global.downloadSuccessPushDescription
                        content.sound = UNNotificationSound.default
                        // show this notification zero seconds from now
                        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                        // choose a random identifier
                        let request = UNNotificationRequest(identifier: self.pushNotificationIdentifier, content: content, trigger: trigger)
                        // when user tapped on local notification for download completion
                        
                        sharedAppDelegate?.onDownloadingCompleteNotification = { [weak self] in
                            EventTracking.shared.eventTracking(name: .activationPushClicked, [.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
                            self?.performOpsPostDownload(openFromNotification: true)
                        }
                        // add our notification request
                        UNUserNotificationCenter.current().add(request)
                        EventTracking.shared.eventTracking(name: .activationCompletionPushSent, [.location: "Mobile", .mode: FDCacheManager.shared.fdTestType.rawValue])
                        print("Fire push notification for download success in background")
                    } else {
                        self.performOpsPostDownload()
                    }
                }
            }
        } else {
            performOpsPostDownload()
        }
    }
    
    func resetEverything() {
        progressView = nil
        estimatedTimeLabel = nil
        downloadRemainingTimeTimer = nil
        checkPoorInternetTimer = nil
        activationCode = nil
        activityRefId = nil
        fromScreen = .blank
        currentlyOccuredError = nil
        resetRetryCount()
    }
    
    func performOpsPostDownload(openFromNotification: Bool = false) {
        let downloadTime = Int(Date.currentDate().timeIntervalSince1970 - (downloadStartTime ?? 0)) * 1000 // in ms
        EventTracking.shared.eventTracking(name: .downloadComplete, [.mode: FDCacheManager.shared.fdTestType.rawValue, .time: downloadTime])
        if checkIfModelCouldLoad() {
            delegate?.startActivation(for: activationCode, with: self.activityRefId, fromScreen: self.fromScreen, openFromNotification: openFromNotification)
            resetEverything()
        } else {
            self.onError(error: .ModelsLoadingError)
        }
    }
    
    func checkIfModelCouldLoad() -> Bool {
        var result = false
        let retryCount: Int = mirrorTestThresolds?.download?.no_of_retry_for_model_loading?.intValue ?? 1
        for _ in 0...retryCount {
            if checkModelsCouldLoad() {
                result = true
                break
            }
        }
        return result
    }
   
    func onError(error: TFLiteModelsDownloadError) {
        currentlyOccuredError = error
        switch error {
        case .DownloadTaskError, .PoorInternet:
            saveRetryCount()
        default:
            break
        }
        self.checkPoorInternetTimer?.invalidate()
        downloadRemainingTimeTimer?.invalidate()
        if let _ = self.activationCode, checkIfRetryCountExceededFor() {
            // cancel ongoing downloads
            currentlyOccuredError = .RetryExceeded
            self.delegate?.onError(error: .RetryExceeded)
            self.oaDownloadManager.cancelAllOutStandingTasks()
            self.oaDownloadManager.updateBatchCompleteStatus() // mark batch as completed with error forcefully
            return
        }
        self.delegate?.onError(error: error)
    }
    
    private func getRetryCount() -> Int {
        return UserDefaults.standard.integer(forKey: UserDefaultsKeys.modelsSetupRetryCount.rawValue)
    }
    
    private func saveRetryCount() {
        let value = getRetryCount() + 1
        UserDefaultsKeys.modelsSetupRetryCount.set(value: value)
    }
    
    func resetRetryCount() {
        UserDefaultsKeys.modelsSetupRetryCount.set(value: 0)
    }
    
    func checkIfRetryCountExceededFor() -> Bool {
        var result = false
        if let maxRetry = mirrorTestThresolds?.download?.no_of_retries_for_slow_internet, getRetryCount() > maxRetry.intValue {
            result = true
        }
        return result
    }
}

extension TFLiteModelsDownloadManager: OADownloadManagerUIDelegate {
    
    func didReachProgress(progress:Float) {
        currentlyOccuredError = nil
        self.progressView?.progress = progress
    }
    
    func didFinishAll() {
        self.progressView?.progress = 1
        self.estimatedTimeLabel?.text = ""
        downloadRemainingTimeTimer?.invalidate()
        checkPoorInternetTimer?.invalidate()
        fireDownloadSuccessPushNotification()
    }
    
    /*
     if condition added to check if already retry exceeded error thrown, and tasks are canceled then
     level error would not be considered as current error usually cancelled task error
     */
    func didHitDownloadErrorOnTask(task: OADownloadTask) {
        oaDownloadManager.cancelAllOutStandingTasks()
        downloadRemainingTimeTimer?.invalidate()
        checkPoorInternetTimer?.invalidate()
        if currentlyOccuredError != .RetryExceeded {
            self.onError(error: .DownloadTaskError)
        }
    }
    
    func didStartDownloading() {
        runDownloadRemainingTimeTimer()
    }
    
    func didFinishBackgroundDownloading() {
//       fireDownloadSuccessPushNotification()
    }
    
    func handleRetry(with error: Error?) {
        if let error = error as? TFLiteModelsDownloadError {
            self.runDownloadRemainingTimeTimer()
            switch error {
            case .DownloadTaskError:
                self.startModelDownloading()
            case .PoorInternet:
                self.oaDownloadManager.continueIncompletedDownloads()
                self.runCheckPoorInternetTimer()
            default:
                break
            }
        }
    }
}



