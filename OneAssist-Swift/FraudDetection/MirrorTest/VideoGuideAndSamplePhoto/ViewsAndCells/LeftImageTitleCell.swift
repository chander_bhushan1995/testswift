//
//  LeftImageTitleCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 19/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class LeftImageTitleCellVM {
    var title:String?
    var image: UIImage?
    init(title: String?, image: UIImage?){
        self.title = title
        self.image = image
    }
}

class LeftImageTitleCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.font = DLSFont.h3.bold
        titleLabel.textColor = UIColor.seaGreen
    }

    func setupModel(_ model: ImageWithValue?) {
        logoImageView.image = UIImage(named: model?.image ?? "")
        titleLabel.text = model?.value
    }
    
    func setViewModel(_ model: LeftImageTitleCellVM?) {
        logoImageView.image = model?.image
        titleLabel.text = model?.title
    }
    
    class func heightForBottomSheetModel(_ model: ImageWithValue?, totalWidth: CGFloat) -> CGFloat {
        let widthConstraints: CGFloat = 32
        let collectiveHeigthConstains: CGFloat = 34
        let titleHeight = model?.value?.height(withWidth: totalWidth - widthConstraints, font: DLSFont.h3.bold) ?? 0.0
        var imageHeight = UIImage(named: model?.image ?? "")?.size.height ?? 65.0
        if imageHeight > 65.0 {
            imageHeight = 65.0
        }
        return collectiveHeigthConstains + titleHeight + imageHeight
    }
    
    class func heightForBottomSheetModel(_ model: LeftImageTitleCellVM?, totalWidth: CGFloat) -> CGFloat {
        let widthConstraints: CGFloat = 32
        let collectiveHeigthConstains: CGFloat = 34
        let titleHeight = model?.title?.height(withWidth: totalWidth - widthConstraints, font: DLSFont.h3.bold) ?? 0.0
        var imageHeight = model?.image?.size.height ?? 65.0
        if imageHeight > 65.0 {
            imageHeight = 65.0
        }
        return collectiveHeigthConstains + titleHeight + imageHeight
    }
}
