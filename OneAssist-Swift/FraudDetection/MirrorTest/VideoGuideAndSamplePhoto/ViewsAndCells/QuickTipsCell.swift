//
//  QuickTipsCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 19/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

class QuickTipsCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightTextLabel: BodyTextRegularBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setViewModel(model: ImageWithValue) {
        leftImageView.setImageWithUrlString(model.image, placeholderImage: UIImage(named: "assets_placeholder"), completionHandler: nil)
        rightTextLabel.text = model.value
    }
    
    class func heightForBottomSheetModel(_ model: ImageWithValue?, totalWidth: CGFloat) -> CGFloat {
        let widthConstraints: CGFloat = 46
        let collectiveHeigthConstains: CGFloat = 25
        let titleHeight = model?.value?.height(withWidth: totalWidth - widthConstraints, font: DLSFont.h3.bold) ?? 0.0
        var imageHeight: CGFloat = 62
        
        return collectiveHeigthConstains + titleHeight + imageHeight
    }

}
