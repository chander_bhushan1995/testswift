//
//  VideoGuideTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol VideoGuideTableCellDelegate: class {
    func onClickPlayVideo(videoUrl: String?)
    func onClickViewSamplePhotos()
}

class VideoGuideTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView  {
    
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var samplePhotoText: UILabel!
    @IBOutlet weak var videoGuideImageView: UIImageView!
    @IBOutlet weak var videoGuideView: UIView!
    @IBOutlet weak var videoPlayButton: UIButton!
    @IBOutlet weak var buttonTextLabel: UILabel!
    
    weak var delegate: VideoGuideTableCellDelegate?
    var videoModel: VideoGuide?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonTextLabel.textColor = UIColor.white
        videoGuideView.clipsToBounds = true
        videoGuideView.layer.cornerRadius = 4.0
        samplePhotoText.textColor = .buttonTitleBlue
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabel(tap:)))
        samplePhotoText.addGestureRecognizer(tap)
        samplePhotoText.isUserInteractionEnabled = true
    }
    
    func setupModel(_ model: VideoGuide?) {
        videoModel = model
        self.titleLable.text = model?.title
        self.videoGuideImageView.image = UIImage(named: "videoGuideImage")
        self.buttonTextLabel.text = model?.buttonText
        samplePhotoText.isHidden = !(model?.showSamplePhoto?.boolValue ?? false)
    }
    
    //MARK:- Button Action
    @IBAction func onClickPlayVideoAction(_ sender: UIButton?){
        self.delegate?.onClickPlayVideo(videoUrl: videoModel?.user_guide_video_url)
    }
    
    @objc func tapLabel(tap: UITapGestureRecognizer) {
        self.delegate?.onClickViewSamplePhotos()
    }
}
