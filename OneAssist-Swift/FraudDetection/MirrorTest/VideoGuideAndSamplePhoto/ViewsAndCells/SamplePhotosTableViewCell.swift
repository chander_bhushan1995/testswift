//
//  SamplePhotosTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 14/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class SamplePhotosTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var headerTitle: BodyTextBoldBlackLabel!
    @IBOutlet weak var listView: ListView!
    @IBOutlet weak var listViewContainet: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var imageviewHeightConstraint: NSLayoutConstraint!
    
    var refreshCellLayout: (()->())? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setViewModel(model: Facts?, refreshCellLayout: (()->())? = nil){
        self.refreshCellLayout = refreshCellLayout
        self.imageview.image = nil
        if let imageUrl = model?.imageUrl {
           self.imageviewHeightConstraint.constant = 200
           imageview.setImageWithUrlString(imageUrl,placeholderImage: UIImage(named: "assets_placeholder")){ [weak self] image in
                guard let self = self else { return }
                if let downLoadImage = image {
                    let ratio = downLoadImage.size.width / downLoadImage.size.height
                    let newHeight = (UIScreen.main.bounds.width-32) / ratio
                    self.imageviewHeightConstraint.constant = newHeight                    
                }
                self.refreshCellLayout?()
            }
            self.imageview.isHidden = false
        }else {
            self.imageviewHeightConstraint.constant = 0.0
            self.imageview.isHidden = true
        }
        
        headerTitle.text = model?.title
        headerTitle.isHidden = !((headerTitle.text?.isEmpty) != nil)
        if let points = model?.points, !points.isEmpty {
           var listModel = ListViewModel()
            listModel.listFont = DLSFont.supportingText.regular
           listModel.list = points
           listView.setViewModel(model: listModel)
           listViewContainet.isHidden = false
        } else {
           listViewContainet.isHidden = true
        }
       
    }
    
}
