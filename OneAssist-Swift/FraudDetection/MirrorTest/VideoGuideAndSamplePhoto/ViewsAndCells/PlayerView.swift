//
//  PlayerView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 14/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import AVFoundation

fileprivate var playerViewControllerKVOContext = 0
fileprivate let assetKeysRequiredToPlay = [
    "playable",
    "hasProtectedContent"
]
fileprivate let seekDuration: Float64 = 5

class PlayerView: UIView {
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }

    @objc var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    private var currentTime: Double {
        get {
            return CMTimeGetSeconds(player?.currentTime() ?? CMTime.zero)
        }
        set {
            let newTime = CMTimeMakeWithSeconds(newValue, preferredTimescale: 1)
            player?.seek(to: newTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        }
    }
    
    private var duration: Double {
        guard let currentItem = player?.currentItem else { return 0.0 }
        return CMTimeGetSeconds(currentItem.duration)
    }
    
    private let timeRemainingFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.zeroFormattingBehavior = .pad
        formatter.allowedUnits = [.minute, .second]
        
        return formatter
    }()
    
    private var timeObserverToken: Any?
    private var playerItem: AVPlayerItem?
    
    var timeElapsedBlock: ((Float) -> Void)? = nil
    var hasValidDurationBlock: ((Bool) -> Void)? = nil
    var newDurationSecondsBlock: ((Float) -> Void)? = nil
    var currentTimeBlock: ((Float) -> Void)? = nil
    var playButtonBlock: ((Int) -> Void)? = nil
    
    deinit {
        if let timeObserverToken = timeObserverToken {
            player?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
        
        self.player?.removeObserver(self, forKeyPath: "status")
        self.player?.removeObserver(self, forKeyPath: "timeControlStatus")
        self.playerItem?.removeObserver(self, forKeyPath: "duration")
        print("deinit of PlayerView")
    }
    
    private func setUpAsset(with url: URL, completion: ((_ asset: AVAsset) -> Void)?) {
        let asset = AVAsset(url: url)
        asset.loadValuesAsynchronously(forKeys: assetKeysRequiredToPlay) {[weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                for key in assetKeysRequiredToPlay {
                    var error: NSError?
                    if asset.statusOfValue(forKey: key, error: &error) == .failed {
                        let stringFormat = NSLocalizedString("error.asset_key_%@_failed.description", comment: "Can't use this AVAsset because one of it's keys failed to load")

                        let message = String.localizedStringWithFormat(stringFormat, key)
                        
                        self.handleErrorWithMessage(message, error: error)
                        
                        return
                    }
                }
                // We can't play this asset.
                if !asset.isPlayable || asset.hasProtectedContent {
                    let message = NSLocalizedString("error.asset_not_playable.description", comment: "Can't use this AVAsset because it isn't playable or has protected content")
                    self.handleErrorWithMessage(message)
                    return
                }
                
                completion?(asset)
            }
        }
    }
    
    private func setUpPlayerItem(with asset: AVAsset) {
        playerItem = AVPlayerItem(asset: asset)
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.player = AVPlayer(playerItem: self.playerItem!)
            self.player?.isMuted = false
            self.player?.addObserver(self, forKeyPath: "status", options: [.old, .new], context: &playerViewControllerKVOContext)
            self.player?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: &playerViewControllerKVOContext)
            self.playerItem?.addObserver(self, forKeyPath: "duration", options: [.old, .new], context: &playerViewControllerKVOContext)
            
            let interval = CMTimeMake(value: 1, timescale: 1)
            self.timeObserverToken = self.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [weak self] time in
                guard let self = self else { return }
                self.timeElapsedBlock?(Float(CMTimeGetSeconds(time)))
            }
        }
    }
    
    func play(with url: URL) {
        setUpAsset(with: url) { [weak self] (asset: AVAsset) in
            guard let self = self else { return }
            self.setUpPlayerItem(with: asset)
        }
    }
    
    func playPausePressed() {
        if player?.rate != 1.0 {
             if currentTime == duration {
                currentTime = 0.0
            }
            player?.play()
        }
        else {
            player?.pause()
        }
    }
    
    func rewindPressed() {
        var newTime = currentTime - seekDuration
        if newTime < 0 {
            newTime = 0
        }
        currentTime = newTime
    }
    
    func fastForwardPressed() {
        let newTime = currentTime + seekDuration

        if newTime < duration {
            currentTime = newTime
        }
    }

    func timeSliderDidChange(_ sender: UISlider) {
        currentTime = Double(sender.value)
    }
        
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // Only handle observations for the playerItemContext
        guard context == &playerViewControllerKVOContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
            
        if keyPath == "duration" {
            let newDuration: CMTime
            if let newDurationAsValue = change?[NSKeyValueChangeKey.newKey] as? NSValue {
                newDuration = newDurationAsValue.timeValue
            }
            else {
                newDuration = CMTime.zero
            }

            let hasValidDuration = newDuration.isNumeric && newDuration.value != 0
            let newDurationSeconds = hasValidDuration ? CMTimeGetSeconds(newDuration) : 0.0
            let currentTime = hasValidDuration ? Float(CMTimeGetSeconds(player?.currentTime() ?? CMTime.zero)) : 0.0
            
            self.hasValidDurationBlock?(hasValidDuration)
            self.newDurationSecondsBlock?(Float(newDurationSeconds))
            self.currentTimeBlock?(currentTime)

        } else if keyPath == "timeControlStatus" {
            
            let newStatus: AVPlayer.TimeControlStatus

            if let newStatusAsNumber = change?[NSKeyValueChangeKey.newKey] as? NSNumber {
                newStatus = AVPlayer.TimeControlStatus(rawValue: newStatusAsNumber.intValue)!
            }
            else {
                newStatus = .paused
            }
            
            switch newStatus {
              case .playing:
                self.playButtonBlock?(1)
                break
              default:
                self.playButtonBlock?(0)
            }
            
        } else if keyPath == "status" {
            let newStatus: AVPlayerItem.Status

            if let newStatusAsNumber = change?[NSKeyValueChangeKey.newKey] as? NSNumber {
                newStatus = AVPlayerItem.Status(rawValue: newStatusAsNumber.intValue)!
            }
            else {
                newStatus = .unknown
            }
            
            if newStatus == .readyToPlay {
                player?.play()
                
            }else {
                handleErrorWithMessage(player?.currentItem?.error?.localizedDescription, error:player?.currentItem?.error)
            }
        }
    }
    
    // MARK: - Error Handling

    func handleErrorWithMessage(_ message: String?, error: Error? = nil) {
        print("message - \(String(describing: message)) \n error - \(error.debugDescription)")
    }
    
    // MARK: Convenience
    
    func createTimeString(time: Float) -> String {
        let components = NSDateComponents()
        components.second = Int(max(0.0, time))
        
        return timeRemainingFormatter.string(from: components as DateComponents)!
    }
}


