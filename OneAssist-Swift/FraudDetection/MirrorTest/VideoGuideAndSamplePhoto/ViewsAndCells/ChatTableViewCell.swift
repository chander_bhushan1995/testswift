//
//  ChatTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 20/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol ChatCellDelegate: class {
    func onClickChat()
}

class ChatTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    
    var delegate: ChatCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.clipsToBounds = true
        containerView.backgroundColor = UIColor.buttonBlue.withAlphaComponent(0.07)
        containerView.layer.cornerRadius = 4.0
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor.buttonBlue.withAlphaComponent(0.4).cgColor
        
        chatButton.clipsToBounds = true
        chatButton.backgroundColor = UIColor.buttonBlue.withAlphaComponent(0.07)
        chatButton.layer.cornerRadius = chatButton.bounds.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setViewModel(_ model: ChatCellModel?){
        titleLabel.text = model?.title
    }
    
    @IBAction func onClickChatAction(_ sender: UIButton?) {
        self.delegate?.onClickChat()
    }
    
    class func heightForBottomSheetModel(_ model: ChatCellModel?, totalWidth: CGFloat) -> CGFloat {
        let collectiveHeigthConstains: CGFloat = 12
        return collectiveHeigthConstains + 46
    }

}
