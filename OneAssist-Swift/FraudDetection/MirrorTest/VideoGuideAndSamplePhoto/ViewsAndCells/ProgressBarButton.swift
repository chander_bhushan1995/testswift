//
//  ProgressBarButton.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 15/08/20.
//  Copyright © 2020 Ankur Batham. All rights reserved.
//

import UIKit

struct BarButtonProperty {
    var progressColor = UIColor(red: 0/255, green: 140/255, blue: 245/255, alpha: 1.0)
    var backgroundColor = UIColor.gray
    var textColor = UIColor.white
    var completionText = "Ok"
    var textFont = UIFont.boldSystemFont(ofSize: 10)
    var cornerRadius: CGFloat = 2
    var progressViewMaxWidth = UIScreen.main.bounds.width
}

class ProgressBarButton: UIButton {
    
    var buttonProperty: BarButtonProperty = BarButtonProperty()
    var progress: CGFloat = 0.0
    let progressView: UIView = UIView()
    /**
     Initialize programmaticaly just like you would any other UIButton.
    */
    deinit {
        print("deinit of ProgressBarButton")
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initalize()
       
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initalize()
    }
    
    func setupProperty() {
        layer.cornerRadius = buttonProperty.cornerRadius
         layer.masksToBounds = true
         backgroundColor = buttonProperty.backgroundColor
         
        titleLabel!.textAlignment = .center
        titleLabel!.textColor = buttonProperty.textColor
        titleLabel!.font = buttonProperty.textFont
        progressView.backgroundColor = buttonProperty.progressColor
    }
    
    private func initalize() {
        progressView.frame = CGRect(x: 0, y: 0, width: 0.0, height: frame.height)
        progressView.frame.size.width = 0.0
        progressView.backgroundColor = buttonProperty.progressColor
        progressView.isUserInteractionEnabled = false
        self.addSubview(progressView)
       
       self.bringSubviewToFront(titleLabel!)
        
        setupProperty()
    }
    
    public func setProgress(progress: CGFloat, _ animated: Bool) {
        self.progress = progress
        progressView.frame.size.width = self.progress * buttonProperty.progressViewMaxWidth
    }
    
    /**
        Set title label text.
    */
    public override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
    }
    
    /**
     Needs to be called explicitly when loading determinate as well.
    */
    public func triggerCompletion() {
        self.setProgress(progress: 1.0, true)
        self.setTitle(buttonProperty.completionText, for: .normal)
    }
}
