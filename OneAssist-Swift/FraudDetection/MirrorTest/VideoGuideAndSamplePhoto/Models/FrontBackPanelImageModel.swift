//
//  FrontBackPanelImageModel.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 25/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

enum MirrorImageType: String {
    case FRONT = "FRONT"
    case BACK = "BACK"
}

class FrontBackPanelImageModel {
    var frontPanelImage: SinglePanelImageModel?
    var backPabelIamge: SinglePanelImageModel?
    
    func setupfrontPanel() {
        let frontPanel = SinglePanelImageModel()
        frontPanel.setupModel(.FRONT, title: "FRONT PANEL")
        frontPanelImage = frontPanel
    }
    
    func setupBackPanel() {
        let backPanel = SinglePanelImageModel()
        backPanel.setupModel(.BACK, title: "BACK PANEL")
        backPabelIamge = backPanel
    }
}


class SinglePanelImageModel {
    var title: String?
    var image: UIImage?
    var imageType: MirrorImageType?
    
    func setupModel(_ imageType: MirrorImageType, title: String){
        self.title = title
        self.imageType = imageType
    }
}
