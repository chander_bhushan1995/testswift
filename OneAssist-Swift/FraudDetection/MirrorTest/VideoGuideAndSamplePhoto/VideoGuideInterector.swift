//
//  VideoGuideInterector.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 12/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol VideoGuidePresenterLogic {
    func validateVideoGuideSupportValue(for response: MirrorTestVideoGuideResponseDTO?, error: Error?)
    func validateSamplePhotoValue(for response: MirrorTestSamplePhotoDTO?, error: Error?)
}


class VideoGuideInterector: BaseInteractor {
    var presenter: VideoGuidePresenterLogic?
}


extension VideoGuideInterector: VideoGuideBusinessLogic {
    func loadVideoGuideSupportData() {
        let _ = GetMirrorTestVideoGuideUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
               guard let self = self else { return }
            self.presenter?.validateVideoGuideSupportValue(for: response, error: error)
           }
    }
    
    func loadSamplePhotoData() {
        let _ = GetMirrorTestSamplePhotsUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
               guard let self = self else { return }
             self.presenter?.validateSamplePhotoValue(for: response, error: error)
           }
    }
}
