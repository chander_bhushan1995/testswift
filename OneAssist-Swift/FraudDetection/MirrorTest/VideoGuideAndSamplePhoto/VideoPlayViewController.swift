//
//  VideoPlayViewController.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 14/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import AVFoundation

protocol VideoPlayViewDelegate: class {
    func onClickSkipVideo()
    func onClickContinueAction()
}

extension VideoPlayViewDelegate {
    func onClickSkipVideo(){}
    func onClickContinueAction() {}
}

class VideoPlayViewController: BaseVC {
    
    @IBOutlet weak var skipVideoButton: UIButton!
    @IBOutlet weak var playerContainetView: UIView!
    @IBOutlet weak var playerBottomView: UIView!
    @IBOutlet weak var playVideoView: UIView!
    @IBOutlet weak var progressButton: OAPrimaryButton!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var rewindButton: UIButton!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var fastForwardButton: UIButton!
    
    var videoPlayUrl: String?
    weak var delegate: VideoPlayViewDelegate?
    
    private var playerView: PlayerView!
    
    deinit {
        print("VideoPlayViewController deinit")
        try! AVAudioSession.sharedInstance().setCategory(.soloAmbient, options: [])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        setUpPlayerView()
        setupProgressButton()
        playVideo()
        
        try! AVAudioSession.sharedInstance().setCategory(.playback, options: [])
        // Do any additional setup after loading the view.
    }
    
    
    private func initializeView() {
        playVideoView.clipsToBounds = true
        playVideoView.layer.cornerRadius = 6
        skipVideoButton.setTitleColor(.white, for: .normal)
        timeSlider.setThumbImage(UIImage(named: "slider_thumb"), for: .normal)
        timeSlider.setThumbImage(UIImage(named: "slider_thumb"), for: .highlighted)
        
        rewindButton.setImage(UIImage(named: "rewind_video"), for: .normal)
        rewindButton.setImage(UIImage(named: "rewind_video"), for: .selected)
        
        fastForwardButton.setImage(UIImage(named: "forword_video"), for: .normal)
        fastForwardButton.setImage(UIImage(named: "forword_video"), for: .selected)
        
        playPauseButton.setImage(UIImage(named: "play_video"), for: .normal)
        
        EventTracking.shared.eventTracking(name: .seeVideo, [.mode: FDCacheManager.shared.fdTestType.rawValue])
    }
    
    private func setUpPlayerView() {
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        playerView = PlayerView()
        playerContainetView.addSubview(playerView)
            
        playerView.translatesAutoresizingMaskIntoConstraints = false
        playerView.leadingAnchor.constraint(equalTo: playerContainetView.leadingAnchor).isActive = true
        playerView.trailingAnchor.constraint(equalTo: playerContainetView.trailingAnchor).isActive = true
        playerView.heightAnchor.constraint(equalTo: playerContainetView.heightAnchor).isActive = true
        playerView.widthAnchor.constraint(equalTo: playerContainetView.widthAnchor).isActive = true
        
        playerView.timeElapsedBlock =  {[weak self] value in
            guard let self = self else { return }
            self.timeSlider.value = Float(value)
            self.startTimeLabel.text = self.playerView.createTimeString(time: value)
            self.updateProgressButton()
        }
        
        playerView.hasValidDurationBlock =  {[weak self] value in
            guard let self = self else { return }
            Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
            self.rewindButton.isEnabled = value
            self.playPauseButton.isEnabled = value
            self.fastForwardButton.isEnabled = value
            self.timeSlider.isEnabled = value
            self.startTimeLabel.isEnabled = value
            self.durationLabel.isEnabled = value
            //self.progressButton.isEnabled = value
        }
        
        playerView.currentTimeBlock =  {[weak self]value in
            guard let self = self else { return }
            self.timeSlider.value = value
            self.startTimeLabel.text = self.playerView.createTimeString(time: value)
        }
        
        playerView.newDurationSecondsBlock =  {[weak self]value in
            guard let self = self else { return }
            self.timeSlider.maximumValue = value
            self.durationLabel.text = self.playerView.createTimeString(time: value)
        }
        
        playerView.playButtonBlock = {[weak self] value in
            guard let self = self else { return }
            let image = value == 1 ? UIImage(named: "pause_video"): UIImage(named: "play_video")
            self.playPauseButton.setImage(image, for: .normal)
        }
    }
    
    func setupProgressButton() {
        progressButton.setTitle(Strings.FDetectionConstants.strContinue, for: .normal)
        progressButton.isEnabled = false
    }
    
    func updateProgressButton() {
        let progressValue = CGFloat(self.timeSlider.value/self.timeSlider.maximumValue)
        if progressValue >= 1.0 {
            self.progressButton.isEnabled = true
        }
    }

    func playVideo() {
        guard let url = URL(string: videoPlayUrl ?? "") else { return }
        playerView.play(with: url)
    }
    
    
    //MARK: - UIButton Action
    
    @IBAction func onClickSkipAction(_ sender: UIButton?){
        let progressValue = Int((self.timeSlider.value/self.timeSlider.maximumValue) * 100)
        EventTracking.shared.eventTracking(name: .skipFromSampleVideo, [.mode: FDCacheManager.shared.fdTestType.rawValue, .videoProgress: "\(progressValue)%"])
        self.dismiss(animated: true, completion: nil)
        self.delegate?.onClickSkipVideo()
    }
    
    @IBAction func onClickContunueAction(_ sender: UIButton?){
        let progressValue = Int((self.timeSlider.value/self.timeSlider.maximumValue) * 100)
        EventTracking.shared.eventTracking(name: .continueFromSampleVideo, [.mode: FDCacheManager.shared.fdTestType.rawValue, .videoProgress: "\(progressValue)%"])
        self.dismiss(animated: true, completion: nil)
        self.delegate?.onClickContinueAction()
    }

    @IBAction func playPauseButtonWasPressed(_ sender: UIButton?) {
        playerView.playPausePressed()
    }
       
    @IBAction func rewindButtonWasPressed(_ sender: UIButton) {
        playerView.rewindPressed()
    }
       
    @IBAction func fastForwardButtonWasPressed(_ sender: UIButton) {
        playerView.fastForwardPressed()
    }

    @IBAction func timeSliderDidChange(_ sender: UISlider) {
        playerView.timeSliderDidChange(sender)
    }

}
