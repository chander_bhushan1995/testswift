//
//  VideoGuideViewController.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 12/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

enum VCType: String {
    case VideoGuide = "VideoGuide"
    case SamplePhoto = "SamplePhoto"
}

protocol VideoGuideBusinessLogic {
    func loadVideoGuideSupportData();
    func loadSamplePhotoData();
}

struct VideoGuideScreenVM {
    var heading: String?
    var videoPlayButtonText: String?
    var videoPlayURL: String?
    var dataSource: [AnyObject] = []
}

class VideoGuideViewController: BaseVC, CommonMirrorTestDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var continueButton: OAPrimaryButton!
    @IBOutlet weak var samplePhotoButton: OASecondaryButton!
    
    @IBOutlet weak var bottomView: OAStickyView!
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var headingLabel: H3BoldLabel!
    @IBOutlet weak var playVideoHintLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var videoPlayImageView: UIImageView!
    
    @IBOutlet weak var bottomViewHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondaryButtonHeightConstraint: NSLayoutConstraint!
    
    var interactor: VideoGuideBusinessLogic?
    var viewModel: VideoGuideScreenVM?
    var vcType: VCType = .VideoGuide
    var countLabels = [UIView]()
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationShadow()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        showNavigationShadow()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        sharedAppDelegate?.applicationState()
    }
    
    private func initializeView() {
        var frame = CGRect.zero
        frame.size.height = 20
        tableView.tableHeaderView = UIView(frame: frame)
        
        headerContainerView.layer.masksToBounds = false
        headerContainerView.layer.shadowColor = UIColor.dlsShadow.cgColor
        headerContainerView.layer.shadowOpacity = 0.5
        headerContainerView.layer.shadowOffset = CGSize(width: 1, height: 2)
        headerContainerView.layer.shadowRadius = 1
        
        continueButton.setTitle(Strings.FDetectionConstants.strContinue, for: .normal)
        samplePhotoButton.setTitle(Strings.FDetectionConstants.seeSamplePhoto, for: .normal)
        
        tableView.register(cell: QuickTipsCell.self)
        tableView.register(cell: SamplePhotosTableViewCell.self)
        
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCTestStatusHeaderView)
        
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        if self.vcType == .SamplePhoto {
            EventTracking.shared.eventTracking(name: .seeSamplePhoto, [.mode: FDCacheManager.shared.fdTestType.rawValue])
            self.bottomView.isHidden = true
            self.headerContainerView.isHidden = true
            self.samplePhotoButton.isHidden = true
            self.bottomViewHeightContraints.constant = 0.0
            self.headerViewHeightConstraint.isActive = true
            self.secondaryButtonHeightConstraint.constant = 0.0
            title = Strings.FDetectionConstants.samplePhoto
            interactor?.loadSamplePhotoData()
            
        }else {
            self.headerViewHeightConstraint.isActive = false
            title = Strings.Global.membershipActivation
            interactor?.loadVideoGuideSupportData()
            FDCacheManager.shared.deleteImagesIntoDirectory()
        }
        countLabels.append(addBarButtons())
    }
    
    private func addBarButtons() -> UIView {
        let chatNotificationView = Utilities.getBadgeView1(target: self,image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: #selector(self.chatClicked(_:)))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView)]
        return chatNotificationView
    }
    
    @objc func chatClicked(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .chat, [.location: Event.EventParams.instructionScreen, .mode: FDCacheManager.shared.fdTestType.rawValue])
        present(ChatVC(), animated: true, completion: nil)
    }
    
    //MARK: - IBActions
    @IBAction func onClickSamplePhotoAction(_ sender: UIButton?) {
        self.movetoSamplePhoto()
    }
    
    @IBAction func onClickContinueAction(_ sender: UIButton?) {
        if RemoteConfigManager.shared.mirrorLanguageEnable {
            EventTracking.shared.eventTracking(name: .continueVoiceSupport, [.mode: FDCacheManager.shared.fdTestType.rawValue])
            self.moveToTextToSpeech()
        }else {
            self.moveToMirrorTest()
        }
    }
    
    @IBAction func onClickPlayVideo(_ sender: Any) {
        self.moveToPlayVideo(videoUrl: viewModel?.videoPlayURL)
    }
}

//MARK: - VideoGuideDisplayLogic
extension VideoGuideViewController: VideoGuideDisplayLogic {
    func displayVideoGuideDataSuccess(with viewModel: VideoGuideScreenVM) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.viewModel = viewModel
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
    }
    
    func displayVideoGuideDataFailure(with error: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension VideoGuideViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let section = viewModel?.dataSource as? [GuidePoints] {
            return section.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let section = viewModel?.dataSource[section] as? GuidePoints {
            return section.points?.count ?? 0
        }
        return viewModel?.dataSource.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCTestStatusHeaderView) as! MHCTestStatusHeaderView
        headerView.setViewModel((viewModel?.dataSource[section] as? GuidePoints)?.title)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = viewModel?.dataSource as? [GuidePoints] {
            return 24
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = viewModel?.dataSource[indexPath.section] else { return UITableViewCell()}
        switch section {
        case is GuidePoints:
            let section = section as! GuidePoints
            if let dataSourceObj = section.points?[indexPath.row] {
                let cell: QuickTipsCell = tableView.dequeueReusableCell(indexPath: indexPath)
                cell.setViewModel(model: dataSourceObj)
                return cell
            }
        case is Facts:
            let dataSourceObj = section as! Facts
            let cell: SamplePhotosTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model: dataSourceObj){[weak self] in
                guard let self = self else {return}
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
            return cell
        default: break
        }
        
        
        return UITableViewCell()
    }
}

//MARK: - VideoPlayViewDelegate
extension VideoGuideViewController: VideoPlayViewDelegate {
    
    func onClickSkipVideo(){
        self.onClickContinueAction(nil)
    }

    func onClickContinueAction() {
        self.onClickContinueAction(nil)
    }
}


//MARK: -  Router Logic
extension VideoGuideViewController {
    func movetoSamplePhoto() {
        let vc = VideoGuideViewController()
        vc.vcType = .SamplePhoto
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToTextToSpeech() {
        let vc = TextToSpeechViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToMirrorTest() {
        decideFlow()
    }
    
    func moveToPlayVideo(videoUrl: String?) {
        let vc = VideoPlayViewController()
        vc.videoPlayUrl = videoUrl
        vc.delegate = self
        presentOverFullScreen(vc, animated: true, completion: nil)
    }
}

// MARK:- Configuration Logic
extension VideoGuideViewController {
    fileprivate func setupDependencyConfigurator() {
        let interactor = VideoGuideInterector()
        let presenter = VideoGuidePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
