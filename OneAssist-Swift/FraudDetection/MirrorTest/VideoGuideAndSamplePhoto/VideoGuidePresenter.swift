//
//  VideoGuidePresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 12/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol VideoGuideDisplayLogic: class {
    func displayVideoGuideDataSuccess(with viewModel: VideoGuideScreenVM)
    func displayVideoGuideDataFailure(with error: String?)
}

class VideoGuidePresenter: BasePresenter {
     weak var viewController: VideoGuideDisplayLogic?
    
}

extension VideoGuidePresenter: VideoGuidePresenterLogic {
    func validateVideoGuideSupportValue(for response: MirrorTestVideoGuideResponseDTO?, error: Error?) {
        if error != nil {
            self.viewController?.displayVideoGuideDataFailure(with: error?.localizedDescription)
            
        }else {
            var viewModel = VideoGuideScreenVM()
            if let resposnemodel = response {
                
                viewModel.heading = resposnemodel.videoGuide?.title
                viewModel.videoPlayButtonText = resposnemodel.videoGuide?.buttonText
                viewModel.videoPlayURL = resposnemodel.videoGuide?.user_guide_video_url
                if let guidePoint = resposnemodel.quickTips {
                    viewModel.dataSource.append(guidePoint)
                }
            }
            self.viewController?.displayVideoGuideDataSuccess(with: viewModel)
        }
    }
    
    func validateSamplePhotoValue(for response: MirrorTestSamplePhotoDTO?, error: Error?) {
        if error != nil {
            self.viewController?.displayVideoGuideDataFailure(with: error?.localizedDescription)
            
        }else {
            var viewModel = VideoGuideScreenVM()
            if let resposnemodel = response, let facts = resposnemodel.facts {
                viewModel.dataSource = facts
            }
            self.viewController?.displayVideoGuideDataSuccess(with: viewModel)
        }
    }
}
