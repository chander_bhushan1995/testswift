//
//  MirrorTestPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 20/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

@objc protocol MirrorTestDisplayLogic: AnyObject {
    func displayPhotoClickshelpData(frontPanelNotClicksModel: [AnyObject], frontPanelClicksModel: [AnyObject], backPanelBottomModel: [String]?, videoUrl: String?, hashKey: String?, objectNotDetected: [AnyObject])
    func displayPhotoClickshelpFailure(with error: String?)
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?)
    func showFDTestTypeChangeSuccess()
    @objc optional func updateFDHashKey(_ hashKey: String?)
    @objc optional func showFDHashAPIError(_ error: String?)
}

class MirrorTestPresenter:  BasePresenter {
    weak var viewController: MirrorTestDisplayLogic?
}

extension MirrorTestPresenter: MirrorTestPresenterLogic {
   
    func validateSampleValue(for photoResponse: PhotoClickHelpPointsDTO?, photoError: Error?, videoResponse: MirrorTestVideoGuideResponseDTO?, videoError: Error?, hashKey: FDHashKeyGenerationResDTO?, hashKeyGenerationError: Error?, lastHashCode: String?) {
        
        do {
            if hashKey != nil || hashKeyGenerationError != nil {
                try checkError(hashKey, error: hashKeyGenerationError)
            }
            if photoError != nil  || videoError != nil {
                self.viewController?.displayPhotoClickshelpFailure(with: photoError?.localizedDescription)
            } else {
                let frontPanelNotClicks = getPhotoNotClickedBottomModel(response: photoResponse?.photo_click_help_points?.front_panel)
                let objectNotDetected = getPhotoNotClickedBottomModel(response: photoResponse?.photo_click_help_points?.pre_front_panel)
                let frontPanelClicks = getPhotoClickedBottomModel(photoResponse: photoResponse)
                let backPanelBottomAnimationText = photoResponse?.photo_click_help_points?.back_panel_animated_hints
                let videoUrl = videoResponse?.videoGuide?.user_guide_video_url
                self.viewController?.displayPhotoClickshelpData(frontPanelNotClicksModel: frontPanelNotClicks, frontPanelClicksModel: frontPanelClicks, backPanelBottomModel: backPanelBottomAnimationText, videoUrl: videoUrl, hashKey: lastHashCode ?? hashKey?.data?.hashKey, objectNotDetected: objectNotDetected)
            }
        } catch let error as LogicalError {
            viewController?.showFDHashAPIError?(error.errorDescription!)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
    
    func presentHashKey(_ response: FDHashKeyGenerationResDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            
            // Success case
            viewController?.updateFDHashKey?(response?.data?.hashKey)
        } catch let error as LogicalError {
            viewController?.showFDHashAPIError?(error.errorDescription)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
    
    private func getPhotoNotClickedBottomModel(response: Facts?) -> [AnyObject] {
        var frontPanelNotClicks: [AnyObject] = []
        
        let imageValue = LeftImageTitleCellVM(title: nil, image: UIImage(named: "holdonhand"))
        frontPanelNotClicks.append(imageValue)
        
        
        if let frontPanelpoints = response {
            
            let activateHeader = ActivationHeader(dictionary: [:])
            activateHeader.title = frontPanelpoints.title
            activateHeader.subTitle = frontPanelpoints.subTitle
            frontPanelNotClicks.append(activateHeader)
            
            let facts = Facts(dictionary: [:])
            facts.points = frontPanelpoints.points
            facts.lineSpacing = 20.0
            frontPanelNotClicks.append(facts)
        }
        
        return frontPanelNotClicks
    }
    
    private func getPhotoClickedBottomModel(photoResponse: PhotoClickHelpPointsDTO?) -> [AnyObject] {
        var frontPanelClicked: [AnyObject] = []
        
        let imageValue = LeftImageTitleCellVM(title: "Awesome!", image: UIImage(named: "leftmirroricon"))
        frontPanelClicked.append(imageValue)
        
        let activateHeader = ActivationHeader(dictionary: [:])
        activateHeader.title = photoResponse?.photo_click_help_points?.back_panel?.title
        activateHeader.subTitle = photoResponse?.photo_click_help_points?.back_panel?.subTitle
        frontPanelClicked.append(activateHeader)
        
        photoResponse?.photo_click_help_points?.back_panel?.points?.forEach({frontPanelClicked.append($0)})
            
        return frontPanelClicked
    }
    
    func presentChangedFDType(_ response: BaseResponseDTO?, _ error: Error?, reasonToChange: String?) {
        do {
            try checkError(response, error: error)
            
            // Success case
            EventTracking.shared.eventTracking(name: .moveToSDT, [.mode: FDCacheManager.shared.fdTestType.rawValue, .reason: reasonToChange ?? ""])
            viewController?.showFDTestTypeChangeSuccess()
        } catch let error as LogicalError {
            viewController?.showChangeFDTestTypeError(error.errorDescription!, reasonToChange: reasonToChange)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
}
