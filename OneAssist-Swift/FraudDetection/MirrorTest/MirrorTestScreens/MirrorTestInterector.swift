//
//  MirrorTestInterector.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 20/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol MirrorTestPresenterLogic {
    func validateSampleValue(for photoResponse: PhotoClickHelpPointsDTO?, photoError: Error?, videoResponse: MirrorTestVideoGuideResponseDTO?, videoError: Error?, hashKey: FDHashKeyGenerationResDTO?, hashKeyGenerationError: Error?, lastHashCode: String?)
    func presentChangedFDType(_ response: BaseResponseDTO?,_ error: Error?, reasonToChange: String?)
    func presentHashKey(_ response: FDHashKeyGenerationResDTO?, _ error: Error?)
}

class MirrorTestInterector: BaseInteractor {
    var presenter: MirrorTestPresenterLogic?
    
    var photoClickResponse: PhotoClickHelpPointsDTO?
    var photoClickError: Error?
    
    var videoGuideResponse: MirrorTestVideoGuideResponseDTO?
    var videoGuideError: Error?
    
    var fdHashKeyResponse: FDHashKeyGenerationResDTO?
    var fdHashKeyError: Error?
}

extension MirrorTestInterector: MirrorTestBusinessLogic {
    
    func loadPhotoClickHelpData(_ activityReferenceId: String?, with lastHashCode: String?) {
        
        let group = DispatchGroup()
        
        group.enter()
        let _ = GetPhotoHelpPointsUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            group.leave()
            guard let self = self else { return }
            self.photoClickResponse = response
            self.photoClickError = error
        }
        
        group.enter()
        let _ = GetMirrorTestVideoGuideUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            group.leave()
            guard let self = self else { return }
            self.videoGuideResponse = response
            self.videoGuideError = error
        }
        
        if let _ = activityReferenceId, lastHashCode == nil {
            group.enter()
            let request = FDHashKeyGenerationReqDTO()
            request.activityReferenceId = activityReferenceId
            FDHashKeyGenerationUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
                guard let self = self else { return }
                self.fdHashKeyError = error
                self.fdHashKeyResponse = response
                group.leave()
            }
        }
        
        group.notify(queue: .main) {[weak self] in
            guard let self = self else { return }
            self.presenter?.validateSampleValue(for: self.photoClickResponse, photoError: self.photoClickError, videoResponse: self.videoGuideResponse, videoError: self.videoGuideError, hashKey: self.fdHashKeyResponse, hashKeyGenerationError: self.fdHashKeyError, lastHashCode: lastHashCode)
        }
    }
    
    func getHashKey(activityReferenceId: String?) {
        let request = FDHashKeyGenerationReqDTO()
        request.activityReferenceId = activityReferenceId
        FDHashKeyGenerationUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.presentHashKey(response, error)
        }
    }
    
    func changeFDFlowType(testType: FDTestType, activityReferenceId: String?, reasonToChange: String?) {
        let request = FDTestTypeChangeRequestDTO(modelType: testType.getAPIValue(), reasonValue: reasonToChange, activityReferenceId: activityReferenceId)
        FDTestTypeChangeUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.presentChangedFDType(response, error, reasonToChange: reasonToChange)
        }
    }
}
