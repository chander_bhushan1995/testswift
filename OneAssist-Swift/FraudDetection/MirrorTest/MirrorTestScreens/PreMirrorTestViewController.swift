//
//  PreMirrorTestViewController.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 19/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit
import AVFoundation

//MARK: PreMirrorTestDelegate Methods
protocol PreMirrorTestDelegate: NSObject {
    func onObjectDetected()
    func redirectToTextToSpeechScreen()
}

class PreMirrorTestViewController: BaseVC, HideNavigationProtocol {
    
    // MARK: - OUTLETS
    @IBOutlet weak var cameraPreview: UIView!
    @IBOutlet weak var errorLabel: H1BoldLabel!
    @IBOutlet weak var overflowMenuButon: UIButton!
    
    // MARK: - Lazy Loadings
    private lazy var cameraManager = OACameraManeger()
    private lazy var textTospeech = OATextToSpeech()
    private lazy var mirrorTestManager: MirrorTestManager? = MirrorTestManager(.objectDetection)
    
    // MARK: - PRIVATE VARIABLE
    private let objectDetectionQueue = DispatchQueue(label: "objectDetectionQueue")
    private var enableVoiceSupport: Bool = false
    private var isReadyForNextFrame: Bool = false
    private var preMirrorTestRetryCount: Int?
    private var activityRefId: String? = FDCacheManager.shared.activityRefId
    private var thresolds = TFLiteModelsDownloadManager.sharedInstance.mirrorTestThresolds
    private var deviceDetectionTimer: Timer?
    private var deviceNotDetectedTimer: Timer?
    private var nextFrameAfterError: TimeInterval = 0.1
    private var objectNotDetectedInTime: TimeInterval?
    private var objectDetectionWork: DispatchWorkItem?
    private var preMirrorTestLogger: OALogger?
    private var startTextToSpeech: String = "" {
        didSet {
            if !startTextToSpeech.isEmpty, let selectedLanguage = self.language, enableVoiceSupport {
                textTospeech.preferredVoiceLanguageCode = self.languageCode
                textTospeech.speechSynthesizerDelegate = self
                var languageCase: LanguageCase? = nil
                if startTextToSpeech      == "CASE_1" { languageCase = selectedLanguage.CASE_1 }
                else if startTextToSpeech == "CASE_2" { languageCase = selectedLanguage.CASE_2 }
                else if startTextToSpeech == "CASE_6" { languageCase = selectedLanguage.CASE_6 }
                else if startTextToSpeech == "CASE_7" { languageCase = selectedLanguage.CASE_7 }
                if let cases = languageCase {
                    textTospeech.delay = TimeInterval(exactly: cases.delay_iOS) ?? 0
                    textTospeech.text = cases.text ?? ""
                    textTospeech.speak()
                }
            }
        }
    }
    
    // MARK: - FILE PRIVATE VARIABLE
    fileprivate var languageCode: String?
    fileprivate var language: Language?
    fileprivate var videoUrl: String?
    fileprivate var interactor: MirrorTestBusinessLogic?
    fileprivate var frontPanelRetryData: [AnyObject] = []
    
    // MARK: - PUBLIC VARIABLE
    weak var delegate: PreMirrorTestDelegate?
    var isRetake: Bool = false
    
    // MARK: - Vc & View LIFECYCLE CALLS
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cameraManager.updatePreviewFrame()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopRunning()
        resetAll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("disappear called")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let vc = Utilities.topMostPresenterViewController
            if let _ = vc as? PreMirrorTestViewController {
                self.setupFrontPanelTimers()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameraPreview.isHidden = true
        cameraPreview.layer.cornerRadius = 8
        cameraPreview.clipsToBounds = true
        cameraManager.captureOuputFor = .AVCaptureVideoDataOutput
        cameraManager.delegate = self
        preMirrorTestLogger = OALogger(initiatedFor: .PreMirrorTest)
        mirrorTestManager?.logger = preMirrorTestLogger
        objectNotDetectedInTime = (thresolds?.preview_obj_detection?.max_ms_for_object_detect?.doubleValue ?? 1000)/1000
        preMirrorTestRetryCount = thresolds?.preview_obj_detection?.bottom_sheet_retry_count?.intValue
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        self.interactor?.loadPhotoClickHelpData(nil, with: nil)
        EventTracking.shared.eventTracking(name: .mobileDetection, [.mode: FDCacheManager.shared.fdTestType.rawValue, .voice: FDCacheManager.shared.selectLanguageForMirrorTest != nil ? "Enabled" : "Disabled" ])
    }
    
    // MARK: - INSTANCE METHODS
    private func setupCameraManeger(with position: AVCaptureDevice.Position? = .back){
        do {
            try cameraManager.captureSetup(in: cameraPreview, withPosition: position)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func resetAll() {
        objectDetectionWork?.cancel()
        stopAllTimer()
        textTospeech.stopSpeech()
    }
    
    private func setupTextToSpeech() {
        if let langauge = FDCacheManager.shared.selectLanguageForMirrorTest {
            languageCode = langauge.languageCode
            language = langauge.data
            enableVoiceSupport = true
        }
    }
    
    // MARK: - IBActions
    @IBAction func onClickCross(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onClickOverflowMenu(_ sender: Any) {
        self.stopAllTimer()
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = self.overflowMenuButon // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        
        var dataSource: [String] = []
        dataSource.append(Strings.iOSFraudDetection.seeVideoGiude)
        if RemoteConfigManager.shared.mirrorLanguageEnable {
            if self.language != nil {
                if enableVoiceSupport {
                    dataSource.append(Strings.iOSFraudDetection.changelanguage)
                    dataSource.append(Strings.iOSFraudDetection.disablevoice)
                }else {
                    dataSource.append(Strings.iOSFraudDetection.enablevoice)
                }
                
            }else {
                dataSource.append(Strings.iOSFraudDetection.enablevoice)
            }
        }
        
        dropDown.dataSource = dataSource
        
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else {return}
            print("Selected item: \(item) at index: \(index)")
            self.textTospeech.stopSpeech()
            switch item {
            case Strings.iOSFraudDetection.seeVideoGiude:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.seeVideoGiude, .screen: Event.EventParams.mobileDetectionScreen])
                self.enableVoiceSupport = false
                self.routeToVideoGuide()
                break
            case Strings.iOSFraudDetection.changelanguage:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.changelanguage, .screen: Event.EventParams.mobileDetectionScreen])
                self.routeToChangeLanguage()
                break
            case Strings.iOSFraudDetection.enablevoice:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.enablevoice, .screen: Event.EventParams.mobileDetectionScreen])
                self.routeToChangeLanguage()
                break
            case Strings.iOSFraudDetection.disablevoice:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.disablevoice, .screen: Event.EventParams.mobileDetectionScreen])
                FDCacheManager.shared.selectLanguageForMirrorTest = nil
                self.enableVoiceSupport = false
                self.setupFrontPanelTimers()
                break
            default:
                break
            }
        }
        
        dropDown.cancelAction = { [weak self] in
            guard let self = self else {return}
            self.setupFrontPanelTimers()
        }
        
        dropDown.width = 320
        dropDown.direction = .bottom
        dropDown.xMargin  = 15
        dropDown.bottomOffset = CGPoint(x: 0.0, y:(dropDown.anchorView?.plainView.bounds.height)! - 15.0)
        dropDown.show()
    }
    
    // MARK:- Object lifecycle
    deinit {
        try! AVAudioSession.sharedInstance().setCategory(.soloAmbient, options: [])
        resetAll()
        cameraManager.stopRunning()
        print("PreMirrorTest deinit")
    }
}

// MARK: - Perform Object Detection Logics
extension PreMirrorTestViewController {
    
    // this below method runs on a seperate queue
    private func startObjectDetection(_ cvPixelBuffer: CVPixelBuffer) {
        if let minConfidenceLevel = thresolds?.preview_obj_detection?.object_detection_min_confidence?.floatValue {
            let originalImage = UIImage(pixelBuffer: cvPixelBuffer)
            do {
                if let manager = mirrorTestManager {
                    let result = try manager.runObjectDetectionModel(pixelBuffer: cvPixelBuffer, with: minConfidenceLevel)
                    if let _ = result {
                        if !(objectDetectionWork?.isCancelled ?? false) {
                            DispatchQueue.main.async { [weak self] in
                                guard let self = self else {return}
                                self.cameraManager.stopRunning()
                                self.resetAll()
                                self.preMirrorTestLogger?.log(errorString: "Success Of PreMirrorTest", primaryImage: originalImage, secondaryImage: nil)
                                self.delegate?.onObjectDetected()
                            }
                        }
                        return
                    }
                    throw MirrorTestError.ObjectDetectionFaild
                }
                throw MirrorTestError.ModelLoadingError
            } catch let error as MirrorTestError {
                preMirrorTestLogger?.log(errorString: MirrorTestError.ObjectDetectionFaild.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: nil)
                if !(objectDetectionWork?.isCancelled ?? false) {
                    DispatchQueue.main.async {
                        self.handleObjectDetectionErrors(error)
                    }
                }
            } catch {
                preMirrorTestLogger?.log(errorString: MirrorTestError.ObjectDetectionFaild.getRespectiveMessage(), primaryImage: originalImage, secondaryImage: nil)
                if !(objectDetectionWork?.isCancelled ?? false) {
                    DispatchQueue.main.async {
                        self.startNextFrameAfterError(self.nextFrameAfterError)
                    }
                }
            }
        }
    }
    
    private func startNextFrameAfterError(_ timeInterval: TimeInterval) {
        deviceDetectionTimer?.invalidate()
        deviceDetectionTimer = nil
        deviceDetectionTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { [weak self] timer in
            timer.invalidate()
            self?.isReadyForNextFrame = true
        })
    }
    
    private func increaseRetryCount(with orderId: String?) {
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            SaveFDDataMode.savedata(saveObject.imei, saveObject.imei2, saveObject.orderId, saveObject.smsNumber, saveObject.mtFrontPanelName, saveObject.mtBackPanelName, (saveObject.mtFrontPanelRetryCount ?? 0)+1, saveObject.mtServerHashCode)
        }
    }
    
    private func setUpObjectNotDetectedView() {
        let timeInterval = Double(objectNotDetectedInTime ?? 30)
        
        if deviceNotDetectedTimer != nil {
            deviceNotDetectedTimer?.invalidate()
            deviceNotDetectedTimer = nil
        }
        
        deviceNotDetectedTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) {[weak self] (timer) in
            DispatchQueue.main.async {
                guard let self = self else {return}
                self.objectDetectionWork?.cancel()
                self.cameraManager.stopRunning()
                self.stopAllTimer()
                let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
                let alreadyRetryCount = SaveFDDataMode.getMTFrontPanelRetryCount(orderId) ?? 0
                let remainingTries = (self.preMirrorTestRetryCount ?? 2) - alreadyRetryCount
                EventTracking.shared.eventTracking(name: .frontImageNotCaptured, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .remainingTries: remainingTries, .screen: Event.EventParams.mobileDetectionScreen])
                if  alreadyRetryCount >= (self.preMirrorTestRetryCount ?? 2) {
                    UIDevice.vibrate()
                    self.routeToSDTErrorScreen()
                } else {
                    self.increaseRetryCount(with: orderId)
                    self.startTextToSpeech = "CASE_6"
                    UIDevice.vibrate()
                    let vc = MirrorTestBottomView()
                    vc.setViewModel(self.frontPanelRetryData, sheetType: .retryFrontPanel)
                    vc.delegate = self
                    Utilities.presentPopoverWithOutGesture(view: vc, height: MirrorTestBottomView.height(forModel: self.frontPanelRetryData, with: 80))
                }
            }
        }
    }
    
    private func handleObjectDetectionErrors(_ error: MirrorTestError) {
        switch error {
        case .ObjectDetectionFaild:
            startNextFrameAfterError(nextFrameAfterError)
        default:
            break
        }
    }
    
    /**
     This Method will start timer for auto capture image of front panel
     */
    private func setupFrontPanelTimers() {
        cameraManager.startRunning()
        if self.cameraManager.cameraPosition == .front {
            startNextFrameAfterError(nextFrameAfterError)
            setUpObjectNotDetectedView()
        }
    }
    
    private func stopAllTimer() {
        self.cameraManager.stopRunning()
        self.isReadyForNextFrame = false
        if self.deviceDetectionTimer != nil {
            deviceDetectionTimer?.invalidate()
            self.deviceDetectionTimer = nil
        }
        
        if deviceNotDetectedTimer != nil {
            deviceNotDetectedTimer?.invalidate()
            deviceNotDetectedTimer = nil
        }
    }
}

// MARK:- Router Logics
extension PreMirrorTestViewController {
    
    func routeToSDTErrorScreen() {
        let device = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.deviceModel
        let errorVC = MirrorTestErrorScreenVC()
        errorVC.errorScreenFor = .StartSDT
        errorVC.deviceModel = device
        errorVC.delegate = self
        self.presentOverFullScreen(errorVC, animated: true, completion: nil)
    }
    
    func routeToVideoGuide() {
        let vc = VideoPlayViewController()
        vc.delegate = self
        vc.videoPlayUrl = self.videoUrl
        presentOverFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToChangeLanguage() {
        self.delegate?.redirectToTextToSpeechScreen()
    }
}

// MARK:- Configuration Logic
extension PreMirrorTestViewController {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MirrorTestInterector()
        let presenter = MirrorTestPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

//MARK:- DISPLAY LOGIC
extension PreMirrorTestViewController: MirrorTestDisplayLogic {
    private func startShowingViews() {
        cameraPreview.isHidden = false
        self.startTextToSpeech = "CASE_1"
    }
    
    func displayPhotoClickshelpData(frontPanelNotClicksModel: [AnyObject], frontPanelClicksModel: [AnyObject], backPanelBottomModel: [String]?, videoUrl: String?, hashKey: String?, objectNotDetected: [AnyObject]) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.frontPanelRetryData = objectNotDetected
        self.videoUrl = videoUrl
        self.setupTextToSpeech()
        self.startShowingViews()
        self.setupCameraManeger(with: .front)
        setupFrontPanelTimers()
    }
    
    func displayPhotoClickshelpFailure(with error: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.showAlert(title: "", message: message, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
            self.interactor?.changeFDFlowType(testType: .SDT, activityReferenceId: self.activityRefId, reasonToChange: reasonToChange)
        }) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showFDTestTypeChangeSuccess() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        FDCacheManager.shared.changeTestType(to: .SDT)
        Utilities.dismissAllPresentedVC {
            if let navVC = UIApplication.shared.delegate?.window??.rootViewController as? BaseNavigationController {
                navVC.popToRootViewController(animated: true)
                let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
                navVC.pushViewController(membershipActivationFDVC, animated: true)
            }
        }
    }
}

//MARK:- Text to Speech Delegate
extension PreMirrorTestViewController: OATextToSpeechDelegate {
    
    func speechDidStart() { }
    
    func speechDidFinish() {
        if self.startTextToSpeech == "CASE_1"{
            self.startTextToSpeech = "CASE_2"
        }
    }
}


//MARK: VideoPlayViewDelegate
extension PreMirrorTestViewController: VideoPlayViewDelegate {
    
    func onClickSkipVideo() {
        self.setupTextToSpeech()
        self.setupFrontPanelTimers()
    }
    
    func onClickContinueAction() {
        self.setupTextToSpeech()
        self.setupFrontPanelTimers()
    }
}

//MARK: - OACameraManeger
extension PreMirrorTestViewController: OACameraManagerDelegate {
    
    func didOutput(_ cvPixelBuffer: CVPixelBuffer) {
        if isReadyForNextFrame {
            self.isReadyForNextFrame = false
            objectDetectionWork?.cancel()
            objectDetectionWork = DispatchWorkItem { [weak self] in
                self?.startObjectDetection(cvPixelBuffer)
            }
            if let _  = objectDetectionWork {
                objectDetectionQueue.async(execute: objectDetectionWork!)
            }
        }
    }
}

//MARK: - MirrorTestBottomViewDelegate
extension PreMirrorTestViewController: MirrorTestBottomViewDelegate {
    func onClickTryAgain() {
        EventTracking.shared.eventTracking(name: .retakeFrontImage, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .screen: Event.EventParams.errorScreen])
        self.startTextToSpeech = "CASE_7"
        Utilities.topMostPresenterViewController.dismiss(animated: true)
        self.setupFrontPanelTimers()
    }
}

extension PreMirrorTestViewController: MirrorTestErrorScreenDelegate {
    func onRetry(with error: Error?) {
        EventTracking.shared.eventTracking(name: .retakeFrontImage, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .screen: Event.EventParams.moveToSDTScreen])
        self.setupFrontPanelTimers()
    }
    
    func onStartSDTtapped() {
        let reason = TFLiteModelsDownloadManager.sharedInstance.mirrorTestErrorMessages?.objectDetectionRetryExceeded
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
        self.interactor?.changeFDFlowType(testType: .SDT, activityReferenceId: self.activityRefId, reasonToChange: reason)
    }
}
