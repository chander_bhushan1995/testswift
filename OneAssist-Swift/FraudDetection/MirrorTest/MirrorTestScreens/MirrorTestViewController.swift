//
//  MirrorTestViewController.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 19/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//
import UIKit
import AVFoundation
import FirebaseCrashlytics

//MARK: Interector Methods
protocol MirrorTestBusinessLogic: AnyObject {
    func loadPhotoClickHelpData(_ activityReferenceId: String?, with lastHashCode: String?)
    func getHashKey(activityReferenceId: String?)
    func changeFDFlowType(testType: FDTestType, activityReferenceId: String?, reasonToChange: String?)
}

//MARK: Delegates
protocol MirrorTestViewDelegate: class {
    func onFrontPanelClicked(image: UIImage?)
    func redirectToTextToSpeechScreen()
    func onBackPanelClicked(image: UIImage?)
}

class MirrorTestViewController: BaseVC, HideNavigationProtocol {
    
    // MARK: Outlets
    @IBOutlet  var frontPanelView: FrontPanelView!
    @IBOutlet  var backPanelView: BackPanelView!
    
    //MARK: Public Variables
    var capturedImage: UIImage?
    var frontPanelSnapShot: UIImage?
    var isCaptureFront: Bool = true
    weak var delegate: MirrorTestViewDelegate?
    
    // MARK: Lazy Loadings
    private lazy var cameraManager = OACameraManeger()
    private lazy var testRecognizer = ImageTextAndBarCodeRecognizer()
    private lazy var textTospeech = OATextToSpeech()
    private lazy var mirrorTestManager: MirrorTestManager? = MirrorTestManager(.both)
    
    //MARK: APIs variables
    fileprivate var interactor: MirrorTestBusinessLogic?
    fileprivate var frontPanelRetryData: [AnyObject] = []
    fileprivate var frontPanelSuccessData: [AnyObject] = []
    fileprivate var backPanelBottomData: [String]?
    fileprivate var videoUrl: String?
    fileprivate var languageCode: String?
    fileprivate var language: Language?
    
    //MARK: Private variables
    private let mirrorTestQueue = DispatchQueue(label: "mirrorTestQueue")
    private let loggingThresolds = TFLiteModelsDownloadManager.sharedInstance.mirrorTestThresolds?.loggingFor
    private var enableVoiceSupport: Bool = false
    private var isReadyForNextFrame: Bool = false
    private var activityRefId: String? = FDCacheManager.shared.activityRefId
    
    private var currentNonMendatoryRetryCount: Int = 0
    private var nonMendatoryRetryThresold: Int?
    private var currentMirrorTestRetryCount: Int?
    private var mirrorTestRetryThresold: Int?
    private var nextFrameAfterError: TimeInterval?
    private var changeHashCodeInterval: TimeInterval?
    private var callHashCodeAPIInterval: TimeInterval?
    private var frontPanelClickTimer: Timer?
    private var frontPanelNotClicktimer: Timer?
    private var hashCodeChangeTimer: Timer?
    private var hashCodeAPICallTimer: Timer?
    private var mirrorTestWorkItem: DispatchWorkItem?
    private var screenshotCapture: ScreenshotCapture?
    private var currentCapturedFrame: UIImage?
    private var mirrorTestLogger: OALogger?
    private var lastServerHashKeyForRetake: String?
    private var startTextToSpeech: String = "" {
        didSet {
            if !startTextToSpeech.isEmpty, let selectedLanguage = self.language, enableVoiceSupport {
                textTospeech.preferredVoiceLanguageCode = self.languageCode
                textTospeech.speechSynthesizerDelegate = self
                var languageCase: LanguageCase? = nil
                if startTextToSpeech      == "CASE_3" { languageCase = selectedLanguage.CASE_3 }
                else if startTextToSpeech == "CASE_4" { languageCase = selectedLanguage.CASE_4 }
                else if startTextToSpeech == "CASE_5" { languageCase = selectedLanguage.CASE_5 }
                else if startTextToSpeech == "CASE_6" { languageCase = selectedLanguage.CASE_6 }
                else if startTextToSpeech == "CASE_7" { languageCase = selectedLanguage.CASE_7 }
                else if startTextToSpeech == "CASE_8" { languageCase = selectedLanguage.CASE_8 }
                if let cases = languageCase {
                    textTospeech.delay = TimeInterval(exactly: cases.delay_iOS) ?? 0
                    textTospeech.text = cases.text ?? ""
                    textTospeech.speak()
                }
            }
        }
    }
    
    // MARK:- Object lifecycle
    deinit {
        try! AVAudioSession.sharedInstance().setCategory(.soloAmbient, options: [])
        print("MirrorTestViewController deinit")
        resetAll()
        cameraManager.stopRunning()
        screenshotCapture?.unregisterApplicationStateObserver()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        try! AVAudioSession.sharedInstance().setCategory(.playback, options: [])
        super.viewDidLoad()
        self.initializeView()
        let thresolds = TFLiteModelsDownloadManager.sharedInstance.mirrorTestThresolds?.front_panel_mirror_test
        nextFrameAfterError = (thresolds?.delay_for_process_next_frame?.doubleValue ?? 400) / 1000
        changeHashCodeInterval = (thresolds?.changeHashCodeThresold?.doubleValue ?? 500) / 1000
        callHashCodeAPIInterval = (thresolds?.hashCodeAPICallThresold?.doubleValue ?? 900000) / 1000
        nonMendatoryRetryThresold = thresolds?.non_mandatory_classes_rety_count?.intValue
        mirrorTestRetryThresold = thresolds?.bottom_sheet_retry_count?.intValue
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
        currentMirrorTestRetryCount = SaveFDDataMode.getMTFrontPanelRetryCount(orderId)
        mirrorTestLogger = OALogger(initiatedFor: .MirrorTest)
        mirrorTestManager?.logger = mirrorTestLogger
        if isCaptureFront {
            addObserver()
            UIApplication.shared.isIdleTimerDisabled = true
            EventTracking.shared.eventTracking(name: .imeiScreen, [.mode: FDCacheManager.shared.fdTestType.rawValue, .voice: FDCacheManager.shared.selectLanguageForMirrorTest != nil ? "Enabled" : "Disabled"])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopRunning()
        saveRetryCount()
        resetAll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if isCaptureFront {
            setUpPreviousBrightnessValue()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.frontPanelRetryData.count > 0 {
            self.setupFrontPanelTimers()
        }
        if isCaptureFront {
            setUpNewBrightnessValue()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cameraManager.updatePreviewFrame()
    }
    
    private func initializeView() {
        self.setupSupprtingViews()
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (time) in
            DispatchQueue.main.async {
                if FDCacheManager.shared.imageCaptureType == .ReuploadBackPanel || !self.isCaptureFront {
                    self.startShowingViews()
                    Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
                    self.interactor?.loadPhotoClickHelpData(nil, with: nil)
                } else {
                    let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
                    if let saveObject = SaveFDDataMode.getSaveData(orderId) {
                        if let imeiSecondNumber = saveObject.imei2 {
                            self.frontPanelView.primaryIMEINumber = saveObject.imei
                            self.frontPanelView.secondaryIMEINumber = imeiSecondNumber
                        } else {
                            self.frontPanelView.primaryIMEINumber = saveObject.imei
                        }
                    }
                    self.screenshotCapture = ScreenshotCapture(delegate: self)
                    self.screenshotCapture?.isContinuousCapture = true
                    self.screenshotCapture?.requestPhotosAuthorization(andThen: {[unowned self] in
                        self.screenshotCapture?.registerScreenShotObserver()
                        self.screenshotCapture?.registerApplicationStateObserver()
                    })
                    self.lastServerHashKeyForRetake = FDDataModel.getMTServerHashCode(orderId)
                    Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
                    self.interactor?.loadPhotoClickHelpData(self.activityRefId, with: self.lastServerHashKeyForRetake)
                }
            }
        }
    }
    
    /**
     Add front and back Panel UI based on Capture and retake type
     */
    private func setupSupprtingViews() {
        if FDCacheManager.shared.imageCaptureType == .ReuploadBackPanel || !isCaptureFront {
            self.backPanelView.addInSuperView(self.view)
            
        }else if FDCacheManager.shared.imageCaptureType == .ReuploadFrontPanel || isCaptureFront {
            self.frontPanelView.addInSuperView(self.view)
            
        }
        
        ///Set up Camera Maneger
        if FDCacheManager.shared.imageCaptureType == .ReuploadBackPanel || !isCaptureFront {
            self.setupCameraManeger()
        } else {
            self.setupCameraManeger(with: .front)
            self.cameraManager.stopRunning()
        }
    }
    
    private func setupCameraManeger(with position: AVCaptureDevice.Position? = .back){
        do {
            if position == .front {
                self.cameraManager.captureOuputFor = .AVCaptureVideoDataOutput
                self.cameraManager.delegate = self
            }
            try cameraManager.captureSetup(in: self.view, withPosition: position)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func startShowingViews() {
        if self.cameraManager.cameraPosition == .back {
            self.frontPanelView.isHidden = true
            self.backPanelView.isHidden = false
            self.backPanelView.showTapToClickToolTip()
            self.backPanelView.setupAnimation(self.backPanelBottomData)
            self.startTextToSpeech = "CASE_3"
        } else {
            self.backPanelView.isHidden = true
            self.frontPanelView.isHidden = false
        }
    }
    
    private func resetAll() {
        if let _ = self.backPanelView {
            self.backPanelView.invalidateTimer()
        }
        textTospeech.stopSpeech()
        stopAllTimer()
    }
    
    private func setupTextToSpeech() {
        if let langauge = FDCacheManager.shared.selectLanguageForMirrorTest {
            languageCode = langauge.languageCode
            language = langauge.data
            enableVoiceSupport = true
        }
    }
    
    private func saveRetryCount() {
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            SaveFDDataMode.savedata(saveObject.imei, saveObject.imei2, saveObject.orderId, saveObject.smsNumber, saveObject.mtFrontPanelName, saveObject.mtBackPanelName, currentMirrorTestRetryCount, saveObject.mtServerHashCode)
        }
    }
    
    //MARK: IBAction Methods
    @IBAction func onClickCrossButtonAction(_ sender: UIButton?){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickThreeDotAction(_ sender: UIButton?){
        self.stopAllTimer()
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = self.frontPanelView.menuButton // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        
        var dataSource: [String] = []
        dataSource.append(Strings.iOSFraudDetection.seeVideoGiude)
        if RemoteConfigManager.shared.mirrorLanguageEnable {
            if self.language != nil {
                if enableVoiceSupport {
                    dataSource.append(Strings.iOSFraudDetection.changelanguage)
                    dataSource.append(Strings.iOSFraudDetection.disablevoice)
                }else {
                    dataSource.append(Strings.iOSFraudDetection.enablevoice)
                }
                
            }else {
                dataSource.append(Strings.iOSFraudDetection.enablevoice)
            }
        }
        
        dropDown.dataSource = dataSource
        
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else {return}
            print("Selected item: \(item) at index: \(index)")
            self.textTospeech.stopSpeech()
            switch item {
            case Strings.iOSFraudDetection.seeVideoGiude:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.seeVideoGiude, .screen: Event.EventParams.imeiScreen])
                self.enableVoiceSupport = false
                self.routeToVideoGuide()
                break
            case Strings.iOSFraudDetection.changelanguage:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.changelanguage, .screen: Event.EventParams.imeiScreen])
                self.routeToChangeLanguage()
                break
            case Strings.iOSFraudDetection.enablevoice:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.enablevoice, .screen: Event.EventParams.imeiScreen])
                self.routeToChangeLanguage()
                break
            case Strings.iOSFraudDetection.disablevoice:
                EventTracking.shared.eventTracking(name: .verifyThreeDot, [.mode: FDCacheManager.shared.fdTestType.rawValue, .option: Strings.iOSFraudDetection.disablevoice, .screen: Event.EventParams.imeiScreen])
                FDCacheManager.shared.selectLanguageForMirrorTest = nil
                self.enableVoiceSupport = false
                self.setupFrontPanelTimers()
                break
            default:
                break
            }
        }
        
        dropDown.cancelAction = {[weak self] in
            guard let self = self else {return}
            print("cancel drop down")
            self.setupFrontPanelTimers()
        }
        
        dropDown.width = 320
        dropDown.direction = .bottom
        dropDown.xMargin  = 15
        dropDown.bottomOffset = CGPoint(x: 0.0, y:(dropDown.anchorView?.plainView.bounds.height)! - 15.0)
        dropDown.show()
    }
    
    @IBAction func onClickCameraButtonAction(_ sender: UIButton?){
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
        self.backPanelView.cameraclickButton.isEnabled = false
        self.cameraManager.capture()
        self.textTospeech.stopSpeech()
        self.cameraManager.onPhotoCapture = {[weak self] (image, error) in
            guard let self = self else { return }
            if error != nil {
                Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
                print(error!.localizedDescription)
            }else {
                self.getSnapshot(image)
            }
        }
    }
}

//MARK:- Front/Back Panel Timers & Helper Methods
extension MirrorTestViewController {
    /**
     This Method will start timer for auto capture image of front panel
     */
    private func startAPICallForHashCode() {
        self.stopAllTimer()
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
        self.interactor?.getHashKey(activityReferenceId: self.activityRefId)
    }
    
    private func setupFrontPanelTimers() {
        if self.cameraManager.cameraPosition == .front {
            self.cameraManager.startRunning()
            self.startTimerToGetImage(0.5)
            self.setupFrontpanelNotClickView()
            self.startTimerForHashCodeAPICall()
            self.startChangeHashCodeTimer()
        }
    }
    
    private func stopAllTimer() {
        cameraManager.stopRunning()
        mirrorTestWorkItem?.cancel()
        frontPanelClickTimer?.invalidate()
        frontPanelClickTimer = nil
        frontPanelNotClicktimer?.invalidate()
        frontPanelNotClicktimer = nil
        hashCodeAPICallTimer?.invalidate()
        hashCodeAPICallTimer = nil
        hashCodeChangeTimer?.invalidate()
        hashCodeChangeTimer = nil
    }
    
    private func startTimerForHashCodeAPICall() {
        if self.cameraManager.cameraPosition == .front {
            self.hashCodeAPICallTimer?.invalidate()
            self.hashCodeAPICallTimer = nil
            self.hashCodeAPICallTimer = Timer.scheduledTimer(withTimeInterval: callHashCodeAPIInterval ?? 900000, repeats: false, block: { [weak self] timer in
                timer.invalidate()
                guard let self = self else {return}
                self.startAPICallForHashCode()
            })
        }
    }
    
    private func startChangeHashCodeTimer() {
        if self.cameraManager.cameraPosition == .front {
            self.hashCodeChangeTimer?.invalidate()
            self.hashCodeChangeTimer = nil
            self.hashCodeChangeTimer = Timer.scheduledTimer(withTimeInterval: changeHashCodeInterval ?? 0.4, repeats: true, block: { [weak self] timer in
                self?.frontPanelView.hashString = self?.mirrorTestManager?.getDisplayableHashKey(with: nil)
            })
        }
    }
    
    private func startTimerToGetImage(_ timeInterval: TimeInterval) {
        if self.cameraManager.cameraPosition == .front {
            frontPanelClickTimer?.invalidate()
            self.frontPanelClickTimer = nil
            frontPanelClickTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { [weak self] timer in
                timer.invalidate()
                self?.frontPanelView.errorText = nil
                // this async after is to make sure errortext have been nil & capture next frame
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                    self?.isReadyForNextFrame = true
                }
            })
        }
    }
    
    // this below method runs on a seperate queue
    private func startMirrorTest(_ cvPixelBuffer: CVPixelBuffer) {
        do {
            if let manager = mirrorTestManager {
                let isSkipNonMendatoryClasses = currentNonMendatoryRetryCount >= (nonMendatoryRetryThresold ?? 3)
                currentCapturedFrame = UIImage(pixelBuffer: cvPixelBuffer)
                let result = try manager.runMirrorTest(pixelBuffer: cvPixelBuffer, originalImage: currentCapturedFrame, isSkipNonMendatoryClasses:  isSkipNonMendatoryClasses)
                if let images = result, var detectedObjectRect = result?.detectedObjectRect {
                    detectedObjectRect.size.height = (currentCapturedFrame?.size.height ?? 0) - detectedObjectRect.minY
                    let imageForOCR = images.originalImage?.cropImage(rect: detectedObjectRect)
                    testRecognizer.getTextFromImage(image: imageForOCR, readTextType: nil, isImageScaleReq: false) { [weak self](recognizTexts, snapshot, errorMessage) in
                        guard let self = self else {return}
                        if !(self.mirrorTestWorkItem?.isCancelled ?? false) {
                            self.frontPanelSnapShot = snapshot
                            guard let ocrResult = self.mirrorTestManager?.doOCROnRecognizedTexts(recognizedTexts: recognizTexts) else { return self.handleMirrorTestErrors(.OCRFailed) }
                            if ocrResult.imeiOCR && ocrResult.leftRightOCR {
                                if !(self.mirrorTestWorkItem?.isCancelled ?? false) {
                                    if let image = images.originalImage {
                                        self.capturedImage = image
                                        UIDevice.vibrate()
                                        self.resetAll()
                                        self.storeImage { [weak self] in
                                            guard let self = self else {return }
                                            DispatchQueue.main.async {
                                                EventTracking.shared.eventTracking(name: .frontImageCapture, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue])
                                                self.delegate?.onFrontPanelClicked(image: self.capturedImage)
                                            }
                                        }
                                    }
                                }
                            } else {
                                let error: MirrorTestError = ocrResult.imeiOCR ? MirrorTestError.OCRFailed : MirrorTestError.LeftRightTextOCRFailed
                                self.mirrorTestLogger?.log(errorString: error.getRespectiveMessage(), primaryImage: images.originalImage, secondaryImage: images.croppedImage)
                                self.handleMirrorTestErrors(error)
                            }
                        }
                    }
                }
            }
        } catch let error as MirrorTestError {
            DispatchQueue.main.async {
                if !(self.mirrorTestWorkItem?.isCancelled ?? false) {
                    self.handleMirrorTestErrors(error)
                }
            }
        } catch  {
            DispatchQueue.main.async {
                if !(self.mirrorTestWorkItem?.isCancelled ?? false) {
                    self.startTimerToGetImage(0.01)
                }
                Crashlytics.crashlytics().record(error: error)
            }
        }
    }
    
    func getSnapshot(_ image: UIImage?) {
        if let cameraImage = image {
            if let imageData = cameraImage.jpegData(compressionQuality: 1.0) {
                EventTracking.shared.eventTracking(name: .backImageCaptured, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue])
                self.capturedImage = UIImage(data: imageData)
                self.resetAll()
                Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
                storeImage { [weak self] in
                    guard let self = self else {return}
                    UIDevice.vibrate()
                    self.backPanelView.cameraclickButton.isEnabled = true
                    self.delegate?.onBackPanelClicked(image: self.capturedImage)
                }
            } else {
                Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
            }
        } else {
            Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
        }
    }
    
    private func handleMirrorTestErrors(_ error: MirrorTestError) {
        switch error {
        case .ObstractImageDetected, .ImageClassificationFailed, .ImageClassificationLowConfidence:
            currentNonMendatoryRetryCount += 1
        default:
            break
        }
        
        //log image to firebase if required
        let errorsToLogOnFirebase: [MirrorTestError] = [.ImageClassificationFailed, .ImageClassificationLowConfidence, .ObstractImageDetected, .OCRFailed, .ObjectIsFar, .ImageIsDark, .LeftRightTextOCRFailed, .ObjectIsTooNear]
        if let config = loggingThresolds, errorsToLogOnFirebase.contains(error), !(mirrorTestLogger?.mirrorTestFBLogData.isEmpty ?? true) {
            let tempUID = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.custId ?? ""
            let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue ?? ""
            
            let isLogImage = ([MirrorTestError.ImageClassificationFailed, .ImageClassificationLowConfidence, .ObstractImageDetected].contains(error) && config.imageClassification)
                || ((error == .OCRFailed || error == .LeftRightTextOCRFailed) && config.ocr) || ((error == .ObjectIsFar || error == .ObjectIsTooNear) && config.far)
                || (error == .ImageIsDark && config.dark)
            
            mirrorTestLogger?.logMirrorTestOnFirebase(image: currentCapturedFrame, tempUserId: tempUID, orderid: orderId, isImageRequired: isLogImage)
        }
        
        frontPanelView.errorText = error.getRespectiveMessage()
        startTimerToGetImage(self.nextFrameAfterError ?? 1.0)
        print(error)
    }
    
    func setupFrontpanelNotClickView() {
        let timeInterval = Double(RemoteConfigManager.shared.mirrorTestRetryTime)
        
        frontPanelNotClicktimer?.invalidate()
        frontPanelNotClicktimer = nil
        
        frontPanelNotClicktimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) {[weak self] (timer) in
            DispatchQueue.main.async {
                guard let self = self else {return}
                self.stopAllTimer()
                self.startTextToSpeech = "CASE_6"
                let remainingTries = (self.mirrorTestRetryThresold ?? 0) - (self.currentMirrorTestRetryCount ?? 0)
                EventTracking.shared.eventTracking(name: .frontImageNotCaptured, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .remainingTries: remainingTries, .screen: Event.EventParams.imeiScreen])
                if (self.currentMirrorTestRetryCount ?? 0) >= (self.mirrorTestRetryThresold ?? 0) {
                    UIDevice.vibrate()
                    self.routeToSDTErrorScreen()
                } else {
                    UIDevice.vibrate()
                    self.currentMirrorTestRetryCount = (self.currentMirrorTestRetryCount ?? 0) + 1
                    let vc = MirrorTestBottomView()
                    vc.setViewModel(self.frontPanelRetryData, sheetType: .retryFrontPanel)
                    vc.delegate = self
                    Utilities.presentPopoverWithOutGesture(view: vc, height: MirrorTestBottomView.height(forModel: self.frontPanelRetryData, with: 80))
                }
            }
        }
    }
}

//MARK:- DISPLAY LOGIC
extension MirrorTestViewController: MirrorTestDisplayLogic {
    
    func updateFDHashKey(_ hashKey: String?) { // update has key by stopping current process
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let _ = lastServerHashKeyForRetake {
            PanelPreviewViewController.removeFromStack(for: .FrontPanel)
            FDCacheManager.shared.deleteFrontPanelImageInDir()
            lastServerHashKeyForRetake = nil
        }
        self.frontPanelView.hashString = mirrorTestManager?.getDisplayableHashKey(with: hashKey)
        self.setupFrontPanelTimers()
    }
    
    func showFDHashAPIError(_ error: String?) {
        stopAllTimer()
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.showAlert(title: "", message: error ?? "", primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
            self.interactor?.getHashKey(activityReferenceId: self.activityRefId)
        }) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func displayPhotoClickshelpData(frontPanelNotClicksModel: [AnyObject], frontPanelClicksModel: [AnyObject], backPanelBottomModel: [String]?, videoUrl: String?, hashKey: String?, objectNotDetected: [AnyObject]) {
        self.frontPanelRetryData = frontPanelNotClicksModel
        self.frontPanelSuccessData = frontPanelClicksModel
        self.backPanelBottomData = backPanelBottomModel
        self.videoUrl = videoUrl
        self.setupTextToSpeech()
        self.startShowingViews() // make sure call after setUpTextToSpeech
        if isCaptureFront, let hashCode = mirrorTestManager?.getDisplayableHashKey(with: hashKey) {
            self.frontPanelView.hashString = hashCode
            self.cameraManager.startRunning()
            self.setupFrontPanelTimers()
        }
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func displayPhotoClickshelpFailure(with error: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.showAlert(title: "", message: message, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
            self.interactor?.changeFDFlowType(testType: .SDT, activityReferenceId: self.activityRefId, reasonToChange: reasonToChange)
        }) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showFDTestTypeChangeSuccess() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        FDCacheManager.shared.changeTestType(to: .SDT)
        Utilities.dismissAllPresentedVC {
            if let navVC = UIApplication.shared.delegate?.window??.rootViewController as? BaseNavigationController {
                navVC.popToRootViewController(animated: true)
                let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
                navVC.pushViewController(membershipActivationFDVC, animated: true)
            }
        }
        Utilities.topMostPresenterViewController.dismiss(animated: false) {
            
        }
    }
}

//MARK:- Text to Speech Delegate
extension MirrorTestViewController: OATextToSpeechDelegate {
    func speechDidStart() { }
    
    func speechDidFinish() {
        if self.startTextToSpeech == "CASE_3" {
            self.startTextToSpeech = "CASE_4"
        }
    }
}

//MARK: MirrorTestBottomViewDelegate
extension MirrorTestViewController: MirrorTestBottomViewDelegate {
    
    func onClickTryAgain() {
        self.startTextToSpeech = "CASE_7"
        EventTracking.shared.eventTracking(name: .retakeFrontImage, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .screen: Event.EventParams.errorScreen])
        Utilities.topMostPresenterViewController.dismiss(animated: true)
        self.setupFrontPanelTimers()
    }
    
    func onClickContinue() {}
}

// MARK:- Configuration Logic
extension MirrorTestViewController {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MirrorTestInterector()
        let presenter = MirrorTestPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

//MARK: VideoPlayViewDelegate
extension MirrorTestViewController: VideoPlayViewDelegate {
    
    func onClickSkipVideo() {
        self.setupTextToSpeech()
        self.setupFrontPanelTimers()
    }
    
    func onClickContinueAction() {
        self.setupTextToSpeech()
        self.setupFrontPanelTimers()
    }
}

//MARK: MirrorTest Error Screen Delegate
extension MirrorTestViewController: MirrorTestErrorScreenDelegate {
    func onRetry(with error: Error?) {
        EventTracking.shared.eventTracking(name: .retakeFrontImage, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .screen: Event.EventParams.moveToSDTScreen])
        self.setupFrontPanelTimers()
    }
    
    func onStartSDTtapped() {
        let reason = TFLiteModelsDownloadManager.sharedInstance.mirrorTestErrorMessages?.mirrorTestRetryExceeded
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
        self.interactor?.changeFDFlowType(testType: .SDT, activityReferenceId: self.activityRefId, reasonToChange: reason)
    }
}

//MARK: MirrorTest OACameraManagerDelegate
extension MirrorTestViewController: OACameraManagerDelegate {
    func didOutput(_ cvPixelBuffer: CVPixelBuffer) {
        if isReadyForNextFrame {
            self.isReadyForNextFrame = false
            mirrorTestWorkItem?.cancel()
            mirrorTestWorkItem = DispatchWorkItem { [weak self] in
                self?.startMirrorTest(cvPixelBuffer)
            }
            if let _  = mirrorTestWorkItem {
                mirrorTestQueue.async(execute: mirrorTestWorkItem!)
            }
        }
    }
}

// MARK:- Router Logics
extension MirrorTestViewController {
    
    func routeToVideoGuide() {
        let vc = VideoPlayViewController()
        vc.delegate = self
        vc.videoPlayUrl = self.videoUrl
        presentOverFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToChangeLanguage() {
        self.delegate?.redirectToTextToSpeechScreen()
    }
    
    func routeToSDTErrorScreen() {
        let device = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.deviceModel
        let errorVC = MirrorTestErrorScreenVC()
        errorVC.errorScreenFor = .StartSDT
        errorVC.deviceModel = device
        errorVC.delegate = self
        self.presentOverFullScreen(errorVC, animated: true, completion: nil)
    }
}

// MARK: - ScreenshotCaptureDelegate
extension MirrorTestViewController: ScreenshotCaptureDelegate {
    func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didDetect screenshot: UIImage) {
        self.startAPICallForHashCode()
    }
    
    func screenshotCapture(_ screenshotCapture: ScreenshotCapture, didFailWith error: ScreenshotCapture.Error) {
    }
}

// MARK: - Brightness Setup
extension MirrorTestViewController {
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func appDidBecomeActive() {
        setUpNewBrightnessValue()
    }
    
    @objc func applicationWillResignActive() {
        setUpPreviousBrightnessValue()
    }
    
    func setUpNewBrightnessValue() {
        let preValue = UIScreen.main.brightness
        UserDefaults.standard.set(preValue, forKey: UserDefaultsKeys.previousBrightnessValue.rawValue)
        UIScreen.main.brightness = CGFloat(RemoteConfigManager.shared.fdDeviceBrightness)
    }
    
    func setUpPreviousBrightnessValue() {
        if let preValue = UserDefaults.standard.value(forKey: UserDefaultsKeys.previousBrightnessValue.rawValue) as? Float {
            UIScreen.main.brightness = CGFloat(preValue)
        }
    }
}

// MARK: - Image Storage Logic
extension MirrorTestViewController {
    private func storeImage(completionHandler: @escaping (()->())) {
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
        DispatchQueue.global(qos: .userInitiated).async {
            self.storeImagesIntoDirectory { status, errorMessage  in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
                    if status {
                        completionHandler()
                    } else {
                        self.showAlert(message: errorMessage ?? "")
                    }
                }
            }
        }
    }
    
    private func storeImagesIntoDirectory(completionBlock: @escaping (Bool,String?)->Void) {
        var metaData: [String: String] = [:]
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
        var errorMessage: String? = nil
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            if let imei = saveObject.imei {
                metaData["imei"] = imei
            }
            if let imei2 = saveObject.imei2 {
                metaData["imei_2"] = imei2
            }
            if let activityrefid = FDCacheManager.shared.getDocsToUploadModel?.activityReferenceId {
                metaData["activityrefid"] = activityrefid
            }
            var metaDataBase64: String?
            do {
                let metaDataData: Data = try JSONSerialization.data(withJSONObject:metaData,options: [])
                if let jsonString = String(data: metaDataData, encoding: .utf8) {
                    metaDataBase64 = jsonString.toBase64()
                }
                
            } catch {}
            
            let fpname = orderId! + "_fp.png"
            let bpname = orderId! + "_bp.png"
            
            let group = DispatchGroup()
            var fpStatus: Bool?
            var bpStatus: Bool?
            
            if isCaptureFront || FDCacheManager.shared.imageCaptureType == .ReuploadFrontPanel {
                group.enter()
                self.saveintoStrore(comment: metaDataBase64, fileName: fpname, image: self.capturedImage) { (status, fpfileName) in
                    fpStatus = status
                    if status == true {
                        if let saveFDObject = SaveFDDataMode.getSaveData(orderId), let hashKey = mirrorTestManager?.serverKey {
                            SaveFDDataMode.savedata(saveFDObject.imei, saveFDObject.imei2, orderId, saveObject.smsNumber, fpfileName, saveFDObject.mtBackPanelName, saveFDObject.mtFrontPanelRetryCount, String(describing: hashKey))
                        }
                    }else {
                        errorMessage = fpfileName ?? MhcStrings.AlertMessage.touchIdRetryAlert
                    }
                    group.leave()
                }
            } else if (!isCaptureFront || FDCacheManager.shared.imageCaptureType == .ReuploadBackPanel) {
                group.enter()
                self.saveintoStrore(comment: metaDataBase64, fileName: bpname, image: self.capturedImage) { (status, bpFileName) in bpStatus = status
                    if status == true {
                        if let saveFDObject = SaveFDDataMode.getSaveData(orderId) {
                            SaveFDDataMode.savedata(saveFDObject.imei, saveFDObject.imei2, orderId, saveFDObject.smsNumber, saveFDObject.mtFrontPanelName, bpFileName, saveFDObject.mtFrontPanelRetryCount, saveFDObject.mtServerHashCode)
                        }
                    }else {
                        errorMessage = bpFileName ?? MhcStrings.AlertMessage.touchIdRetryAlert
                    }
                    group.leave()
                }
            }
            
            group.notify(queue: .main) {[weak self] in
                guard self != nil else { return }
                if let fpSaveStatus = fpStatus, let bpSavestatus = bpStatus { //both save
                    if fpSaveStatus == true, bpSavestatus == true {
                        completionBlock(true, errorMessage)
                    }else {
                        completionBlock(false, errorMessage)
                    }
                    
                } else if let fpSaveStatus = fpStatus { //front only save
                    completionBlock(fpSaveStatus, errorMessage)
                } else if let bpSavestatus = bpStatus { //back only save
                    completionBlock(bpSavestatus, errorMessage)
                    
                } else {
                    completionBlock(false, errorMessage)
                }
            }
            
        }else {
            completionBlock(false, MhcStrings.AlertMessage.touchIdRetryAlert)
        }
    }
    
    private func saveintoStrore(comment: String?, fileName: String, image: UIImage?, completionBlock: (Bool, String?)->Void) {
        DataDiskStogare.saveImageintoDirectory(image: image, fileName: fileName) { (status, fileurl) in
            if let source = fileurl, let sourceUrl = URL(string: source) {
                // print(getEXIFUserComment(from: sourceUrl) ?? "No EXIF UserComment")
                
                guard let destinationURL = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(fileName)
                else { completionBlock(false, "Destination URL not created")
                    return
                }
                setEXIFUserComment(comment ?? "", using: sourceUrl, destination: destinationURL)
                completionBlock(true, fileName)
                
            }else{
                completionBlock(false, "sourceUrl URL not created")
                
            }
        }
    }
}

class ImageSaver: NSObject {
    func writeToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
    }
    
    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Save finished!")
    }
}
