//
//  MirrorTestPreviewPresenter.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 27/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation


protocol PanelPreviewDisplayLogic: AnyObject {
    func displayInitialData(dataSource: [Any], termsData: [AnyObject], frontPanelClickedData: [AnyObject])
    func displayUploadedImageSuccess(frontPanelname: String?, frontPaneldata: String?, backPanelname: String?, backPaneldata: String?)
    func displayImageFailure(error: String?, with name: String?)
    
    func submitDocsSuccess()
    func displayError(_ error: String)
}

class PanelPreviewPresenter: PanelPreviewPresenterLogic {
    
    weak var viewController: PanelPreviewDisplayLogic?
    
    func presentInitialData(capturedImage: UIImage?,imageCapturedFor: CameraType, termsDataResponse: PhotoClickHelpPointsDTO?, termsDataError: Error?) {
        
        var dataSource = [Any]()
        if imageCapturedFor == .FrontPanel {
            dataSource.append(ImagePreviewDescriptionVM(image: capturedImage, description: Strings.MirrorTestPreviewScreen.frontPhotoDescription))
            dataSource.append(CheckBoxAndTitleCellVM(idForCheckBox: nil, title: Strings.MirrorTestPreviewScreen.frontPanelFirstConsent, isEnabled: false))
            dataSource.append(CheckBoxAndTitleCellVM(idForCheckBox: nil, title: Strings.MirrorTestPreviewScreen.frontPanelSecondConsent, isEnabled: false))
        } else {
            dataSource.append(ImagePreviewDescriptionVM(image: capturedImage, description: Strings.MirrorTestPreviewScreen.backPhotoDescription))
            dataSource.append(CheckBoxAndTitleCellVM(idForCheckBox: nil, title: Strings.MirrorTestPreviewScreen.backPanelConsent, isEnabled: false))
        }
        
        var termsDataSource = [AnyObject]()
        if let frontPanelpoints = termsDataResponse?.agree_submit_photos {
            
            let activateHeader = ActivationHeader(dictionary: [:])
            activateHeader.title = frontPanelpoints.title
            termsDataSource.append(activateHeader)
            
            let facts = Facts(dictionary: [:])
            facts.points = frontPanelpoints.points
            facts.lineSpacing = 20.0
            termsDataSource.append(facts)
        }
        
        var frontPanelClicked: [AnyObject] = []
        
        let imageValue = LeftImageTitleCellVM(title: "Awesome!", image: UIImage(named: "leftmirroricon"))
        frontPanelClicked.append(imageValue)
        
        let activateHeader = ActivationHeader(dictionary: [:])
        activateHeader.title = termsDataResponse?.photo_click_help_points?.back_panel?.title
        activateHeader.subTitle = termsDataResponse?.photo_click_help_points?.back_panel?.subTitle
        frontPanelClicked.append(activateHeader)
        
        termsDataResponse?.photo_click_help_points?.back_panel?.points?.forEach({frontPanelClicked.append($0)})
        
        viewController?.displayInitialData(dataSource: dataSource, termsData: termsDataSource, frontPanelClickedData: frontPanelClicked)
    }
    
    
    func validateImagesResposne(frontPanelName: String?, frontPanelResposne: UploadInvoiceFDResponseDTO?, frontPanelError: Error?, backPanelName: String?, backPanelResposne: UploadInvoiceFDResponseDTO?, backPanelError: Error?) {
        do {
            if let _ = frontPanelName {
                try Utilities.checkError(frontPanelResposne, error: frontPanelError)
            }
            
            if let _ = backPanelName {
               try Utilities.checkError(backPanelResposne, error: backPanelError)
            }
            viewController?.displayUploadedImageSuccess(frontPanelname: frontPanelName, frontPaneldata: frontPanelResposne?.data, backPanelname: backPanelResposne?.data, backPaneldata: backPanelName)
            
        } catch {
            viewController?.displayImageFailure(error: error.localizedDescription, with: frontPanelName)
        }
    }
    
    func submitDocsResponseReceived(response: SubmitDocResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.submitDocsSuccess()
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
    
}
