//
//  ImagePreviewDescriptionCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 27/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

struct ImagePreviewDescriptionVM {
    var image: UIImage?
    var description: String?
}

class ImagePreviewDescriptionCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: H3BoldLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        previewImageView.layer.borderColor = UIColor.dlsShadow.cgColor
        previewImageView.layer.borderWidth = 1
        previewImageView.layer.cornerRadius = 8
    }
    
    func setViewModel(model: ImagePreviewDescriptionVM?) {
        previewImageView.image = model?.image
        descriptionLabel.text = model?.description
    }
}
