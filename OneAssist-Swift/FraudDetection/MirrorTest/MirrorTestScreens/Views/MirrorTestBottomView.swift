//
//  MirrorTestBottomView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 20/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

enum BottomSheetType: String {
    case retryFrontPanel = "retryFrontPanel"
    case successFrontPanel = "successFrontPanel"
    case submission = "submission"
}

@objc protocol MirrorTestBottomViewDelegate: AnyObject {
    func onClickTryAgain()
    @objc optional func onClickContinue()
    @objc optional func onClickChatStart()
}

class MirrorTestBottomView: UIView, UITableViewDelegate, UITableViewDataSource, CommonMirrorTestDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var bottomSheetView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var securityView: UIView!
    @IBOutlet weak var securityTextLabel: UILabel!
    
    var dataSource: [AnyObject] = []
    var sheetType: BottomSheetType = .retryFrontPanel
    weak var delegate: MirrorTestBottomViewDelegate?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCornersWithBorderAndShadow(UIRectCorner(arrayLiteral: [.topLeft, .topRight]), radius: 8, with: 0, and: nil, with: nil, and: nil, and: nil, and: nil)
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("MirrorTestBottomView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        tableView.register(cell: TitleSubtitleTableViewCell.self)
        tableView.register(cell: VideoGuideTableViewCell.self)
        tableView.register(cell: SamplePhotosTableViewCell.self)
        tableView.register(cell: LeftImageTitleCell.self)
        tableView.register(cell: ChatTableViewCell.self)
        tableView.register(cell: KnowMoreTableViewCell.self)
        tableView.register(cell: QuickTipsCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
    }
    
    func setViewModel(_ model: [AnyObject], sheetType: BottomSheetType) {
        self.sheetType = sheetType
           self.dataSource = model
           tableView.reloadData()
        if self.sheetType == .successFrontPanel {
            self.continueButton.setTitle(Strings.FDetectionConstants.strContinue, for: .normal)
            self.securityView.isHidden = true
        } else if self.sheetType == .submission {
            self.continueButton.setTitle(Strings.FDetectionConstants.submitPhotos, for: .normal)
            self.securityView.isHidden = true
        } else {
            self.continueButton.setTitle(Strings.FDetectionConstants.tryAgain, for: .normal)
            self.securityView.isHidden = true
        }
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return dataSource.count
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataSourceObj = dataSource[indexPath.row]
        
        switch dataSourceObj {
        case is ActivationHeader:
            let cell: TitleSubtitleTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModelForBottomSheet(dataSourceObj as? ActivationHeader)
            return cell
        case is ImageWithValue:
            let cell: QuickTipsCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model: dataSourceObj as! ImageWithValue)
            return cell
        case is LeftImageTitleCellVM:
            let cell: LeftImageTitleCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(dataSourceObj as! LeftImageTitleCellVM)
            return cell
        case is Facts:
            let cell: KnowMoreTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(dataSourceObj as? Facts)
            return cell
        case is ChatCellModel:
            let cell: ChatTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.delegate = self
            cell.setViewModel(dataSourceObj as? ChatCellModel)
            return cell
            
        default: break
            
        }
        return UITableViewCell()
    }
    
    @IBAction func onClickContinueAction(_ sender: UIButton?){
        if self.sheetType == .successFrontPanel {
            delegate?.onClickContinue?()
        }else {
            self.delegate?.onClickTryAgain()
        }
    }
    
    
    class func height(forModel model:[AnyObject], with extraHeight: CGFloat) -> CGFloat {
        var height : CGFloat = 0.0
        var contentHeight: CGFloat = 0.0
        for dataSourceObj in model {
            switch dataSourceObj {
            case is ActivationHeader:
                contentHeight = contentHeight + TitleSubtitleTableViewCell.heightForBottomSheetModel(dataSourceObj as? ActivationHeader, totalWidth: screensize.width)
            case is ImageWithValue:
                contentHeight = contentHeight + QuickTipsCell.heightForBottomSheetModel(dataSourceObj as? ImageWithValue, totalWidth: screensize.width)
            case is LeftImageTitleCellVM:
                contentHeight = contentHeight + LeftImageTitleCell.heightForBottomSheetModel(dataSourceObj as? LeftImageTitleCellVM, totalWidth: screensize.width)
            case is Facts:
                contentHeight = contentHeight + KnowMoreTableViewCell.heightForBottomSheetModel(dataSourceObj as? Facts, totalWidth: screensize.width)
            case is ChatCellModel:
                contentHeight = contentHeight + ChatTableViewCell.heightForBottomSheetModel(dataSourceObj as? ChatCellModel, totalWidth: screensize.width)
            default: break
                
            }
        }
        height = contentHeight + extraHeight
        return height+10
    }
}

extension MirrorTestBottomView: ChatCellDelegate {
    func onClickChat() {
        self.delegate?.onClickChatStart?()
    }
}
