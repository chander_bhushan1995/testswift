//
//  FrontPanelView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class FrontPanelView: UIView {
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var leftNumberLabel: H1BoldLabel!
    @IBOutlet weak var rightNumberLabel: H1BoldLabel!
    @IBOutlet weak var errorLabel: H1BoldLabel!
    
    @IBOutlet weak var imeiNumberLabel: H1BoldLabel!
    @IBOutlet weak var imeiSecondLabel: H1BoldLabel!
    @IBOutlet weak var hashStringLabel: H2BoldLabel!
    
    @IBOutlet var extremeLabelConstraint: NSLayoutConstraint!
    @IBOutlet var hashTopConstraint: NSLayoutConstraint!
    
    var primaryIMEINumber: String? = nil {
        didSet {
            imeiNumberLabel.text = primaryIMEINumber
            imeiSecondLabel.isHidden = true
            hashTopConstraint.isActive = false
            extremeLabelConstraint.priority = .defaultHigh
        }
    }
    
    var secondaryIMEINumber: String? = nil {
        didSet {
            if !(secondaryIMEINumber?.isEmpty ?? true) {
                imeiNumberLabel.text = primaryIMEINumber
                imeiSecondLabel.text = secondaryIMEINumber
                imeiSecondLabel.isHidden = false
                extremeLabelConstraint.priority = .defaultLow
                hashTopConstraint.isActive = true
            }
        }
    }
    
    var hashString: String? = nil {
        didSet {
            hashStringLabel.text = hashString
            hashStringLabel.isHidden = hashString?.isEmpty ?? true
        }
    }
    
    var errorText: String? = nil {
        didSet {
            errorLabel?.text = errorText
            errorLabel?.isHidden = errorText?.isEmpty ?? true
        }
    }
    
    var videoGuideTip: PopTip {
        let popTip = PopTip()
        popTip.shouldShowMask = false
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
        popTip.shouldDismissOnSwipeOutside = true
        popTip.bubbleColor = UIColor.black
        popTip.font = DLSFont.bodyText.bold
        popTip.edgeMargin = 16
        popTip.offset = -15
        return popTip
    }
    
    deinit {
        print("FrontPanelView deinit")
    }
    
    func setupValue() {
        leftNumberLabel.font = DLSFont.h0.bold
        rightNumberLabel.font = DLSFont.h0.bold
        leftNumberLabel.text = LeftLabelText
        rightNumberLabel.text = RightLabelText
        errorLabel.textColor = .buttonBlue
        errorLabel.font = DLSFont.h1Large.bold
        leftNumberLabel.transform = CGAffineTransform(scaleX: -1, y: 1);
        rightNumberLabel.transform = CGAffineTransform(scaleX: -1, y: 1);
        errorLabel.transform = CGAffineTransform(scaleX: -1, y: 1);
        imeiNumberLabel.transform = CGAffineTransform(scaleX: -1, y: 1);
        imeiSecondLabel.transform = CGAffineTransform(scaleX: -1, y: 1);
        hashStringLabel.transform = CGAffineTransform(scaleX: -1, y: 1);
        imeiNumberLabel.text = primaryIMEINumber
        imeiSecondLabel.text = secondaryIMEINumber
        hashStringLabel.text = hashString
    }
    
    func addInSuperView(_ view: UIView){
        view.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        self.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        self.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        setupValue()
        showVideoGuideToolTip()
    }
    
    func showVideoGuideToolTip() {
        self.videoGuideTip.isHidden = false
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) {[weak self] (timer) in
            DispatchQueue.main.async {
                guard let self = self else {return}
                self.videoGuideTip.show(text: Strings.FDetectionConstants.seeVideoGuide, direction: .down, maxWidth: 200, in: self, from: self.menuButton.frame, duration: 2)
            }
        }
    }
}
