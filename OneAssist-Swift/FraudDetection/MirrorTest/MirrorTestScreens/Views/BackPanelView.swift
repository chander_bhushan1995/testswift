//
//  BackPanelView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class BackPanelView: UIView {
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var infoIcon: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var cameragridImageView: UIImageView!
    @IBOutlet weak var cameraclickButton: UIButton!
    
    var taptoClickToolTip: PopTip {
        let popTip = PopTip()
        popTip.shouldShowMask = false
        popTip.shouldDismissOnTap = false
        popTip.shouldDismissOnTapOutside = true
        popTip.shouldDismissOnSwipeOutside = false
        popTip.bubbleColor = UIColor.white
        popTip.font = DLSFont.bodyText.bold
        popTip.textColor = .black2
        return popTip
    }
    
    deinit {
        invalidateTimer()
        print("BackPanelView deinit")
    }
    
    var timer: Timer?
    func addInSuperView(_ view: UIView){
        view.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        self.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        self.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 5.0
        self.bottomView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
    }
    
    func setupAnimation(_ text: [String]?) {
        if let animationText = text {
            self.bottomView.isHidden = false
            var i: Int = 0
            self.infoLabel.text = animationText[i]
            i = i + 1
            timer = Timer.scheduledTimer(withTimeInterval: 4, repeats: true) {[weak self] (timer) in
                guard let self = self else {
                    return
                    
                }
                if i >= animationText.count {
                    i = 0
                }
                self.bottomView.fadeOut {
                    (finished: Bool) -> Void in
                    DispatchQueue.main.async {
                        self.infoLabel.text = animationText[i]
                    }
                }
                
                self.bottomView.fadeIn(completion: {
                    (finished: Bool) -> Void in
                    i = i + 1
                })
            }
        }else {
            self.bottomView.isHidden = true
        }
        
    }
    
    func invalidateTimer() {
        if self.timer != nil {
            timer?.invalidate()
            self.timer = nil
        }
    }
    
    func showTapToClickToolTip() {
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) {[weak self] (timer) in
            DispatchQueue.main.async {
                guard let self = self else {return}
                self.taptoClickToolTip.show(text: Strings.iOSFraudDetection.taptoClick, direction: .up, maxWidth: 200, in: self, from: self.cameraclickButton.frame, duration: 2)
            }
        }
    }
}
