//
//  MirrorImagePreviewViewController.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 26/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol MirrorImagePreviewDelegate: class {
    func onClickRetakePhoto(_ model: SinglePanelImageModel?)
}

class MirrorImagePreviewViewController: BaseVC {
    @IBOutlet weak var retakePhotoButton: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
    
    var dataSource: SinglePanelImageModel?
    
    var delegate: MirrorImagePreviewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
        // Do any additional setup after loading the view.
    }

    private func initializeView() {
        let image = UIImage(named: "compare_cross")?.withRenderingMode(.alwaysTemplate)
        crossButton.setImage(image, for: .normal)
        crossButton.tintColor = UIColor.white
        
        imageContainerView.clipsToBounds = true
        imageContainerView.layer.cornerRadius = 5.0
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 3.0
        
        if let image = dataSource?.image {
            imageView.contentMode = .scaleAspectFill;
               if (imageView.bounds.size.width > image.size.width && imageView.bounds.size.height > image.size.height) {
                imageView.contentMode = .scaleAspectFit;
               }
            imageView.image = image
        }
        
        
    }
    
    
    @IBAction func onClickRetakePhoto(_ sender: UIButton?){
        self.delegate?.onClickRetakePhoto(dataSource)
    }
    
    @IBAction func onClickCrossAction(_ sender: UIButton?){
        self.dismiss(animated: true, completion: nil)
    }
}
