//
//  MTEnumsConstant.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

let LeftLabelText = "69"
let RightLabelText = "37"

enum Constant {
    static let videoDataOutputQueueLabel = "com.mirrorpoc.Queue"
    static let sessionQueueLabel = "com.mirrorpoc.visiondetector.SessionQueue"
}

enum CameraType {
    case FrontPanel
    case BackPanel
}
