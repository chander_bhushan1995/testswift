//
//  MirrorTestPreviewInterector.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 27/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

protocol PanelPreviewPresenterLogic {
    func presentInitialData(capturedImage: UIImage?,imageCapturedFor: CameraType, termsDataResponse: PhotoClickHelpPointsDTO?, termsDataError: Error?)
    func validateImagesResposne(frontPanelName: String?, frontPanelResposne: UploadInvoiceFDResponseDTO?, frontPanelError: Error?, backPanelName: String?, backPanelResposne: UploadInvoiceFDResponseDTO?, backPanelError: Error?)
    func submitDocsResponseReceived(response: SubmitDocResponseDTO?, error: Error?)
}


class PanelPreviewInterector: PanelPreviewBusinessLogic {
    
    var presenter: PanelPreviewPresenterLogic?
    var termsDataResponse: PhotoClickHelpPointsDTO?
    var termsDataError: Error?
    
    var frontPanelName: String?
    var frontPanelResposne: UploadInvoiceFDResponseDTO?
    var frontPanelError: Error?
    
    var backPanelName: String?
    var backPanelResposne: UploadInvoiceFDResponseDTO?
    var backPanelError: Error?
    
    func loadInitialData(capturedImage: UIImage?,imageCapturedFor: CameraType){
        let group = DispatchGroup()
        
        group.enter()
        let _ = GetPhotoHelpPointsUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            group.leave()
            guard let self = self else { return }
            self.termsDataResponse = response
            self.termsDataError = error
        }
        group.notify(queue: .main) {[weak self] in
            guard let self = self else { return }
            self.presenter?.presentInitialData(capturedImage: capturedImage, imageCapturedFor: imageCapturedFor, termsDataResponse: self.termsDataResponse, termsDataError: self.termsDataError)
        }
    }
    
    func uploadFDDocuments(for frontPanel: UploadInvoiceFDRequestDTO?, backPanel: UploadInvoiceFDRequestDTO?) {
        
        //both panel
        if let frontPanelReq = frontPanel,
            let backPanelReq = backPanel {
            print("front/ back pannel api hit")
            let _ = UploadInvoiceFDUseCase.service(requestDTO: frontPanelReq) {[weak self] (usecase, response, error) in
                guard let self = self else { return }
                self.frontPanelName = frontPanelReq.name
                print("front pannel api resposne \(String(describing: response?.data))")
                self.frontPanelResposne = response
                self.frontPanelError = error
                
                let _ = UploadInvoiceFDUseCase.service(requestDTO: backPanelReq) {[weak self] (usecase, response, error) in
                    guard let self = self else { return }
                    self.backPanelName = backPanelReq.name
                    print("back pannel api resposne \(String(describing: response?.data))")
                    self.backPanelResposne = response
                    self.backPanelError = error
                    
                    self.callPresenterLogic()
                }
            }
            
        }else if let frontPanelReq = frontPanel { // only front panel
            print("front/ back pannel api hit")
            let _ = UploadInvoiceFDUseCase.service(requestDTO: frontPanelReq) {[weak self] (usecase, response, error) in
                guard let self = self else { return }
                self.frontPanelName = frontPanelReq.name
                print("front pannel api resposne \(String(describing: response?.data))")
                self.frontPanelResposne = response
                self.frontPanelError = error
                
                self.callPresenterLogic()
            }
            
        }else if let backPanelReq = backPanel { // only back panel
            print("back pannel api hit")
            let _ = UploadInvoiceFDUseCase.service(requestDTO: backPanelReq) {[weak self] (usecase, response, error) in
                guard let self = self else { return }
                self.backPanelName = backPanelReq.name
                print("back pannel api resposne \(String(describing: response?.data))")
                self.backPanelResposne = response
                self.backPanelError = error
                
                self.callPresenterLogic()
            }
        }
    }
    
    private func callPresenterLogic() {
        self.presenter?.validateImagesResposne(frontPanelName: self.frontPanelName, frontPanelResposne: self.frontPanelResposne, frontPanelError: self.frontPanelError, backPanelName: self.backPanelName, backPanelResposne: self.backPanelResposne, backPanelError: self.backPanelError)
    }
    
    func submitDocs(for activityRefId: String?) {
        let req = SubmitDocRequestDTO(activityRefId: activityRefId)
        
        let _ = SubmitDocUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.submitDocsResponseReceived(response: response, error: error)
        }
    }
}
