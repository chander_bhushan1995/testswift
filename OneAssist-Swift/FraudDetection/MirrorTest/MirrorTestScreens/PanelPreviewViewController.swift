//
//  MirrorTestPreviewViewController.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 27/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

//MARK: - PanelPreviewDelegate
protocol PanelPreviewDelegate: NSObject {
    func retakePhoto(for: CameraType)
}

//MARK: Interector Methods
protocol PanelPreviewBusinessLogic: AnyObject {
    func loadInitialData(capturedImage: UIImage?,imageCapturedFor: CameraType)
    func uploadFDDocuments(for frontPanel: UploadInvoiceFDRequestDTO?, backPanel: UploadInvoiceFDRequestDTO?)
    func submitDocs(for activityRefId: String?)
}


class PanelPreviewViewController: BaseVC, CommonMirrorTestDelegate, DismissPresentedViewInStack {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var continueButton: OAPrimaryButton!
    @IBOutlet weak var retakePhotoButton: OASecondaryButton!
    
    //MARK: - properties
    var imageCapturedFor: CameraType = .FrontPanel
    var capturedImage: UIImage?
    var submissionBottomSheetData: [AnyObject] = []
    var frontPanelSuccessData: [AnyObject] = []
    var dataSource: [Any] = []
    var interactor: PanelPreviewBusinessLogic?
    var enableVoiceSupport: Bool = false
    var isDismissedInStack: Bool = false
    weak var delegate: PanelPreviewDelegate?
    
    //MARK: - Life Cycle Calls
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isDismissedInStack {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var frame = CGRect.zero
        frame.size.height = 10
        tableView.tableHeaderView = UIView(frame: frame)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cell: CheckBoxAndTitleCell.self)
        tableView.register(cell: ImagePreviewDescriptionCell.self)
        retakePhotoButton.setTitle(Strings.MirrorTestPreviewScreen.noRetakePhoto, for: .normal)
        continueButton.setTitle(Strings.MirrorTestPreviewScreen.yesContinue, for: .normal)
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        interactor?.loadInitialData(capturedImage: capturedImage, imageCapturedFor: imageCapturedFor)
        if let langauge = FDCacheManager.shared.selectLanguageForMirrorTest {
            enableVoiceSupport = true
        }
    }
    
    deinit {
        print("PanelPreviewViewController deinit")
    }
    
    func retakeImage() {
        if imageCapturedFor == .FrontPanel {
            EventTracking.shared.eventTracking(name: .retakeFrontImage, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .screen: Event.EventParams.previewScreen])
            delegate?.retakePhoto(for: .FrontPanel)
        } else {
            EventTracking.shared.eventTracking(name: .retakeBackImage, [.voice: self.enableVoiceSupport ? "Enabled" : "Disabled" , .mode: FDCacheManager.shared.fdTestType.rawValue, .screen: Event.EventParams.previewScreen])
            delegate?.retakePhoto(for: .BackPanel)
        }
    }
    
    func enableOrDisablePrimaryButton() {
        if let _ = dataSource.compactMap({$0 as? CheckBoxAndTitleCellVM}).filter({!$0.isEnabled}).first {
            continueButton.isEnabled = false
        } else {
            continueButton.isEnabled = true
        }
    }
    
    func updateImageAfterRetake(image: UIImage?) {
        capturedImage = image
        dataSource = dataSource.map({ cellVM in
            if var viewModel = cellVM as? ImagePreviewDescriptionVM {
                viewModel.image = capturedImage
                return viewModel
            }
            return cellVM
        })
        tableView.reloadData()
    }
    
    //MARK: - IBActions
    @IBAction func onClickCross(_ sender: Any) {
        Utilities.topMostPresenterViewController.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickRetake(_ sender: Any) {
        self.retakeImage()
    }
    
    @IBAction func onClickContinue(_ sender: Any) {
        
        if ((imageCapturedFor == .FrontPanel && FDCacheManager.shared.imageCaptureType != .ReuploadFrontPanel)) || (FDCacheManager.shared.imageCaptureType == .ReuploadBoth && imageCapturedFor == .FrontPanel) {
            let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
            if let saveObject = SaveFDDataMode.getSaveData(orderId) {
                // back panel image already there
                if let mtBackPanelName = saveObject.mtBackPanelName {
                    self.openPanelPreview(with: DataDiskStogare.getImageFromDocumentDirectory(filename: mtBackPanelName), for: .BackPanel)
                } else {
                    self.showFrontPanelClickedBottomSheet()
                }
            }
            return
        }
        EventTracking.shared.eventTracking(name: .submitImageConfirmation, [.location: Event.EventParams.mobileFDActivation ,.mode: FDCacheManager.shared.fdTestType.rawValue, .voice: enableVoiceSupport ? "Enabled" : "Disabled"])
        self.showSubmissionBottomSheet()
    }
}

//MARK: - TableView Datasource & Delegate
extension PanelPreviewViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataSourceObj = dataSource[indexPath.row]
        
        switch dataSourceObj {
        case is CheckBoxAndTitleCellVM:
            let cell: CheckBoxAndTitleCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model: dataSourceObj as? CheckBoxAndTitleCellVM)
            return cell
        case is ImagePreviewDescriptionVM:
            let cell: ImagePreviewDescriptionCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model: dataSourceObj as? ImagePreviewDescriptionVM)
            return cell
        default: break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tappedCellVM = dataSource[indexPath.row]
        if var checkBoxVM = tappedCellVM as? CheckBoxAndTitleCellVM {
            checkBoxVM.isEnabled.toggle()
            dataSource[indexPath.row] = checkBoxVM
            tableView.reloadData()
        }
        self.enableOrDisablePrimaryButton()
    }
}

//MARK: - Routing Logic
extension PanelPreviewViewController {
    func showSubmissionBottomSheet() {
        let vc = MirrorTestBottomView()
        vc.setViewModel(self.submissionBottomSheetData, sheetType: .submission)
        vc.delegate = self
        Utilities.presentPopover(view: vc, height: MirrorTestBottomView.height(forModel: self.submissionBottomSheetData, with: 80))
    }
    
    func showFrontPanelClickedBottomSheet() {
        let vc = MirrorTestBottomView()
        vc.setViewModel(self.frontPanelSuccessData, sheetType: .successFrontPanel)
        vc.delegate = self
        Utilities.presentPopover(view: vc, height: MirrorTestBottomView.height(forModel: self.frontPanelSuccessData, with: 0))
    }
    
    class func removeFromStack(for: CameraType) {
        let viewController: PanelPreviewViewController? = Utilities.getPresentedVCInStack()
        if let previewController = viewController, previewController.imageCapturedFor == `for` {
            previewController.setDismissProperty()
        }
    }
}

//MARK: - Display Logic
extension PanelPreviewViewController: PanelPreviewDisplayLogic {
    
    func displayInitialData(dataSource: [Any], termsData: [AnyObject], frontPanelClickedData: [AnyObject]) {
        Utilities.removeLoader(fromView: view, count: &self.loaderCount)
        self.submissionBottomSheetData = termsData
        self.dataSource = dataSource
        self.frontPanelSuccessData = frontPanelClickedData
        tableView.reloadData()
        self.enableOrDisablePrimaryButton()
    }
    
    func displayUploadedImageSuccess(frontPanelname: String?, frontPaneldata: String?, backPanelname: String?, backPaneldata: String?) {
        if let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
            if let verificationDoc = verificationDocs.filter({ $0.name == frontPanelname }).first {
                verificationDoc.value = frontPaneldata
            }
        }
        
        if let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
            if let verificationDoc = verificationDocs.filter({ $0.name == backPanelname }).first {
                verificationDoc.value = backPaneldata
            }
        }
        
        interactor?.submitDocs(for: FDCacheManager.shared.getDocsToUploadModel?.activityReferenceId)
    }
    
    func displayImageFailure(error: String?, with name: String?) {
        Utilities.removeLoader(count: &self.loaderCount)
        if let error = error {
           showAlert(message: error)
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &self.loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
    
    func submitDocsSuccess() {
        Utilities.removeLoader(count: &self.loaderCount)
        FDCacheManager.shared.deleteImagesIntoDirectory()
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        Utilities.dismissAllPresentedVC() {
            if let rootVC = Utilities.topMostPresenterViewController.popToRootView() as? TabVC {
                rootVC.routeToMirrorTestRatingScreen()
            }
        }
    }
}

//MARK: - MirrorTestBottomViewDelegate
extension PanelPreviewViewController: MirrorTestBottomViewDelegate {
    // On clicked primary button of terms & conditions bottomsheet before hitting submission API
    func onClickTryAgain() {
        //submit photos
        Utilities.addLoader(message: Strings.RatingFeedback.pleaseWait, count: &loaderCount, isInteractionEnabled: false)
        self.submitPhotos()
    }
    
    func onClickContinue() {
        EventTracking.shared.eventTracking(name: .continueToBackPanel, [.mode: FDCacheManager.shared.fdTestType.rawValue, .voice: enableVoiceSupport ? "Enabled" : "Disabled", .screen: Event.EventParams.backPanelInstruction])
        Utilities.topMostPresenterViewController.dismiss(animated: false) {
            self.redirectToMirrorTestScreen(isCaptureFront: false)
        }
    }
    
    func submitPhotos() {
        EventTracking.shared.eventTracking(name: .submitImages, [.declaration: "Yes", .mode: FDCacheManager.shared.fdTestType.rawValue, .voice: enableVoiceSupport ? "Enabled" : "Disabled"])
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue

        var frontPanelRequest: UploadInvoiceFDRequestDTO?
        var backPanelRequest: UploadInvoiceFDRequestDTO?
        
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            if let frontPanelImage = saveObject.mtFrontPanelName {
                if let frontPanelImageData = DataDiskStogare.getDataFromDocumentDirectory(filename: frontPanelImage) {
                    frontPanelRequest = self.getImageRequest(docName: Constants.RequestKeys.frontPanel, fileName: "front-panel-apple.png", imageData: frontPanelImageData)
                }
            }
            
            if let backPanelImage = saveObject.mtBackPanelName {
                if let backPanelImageData = DataDiskStogare.getDataFromDocumentDirectory(filename: backPanelImage) {
                    backPanelRequest = self.getImageRequest(docName: Constants.RequestKeys.backPanel, fileName: "back-panel-apple.png", imageData: backPanelImageData)
                }
            }
        }
        interactor?.uploadFDDocuments(for: frontPanelRequest, backPanel: backPanelRequest)
    }
    
    func getImageRequest(docName: String, fileName: String, imageData: Data) -> UploadInvoiceFDRequestDTO? {
        var verificationDoc: VerificationDocument?
        
        if let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.filter({
            ($0.productCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.verificationDocuments {
            verificationDoc = verificationDocs.filter({ $0.name == docName }).first
        }
        
        var verifiedFileName = fileName
        
        if let fileName = verificationDoc?.sampleImageName {
            verifiedFileName = fileName
        }
        
        let pathExtention = (verifiedFileName as NSString).pathExtension
         
        let retuest: UploadInvoiceFDRequestDTO? = UploadInvoiceFDRequestDTO(activityReferenceId: FDCacheManager.shared.getDocsToUploadModel?.activityReferenceId, name: verificationDoc?.name, fileName: verifiedFileName, docFileContentType: "image/.\(pathExtention.lowercased())", displayName: verificationDoc?.displayName, docMandatory: verificationDoc?.docMandatory, uploadedFile: imageData)
        
        return retuest
    }
}

//MARK: - Dependency Injection
extension PanelPreviewViewController {
    fileprivate func setupDependencyConfigurator() {
        let interactor = PanelPreviewInterector()
        let presenter = PanelPreviewPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

