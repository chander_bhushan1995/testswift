//
//  ChatBridge.m
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/09/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTModuleData.h>
#import <UIKit/UIKit.h>

@interface RCT_EXTERN_MODULE(ChatBridge, NSObject)

RCT_EXTERN_METHOD(goBack)
RCT_EXTERN_METHOD(goBackWithAmnimatedParam:(BOOL)animated)

RCT_EXTERN_METHOD(disconnectSocket)

RCT_EXTERN_METHOD(connectSocketWithPassword:(nonnull NSString *)password callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(reconnectSocket)

RCT_EXTERN_METHOD(uploadDocument:(NSString *)imageUri callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(showLocalNotification:(NSString *)notificationMessage)

RCT_EXTERN_METHOD(imagePickerPermissionDenied:(NSString *)type)

RCT_EXTERN_METHOD(getUsername:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(getImageUrl:(NSString *)productCode callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(getAppVersion:(RCTResponseSenderBlock)callback)

RCT_EXPORT_METHOD(openURLiOS:(NSString *)URL)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL opened = [UIApplication.sharedApplication openURL:[NSURL URLWithString:URL]];
    });
}

RCT_EXTERN_METHOD(getApiProperty:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(getUserInfo:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(getBuybackCouponCode:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(openChat)

RCT_EXTERN_METHOD(openChatWithInitialPrams: (nonnull NSDictionary *)withIntialProps)

RCT_EXTERN_METHOD(openFavoriteRecommendations)

RCT_EXTERN_METHOD(openRating:(NSString *)quoteId servedBy:(NSString *)servedBy isAbb:(nonnull BOOL *)isAbb)

RCT_EXTERN_METHOD(updateFavoriteRecommendation:(NSString *)rId uiComponentId:(NSString *)uiComponentId isFavorite:(nonnull BOOL *)isFavorite)

RCT_EXTERN_METHOD(getRecommendationProps:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(openMHC:(NSString *)questionArray resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(checkMHC:(NSString *)questionArray resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getBuybackTAndCUrl:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(getPrePostOnboardingFormFields:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(openTabBarWithIndex:(nonnull NSNumber *)index)

RCT_EXTERN_METHOD(openMemTab)
RCT_EXTERN_METHOD(startFDFlow:(NSString *)productCode tempCustomerInfo:(NSDictionary *)tempCustomerInfo)
RCT_EXTERN_METHOD(openScheduleInspectionFlow:(NSString *)activationCode pincode:(NSString *))

RCT_EXTERN_METHOD(shareLink:(NSString *)textToShare)
RCT_EXTERN_METHOD(getNameForSubCategory:(NSString *)productCode callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getProductTerms:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getAutoSuggestionsData:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(downloadDocumentFromSP:(NSString *)fileTypeRequired storageRefId:(NSString *)storageRefId callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(downloadTemplate:(NSString *)draftId callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(paymentSuccessful)

RCT_EXTERN_METHOD(getFMIPData:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getMILogoutData:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getBBQuestionsRemoteVersion:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getBuybackDeclarationData:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(updateUserInfo: (nonnull NSDictionary *)userInfo)
RCT_EXTERN_METHOD(refreshBuybackStatus)

//for SOD
RCT_EXTERN_METHOD(showSRDetails:(NSString *)stringifiedData)
RCT_EXTERN_METHOD(rateCompletedService:(NSString *)stringifiedData)
RCT_EXTERN_METHOD(rescheduleServiceRequest:(NSString *)stringifiedData)
RCT_EXTERN_METHOD(showPaymentPending:(NSString *)stringifiedData)
RCT_EXTERN_METHOD(rescheduleOnHoldRequest:(NSString *)stringifiedData)
RCT_EXTERN_METHOD(sendScheduleVisitEvent:(NSString *)location appliance: (NSString *)appliance count: (NSString *)count serviceType: (NSString *)serviceType cost: (NSString *)cost date: (NSString *)date time: (NSString *)time)
RCT_EXTERN_METHOD(sendSODDetailsSubmitEvent:(NSString *)appliance count: (NSString *)count serviceType: (NSString *)serviceType cost: (NSString *)cost)
RCT_EXTERN_METHOD(logApppsFlyerEvent:(NSString *)eventName eventAttributes: (NSDictionary *)eventAttributes)
RCT_EXTERN_METHOD(sendPaymentSuccessEvent:(NSString *)eventData)

RCT_EXTERN_METHOD(goToSettings)
RCT_EXTERN_METHOD(switchValueChanged:(nonnull BOOL *)isSwitchEnabled)

//for CATALYST
RCT_EXTERN_METHOD(routeToExplorePlans:(nonnull NSString *)url)
RCT_EXTERN_METHOD(processDeeplink:(nonnull NSString *)url eventData:(NSDictionary *)eventData)

RCT_EXTERN_METHOD(getRewardFeatureOnStatus:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(sodAppliancesMapping:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getOneAssistPromiseData:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getPlansCategoryData:(RCTResponseSenderBlock)callback)


//RCT_EXTERN_METHOD(shareLink)
RCT_EXTERN_METHOD(goToMyProfile)
RCT_EXTERN_METHOD(goToMHCMainNavigator: (nonnull NSString *)startMHC mhcCategory:(NSDictionary *)mhcCategory)
RCT_EXTERN_METHOD(startHAFlow)
RCT_EXTERN_METHOD(startEWFlow)
RCT_EXTERN_METHOD(startQuestionScreen: (nonnull NSArray *)questions totalQuestions:(nonnull NSNumber *)totalQuestions currentQuestionNumber:(nonnull NSNumber *)currentQuestionNumber flowType: (nonnull NSString *)flowType)
RCT_EXTERN_METHOD(editQuestionsScreen: (nonnull NSArray *)questions)
RCT_EXTERN_METHOD(goToATMsBankScreen: (nonnull NSString *) destination)
RCT_EXTERN_METHOD(startBuyBack: (nonnull NSDictionary *) buybackData deeplinkURL:(nullable NSString *)deeplinkURL)
RCT_EXTERN_METHOD(handleSpotLight: (nonnull NSString *) deepLinkURL)
RCT_EXTERN_METHOD(updateProfile)
RCT_EXTERN_METHOD(sendCustomerDetailsUpdatedEvent)
RCT_EXTERN_METHOD(sendBuyIDFenceEventFromRiskCalculator)
RCT_EXTERN_METHOD(setVerifyEmailDismissed: (BOOL)dismissed)
RCT_EXTERN_METHOD(updateCustomerDetails: (nonnull NSString *)data)
RCT_EXTERN_METHOD(routeToCardManagement)
RCT_EXTERN_METHOD(viewAllOffers:(nonnull NSString *)offerData category: (nonnull NSString *)category fromScreen: (nonnull NSString *)fromScreen selectedIndex: (nonnull NSNumber *)selectedIndex)
RCT_EXTERN_METHOD(startMyRewardScreen:(BOOL)isAnsweredAllQuestions showScratchCard:(BOOL)showScratchCard)
RCT_EXTERN_METHOD(startEditDetails)
RCT_EXTERN_METHOD(openScratchCard:(nonnull NSString *)state)

RCT_EXTERN_METHOD(updateRewardState:(nonnull NSString *)state)
RCT_EXTERN_METHOD(routeToOAIDFenceVC: (nonnull NSString *)membershipDataText renewalDataStrigified:(NSString *)renewalDataStrigified)
RCT_EXTERN_METHOD(refreshMemberships)
RCT_EXTERN_METHOD(openLogin:(BOOL)isFromWelcome)
RCT_EXTERN_METHOD(handleSODFlow:(nonnull NSString *)deeplinkURL eventParams:(NSDictionary *)eventParams)
RCT_EXTERN_METHOD(showMyServiceRequestsScreen)
RCT_EXTERN_METHOD(getWebEngageAttr:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(openLoginAsRoot)

RCT_EXTERN_METHOD(openLoginWithCallback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(gotoTabbar:(BOOL)isSkipVerification isShowRewardCard: (BOOL)isShowRewardCard isFromBoardingQ: (BOOL)isFromBoardingQ)
RCT_EXTERN_METHOD(goToBookMarkOffers)

RCT_EXTERN_METHOD(openWebView:(nonnull NSString *)param title: (nonnull NSString *)title url: (nonnull NSString *)url)

RCT_EXTERN_METHOD(showPopupForVerifyNumber: (NSString *)title subTitle: (NSString *)subTitle source: (NSString *)source  callback:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(goToActivateVoucher:(nonnull NSString *)fromScreen)

RCT_EXTERN_METHOD(handleLogout)

RCT_EXTERN_METHOD(callForGetCustumerResposne:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(getIDFenceDashboardDetails:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(downloadDocument:(NSString *)urlStr fileName:(NSString *)fileName message:(NSString *)message)
RCT_EXTERN_METHOD(openIDFencePlanDetails:(BOOL)updatePaymentOrRenew url:(NSString *)url)

RCT_EXTERN_METHOD(saveReminderForWalletId:(nonnull NSNumber *)walletId date:(nonnull NSNumber *)date title:(nonnull NSString *)title)
RCT_EXTERN_METHOD(removeReminderForWalletId:(nonnull NSNumber *)walletId removeAll:(nonnull NSNumber *)removeAll)
RCT_EXTERN_METHOD(viewOffers:(nonnull NSString *)walletData)
RCT_EXTERN_METHOD(viewBankBranches:(nonnull NSString *)walletData)
RCT_EXTERN_METHOD(viewOnlineOfferDetail:(nonnull NSString *)offerData)
RCT_EXTERN_METHOD(submitRatingOnAppStore)
RCT_EXTERN_METHOD(refreshCards)
RCT_EXTERN_METHOD(dismissBottomSheet)

RCT_EXTERN_METHOD(getDataFromFirebase:(nonnull NSString *)firebaseKey callback: (RCTResponseSenderBlock *)callback)
RCT_EXTERN_METHOD(openBuyPlanJourney:(nonnull NSDictionary *)params)
RCT_EXTERN_METHOD(setOnBoardingData:(nonnull NSString *)data)
RCT_EXTERN_METHOD(getIssuers:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(saveIssuers:(NSString *)issuers)
RCT_EXTERN_METHOD(getSODProceedToPaymentSubTitle:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(openOCRflow:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getUserLocation:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getCDNDataVersioning:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(getAllMemberships:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(openPaymentModule:(nonnull NSDictionary *)params)
RCT_EXTERN_METHOD(openRiskCalculator:(nonnull NSDictionary *)params)

RCT_EXTERN_METHOD(openOSMap:(nonnull NSDictionary *)fromTo)
RCT_EXTERN_METHOD(showPopUpForStoreRating:(nonnull NSString *)location callback:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(openHARenewalFlow:(nonnull NSString *)paymentLinkURL)
RCT_EXTERN_METHOD(getBuySubtabSelectedIndex:(RCTResponseSenderBlock)callback)

RCT_EXTERN_METHOD(handleMembershipTabActions: (nonnull NSString *)action data:(nullable NSDictionary *)data)
RCT_EXTERN_METHOD(getProductSubcategories:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(routeToBuyback:(nonnull NSDictionary *)data)
RCT_EXTERN_METHOD(openPaymentFlow:(nonnull NSDictionary *)data)
RCT_EXTERN_METHOD(refreshOnGoingSRScreen:(NSDictionary *)jsonData)
RCT_EXTERN_METHOD(getMembershipResponse:(RCTResponseSenderBlock)callback)
RCT_EXTERN_METHOD(refreshTabBarMemberships)
RCT_EXTERN_METHOD(showBuybackServicePicker:(RCTResponseSenderBlock)callback)

// NEW METHODS TAB BAR
RCT_EXTERN_METHOD(updateMembershipDataOnNative:(NSDictionary *)jsonData)
RCT_EXTERN_METHOD(loadInitialTabBar:(NSDictionary *)jsonData)
RCT_EXTERN_METHOD(goToInboxScreen)
RCT_EXTERN_METHOD(tabChanged:(nonnull NSNumber *)index)


RCT_EXTERN_METHOD(showDownloadingErrorScreen:(nonnull NSString *)error)
RCT_EXTERN_METHOD(performPostDownload:(nonnull NSString *)activationCode)
RCT_EXTERN_METHOD(showDownloadingProgressScreen)

@end
