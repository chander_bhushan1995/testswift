//
//  ChatBridge+MyAccount.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 09/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation
import React

enum BUYBACK_STATUS : String {
    case BUYBACK_NOT_STARTED
    case SERVICEABILITY_CHECKED
    case MHC_COMPLETED
    case QUESTIONS_IN_PROGRESS
    case QUOTE_GENERATED
    case ORDER_PLACED
    case MODEL_NOT_FOUND
}

enum GoToATMsBank: String {
    case SHOW_ATM
    case SHOW_BANK
}

enum SelectedActionType:String{
    case CHAT = "CHAT"
    case TABSWITHCHED = "TABSWITHCHED"
}


extension ChatBridge {
    
    @objc func startHAFlow(){
        DispatchQueue.main.async {
            DeepLinkManager.handleLink(URL(string: "https://www.oneassist.in/sales/recommendation?category=HA&service=WHC"))
        }
    }
    
    @objc func startEWFlow() {
        DispatchQueue.main.async {
            DeepLinkManager.handleLink(URL(string: "https://www.oneassist.in/sales/recommendation?category=HA&service=EW"))
        }
    }
    
    @objc func openLogin(_ isFromWelcome: Bool) {
        DispatchQueue.main.async {
            let loginVC = LoginMobileVC()
            if let vc = Utilities.topMostPresenterViewController as? UINavigationController{
                vc.pushViewController(loginVC, animated: true)
            }
        }
    }
    
    @objc func openLoginWithCallback(_ callBack: @escaping RCTPromiseResolveBlock) {
        DispatchQueue.main.async {
            let loginVC = LoginMobileVC()
            loginVC.setUpLoginAction { (status) in
                callBack(["DONE"])
            }
            let navVC = BaseNavigationController(rootViewController: loginVC)
            Utilities.topMostPresenterViewController.presentInFullScreen(navVC, animated: true, completion: nil)
        }
    }
    
    @objc func openLoginAsRoot() {
        DispatchQueue.main.async {
            let vc = LoginMobileVC()
            vc.isRootToWindow = true
            ChatBridge.setRootWithVC(vc)
        }
    }
    
    /// redirects to tab bar
    ///
    /// - Parameters:
    ///   - isSkip: came by skipping login
    ///   - isShowRewardCard: scratch card will be shown on dashboard or not
    ///   - isFromBordingQ: is came from boarding questions or not (specifically used and android only)
    @objc func gotoTabbar(_ isSkip: Bool, isShowRewardCard: Bool, isFromBoardingQ: Bool){
        DispatchQueue.main.async {
            if isSkip {
                UserDefaultsKeys.custType.set(value: CustType.notVerified)
            }
            let vc = TabVC()
            vc.isShowScratchCard = isShowRewardCard
            ChatBridge.setRootWithVC(vc)
        }
    }
    
    class func setRootWithVC(_ vc: UIViewController) {
        rootNavVC?.dismiss(animated: true, completion: nil)
        rootNavVC?.popToRootViewController(animated: true)
        sharedAppDelegate?.window?.rootViewController = BaseNavigationController(rootViewController: vc)
        UIView.transition(with: sharedAppDelegate?.window ?? UIView(),
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
    
    
    /// open a native web view
    ///
    /// - Parameters:
    ///   - param: is describes a specific enum value for which native web view will be open
    ///   - title: title of the webview in case param is null or empty
    ///   - url: url for the webview in case param is null or empty
    @objc func openWebView(_ param: String, title: String, url: String){
        DispatchQueue.main.async {
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                
                switch param {
                case OpenWebViewFor.PRIVACYPOLICY.rawValue:
                    let vc = WebViewVC()
                    vc.urlString = Constants.Urls.SignUp.privacyPolicy
                    vc.screenTitle = Strings.MoreTabScene.privacyPolicy
                    navVC.pushViewController(vc, animated: true)
                //                    topMostView.navigationController?.pushViewController(vc, animated: true)
                case OpenWebViewFor.RATEONAPPSTORE.rawValue:
                    EventTracking.shared.eventTracking(name: .rateApp,[.location: "Account Tab Screen"])
                    let externalLink = Strings.Global.appStoreLink
                    if let url = URL(string : externalLink) {
                        Utilities.openCustomURL(url: url)
                    }
                default:
                    let vc = WebViewVC()
                    vc.urlString = url
                    vc.screenTitle = title
                    tabVC.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func goToSettings(){
        DispatchQueue.main.async {
            let settingsVC = SettingsVC()
            settingsVC.prepareInitialProps()
            rootTabVC?.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    @objc func switchValueChanged(_ isSwitchEnabled: Bool) {
        DispatchQueue.main.async {
            print(isSwitchEnabled)
            if (sharedAppDelegate?.pushPermissionStatus) != nil {
                if isSwitchEnabled {
                    Utilities.showAlertForSetting(on: Utilities.topMostPresenterViewController, title: MhcStrings.AlertMessage.enablePushHeader, message: MhcStrings.AlertMessage.enablePushParagraph, secondaryButtonTitle: "Not Now", secondAction: {
                        EventTracking.shared.eventTracking(name: .pushSettintRationale, [.location: "Notification Inbox", .action: "Deny"])
                        sharedAppDelegate?.getPushNotificationPermission{}
                    }) {
                        //primary button tapped
                        EventTracking.shared.eventTracking(name: .pushSettintRationale, [.location: "Notification Inbox", .action: Event.EventParams.settings])
                    }
                }else {
                    Utilities.showAlertForSetting(on: Utilities.topMostPresenterViewController, title: MhcStrings.AlertMessage.disablePushHeader, message: MhcStrings.AlertMessage.disablePushParagraph, secondaryButtonTitle: "Not Now", secondAction: {
                        EventTracking.shared.eventTracking(name: .pushSettintRationale, [.location: "Notification Inbox", .action: "Deny"])
                        sharedAppDelegate?.getPushNotificationPermission{}
                    }) {
                        //primary button tapped
                        EventTracking.shared.eventTracking(name: .pushSettintRationale, [.location: "Notification Inbox", .action: Event.EventParams.settings])
                    }
                }
            }else {
                sharedAppDelegate?.enablePushNotification(UIApplication.shared)
            }
        }
    }
    
    @objc func goToMyProfile() {
        DispatchQueue.main.async {
            let vc = MyProfileVC()
            var stringifiedQuestionsData = "{}"
            if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count>0 {
                let dictObject = Serialization.getDictionaryFromObject(object: responseDTO)
                if JSONSerialization.isValidJSONObject(dictObject) {
                    if let data = try? JSONSerialization.data(withJSONObject: dictObject, options: []), let jsonString = String(data: data, encoding: .utf8) {
                        stringifiedQuestionsData = jsonString
                    }
                }
            }
            vc.addToInitialProperties(["onBoardingQA" : stringifiedQuestionsData])
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                tabVC.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func goToBookMarkOffers() {
        DispatchQueue.main.async {
            rootTabVC?.navigationController?.pushViewController(BookmarkedOffersVC(), animated: true)
        }
    }
    
    @objc func goToActivateVoucher(_ fromScreen: String) {
        DispatchQueue.main.async {
            EventTracking.shared.eventTracking(name: .activateVouchers, [.location: fromScreen], withGI: true)
            rootTabVC?.navigationController?.pushViewController(ActivateVoucherVC(), animated: true)
        }
    }
    
    @objc func handleLogout() {
        DispatchQueue.main.async {
            Utilities.forceUserLogout()
        }
    }
    
    @objc func showPopupForVerifyNumber(_ title: String, subTitle: String,source: String, callback:@escaping RCTResponseSenderBlock) -> Void {
        DispatchQueue.main.async {
            let topVC = Utilities.topMostPresenterViewController
            topVC.showPopupForVerifyNumber(title: title, subTitle: subTitle.count>0 ? subTitle: nil, forScreen: source.isEmpty ? "" : source ) { (status) in
                callback([status])
            }
        }
    }
    
    @objc func startQuestionScreen(_ questions: NSArray,totalQuestions: NSNumber,currentQuestionNumber: NSNumber,flowType: String) {
        DispatchQueue.main.async {
            let userRegistrationVC = UserRegisterationVC()
            let questDict = ["data":questions]
            if JSONSerialization.isValidJSONObject(questDict) {
                if let data = try? JSONSerialization.data(withJSONObject: questDict, options: []), let jsonString = String(data: data, encoding: .utf8) {
                    userRegistrationVC.addToInitialProperties( ["flowType": flowType,"totalScreens": totalQuestions,"currentScreenNo":currentQuestionNumber,"questionAnswer":jsonString,"currentBrand": "Apple","currentModel": (userRegistrationVC.initialProperties["modelName"] as? String) ?? ""])
                    let topMostView = Utilities.topMostPresenterViewController
                    if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                        tabVC.present(userRegistrationVC, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @objc func editQuestionsScreen(_ questions:NSArray) {
        DispatchQueue.main.async {
            let userRegistrationVC = UserRegisterationVC()
            let questDict = ["data":questions]
            if JSONSerialization.isValidJSONObject(questDict) {
                if let data = try? JSONSerialization.data(withJSONObject: questDict, options: []), let jsonString = String(data: data, encoding: .utf8) {
                    userRegistrationVC.addToInitialProperties( ["flowType": "Edit","questionAnswer":jsonString,"currentBrand": "Apple","currentModel": (userRegistrationVC.initialProperties["modelName"] as? String) ?? ""])
                    Utilities.topMostPresenterViewController.present(userRegistrationVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func updateProfile() {
        ChatBridge.updateMyAccount()
    }
    
    @objc func updateRewardState(_ state: String) {
        UserDefaults.standard.set(state, forKey: "rewardState")
    }
    
    @objc func callForGetCustumerResposne(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) -> Void {
        DispatchQueue.main.async {
            var membershipUseCase: MembershipListUseCase? = MembershipListUseCase()
            membershipUseCase?.getMembershipForCustomerDetail {(response, error) in
                membershipUseCase = nil
                if error == nil {
                    resolve("done")
                } else {
                    reject("code", (error as NSError?)?.code == -1009 ? "10000" : "10001", nil )
                }
            }
        }
    }
    
    @objc func goToATMsBankScreen(_ destination: String){
        let goToATMsBank:GoToATMsBank = GoToATMsBank(rawValue: destination as String) ?? GoToATMsBank.SHOW_ATM
        DispatchQueue.main.async {
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                var vc:UIViewController?
                switch goToATMsBank {
                case .SHOW_ATM:
                    EventTracking.shared.eventTracking(name: .findATM)
                    vc = AtmBranchesMapVC()
                case .SHOW_BANK:
                    EventTracking.shared.addScreenTracking(with: .nearbyBanks)
                    vc = BankBranchesListVC()
                }
                tabVC.navigationController?.pushViewController(vc!, animated: true)
            }
        }
    }
    
    @objc func startBuyBack(_ buybackData: NSDictionary,deeplinkURL: String){
        let dictionary = buybackData as! Dictionary<String,Any>
        DispatchQueue.main.async {
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                if JSONSerialization.isValidJSONObject(dictionary) {
                    if let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: []), let buyBackStatusData: BuyBackStatusData = try? JSONParserSwift.parse(data: jsonData) {
                        let data = buyBackStatusData.buybackStatus ?? []
                        if let currrentData = data.first(where: {$0.deviceInfo?.deviceIdentifier == UIDevice.uuid}), data.count == 1 {
                            let isSupported = currrentData.buyBackStatus != BUYBACK_STATUS.MODEL_NOT_FOUND.rawValue
                            let buyBackStatus = currrentData.buyBackStatus ?? ""
                            EventTracking.shared.eventTracking(name: .mobileBuybackIntent, [.location: "Explore", .brandAndModel: "Apple \(UIDevice.currentDeviceModel)", .supported: isSupported ? "YES" : "NO", .Stage: buyBackStatus])
                        }
                        if let buyback_status_list = try? JSONParserSwift.getJSON(object: data) {
                            let buybackVC = BuybackVC()
                            let customerMembershipResponse = tabVC.customerMembershipResponse?.data
                            var mems: [Any] = customerMembershipResponse?.memberships ?? []
                            mems.append(contentsOf: customerMembershipResponse?.pendingMemberships ?? [])
                            let membershipList = (try? JSONParserSwift.getJSON(object: mems)) ?? ""
                            buybackVC.addToInitialProperties(["buyback_status_list": buyback_status_list, "membershipList": membershipList])
                            if (!deeplinkURL.isEmpty){buybackVC.addToInitialProperties(["deeplink_url_string": deeplinkURL])}
                            tabVC.present(buybackVC, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    @objc func startEditDetails() {
        DispatchQueue.main.async {
            let vc = EditDetailsVC()
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                tabVC.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func startMyRewardScreen(_ isAnsweredAllQuestions:Bool, showScratchCard:Bool){
        DispatchQueue.main.async {
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                let vc = MyRewardsVC()
                vc.membershipResponse = tabVC.customerMembershipResponse
                vc.prepareInitialProps(isAnsweredAllQuestions: isAnsweredAllQuestions,showScratchCard: showScratchCard)
                tabVC.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func openScratchCard(_ state: String) {
        DispatchQueue.main.async {
            Utilities.openScratchCard(state: RewardStates(rawValue: state as String)!, nil)
        }
    }
    
    @objc func routeToOAIDFenceVC(_ membershipDataText: String,renewalDataStrigified: String) {
        DispatchQueue.main.async {
            let idfenceVC = OAIDFenceVC()
            idfenceVC.addToInitialProperties(["membershipData": membershipDataText, "renewalData": renewalDataStrigified,"deeplinkData":"{}"])
            
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                tabVC.present(idfenceVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func setOnBoardingData(_ data: String) {
        DispatchQueue.main.async {
            let alreadyAddedCards = ChatBridge.getObBoardingCards().map{$0.optionCode ?? ""}
            if let jsonData = data.data(using: .utf8), let json = (try? JSONSerialization.jsonObject(with: jsonData, options: [])) as? [String: Any], let resultData = try? JSONSerialization.data(withJSONObject: json)  {
                DataDiskStogare.saveDatawithFile(data: resultData, fileName: questionFilename)
            }
            let newAddedCards = ChatBridge.getObBoardingCards().map{$0.optionCode ?? ""}
            if alreadyAddedCards.joined(separator: ",") !=  newAddedCards.joined(separator: ",") {
                var stringifiedQuestionsData:String?
                if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count>0 {
                    let dictObject = Serialization.getDictionaryFromObject(object: responseDTO)
                    if JSONSerialization.isValidJSONObject(dictObject) {
                        if let data = try? JSONSerialization.data(withJSONObject: dictObject, options: []), let jsonString = String(data: data, encoding: .utf8) {
                            stringifiedQuestionsData = jsonString as String
                        }
                    }
                }
                ChatBridge.sendNotification(withName: "refreshCardAndOffers", body: ["onBoardingQA": stringifiedQuestionsData ?? "{}"])
            }
        }
    }
    
    
    @objc func handleSODFlow(_ deeplinkURL: String, eventParams: NSDictionary){
        DispatchQueue.main.async {
            if var url = URL(string: deeplinkURL), deeplinkURL.contains("category=QR") {
                url = url.appending("eventLocation", value: (eventParams["location"] as? String) ?? "Home")
                rootTabVC?.goToSpotLightActions(url: url.absoluteString)
            }
        }
    }
    
    @objc func openOCRflow(_ callback: @escaping RCTResponseSenderBlock) {
        DispatchQueue.main.async {
            if let tabVC = rootTabVC {
                let imeiVCCallback: ((Bool) -> ()) = { status in
                    if status {
                        let vc = IMEIVerificationVC()
                        vc.isFromBuyback = true
                        vc.imeiCallback = { imei in callback([imei]) }
                        tabVC.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        callback([""])
                    }
                }
                if (Permission.checkGalleryPermission() == .authorized) {
                    imeiVCCallback(true)
                } else {
                    let permissionVC = PermissionScreenVC()
                    permissionVC.isFromBuyback = true
                    permissionVC.callback = imeiVCCallback
                    tabVC.navigationController?.pushViewController(permissionVC, animated: true)
                }
            }
        }
    }
    
    @objc func getUserLocation(_ showSettingsDialoge: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            if let reactVC = rootTopVC {
                Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.home) {[weak self] (status) in
                    if reactVC.isLocationSet {
                        return
                    }
                    if status == .authorizedAlways || status == .authorizedWhenInUse {
                        Permission.shared.getLocationPinCode { [weak reactVC] (location, pincode) in
                            guard let _ = reactVC else {return}
                            ChatBridge.sendNotification(withName: "willReceiveLocation", body: ["latitude": location?.coordinate.latitude, "longitude": location?.coordinate.longitude,"pincode": pincode])
                            reactVC?.isLocationSet = true
                        }
                    }else{
                        showSettingsDialoge([])
                    }
                }
            }
        }
    }
    
    //Mark:- class method
    class func onRewardStateChange(state: String, scratchDate: String){
        ChatBridge.sendNotification(withName: "rewardStateChange", body: ["rewardState":state,"rewardScratchDate":scratchDate])
    }
    
    class func updateMyAccount() {
        DispatchQueue.main.async {
            let isIDFenceInMem = rootTabVC?.customerMembershipResponse?.checkCustomerHaveAnyIDFencePlan() ?? false
            let body: [String: Any] = [
                "mobile_number": userMobileNumber,
                "customer_id": UserCoreDataStore.currentUser?.cusId ?? "0",
                "user_Name":UserCoreDataStore.currentUser?.userName ?? "",
                "user_Email": CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "",
                "customer_Address": [
                    "addressLine1": CustomerDetailsCoreDataStore.currentCustomerDetails?.addressLine1 ?? "",
                    "cityName":CustomerDetailsCoreDataStore.currentCustomerDetails?.cityName ?? "",
                    "stateName":CustomerDetailsCoreDataStore.currentCustomerDetails?.stateName ?? ""
                ],
                "token": AppUserDefaults.sessionToken,
                "isIDFenceInMem": isIDFenceInMem,
                "isNonSubscriber": UserDefaults.standard.bool(forKey: UserDefaultsKeys.isNonSubscriber.rawValue)
            ]
            ChatBridge.sendNotification(withName: "ProfileUpdated", body: body)
        }
    }
    
    @objc func sendCustomerDetailsUpdatedEvent() {
        ChatBridge.sendNotification(withName: "CustomerDetailsUpdated", body: [:])
    }
    
    @objc func setVerifyEmailDismissed(_ dismissed: Bool) {
        AppUserDefaults.isVerifyEmailDismissed = dismissed
    }
    
    @objc func updateCustomerDetails(_ data: String) {
        DispatchQueue.main.async {
            if let jsonData = data.data(using: .utf8), let customerModel: GetCustDetailsResponseDTO = try? JSONParserSwift.parse(data: jsonData), let customerData = customerModel.data?.customers?.first {
                
                let custAddress = customerData.addresses?.first(where: { $0.addressType == "PER" }) ?? customerData.addresses?.first
                
                let user = UserCoreDataStore.currentUser ?? UserCoreDataStore(context: CoreDataStack.sharedStack.mainContext)
                let customer = CustomerDetailsCoreDataStore.currentCustomerDetails ?? CustomerDetailsCoreDataStore(context: CoreDataStack.sharedStack.mainContext)
                
                user.cusId = customerData.custId?.stringValue
                user.mobileNo = customerData.mobileNumber
                user.userName = customerData.firstName
                
                customer.firstName = customerData.firstName
                customer.lastName = customerData.lastName
                customer.gender = customerData.gender
                customer.addressLine1 = custAddress?.addressLine1
                customer.addressLine2 = custAddress?.addressLine2
                customer.cityName = custAddress?.cityName
                customer.stateName = custAddress?.stateName
                customer.pincode = custAddress?.pincode
                customer.cityCode = custAddress?.cityCode
                customer.stateCode = custAddress?.stateCode
                customer.mobileNumber = customerData.mobileNumber
                customer.email = customerData.emailId
                
                CoreDataStack.sharedStack.saveMainContext()
            }
        }
    }
    
    class func refreshHomeScreen() {
        if let tabVC = rootTabVC {
            let (isRenew,isLead,haServices,peProducts,fProducts) = tabVC.getLeadAndInReviewStatus()
            let HAServices = Array(haServices)
            let PEProducts = peProducts
            let FProducts = Array(fProducts)
            let isIDFenceInMem = tabVC.customerMembershipResponse?.checkCustomerHaveAnyIDFencePlan() ?? false
            let questionStatus =  tabVC.checkQuestionStatus()
            var stringifiedQuestionsData:String?
            if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count>0 {
                let dictObject = Serialization.getDictionaryFromObject(object: responseDTO)
                if JSONSerialization.isValidJSONObject(dictObject) {
                    if let data = try? JSONSerialization.data(withJSONObject: dictObject, options: []), let jsonString = String(data: data, encoding: .utf8) {
                        stringifiedQuestionsData = jsonString as String
                    }
                }
            }
            
            ChatBridge.sendNotification(withName: "UpdateHomeScreen", body:
                                            ["mobile_number": userMobileNumber,
                                             "customer_id": UserCoreDataStore.currentUser?.cusId ?? "0",
                                             "user_Name":UserCoreDataStore.currentUser?.userName ?? "",
                                             "user_Email": CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "",
                                             "token": AppUserDefaults.sessionToken,
                                             "isLead": isLead,
                                             "isRenew": isRenew,
                                             "availableHAServices": HAServices,
                                             "availablePEProducts":PEProducts,
                                             "availableFProducts":FProducts,
                                             "isIDFenceInMem":isIDFenceInMem,
                                             "rewardScratchDate":UserDefaults.standard.string(forKey: "rewardScratchDate"),
                                             "isNonSubscriber": UserDefaults.standard.bool(forKey: UserDefaultsKeys.isNonSubscriber.rawValue),
                                             "isAnsweredAllQuestions": !questionStatus,
                                             "onBoardingQA": stringifiedQuestionsData ?? "{}"])
        }
    }
    
    class func refreshOffersAndCards() {
        ChatBridge.sendNotification(withName: "refreshCardAndOffers", body: [:])
    }
    
    //TODO:- remove below code
    @objc func refreshBuybackStatus() {
        ChatBridge.sendNotification(withName: "refreshBuyBackStatus", body: ["customer_id": UserCoreDataStore.currentUser?.cusId ?? "0","token": AppUserDefaults.sessionToken])
    }
    
    /// this below method fires an event in case user is performing specific action among SelectedActionType
    ///
    /// - Parameter type: an enum value of SelectedActionType
    class func selectedActionType(type: SelectedActionType) {
        let tabBarSelectedIndex = rootTabVC?.selectedIndex.rawValue ?? 0
        switch type {
        case .TABSWITHCHED:
            ChatBridge.sendNotification(withName: "selectedActionType", body: ["type":type.rawValue, "tabIndex": tabBarSelectedIndex])
        default:
            ChatBridge.sendNotification(withName: "selectedActionType", body: ["type":type.rawValue])
        }
    }
    
    class func getObBoardingCards()-> [OptionList] {
        var cards: [OptionList] = []
        if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count>0 {
            if let selectedOptionList = data.filter({$0.questionCode == "ASSET_F"}).first?.optionList?.filter({$0.selected == 1}), selectedOptionList.count > 0 {
                cards = selectedOptionList
            }
        }
        return cards
    }
    
}



