//
//  ChatBridge.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/09/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import UIKit
import React
import FirebaseAnalytics
class ReactNotificationData {
    var name: String
    var body: Any
    
    init(withName name: String, body: Any) {
        self.name = name
        self.body = body
    }
}

extension NSNotification.Name {
    static let ReactNotification = NSNotification.Name(rawValue: "ReactNotification")
}


enum OpenWebViewFor:String {
    case PRIVACYPOLICY
    case RATEONAPPSTORE
    case ACTIVATEVOUCHER
}

enum StartMHC:String {
    case START_COMPLETE_MHC
    case START_SECTIONAL_MHC
}

enum HomeScreenSubTabs: Int8 {
    case TODAY_VIEW = 0
    case EXPLORE_VIEW = 1
}

@objc(ChatBridge)
class ChatBridge: RCTEventEmitter {
    
    override func supportedEvents() -> [String]! {
        return [
            "ReceiveEvent",
            "StatusEvent",
            "ProfileUpdated",
            "IDFenceRenewalEvent",
            "rewardStateChange",
            "DeeplinksData",
            "refreshCardAndOffers",
            "refreshBuyBackStatus",
            "UpdateHomeScreen",
            "selectedActionType",
            "pushStatusUpdated",
            "CustomerDetailsUpdated",
            "willReceiveLocation",
            "BuyIDFenceFromEventRiskCalculator",
            "ChangeBuyPlanCategory",
            "FavoriteRecommendationUpdated",
            "Logout",
            "HomeViewAppeared",
            "SwitchHomeScreenSubTab",
            "RefreshMemDraftSRStatus",
            "RefreshMemBuybackStatus",
            "getMembershipDataFromHome",
            "RefreshOnGoingSRScreen",
            "RefreshMembershipsOnTab",
            "RefreshMemberships",
            "ChangeTabSelected",
            "UpdateNotificationCount",
            "DownloadingStripVisibility",
            "OnDownloadRemainingTimeChange",
            "OnDownloadError",
        ]
    }
    
    func requiresMainQueueSetup() -> Bool {
        return true
    }
    
    override func startObserving() {
        NotificationCenter.default.addObserver(forName: .ReactNotification, object: nil, queue: .main) { [weak self](notification) in
            if let reactNotificationData = notification.object as? ReactNotificationData {
                self?.sendEvent(withName: reactNotificationData.name, body: reactNotificationData.body)
            }
        }
    }
    
    override func stopObserving() {
        NotificationCenter.default.removeObserver(self, name: .ReactNotification, object: nil)
    }
    
    @objc class func sendNotification(withName name: String, body: Any) {
        NotificationCenter.default.post(name: .ReactNotification, object: ReactNotificationData(withName: name, body: body))
    }
    
    override static func requiresMainQueueSetup() -> Bool {
        return true
    }
    
    @objc func showLocalNotification(_ notificationMessage: String) {
        DispatchQueue.main.async {
            let content = UNMutableNotificationContent()
            content.title = NSString.localizedUserNotificationString(forKey: "Hi, \(chatUserName.trimmingCharacters(in: .whitespacesAndNewlines))", arguments: nil)
            content.body = NSString.localizedUserNotificationString(forKey: notificationMessage, arguments: nil)
            content.sound = .default
            content.categoryIdentifier = "com.oneAssist.oneAssist"
            UNUserNotificationCenter.current().add(UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)))
        }
    }
    
    @objc func uploadDocument(_ imageUri:String, callback: @escaping RCTResponseSenderBlock) {
        XMPPManager.shared.uploadDocument(imageUri) { (url, status) in
            let jsonObject = ["url":url, "status": status] as [String : Any]
            let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
            callback([jsonString])
        }
    }
    
    @objc class func receivedMessage(_ message:String, from: String, chatId: String, msgId: String) {
        print("receivedMessage \(from) \(message)")
        let jsonObject = ["message": message, "from": from, "chatId": chatId, "msgId": msgId]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        sendNotification(withName: "ReceiveEvent", body: jsonString)
    }
    
    @objc class func receivedStatus(_ status:String, from: String) {
        print("receivedStatus \(from) \(status)")
        let jsonObject = ["status": status, "from": from]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        sendNotification(withName: "StatusEvent", body: jsonString)
    }
    
    @objc class func receivedRenewalAPIResponse(_ message: String) {
        sendNotification(withName: "IDFenceRenewalEvent", body: message)
    }
    
    @objc class func updateRecommendationStatus(_ rId: String, uiComponentId: String, isFavorite: Bool) {
        print("updateRecommendationStatus \(rId) \(uiComponentId) \(isFavorite)")
        sendNotification(withName: "FavoriteRecommendationUpdated", body: ["rId": rId, "uiComponentId": uiComponentId, "isFavorite": isFavorite])
    }
    
    @objc class func logout() {
        sendNotification(withName: "Logout", body: 0)
    }
    
    @objc class func homeScreenAppeared() {
        sendNotification(withName: "HomeViewAppeared", body: 0)
    }
    
    @objc func goBack() {
        dismissReactMainVC()
    }
    
    @objc func goBackWithAmnimatedParam(_ animated: Bool) {
        dismissReactMainVC(animated)
    }
    
    @objc func dismissBottomSheet() {
        DispatchQueue.main.async {
            let topMostView = Utilities.topMostPresenterViewController
            if topMostView.isPresented {
                topMostView.dismiss(animated: true, completion: {
                    
                })
            }else {
                topMostView.removeFromParent()
            }
        }
    }
    
    func dismissReactMainVC(_ animated: Bool = true, _ completion:(() -> Void)? = nil) {
        DispatchQueue.main.async {

            let topMostView = Utilities.topMostPresenterViewController
            if let viewController = topMostView as? BuyPlanJourneyVC , let isPresent = viewController.initialProperties["isPresent"] as? NSNumber, isPresent.boolValue, viewController.isPresented{
                topMostView.dismiss(animated: true, completion: completion)
                return
            }

            if let reactNavVC = rootNavVC, let reactVC = reactNavVC.topViewController as? ReactMainVC {
                if reactVC is ChatVC {
                    XMPPManager.shared.disconnect()
                    ChatBridge.sendNotification(withName: "RefreshMemDraftSRStatus", body: [:])
                } else if reactVC is BuybackVC {
                    BuybackQuestionManager.shared.isFromBuyback = false
                    ChatBridge.sendNotification(withName: "RefreshMemBuybackStatus", body: [:])
                }
                if reactNavVC.viewControllers.count == 1 {
                    reactVC.dismiss(animated: animated, completion: completion)
                } else {
                    reactNavVC.popViewController(animated: animated)
                    completion?()
                }
            } else {
                if topMostView.isPresented {
                    topMostView.dismiss(animated: true, completion: completion)
                }else {
                    topMostView.removeFromParent()
                    completion?()
                }
            }
        }
    }
    
    @objc func disconnectSocket() {
        XMPPManager.shared.disconnect()
    }
    
    @objc func reconnectSocket() {
        XMPPManager.shared.reconnect()
    }
    
    @objc func connectSocketWithPassword(_ password: String, callback: @escaping RCTResponseSenderBlock) {
        XMPPManager.shared.createXMPPConnection(withPassword: password) { (connected) in
            callback([connected ? "true" : "false"])
        }
    }
    
    @objc func getUsername(_ callback: @escaping RCTResponseSenderBlock) {
        DispatchQueue.main.async {
            let username = chatUserName.trimmingCharacters(in: .whitespacesAndNewlines)
            callback([username])
        }
    }
    
    @objc func imagePickerPermissionDenied(_ type:String) {
        DispatchQueue.main.async {
            ImagePickerManager.shared.showPermissionAlert(source: type == "camera" ? .camera : .photoLibrary)
        }
    }
    
    @objc func getImageUrl(_ productCode:String, callback:RCTResponseSenderBlock) {
        callback([Utilities.getProduct(productCode: productCode)?.subCatImageUrl ?? ""])
    }
    
    @objc func getAppVersion(_ callback:RCTResponseSenderBlock) {
        callback([UIApplication.appVersion])
    }
    
    @objc func getApiProperty(_ callback:RCTResponseSenderBlock) {
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: defaultRNInitialProps)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getUserInfo(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["userName":UserCoreDataStore.currentUser?.userName ?? "",
                          "userEmail": CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "",
                          "userMobile": userMobileNumber,
                          "customerId":  UserCoreDataStore.currentUser?.cusId ?? "0"]
        
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getRewardFeatureOnStatus(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["reward_feature_on": RemoteConfigManager.shared.rewardFeatureOn]
        
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        print(jsonString)
        callback([jsonString])
    }
    
    @objc func getBuybackCouponCode(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["buyback_coupon_code": RemoteConfigManager.shared.buybackCouponCode ?? ""]
        
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getPlansCategoryData(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["plansCategoryData": RemoteConfigManager.shared.buyPlanRevamp]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func sodAppliancesMapping(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["sodAppliancesData": RemoteConfigManager.shared.sodAppliancesData]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getOneAssistPromiseData(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["oneassist_promise": RemoteConfigManager.shared.oaPromisesData]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    func navigateToChat(initialProps: [String: Any]?) {
        let topVC = Utilities.topMostPresenterViewController
        topVC.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.chatWithUs.message, subTitle:nil, forScreen: "Chat") {(status) in
            if status {
                let chatVC = ChatVC()
                if let props = initialProps {
                    chatVC.addToInitialProperties(props)
                }
                rootNavVC?.pushViewController(chatVC, animated: true)
            }
        }
    }
    
    @objc func openChat() {
        DispatchQueue.main.async {[weak self] in
            self?.navigateToChat(initialProps: nil)
        }
    }
    
    @objc func openChatWithInitialPrams(_ withIntialProps: NSDictionary) {
        DispatchQueue.main.async {[weak self] in
            if let initialPropsData = withIntialProps as? [String: Any] {
                self?.navigateToChat(initialProps: initialPropsData)
            }
        }
    }
    
    @objc func openFavoriteRecommendations() {
        DispatchQueue.main.async {
            if let topMostView = Utilities.topMostPresenterViewController as? BaseNavigationController, let tabVC = topMostView.topViewController as? TabVC {
                tabVC.present(FavoriteRecommendationsVC(), animated: true, completion: nil)
            }
        }
    }
    
    @objc func openRating(_ quoteId: String, servedBy: String, isAbb: Bool) {
        DispatchQueue.main.async {
            if let topMostView = Utilities.topMostPresenterViewController as? BaseNavigationController, let tabVC = topMostView.topViewController as? TabVC {
                let vc = RatingVC()
                vc.addToInitialProperties(["quoteId":quoteId, "servedBy": servedBy, "isAbb": isAbb])
                tabVC.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func updateFavoriteRecommendation(_ rId: String, uiComponentId: String, isFavorite: Bool) {
        ChatBridge.updateRecommendationStatus(rId, uiComponentId: uiComponentId, isFavorite: isFavorite)
    }
    
    @objc func getRecommendationProps(_ callback:RCTResponseSenderBlock) {
        let jsonObject = [
            "show_catalyst_recommendations": RemoteConfigManager.shared.showCatalystRecommendations,
            "show_today_tab": RemoteConfigManager.shared.showTodayTab,
            "recommendation_force_update_interval": RemoteConfigManager.shared.recommendationForceUpdateInterval,
            "catalyst_user_group": AppUserDefaults.isCatalystUserGroup
        ] as [String : Any]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func openMHC(_ questionArray: String, resolver resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
        DispatchQueue.main.async {
            if let jsonData = questionArray.data(using: .utf8), let questionsData: MHCQuestionData = try? JSONParserSwift.parse(data: jsonData) {
                print(questionsData)
                BuybackQuestionManager.shared.updateWith(mhcQuestionsData: questionsData)
                if BuybackQuestionManager.shared.remainingTest().isEmpty {
                    if let resolveString = BuybackQuestionManager.shared.dataString() {
                        print(resolveString)
                        resolve(resolveString)
                    } else {
                        reject("code", "10001", nil )
                    }
                } else {
                    // this event is already calling in React native
                    // EventTracking.shared.eventTracking(name: .mhcCheckNow,[.location: Event.EventParams.buyback])
                    let mhcVC = MHCCategoryVC()
                    BuybackQuestionManager.shared.isFromBuyback = true
                    mhcVC.isFromBuyback = true
                    mhcVC.buybackCompletionBlock = { data in
                        if let resolveString = data {
                            resolve(resolveString)
                        } else {
                            reject("code", "10001", nil )
                        }
                    }
                    Utilities.topMostPresenterViewController.presentInFullScreen(UINavigationController(rootViewController: mhcVC), animated: true, completion: nil)
                }
            } else {
                reject("code", "10001", nil )
            }
        }
    }
    
    @objc func checkMHC(_ questionArray: String, resolver resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) {
        DispatchQueue.main.async {
            if let jsonData = questionArray.data(using: .utf8), let questionsData: MHCQuestionData = try? JSONParserSwift.parse(data: jsonData), let _ = questionsData.data {
                BuybackQuestionManager.shared.updateWith(mhcQuestionsData: questionsData)
                if let resolveString = BuybackQuestionManager.shared.dataString() {
                    print(resolveString)
                    resolve(resolveString)
                } else {
                    reject("code", "10001", nil )
                }
            } else {
                reject("code", "10001", nil )
            }
        }
    }
    
    @objc func getBuybackTAndCUrl(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["buyback_tnc": RemoteConfigManager.shared.buybackTNCPath ?? ""]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getIDFenceDashboardDetails(_ callback:RCTResponseSenderBlock) {
        var jsonObject = ["id_fence_dashboard_details": RemoteConfigManager.shared.idFenceDashboardData, "blogsData": "{}"]
        if let blogsJsonStringified = RemoteConfigManager.shared.idFenceDetailPageData.data(using: .utf8), let idfPlanDetail: PlanDetailPageData = try? JSONParserSwift.parse(data: blogsJsonStringified) {
            jsonObject["blogsData"] = try? JSONParserSwift.getJSON(object: idfPlanDetail.blogs?.data ?? "{}")
        }
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    @objc func getPrePostOnboardingFormFields(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["pre_post_onboarding_form_fields": RemoteConfigManager.shared.prePostOnboardingFormFieldsPath ?? ""]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    class func routeToMembershipTab() {
        rootNavVC?.popToRootViewController(animated: false)
        rootTabVC?.refreshAndRouteToMembershipTab()
    }
    
    private func routeToTabIndex(navVC: BaseNavigationController, index: Int){
        rootNavVC?.popToRootViewController(animated: false)
        rootTabVC?.changeTabIndex(itemTag: TabIndex(rawValue: index) ?? .newHome)
    }
    
    
    @objc func openMemTab() {
        dismissReactMainVC(true, {
            if let navVC = rootNavVC {
                if let presentedViewController = navVC.topViewController?.presentedViewController {
                    presentedViewController.dismiss(animated: false, completion: {
                        ChatBridge.routeToMembershipTab()
                    })
                } else {
                    ChatBridge.routeToMembershipTab()
                }
            }
        })
    }
    
    @objc func openTabBarWithIndex(_ index: NSNumber) {
        dismissReactMainVC(true, { [weak self] in
            
            if index.intValue == TabIndex.newHome.rawValue {
                ChatBridge.sendNotification(withName: "SwitchHomeScreenSubTab", body: ["tabIndex" : HomeScreenSubTabs.EXPLORE_VIEW.rawValue])
            }
            
            if let navVC = rootNavVC {
                if let presentedViewController = navVC.topViewController?.presentedViewController {
                    presentedViewController.dismiss(animated: false, completion: {
                        self?.routeToTabIndex(navVC: navVC,index: index.intValue)
                    })
                } else {
                    self?.routeToTabIndex(navVC: navVC,index: index.intValue)
                }
            }
        })
    }
    
    @objc func startFDFlow(_ productCode: String, tempCustomerInfo: NSDictionary) {
        let customerInfo: [String: Any]? = (tempCustomerInfo.value(forKey: "customerDetails") as? [[String: Any]])?.first
        let activityRefId = customerInfo?["activityRefId"] as? String
        let custId = ((customerInfo?["custId"] as? Int) ?? 0).description
        let brand = customerInfo?["deviceMake"] as? String
        let testType = ((tempCustomerInfo.value(forKey: "task") as? [[String: Any]])?.first?["fdTestType"] as? String) ?? FDTestType.SDT.getAPIValue()
        
        UserDefaultsKeys.Location.set(value: "")  // remove rating popup fro FD flow
        
        dismissReactMainVC(false, {
            DispatchQueue.main.async {
                ChatBridge.routeToMembershipTab()
                if let tabVC = rootTabVC {
                    FDCacheManager.clearCache()
                    tabVC.startFDFlow(for: productCode, with: nil, with: activityRefId, brand: brand, testType: testType, fromScreen: .PEPaymentSuccessScreen)
                }
            }
        })
    }
    
    @objc func openScheduleInspectionFlow(_ activationCode: String, pincode:String){
        dismissReactMainVC(true, { [weak self ] in
            self?.routeToInspection(activationCode: activationCode, pincode: pincode)
        })
    }
    
    class func openIDFence(_ isSilentMoveToPayment:Bool = false, isFreeReward: Bool =  false){
        DispatchQueue.main.async {
            guard let vc = (Utilities.topMostPresenterViewController as? BaseNavigationController) else {
                return
            }
            guard let tabBarVC = vc.viewControllers.first as? TabVC else {
                return
            }
            tabBarVC.routeToIdFencePlans()
        }
    }
    
    @objc func openIDFencePlanDetails(_ updatePaymentOrRenew:Bool, url: String) {
        DispatchQueue.main.async {
            if let reactVC = rootTopVC as? BuyPlanJourneyVC {
                if let tabVC = rootTabVC {
                    if updatePaymentOrRenew {
                        let paymentVC = OAPaymentVC()
                        paymentVC.addToInitialProperties(["data": url, "service_Type": "IDFence", "hasEW": false, "isRenewal": true])
                        reactVC.present(paymentVC, animated: true, completion: nil)
                    } else {
                        tabVC.routeToIdFencePlans()
                    }
                }
            }else{
                self.dismissReactMainVC(false) {
                    if let tabVC = rootNavVC?.viewControllers.last as? TabVC {
                        if updatePaymentOrRenew {
                            let paymentVC = OAPaymentVC()
                            paymentVC.addToInitialProperties(["data": url, "service_Type": "IDFence", "hasEW": false, "isRenewal": true])
                            tabVC.present(paymentVC, animated: true, completion: nil)
                        } else {
                            tabVC.routeToIdFencePlans()
                        }
                    }
                }
            }
        }
    }
    
    @objc func shareLink(_ textToShare: String) {
        DispatchQueue.main.async {
            if let reactVC = rootTopVC {
                if reactVC is RiskCalculatorVC {
                    let objectsToShare = [textToShare] as [Any]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    activityVC.popoverPresentationController?.sourceView = reactVC.view
                    reactVC.present(activityVC, animated: true, completion: nil)
                } else {
                    if textToShare.isEmpty {
                        let textToShare = Strings.Global.shareText + Strings.Global.appDownloadUrlString
                        Utilities.showShareActionSheet(viewController: Utilities.topMostPresenterViewController, dataToShare: [textToShare])
                    }else{
                        let objectsToShare = [textToShare] as [Any]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        activityVC.popoverPresentationController?.sourceView = reactVC.view
                        reactVC.present(activityVC, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @objc func getNameForSubCategory(_ productCode:String, callback:RCTResponseSenderBlock) {
        callback([Utilities.getProductCategory(for: productCode)?.subCategoryName ?? ""])
    }
    
    @objc func getProductTerms(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["product_terms": RemoteConfigManager.shared.productTNCPath ?? ""]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getAutoSuggestionsData(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["auto_suggestion_data": RemoteConfigManager.shared.autoSuggestionData ?? ""]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func paymentSuccessful() {
        UserDefaultsKeys.Location.set(value: Event.EventParams.purchase)
    }
    
    @objc func routeToExplorePlans(_ url: String){
        DispatchQueue.main.async {
            if let url = URL(string: url){
                DeepLinkManager.handleLink(url)
            }
        }
    }
    
    @objc func processDeeplink(_ url: String, eventData: NSDictionary) {
        DispatchQueue.main.async {
            if let url = URL(string: url) {
                sharedAppDelegate?.recoCatalystProduct = eventData.object(forKey: "Product") as? String
                sharedAppDelegate?.recoCatalystService = eventData.object(forKey: "Service") as? String
                sharedAppDelegate?.recoCatalystCommSubcat = eventData.object(forKey: "CommSubcat") as? String
                sharedAppDelegate?.recoCatalystRID = eventData.object(forKey: "ID") as? String
                sharedAppDelegate?.recoCatalystUIComponentID = eventData.object(forKey: "ComponentId") as? String
                sharedAppDelegate?.recoCatalystRCID = eventData.object(forKey: "CommId") as? String
                DeepLinkManager.handleLink(url)
            }
        }
    }
    
    @objc func downloadDocumentFromSP(_ fileTypeRequired:String, storageRefId: String ,callback: @escaping RCTResponseSenderBlock){
        let urlStr = "\(Constants.SchemeVariables.servicePlatform)servicerequests/documents?fileTypeRequired=\(fileTypeRequired)&storageRefIds=\(storageRefId )"
        guard let url = URL(string: urlStr) else { callback([""]); return }
        FileDownloader.loadDocumentsAsync(url: url, name: storageRefId) { (path, error) in
            print("PDF File downloaded to : \(path ?? "")")
            callback([path ?? ""])
        }
    }
    
    @objc func downloadTemplate(_ draftId: String ,callback: @escaping RCTResponseSenderBlock){
        
        DispatchQueue.main.async {
            let vc = Utilities.topMostPresenterViewController
            var loaderCount = 0
            Utilities.addLoader(onView: vc.view, count: &loaderCount, isInteractionEnabled: false)
        }
        
        let urlStr = "\(Constants.SchemeVariables.servicePlatform)intimation/\(draftId)/document/download?documentKey=SUBROGATION_LETTER"
        
        guard let url = URL(string: urlStr) else { callback([""]); return }
        FileDownloader.loadDocumentsAsync(url: url, name: draftId) { [weak self] (path, error) in
            DispatchQueue.main.async {
                let vc = Utilities.topMostPresenterViewController
                var loaderCount = 0
                Utilities.removeLoader(fromView: vc.view, count: &loaderCount)
            }
            
            print("PDF File downloaded to : \(path ?? "")")
            callback([path ?? ""])
            if error == nil {
                self?.showPrintOption(urlstring: path ?? "")
            }
        }
    }
    
    @objc func getFMIPData(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["fmip_data": RemoteConfigManager.shared.fmipData ]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func getMILogoutData(_ callback:RCTResponseSenderBlock) {
        let jsonObject = ["milogout_data": RemoteConfigManager.shared.milogoutData]
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        callback([jsonString])
    }
    
    @objc func downloadDocument(_ urlStr: String , fileName: String, message: String) {
        print("File download from path : \(urlStr)")
        if let url = URL(string: urlStr) {
            let headerTitle = message == Strings.Global.invoiceDownloaded ? Strings.Global.invoice :  Strings.Global.report
            FileDownloader.fileOpeninWebView(url: url, fileName: fileName, headerTitle: headerTitle, message: message)
        }
    }
    
    // MARK: CARD MANAGEMENT
    @objc func saveReminderForWalletId(_ walletId: NSNumber, date: NSNumber, title:String) {
        EventManager().createEventForWalletId(walletId.stringValue, date: Date(timeIntervalSince1970: date.doubleValue/1000), withTitle: title)
    }
    
    @objc func removeReminderForWalletId(_ walletId: NSNumber, removeAll: NSNumber) {
        EventManager().removeEventForWalletId(walletId.stringValue, deleteAllEvent: removeAll.boolValue)
    }
    
    @objc func viewOffers(_ walletData: String) {
        DispatchQueue.main.async {
            if let topmostvc = Utilities.topMostPresenterViewController as? BaseNavigationController{
                let vcs = topmostvc.viewControllers
                if vcs.count > 0, vcs.last is AllOffersVC { return } // this is a PATCH will fix when "DeeplinksData" observer will remove correctory
                if let jsonData = walletData.data(using: .utf8), let walletObj: WalletCard = try? JSONParserSwift.parse(data: jsonData) {
                    let vc = AllOffersVC()
                    vc.fromScreen = .fromHome
                    vc.userCards = [Card(walletCard: walletObj)]
                    topmostvc.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func viewAllOffers(_ walletData: String, category: String, fromScreen: String, selectedIndex: NSNumber) {
        DispatchQueue.main.async {
            if let topmostvc = Utilities.topMostPresenterViewController as? BaseNavigationController{
                let vcs = topmostvc.viewControllers
                if vcs.count > 0, vcs.last is AllOffersVC { return } // this is a PATCH will fix when "DeeplinksData" observer will remove correctory
                if let jsonData = walletData.data(using: .utf8), let walletObj: WalletCardsData = try? JSONParserSwift.parse(data: jsonData) {
                    print("DEEPLINK FOR OFFER \(category)")
                    var cards = [Card]()
                    walletObj.cards?.forEach({ (card) in
                        cards.append(Card(walletCard: card))
                    })
                    let vc = AllOffersVC()
                    if let fromScreen = FromScreen(rawValue: fromScreen){
                        vc.fromScreen = fromScreen
                    }
                    if !category.isEmpty {
                        vc.filterName = category
                    }
                    vc.segmentIndex = selectedIndex as? Int
                    vc.userCards = cards
                    topmostvc.pushViewController(vc, animated: true)
                }else{
                    let vc = AllOffersVC()
                    vc.segmentIndex = selectedIndex.intValue
                    topmostvc.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func viewBankBranches(_ walletData: String) {
        DispatchQueue.main.async {
            if let jsonData = walletData.data(using: .utf8), let walletObj: WalletCard = try? JSONParserSwift.parse(data: jsonData) {
                EventTracking.shared.eventTracking(name: .findBanks,[.location: Event.EventParams.threeDotMenu])
                let vc = BankMapVC()
                vc.issuerModel = walletObj
                if let topmostvc = Utilities.topMostPresenterViewController as? BaseNavigationController{
                    topmostvc.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func viewOnlineOfferDetail(_ offerData: String) {
        DispatchQueue.main.async {
            if let jsonData = offerData.data(using: .utf8), let offerModel: BankCardOffers = try? JSONParserSwift.parse(data: jsonData) {
                let vc = OnlineOfferDetailVC()
                vc.fromScreen = .fromHome
                vc.selectedOffer = offerModel
                if let topmostvc = Utilities.topMostPresenterViewController as? BaseNavigationController{
                    topmostvc.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func submitRatingOnAppStore() {
        DispatchQueue.main.async {
            Utilities.openReview()
        }
    }
    
    @objc func refreshCards() {
        ChatBridge.refreshOffersAndCards()
    }
    
    @objc func getIssuers(_ callback:RCTResponseSenderBlock) {
        callback([RWDataManager.stringFromFile(issuerFile) ?? "{}"])
    }
    
    @objc func saveIssuers(_ issuers:String) {
        RWDataManager.writeString(issuers, to: issuerFile)
    }
    
    @objc func getBBQuestionsRemoteVersion(_ callback:RCTResponseSenderBlock) {
        callback([RemoteConfigManager.shared.bbQquestionDataVersion])
    }
    
    @objc func getBuybackDeclarationData(_ callback:RCTResponseSenderBlock) {
        callback([RemoteConfigManager.shared.buybackDeclarationData])
    }
}

extension ChatBridge {
    private func routeToInspection(activationCode: String, pincode: String) {
        EventTracking.shared.eventTracking(name: .ScheduleInspection, [.location: Event.EventParams.activation])
        
        let purchaseDate = Date.currentDate()
        
        let tempCustomerDetail = TempCustomerDetail(dictionary: [:])
        tempCustomerDetail.pinCode = pincode
        tempCustomerDetail.relationshipCode = "self"
        let addressRegistrationVC = AddressRegistrationVC()
        addressRegistrationVC.activationCode = activationCode
        addressRegistrationVC.pincode = pincode
        addressRegistrationVC.customerDetails = tempCustomerDetail
        addressRegistrationVC.purchasedDate = purchaseDate
        addressRegistrationVC.isAdding = true
        ChatBridge.routeToMembershipTab()
        rootNavVC?.pushViewController(addressRegistrationVC, animated: true)
    }
    
    private func showPrintOption(urlstring: String) {
        DispatchQueue.main.async {[weak self] in
            self?.openWebView(urlString: urlstring)
        }
    }
    
    private func openWebView(urlString: String){
        let webViewVC = WebViewVC()
        webViewVC.isLinkWebView = true
        webViewVC.isShowShareOption = true
        webViewVC.screenTitle = Strings.SalesRevamp.template
        webViewVC.urlString = urlString
        Utilities.topMostPresenterViewController.present(BaseNavigationController(rootViewController: webViewVC), animated: true, completion: nil)
    }
    
    @objc func goToMHCMainNavigator(_ startMHC: String, mhcCategory: NSDictionary) {
        DispatchQueue.main.async {
            let topMostView = Utilities.topMostPresenterViewController
            if let navVC = topMostView as? BaseNavigationController, let tabVC = navVC.viewControllers.first as? TabVC {
                let startMHCEnum:StartMHC = StartMHC(rawValue: startMHC) ?? StartMHC.START_COMPLETE_MHC
                switch startMHCEnum {
                case .START_COMPLETE_MHC:
                    tabVC.routeToCompleteMHC()
                case .START_SECTIONAL_MHC:
                    tabVC.routeToMHCTest(mhcCategory: mhcCategory)
                }
            }
        }
    }
    
    @objc func handleSpotLight(_ deepLinkURL: String){
        DispatchQueue.main.async {
            var deepLinkURL = deepLinkURL
            if let tabVC = rootTabVC {
                if var url = URL(string: deepLinkURL), deepLinkURL.contains("category=QR") {
                    url = url.appending("eventLocation", value: "Spotlight")
                    deepLinkURL = url.absoluteString
                }
                tabVC.goToSpotLightActions(url: deepLinkURL)
            }
        }
    }
    
    @objc func routeToCardManagement(){
        DispatchQueue.main.async {
            EventTracking.shared.eventTracking(name: .myCards, [.location: Event.EventParams.homeTab])
            let cardManagementVC = CardManagementVC()
            if let topMostView = Utilities.topMostPresenterViewController as? BaseNavigationController {
                if let tabVC = topMostView.topViewController as? TabVC {
                    tabVC.present(cardManagementVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func updateUserInfo(_ userInfo:NSDictionary){
        DispatchQueue.main.async {
            if let user = UserCoreDataStore.currentUser {
                if let name = userInfo.object(forKey: "userName") as? String{
                    user.userName = name
                }
            }
            if let customer = CustomerDetailsCoreDataStore.currentCustomerDetails {
                if let name = userInfo.object(forKey: "userName") as? String{
                    customer.firstName = name
                }
                if let email = userInfo.object(forKey: "userEmail") as? String{
                    customer.email = email
                }
            }
            CoreDataStack.sharedStack.saveMainContext()
        }
    }
    
    @objc func sendScheduleVisitEvent(_ location: String, appliance: String, count: String, serviceType: String, cost: String, date: String, time: String){
        DispatchQueue.main.async{
            EventTracking.shared.eventTracking(name: .SRVisitDT_Submit,[.location: location, .appliance: appliance, .count: count, .serviceType: serviceType, .cost: cost, .date: date.toDate(withFormat: "dd-MMM-yyyy"), .time: time])
        }
    }
    
    @objc func sendSODDetailsSubmitEvent(_ appliance: String, count: String, serviceType: String, cost: String){
        DispatchQueue.main.async {
            let eventDict:[String:String] = [Event.EventKeys.appliance.rawValue: appliance, Event.EventKeys.count.rawValue: count, Event.EventKeys.serviceType.rawValue: serviceType, Event.EventKeys.cost.rawValue: cost]
            EventTracking.shared.addEventToFirebase(Event.Events.sodDetailsSubmit.rawValue.replacingOccurrences(of: " ", with: "_"), eventDict)
        }
    }
    
    @objc func logApppsFlyerEvent(_ eventName: String, eventAttributes: NSDictionary){
        DispatchQueue.main.async {
            EventTracking.shared.addToAppsFlyer(name: eventName, model: eventAttributes as? [String : Any])
        }
    }
    
    @objc func sendPaymentSuccessEvent(_ eventData:String){
        DispatchQueue.main.async {
            if let data = eventData.data(using: .utf8){
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        var dict = [Event.EventKeys: Any]()
                        dict[.activity] = json["activity"] ?? ""
                        dict[.netAmount] = json["netAmount"] ?? 0
                        dict[.product] = json["product"] ?? ""
                        dict[.category] = json["category"] ?? ""
                        dict[.price] = json["price"] ?? 0
                        dict[.planCode] = json["planCode"]
                        dict[.serviceType] = json["serviceType"] ?? ""
                        dict[.buName] = Constants.SchemeVariables.partnerBuName
                        dict[.subCat] = json["subCategory"] ?? ""
                        dict[.planName] = json["planName"] ?? ""
                        EventTracking.shared.eventTracking(name: .payment, dict)
                    }
                }catch let error as NSError {
                    print("Failed to load json data: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    @objc func showMyServiceRequestsScreen(){
        DispatchQueue.main.async {
            if let tabVC = rootTabVC {
                let vc = MyServiceRequestsVC()
                tabVC.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func showSRDetails(_ stringifiedData: String){
        DispatchQueue.main.async {
            let dataString = stringifiedData
            if let data = dataString.data(using: .utf8){
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let serviceRequest = SearchService(dictionary: json)
                        let timeline = TimelineBaseVC()
                        timeline.srId = serviceRequest.serviceRequestId?.stringValue ?? ""
                        timeline.boolHistoryTimeLine = false
                        timeline.crmTrackingNumber = serviceRequest.refPrimaryTrackingNo
                        timeline.serviceRequestSourceType = serviceRequest.serviceRequestSourceType
                        if let tabVC = rootTabVC {
                            tabVC.navigationController?.pushViewController(timeline, animated: true)
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
    }
    
    @objc func rateCompletedService(_ stringifiedData: String){
        DispatchQueue.main.async {
            let dataString = stringifiedData
            if let data = dataString.data(using: .utf8){
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let serviceRequest = SearchService(dictionary: json)
                        let serviceRatingVC = ServiceRatingVC()
                        serviceRatingVC.serviceRequestID = serviceRequest.serviceRequestId?.stringValue ?? ""
                        serviceRatingVC.serviceRequestNumber = serviceRequest.refPrimaryTrackingNo
                        serviceRatingVC.productCode = serviceRequest.assets?[0].productCode
                        serviceRatingVC.serviceRequestType = serviceRequest.serviceRequestType
                        serviceRatingVC.serviceAddress = serviceRequest.serviceAddress
                        if let navVC = rootNavVC, let tabVC = navVC.viewControllers.first as? TabVC {
                            tabVC.navigationController?.pushViewController(serviceRatingVC, animated: true)
                        }
                    }
                }catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
    }
    
    @objc func rescheduleServiceRequest(_ stringifiedData: String){
        DispatchQueue.main.async {
            if let navVC = rootNavVC {
                navVC.popToRootViewController(animated: false)
                let paymentVC = OAPaymentVC()
                paymentVC.addToInitialProperties(["data": stringifiedData,"service_Type": "SOD","isRescheduleSlot":true])
                rootTabVC?.present(paymentVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func showPaymentPending(_ stringifiedData: String){
        DispatchQueue.main.async {
            if let navVC = rootNavVC {
                navVC.popToRootViewController(animated: false)
                let paymentVC = OAPaymentVC()
                paymentVC.addToInitialProperties(["data": stringifiedData,"service_Type": "SOD","isPendingPayment":true])
                rootTabVC?.present(paymentVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func rescheduleOnHoldRequest(_ stringifiedData: String){
        DispatchQueue.main.async {
            let dataString = stringifiedData
            if let data = dataString.data(using: .utf8){
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let serviceRequest = SearchService(dictionary: json)
                        let vc = ScheduleVisitVC()
                        EventTracking.shared.eventTracking(name: .SRReschedule)
                        vc.serviceReqId = serviceRequest.serviceRequestId?.stringValue
                        vc.crmTrackingNumber =  serviceRequest.refPrimaryTrackingNo
                        vc.scheduleVisitDelegate = nil
                        vc.getAPiCalledFirstTime = true
                        vc.serviceType =  serviceRequest.serviceRequestType
                        vc.addressDetail = AddressDetail(pincode:  serviceRequest.serviceAddress?.pincode)
                        if let navVC = rootNavVC {
                            navVC.popToRootViewController(animated: false)
                            navVC.pushViewController(vc, animated: true)
                        }
                    }
                }catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
    }
    
    @objc func getWebEngageAttr(_ callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            var dict = [String:Any]()
            if let link = sharedAppDelegate?.appOpenedWithLink, link != Constants.AppConstants.organic{
                dict["link"] = link
            }
            if let userType:String = UserDefaultsKeys.custType.get(), userType != CustType.notVerified{
                dict["userType"] = userType
            }
            dict["userGroup"] = AppUserDefaults.isCatalystUserGroup ? "Test group" : "Control group"
            if let product = sharedAppDelegate?.recoCatalystProduct {
                dict["Catalyst_Product"] = product
            }
            if let service = sharedAppDelegate?.recoCatalystService {
                dict["Catalyst_Service"] = service
            }
            if let subCategory = sharedAppDelegate?.recoCatalystCommSubcat {
                dict["Catalyst_CommSubcat"] = subCategory
            }
            if let rID = sharedAppDelegate?.recoCatalystRID {
                dict["Catalyst_RID"] = rID
            }
            if let uiComponentID = sharedAppDelegate?.recoCatalystUIComponentID {
                dict["Catalyst_UIComponentID"] = uiComponentID
            }
            if let rcID = sharedAppDelegate?.recoCatalystRCID {
                dict["Catalyst_RCID"] = rcID
            }
            
            let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: dict)) ?? Data(), encoding: .utf8) ?? ""
            callback([jsonString])
        }
    }
    
    @objc func getSODProceedToPaymentSubTitle(_ callback:RCTResponseSenderBlock) {
        var infoText: String = ""
        let value = RemoteConfigManager.shared.sodProductServiceScreen
        if let data = value.data(using: .utf8) {
            do {
                let dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                if let myDictionary = dictonary,
                   let buttonObj = myDictionary["buttonActionData"],
                   let paragraph = buttonObj["paragraph"] as? String {
                    infoText = paragraph
                }
                
            }catch {
            }
        }
        let jsonObject = ["sod_proceed_to_payment_subtitle": infoText]
        
        let jsonString = String(data: (try? JSONSerialization.data(withJSONObject: jsonObject)) ?? Data(), encoding: .utf8) ?? ""
        print(jsonString)
        callback([jsonString])
    }
    
    @objc func getCDNDataVersioning(_ callback:RCTResponseSenderBlock) {
        callback([RemoteConfigManager.shared.cdnDataVersioning])
    }
    
    @objc func openBuyPlanJourney(_ params: NSDictionary){
        DispatchQueue.main.async {
            if let tabVC = rootTabVC {
                let vc: BuyPlanJourneyVC = BuyPlanJourneyVC()
                if let dict = params as? [String: Any] {
                    vc.addToInitialProperties(dict)
                    if let isPresent = (params["isPresent"] as? Bool), isPresent{
                        vc.rootViewBGColor = .clear
                        Utilities.topMostPresenterViewController.presentOverCurrentContext(vc, animated: true)
                        return
                    }
                }
                tabVC.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    @objc func getDataFromFirebase(_ firebaseKey: String, callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            callback([RemoteConfigManager.shared.getFirebaseDataWith(key: firebaseKey)])
        }
    }
    
    @objc func getAllMemberships(_ callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            let memberships = Utilities.getAllMemberships()
            let dict = ["activeMemberships":Serialization.getDictionaryFromArray(array: memberships.memberships),"pendingMemberships":Serialization.getDictionaryFromArray(array: memberships.pendingMemberships)] as [String : Any]
            callback([dict])
        }
    }
    
    @objc func openPaymentModule(_ params: NSDictionary){
        DispatchQueue.main.async {
            if let reactVC = rootTopVC as? BuyPlanJourneyVC {
                let paymentVC = OAPaymentVC()
                paymentVC.addToInitialProperties(params as! [String : Any])
                reactVC.present(paymentVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func openRiskCalculator(_ params: NSDictionary){
        DispatchQueue.main.async {
            if let reactVC = rootTopVC as? BuyPlanJourneyVC {
                let riskCalculatorVC = RiskCalculatorVC()
                riskCalculatorVC.addToInitialProperties(params as! [String: Any])
                reactVC.present(riskCalculatorVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func openOSMap(_ fromTo:NSDictionary){
        DispatchQueue.main.async {
            if let from = fromTo.value(forKey: "from") as? NSDictionary, let to = fromTo.value(forKey: "to") as? NSDictionary {
                let fromCoordinate = CLLocationCoordinate2D(latitude: (from.value(forKey: "latitude") as? Double) ?? 0, longitude: (from.value(forKey: "longitude") as? Double) ?? 0)
                let toCoordinate = CLLocationCoordinate2D(latitude: (to.value(forKey: "latitude") as? Double) ?? 0, longitude: (to.value(forKey: "longitude") as? Double) ?? 0)
                Utilities.openGoogleMapsDirectionsUrl(from: fromCoordinate, to: toCoordinate)
            }
        }
    }
    
    @objc func showPopUpForStoreRating(_ location: String, callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            let topVC = Utilities.topMostPresenterViewController
            UserDefaultsKeys.Location.set(value: location)
            Utilities.checkForRatingPopUp(viewController: topVC, isShowForceFully: true){
                callback([])
            }
        }
    }
    
    @objc func sendBuyIDFenceEventFromRiskCalculator() {
        ChatBridge.sendNotification(withName: "BuyIDFenceFromEventRiskCalculator", body: [:])
    }
    
    @objc func openHARenewalFlow(_ paymentLinkURL:String){
        DispatchQueue.main.async {
            if let reactVC = rootTopVC as? BuyPlanJourneyVC {
                if let tabVC = rootTabVC {
                    let paymentVC = OAPaymentVC()
                    paymentVC.addToInitialProperties(["data": paymentLinkURL, "service_Type": "HS", "hasEW": false, "isRenewal": true])
                    reactVC.present(paymentVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func getBuySubtabSelectedIndex(_ callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            if let tabVC = rootTabVC, (rootTopVC as? BuyPlanJourneyVC) != nil {
                callback([["selectedCategory": tabVC.buySubTabSelectedCategory]])
            }
        }
    }
    
    @objc func handleMembershipTabActions(_ action: String, data: NSDictionary){
        DispatchQueue.main.async {
            rootTabVC?.handleMembershipTabActions(action: action, data: data)
        }
    }
    
    @objc func getProductSubcategories(_ callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            callback([["categoriesData": CacheManager.shared.productCategories]])
        }
    }
    
    @objc func routeToBuyback(_ data: NSDictionary){
        DispatchQueue.main.async {
            if let initialProps = data as? [String:Any] {
                let buybackVC = BuybackVC()
                buybackVC.addToInitialProperties(initialProps)
                rootTabVC?.present(buybackVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func openPaymentFlow(_ initialProps: NSDictionary) {
        DispatchQueue.main.async {
            if let props = initialProps as? [String: Any] {
                if let topMostView = Utilities.topMostPresenterViewController as? BaseNavigationController {
                    let paymentVC = OAPaymentVC()
                    paymentVC.addToInitialProperties(props)
                    topMostView.present(paymentVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func refreshTabBarMemberships() {
        DispatchQueue.main.async {
            ChatBridge.refreshMembershipOnReactNative()
        }
    }
    
    @objc func refreshMemberships() {
        DispatchQueue.main.async {
            ChatBridge.refreshMembershipOnReactNative()
        }
    }
    
    @objc func refreshOnGoingSRScreen(_ jsonData: NSDictionary) {
        DispatchQueue.main.async {
            if let memberships = jsonData["memberships"] {
                if let data = try? JSONSerialization.data(withJSONObject: memberships, options: []), let jsonString = String(data: data, encoding: .utf8) {
                    ChatBridge.sendNotification(withName: "RefreshOnGoingSRScreen", body: ["memberships": jsonString])
                }
            }
        }
    }
    
    @objc func getMembershipResponse(_ callback: @escaping RCTResponseSenderBlock) {
        DispatchQueue.main.async {
            if let tabVC = rootTabVC, let response = tabVC.customerMembershipResponse {
                if let data = try? JSONSerialization.data(withJSONObject: Serialization.getDictionaryFromObject(object: response), options: []), let jsonString = String(data: data, encoding: .utf8) {
                    callback([["membershipResponse": jsonString]])
                }
            }
        }
    }
    
    @objc func showBuybackServicePicker(_ callback: @escaping RCTResponseSenderBlock){
        DispatchQueue.main.async {
            let serviceList: [ServicePickerOption] = [.sellYourPhone, .bookAService]
             let servicePicker = ServicePickerView()
             servicePicker.setViewModel(serviceList, delegate: nil, index: 0)
            servicePicker.onServiceSelection = { index in
                Utilities.topMostPresenterViewController.dismiss(animated: false) {
                    callback([index])
                }
            }
             Utilities.presentPopover(view: servicePicker, height: ServicePickerView.height(forModel: serviceList))
        }
    }
}

private let issuerFile = "issuer.json"

class RWDataManager: NSObject {
    
    class func writeString(_ data: String, to fileName: String) {
        try? data.write(to: fileUrl(fileName), atomically: true, encoding: .utf8)
    }
    
    class func stringFromFile(_ fileName: String) -> String? {
        return try? String(contentsOf: fileUrl(fileName), encoding: .utf8)
    }
    
    private class func fileUrl(_ fileName: String) -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(fileName)
    }
}

// ALL THE NEW WORK
extension ChatBridge {
    @objc class func refreshMembershipOnReactNative() {
        sendNotification(withName: "RefreshMemberships", body: 0)
    }
    
    @objc class func changeTabIndex(_ tabIndex: Int) {
        sendNotification(withName: "ChangeTabSelected", body: ["tabIndex": tabIndex])
    }
    
    @objc class func updateNotificationCount(_ count: Int) {
        sendNotification(withName: "UpdateNotificationCount", body: ["notiCount":count])
    }
    
    @objc func updateMembershipDataOnNative(_ jsonData: NSDictionary?) {
        DispatchQueue.main.async {
            if let tabVC = rootTabVC {
                if let data = jsonData as? [String : Any] {
                    tabVC.handledMembershipResponse(membershipResponse: CustomerMembershipDetailsResponseDTO(dictionary: data))
                } else {
                    tabVC.handledMembershipResponse(membershipResponse: nil)
                }
            }
        }
    }
    
    @objc func loadInitialTabBar(_ jsonData: NSDictionary?) {
        DispatchQueue.main.async {
            if let tabVC = rootTabVC {
                if let data = jsonData as? [String : Any] {
                    tabVC.handledInitialMembershipResponse(membershipResponse: CustomerMembershipDetailsResponseDTO(dictionary: data))
                } else {
                    tabVC.handledInitialMembershipResponse(membershipResponse: nil)
                }
            }
        }
    }
    
    @objc func goToInboxScreen() {
        DispatchQueue.main.async {
            rootTabVC?.notificationClicked()
        }
    }
    
    @objc func tabChanged(_ index: NSNumber) {
        DispatchQueue.main.async {
            rootTabVC?.tabBarIndexChanged(index.intValue)
        }
    }
    
    @objc func showDownloadingErrorScreen(_ error: String) {
        DispatchQueue.main.async {
            rootTabVC?.handleOnError(TFLiteModelsDownloadError(rawValue: error)!)
        }
    }
    
    @objc func performPostDownload(_ activationCode: String) {
        DispatchQueue.main.async {
            rootTabVC?.handlePostDownloadAction(activationCode: activationCode)
        }
    }
    
    @objc func showDownloadingProgressScreen() {
        DispatchQueue.main.async {
            let vc = DownloadingStatusVC()
            rootTabVC?.downloadManager.delegate = vc
            rootTabVC?.presentInFullScreen(vc, animated: true)
        }
    }
}
