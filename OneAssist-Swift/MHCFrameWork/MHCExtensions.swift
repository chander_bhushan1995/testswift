//
//  MHCExtensions.swift
//  MHC
//
//  Created by Ashish Bhandari on 04/05/17.
//  Copyright © 2017 Sudhir. All rights reserved.
//

import Foundation
import UIKit

typealias AlertAction = ()->()

extension NSObject {
    static  var className : String {
        return String(describing: self)
    }
}

extension UIView {
    
    
    var height : CGFloat {
        get {
            return self.frame.size.height;
        }
        set {
            self.frame.size.height = newValue;
        }
    }
    
    var width : CGFloat {
        get {
            return self.frame.size.width;
        }
        set {
            self.frame.size.width = newValue;
        }
    }
    
    class func instanceFromNib(bundle:Bundle) -> UIView {
        
        let nib = UINib(nibName: self.className, bundle: bundle);
        
        let arrNib = nib.instantiate(withOwner: nil, options: nil);
        return (arrNib[0] as? UIView)!
    }
    
    func addSubviewWithConstraints(_ view: UIView, contentInsets insets: UIEdgeInsets = .zero) {
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: insets.left).isActive = true
        topAnchor.constraint(equalTo: view.topAnchor, constant: insets.top).isActive = true
        view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: insets.right).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: insets.bottom).isActive = true
    }
    
    
}


extension UIViewController {
    
    func addBackButton() {
        let btnLeftMenu: UIButton = UIButton()
        let image = UIImage.init(named: "backButtonImage", in: Bundle.init(for: type(of: self)), compatibleWith: nil);
        btnLeftMenu.setImage(image, for: UIControl.State.normal)
        btnLeftMenu.setTitle("Back".localized(), for: UIControl.State.normal);
        btnLeftMenu.sizeToFit()
        btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backButtonClick(sender : UIButton){
        self.navigationController?.popViewController(animated: true);
    }
    
    public var isVisible: Bool {
        if isViewLoaded {
            return view.window != nil
        }
        return false
    }
    
    public var isTopViewController: Bool {
        // TODO: KAG, check here!
        if self.navigationController != nil {
            return self.navigationController?.visibleViewController === self
        } else {
            return self.presentedViewController == nil && self.isVisible
        }
    }
    
    public var isPresentVC: Bool {
        
        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        // TODO: KAG, check here!
        let presentingIsTabBar = presentingViewController is UITabBarController
        
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }
}

extension UIDevice {
    var systemSize: Int64? {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let totalSize = (systemAttributes[.systemSize] as? NSNumber)?.int64Value else {
                return nil
        }
        
        return totalSize
    }
    
    var systemFreeSize: Int64? {
        guard let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String),
            let freeSize = (systemAttributes[.systemFreeSize] as? NSNumber)?.int64Value else {
                return nil
        }
        
        return freeSize
    }
}

extension Date {
    func toString(dateFormat format: String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = gregorianCalendar
        return dateFormatter.string(from: self)
    }
    
}

public extension CGPoint {
    
    func rotate(by angle: CGFloat, from origin: CGPoint) -> CGPoint {
        let xt = x - origin.x
        let yt = y - origin.y
        
        let xr = xt * cos(angle) - yt * sin(angle)
        let yr = xt * sin(angle) + yt * cos(angle)
        
        let finalX = xr + origin.x
        let finalY = yr + origin.y
        
        return CGPoint(x: finalX, y: finalY)
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
       
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
