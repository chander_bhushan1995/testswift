//
//  PermissionObject.swift
//  MHCFrameWork
//
//  Created by Sudhir.Kumar on 28/09/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation
import AVFoundation
import CoreLocation
import Speech
import Photos

class Permission: NSObject {
    
    fileprivate var locationCallBack: ((CLAuthorizationStatus)->Void)?
    fileprivate var latestLocationWithPinCode: ((CLLocation?, String?)->Void)?
    fileprivate var latestLatLong:((CLLocation?)->Void)?
    //    fileprivate var currentLocation:(
    let locationManager = CLLocationManager()
    var locationEventName: String?
    static let shared = Permission()
    private(set) var locationAddress:String?
    private(set) var pincode:String?
    var location: CLLocation? {
        didSet {
            geocode()
        }
    }
    private var geocoder = CLGeocoder()
    
    override private init() {
        super.init()
    }
    
    func resetData() {
        locationCallBack = nil
        latestLocationWithPinCode = nil
        latestLatLong = nil
        locationAddress = nil
        pincode = nil
        location = nil
        locationManager.delegate = nil
    }
    
    static func checkMicroPhonePermission(authorizationStatus : @escaping (AVAuthorizationStatus) -> Void){
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        switch microPhoneStatus {
        // Microphone disabled in settings
        case .notDetermined:
            sharedAppDelegate?.isShowBlurView = false
            let audioSession =  AVAudioSession.sharedInstance()
            audioSession.requestRecordPermission({ (granted) in
                DispatchQueue.main.async {
                    
                    var action = ""
                    if granted {
                        action = Event.EventParams.allow
                        authorizationStatus(.authorized)
                    } else {
                        action = Event.EventParams.deny
                        authorizationStatus(.denied)
                    }
                    
                    EventTracking.shared.eventTracking(name: .micPermission, [.location: Event.EventParams.mhcMic, .action: action])
                }
            })
        default:
            authorizationStatus(microPhoneStatus)
        }
    }
    
    static func checkSpeechPermission(authorizationStatus : @escaping (SFSpeechRecognizerAuthorizationStatus) -> Void) {
        
        let status = SFSpeechRecognizer.authorizationStatus()
        switch status {
        case .notDetermined, .denied:
            sharedAppDelegate?.isShowBlurView = false
            SFSpeechRecognizer.requestAuthorization { (status) in
                DispatchQueue.main.sync {
                    authorizationStatus(status)
                }
            }
        default:
            authorizationStatus(status)
        }
    }
    
    static  func checkCameraPermission(eventLocation: String?, authorizationStatus: @escaping (AVAuthorizationStatus)->Void) {
        
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied:
            authorizationStatus(.denied)
        case .authorized :
            authorizationStatus(.authorized)
        case .restricted:
            authorizationStatus(.restricted)
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            sharedAppDelegate?.isShowBlurView = false
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                DispatchQueue.main.sync {
                    var action = ""
                    if granted {
                        action = Event.EventParams.allow
                        authorizationStatus(.authorized)
                    } else {
                        action = Event.EventParams.deny
                        authorizationStatus(.denied)
                    }
                    if let location = eventLocation {
                        EventTracking.shared.eventTracking(name: .cameraPermission, [.location: location, .action: action])
                    }
                }
                
            }
        }
    }
    
    func checkLocationPermission(eventLocation: String? = nil,  authorizationStatus: @escaping (CLAuthorizationStatus) -> Void) {
        
        locationCallBack = authorizationStatus
        locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .notDetermined:
            sharedAppDelegate?.isShowBlurView = false
            locationEventName = eventLocation
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        default:
            locationCallBack?(status)
        }
    }
    
    func getLocationPinCode(callBack: @escaping (CLLocation?, String?) -> Void) {
        latestLocationWithPinCode = callBack
        prefetchLocation()
    }
    
    func getLatestLatLong(callback: @escaping (CLLocation?)->Void){
        latestLatLong = callback
        prefetchLocation()
    }
    
    func prefetchLocation() {
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            self.locationManager.delegate = self
            locationManager.distanceFilter = 100.0
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.requestLocation()
        }else{
            latestLocationWithPinCode?(nil,nil)
            latestLatLong?(nil)
        }
    }
    
    func geocode() {
        if let latitude = MHCResult.shared.deviceDetails.Q_GPS.latitude, let longitude = MHCResult.shared.deviceDetails.Q_GPS.longitude {
            geocoder.reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { (placemarks, error) in
                if error != nil {
                    self.locationAddress = nil
                    self.pincode = nil
                } else if let placemark = placemarks?.first, let administrativeArea = placemark.administrativeArea {
                    if let subAdministrativeArea = placemark.subAdministrativeArea {
                        self.locationAddress = subAdministrativeArea + ", " + administrativeArea
                    } else {
                        self.locationAddress = administrativeArea
                    }
                    self.pincode = placemark.postalCode
                } else {
                    self.locationAddress = nil
                    self.pincode = nil
                }
                self.latestLocationWithPinCode?(self.location, self.pincode)
                self.latestLatLong?(self.location)
            }
        } else if let latestLocationWithPincodeCallback = latestLocationWithPinCode, let currentLocation = location {
            geocoder.reverseGeocodeLocation(CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)) { (placemarks, error) in
                if error != nil {
                    self.locationAddress = nil
                    self.pincode = nil
                } else if let placemark = placemarks?.first, let administrativeArea = placemark.administrativeArea {
                    if let subAdministrativeArea = placemark.subAdministrativeArea {
                        self.locationAddress = subAdministrativeArea + ", " + administrativeArea
                    } else {
                        self.locationAddress = administrativeArea
                    }
                    self.pincode = placemark.postalCode
                } else {
                    self.locationAddress = nil
                    self.pincode = nil
                }
                latestLocationWithPincodeCallback(self.location, self.pincode)
            }
        }else if let latestLatLong = latestLatLong, let currentLocation = location{
            latestLatLong(currentLocation)
        }
    }
    
    //Mark: Gallery Permission
    static func checkGalleryPermission()->PHAuthorizationStatus {
        if #available(iOS 14, *) {
            return PHPhotoLibrary.authorizationStatus(for: .readWrite)
        } else {
            return PHPhotoLibrary.authorizationStatus()
            // Fallback on earlier versions
        }
    }
    
    static func requestPhotosAuthorization(andThen f:((PHAuthorizationStatus)->())? = nil) {
        sharedAppDelegate?.isShowBlurView = false
        PHPhotoLibrary.requestAuthorization { authorizationStatus in
            OperationQueue.main.addOperation {
                switch authorizationStatus {
                case .authorized:
                    f?(authorizationStatus)
                    break
                case .denied, .notDetermined, .restricted:
                    f?(authorizationStatus)
                @unknown default:
                    f?(authorizationStatus)
                }
            }
        }
    }
}

extension Permission: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined {
            if status == .denied {
                if let locationEvent = self.locationEventName {
                    EventTracking.shared.eventTracking(name: .locationPermission, [.location: locationEvent, .action: "Deny"])
                    self.locationEventName = nil
                }
            }else if status == .authorizedAlways {
                if let locationEvent = self.locationEventName {
                    EventTracking.shared.eventTracking(name: .locationPermission, [.location: locationEvent, .action: "Allow"])
                    self.locationEventName = nil
                }
            }else if status == .authorizedWhenInUse {
                if let locationEvent = self.locationEventName {
                    EventTracking.shared.eventTracking(name: .locationPermission, [.location: locationEvent, .action: "Allow Once"])
                    self.locationEventName = nil
                }
            }
            locationCallBack?(status)
            
        }else {
            if locationCallBack != nil {
                locationCallBack?(status)
                
            }else {
                latestLocationWithPinCode?(nil, nil)
                latestLatLong?(nil)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.last
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        location = nil
        if locationCallBack != nil {
            locationCallBack?(CLLocationManager.authorizationStatus())
            
        }else {
            latestLocationWithPinCode?(nil, nil)
            latestLatLong?(nil)
        }
    }
}
