//
//  MHCConstants.swift
//  MHC
//
//  Created by Ashish Bhandari on 04/05/17.
//  Copyright © 2017 Sudhir. All rights reserved.
//

import Foundation
private let keyMHCCompletionStatus = "mhcCompletionStatus"
private let keyMHCUpdateDate = "mhcUpdateDate"
private let keyMHCScore = "mhcScore"
private let keyIsDataUpdateOnServerPending = "isDataUpdateOnServerPending"

enum MHCScreenTitles: String {
    case MicrophoneTest = "OAMicrophoneTestVC"
   
    func title() -> String {
        return self.rawValue.localized()
    }
}

enum MHCScore: Float {
    case excellent
    case good
    case average
    case poor
    
    init(rawValue: Float) {
        if rawValue == 1.0 { self = .excellent }
        else if rawValue >= 0.8 { self = .good }
        else if rawValue >= 0.6 { self = .average }
        else { self = .poor }
    }
    
    func toString() -> String {
        switch self {
        case .excellent:    return "Excellent"
        case .good:         return "Good"
        case .average:      return "Average"
        case .poor:         return "Poor"
        }
    }
    
    func mainImage() -> UIImage {
        switch self {
        case .excellent:
            return #imageLiteral(resourceName: "MHC_excellent_success")
        case .good:
            return #imageLiteral(resourceName: "MHC_excellent_success")
        case .average:
            return #imageLiteral(resourceName: "MHC_avg_success")
        case .poor:
            return #imageLiteral(resourceName: "MHC_poor_success")
        }
    }
}


enum MHCCompletionStatus: String {
    /// Completed : When a user completes all the components in the MHC test.
    case completed
    
    /// Skipped : When a user skips atleast 1 component in the MHC test & completes the test
    case skipped
    
    /// Cancelled : When a user cancels the MHC test
    case cancelled
    
    /// Pending : When a user drops out of the MHC test before completing all components
    case pending
    
    public init?(value: String) {
        self.init(rawValue: value)
    }
}


struct MHCConstants {
    
    static var mhcCompletionStatus: MHCCompletionStatus? {
        get {
            return MHCCompletionStatus(value: UserDefaults.standard.string(forKey: keyMHCCompletionStatus) ?? MHCCompletionStatus.completed.rawValue)
        } set {
            isDataUpdateOnServerPending = true
            if let newValue = newValue {
                UserDefaults.standard.set(newValue.rawValue, forKey: keyMHCCompletionStatus)
            } else {
                UserDefaults.standard.removeObject(forKey: keyMHCCompletionStatus)
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    static var mhcUpdateDate: Date? {
        get {
            return UserDefaults.standard.value(forKey: keyMHCUpdateDate) as? Date
        } set {
            if let newValue = newValue {
                UserDefaults.standard.set(newValue, forKey: keyMHCUpdateDate)
            } else {
                UserDefaults.standard.removeObject(forKey: keyMHCUpdateDate)
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    static var mhcScore: Float {
        get {
            return UserDefaults.standard.float(forKey: keyMHCScore)
        } set {
            UserDefaults.standard.set(newValue, forKey: keyMHCScore)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    static var isDataUpdateOnServerPending: Bool {
        get {
            return UserDefaults.standard.bool(forKey: keyIsDataUpdateOnServerPending)
        } set {
            UserDefaults.standard.set(newValue, forKey: keyIsDataUpdateOnServerPending)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var mhcScoreValue: String {
        return MHCScore(rawValue: mhcScore).toString()
    }
    
    static func reset() {
        MHCConstants.mhcCompletionStatus = .completed
        MHCConstants.mhcScore = 0
        MHCConstants.mhcUpdateDate = nil
    }
}

struct Platform {
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}
