import DeviceKit

extension Device {
    /// Returns screen resolution as a tuple
    var screenResolution: (width: Int, height: Int) {
        #if os(iOS)
        switch self {
        case .iPodTouch5, .iPodTouch6, .iPodTouch7: return (width: 640, height: 1136)
            
        case .iPhone4, .iPhone4s: return (width: 640, height: 960)
            
        case .iPhone5, .iPhone5c, .iPhone5s, .iPhoneSE: return (width: 640, height: 1136)
            
        case .iPhoneSE2, .iPhone6, .iPhone6s, .iPhone7, .iPhone8: return (width: 750, height: 1334)
            
        case .iPhone6Plus, .iPhone6sPlus, .iPhone7Plus, .iPhone8Plus: return (width: 1080, height: 1920)
            
        case .iPhoneX, .iPhoneXS: return (width: 1125, height: 2436)
            
        case .iPhoneXSMax: return (width: 1242, height: 2688)
            
        case .iPhoneXR: return (width: 828, height: 1792)
            
        case .iPhone11: return (width: 828, height: 1792)
        case .iPhone11Pro: return (width: 1125, height: 2436)
        case .iPhone11ProMax: return (width: 1242, height: 2688)
            
        case .iPhone12Mini: return (width: 1080, height: 2340)
            
        case .iPhone12, .iPhone12Pro: return (width: 1170, height: 2532)
            
        case .iPhone12ProMax: return (width: 1284, height: 2778)
            
        case .iPadMini, .iPad2: return (width: 768, height: 1024)
            
        case .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPad3, .iPad4, .iPad5, .iPad6, .iPadAir, .iPadAir2, .iPadPro9Inch: return (width: 1536, height: 2048)
            
        case .iPadAir3, .iPadPro10Inch: return (width: 1668, height: 2224)
            
        case .iPadPro12Inch, .iPadPro12Inch2, .iPadPro12Inch3, .iPadPro12Inch4: return (width: 2048, height: 2732)
            
        case .iPad7, .iPad8: return (width: 1620, height: 2160)
            
        case .iPadAir4: return (width: 1640, height: 2360)
            
        case .iPadPro11Inch, .iPadPro11Inch2: return (width: 1668, height: 2388)
            
        case .homePod: return (width: -1, height: -1)
        case .simulator(let model): return model.screenResolution
        case .unknown: return (width: -1, height: -1)
        }
        #elseif os(watchOS)
        switch self {
        case .appleWatchSeries0_38mm: return (width: 4, height: 5)
        case .appleWatchSeries0_42mm: return (width: 4, height: 5)
        case .appleWatchSeries1_38mm: return (width: 4, height: 5)
        case .appleWatchSeries1_42mm: return (width: 4, height: 5)
        case .appleWatchSeries2_38mm: return (width: 4, height: 5)
        case .appleWatchSeries2_42mm: return (width: 4, height: 5)
        case .appleWatchSeries3_38mm: return (width: 4, height: 5)
        case .appleWatchSeries3_42mm: return (width: 4, height: 5)
        case .appleWatchSeries4_40mm: return (width: 4, height: 5)
        case .appleWatchSeries4_44mm: return (width: 4, height: 5)
        case .simulator(let model): return model.screenResolution
        case .unknown: return (width: -1, height: -1)
        }
        #elseif os(tvOS)
        return (width: -1, height: -1)
        #endif
    }
    
    /// Returns camera resolution as a tuple
    var cameraResolution: (front: Float, back: Float) {
        #if os(iOS)
        switch self {
        case .iPodTouch5: return (front: 1.2, back: 5)
        case .iPodTouch6, .iPodTouch7: return (front: 1.2, back: 8)
            
        case .iPhone4: return (front: 0.3, back: 5)
        case .iPhone4s: return (front: 0.3, back: 8)
            
        case .iPhone5, .iPhone5c, .iPhone5s, .iPhone6, .iPhone6Plus: return (front: 1.2, back: 8)
        case .iPhoneSE: return (front: 1.2, back: 12)
            
        case .iPhone6s, .iPhone6sPlus: return (front: 5, back: 12)
            
        case .iPhone7, .iPhone7Plus, .iPhone8, .iPhone8Plus, .iPhoneX, .iPhoneXS, .iPhoneXSMax, .iPhoneXR: return (front: 7, back: 12)
            
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhone12, .iPhone12Mini, .iPhone12Pro, .iPhone12ProMax: return (front: 12, back: 12)
            
        case .iPad2: return (front: 0.3, back: 0.7)
            
        case .iPad3: return (front: 0.3, back: 5)
            
        case .iPadMini, .iPadMini2, .iPadMini3, .iPad4, .iPadAir: return (front: 1.2, back: 5)
            
        case .iPadMini4, .iPad5, .iPad6, .iPadAir2, .iPadPro12Inch, .iPad7, .iPad8: return (front: 1.2, back: 8)
            
        case .iPadMini5, .iPadAir3: return (front: 7, back: 8)
            
        case .iPadPro9Inch: return (front: 5, back: 12)
            
        case .iPhoneSE2, .iPadPro10Inch, .iPadPro11Inch, .iPadPro11Inch2, .iPadPro12Inch2, .iPadPro12Inch3, .iPadPro12Inch4, .iPadAir4: return (front: 7, back: 12)
            
        case .homePod: return (front: -1, back: -1)
        case .simulator(let model): return model.cameraResolution
        case .unknown: return (front: -1, back: -1)
        }
        #elseif os(watchOS)
        return (front: -1, back: -1)
        #elseif os(tvOS)
        return (width: -1, back: -1)
        #endif
    }
    
    var deviceDescription: String {
        switch self {
        case .simulator(let model): return model.safeDescription
        case let model: return model.safeDescription
        }
    }
}

extension UIDevice {
    static var currentDeviceModel: String {
        return Device.current.deviceDescription
    }
    
    static var currentDeviceName: String {
        return Device.current.name ?? ""
    }
    
    static var currentDeviceDiagonal: Double {
        return Device.current.diagonal
    }
    
    static var currentDeviceCameraResolution: (front: Float, back: Float) {
        return Device.current.cameraResolution
    }
    
    static var currentDeviceScreenResolution: (width: Int, height: Int) {
        return Device.current.screenResolution
    }
    
    static var isDeviceCharging: Bool {
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        if (UIDevice.current.batteryState != .unplugged) {
            print("Device is charging.")
            UIDevice.current.isBatteryMonitoringEnabled = false
            return true
        }
        UIDevice.current.isBatteryMonitoringEnabled = false
        return false
    }
    
    var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    var isJailBroken: Bool {
        get {
            if UIDevice.current.isSimulator { return false }
            if JailBrokenHelper.hasCydiaInstalled() { return true }
            if JailBrokenHelper.isContainsSuspiciousApps() { return true }
            if JailBrokenHelper.isSuspiciousSystemPathsExists() { return true }
            return JailBrokenHelper.canEditSystemFiles()
        }
    }
}

private struct JailBrokenHelper {
    //check if cydia is installed (using URI Scheme)
    static func hasCydiaInstalled() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: "cydia://")!)
    }
    
    //Check if suspicious apps (Cydia, FakeCarrier, Icy etc.) is installed
    static func isContainsSuspiciousApps() -> Bool {
        for path in suspiciousAppsPathToCheck {
            if FileManager.default.fileExists(atPath: path) {
                return true
            }
        }
        return false
    }
    
    //Check if system contains suspicious files
    static func isSuspiciousSystemPathsExists() -> Bool {
        for path in suspiciousSystemPathsToCheck {
            if FileManager.default.fileExists(atPath: path) {
                return true
            }
        }
        return false
    }
    
    //Check if app can edit system files
    static func canEditSystemFiles() -> Bool {
        let jailBreakText = "Developer Insider"
        do {
            try jailBreakText.write(toFile: jailBreakText, atomically: true, encoding: .utf8)
            return true
        } catch {
            return false
        }
    }
    
    //suspicious apps path to check
    static var suspiciousAppsPathToCheck: [String] {
        return ["/Applications/Cydia.app",
                "/Applications/blackra1n.app",
                "/Applications/FakeCarrier.app",
                "/Applications/Icy.app",
                "/Applications/IntelliScreen.app",
                "/Applications/MxTube.app",
                "/Applications/RockApp.app",
                "/Applications/SBSettings.app",
                "/Applications/WinterBoard.app"
        ]
    }
    
    //suspicious system paths to check
    static var suspiciousSystemPathsToCheck: [String] {
        return ["/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
                "/Library/MobileSubstrate/DynamicLibraries/Veency.plist",
                "/private/var/lib/apt",
                "/private/var/lib/apt/",
                "/private/var/lib/cydia",
                "/private/var/mobile/Library/SBSettings/Themes",
                "/private/var/stash",
                "/private/var/tmp/cydia.log",
                "/System/Library/LaunchDaemons/com.ikey.bbot.plist",
                "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
                "/usr/bin/sshd",
                "/usr/libexec/sftp-server",
                "/usr/sbin/sshd",
                "/etc/apt",
                "/bin/bash",
                "/Library/MobileSubstrate/MobileSubstrate.dylib"
        ]
    }
}
