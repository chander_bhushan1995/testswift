//
//  BuybackQuestionManager.swift
//  MHCFrameWork
//
//  Created by Varun Dudeja on 06/09/18.
//  Copyright © 2018 Sudhir.Kumar. All rights reserved.
//

import Foundation
import UIKit

private let mhcValidityDays = 7

class MHCQuestionData: ParsableModel {
    var data: [Questions]?
    var questionType: String?
    var mhcScore: NSNumber?
}

class Questions: ParsableModel {
    var answer: String = ""
    var questionId: String?
    var isCompleted: NSNumber?
    var isSingleChoice: NSNumber?
}

class BuybackQuestionManager {
    
    static let shared = BuybackQuestionManager()
    private(set) var mhcTestList: [MhcTestName] = []
    private(set) var mhcCategoryList: [MhcTestCategory] = []
    private(set) var mhcQuestionsData: MHCQuestionData?
    var isFromBuyback: Bool = false
    
    private init() {}
    
    func updateWith(mhcQuestionsData: MHCQuestionData) {
        self.mhcQuestionsData = mhcQuestionsData
        self.mhcTestList = mhcQuestionsData.data?.compactMap({MhcTestName.test(forQuestionId: $0.questionId)}) ?? []
        self.mhcCategoryList = MhcTestCategory.values.filter({!$0.buybackMandatoryTests().isEmpty})
        if let mhcQuestionsData = self.mhcQuestionsData {
            if let questions = mhcQuestionsData.data {
                for (index, question) in questions.enumerated() {
                    if let test = MhcTestName.test(forQuestionId: question.questionId) {
                        let status = test.getStatus()
                        if let days = Date().days(from: Date(timeIntervalSince1970: status.testDate)), days < mhcValidityDays {
                            question.answer = status.testResult ?? ""
                        } else {
                            test.setTestStatus(.result_not_attempted)
                            question.answer = HealthCheckStatus.result_not_attempted.rawValue
                        }
                    } else {
                        question.answer = HealthCheckStatus.result_not_available.rawValue
                    }
                    
                    mhcQuestionsData.data?[index] = question
                }
            }
            mhcQuestionsData.mhcScore = NSNumber(value: MhcTestName.testResult(mhcTestList, isFromBuyback: true))
        }
    }
    
    func dataString() -> String? {
        if let mhcQuestionsData = mhcQuestionsData {
            if let questions = mhcQuestionsData.data {
                for (index, question) in questions.enumerated() {
                    if question.questionId == "Q_ROOT"{
                        question.answer = UIDevice().isJailBroken ? "OPT-YES" : "OPT-NO"
                        mhcQuestionsData.data?[index] = question
                    }
                    else if let test = MhcTestName.test(forQuestionId: question.questionId) {
                        let status = test.getStatus()
                        if let days = Date().days(from: Date(timeIntervalSince1970: status.testDate)), days < mhcValidityDays {
                            question.answer = status.testResult ?? ""
                        } else {
                            test.setTestStatus(.result_not_attempted)
                            question.answer = HealthCheckStatus.result_not_attempted.rawValue
                        }
                    } else {
                        question.answer = HealthCheckStatus.result_not_available.rawValue
                    }
                    
                    mhcQuestionsData.data?[index] = question
                }
            }
            mhcQuestionsData.mhcScore = NSNumber(value: MhcTestName.testResult(mhcTestList, isFromBuyback: true))
            print(mhcQuestionsData)
            let resolveString = try? JSONParserSwift.getJSON(object: mhcQuestionsData)
            return resolveString
        } else {
            return nil
        }
    }
    
    func pendingCategories() -> [MhcTestCategory] {
        return MhcTestCategory.values.filter({!$0.buybackRemainingTests().isEmpty})
    }
    
    func doneCategories() -> [MhcTestCategory] {
        return MhcTestCategory.values.filter({$0.buybackRemainingTests().isEmpty})
    }
    
    func remainingTest() -> [MhcTestName] {
        return MhcTestName.notPassed(tests: self.mhcTestList)
    }
    
    func doneTest() -> [MhcTestName] {
        return MhcTestName.passed(tests: self.mhcTestList)
    }
    
    func failedTests() -> [MhcTestName] {
        return MhcTestName.filtered(tests: self.mhcTestList, with: [.result_fail], includingStatus: true)
    }
    
    func isRetry() -> Bool {
        return MhcTestName.filtered(tests: self.mhcTestList, with: [.result_not_attempted, .result_not_picked], includingStatus: true).isEmpty
    }
    
    func isAnyPassed() -> Bool {
        return !MhcTestName.passed(tests: self.mhcTestList).isEmpty
    }
    
    func isAllFailedOrSkipped() -> Bool {
        return MhcTestName.filtered(tests: self.mhcTestList, with: [.result_fail, .result_skipped], includingStatus: true).count == self.mhcTestList.count
    }
}
