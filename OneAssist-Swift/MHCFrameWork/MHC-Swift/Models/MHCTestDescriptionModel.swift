//
//  MHCTestDescriptionModel.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

enum TestState {
    case notStarted
    case started
    case completed
}

class MHCTestDescriptionModel {
    var image: UIImage?
    var testName: String?
    var testDescription: String?
    var state: TestState = .notStarted
    var testType: MhcOtherTests = .wifi
    var testResultImage = #imageLiteral(resourceName: "tickStatus")
}
