//
//  HealthCheckResult.swift
//  MHCFrameWork
//
//  Created by Varun Dudeja on 06/09/18.
//  Copyright © 2018 Sudhir.Kumar. All rights reserved.
//

import Foundation
import CoreTelephony

fileprivate let mhcResultFileName = "MHCResults.json"

fileprivate func getDocumentsDirectory() -> URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
}

fileprivate func mhcFileName() -> URL {
    return getDocumentsDirectory().appendingPathComponent(mhcResultFileName)
}

class MHCResult: NSObject, Codable {
    var deviceDetails: TestResults = TestResults()
    var Basic: BasicDetails = BasicDetails()
    var update: Bool = false
    
    static var shared = savedData() ?? MHCResult()
    
    func clearData() {
        deviceDetails.clearData()
        Basic = BasicDetails()
        update = false
    }
    
    class func save() {
        let encodedObject = try? JSONEncoder().encode(shared)
        if let encodedObjectJsonString = String(data: encodedObject!, encoding: .utf8) {
            print(mhcFileName())
            try? encodedObjectJsonString.write(to: mhcFileName(), atomically: true, encoding: .utf8)
        }
    }
    
    class func savedData() -> MHCResult? {
        if let encodedObjectJsonString = try? String(contentsOf: mhcFileName(), encoding: .utf8), let jsonData = encodedObjectJsonString.data(using: .utf8), let testResultObject = try? JSONDecoder().decode(MHCResult.self, from: jsonData) {
            print(jsonData)
            return testResultObject
        }
        return nil
    }
}

struct TestResults: Codable {
    
    var Q_MULTITOUCH: BaseResult = BaseResult(testName: MhcTestName.multitouch.rawValue)
    
    var Q_BACKCAMERA: BackCamera = BackCamera(testName: MhcTestName.backCamera.rawValue)
    var Q_FRONTCAMERA: FrontCamera = FrontCamera(testName: MhcTestName.frontCamera.rawValue)
    var Q_AUTOFOCUS: AutoFocus = AutoFocus(testName: MhcTestName.autoFocus.rawValue)
    var Q_FLASH: Flash = Flash(testName: MhcTestName.flash.rawValue)
    
    var Q_MIC: BaseResult = BaseResult(testName: MhcTestName.mic.rawValue)
    var Q_SPEAKER: Speaker = Speaker(testName: MhcTestName.speaker.rawValue)
    var Q_VIBRATION: Vibration = Vibration(testName: MhcTestName.vibration.rawValue)
    
    var Q_GPS: GPS = GPS(testName: MhcTestName.gps.rawValue)
    var Q_NFC: BaseResult = BaseResult(testName: MhcTestName.nfc.rawValue)
    
    var Q_VOLUMEUPBUTTON: BaseResult = BaseResult(testName: MhcTestName.volumeUpButton.rawValue)
    var Q_VOLUMEDOWNBUTTON: BaseResult = BaseResult(testName: MhcTestName.volumeDownButton.rawValue)
    
    var Q_GYROSCOPE: BaseResult = BaseResult(testName: MhcTestName.gyroscope.rawValue)
    var Q_ACCELEROMETER: BaseResult = BaseResult(testName: MhcTestName.accelerometer.rawValue)
    var Q_MAGNETOMETER: BaseResult = BaseResult(testName: MhcTestName.magnetometer.rawValue)
    var Q_PROXIMITY: BaseResult = BaseResult(testName: MhcTestName.proximity.rawValue)
    var Q_FINGERPRINT: BaseResult = BaseResult(testName: MhcTestName.fingerprint.rawValue)
    
    var Q_HEADPHONEJACK = BaseResult(testName: MhcTestName.headphoneJack.rawValue)
    var Q_CHARGINGPORT: Battery = Battery(testName: MhcTestName.chargingPort.rawValue)
    
    var Q_WIFI: BaseResult = BaseResult(testName: MhcTestName.wifi.rawValue)
    var Q_BLUETOOTH: BaseResult = BaseResult(testName: MhcTestName.bluetooth.rawValue)
    var Q_RAM: RAM = RAM(testName: MhcTestName.ram.rawValue)
    var Q_STORAGE: Storages = Storages(testName: MhcTestName.storage.rawValue)
    var Q_ANTENNA: BaseResult = BaseResult(testName: MhcTestName.antenna.rawValue)
    
    var Q_BATTERY: BaseResult = BaseResult(testName: MhcTestName.battery.rawValue)
    var Q_ROOT: Rooted = Rooted(testName: "Rooted")

    mutating func clearData() {
        Q_CHARGINGPORT = Battery(testName: MhcTestName.chargingPort.rawValue)
        Q_BACKCAMERA = BackCamera(testName: MhcTestName.backCamera.rawValue)
        Q_FRONTCAMERA = FrontCamera(testName: MhcTestName.frontCamera.rawValue)
        Q_AUTOFOCUS = AutoFocus(testName: MhcTestName.autoFocus.rawValue)
        Q_HEADPHONEJACK = BaseResult(testName: MhcTestName.headphoneJack.rawValue)
        Q_FLASH = Flash(testName: MhcTestName.flash.rawValue)
        Q_GPS = GPS(testName: MhcTestName.gps.rawValue)
        Q_MAGNETOMETER = BaseResult(testName: MhcTestName.magnetometer.rawValue)
        Q_MULTITOUCH = BaseResult(testName: MhcTestName.multitouch.rawValue)
        Q_MIC = BaseResult(testName: MhcTestName.mic.rawValue)
        Q_PROXIMITY = BaseResult(testName: MhcTestName.proximity.rawValue)
        Q_SPEAKER = Speaker(testName: MhcTestName.speaker.rawValue)
        Q_STORAGE = Storages(testName: MhcTestName.storage.rawValue)
        Q_VIBRATION = Vibration(testName: MhcTestName.vibration.rawValue)
        Q_ACCELEROMETER = BaseResult(testName: MhcTestName.accelerometer.rawValue)
        Q_GYROSCOPE = BaseResult(testName: MhcTestName.gyroscope.rawValue)
        Q_VOLUMEUPBUTTON = BaseResult(testName: MhcTestName.volumeUpButton.rawValue)
        Q_VOLUMEDOWNBUTTON = BaseResult(testName: MhcTestName.volumeDownButton.rawValue)
        Q_RAM = RAM(testName: MhcTestName.ram.rawValue)
        Q_WIFI = BaseResult(testName: MhcTestName.wifi.rawValue)
        Q_BLUETOOTH = BaseResult(testName: MhcTestName.bluetooth.rawValue)
        Q_FINGERPRINT = BaseResult(testName: MhcTestName.fingerprint.rawValue)
        Q_NFC = BaseResult(testName: MhcTestName.nfc.rawValue)
        Q_ANTENNA = BaseResult(testName: MhcTestName.antenna.rawValue)
        Q_BATTERY = BaseResult(testName: MhcTestName.battery.rawValue)
        Q_ROOT = Rooted(testName: "Rooted")
    }
}

protocol TestStatus {
    var testResult: String? { get set}
    var userIntervention: Bool { get set}
    var testTime: Double { get set}
    var testName: String { get set }
    var testDate: TimeInterval { get set }
    
    mutating func update(with result:TestStatus)
}

struct BaseResult: Codable, TestStatus {
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct Speaker: Codable, TestStatus {
    var speakerOutputInDb: Float?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var loudSpaekerResult: String?
    var receiverSpeakerResult: String?
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct Rooted: Codable,TestStatus {
    var testResult: String? = UIDevice().isJailBroken ? "OPT-YES" : "OPT-NO" {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}


struct Vibration: Codable, TestStatus {
    var varianceX: Float?
    var varianceY: Float?
    var varianceZ: Float?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct Storages: Codable, TestStatus {
    var internalStorage: Float?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct GPS: Codable, TestStatus {
    var latitude: Double?
    var longitude: Double?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct BackCamera: Codable, TestStatus {
    var backCamera: Float?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct FrontCamera: Codable, TestStatus {
    var frontCamera: Float?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct AutoFocus: Codable, TestStatus {
    var autofocusPoint: CGPoint?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct Battery: Codable, TestStatus {
    var batteryLevel: Float?
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct Flash: Codable, TestStatus {
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var tourchLevel: Float?
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct RAM: Codable, TestStatus {
    var testResult: String? = HealthCheckStatus.result_not_attempted.rawValue {
        didSet {
            testDate = Date().timeIntervalSince1970
        }
    }
    var totalRam: UInt64?
    var userIntervention: Bool = true
    var testTime: Double = 0
    var testName: String
    var testDate: TimeInterval
    
    init(testName: String) {
        self.testName = testName
        self.testDate = Date().timeIntervalSince1970
    }
    
    mutating func update(with result:TestStatus) {
        testResult = result.testResult
        userIntervention = result.userIntervention
        testTime = result.testTime
    }
}

struct BasicDetails: Codable {
    var systemOS: String?
    var osVersion: String?
    var brand: String?
    var serialNo: String?
    var model: String?
    var resolutionHeight: String?
    var resolutionWidth: String?
    var screenSize: String?
    var mcc: String?
    var mnc: String?
    var mhcTestStatus: String?
    var mhcTimeStamp: Double?
    var ramSize: String?
    var diskSize: String?
    var totalTestTime: Double?
    var customerId: String?
    var resultScore: Float?
    var mobileNumber: String?
    var userIntervention = true
    var mhcTestResult: String?
    
    init(totalTestTime: Double = 0) {
        self.osVersion = UIDevice.current.systemVersion
        self.serialNo = UIDevice.uuid.replacingOccurrences(of: "-", with: "")
        self.model = UIDevice.currentDeviceModel
        self.systemOS = UIDevice.current.systemName
        self.brand = "Apple"
        self.resolutionHeight = Int(UIScreen.main.bounds.height * UIScreen.main.scale).description
        self.resolutionWidth = Int(UIScreen.main.bounds.width * UIScreen.main.scale).description
        let netInfo = CTTelephonyNetworkInfo().subscriberCellularProvider
        self.mnc = netInfo?.mobileNetworkCode
        self.mcc = netInfo?.mobileCountryCode
        self.ramSize = "\(UInt64.ramSize) GB"
        self.diskSize = "\(UInt64.diskSize) GB"
        self.mhcTestStatus = MHCConstants.mhcCompletionStatus?.rawValue
        self.mhcTimeStamp = MHCConstants.mhcUpdateDate?.timeIntervalSince1970
        self.resultScore = MHCConstants.mhcScore
        self.totalTestTime = totalTestTime
        self.customerId = UserCoreDataStore.currentUser?.cusId
        self.mobileNumber = userMobileNumber
        self.mhcTestResult = MHCConstants.mhcScoreValue
    }
}
