//
//  MHCTestStatusInteractor.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCTestStatusPresentationLogic {
    func presentMHCData(response: MHCResult)
    func invalidate()
}

class MHCTestStatusInteractor: BaseInteractor, MHCTestStatusBusinessLogic {
    var presenter: MHCTestStatusPresentationLogic?
    
    // MARK: Business Logic Conformance
    func invalidate() {
        presenter?.invalidate()
    }
    
    func initializeMHCData() {
        self.presenter?.presentMHCData(response: MHCResult.shared)
    }
}

