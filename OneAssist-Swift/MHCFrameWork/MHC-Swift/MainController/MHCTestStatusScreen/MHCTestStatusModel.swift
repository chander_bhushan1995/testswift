//
//  MHCTestStatusModel.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

struct MHCTestStatusModel {
    var sections: [MHCTestStatusSectionModel] = []
}

struct MHCTestStatusSectionModel {
    var title: String?
    var cells: [MHCTestStatusCellModel] = []
}

struct MHCTestStatusCellModel {
    var title: NSAttributedString?
    var image: UIImage?
    var test: MhcTestName = .multitouch
    
    init(withTest test: MhcTestName) {
        self.test = test
    }
}
