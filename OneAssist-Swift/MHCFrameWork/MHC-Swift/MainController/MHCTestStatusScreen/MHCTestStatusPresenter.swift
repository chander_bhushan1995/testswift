//
//  MHCTestStatusPresenter.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCTestStatusDisplayLogic: class {
    func displayMHCData(_ plans: MHCTestStatusModel)
    func displayError(_ error: String)
}

class MHCTestStatusPresenter: BasePresenter, MHCTestStatusPresentationLogic {
    
    weak var viewController: MHCTestStatusDisplayLogic?
    var mhcData: MHCTestStatusModel = MHCTestStatusModel()
    
    // MARK: Presentation Logic Conformance
    
    func presentMHCData(response: MHCResult) {
        var model = MHCTestStatusModel()
        var sections = [MHCTestStatusSectionModel]()
        for category in MhcTestCategory.values.filter({$0.isPicked()}) {
            var section = MHCTestStatusSectionModel()
            section.title = category.rawValue.uppercased()
            var cells = [MHCTestStatusCellModel]()
            for test in category.tests() {
                let status = test.getStatus()
                var cell = MHCTestStatusCellModel(withTest: test)
                if status.testResult == HealthCheckStatus.result_pass.rawValue {
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.charcoalGrey])
                    cell.image = #imageLiteral(resourceName: "MHC_excellent_success")
                } else if status.testResult == HealthCheckStatus.result_fail.rawValue {
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.charcoalGrey])
                    cell.image = #imageLiteral(resourceName: "MHC_test_fail")
                } else if status.testResult == HealthCheckStatus.result_skipped.rawValue {
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.charcoalGrey])
                    cell.image = #imageLiteral(resourceName: "ic_resultSkip")
                } else {
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.disabledAndLines])
                    cell.image = #imageLiteral(resourceName: "Not_started")
                }
                
                cells.append(cell)
            }
            section.cells = cells
            sections.append(section)
        }
        model.sections = sections
        
        viewController?.displayMHCData(model)
    }
    
    func invalidate() {
        mhcData = MHCTestStatusModel()
    }
}

