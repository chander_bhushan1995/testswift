//
//  MHCTestStatusVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCTestStatusBusinessLogic {
    func initializeMHCData()
    func invalidate()
}

class MHCTestStatusVC: BaseVC {
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    var interactor: MHCTestStatusBusinessLogic?
    var testStatusData: MHCTestStatusModel = MHCTestStatusModel()
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
    }
    
    func recievedErrorOnRefreshing() {
        Utilities.removeLoader(count: &loaderCount)
    }
    
    // MARK:- private methods
    private func initializeView() {
        tableViewObj.allowsSelection = false
        tableViewObj.register(cell: MHCTestStatusCell.self)
        
        tableViewObj.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCTestStatusHeaderView)
        
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 48
        
        tableViewObj.sectionHeaderHeight = UITableView.automaticDimension
        tableViewObj.estimatedSectionHeaderHeight = 24
        
        tableViewObj.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.white
        
        interactor?.initializeMHCData()
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK:- Table DataSource
extension MHCTestStatusVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return testStatusData.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testStatusData.sections[section].cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = testStatusData.sections[indexPath.section].cells[indexPath.row]
        let cell: MHCTestStatusCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setViewModel(model)
        return cell
    }
}

// MARK:- Table Delegate
extension MHCTestStatusVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let model = testStatusData.sections[section]
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCTestStatusHeaderView) as! MHCTestStatusHeaderView
        headerView.setViewModel(model)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}

// MARK:- Display Logic Conformance
extension MHCTestStatusVC: MHCTestStatusDisplayLogic {
    
    func displayMHCData(_ data: MHCTestStatusModel) {
        self.testStatusData = data
        tableViewObj.dataSource = self
        tableViewObj.delegate = self
        tableViewObj.reloadData()
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
}

// MARK:- Configuration Logic
extension MHCTestStatusVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MHCTestStatusInteractor()
        let presenter = MHCTestStatusPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
