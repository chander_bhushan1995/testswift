//
//  MHCTestStatusCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCTestStatusCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var titleLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    func setViewModel(_ model: MHCTestStatusCellModel) {
        titleLabel.attributedText = model.title
        statusImage.image = model.image
    }
}
