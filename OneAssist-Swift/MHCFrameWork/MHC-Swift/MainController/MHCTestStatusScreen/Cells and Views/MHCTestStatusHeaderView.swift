//
//  MHCTestStatusHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCTestStatusHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel: TagsBoldGreyLabel!
    
    func setViewModel(_ model: MHCTestStatusSectionModel) {
        titleLabel.text = model.title
    }
    
    func setViewModel(_ title: String?) {
        titleLabel.text = title
    }

}
