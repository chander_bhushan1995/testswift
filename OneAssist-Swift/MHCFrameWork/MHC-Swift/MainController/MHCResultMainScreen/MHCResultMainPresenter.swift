//
//  MHCResultMainPresenter.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCResultMainDisplayLogic: class {
    func displayMHCData(_ plans: MHCResultMainModel)
    func displayError(_ error: String)
}

class MHCResultMainPresenter: BasePresenter, MHCResultMainPresentationLogic {
    
    weak var viewController: MHCResultMainDisplayLogic?
    var mhcData: MHCResultMainModel = MHCResultMainModel()
    
    // MARK: Presentation Logic Conformance
    
    func presentMHCData(response: MHCResult, isFromBuyBack: Bool) {
        var model = MHCResultMainModel()
        let mhcCompletionStatus = MHCConstants.mhcCompletionStatus
        let mhcScore = MHCScore(rawValue: MHCConstants.mhcScore)
        if  mhcCompletionStatus == .completed || mhcCompletionStatus == .skipped {
            if isFromBuyBack {
                // Buyback case
                switch mhcScore {
                case .excellent:
                    model.title = MhcStrings.ResultMainScreen.buybackTitleExcellent
                    model.subtitle = MhcStrings.ResultMainScreen.buybackSubtitleExcellent
                    model.image = mhcScore.mainImage()
                    model.color = .seaGreen
                case .good:
                    model.title = MhcStrings.ResultMainScreen.buybackTitleGood
                    model.subtitle = MhcStrings.ResultMainScreen.buybackSubtitleExcellent
                    model.image = mhcScore.mainImage()
                    model.color = .seaGreen
                case .average:
                    model.title = MhcStrings.ResultMainScreen.titleAverage
                    model.subtitle = MhcStrings.ResultMainScreen.buybackSubtitlePoor
                    model.image = mhcScore.mainImage()
                    model.color = .saffronYellow
                case .poor:
                    model.title = MhcStrings.ResultMainScreen.titlePoor
                    model.subtitle = MhcStrings.ResultMainScreen.buybackSubtitlePoor
                    model.image = mhcScore.mainImage()
                    model.color = UIColor.errorText
                }
                if BuybackQuestionManager.shared.mhcTestList.count != BuybackQuestionManager.shared.doneTest().count {
                    model.isPrimaryButtonContinue = mhcScore == .good
                    model.primaryButtonText = mhcScore == .good ? MhcStrings.Buttons.continue : MhcStrings.Buttons.retryTests
                    model.secondaryButtonText = mhcScore == .good ? MhcStrings.Buttons.retryTests : MhcStrings.Buttons.iWillDoItLater
                    model.hasSecondaryButton = true
                } else {
                    model.primaryButtonText = MhcStrings.Buttons.continue
                }
            } else if MhcTestCategory.values.contains(where: {$0.tests().contains(where: {$0.getStatus().testResult == HealthCheckStatus.result_not_picked.rawValue})}) {
                // Selective Cateogry case
                switch mhcScore {
                case .excellent:
                    model.title = MhcStrings.ResultMainScreen.mhcGroupResultTitleExcellent
                    model.subtitle = MhcStrings.ResultMainScreen.mhcGroupResultSubtitleExcellent
                    model.image = mhcScore.mainImage()
                    model.color = .seaGreen
                case .good:
                    model.title = MhcStrings.ResultMainScreen.mhcGroupResultTitleGood
                    model.subtitle = MhcStrings.ResultMainScreen.mhcGroupResultSubtitleExcellent
                    model.image = mhcScore.mainImage()
                    model.color = .seaGreen
                case .average:
                    model.title = MhcStrings.ResultMainScreen.titleAverage
                    model.subtitle = MhcStrings.ResultMainScreen.mhcGroupResultAverageSubtitle
                    model.image = mhcScore.mainImage()
                    model.color = .saffronYellow
                case .poor:
                    model.title = MhcStrings.ResultMainScreen.titlePoor
                    model.subtitle = MhcStrings.ResultMainScreen.mhcGroupResultPoorSubtitle
                    model.image = mhcScore.mainImage()
                    model.color = UIColor.errorText
                }
                model.primaryButtonText = MhcStrings.Common.viewDetails
                model.secondaryButtonText = MhcStrings.Common.completeRemaingTests
                model.hasSecondaryButton = true
            } else {
                // All category case
                switch mhcScore {
                case .excellent:
                    model.title = MhcStrings.ResultMainScreen.buybackTitleExcellent
                    model.subtitle = MhcStrings.ResultMainScreen.mhcResultExcellentSubtitle
                    model.image = mhcScore.mainImage()
                    model.color = .seaGreen
                case .good:
                    model.title = MhcStrings.ResultMainScreen.buybackTitleGood
                    model.subtitle = MhcStrings.ResultMainScreen.mhcResultExcellentSubtitle
                    model.image = mhcScore.mainImage()
                    model.color = .seaGreen
                case .average:
                    model.title = MhcStrings.ResultMainScreen.titleAverage
                    model.subtitle = MhcStrings.ResultMainScreen.mhcGroupResultAverageSubtitle
                    model.image = mhcScore.mainImage()
                    model.color = .saffronYellow
                case .poor:
                    model.title = MhcStrings.ResultMainScreen.titlePoor
                    model.subtitle = MhcStrings.ResultMainScreen.mhcGroupResultPoorSubtitle
                    model.image = mhcScore.mainImage()
                    model.color = UIColor.errorText
                }
                model.primaryButtonText = MhcStrings.Common.viewDetails
            }
        }
        
        viewController?.displayMHCData(model)
    }
    
    func invalidate() {
        mhcData = MHCResultMainModel()
    }
}
