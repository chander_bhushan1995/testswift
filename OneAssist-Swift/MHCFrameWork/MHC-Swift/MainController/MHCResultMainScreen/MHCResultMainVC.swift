//
//  MHCResultMainVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCTestResultDelegate {
    func result()
    func completePendingTest()
    func retryTests()
}

protocol MHCResultMainBusinessLogic {
    func initializeMHCData(isFromBuyBack: Bool)
    func invalidate()
}

class MHCResultMainVC: BaseVC {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var subtitleLabel: H3RegularBlackLabel!
    @IBOutlet weak var primaryButton: OAPrimaryButton!
    @IBOutlet weak var secondaryButton: OASecondaryButton!
    var isFromBuyBack: Bool = false
    var interactor: MHCResultMainBusinessLogic?
    var mhcResultMainData: MHCResultMainModel = MHCResultMainModel()
    var delegate: MHCTestResultDelegate?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
    }
    
    func recievedErrorOnRefreshing() {
        Utilities.removeLoader(count: &loaderCount)
    }
    
    // MARK:- private methods
    private func initializeView() {
        interactor?.initializeMHCData(isFromBuyBack:isFromBuyBack)
    }
    
    @IBAction func primaryButtonTapped(_ sender: Any) {
        if isFromBuyBack && mhcResultMainData.hasSecondaryButton && !mhcResultMainData.isPrimaryButtonContinue {
            dismiss(animated: false) {
                self.delegate?.retryTests()
            }
        } else {
            dismiss(animated: false) {
                self.delegate?.result()
            }
        }
    }
    
    @IBAction func secondaryButtonTapped(_ sender: Any) {
        if self.mhcResultMainData.hasSecondaryButton {
            if isFromBuyBack {
                if mhcResultMainData.isPrimaryButtonContinue {
                    dismiss(animated: false) {
                        self.delegate?.retryTests()
                    }
                } else {
                    dismiss(animated: false) {
                        self.delegate?.result()
                    }
                }
            } else {
                dismiss(animated: false) {
                    self.delegate?.completePendingTest()
                }
            }
        }
    }
    
    @IBAction func crossTapped(_ sender: Any) {
        dismiss(animated: true)
    }
}

// MARK:- Display Logic Conformance
extension MHCResultMainVC: MHCResultMainDisplayLogic {
    
    func displayMHCData(_ data: MHCResultMainModel) {
        self.mhcResultMainData = data
        imageView.image = self.mhcResultMainData.image
        subtitleLabel.text = self.mhcResultMainData.subtitle
        titleLabel.text = self.mhcResultMainData.title
        titleLabel.textColor = self.mhcResultMainData.color
        primaryButton.setTitle(self.mhcResultMainData.primaryButtonText, for: .normal)
        if self.mhcResultMainData.hasSecondaryButton {
            secondaryButton.setTitle(self.mhcResultMainData.secondaryButtonText, for: .normal)
            secondaryButton.isHidden = false
        } else {
            secondaryButton.isHidden = true
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
}

// MARK:- Configuration Logic
extension MHCResultMainVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MHCResultMainInteractor()
        let presenter = MHCResultMainPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
