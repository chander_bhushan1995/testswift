//
//  MHCResultMainInteractor.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCResultMainPresentationLogic {
    func presentMHCData(response: MHCResult, isFromBuyBack: Bool)
    func invalidate()
}

class MHCResultMainInteractor: BaseInteractor, MHCResultMainBusinessLogic {
    var presenter: MHCResultMainPresentationLogic?
    
    // MARK: Business Logic Conformance
    func invalidate() {
        presenter?.invalidate()
    }
    
    func initializeMHCData(isFromBuyBack: Bool) {
        self.presenter?.presentMHCData(response: MHCResult.shared, isFromBuyBack: isFromBuyBack)
    }
}
