//
//  MHCResultMainModel.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

struct MHCResultMainModel {
    var title: String?
    var subtitle: String?
    var color: UIColor?
    var image: UIImage?
    var primaryButtonText: String?
    var secondaryButtonText: String?
    var hasSecondaryButton: Bool = false
    var isPrimaryButtonContinue: Bool = false
}
