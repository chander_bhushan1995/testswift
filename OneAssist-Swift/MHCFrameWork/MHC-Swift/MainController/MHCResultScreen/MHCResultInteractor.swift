//
//  MHCResultInteractor.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 30/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCResultPresentationLogic {
    func presentMHCData(response: MHCResult)
    func invalidate()
}

class MHCResultInteractor: BaseInteractor, MHCResultBusinessLogic {
    var presenter: MHCResultPresentationLogic?
    
    // MARK: Business Logic Conformance
    func invalidate() {
        presenter?.invalidate()
    }
    
    func initializeMHCData() {
        self.presenter?.presentMHCData(response: MHCResult.shared)
    }
}

