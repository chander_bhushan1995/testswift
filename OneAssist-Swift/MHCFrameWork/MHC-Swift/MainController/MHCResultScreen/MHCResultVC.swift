//
//  MHCResultVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 30/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCResultBusinessLogic {
    func initializeMHCData()
    func invalidate()
}

class MHCResultVC: BaseVC {
    
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var startTestButton: OAPrimaryButton!
    @IBOutlet weak var bottomViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    var interactor: MHCResultBusinessLogic?
    var mhcResultData: MHCResultModel = MHCResultModel()
    var navigator: MHCMainNavigator?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
    }
    
    func recievedErrorOnRefreshing() {
        Utilities.removeLoader(count: &loaderCount)
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        title = MhcStrings.ResultScreen.screenTitle
        headerLabel.textColor = .white
        tableViewObj.allowsSelection = false
        tableViewObj.register(cell: MHCResultTestCell.self)
        tableViewObj.register(cell: MHCResultDeviceInfoCell.self)
        
        tableViewObj.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultAwesomeHeaderView)
        tableViewObj.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultInfoHeaderView)
        tableViewObj.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultScreenHeaderView)
        tableViewObj.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultTestHeaderView)
        tableViewObj.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultTitleHeaderView)
        
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 200
        
        tableViewObj.sectionHeaderHeight = UITableView.automaticDimension
        tableViewObj.estimatedSectionHeaderHeight = 2
        
        tableViewObj.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.whiteSmoke
        
        
        interactor?.initializeMHCData()
    }
    
    @IBAction func startTestTapped(_ sender: Any) {
        let categories = MhcTestCategory.values.filter({!$0.retryTests().isEmpty})
        let categorizedtests = categories.map({$0.retryTests()})
        EventTracking.shared.eventTracking(name: .MHCRetry, [.location: Event.EventParams.mhcHome, .screenName: Event.EventParams.resultScreen])
        navigator = MHCMainNavigator(categorizedtests: categorizedtests)
        navigator?.delegate = self
        navigator?.presentMainContainerVC(viewController: self)
        MHCConstants.mhcCompletionStatus = .pending
        MHCResult.save()
    }
}

// MARK:- Table DataSource
extension MHCResultVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return mhcResultData.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionData = mhcResultData.sections[section]
        return sectionData.isExpanded ? sectionData.cells.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionData = mhcResultData.sections[indexPath.section]
        if sectionData.isExpanded {
            let cellData = sectionData.cells[indexPath.row]
            if let model = cellData as? MHCResultDeviceInfoCellModel {
                let cell: MHCResultDeviceInfoCell = tableView.dequeueReusableCell(indexPath: indexPath)
                cell.setViewModel(model)
                return cell
            } else if let model = cellData as? MHCResultTestCellModel {
                let cell: MHCResultTestCell = tableView.dequeueReusableCell(indexPath: indexPath)
                cell.setViewModel(model)
                return cell
            }
        }
        return UITableViewCell()
    }
}

// MARK:- Table Delegate
extension MHCResultVC: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let model = mhcResultData.sections[section]
        switch model.headerType {
        case .awesomeHeader:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultAwesomeHeaderView) as! MHCResultAwesomeHeaderView
            headerView.setViewModel(model, delegate: self)
            return headerView
        case .screenHeader:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultScreenHeaderView) as! MHCResultScreenHeaderView
            headerView.setViewModel(model)
            return headerView
        case .titleHeader:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultTitleHeaderView) as! MHCResultTitleHeaderView
            headerView.setViewModel(model)
            return headerView
        case .infoHeader:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultInfoHeaderView) as! MHCResultInfoHeaderView
            headerView.setViewModel(model, section: section, delegate: self)
            return headerView
        case .testHeader:
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCResultTestHeaderView) as! MHCResultTestHeaderView
            headerView.setViewModel(model, section: section, delegate: self)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch mhcResultData.sections[section].headerType {
        case .awesomeHeader:
            return 152
        case .screenHeader:
            return 104
        case .titleHeader:
            return 52
        case .infoHeader:
            return 84
        case .testHeader:
            return 56
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}

// MARK:- Display Logic Conformance
extension MHCResultVC: MHCResultDisplayLogic {
    
    func displayMHCData(_ data: MHCResultModel) {
        self.mhcResultData = data
        tableViewObj.dataSource = self
        tableViewObj.delegate = self
        tableViewObj.reloadData()
        
        if let bottomModel = self.mhcResultData.bottomModel {
            bottomViewHeightConstraint.constant = 80
            startTestButton.isHidden = false
            startTestButton.setTitle(bottomModel.primaryButtonText, for: .normal)
        } else {
            bottomViewHeightConstraint.constant = 0
            startTestButton.isHidden = true
        }
        
        if let topModel = self.mhcResultData.topModel {
            headerImageView.image = topModel.image
            headerLabel.text = topModel.title
            headerView.backgroundColor = topModel.color
        }
        
        if [.excellent, .good].contains(MHCScore(rawValue: MHCConstants.mhcScore)) {
            // show rating pop-up if the score is good or excellent.
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                guard let self = self else { return }
                UserDefaultsKeys.Location.set(value: Event.EventParams.resultScreen)
                Utilities.checkForRatingPopUp(viewController: self)
            }
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
}

extension MHCResultVC: MHCResultAwesomeHeaderViewDelegate {
    func shareApp() {
        EventTracking.shared.eventTracking(name: .MHCShare,[.location: Event.EventParams.mhcHome])
        presentShareActivityView(shareObject: MhcStrings.Common.shareString)
    }
}

extension MHCResultVC: MHCResultHeaderExpandableDelegate {
    func toggleExpansion(_ section: Int) {
        let sections = mhcResultData.sections
        var indexes = [Int]()
        for (index, sectionData) in sections.enumerated() {
            if index == section {
                mhcResultData.sections[index].isExpanded = !sectionData.isExpanded
                indexes.append(index)
            } else if sectionData.isExpanded {
                mhcResultData.sections[index].isExpanded = false
                indexes.append(index)
            }
        }
        
        var y = self.tableViewObj.contentOffset.y
        self.tableViewObj.beginUpdates()
        UIView.setAnimationsEnabled(false)
        self.tableViewObj.reloadSections(IndexSet(indexes), with: .automatic)
        self.tableViewObj.endUpdates()
        let maxOffset = abs(self.tableViewObj.contentSize.height - self.tableViewObj.frame.size.height)
        if y > maxOffset {
            y = maxOffset
        }
        self.tableViewObj.contentOffset.y = y
        UIView.setAnimationsEnabled(true)
    }
}

extension MHCResultVC: MHCTestDelegate {
    func testFinished() {
        navigator = nil
        interactor?.initializeMHCData()
    }
}

// MARK:- Configuration Logic
extension MHCResultVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MHCResultInteractor()
        let presenter = MHCResultPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension MHCResultVC {
    

}

