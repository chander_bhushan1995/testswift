//
//  MHCResultModel.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 30/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum MHCResultHeaderType {
    /// for awesome card
    case awesomeHeader
    
    /// for screen card
    case screenHeader
    
    /// for title card
    case titleHeader
    
    /// for device info card
    case infoHeader
    
    /// for category info card
    case testHeader
}

struct MHCResultModel {
    var topModel: MHCResultTopModel?
    var sections: [MHCResultSectionModel] = []
    var bottomModel: MHCResultBottomModel?
}

struct MHCResultTopModel {
    var title: String?
    var image: UIImage?
    var color: UIColor?
}

struct MHCResultBottomModel {
    var primaryButtonText: String?
}

struct MHCResultSectionModel {
    var title: String?
    var subtitle: NSAttributedString?
    var icon: UIImage?
    var headerType: MHCResultHeaderType = .testHeader
    var isExpanded: Bool = false
    var cells: [Any] = []
}

struct MHCResultDeviceInfoCellModel {
    var osVersion: String?
    var resolution: String?
    var ram: String?
    var internalStorage: String?
    var backCamera: String?
    var frontCamera: String?
}

struct MHCResultTestCellModel {
    var title: NSAttributedString?
    var subTitle: NSAttributedString?
    var image: UIImage?
    var test: MhcTestName = .multitouch
    
    init(withTest test: MhcTestName) {
        self.test = test
    }
}
