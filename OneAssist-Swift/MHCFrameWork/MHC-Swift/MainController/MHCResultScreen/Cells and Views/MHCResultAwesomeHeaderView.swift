//
//  MHCResultAwesomeHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCResultAwesomeHeaderViewDelegate {
    func shareApp()
}

class MHCResultAwesomeHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var subtitleLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var shareButton: UIButton!
    
    var delegate: MHCResultAwesomeHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shareButton.layer.cornerRadius = 4
        shareButton.layer.borderWidth = 1
        shareButton.layer.borderColor = UIColor.brColor.cgColor
    }
    
    func setViewModel(_ model: MHCResultSectionModel, delegate: MHCResultAwesomeHeaderViewDelegate) {
        self.delegate = delegate;
        titleLabel.text = model.title
        subtitleLabel.attributedText = model.subtitle
    }
    
    @IBAction func shareAppTapped(_ sender: Any) {
        delegate?.shareApp()
    }
    
}
