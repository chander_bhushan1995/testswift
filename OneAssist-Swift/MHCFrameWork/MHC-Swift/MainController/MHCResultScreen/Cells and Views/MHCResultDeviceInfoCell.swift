//
//  MHCResultDeviceInfoCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCResultDeviceInfoCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var osVersionLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var internalStorageLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var resolutionLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var ramLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var backCameraLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var frontCameraLabel: SupportingTextBoldBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setViewModel(_ model: MHCResultDeviceInfoCellModel) {
        osVersionLabel.text = model.osVersion
        internalStorageLabel.text = model.internalStorage
        resolutionLabel.text = model.resolution
        ramLabel.text = model.ram
        backCameraLabel.text = model.backCamera
        frontCameraLabel.text = model.frontCamera
    }
}
