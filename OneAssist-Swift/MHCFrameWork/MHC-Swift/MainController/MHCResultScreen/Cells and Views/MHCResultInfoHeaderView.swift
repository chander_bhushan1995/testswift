//
//  MHCResultInfoHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCResultHeaderExpandableDelegate {
    func toggleExpansion(_ section: Int)
}

class MHCResultInfoHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleView: H3BoldLabel!
    @IBOutlet weak var subtitleView: SupportingTextRegularGreyLabel!
    @IBOutlet weak var chevornIcon: UIImageView!
    
    var delegate: MHCResultHeaderExpandableDelegate?
    var section = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(headerTapped)))
    }
    
    @objc func headerTapped() {
        delegate?.toggleExpansion(section)
    }
    
    func setViewModel(_ model: MHCResultSectionModel, section: Int, delegate: MHCResultHeaderExpandableDelegate) {
        self.delegate = delegate
        self.section = section
        productImageView.image = model.icon
        titleView.text = model.title
        subtitleView.attributedText = model.subtitle
        chevornIcon.image = #imageLiteral(resourceName: "downArrowBlue")
        chevornIcon.transform = CGAffineTransform(rotationAngle: model.isExpanded ? .pi : 0)
    }
}
