//
//  MHCResultTestHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCResultTestHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var titleView: H3RegularBlackLabel!
    @IBOutlet weak var subtitleView: SupportingTextRegularGreyLabel!
    @IBOutlet weak var chevornIcon: UIImageView!
    var delegate: MHCResultHeaderExpandableDelegate?
    var section = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(headerTapped)))
    }
    
    @objc func headerTapped() {
        delegate?.toggleExpansion(section)
    }
    
    func setViewModel(_ model: MHCResultSectionModel, section: Int, delegate: MHCResultHeaderExpandableDelegate) {
        self.delegate = delegate
        self.section = section
        categoryImageView.image = model.icon
        titleView.text = model.title
        subtitleView.attributedText = model.subtitle
        chevornIcon.image = #imageLiteral(resourceName: "downArrowBlue")
        chevornIcon.transform = CGAffineTransform(rotationAngle: model.isExpanded ? .pi : 0)
    }
}
