//
//  MHCResultScreenHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCResultScreenHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: H1BoldLabel!
    
    func setViewModel(_ model: MHCResultSectionModel) {
        titleLabel.text = model.title
    }
}
