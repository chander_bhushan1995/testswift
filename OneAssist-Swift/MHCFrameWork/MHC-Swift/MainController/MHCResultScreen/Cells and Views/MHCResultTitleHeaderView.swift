//
//  MHCResultTitleHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCResultTitleHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: TagsBoldGreyLabel!
    
    func setViewModel(_ model: MHCResultSectionModel) {
        titleLabel.text = model.title
    }
}
