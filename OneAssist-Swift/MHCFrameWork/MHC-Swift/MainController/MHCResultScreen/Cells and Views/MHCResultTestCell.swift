//
//  MHCResultTestCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCResultTestCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var titleLabel: SupportingTextBoldBlackLabel!
    @IBOutlet weak var subtitleLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    func setViewModel(_ model: MHCResultTestCellModel) {
        titleLabel.attributedText = model.title
        statusImage.image = model.image
        subtitleLabel.attributedText = model.subTitle
    }
}
