//
//  MHCResultPresenter.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 30/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//


import UIKit
import CoreMedia

protocol MHCResultDisplayLogic: class {
    func displayMHCData(_ plans: MHCResultModel)
    func displayError(_ error: String)
}

class MHCResultPresenter: BasePresenter, MHCResultPresentationLogic {
    
    weak var viewController: MHCResultDisplayLogic?
    var mhcData: MHCResultModel = MHCResultModel()
    
    // MARK: Presentation Logic Conformance
    
    func presentMHCData(response: MHCResult) {
        var model = MHCResultModel()
        
        let testResult = response
        _ = MHCConstants.mhcCompletionStatus
//        if  mhcCompletionStatus == .completed || mhcCompletionStatus == .skipped {
            var topModel = MHCResultTopModel()
            let mhcScore = MHCScore(rawValue: MHCConstants.mhcScore)
            let mobileInfoSections = self.mobileInfoSections(info: testResult.Basic)
            let categorySections = self.categorySections()
            switch mhcScore {
            case .excellent:
                topModel.title = MhcStrings.ResultScreen.topHeaderExcellent
                topModel.image = #imageLiteral(resourceName: "result_success")
                topModel.color = .seaGreen
                var awesomeSection = MHCResultSectionModel()
                awesomeSection.title = MhcStrings.ResultScreen.awesomeHeaderTitle
                awesomeSection.subtitle = NSAttributedString(string: MhcStrings.ResultScreen.awesomeHeaderSubtitle, attributes: [.foregroundColor: UIColor.gray])
                awesomeSection.headerType = .awesomeHeader
                model.sections.append(awesomeSection)
                model.sections.append(contentsOf: mobileInfoSections)
                model.sections.append(contentsOf: categorySections)
            case .good:
                topModel.title = MhcStrings.ResultScreen.topHeaderGood
                topModel.image = #imageLiteral(resourceName: "result_success")
                topModel.color = .seaGreen
                var awesomeSection = MHCResultSectionModel()
                awesomeSection.title = MhcStrings.ResultScreen.awesomeHeaderTitle
                awesomeSection.subtitle = NSAttributedString(string: MhcStrings.ResultScreen.awesomeHeaderSubtitle, attributes: [.foregroundColor: UIColor.gray])
                awesomeSection.headerType = .awesomeHeader
                model.sections.append(awesomeSection)
                model.sections.append(contentsOf: mobileInfoSections)
                model.sections.append(contentsOf: categorySections)
            case .average:
                topModel.title = MhcStrings.ResultScreen.topHeaderAverage
                topModel.image = #imageLiteral(resourceName: "result_avg")
                topModel.color = .saffronYellow
                var titleSection = MHCResultSectionModel()
                titleSection.title = MhcStrings.ResultScreen.sectionHeaderTitleAverage
                titleSection.headerType = .screenHeader
                model.sections.append(titleSection)
                model.sections.append(contentsOf: categorySections)
                model.sections.append(contentsOf: mobileInfoSections)
            case .poor:
                topModel.title = MhcStrings.ResultScreen.topHeaderPoor
                topModel.image = #imageLiteral(resourceName: "result_poor")
                topModel.color = UIColor.errorText
                var titleSection = MHCResultSectionModel()
                titleSection.title = MhcStrings.ResultScreen.sectionHeaderTitlePoor
                titleSection.headerType = .screenHeader
                model.sections.append(titleSection)
                model.sections.append(contentsOf: categorySections)
                model.sections.append(contentsOf: mobileInfoSections)
            }
            
            if MhcTestName.isRetryEnabled() {
                var bottomModel = MHCResultBottomModel()
                bottomModel.primaryButtonText = MhcStrings.Buttons.retryTests
                model.bottomModel = bottomModel
            }
            
            model.topModel = topModel
//        }
        
        viewController?.displayMHCData(model)
    }
    
    func invalidate() {
        mhcData = MHCResultModel()
    }
    
    private func mobileInfoSections(info: BasicDetails) -> [MHCResultSectionModel] {
        
        var section = MHCResultSectionModel()
        section.title = MhcStrings.ResultScreen.mobileInfoSectionTitle
        section.headerType = .titleHeader
        var infoSection = MHCResultSectionModel()
        infoSection.title = info.brand
        if let uuid = info.serialNo {
            infoSection.subtitle = NSAttributedString(string: "UUID: \(uuid)", attributes: [.foregroundColor: UIColor.gray])
        }
        infoSection.icon = #imageLiteral(resourceName: "result_mobileInfo")
        infoSection.headerType = .infoHeader
        var cell = MHCResultDeviceInfoCellModel()
        cell.osVersion = info.osVersion
        let resolution = UIDevice.currentDeviceScreenResolution
        cell.resolution = "\(resolution.width) by \(resolution.height)"
        cell.ram = info.ramSize
        cell.internalStorage = info.diskSize
        let cameraResolution = UIDevice.currentDeviceCameraResolution
        cell.frontCamera = "\(cameraResolution.front) MP"
        cell.backCamera = "\(cameraResolution.back) MP"
        infoSection.cells = [cell]
        return [section, infoSection]
    }
    
    private func categorySections() -> [MHCResultSectionModel] {
        var sections = [MHCResultSectionModel]()
        
        var section = MHCResultSectionModel()
        section.title = MhcStrings.ResultScreen.testResultSectionTitle
        section.headerType = .titleHeader
        sections.append(section)
        
        for category in MhcTestCategory.values.filter({!$0.isNotPicked()}) {
            var section = MHCResultSectionModel()
            section.title = category.name()
            section.headerType = .testHeader
            section.icon = category.categoryImage()
            var cells = [MHCResultTestCellModel]()
            var cleared = 0
            var failed = 0
            var skipped = 0
            let total = category.totalTest()
            
            var clearedText: NSAttributedString?
            var failedText: NSAttributedString?
            var skippedText: NSAttributedString?
            
            let subtitle = NSMutableAttributedString(string: "")
            for test in category.tests() {
                let status = test.getStatus()
                if status.testResult == HealthCheckStatus.result_pass.rawValue {
                    var cell = MHCResultTestCellModel(withTest: test)
                    cleared += 1
                    clearedText = NSAttributedString(string: cleared == total ? "\(cleared) of \(total) \(MhcStrings.ResultScreen.cleared)" : "\(cleared) \(MhcStrings.ResultScreen.cleared)", attributes: [.foregroundColor: UIColor.gray])
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.charcoalGrey])
                    cell.subTitle = test.testSuccessTitle()
                    cell.image = #imageLiteral(resourceName: "MHC_excellent_success")
                    cells.append(cell)
                } else if status.testResult == HealthCheckStatus.result_fail.rawValue {
                    var cell = MHCResultTestCellModel(withTest: test)
                    failed += 1
                    failedText = NSAttributedString(string: failed == total ? "\(failed) of \(total) \(MhcStrings.ResultScreen.failed)" : "\(failed) \(MhcStrings.ResultScreen.failed)", attributes: [.foregroundColor: UIColor.errorText])
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.charcoalGrey])
                    cell.subTitle = NSAttributedString(string: test.testFailedTitle(), attributes: [.foregroundColor: UIColor.gray])
                    cell.image = #imageLiteral(resourceName: "MHC_test_fail")
                    cells.append(cell)
                } else if status.testResult == HealthCheckStatus.result_skipped.rawValue {
                    var cell = MHCResultTestCellModel(withTest: test)
                    skipped += 1
                    skippedText = NSAttributedString(string: skipped == total ? "\(skipped) of \(total) \(MhcStrings.ResultScreen.skipped)" : "\(skipped) \(MhcStrings.ResultScreen.skipped)", attributes: [.foregroundColor: UIColor.errorText])
                    cell.title = NSAttributedString(string: test.testName, attributes: [.foregroundColor: UIColor.gray])
                    cell.subTitle = NSAttributedString(string: test.testSkippedTitle(), attributes: [.foregroundColor: UIColor.gray])
                    cell.image = #imageLiteral(resourceName: "ic_resultSkip")
                    cells.append(cell)
                }
            }
            
            if !cells.isEmpty {
                if let clearedText = clearedText {
                    subtitle.append(clearedText)
                }
                
                if let failedText = failedText {
                    if !subtitle.string.isEmpty {
                        subtitle.append(NSAttributedString(string: " | ", attributes: [.foregroundColor: UIColor.gray]))
                    }
                    
                    subtitle.append(failedText)
                }
                
                if let skippedText = skippedText {
                    if !subtitle.string.isEmpty {
                        subtitle.append(NSAttributedString(string: " | ", attributes: [.foregroundColor: UIColor.gray]))
                    }
                    
                    subtitle.append(skippedText)
                }
                
                section.subtitle = subtitle
                section.cells = cells
                sections.append(section)
            }
        }
        return sections
    }
}
