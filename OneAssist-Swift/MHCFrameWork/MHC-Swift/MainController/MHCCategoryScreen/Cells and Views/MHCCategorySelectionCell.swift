//
//  MHCCategorySelectionCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 22/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit


protocol MHCCategorySelectionCellDelegate: class {
    func tapOnMoreCategoryListLable(_ indexPath: IndexPath?)
    func tapOnCategorySelectionCell(_ indexPath: IndexPath?)
}

class MHCCategorySelectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var subTitleLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var imageView: UIImageView!
    var indexPath: IndexPath?
    weak var delegate: MHCCategorySelectionCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabel(tap:)))
        subTitleLabel.addGestureRecognizer(tap)
        subTitleLabel.isUserInteractionEnabled = true
        // Initialization code
    }
    
    func setViewModel(_ model: MHCCategorySelectionCellModel, isMandatory: Bool, indexPath: IndexPath) {
        self.indexPath = indexPath
        
        titleLabel.text = model.title
        subTitleLabel.text = model.subTitle
        if let subTitle = subTitleLabel.text {
            if let range1 = subTitle.range(of: MoreTapableContent.twoMore.rawValue) {
                self.addAttributedSubTitle(NSRange(range1, in: subTitle), subTitle: subTitle)
                
            }else if let range1 = subTitle.range(of: MoreTapableContent.oneMore.rawValue) {
                self.addAttributedSubTitle(NSRange(range1, in: subTitle), subTitle: subTitle)
                
            }
        }
        
        if isMandatory {
            imageView.isHidden = true
        } else {
            imageView.isHidden = false
            imageView.image = model.isSelected ? #imageLiteral(resourceName: "checkBox_tick") : #imageLiteral(resourceName: "checkBox_normal")
        }
    }
    
    func addAttributedSubTitle(_ range: NSRange, subTitle: String){
        let underlineAttriString = NSMutableAttributedString(string: subTitle)
        underlineAttriString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        underlineAttriString.addAttribute(.foregroundColor, value: UIColor.buttonBlue, range: range)
        subTitleLabel.attributedText = underlineAttriString
    }
    
    @objc func tapLabel(tap: UITapGestureRecognizer) {
        if let subTitle = subTitleLabel.text {
            if ((subTitle.range(of: MoreTapableContent.twoMore.rawValue) != nil) ||
                (subTitle.range(of: MoreTapableContent.oneMore.rawValue) != nil)) &&
                delegate != nil {
                 self.delegate?.tapOnMoreCategoryListLable(self.indexPath)
                
            }else {
                self.delegate?.tapOnCategorySelectionCell(self.indexPath)
            }
        }else {
            self.delegate?.tapOnCategorySelectionCell(self.indexPath)
        }
        
    }
}
