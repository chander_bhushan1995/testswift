//
//  MHCCategoryBottomSheetCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 13/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCCategoryBottomSheetCell: UITableViewCell, NibLoadableView, ReuseIdentifier {

    @IBOutlet weak var titleLabel: H3RegularBlackLabel!
    
    func setViewModel(_ model: MhcTestName) {
        titleLabel.text = model.rawValue
    }
    
    func setLanguageModel(_ model: LanguageData) {
           titleLabel.text = model.language
       }
    
}
