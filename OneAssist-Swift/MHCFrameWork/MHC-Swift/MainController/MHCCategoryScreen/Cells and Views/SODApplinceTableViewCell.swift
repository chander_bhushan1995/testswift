//
//  SODApplinceTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class SODApplinceTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var titleLabel: BodyTextRegularBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupModel(model: HomeApplianceSubCategories) {
        titleLabel.text = model.subCategoryName
    }
    
}
