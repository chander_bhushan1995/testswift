//
//  MHCCategorySectionHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 29/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCCategorySectionHeaderView: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setViewModel(_ model: MHCCategorySectionHeaderModel) {
        titleLabel.text = model.selectionText
    }
    
    class func height(forModel model:MHCCategorySectionHeaderModel) -> CGFloat {
        return 32
    }
    
}
