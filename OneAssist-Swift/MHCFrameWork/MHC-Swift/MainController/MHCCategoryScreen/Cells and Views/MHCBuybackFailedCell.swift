//
//  MHCBuybackFailedCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 20/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCBuybackFailedCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setViewModel(_ model: MHCBuybackFailedCellModel) {
        titleLabel.text = model.title
    }

}
