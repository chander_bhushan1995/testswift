//
//  MHCCategoryPrimarySectionHeaderView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 30/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCCategoryPrimarySectionHeaderDelegate: class {
    func selectionChanged(select: Bool)
    func viewAllDetails()
}

class MHCCategoryPrimarySectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var resultView: UIView!
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var resultDateLabel: UILabel!
    @IBOutlet weak var resultStatusLabel: UILabel!
    @IBOutlet weak var resultViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerLabel: H1BoldLabel!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var tipLabelTopContraint: NSLayoutConstraint!
    @IBOutlet weak var tipLabelHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subtitleLabelTopContraint: NSLayoutConstraint!
    @IBOutlet weak var subtitleLabelHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    
    @IBOutlet weak var viewDetailsButton: OASecondaryButton!
    var isSelectAll = false
    
    weak var delegate: MHCCategoryPrimarySectionHeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setViewModel(_ model: MHCCategorySectionHeaderModel, delegate: MHCCategoryPrimarySectionHeaderDelegate, tag:Int) {
        self.tag = tag
        if let resultModel = model.lastResultModel {
            resultViewHeightConstraint.constant = 56
            resultDateLabel.text = resultModel.subtitle
            resultStatusLabel.text = resultModel.title
            resultImageView.image = resultModel.image
            resultView.isHidden = false
            if resultModel.isViewDetailsHidden {
                viewDetailsButton.setTitle(nil, for: .normal)
                viewDetailsButton.isHidden = true
            } else {
                viewDetailsButton.setTitle(MhcStrings.Common.viewDetails, for: .normal)
                viewDetailsButton.isHidden = false
            }
        } else {
            resultViewHeightConstraint.constant = 0
            resultView.isHidden = true
        }
        
        headerLabel.text = model.title
        
        if let tipText = model.tipTitle {
            tipLabelTopContraint.constant = 16
            tipLabelHeightContraint.constant = 32
            tipLabel.text = tipText
        } else {
            tipLabelTopContraint.constant = 0
            tipLabelHeightContraint.constant = 0
        }
        
        if let subtitle = model.subTitle {
            subtitleLabelTopContraint.constant = 16
            subtitleLabelHeightContraint.constant = 32
            subtitleLabel.text = subtitle
        } else {
            subtitleLabelTopContraint.constant = 0
            subtitleLabelHeightContraint.constant = 0
        }
        
        titleLabel.text = model.selectionText
        updateSelection(model.isSelectAll)
        selectButton.isHidden = !model.isButtonShown
        self.delegate = delegate
    }
    
    @IBAction func viewDetailsTapped(_ sender: Any) {
        delegate?.viewAllDetails()
    }
    
    @IBAction func selectButtonTapped(_ sender: Any) {
        delegate?.selectionChanged(select: isSelectAll)
    }
    
    func updateSelection(_ selection:Bool) {
        isSelectAll = selection
        selectButton.setTitle(isSelectAll ? MhcStrings.CategoryScreen.mhcSelectButtonTitle : MhcStrings.CategoryScreen.mhcDeselectButtonTitle, for: .normal)
    }
    
    class func height(forModel model:MHCCategorySectionHeaderModel) -> CGFloat {
        var height: CGFloat = 136;
        if model.lastResultModel != nil {
            height += 56
        }
        if model.tipTitle != nil {
            height += 48
        }
        
        if model.subTitle != nil {
            height += 48
        }
        
        return height
    }
}
