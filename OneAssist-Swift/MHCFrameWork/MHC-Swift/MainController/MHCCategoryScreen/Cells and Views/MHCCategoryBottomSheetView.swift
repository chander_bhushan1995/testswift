
//
//  MHCCategoryBottomSheetView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 13/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCCategoryBottomSheetViewDelegate: class {
    func dismiss()
    func didSelectApplince(applince: HomeApplianceSubCategories)
    func didSelectLanguage(language: LanguageData)
}

extension MHCCategoryBottomSheetViewDelegate {
    func dismiss() {}
    func didSelectApplince(applince: HomeApplianceSubCategories) {}
    func didSelectLanguage(language: LanguageData){}
}

class MHCCategoryBottomSheetView: UIView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var titleLabel: TagsBoldGreyLabel!
    @IBOutlet weak var crossImageView: UIImageView!
    @IBOutlet var bottomSheetView: UIView!
    var pendingTest: [MhcTestName] = []
    var otherCategory: OtherCategories?
    var languageData: [LanguageData]?
    weak var delegate: MHCCategoryBottomSheetViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("MHCCategoryBottomSheetView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        titleLabel.textColor = UIColor.errorText
        crossImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismiss)))
        tableViewObj.register(cell: MHCCategoryBottomSheetCell.self)
        tableViewObj.register(cell: SODApplinceTableViewCell.self)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
    }
    
    func setViewModel(_ model: MHCCategoryPendingCellModel, delegate: MHCCategoryBottomSheetViewDelegate, forLoadMoreSubcategory: Bool = false) {
        self.delegate = delegate
        self.pendingTest = model.pendingTests
        if forLoadMoreSubcategory {
            titleLabel.text = model.title?.uppercased()
            titleLabel.textColor = UIColor.bodyTextGray
        }else {
            titleLabel.text = "\(pendingTest.count) \(MhcStrings.CategoryScreen.testsRemaining)"
        }
        tableViewObj.reloadData()
    }
    
    func setApplinceViewModel(_ model: OtherCategories?, delegate: MHCCategoryBottomSheetViewDelegate) {
        self.delegate = delegate
        otherCategory = model
        titleLabel.text = model?.title?.uppercased()
        titleLabel.textColor = UIColor.bodyTextGray
        tableViewObj.reloadData()
    }
    
    func setLanguageDataModel(_ model: [LanguageData], delegate: MHCCategoryBottomSheetViewDelegate) {
        self.delegate = delegate
        languageData = model
        titleLabel.text = Strings.iOSFraudDetection.selectLanguage.uppercased()
        titleLabel.textColor = UIColor.bodyTextGray
        tableViewObj.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableViewObj.reloadData()
    }
    
    func setRelationDataModel(_ model: [LanguageData], delegate: MHCCategoryBottomSheetViewDelegate) {
        self.delegate = delegate
        languageData = model
        titleLabel.text = Strings.iOSFraudDetection.selectRelationship.uppercased()
        titleLabel.textColor = UIColor.bodyTextGray
        tableViewObj.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableViewObj.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sodApplince = otherCategory {
            return sodApplince.subCategories?.count ?? 0
        }
        if let language = languageData {
            return language.count
        }
        return pendingTest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let sodApplince = otherCategory {
            guard let obj = sodApplince.subCategories?[indexPath.row] else { return UITableViewCell() }
            let cell: SODApplinceTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setupModel(model: obj)
            return cell
        }else if let language = languageData {
            let cell: MHCCategoryBottomSheetCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setLanguageModel(language[indexPath.row])
            return cell
            
        }else {
            let cell: MHCCategoryBottomSheetCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(pendingTest[indexPath.row])
            return cell
        }
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let sodApplince = otherCategory {
            Utilities.topMostPresenterViewController.dismiss(animated: false)
            guard let obj = sodApplince.subCategories?[indexPath.row] else { return }
            delegate?.didSelectApplince(applince: obj)
        }else if let language = languageData {
            Utilities.topMostPresenterViewController.dismiss(animated: false)
            delegate?.didSelectLanguage(language: language[indexPath.row])
        }
    }
    
    @objc func dismiss() {
        delegate?.dismiss()
    }
    
    class func height(forModel model:MHCCategoryPendingCellModel) -> CGFloat {
        var bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        bottomSafeArea = bottomSafeArea + 72.0 + 12.0
        return CGFloat(model.pendingTests.count * 44) + bottomSafeArea
    }
    
    class func applinceHeight(forModel model:OtherCategories?) -> CGFloat {
        var bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        bottomSafeArea = bottomSafeArea + 72.0 + 12.0
        return CGFloat((model?.subCategories?.count ?? 0) * 44) + bottomSafeArea
    }
    
    class func languageHeight(forModel model:[LanguageData]?) -> CGFloat {
        var bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        bottomSafeArea = bottomSafeArea + 72.0 + 12.0
        return CGFloat((model?.count ?? 0) * 44) + bottomSafeArea
    }
}
