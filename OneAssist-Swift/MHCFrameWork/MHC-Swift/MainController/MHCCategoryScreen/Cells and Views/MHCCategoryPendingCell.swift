//
//  MHCCategoryPendingCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 29/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MHCCategoryPendingCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var subtitleLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var remainingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setViewModel(_ model: MHCCategoryPendingCellModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subTitle
        remainingLabel.text = model.remainingTest
    }
}
