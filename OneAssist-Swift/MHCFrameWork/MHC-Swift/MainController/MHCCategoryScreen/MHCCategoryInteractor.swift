//
//  MHCCategoryInteractor.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCCategoryPresentationLogic {
    func presentMHCData(response: MHCResult, isFromBuyback: Bool)
    func invalidate()
}

class MHCCategoryInteractor: BaseInteractor, MHCCategoryBusinessLogic {
    var presenter: MHCCategoryPresentationLogic?
    
    // MARK: Business Logic Conformance
    func invalidate() {
        presenter?.invalidate()
    }
    
    func initializeMHCData(isFromBuyback: Bool) {
        self.presenter?.presentMHCData(response: MHCResult.shared, isFromBuyback: isFromBuyback)
    }
}
