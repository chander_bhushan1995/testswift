//
//  MHCCategoryModel.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

struct MHCCategoryModel {
    var sections: [MHCCategorySectionModel] = []
    var bottomModel: MHCCategoryBottomModel?
}

struct MHCCategoryBottomModel {
    var primaryButtonText: String?
    var secondaryButtonText: String?
    var isDropout: Bool = false
}

struct MHCCategoryLastResultModel {
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var isViewDetailsHidden: Bool = false
}

struct MHCCategorySectionModel {
    var header: MHCCategorySectionHeaderModel?
    var cells: [Any] = []
}

struct MHCCategorySectionHeaderModel {
    var title: String?
    var subTitle: String?
    var tipTitle: String?
    var lastResultModel: MHCCategoryLastResultModel?
    var isPrimaryHeader: Bool = false
    var selectionText: String?
    var isSelectAll: Bool = false
    var isButtonShown: Bool = false
}

struct MHCBuybackRemainingCellModel {
    var title: String?
    var subTitle: String?
    var image: UIImage?
    var category: MhcTestCategory = .screen
    
    init(withCategory category: MhcTestCategory) {
        self.category = category
        self.title = category.name()
        self.subTitle = category.testDescription()
        self.image = category.categoryImage()
    }
}

struct MHCCategorySelectionCellModel {
    var title: String?
    var subTitle: String?
    var isSelected: Bool = true
    var category: MhcTestCategory = .screen
    
    init(withCategory category: MhcTestCategory) {
        self.category = category
        self.title = category.name()
        self.subTitle = category.testDescription()
    }
}

struct MHCCategoryPendingCellModel {
    var title: String?
    var subTitle: String?
    var remainingTest: String?
    var category: MhcTestCategory = .screen
    var pendingTests: [MhcTestName] = []
    
    init(withCategory category: MhcTestCategory, tests:[MhcTestName], pendingTests: [MhcTestName]) {
        self.category = category
        self.pendingTests = pendingTests
        self.title = category.name()
        if pendingTests.count == tests.count {
            self.subTitle = MhcStrings.CategoryScreen.notYetStarted
        } else {
            self.subTitle = "\(pendingTests.count) of \(tests.count) completed"
        }
        
        self.remainingTest = "\(pendingTests.count) remaining"
    }
}

struct MHCCategoryTestCellModel {
    var title: String?
    var test: MhcTestName = MhcTestName.multitouch
    
    init(withTest test: MhcTestName) {
        self.test = test
        self.title = test.testName
    }
}

struct MHCBuybackFailedCellModel {
    var title: String?
    var test: MhcTestName = MhcTestName.multitouch
    
    init(withTest test: MhcTestName) {
        self.test = test
        self.title = test.testName
    }
}
