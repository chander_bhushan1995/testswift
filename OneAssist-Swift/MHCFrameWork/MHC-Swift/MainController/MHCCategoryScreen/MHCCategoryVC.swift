//
//  MHCCategoryVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 22/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

let screensize = UIScreen.main.bounds.size
private let selectionHeaderTag = 1024

protocol MHCCategoryBusinessLogic {
    func initializeMHCData(isFromBuyback: Bool)
    func invalidate()
}

class MHCCategoryVC: BaseVC {
    
    @IBOutlet weak var collectionViewObj: UICollectionView!
    @IBOutlet weak var startTestButton: OAPrimaryButton!
    @IBOutlet weak var otherButton: OASecondaryButton!
    @IBOutlet weak var otherButtonLeadingContraint: NSLayoutConstraint!
    @IBOutlet weak var otherButtonWidthConstraint: NSLayoutConstraint!
    
    var interactor: MHCCategoryBusinessLogic?
    var mhcData: MHCCategoryModel = MHCCategoryModel()
    var isFromBuyback: Bool = false
    var buybackCompletionBlock: ((String?) -> Void)? = nil
    var navigator: MHCMainNavigator?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        EventTracking.shared.addScreenTracking(with: .startMHC)
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.initializeMHCData(isFromBuyback: isFromBuyback)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
    }
    
    func recievedErrorOnRefreshing() {
        Utilities.removeLoader(count: &loaderCount)
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        title = MhcStrings.CategoryScreen.screenTitle
        if isFromBuyback {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(dismissVC))
        }
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCCategorySelectionCell)
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCCategoryPendingCell)
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCCategoryTestCell)
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCBuybackFailedCell)
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCBuybackRemainingCell)
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCCategorySectionHeaderView, forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: Constants.CellIdentifiers.MHCCategorySectionHeaderView)
        collectionViewObj.register(nib: Constants.CellIdentifiers.MHCCategoryPrimarySectionHeaderView, forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: Constants.CellIdentifiers.MHCCategoryPrimarySectionHeaderView)

        collectionViewObj.backgroundColor = UIColor.clear
        view.backgroundColor = isFromBuyback ? UIColor.white : UIColor.whiteSmoke
        Permission.shared.geocode()
    }
    
    @IBAction func startTestTapped(_ sender: Any) {
        var categorizedtests = [[MhcTestName]]()
        var categories = [MhcTestCategory]()
        if isFromBuyback {
            let pendingCells = mhcData.sections[0].cells as? [MHCBuybackRemainingCellModel] ?? []
            categories = pendingCells.compactMap({$0.category})
            categorizedtests = pendingCells.compactMap({$0.category}).map({$0.buybackRemainingTests()})
            for tests in categorizedtests {
                for test in tests {
                    test.setTestStatus(.result_not_attempted)
                }
            }
        } else {
            if mhcData.bottomModel?.isDropout ?? false {
                let pendingCells = mhcData.sections[0].cells as? [MHCCategoryPendingCellModel] ?? []
                categories = pendingCells.compactMap({$0.category})
                categorizedtests = pendingCells.compactMap({$0.pendingTests})
            } else {
                let selectionCells = mhcData.sections[0].cells as? [MHCCategorySelectionCellModel] ?? []
                MHCResult.shared.clearData()
                for category in selectionCells.compactMap({!$0.isSelected ? $0.category : nil}) {
                    for test in category.tests() {
                        test.setTestStatus(.result_not_picked)
                    }
                }
                categories = selectionCells.compactMap({$0.isSelected ? $0.category : nil})
                categorizedtests = selectionCells.compactMap({$0.isSelected ? $0.category : nil}).map({$0.tests()})
            }
        }
        let group = categories.count == MhcTestCategory.values.count ? Event.EventParams.all : categories.map({$0.name()}).joined(separator: ",")
        EventTracking.shared.eventTracking(name: .startMHC, [.location: isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome, .group: group], withAppFlyer: true)

        navigator = MHCMainNavigator(categorizedtests: categorizedtests, isFromBuyback:isFromBuyback)
        navigator?.delegate = self
        navigator?.presentMainContainerVC(viewController: self)
        MHCConstants.mhcCompletionStatus = .pending
        MHCResult.save()
    }
    
    @IBAction func otherButtonTapped(_ sender: Any) {
        self.showAlert(title: MhcStrings.CategoryScreen.startNewTestAlertTitle, message: "", primaryButtonTitle: MhcStrings.CategoryScreen.startNewTestAlertPrimary, secondaryButtonTitle: MhcStrings.Buttons.cancel, isCrossEnable: true, primaryAction: {
            MHCConstants.reset()
            MHCResult.shared.clearData()
            MHCResult.save()
            self.interactor?.initializeMHCData(isFromBuyback: self.isFromBuyback)
        }) { }
    }
    
    @objc func dismissVC() {
        if isFromBuyback {
            self.dismiss(animated: true) {
                self.buybackCompletionBlock?(nil)
            }
        }
    }
}

// MARK:- Collection DataSource
extension MHCCategoryVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return mhcData.sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mhcData.sections[section].cells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let anyObject = mhcData.sections[indexPath.section].cells[indexPath.row]
        
        if let model = anyObject as? MHCCategorySelectionCellModel {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.MHCCategorySelectionCell, for: indexPath) as! MHCCategorySelectionCell
            cell.setViewModel(model, isMandatory: isFromBuyback, indexPath: indexPath)
            cell.delegate = self
            return cell
        } else if let model = anyObject as? MHCCategoryPendingCellModel {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.MHCCategoryPendingCell, for: indexPath) as! MHCCategoryPendingCell
            cell.setViewModel(model)
            return cell
        } else if let model = anyObject as? MHCCategoryTestCellModel {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.MHCCategoryTestCell, for: indexPath) as! MHCCategoryTestCell
            cell.setViewModel(model)
            return cell
        } else if let model = anyObject as? MHCBuybackFailedCellModel {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.MHCBuybackFailedCell, for: indexPath) as! MHCBuybackFailedCell
            cell.setViewModel(model)
            return cell
        } else if let model = anyObject as? MHCBuybackRemainingCellModel {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.MHCBuybackRemainingCell, for: indexPath) as! MHCBuybackRemainingCell
            cell.delegate = self
            cell.setViewModel(model, indexPath: indexPath)
            return cell
        }
        
        return UICollectionViewCell()
    }
}


// MARK:- Collection Delegate
extension MHCCategoryVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let anyObject = mhcData.sections[indexPath.section].cells[indexPath.row]
        let collectionCellWidth = screensize.width/2 - 8
        if anyObject is MHCCategorySelectionCellModel {
            return CGSize(width: collectionCellWidth, height: 126)
        } else if anyObject is MHCCategoryPendingCellModel {
            return CGSize(width: collectionCellWidth, height: 126)
        } else if anyObject is MHCCategoryTestCellModel {
            return CGSize(width: collectionCellWidth, height: 28)
        } else if anyObject is MHCBuybackFailedCellModel {
            return CGSize(width: collectionCellWidth, height: 28)
        } else if anyObject is MHCBuybackRemainingCellModel {
            return CGSize(width: collectionCellWidth, height: 78)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let headerData = mhcData.sections[section].header {
            if headerData.isPrimaryHeader {
                return CGSize(width: screensize.width, height: MHCCategoryPrimarySectionHeaderView.height(forModel: headerData))
            } else {
                return CGSize(width: screensize.width, height: MHCCategorySectionHeaderView.height(forModel: headerData))

            }
        }
        
        return CGSize()
    }
}



// MARK:- Collection Delegate
extension MHCCategoryVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let headerData = mhcData.sections[indexPath.section].header, kind == UICollectionView.elementKindSectionHeader {
            if headerData.isPrimaryHeader {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constants.CellIdentifiers.MHCCategoryPrimarySectionHeaderView, for: indexPath) as! MHCCategoryPrimarySectionHeaderView
                headerView.setViewModel(headerData, delegate: self, tag:selectionHeaderTag)
                return headerView
            } else {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constants.CellIdentifiers.MHCCategorySectionHeaderView, for: indexPath) as! MHCCategorySectionHeaderView
                headerView.setViewModel(headerData)
                return headerView
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let anyObject = mhcData.sections[indexPath.section].cells[indexPath.row]
        
        if var model = anyObject as? MHCCategorySelectionCellModel {
            model.isSelected = !model.isSelected
            mhcData.sections[indexPath.section].cells[indexPath.row] = model
            let selectionCells = mhcData.sections[indexPath.section].cells as? [MHCCategorySelectionCellModel] ?? []
            let selectedCategoriesCount = selectionCells.filter({$0.isSelected}).count
            if selectedCategoriesCount == 0 {
                startTestButton.isEnabled = false
                mhcData.sections[indexPath.section].header?.isSelectAll = true
            } else if selectedCategoriesCount == selectionCells.count {
                startTestButton.isEnabled = true
                mhcData.sections[indexPath.section].header?.isSelectAll = false
            } else if selectedCategoriesCount > 0 {
                startTestButton.isEnabled = true
                mhcData.sections[indexPath.section].header?.isSelectAll = true
            }
            
            if let headerView = collectionView.viewWithTag(selectionHeaderTag) as? MHCCategoryPrimarySectionHeaderView {
                headerView.updateSelection(mhcData.sections[indexPath.section].header?.isSelectAll ?? false)
            }

            collectionView.reloadItems(at: [indexPath])
        } else if let model = anyObject as? MHCCategoryPendingCellModel {
            let timePicker = MHCCategoryBottomSheetView()
            timePicker.setViewModel(model, delegate: self)
            timePicker.delegate = self
            Utilities.presentPopover(view: timePicker, height: MHCCategoryBottomSheetView.height(forModel: model))
        }
    }
}

extension MHCCategoryVC: MHCCategorySelectionCellDelegate, MHCBuybackRemainingCellDelegate {
    func tapOnMoreCategoryListLable(_ indexPath: IndexPath?) {
        if let selectedIndexPath = indexPath {
            let anyObject = mhcData.sections[selectedIndexPath.section].cells[selectedIndexPath.row]
            
            var tests: [MhcTestName] = []
            var category: MhcTestCategory = .screen
            
            if let model = anyObject as? MHCCategorySelectionCellModel {
                tests = model.category.tests()
                category = model.category
            }else if let model = anyObject as? MHCBuybackRemainingCellModel {
                tests = model.category.tests()
                category = model.category
            }
            
            if tests.count > 0 {
                let moreData = MHCCategoryPendingCellModel(withCategory: category, tests: tests, pendingTests: tests)
                let timePicker = MHCCategoryBottomSheetView()
                timePicker.setViewModel(moreData, delegate: self, forLoadMoreSubcategory: true)
                timePicker.delegate = self
                Utilities.presentPopover(view: timePicker, height: MHCCategoryBottomSheetView.height(forModel: moreData))
            }
        }
    }
    
    func tapOnCategorySelectionCell(_ indexPath: IndexPath?) {
        if let selectedIndexPath = indexPath {
            collectionView(self.collectionViewObj, didSelectItemAt: selectedIndexPath)
        }
    }
}

extension MHCCategoryVC: MHCTestDelegate {
    func testFinished() {
        navigator = nil
        EventTracking.shared.eventTracking(name: .MHCComplete, [.location: isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome, .result: MHCConstants.mhcScoreValue], withAppFlyer: true)
        let vc = MHCResultMainVC()
        vc.delegate = self
        vc.isFromBuyBack = isFromBuyback
        presentInFullScreen(vc, animated: false)
    }
}

extension MHCCategoryVC: MHCTestResultDelegate {
    
    func result() {
        if isFromBuyback {
            self.dismiss(animated: false) {
                self.buybackCompletionBlock?(BuybackQuestionManager.shared.dataString())
            }
        } else {
            routeToMHCResultScreen(animated: false)
        }
    }
    
    func completePendingTest() {
        var categorizedtests = [[MhcTestName]]()
        var categories = [MhcTestCategory]()
        for category in MhcTestCategory.values {
            let tests = category.tests().filter({$0.getStatus().testResult == HealthCheckStatus.result_not_picked.rawValue})
            if !tests.isEmpty {
                categories.append(category)
                categorizedtests.append(tests)
            }
        }
        let group = categories.count == MhcTestCategory.values.count ? Event.EventParams.all : categories.map({$0.name()}).joined(separator: ",")
        EventTracking.shared.eventTracking(name: .startMHC, [.location: isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome, .group: group], withAppFlyer: true)
        navigator = MHCMainNavigator(categorizedtests: categorizedtests, isFromBuyback:isFromBuyback)
        navigator?.delegate = self
        navigator?.presentMainContainerVC(viewController: self)
        MHCConstants.mhcCompletionStatus = .pending
        MHCResult.save()
    }
    
    func retryTests() {
        let categories = BuybackQuestionManager.shared.pendingCategories()
        let categorizedtests = categories.map({$0.buybackRemainingTests()})
        for tests in categorizedtests {
            for test in tests {
                test.setTestStatus(.result_not_attempted)
            }
        }
        
        EventTracking.shared.eventTracking(name: .MHCRetry, [.location: isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome, .screenName: Event.EventParams.resultScreen])
        navigator = MHCMainNavigator(categorizedtests: categorizedtests, isFromBuyback:isFromBuyback)
        navigator?.delegate = self
        navigator?.presentMainContainerVC(viewController: self)
        MHCConstants.mhcCompletionStatus = .pending
        MHCResult.save()
    }
}

extension MHCCategoryVC: MHCCategoryPrimarySectionHeaderDelegate {
    func selectionChanged(select: Bool) {
        var sections = mhcData.sections
        for sectionIndex in 0..<sections.count {
            var section = sections[sectionIndex]
            for cellIndex in 0..<section.cells.count {
                if var cell = sections[sectionIndex].cells[cellIndex] as? MHCCategorySelectionCellModel {
                    cell.isSelected = select
                    section.cells[cellIndex] = cell
                }
            }
            section.header?.isSelectAll = !select
            sections[sectionIndex] = section
        }
        
        startTestButton.isEnabled = select
        mhcData.sections = sections
        collectionViewObj.reloadData()
    }
    
    func viewAllDetails() {
        routeToMHCResultScreen(animated: true)
    }
}

extension MHCCategoryVC: MHCCategoryBottomSheetViewDelegate {
    
    func dismiss() {
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
}

// MARK:- Display Logic Conformance
extension MHCCategoryVC: MHCCategoryDisplayLogic {
    
    func displayMHCData(_ data: MHCCategoryModel) {
        self.mhcData = data
        collectionViewObj.dataSource = self
        collectionViewObj.delegate = self
        collectionViewObj.reloadData()
        
        if let bottomModel = self.mhcData.bottomModel {
            if bottomModel.isDropout {
                startTestButton.setTitle(bottomModel.primaryButtonText, for: .normal)
                otherButton.setTitle(bottomModel.secondaryButtonText, for: .normal)
                otherButtonLeadingContraint.constant = 12
                otherButtonWidthConstraint.constant = (screensize.width - 12*3)/2
                otherButton.isHidden = false
                otherButton.isEnabled = true
            } else {
                startTestButton.setTitle(bottomModel.primaryButtonText, for: .normal)
                otherButton.setTitle(bottomModel.secondaryButtonText, for: .normal)
                otherButtonLeadingContraint.constant = 0
                otherButtonWidthConstraint.constant = 0
                otherButton.isHidden = true
                otherButton.isEnabled = false
            }
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
}

// MARK:- Configuration Logic
extension MHCCategoryVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = MHCCategoryInteractor()
        let presenter = MHCCategoryPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension MHCCategoryVC {
    func routeToMHCResultScreen(animated: Bool) {
        EventTracking.shared.eventTracking(name: .MHCSummary, [.location: isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome])
        let mhcVC : MHCResultVC = MHCResultVC(nibName: "MHCResultVC", bundle: nil)
        navigationController?.pushViewController(mhcVC, animated: animated)
    }
}
