//
//  MHCCateogryPresenter.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MHCCategoryDisplayLogic: class {
    func displayMHCData(_ plans: MHCCategoryModel)
    func displayError(_ error: String)
}

class MHCCategoryPresenter: BasePresenter, MHCCategoryPresentationLogic {
    
    weak var viewController: MHCCategoryDisplayLogic?
    var mhcData: MHCCategoryModel = MHCCategoryModel()
    
    // MARK: Presentation Logic Conformance
    
    func presentMHCData(response: MHCResult, isFromBuyback: Bool) {
        var model = MHCCategoryModel()
        
        if isFromBuyback {
            if BuybackQuestionManager.shared.doneTest().isEmpty {
                // Buyback MHC start screen
                var categorySectionModel = MHCCategorySectionModel()
                var categorySectionHeaderModel = MHCCategorySectionHeaderModel()
                categorySectionHeaderModel.title = MhcStrings.CategoryScreen.buybackInitialTitle
                categorySectionHeaderModel.subTitle = MhcStrings.CategoryScreen.buybackInitialSubtitle
                categorySectionHeaderModel.isPrimaryHeader = true
                categorySectionHeaderModel.isButtonShown = false
                categorySectionModel.cells = BuybackQuestionManager.shared.pendingCategories().map({MHCBuybackRemainingCellModel(withCategory: $0)})
                if BuybackQuestionManager.shared.isAllFailedOrSkipped() {
                    var lastResultModel = MHCCategoryLastResultModel()
                    lastResultModel.subtitle = MhcStrings.CategoryScreen.buybackLastResultSubtitle
                    lastResultModel.title = MhcStrings.CategoryScreen.buybackLastResultTitlePoor
                    lastResultModel.image = #imageLiteral(resourceName: "MHC_poor_success")
                    lastResultModel.isViewDetailsHidden = true
                    categorySectionHeaderModel.lastResultModel = lastResultModel
                }
                categorySectionModel.header = categorySectionHeaderModel
                model.sections = [categorySectionModel]
                var bottomModel = MHCCategoryBottomModel()
                bottomModel.primaryButtonText = MhcStrings.Buttons.startTest
                model.bottomModel = bottomModel
            } else {
                // Buyback MHC dropout screen
                var categorySectionModel = MHCCategorySectionModel()
                var categorySectionHeaderModel = MHCCategorySectionHeaderModel()
                categorySectionHeaderModel.title = MhcStrings.CategoryScreen.buybackPendingTitle
                categorySectionHeaderModel.subTitle = MhcStrings.CategoryScreen.buybackInitialSubtitle
                categorySectionHeaderModel.selectionText = MhcStrings.CategoryScreen.remainingTests
                categorySectionHeaderModel.isPrimaryHeader = true
                categorySectionHeaderModel.isButtonShown = false
                categorySectionModel.cells = BuybackQuestionManager.shared.pendingCategories().map({MHCBuybackRemainingCellModel(withCategory: $0)})
                if BuybackQuestionManager.shared.isAnyPassed() {
                    var lastResultModel = MHCCategoryLastResultModel()
                    lastResultModel.subtitle = MhcStrings.CategoryScreen.buybackLastResultSubtitle
                    lastResultModel.title = MhcStrings.CategoryScreen.buybackLastResultTitleExcellent
                    lastResultModel.image = #imageLiteral(resourceName: "MHC_excellent_success")
                    lastResultModel.isViewDetailsHidden = true
                    categorySectionHeaderModel.lastResultModel = lastResultModel
                } else if BuybackQuestionManager.shared.isAllFailedOrSkipped() {
                    var lastResultModel = MHCCategoryLastResultModel()
                    lastResultModel.subtitle = MhcStrings.CategoryScreen.buybackLastResultSubtitle
                    lastResultModel.title = MhcStrings.CategoryScreen.buybackLastResultTitlePoor
                    lastResultModel.image = #imageLiteral(resourceName: "MHC_poor_success")
                    lastResultModel.isViewDetailsHidden = true
                    categorySectionHeaderModel.lastResultModel = lastResultModel
                }
                categorySectionModel.header = categorySectionHeaderModel
                model.sections = [categorySectionModel]
                
                let failedTests = BuybackQuestionManager.shared.failedTests()
                if !failedTests.isEmpty {
                    var testSectionModel = MHCCategorySectionModel()
                    var testSectionHeaderModel = MHCCategorySectionHeaderModel()
                    testSectionHeaderModel.selectionText =  MhcStrings.CategoryScreen.testFailed
                    testSectionHeaderModel.isButtonShown = false
                    testSectionHeaderModel.isPrimaryHeader = false
                    testSectionModel.header = testSectionHeaderModel
                    
                    testSectionModel.cells = failedTests.map({MHCBuybackFailedCellModel(withTest: $0)})
                    model.sections.append(testSectionModel)
                }
                
                var bottomModel = MHCCategoryBottomModel()
                bottomModel.primaryButtonText = BuybackQuestionManager.shared.isRetry() ? MhcStrings.Buttons.retryTests : MhcStrings.Buttons.completeTests
                model.bottomModel = bottomModel
            }
        } else {
            let mhcCompletionStatus = MHCConstants.mhcCompletionStatus
            if mhcCompletionStatus == .completed || mhcCompletionStatus == .skipped || MhcTestName.filtered(tests: MhcTestName.values, with: [.result_not_attempted], includingStatus: true).isEmpty {
                var categorySectionModel = MHCCategorySectionModel()
                var categorySectionHeaderModel = MHCCategorySectionHeaderModel()
                categorySectionHeaderModel.title = MhcStrings.CategoryScreen.mhcCompletedTitle
                categorySectionHeaderModel.tipTitle = MhcStrings.CategoryScreen.mhcCompletedTipTitle
                categorySectionHeaderModel.selectionText = MhcStrings.CategoryScreen.mhcSelectionTitle
                categorySectionHeaderModel.isPrimaryHeader = true
                categorySectionHeaderModel.isButtonShown = RemoteConfigManager.shared.showDeselectAllMHCCategoryIcon
                if let mhcUpdateDate = MHCConstants.mhcUpdateDate {
                    var lastResultModel = MHCCategoryLastResultModel()
                    lastResultModel.subtitle = "\(MhcStrings.CategoryScreen.lastCheckedOn) \(mhcUpdateDate.ordinalDate())"
                    let mhcScore = MHCScore(rawValue: MHCConstants.mhcScore)
                    switch mhcScore {
                    case .excellent:
                        lastResultModel.title = MhcStrings.CategoryScreen.mhcLastResultTitleExcellent
                        lastResultModel.image = mhcScore.mainImage()
                    case .good:
                        lastResultModel.title = MhcStrings.CategoryScreen.mhcLastResultTitleGood
                        lastResultModel.image = mhcScore.mainImage()
                    case .average:
                        lastResultModel.title = MhcStrings.CategoryScreen.mhcLastResultTitleAverage
                        lastResultModel.image = mhcScore.mainImage()
                    case .poor:
                        lastResultModel.title = MhcStrings.CategoryScreen.mhcLastResultTitlePoor
                        lastResultModel.image = mhcScore.mainImage()
                    }
                    categorySectionHeaderModel.lastResultModel = lastResultModel
                }
                categorySectionModel.header = categorySectionHeaderModel
                categorySectionModel.cells = MhcTestCategory.values.map({MHCCategorySelectionCellModel(withCategory: $0)})
                model.sections = [categorySectionModel]
                
                var bottomModel = MHCCategoryBottomModel()
                bottomModel.primaryButtonText = MhcStrings.Buttons.startTest
                model.bottomModel = bottomModel
            } else if mhcCompletionStatus == .cancelled || mhcCompletionStatus == .pending {
                var categorySectionModel = MHCCategorySectionModel()
                var categorySectionHeaderModel = MHCCategorySectionHeaderModel()
                categorySectionHeaderModel.title = MhcStrings.CategoryScreen.mhcPendingTitle
                categorySectionHeaderModel.selectionText = MhcStrings.CategoryScreen.remainingTests
                categorySectionHeaderModel.isButtonShown = false
                categorySectionHeaderModel.isPrimaryHeader = true
                categorySectionModel.header = categorySectionHeaderModel
                
                var cellsData: [MHCCategoryPendingCellModel] = []
                for category in MhcTestCategory.values {
                    let tests = category.tests()
                    let remainingTest = category.remainingTest()
                    if remainingTest.count != 0 {
                        cellsData.append(MHCCategoryPendingCellModel(withCategory: category, tests: tests, pendingTests: remainingTest))
                    }
                }
                categorySectionModel.cells = cellsData
                model.sections = [categorySectionModel]
                
                let completedTests = MhcTestCategory.values.map({$0.completedTests()})
                var allCompletedTests: [MhcTestName] = []
                for completedTestList in completedTests {
                    allCompletedTests.append(contentsOf: completedTestList)
                }
                if !allCompletedTests.isEmpty {
                    var testSectionModel = MHCCategorySectionModel()
                    var testSectionHeaderModel = MHCCategorySectionHeaderModel()
                    testSectionHeaderModel.selectionText =  "\(allCompletedTests.count) \(MhcStrings.CategoryScreen.testCompleted)"
                    testSectionHeaderModel.isButtonShown = false
                    testSectionHeaderModel.isPrimaryHeader = false
                    testSectionModel.header = testSectionHeaderModel
                    
                    testSectionModel.cells = allCompletedTests.map({MHCCategoryTestCellModel(withTest: $0)})
                    model.sections.append(testSectionModel)
                }
                
                var bottomModel = MHCCategoryBottomModel()
                bottomModel.primaryButtonText = MhcStrings.Buttons.resumeTest
                bottomModel.secondaryButtonText = MhcStrings.Buttons.startNewTest
                bottomModel.isDropout = true
                model.bottomModel = bottomModel
            }
        }
        
        viewController?.displayMHCData(model)
    }
    
    func invalidate() {
        mhcData = MHCCategoryModel()
    }
}
