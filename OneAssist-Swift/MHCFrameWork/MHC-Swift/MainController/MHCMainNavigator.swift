//
//  MHCMainNavigator.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 20/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

public protocol MHCTestDelegate: class {
    func testFinished()
}

public class MHCMainNavigator {
    
    fileprivate var testList = [[MhcTestName]]()
    fileprivate var categoryIndex = 0
    fileprivate var mainVC = MHCMainVC()
    public weak var delegate: MHCTestDelegate?
    fileprivate var startingTime: Date?
    fileprivate var isFromBuyback: Bool
    
    init(categorizedtests: [[MhcTestName]], isFromBuyback: Bool = false) {
        self.testList = categorizedtests
        self.isFromBuyback = isFromBuyback
    }
    
    public func presentMainContainerVC(viewController: UIViewController) {
        startingTime = Date()
        if !testList.isEmpty {
            mainVC.mainNavigator = self
            mainVC.isFromBuyback = isFromBuyback
            mainVC.totalTestCount = testList.count
            viewController.presentInFullScreen(mainVC, animated: true, completion: nil)
            startNextTest()
        }
        
    }
    
    func startNextTest() {
        if let test = testList[categoryIndex].first {
            let vc = test.getTestController()
            vc.testName = test.testName
            mainVC.currentIndex = categoryIndex
            mainVC.setPageVC(testVC: vc, testType: test)
        } else {
            // should not execute
        }
    }
}

extension MHCMainNavigator: TestResultDelegate {
    
    func testFinished() {
        let currentTest = testList[categoryIndex].first?.relatedTests() ?? []
        testList[categoryIndex].removeAll(where: {currentTest.contains($0)})
        
        if !testList[categoryIndex].isEmpty {
            startNextTest()
            MHCResult.save()
        } else if categoryIndex < testList.count - 1 {
            categoryIndex += 1
            startNextTest()
            MHCResult.save()
        } else {
            let totalTestTime = self.startingTime?.timeIntervalTillNowInMilliSeconds() ?? 0
            MHCConstants.mhcCompletionStatus = MhcTestCategory.values.contains(where: {$0.isAnySkipped()}) ? .skipped :.completed
            MHCConstants.mhcUpdateDate = Date()
            MHCConstants.mhcScore = isFromBuyback ? MhcTestName.testResult(BuybackQuestionManager.shared.mhcTestList, isFromBuyback: true) : MhcTestName.testResult()
            MHCResult.shared.Basic = BasicDetails(totalTestTime: totalTestTime)
            MHCResult.save()
            MHCConstants.isDataUpdateOnServerPending = true
            Utilities.updateMHCDataOnServer()
            mainVC.dismiss(animated: false) {
                self.delegate?.testFinished()
            }
        }
    }
}
