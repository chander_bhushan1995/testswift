//
//  MHCMainVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 16/01/19.
//  Copyright (c) 2019 com.chanchal. All rights reserved.
//

import UIKit

protocol MHCMainBusinessLogic {
    func initialize(selectedCategories: [MhcTestCategory]?)
    func startMhcTest()
}

class MHCMainVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var subtitleLabel: TagsRegularGreyLabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dropdownImageView: UIImageView!
    var isFromBuyback: Bool = false

    var selectedCategories: [MhcTestCategory] = []
    var pageController: UIPageViewController!
    var currentIndex = 0
    var totalTestCount = 0
    var testType: MhcTestName!
    
    weak var mainNavigator: MHCMainNavigator?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "MHCMainVC", bundle: Bundle(for: type(of: self)))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK:- View lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        Permission.shared.prefetchLocation()
    }
    
    // MARK:- private methods
    private func initializeView() {
        headerView.layer.shadowColor = UIColor.dlsShadow.cgColor
        headerView.layer.shadowOpacity = 1
        headerView.layer.shadowOffset = CGSize(width: 2, height: 2)
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 2)
        progressView.layer.cornerRadius = 2
        titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showStatus)))
        subtitleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showStatus)))
        dropdownImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showStatus)))
        pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        addChild(pageController)
        containerView.addSubview(pageController.view)
        containerView.addSubviewWithConstraints(pageController.view) // can be called twice
        pageController.didMove(toParent: self)
    }
    
    func setPageVC(testVC: TestStartVC, testType: MhcTestName) {
        self.testType = testType
        testVC.testDelegate = mainNavigator
        let category = testType.category()
        titleLabel.text = "\(category.name()) (\(currentIndex+1) of \(totalTestCount))"
        subtitleLabel.text = category.testDescription()
        progressView.progress = Float(currentIndex+1)/Float(totalTestCount)
        pageController.setViewControllers([testVC], direction: .forward, animated: true, completion: nil)
    }

    // MARK:- Action Methods
    
    @IBAction func clickedBtnCancel(_ sender: Any) {
        self.showAlert(title: MhcStrings.AlertMessage.cancelTestTitle, message: MhcStrings.AlertMessage.cancelTest, primaryButtonTitle: MhcStrings.Buttons.cancelTest, MhcStrings.Buttons.dismiss, isCrossEnable: true, primaryAction: {
            self.cancelMHC()
        }, nil)
    }
    
    private func cancelMHC() {
        EventTracking.shared.eventTracking(name: .cancelMHC, [.location: isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome, .screenName: self.testType.category().name()])
        
        if isFromBuyback {
            MHCConstants.mhcCompletionStatus = .cancelled
        } else {
            if !MhcTestCategory.values.contains(where: {!$0.isAllNotAttempted()}) {
                MHCConstants.mhcCompletionStatus = .completed
                for test in MhcTestName.values {
                    test.setTestStatus(.result_not_picked)
                }
            } else {
                MHCConstants.mhcCompletionStatus = .cancelled
            }
        }
        MHCConstants.isDataUpdateOnServerPending = true
        Utilities.updateMHCDataOnServer()
        MHCResult.save()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showStatus() {
        presentInFullScreen(MHCTestStatusVC(), animated: true)
    }

}

