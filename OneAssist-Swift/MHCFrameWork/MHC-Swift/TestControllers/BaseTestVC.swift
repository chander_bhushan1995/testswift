//
//  BaseTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 23/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

protocol TestResultDelegate: class {
    func testFinished()
}

class BaseTestVC: BaseVC {

    weak var testDelegate: TestResultDelegate?

    var timer: Timer?
    var totalTime = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func delay(_ seconds: Double,completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }

    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(1.0), target: self, selector: #selector(timerStarted), userInfo: nil, repeats: true)
    }
    
    @objc func timerStarted() {
        
        totalTime -= 1
        if totalTime <= 0  {
            timer?.invalidate()
        }
    }
    
    func addObserverActiveApp() {
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func removeObserverActiveApp() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func appBecomeActive() {
        removeObserverActiveApp()
    }
    
}
