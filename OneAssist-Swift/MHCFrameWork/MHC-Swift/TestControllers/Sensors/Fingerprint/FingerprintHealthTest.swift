//
//  FingerprintHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 29/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import LocalAuthentication

enum BiometricType {
    case none
    case touchId
    case faceId
}

class FingerprintHealthTest: BaseHealthTest {
    
    override func startHealthTest() {
        super.startHealthTest()
        authenticationWithTouchID()
    }
    
    static func biometricType() -> BiometricType {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .none:
                return .none
            case .touchID:
                return .touchId
            case .faceID:
                return .faceId
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touchId : .none
        }
    }
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = ""
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                DispatchQueue.main.async {
                    if success {
                        print("success")
                        self.testSuccess()
                    } else {
                        
                        guard let error = evaluateError else {
                            self.testFailed(status: .result_fail)
                            self.healthTestDelegate?.testFinished()
                            self.healthTestDelegate = nil
                            return
                        }
                        self.evaluateAuthenticationPolicyMessageForLA(error: error as NSError)
                    }
                }
            }
        } else {
            guard let error = authError else {
                testFailed(status: .result_fail)
                healthTestDelegate?.testFinished()
                healthTestDelegate = nil
                return
            }
            evaluateAuthenticationPolicyMessageForLA(error: error as NSError)
        }
    }
    
    func evaluatePolicyFailErrorMessageForLA(error: NSError) {
        switch error.code {
        case LAError.biometryNotEnrolled.rawValue,
             LAError.biometryLockout.rawValue,
             LAError.biometryNotAvailable.rawValue:
            testFailed(status: .result_fail)
            healthTestDelegate?.testFailed(error: error)
        default:
            testFailed(status: .result_fail)
            healthTestDelegate?.testFinished()
            healthTestDelegate = nil
        }
    }
    
    func evaluateAuthenticationPolicyMessageForLA(error: NSError) {
        
        switch error.code {
        case LAError.authenticationFailed.rawValue,
             LAError.systemCancel.rawValue,
             LAError.userFallback.rawValue:
            testFailed(status: .result_fail)
            healthTestDelegate?.testFailed(error: error)
        case LAError.appCancel.rawValue,
             LAError.invalidContext.rawValue,
             LAError.userCancel.rawValue:
            if error.code == LAError.userCancel.rawValue {
                testFailed(status: .result_skipped)
            } else {
                testFailed(status: .result_fail)
            }
            healthTestDelegate?.testFinished()
            healthTestDelegate = nil
        case LAError.passcodeNotSet.rawValue:
            testFailed(status: .result_not_available)
            healthTestDelegate?.testFailed(error: error)
        default:
            evaluatePolicyFailErrorMessageForLA(error: error)
        }
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    func testFailed(status: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testResult = status.rawValue
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testTime = getTimeConsumedInTest()
    }
    
    func testSkipped() {
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFinished()
        healthTestDelegate = nil
    }
}
