//
//  FingerprintTestStartVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 29/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit
import LocalAuthentication

class FingerprintTestStartVC: TestStartVC {

    var bioMetricAvailable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView() {
        labelTestName.text = MhcStrings.FingerprintSensor.startTitle
        labelTestDescription.text = MhcStrings.FingerprintSensor.startDescription
        switch FingerprintHealthTest.biometricType() {
        case .none:
            bioMetricAvailable = false
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                FingerprintHealthTest().testFailed(status: .result_not_available)
                self.testDelegate?.testFinished()
            }
        case .touchId:
            imageViewTest.image = #imageLiteral(resourceName: "ic_fingerprint")
            labelTestName.text = MhcStrings.FingerprintSensor.startTitle
            labelTestDescription.text = MhcStrings.FingerprintSensor.startDescription
        case .faceId:
            imageViewTest.image = #imageLiteral(resourceName: "ic_faceID")
            labelTestName.text = MhcStrings.FaceIdSensors.startTitle
            labelTestDescription.text = MhcStrings.FaceIdSensors.startDescription
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !bioMetricAvailable {
            
        }
    }
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        let vc = FingerprintTestVC()
        vc.testDelegate = testDelegate
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_FINGERPRINT.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }

}
