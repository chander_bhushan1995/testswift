//
//  FingerprintTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 29/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit
import LocalAuthentication

class FingerprintTestVC: CommonTestVC {
    
    private var fingerprintTest = FingerprintHealthTest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fingerprintTest.startHealthTest()
    }
    
    func initializeView() {
        if FingerprintHealthTest.biometricType() == .faceId {
            imageView.image = #imageLiteral(resourceName: "ic_faceID")
            labelInstruction.text = MhcStrings.FaceIdSensors.instruction
            testName = MhcStrings.FaceIdSensors.startTitle
        } else {
            imageView.image = #imageLiteral(resourceName: "ic_fingerScan")
            labelInstruction.text = MhcStrings.FingerprintSensor.instruction
            testName = MhcStrings.FingerprintSensor.startTitle
        }
        labelTitle.text = ""
        labelRemainingTime.isHidden = true
        labelDescription.text = ""
        totalTime = 10
        fingerprintTest.healthTestDelegate = self
    }
    
    @objc override func testFailed(error: Error?) {
        let code = error?._code ?? 0
        switch code {
        case LAError.authenticationFailed.rawValue,
             LAError.systemCancel.rawValue,
             LAError.userFallback.rawValue:
            openRetryScreen()
        case LAError.passcodeNotSet.rawValue:
            showSettingAlert()
        default: break
        }
        if LAError.biometryNotEnrolled.rawValue == code || LAError.biometryLockout.rawValue == code {
            showSettingAlert()
        } else if LAError.biometryNotAvailable.rawValue == code {
            showAlert(title: MhcStrings.AlertMessage.faceIdPermissionTitle, message: MhcStrings.AlertMessage.faceIdPermissionSetting, primaryButtonTitle: MhcStrings.Buttons.settings, MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                self.addObserverActiveApp()
                Utilities.openAppSettings()
            }) {
                self.fingerprintTest.testSkipped()
            }
        }
    }
    
    override func tappedOnRetry() {
        super.tappedOnRetry()
        initializeView()
    }
    
    private func showSettingAlert() {
        
        if FingerprintHealthTest.biometricType() == .touchId {
            showAlert(title: MhcStrings.AlertMessage.touchIdTitle, message: MhcStrings.AlertMessage.touchIdSetting, primaryButtonTitle: MhcStrings.Buttons.settings, MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                self.addObserverActiveApp()
                Utilities.openAppSettings()
            }) {
                self.fingerprintTest.testSkipped()
            }
        } else {
            
            showAlert(title: MhcStrings.AlertMessage.faceIdTitle, message: MhcStrings.AlertMessage.faceIdSetting, primaryButtonTitle: MhcStrings.Buttons.settings, MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                self.addObserverActiveApp()
                Utilities.openAppSettings()
            }) {
                self.fingerprintTest.testSkipped()
            }
        }
    }
    
    override func appBecomeActive() {
        super.appBecomeActive()
        self.fingerprintTest.startHealthTest()
    }
}
