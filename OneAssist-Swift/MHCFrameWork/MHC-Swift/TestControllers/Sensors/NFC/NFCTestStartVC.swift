//
//  NFCTestStartVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 02/04/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class NFCTestStartVC: TestStartVC {

    var spinnerView: SpinnerView?
    private var nfcHealthTest = NFCHealthTest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_nfc")
        labelTestName.text = MhcStrings.NFC.startTitle
        labelTestDescription.text = MhcStrings.NFC.startDescription
        nfcHealthTest.healthTestDelegate = self
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        
        nfcHealthTest.startHealthTest()
        self.viewTestStatus.isHidden = false
//        self.spinnerView = SpinnerView.init(view: self.viewTestStatus)
//        self.spinnerView?.startAnimating()
    }

    override func testTimerStarted() {
        super.testTimerStarted()
        if totalTime <= 0  {
            nfcHealthTest.testFailed(.result_fail)
        }
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_NFC.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
}

extension NFCTestStartVC: HealthTestDelegate {
    
    func testFinished() {
        testTimer?.invalidate()
        self.testDelegate?.testFinished()
    }
    
    func testPass() {
        spinnerView?.stopAnimating()
        testTimer?.invalidate()
        viewTestStatus.isHidden = false
        viewSuccess.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.testFinished()
        }
    }
}
