//
//  NFCHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 02/04/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
#if canImport(CoreNFC)
import CoreNFC
#endif

#if canImport(CoreNFC)
private var session:NFCNDEFReaderSession? = nil
#endif

class NFCHealthTest: BaseHealthTest {
    
    override func startHealthTest() {
        super.startHealthTest()
        #if canImport(CoreNFC)
        if NFCNDEFReaderSession.readingAvailable {
            testSuccess()
            //                session = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
            //                session?.alertMessage = "Hold your iPhone near the item to learn more about it."
            //                session?.begin()
        } else {
            testFailed(.result_not_available)
        }
        #endif
    }
    
    fileprivate func testSuccess() {
        MHCResult.shared.deviceDetails.Q_NFC.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_NFC.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    func testFailed(_ status: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_NFC.testResult = status.rawValue
        MHCResult.shared.deviceDetails.Q_NFC.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFinished()
        healthTestDelegate = nil
    }
}

#if canImport(CoreNFC)
extension NFCHealthTest: NFCNDEFReaderSessionDelegate {
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("NFC NDEF Invalidated")
        testFailed(.result_fail)
        print("\(error)")
        
        if let readerError = error as? NFCReaderError {
            // Show an alert when the invalidation reason is not because of a success read
            // during a single tag read mode, or user canceled a multi-tag read mode session
            // from the UI or programmatically using the invalidate method call.
            if (readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead)
                && (readerError.code != .readerSessionInvalidationErrorUserCanceled) {
                print("NFC NDEF Invalidated")
            }
        }
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        testSuccess()
        for message in messages {
            for record in message.records {
                print("New Record Found:")
                print(record.identifier)
                print(record.payload)
                print(record.type)
                print(record.typeNameFormat)
            }
        }
    }
}
#endif
