//
//  GyroscopeTestStartVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 19/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class GyroscopeTestStartVC: TestStartVC {

    var imageViewBall: UIImageView = UIImageView()
    var imgBallFrame = CGRect.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_gyroscope")
        labelTestName.text = MhcStrings.Gyroscope.startTitle
        labelTestDescription.text = MhcStrings.Gyroscope.startDescription
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addAnimationView()
    }
    
    private func addAnimationView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_mobile")
        outerView.setNeedsLayout()
        outerView.layoutIfNeeded()
        let imageViewBase = UIImageView.init(frame: CGRect(x: imageViewTest.frame.minX, y: imageViewTest.frame.maxY-10, width: imageViewTest.frame.width-35, height: 20))
        imageViewBase.image = #imageLiteral(resourceName: "ic_gyro_base")
        imageViewBase.contentMode = .scaleAspectFit
        outerView.addSubview(imageViewBase)
        imageViewBase.center.x = outerView.center.x
        
        imageViewBall = UIImageView.init(frame: CGRect(x: outerView.center.x-9, y: imageViewTest.frame.maxY-35, width: 18, height: 18))
        imageViewBall.image = #imageLiteral(resourceName: "ic_football")
        imageViewBall.contentMode = .scaleAspectFit
        outerView.addSubview(imageViewBall)
        imgBallFrame = imageViewBall.frame
        startAnimation()
    }
    
    private func startAnimation() {
        
        UIView.animate(withDuration: 2.0, delay: 0.0, options: [], animations: { [weak self] in
            self?.imageViewTest.transform = CGAffineTransform(rotationAngle: -0.1)
            self?.imageViewBall.transform = CGAffineTransform(translationX: -20.0, y: -50.0)
        }) { [weak self] _ in
            UIView.animate(withDuration: 4.0, delay: 0.0, options: [], animations: {
                self?.imageViewTest.transform = CGAffineTransform(rotationAngle: 0.1)
                self?.imageViewBall.transform = CGAffineTransform(translationX: 20.0, y: -90.0)
            }, completion: { _ in
                UIView.animate(withDuration: 2.0, delay: 0.0, options: [], animations: {
                    self?.imageViewTest.transform = CGAffineTransform(rotationAngle: 0.0)
                    self?.imageViewBall.alpha = 0.0
                }, completion: { _ in
                    self?.imageViewBall.transform = CGAffineTransform(translationX: 0.0, y: 0.0)
                    self?.imageViewBall.alpha = 1.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self?.startAnimation()
                    }
                })
            })
        }
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        let vc = GyroscopeTestVC()
        vc.testDelegate = testDelegate
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_GYROSCOPE.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
    
}
