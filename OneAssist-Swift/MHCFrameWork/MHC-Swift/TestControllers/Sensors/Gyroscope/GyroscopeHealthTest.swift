//
//  GyroscopeHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 22/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

protocol GyroHealthTestDelegate: HealthTestDelegate {
    func angleReceived(angleX: Double, angleY: Double)
}

class GyroscopeHealthTest: BaseHealthTest {
    
    weak var gyroHealthDelegate: GyroHealthTestDelegate?

    override func startHealthTest() {
        super.startHealthTest()
        let motionManager = MotionManager.shared
        if motionManager.isDeviceMotionAvailable {
            motionManager.updateInterval = 0.1

            motionManager.getMotionUpdates { (yaw, roll, pitch, error) in
                if let pitch = pitch, let roll = roll {
                    self.gyroHealthDelegate?.angleReceived(angleX: roll, angleY: pitch)
                    print("x: \(pitch)  y: \(roll)")
                }
            }
        }
    }
    
    func testSuccess() {
        MotionManager.shared.stopMotionUpdate()
        MHCResult.shared.deviceDetails.Q_GYROSCOPE.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_GYROSCOPE.testTime = getTimeConsumedInTest()
        gyroHealthDelegate?.testPass()
        gyroHealthDelegate = nil
    }
    
    func testFail() {
        MotionManager.shared.stopMotionUpdate()
        MHCResult.shared.deviceDetails.Q_GYROSCOPE.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_GYROSCOPE.testTime = getTimeConsumedInTest()
        gyroHealthDelegate?.testFailed(error: nil)
    }
}
