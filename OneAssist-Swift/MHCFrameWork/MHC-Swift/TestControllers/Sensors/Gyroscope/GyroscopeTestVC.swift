//
//  GyroscopeTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 22/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class GyroscopeTestVC: BaseTestVC {

    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var viewCourt: UIView!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var labelSuccess: H3BoldLabel!
    
    fileprivate var gyroTest = GyroscopeHealthTest()
    fileprivate var imageViewBall = UIImageView()
    fileprivate var ballPoints = CGPoint(x: 0, y: 0)
    fileprivate var goalPostRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    fileprivate let goalPostLayer = CAShapeLayer()
    var viewAnimation = UIView()
    var isReloadView = true
    var isFirstLoad = true
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "GyroscopeTestVC", bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        totalTime = 30
        labelTimer.text = "\(totalTime) s"
        labelSuccess.textColor = UIColor.seaGreen
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isReloadView {
            addBorder()
            initializeView()
        }
    }
    
    fileprivate func initializeView() {
        
        gyroTest.gyroHealthDelegate = self
        totalTime = 30
        labelTimer.text = "\(totalTime) s"
    }
    
    private func addToolTip() {
        addTap()
        if isFirstLoad {
            isFirstLoad = false
            
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.center
            
            let attributedText = NSMutableAttributedString.init(string: MhcStrings.Gyroscope.tipMessage, attributes: [.foregroundColor: UIColor.white, .font: DLSFont.bodyText.regular,  .paragraphStyle: style])
            Utilities.tipTool(with: 16).show(attributedText: attributedText, direction: .up, maxWidth: UIScreen.main.bounds.width - 64, in: view, from: viewAnimation.frame)
        }
    }
    
    override func tapped(tap: UITapGestureRecognizer) {
        removeTap()
        startTimer()
        gyroTest.startHealthTest()
        viewAnimation.isHidden = true
    }
    
    fileprivate func addBorder() {
        
        // Add ball on view
        imageViewBall.frame = CGRect(x: view.frame.width/2-25.0, y: viewCourt.frame.maxY - 25, width: 50, height: 50)
        imageViewBall.image = #imageLiteral(resourceName: "ic_football")
        imageViewBall.layer.cornerRadius = 25.0
        imageViewBall.clipsToBounds = true
        view.addSubview(imageViewBall)
        
        // Add border on screen
        let circlePath = UIBezierPath(roundedRect: viewCourt.bounds, cornerRadius: 5.0)
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.footballCourt.cgColor
        shapeLayer.lineWidth = 5.0
        viewCourt.layer.addSublayer(shapeLayer)
        
        // add center part of screen
        let centerPath = UIBezierPath.init(arcCenter: CGPoint(x: viewCourt.frame.width/2, y: viewCourt.frame.height/2), radius: 60, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: true)
        
        centerPath.move(to: CGPoint(x: viewCourt.frame.width/2 - 60, y: viewCourt.frame.height/2))
        centerPath.addLine(to: CGPoint(x: 0, y: viewCourt.frame.height/2))
        centerPath.move(to: CGPoint(x: viewCourt.frame.width/2 + 60, y: viewCourt.frame.height/2))
        centerPath.addLine(to: CGPoint(x: viewCourt.frame.width, y: viewCourt.frame.height/2))
        centerPath.close()
        
        let centerLayer = CAShapeLayer()
        centerLayer.path = centerPath.cgPath
        centerLayer.fillColor = UIColor.clear.cgColor
        centerLayer.strokeColor = UIColor.footballCourt.cgColor
        centerLayer.lineWidth = 5.0
        viewCourt.layer.addSublayer(centerLayer)
        
        // Draw Goalpost on screen
        goalPostRect = CGRect(x: viewCourt.frame.width/2-80, y: 0.0, width: 160.0, height: 50)
        let goalPost = UIBezierPath(roundedRect: goalPostRect, cornerRadius: 5.0)
        goalPost.move(to: CGPoint(x: viewCourt.frame.width/2 - 38, y: 50))
        goalPost.addArc(withCenter: CGPoint(x: viewCourt.frame.width/2, y: 50), radius: 38, startAngle: 0.0, endAngle: .pi, clockwise: true)
        goalPost.close()
        
        goalPostLayer.strokeColor = UIColor.footballCourt.cgColor
        goalPostLayer.path = goalPost.cgPath
        goalPostLayer.lineWidth = 5.0
        goalPostLayer.cornerRadius = 5.0
        goalPostLayer.fillColor = UIColor.clear.cgColor
        viewCourt.layer.addSublayer(goalPostLayer)
        
        let labelGoal = H2BoldLabel(frame: goalPostRect)
        labelGoal.text = "G O A L"
        labelGoal.textColor = UIColor.seaGreen
        labelGoal.textAlignment = .center
        viewCourt.addSubview(labelGoal)
        
        goalPostRect = view.convert(goalPostRect, from: viewCourt)
        ballPoints = CGPoint(x: imageViewBall.center.x, y: imageViewBall.center.y)
        labelTimer.isHidden = false
        
        addAnimationView()
        addToolTip()
    }
    
    private func addAnimationView() {
        
        viewAnimation = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 100, height: 100))
        view.addSubview(viewAnimation)
        viewAnimation.center = imageViewBall.center
        view.bringSubviewToFront(imageViewBall)
        setUpAnimation(in: viewAnimation.layer, size: viewAnimation.frame.size, color: UIColor.dodgerBlue)
    }
    
    override func timerStarted() {
        super.timerStarted()
        
        labelTimer.text = "\(totalTime) s"
        if totalTime <= 0  {
            gyroTest.testFail()
        }
    }
}

extension GyroscopeTestVC: GyroHealthTestDelegate {
    
    func angleReceived(angleX: Double, angleY: Double) {
        
        ballPoints.x = ballPoints.x + CGFloat(angleX*70)
        ballPoints.y = ballPoints.y + CGFloat(angleY*70)
        
        ballPoints.x = (ballPoints.x > (view.frame.maxX-35.0)) ? view.frame.maxX-35.0 : ballPoints.x
        ballPoints.y = (ballPoints.y > (view.frame.maxY-35.0)) ? view.frame.maxY-35.0 : ballPoints.y
        
        ballPoints.x = (ballPoints.x < 35) ? 35 : ballPoints.x
        ballPoints.y = (ballPoints.y < 55) ? 55 : ballPoints.y

        UIView.animate(withDuration: 0.1) {
            self.imageViewBall.center = self.ballPoints
        }
        
        if goalPostRect.intersects(imageViewBall.frame) {
            gyroTest.testSuccess()
        }
    }
    
    func testFinished() {
        timer?.invalidate()
        timer = nil
        testDelegate?.testFinished()
        dismiss(animated: true, completion: nil)
    }
    
    func testPass() {
        timer?.invalidate()
        timer = nil
        viewSuccess.isHidden = false
        goalPostLayer.strokeColor = UIColor.seaGreen.cgColor
        goalPostLayer.fillColor = UIColor.backgroundSuccess.cgColor
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.testFinished()
        }
    }
    
    func testFailed(error: Error?) {
        openRetryScreen()
    }
    
    func openRetryScreen() {
        let vc = RetryTestVC()
        vc.testTitle = MhcStrings.Gyroscope.startTitle
        vc.delegate = self
        isReloadView = false
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension GyroscopeTestVC: RetryTestDelegate {
    
    func tappedOnRetry() {
        isReloadView = true
        addBorder()
        initializeView()
    }
    
    func tappedOnSkipped() {
        isReloadView = true
        dismiss(animated: true) {
            self.testDelegate?.testFinished()
        }
    }
    
    func setUpAnimation(in layer: CALayer, size: CGSize, color: UIColor) {
        let duration: CFTimeInterval = 3
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0, 1.5, 3.0]
        
        // Scale animation
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = duration
        scaleAnimation.fromValue = 0
        scaleAnimation.toValue = 1
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimation.duration = duration
        opacityAnimation.keyTimes = [0, 0.05, 1]
        opacityAnimation.values = [0, 1, 0]
        
        // Animation
        let animation = CAAnimationGroup()
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false

        // Draw balls
        for i in 0 ..< 3 {

            let layer1: CAShapeLayer = CAShapeLayer()
            let path: UIBezierPath = UIBezierPath()
    
            path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                        radius: size.width / 2,
                        startAngle: 0,
                        endAngle: CGFloat(2 * Double.pi),
                        clockwise: false)
            layer1.fillColor = color.cgColor
            layer1.backgroundColor = nil
            layer1.path = path.cgPath
            layer1.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            
            let circle = layer1
            
            
            let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                               y: (layer.bounds.size.height - size.height) / 2,
                               width: size.width,
                               height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.frame = frame
            circle.opacity = 0
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
}
