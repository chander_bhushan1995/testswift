//
//  MotionManager.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 14/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import CoreMotion

class MotionManager {
    
    static let shared = MotionManager()
    private lazy var motionManager = CMMotionManager()
    var updateInterval = 0.1
    
    private init() {
    }
    
    var isAccelerometerAvailable: Bool {
        return motionManager.isAccelerometerAvailable
    }
    
    var isDeviceMotionAvailable: Bool {
        return motionManager.isDeviceMotionAvailable
    }
    
    func stopMotionUpdate() {
        motionManager.stopDeviceMotionUpdates()
    }
    
    func stopAccelerometerUpdates() {
        motionManager.stopAccelerometerUpdates()
    }
    
    func getMotionUpdates(handler: @escaping (_ yaw: Double?,_ roll: Double?,_ pitch: Double?, _ error: Error?) -> Void) {
        if isDeviceMotionAvailable {
            motionManager.deviceMotionUpdateInterval = updateInterval
            motionManager.startDeviceMotionUpdates(to: .main) { (datas, error) in
                if let err = error {
                    handler(nil, nil, nil, err)
                }
                if let data = datas {
                    handler(data.attitude.yaw, data.attitude.roll, data.attitude.pitch, error)
                }
            }
        }
    }
    
    func getAccelerationUpdate(handler: @escaping (_ data: (Double, Double, Double)?, _ error: Error?) -> Void) {
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = updateInterval
            motionManager.startAccelerometerUpdates(to: .main) { (accData, error) in
                if let data = accData {
                    handler((data.acceleration.x, data.acceleration.y, data.acceleration.z), error)
                } else {
                    handler(nil, error)
                }
            }
        }
    }
}
