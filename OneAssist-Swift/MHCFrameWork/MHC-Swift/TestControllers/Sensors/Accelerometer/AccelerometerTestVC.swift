//
//  AccelerometerTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 19/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class AccelerometerTestVC: CommonTestVC {

    private var accelerometerTest = AccelerometerHealthTest()
    
    private var lock = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView() {
        testName = MhcStrings.Accelerometer.startTitle
        labelTitle.text = ""
        labelDescription.text = ""
        labelInstruction.text = MhcStrings.Accelerometer.instruction
        imageView.image = #imageLiteral(resourceName: "shake")
        totalTime = 8
        labelRemainingTime.text = "\(totalTime)s"
        accelerometerTest.healthTestDelegate = self
        startTimer()
        accelerometerTest.startHealthTest()
    }
    
    override func timerStarted() {
        super.timerStarted()
        if lock == 0 {
            imageView.setAnchorPoint(CGPoint(x: 0.8, y: 0.5))
            animateImageView()
            lock += 1
        }
        if totalTime <= 0  {
            accelerometerTest.testTimeout()
        } 
    }
    
    private func animateImageView() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
            self?.imageView.transform = CGAffineTransform(rotationAngle: 60.0 * CGFloat.pi/180.0)
        }, completion: { [weak self] _ in
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: {
                self?.imageView.transform = CGAffineTransform(rotationAngle: -60 * CGFloat.pi/180.0)
            }, completion: { _ in
                UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                    self?.imageView.transform = CGAffineTransform(rotationAngle: 0 * CGFloat.pi/180.0)
                }, completion: { _ in
                    if (self?.totalTime ?? 0) > 0 {
                        self?.delay(1) {
                            self?.animateImageView()
                        }
                    } else {
                        self?.lock = 0
                    }
                    self?.view.layoutIfNeeded()
                })
            })
        })
    }

    override func tappedOnRetry() {
        super.tappedOnRetry()
        initializeView()
    }
}
