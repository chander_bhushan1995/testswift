//
//  AccelerometerStartTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 19/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class AccelerometerTestStartVC: TestStartVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_shake")
        labelTestName.text = MhcStrings.Accelerometer.startTitle
        labelTestDescription.text = MhcStrings.Accelerometer.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        
        let vc = AccelerometerTestVC()
        vc.testDelegate = testDelegate
        presentInFullScreen(vc, animated: true, completion: nil)
    }

    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
    
}
