//
//  AccelerometerHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 19/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

class AccelerometerHealthTest: BaseHealthTest {
    
    private var lastUpdate: Date!
    private var lastShake: Date!
    private var currentValues = (0.0, 0.0, 0.0)
    private var previousValues = (0.0, 0.0, 0.0)
    private var isMegnetoDone = false {
        didSet {
            testsCompleted()
        }
    }
    private var isAccDone = false {
        didSet {
            testsCompleted()
        }
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        
        isMegnetoDone = false
        isAccDone = false
        
        let motionManager = MotionManager.shared
        if motionManager.isAccelerometerAvailable {
            motionManager.updateInterval = 0.1
            motionManager.getAccelerationUpdate { (data, error) in
                if data != nil {
                    self.getAcceleroUpdate(data: data!)
                } else {
                    self.accTestFail()
                }
            }
        }  else {
            MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult = HealthCheckStatus.result_not_available.rawValue
            isAccDone = true
        }
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.updateInterval = 0.02
            
            motionManager.getMotionUpdates { (yaw, roll, pitch, error) in
                if let yaw = yaw {
                    let angle = yaw * 180 / .pi
                    print(angle)
                    if angle >= 30.0 || angle <= -30.0 {
                        self.magTestSuccess()
                    }
                } else if error != nil {
                    self.magTestFail()
                }
            }
        } else {
            MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult = HealthCheckStatus.result_not_available.rawValue
            isMegnetoDone = true
        }
    }
    
    private func getAcceleroUpdate(data: (Double, Double, Double)) {
        
        let currentTime = Date()
        currentValues = (data.0, data.1, data.2)
        
        if lastUpdate == nil {
            previousValues = currentValues
            lastUpdate = currentTime
            lastShake = currentTime
        } else {
            let timeDiff = currentTime.timeIntervalSince(lastUpdate)
            if timeDiff > 0 {
                let force = abs(currentValues.0 + currentValues.1 + currentValues.2 - previousValues.0 - previousValues.1 - previousValues.2)
                if force > 1.53 {
                    if currentTime.timeIntervalSince(lastShake) > 0.2 {
                        accTestSuccess()
                    }
                    lastShake = currentTime
                }
                previousValues = currentValues
                lastUpdate = currentTime
            }
        }
    }
    
    private func accTestSuccess() {
        MotionManager.shared.stopAccelerometerUpdates()
        MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testTime = getTimeConsumedInTest()
        isAccDone = true
    }
    
    func accTestFail() {
        MotionManager.shared.stopAccelerometerUpdates()
        MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testTime = getTimeConsumedInTest()
        isAccDone = true
    }
    
    private func magTestSuccess() {
        MotionManager.shared.stopMotionUpdate()
        MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testTime = getTimeConsumedInTest()
        isMegnetoDone = true
    }
    
    func magTestFail() {
        MotionManager.shared.stopMotionUpdate()
        MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testTime = getTimeConsumedInTest()
        isMegnetoDone = true
    }
    
    func testTimeout() {
        
        if !isMegnetoDone || !isAccDone {
            if !isMegnetoDone {
               magTestFail()
            }
            if !isAccDone {
               accTestFail()
            }
        } else {
            healthTestDelegate?.testFailed(error: nil)
            healthTestDelegate = nil
        }
    }
    
    func testsCompleted() {
        if isAccDone && isMegnetoDone {
            if MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult == HealthCheckStatus.result_pass.rawValue &&
                MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult == HealthCheckStatus.result_pass.rawValue {
                healthTestDelegate?.testPass()
                healthTestDelegate = nil
            } else if MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult == HealthCheckStatus.result_fail.rawValue ||
                MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult == HealthCheckStatus.result_fail.rawValue {
                healthTestDelegate?.testFailed(error: nil)
                healthTestDelegate = nil
            }
        }
    }
}
