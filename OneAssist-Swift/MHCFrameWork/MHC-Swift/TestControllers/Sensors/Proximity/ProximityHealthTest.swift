//
//  ProximityHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 16/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

class ProximityHealthTest: BaseHealthTest {
    
    override func startHealthTest() {
        super.startHealthTest()
        let device = UIDevice.current
        device.isProximityMonitoringEnabled = true
        if device.isProximityMonitoringEnabled {
            NotificationCenter.default.addObserver(self, selector: #selector(proximityStateChanged), name: UIDevice.proximityStateDidChangeNotification, object: device)
        } else {
            testTimeout()
        }
    }
    
    @objc private func proximityStateChanged() {
        UIDevice.current.isProximityMonitoringEnabled = false
        testSuccess()
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_PROXIMITY.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_PROXIMITY.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    func testTimeout() {
        UIDevice.current.isProximityMonitoringEnabled = false
        MHCResult.shared.deviceDetails.Q_PROXIMITY.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_PROXIMITY.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
    }
}
