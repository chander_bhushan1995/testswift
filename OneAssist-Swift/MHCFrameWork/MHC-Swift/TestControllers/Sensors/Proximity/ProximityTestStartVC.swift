//
//  ProximityTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class ProximityTestStartVC: TestStartVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_proximity")
        labelTestName.text = MhcStrings.Proximity.startTitle
        labelTestDescription.text = MhcStrings.Proximity.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        
        let vc = ProximityTestVC()
        vc.testDelegate = testDelegate
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_PROXIMITY.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
    
}
