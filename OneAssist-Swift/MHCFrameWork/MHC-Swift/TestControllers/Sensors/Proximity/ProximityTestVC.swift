//
//  ProximityTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 16/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class ProximityTestVC: CommonTestVC {

    private let proximityTest = ProximityHealthTest()
    private var lock = 0
    private var handImageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView() {
        testName = MhcStrings.Proximity.startTitle
        imageView.image = #imageLiteral(resourceName: "proxiDevice")
        labelTitle.text = ""
        labelDescription.text = ""
        addHandImageView()
        labelInstruction.text = MhcStrings.Proximity.instruction
        totalTime = 10
        labelRemainingTime.text = "\(totalTime) s"
        proximityTest.healthTestDelegate = self
        startTimer()
        proximityTest.startHealthTest()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        handImageView?.alpha = 0
    }
    
    private func addHandImageView() {
        // Same is the height
        let width = UIScreen.main.bounds.width - (50*2)
        
        if handImageView == nil {
            handImageView = UIImageView(frame: CGRect(x: width/2 - 10, y: width/2 - 140, width: 100, height: 100))
            viewForBackImage.addSubview(handImageView!)
        }
        
        handImageView?.transform = CGAffineTransform(rotationAngle: 0.0)
        handImageView?.contentMode = .center
        handImageView?.image = #imageLiteral(resourceName: "proxiHand")
        handImageView?.alpha = 0
    }
    
    override func timerStarted() {
        super.timerStarted()
        if lock == 0 {
            handImageView?.setAnchorPoint(CGPoint(x: 1, y: 1))
            animateHand()
            lock += 1
        }
        if totalTime <= 0  {
            proximityTest.testTimeout()
        }
    }
    
    private func animateHand() {
        UIView.animateKeyframes(withDuration: 1, delay: 0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.4, animations: {
                self.handImageView?.alpha = 1
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {
                self.handImageView?.transform = CGAffineTransform(rotationAngle: -50.0 * CGFloat.pi/180.0)
            })
        }) { _ in
            UIView.animate(withDuration: 1, delay: 0, options: [.repeat,.autoreverse,.curveEaseInOut], animations: {
                self.handImageView?.transform = CGAffineTransform(rotationAngle: 0.0 * CGFloat.pi/180.0)
            }, completion: nil)
        }
    }

    override func tappedOnRetry() {
        super.tappedOnRetry()
        lock = 0
        initializeView()
    }
}
