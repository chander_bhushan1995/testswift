//
//  CameraHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 06/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit
import AVFoundation

class CameraHealthTest: BaseHealthTest {
    
    fileprivate let imagePickerVC = UIImagePickerController()
    fileprivate var timer: Timer?
    weak var context: CameraTestVC?
    weak var testDelegate: TestResultDelegate?
    fileprivate var device =  AVCaptureDevice.default(for: .video)
    fileprivate var timeElapse = 3
    fileprivate var testFailTimer: Timer?
    private var tourchStartTime: Date?
    
    init(context: CameraTestVC) {
        self.context = context
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        Permission.checkCameraPermission(eventLocation: Event.EventParams.mhcCamera, authorizationStatus: { (status) in
            if status != .authorized {
                if status == .denied {
                    Utilities.showAlertForSetting(on: self.context ?? UIViewController(), title: MhcStrings.AlertMessage.cameraTitle,  message: MhcStrings.AlertMessage.cameraSetting, secondAction: {
                        self.testFailed(with: .result_skipped)
                        
                        EventTracking.shared.eventTracking(name: .cameraSetting, [.location: Event.EventParams.mhcCamera, .action: Event.EventParams.close])
                    }) {
                        EventTracking.shared.eventTracking(name: .cameraSetting, [.location: Event.EventParams.mhcCamera, .action: Event.EventParams.settings])
                    }
                } else {
                    self.testFailed(with: .result_not_available)
                }
            } else {
                self.initializePickerView()
            }
        })
    }
    
    fileprivate func initializePickerView() {
        imagePickerVC.sourceType = .camera
        imagePickerVC.cameraCaptureMode = .photo
        imagePickerVC.cameraFlashMode = .off
        imagePickerVC.showsCameraControls = false
        imagePickerVC.cameraDevice = .front
        imagePickerVC.delegate = self

        context?.labelTitle.text = "Front Camera"
        context?.lblTimer.isHidden = false
        context?.addChild(imagePickerVC)
        context?.viewCamera.addSubview(imagePickerVC.view)
        
        imagePickerVC.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: imagePickerVC.view, attribute: .top, relatedBy: .equal, toItem: context?.viewCamera, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: imagePickerVC.view, attribute: .bottom, relatedBy: .equal, toItem: context?.viewCamera, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: imagePickerVC.view, attribute: .leading, relatedBy: .equal, toItem: context?.viewCamera, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: imagePickerVC.view, attribute: .trailing, relatedBy: .equal, toItem: context?.viewCamera, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        
        initialiseTimer()
    }
    
    fileprivate func initialiseTimer()  {
        timeElapse = 3
        setTextCameraTimer(time: self.timeElapse)
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(cameraTimerStarted), userInfo: nil, repeats: true)
    }
    
    fileprivate func setTextCameraTimer(time : Int) {
        context?.lblTimer.text = "Clicking picture in \(time)s…"
    }
    
    @objc fileprivate func cameraTimerStarted() {
        timeElapse -= 1
        setTextCameraTimer(time: timeElapse)
        
        if timeElapse <= 0 {
            timer?.invalidate()
            context?.lblTimer.isHidden = true
            imagePickerVC.takePicture()
            testFailTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(cameraTestFail), userInfo: nil, repeats: false)
        }
    }
    
    @objc fileprivate func cameraTestFail() {
        if imagePickerVC.cameraDevice == .front {
            MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testResult = HealthCheckStatus.result_fail.rawValue
            initialiseTimer()
            MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testTime = getTimeConsumedInTest()
            changeCamera()
        } else {
            startFlashTest()
            MHCResult.shared.deviceDetails.Q_BACKCAMERA.testResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_BACKCAMERA.testTime = getTimeConsumedInTest()
        }
    }

    fileprivate func testFinished() {
        timer?.invalidate()
        testFailTimer?.invalidate()
        let testResult = MHCResult.shared.deviceDetails
        if [testResult.Q_FRONTCAMERA.testResult, testResult.Q_BACKCAMERA.testResult, testResult.Q_FLASH.testResult, testResult.Q_AUTOFOCUS.testResult].contains(HealthCheckStatus.result_fail.rawValue) {
            openRetryScreen()
        } else {
            context?.dismiss(animated: true) {
                self.testDelegate?.testFinished()
                self.testDelegate = nil
            }
        }
    }
    
    private func openRetryScreen() {
        let vc = RetryTestVC()
        vc.testTitle = MhcStrings.Camera.startTitle
        vc.delegate = self
        context?.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func testFailed(with result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testTime = getTimeConsumedInTest()
        MHCResult.shared.deviceDetails.Q_BACKCAMERA.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_BACKCAMERA.testTime = getTimeConsumedInTest()
        timer?.invalidate()
        testFailTimer?.invalidate()
        context?.dismiss(animated: true) {
            self.testDelegate?.testFinished()
            self.testDelegate = nil
        }
    }
    
    fileprivate func changeCamera() {
        
        startedTime = Date()
        imagePickerVC.cameraDevice = .rear
        context?.lblTimer.isHidden = false
        context?.labelTitle.text = "Back Camera"
        initialiseTimer()
        
        if (((try? device?.lockForConfiguration()) as ()??)) != nil && (device?.isFocusPointOfInterestSupported ?? false && device?.isFocusModeSupported(.continuousAutoFocus) ?? false) {
            device?.focusMode = .continuousAutoFocus
            autofocusResult(with: .result_pass)
            device?.unlockForConfiguration()
        } else {
            autofocusResult(with: .result_not_available)
        }
    }
    
    private func autofocusResult(with status: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_AUTOFOCUS.testResult = status.rawValue
        MHCResult.shared.deviceDetails.Q_AUTOFOCUS.testTime = getTimeConsumedInTest()
        MHCResult.shared.deviceDetails.Q_AUTOFOCUS.autofocusPoint = device?.focusPointOfInterest
    }
    
    func startFlashTest() {
        tourchStartTime = Date()
        if let isFlash = device?.hasFlash, let isTourch = device?.hasTorch, isFlash && isTourch {
            onFlash()
        } else {
            getFlashTestResult(result: HealthCheckStatus.result_not_available)
        }
    }
    
    private func onFlash() {
        do {
            try device?.lockForConfiguration()
            device?.torchMode = .on
            device?.flashMode = .on
            device?.unlockForConfiguration()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(20)) {
                MHCResult.shared.deviceDetails.Q_FLASH.tourchLevel = self.device?.torchLevel
                self.offFlash()
            }
        }
        catch {
            getFlashTestResult(result: HealthCheckStatus.result_fail)
        }
    }
    
    private func offFlash() {
        do {
            try device?.lockForConfiguration()
            device?.torchMode = .off
            device?.flashMode = .off
            device?.unlockForConfiguration()
            getFlashTestResult(result: HealthCheckStatus.result_pass)
        }
        catch {
            getFlashTestResult(result: HealthCheckStatus.result_fail)
        }
        
    }
    
    func getFlashTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_FLASH.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_FLASH.testTime = tourchStartTime?.timeIntervalTillNowInMilliSeconds() ?? 0
        testFinished()
    }
}

extension CameraHealthTest: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        timer?.invalidate()
        testFailTimer?.invalidate()
        if let _ = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if picker.cameraDevice == .rear {
                MHCResult.shared.deviceDetails.Q_BACKCAMERA.testResult = HealthCheckStatus.result_pass.rawValue
            } else if picker.cameraDevice == .front {
                MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testResult = HealthCheckStatus.result_pass.rawValue
            }
        } else {  // Camera is not able to create Image
            if picker.cameraDevice == .rear {
                MHCResult.shared.deviceDetails.Q_BACKCAMERA.testResult = HealthCheckStatus.result_fail.rawValue
            } else if picker.cameraDevice == .front {
                MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testResult = HealthCheckStatus.result_fail.rawValue
            }
        }
        
        if picker.cameraDevice == .front {
            MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testTime = getTimeConsumedInTest()
            changeCamera()
        } else if picker.cameraDevice == .rear {
            MHCResult.shared.deviceDetails.Q_BACKCAMERA.testTime = getTimeConsumedInTest()
            startFlashTest()
        }
    }
}

extension CameraHealthTest: RetryTestDelegate {
    func tappedOnRetry() {
        startHealthTest()
    }
    
    func tappedOnSkipped() {
        context?.dismiss(animated: true) {
            self.timer?.invalidate()
            self.testFailTimer?.invalidate()
            self.testDelegate?.testFinished()
        }
    }
}
