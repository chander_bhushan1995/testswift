//
//  CameraTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class CameraTestStartVC: TestStartVC {
    
    var cameraTest: CameraTestVC? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_camera")
        labelTestName.text = MhcStrings.Camera.startTitle
        labelTestDescription.text = MhcStrings.Camera.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        cameraTest = CameraTestVC()
        cameraTest?.testDelegate = testDelegate
        presentInFullScreen(cameraTest!, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_BACKCAMERA.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_AUTOFOCUS.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_FLASH.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
}
