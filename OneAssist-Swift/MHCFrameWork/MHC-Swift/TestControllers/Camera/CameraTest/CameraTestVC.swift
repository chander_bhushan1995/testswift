//
//  CameraTestVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 31/05/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class CameraTestVC: BaseTestVC {

    @IBOutlet weak var lblTimer : UILabel!
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var labelTitle: H2BoldLabel!
    @IBOutlet weak var viewCamera: UIView!
    
    var cameraTest: CameraHealthTest? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        labelTitle.textColor = UIColor.white
        lblTimer.textColor = UIColor.white
        cameraTest = CameraHealthTest(context: self)
        cameraTest?.testDelegate = testDelegate
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkPermission()
    }
    
    func checkPermission() {
        Permission.checkCameraPermission(eventLocation: Event.EventParams.mhcCamera, authorizationStatus: { (status) in
            if status != .authorized {
                if status == .denied {
                    self.addObserverActiveApp()
                    Utilities.showAlertForSetting(on: self, title: MhcStrings.AlertMessage.cameraTitle, message: MhcStrings.AlertMessage.cameraSetting, secondAction: {
                        self.cameraTest?.testFailed(with: .result_skipped)
                        self.removeObserverActiveApp()
                        EventTracking.shared.eventTracking(name: .cameraSetting, [.location: Event.EventParams.mhcCamera, .action: Event.EventParams.close])
                    }) {
                        EventTracking.shared.eventTracking(name: .cameraSetting, [.location: Event.EventParams.mhcCamera, .action: Event.EventParams.settings])
                    }
                } else {
                    self.cameraTest?.testFailed(with: .result_not_available)
                }
            } else {
                self.cameraTest?.startHealthTest()
            }
        })
    }
    
    override func appBecomeActive() {
        super.appBecomeActive()
        checkPermission()
    }
}
