//
//  TouchHealthTest.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 20/11/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

// Present your view controller only after passing your content view to this manager. Just after loading the view, it would create a wrapper (cover view) around that.
class TouchHealthTest: BaseHealthTest {
    var contentView: UIView
    var height: CGFloat
    var width: CGFloat
    var color: UIColor
    var spacing: CGFloat = 0.0
    
    // Above fields are only accessible outside
    private var buttons: [UIButton] = []
    private var coverView: UIView?
    private var calculatedWidth: CGFloat = 0.0
    private var calculatedHeight: CGFloat = 0.0
    private var panGestureRecognizer: UIPanGestureRecognizer?
    private var isTouchStarted: Bool = false
    private var totalButtonCount: Int = 1
    
    init(contentView: UIView, height: CGFloat, width: CGFloat, color: UIColor, spacing: CGFloat = 0.0) {
        self.contentView = contentView
        self.height = height
        self.width = width
        self.color = color
        self.spacing = spacing
        super.init()
        initialSetup()
    }
    
    private func initialSetup() {
        coverView = UIView(frame: UIScreen.main.bounds)
        if let coverView = coverView {
            coverView.backgroundColor = UIColor.clear
            contentView.addSubview(coverView)
            
            // Write seperate method to add rectangle boxes inside cover view
            addRectangleBoxesOnContentView()
            
            // To add Gestures
            setGestures(on: coverView)
        }
    }
    
    func clearContent() {
        isTouchStarted = false
        coverView?.removeFromSuperview()
        coverView = nil
    }
    
    private func getCalculatedValue(with numberOfRectangles: Int, mainSize: CGFloat) -> CGFloat {
        return (mainSize - (CGFloat(numberOfRectangles - 1) * spacing))/CGFloat(numberOfRectangles)
    }
    
    private func addRectangleBoxesOnContentView() {
        let screenSize = UIScreen.main.bounds
        
        calculatedWidth = getCalculatedValue(with: Int(screenSize.width/width), mainSize: screenSize.width)
        calculatedHeight = getCalculatedValue(with: Int(screenSize.height/height), mainSize: screenSize.height)
        
        for row in stride(from: 0, to: Int(screenSize.height/height), by: 1) {
            for col in stride(from: 0, to: Int(screenSize.width/width), by: 1) {
                let btnView = UIButton(frame: CGRect(x: CGFloat(col) * (calculatedWidth + spacing), y: CGFloat(row) * (calculatedHeight + spacing), width: calculatedWidth, height: calculatedHeight))
                btnView.backgroundColor = color
                buttons.append(btnView)
                coverView?.addSubview(btnView)
            }
        }
        totalButtonCount = buttons.count
    }
    
    private func setGestures(on view: UIView) {
        if panGestureRecognizer == nil {
            panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognized(_:)))
            view.addGestureRecognizer(panGestureRecognizer!)
        }
        
        for btn in buttons {
            btn.addTarget(self, action: #selector(buttonTapped), for: [.allTouchEvents])
        }
    }
    
    private func unsetGestures(on view: UIView) {
        if let panGestureRecognizer = self.panGestureRecognizer {
            view.removeGestureRecognizer(panGestureRecognizer)
            self.panGestureRecognizer = nil
        }
        
        for btn in buttons {
            btn.removeTarget(self, action: #selector(buttonTapped), for: [.allTouchEvents])
        }
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        animate(btnView: sender)
    }
    
    private func animate(btnView: UIButton) {
        if !isTouchStarted {
            healthTestDelegate?.testStarted()
            isTouchStarted = true
        }
        if buttons.isEmpty {
            return
        }
        UIView.animate(withDuration: 0.4, animations: {
            btnView.backgroundColor = .clear
            btnView.frame.origin = CGPoint(x: btnView.frame.origin.x - 20, y: btnView.frame.origin.y - 20)
        }) { finished in
            if finished {
                btnView.removeFromSuperview()
                self.buttons.removeAll(where: { $0 == btnView })
                self.healthTestDelegate?.testCompleted(Int(ceil(100*Double(self.buttons.count)/Double(self.totalButtonCount))))
                if self.buttons.isEmpty {
                    self.healthTestDelegate?.testPass()
                }
            }
        }
    }
    
    @objc func panGestureRecognized(_ recognizer: UIPanGestureRecognizer) {
        let location = recognizer.location(in: coverView)
        if let btnView = buttons.first(where: { isButton($0, containsLocation: location) }) {
            animate(btnView: btnView)
        }
    }
    
    private func isButton(_ button: UIButton, containsLocation location: CGPoint) -> Bool {
        return (location.x > button.frame.origin.x && location.x < button.frame.origin.x + calculatedWidth) && (location.y > button.frame.origin.y && location.y < button.frame.origin.y + calculatedHeight)
    }
}
