//
//  TouchTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class TouchTestStartVC: TestStartVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "device")
        labelTestName.text = MhcStrings.MultiTouch.startTitle
        labelTestDescription.text = MhcStrings.MultiTouch.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        
        let touchTestVC = TouchTestVC()
        touchTestVC.testDelegate = testDelegate
        presentInFullScreen(touchTestVC, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_MULTITOUCH.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
}
