//
//  TouchTestOverlayView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 20/11/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TouchTestOverlayView: UIView {
    
    @IBOutlet weak var gradientView: LayerContainerView!
    @IBOutlet weak var touchIcon: UIImageView!
    private var isAnimationEnabled = true
    
    class func loadView() -> TouchTestOverlayView {
        return Bundle.main.loadNibNamed("TouchTestOverlayView", owner: self, options: nil)?[0] as! TouchTestOverlayView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        touchIcon.alpha = 0.0
        touchIcon.frame.origin = CGPoint(x: 8, y: 146)
        gradientView.update(start: .topCenter, end: .bottomCenter, colors: [UIColor.white.withAlphaComponent(0.3), UIColor.white.withAlphaComponent(0)], type: .axial)
        gradientView.clipsToBounds = true
        gradientView.layer.cornerRadius = 4
    }
    
    func startAnimation() {
        if isAnimationEnabled {
            touchIcon.frame.origin = CGPoint(x: 8, y: 146)
            touchIcon.alpha = 0.2
            UIView.animate(withDuration: 0.32, delay: 0, options: [.curveEaseIn], animations: { [weak self] in
                self?.touchIcon.frame.origin = CGPoint(x: 82, y: 88)
                self?.touchIcon.alpha = 1
            }) { [weak self] _ in
                if self?.isAnimationEnabled ?? false {
                    UIView.animate(withDuration: 0.34, delay: 0, options: [.curveLinear], animations: {
                        self?.touchIcon.frame.origin = CGPoint(x: 170, y: 42)
                        self?.touchIcon.alpha = 1
                    }) { _ in
                        if self?.isAnimationEnabled ?? false  {
                            UIView.animate(withDuration: 0.33, delay: 0, options: [.curveEaseOut], animations: {
                                self?.touchIcon.frame.origin = CGPoint(x: 240, y: 8)
                                self?.touchIcon.alpha = 0.2
                            }) { _ in
                                if self?.isAnimationEnabled ?? false {
                                    self?.startAnimation()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func stopAnimation() {
        isAnimationEnabled = false
    }
}
