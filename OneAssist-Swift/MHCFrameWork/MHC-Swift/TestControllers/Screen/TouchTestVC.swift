//
//  TouchTestVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 21/11/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TouchTestVC: BaseTestVC {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var viewSuccess: UIView!
    
    var overlayView: TouchTestOverlayView?
    
    var touchTest: TouchHealthTest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    fileprivate func initializeView() {
        descriptionLabel.textColor = .white
        descriptionLabel.font = DLSFont.h2.regular
        labelTimer.textColor = .saffronYellow
        labelTimer.font = DLSFont.h0.bold
        remainingLabel.textColor = .charcoalGrey
        remainingLabel.font = DLSFont.h2.regular
        remainingLabel.text = "100% remaining"
        remainingLabel.isHidden = true
        totalTime = 30
        labelTimer.text = "\(totalTime)s"
        touchTest = TouchHealthTest(contentView: contentView, height: 48, width: 48, color: .buttonTitleBlue)
        touchTest?.healthTestDelegate = self
        overlayView = TouchTestOverlayView.loadView()
        view.addSubview(overlayView!)
        overlayView?.frame = view.bounds
        overlayView?.isUserInteractionEnabled = false
        overlayView?.startAnimation()
    }
    
    private func hideOverlayView() {
        overlayView?.stopAnimation()
        overlayView?.removeFromSuperview()
        overlayView = nil
        startTimer()
    }
    
    override func timerStarted() {
        super.timerStarted()
        labelTimer.text = "\(totalTime) s"
        if totalTime <= 0  {
            getTestResult(result: .result_fail)
        }
    }
    
    private func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_MULTITOUCH.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_MULTITOUCH.testTime = Double(30 - totalTime) * 1000
        timer?.invalidate()
        touchTest?.clearContent()
        touchTest = nil
        
        if result == .result_fail {
            openRetryScreen()
        } else {
            labelTimer.isHidden = true
            viewSuccess.isHidden = false
            descriptionLabel.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                self.dismiss(animated: true) {
                    self.testDelegate?.testFinished()
                }
            }
        }
    }
    
    private func openRetryScreen() {
        let vc = RetryTestVC()
        vc.testTitle = MhcStrings.MultiTouch.startTitle
        vc.delegate = self
        presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension TouchTestVC: RetryTestDelegate {
    func tappedOnRetry() {
        initializeView()
    }
    
    func tappedOnSkipped() {
        dismiss(animated: true) {
            self.testDelegate?.testFinished()
        }
    }
}

extension TouchTestVC: HealthTestDelegate {
    
    func testCompleted(_ percentageRemaining: Int) {
        remainingLabel.text = "\(percentageRemaining)% remaining"
    }
    
    func testStarted() {
        remainingLabel.isHidden = false
        hideOverlayView()
    }
    
    @objc func testFinished() {
        remainingLabel.isHidden = true
        timer?.invalidate()
        dismiss(animated: true) {
            self.testDelegate?.testFinished()
        }
    }
    
    @objc func testPass() {
        remainingLabel.isHidden = true
        getTestResult(result: .result_pass)
    }
}
