//
//  TestDescriptionCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TestDescriptionCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var testName: H3BoldLabel!
    @IBOutlet weak var testDesc: BodyTextRegularGreyLabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusImg: UIImageView!
    @IBOutlet weak var shadowView: DLSShadowView!
    
    var mhcTestCategory: MHCTestCategory = .otherTests
    var spinnerView: SpinnerView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        spinnerView = SpinnerView(view: statusView)
    }
    
    func setViewModel(_ model: MHCTestDescriptionModel) {
        // write logic
        if mhcTestCategory == .button {
            testDesc?.removeFromSuperview()
            spinnerView = nil
        }
        
        imgView.image = model.image
        testName.text = model.testName
        testDesc?.text = model.testDescription
        
        if model.state == TestState.completed {
            shadowView.isShadowHidden = true
            spinnerView?.stopAnimating()
            statusImg.image = model.testResultImage
            statusImg.isHidden = false
            testDesc?.text = "Test completed."
        } else if model.state == TestState.started {
            shadowView.isShadowHidden = false
            spinnerView?.startAnimating()
            statusImg.isHidden = true
            testDesc?.text = "Checking \(model.testName ?? "")..."
        }
    }

}
