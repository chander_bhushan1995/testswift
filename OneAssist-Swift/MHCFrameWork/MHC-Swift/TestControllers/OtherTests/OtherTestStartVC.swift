//
//  OtherTestStartVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 12/06/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OtherTestStartVC: TestStartVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_autoTest")
        imageViewTest.contentMode = .center
        imageViewTest.clipsToBounds = false
        labelTestName.text = MhcStrings.OtherTest.startTitle
        labelTestDescription.text = MhcStrings.OtherTest.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        let vc = OtherTestVC()
        // vc.mhcTestCategory = .otherTests
        vc.testDelegate = testDelegate
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_BLUETOOTH.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_WIFI.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_RAM.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_STORAGE.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }

}
