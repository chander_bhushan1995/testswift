//
//  TestExecutionHeader.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TestExecutionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var testHeaderText: BodyTextRegularBlackLabel!
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    func setViewModel(with headerText: String) {
         testHeaderText.text = headerText
    }
}
