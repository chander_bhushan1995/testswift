//
//  BatteryHealthTest.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 23/07/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BatteryHealthTest: BaseHealthTest {
    
    override func startHealthTest() {
        super.startHealthTest()
        
        let myDevice = UIDevice.current
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        if myDevice.batteryLevel > 0.0 {
            testSuccess()
        } else {
            getTestResult(result: .result_fail)
        }
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_BATTERY.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_BATTERY.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
        UIDevice.current.isBatteryMonitoringEnabled = false
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_BATTERY.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_BATTERY.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
        healthTestDelegate = nil
        UIDevice.current.isBatteryMonitoringEnabled = false
    }
    
}

