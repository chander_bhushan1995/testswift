
//
//  OtherTestVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 12/06/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import MediaPlayer

enum MHCTestCategory {
    case otherTests
    case button
}

let headerTextForOtherCategory = "It’ll only take a few seconds."
let headerTextForButtonCategory = "Press the following keys on your mobile"

class OtherTestVC: BaseTestVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTimer: H1BoldLabel!
    
    var otherHealthTest: OtherHealthTest?
    var dataSource: [MHCTestDescriptionModel] = []
    var currentTestIndex = 0
    var mhcTestCategory: MHCTestCategory = .otherTests
    var isReloadScreen = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isReloadScreen {
            startNextTest()
        }
    }
    
    func startNextTest() {
        if mhcTestCategory == .otherTests {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                self.otherHealthTest?.startTest(test: self.dataSource[self.currentTestIndex].testType)
            }
        } else {
            totalTime = 10
            startTimer()
            labelTimer.text = "\(totalTime)s"
            self.otherHealthTest?.startTest(test: self.dataSource[self.currentTestIndex].testType)
        }
    }
    
    override func timerStarted() {
        super.timerStarted()
        if totalTime >= 0  {
            labelTimer.text = "\(totalTime)s"
        }
    }
    
    // MARK:- private methods
    fileprivate func initializeView() {
        if mhcTestCategory == .button {
            let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            view.addSubview(volumeView)
            
            labelTimer.isHidden = false
        }
        currentTestIndex = 0
        otherHealthTest = OtherHealthTest(vc: self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cell: TestDescriptionCell.self)
        tableView.register(cell: RemainingTestDescriptionCell.self)
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.MHCTestExecutionHeader)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 100
        tableView.backgroundColor = UIColor.clear
        
        populateTestData()
    }
    
    func populateTestData() {

        dataSource = []
        
        if mhcTestCategory == .button {
            dataSource.append(MhcOtherTests.volumeUp.getDescriptionModel())
            dataSource.append(MhcOtherTests.volumeDown.getDescriptionModel())
        } else if mhcTestCategory == .otherTests {
            dataSource.append(MhcOtherTests.wifi.getDescriptionModel())
            dataSource.append(MhcOtherTests.bluetooth.getDescriptionModel())
            dataSource.append(MhcOtherTests.antenna.getDescriptionModel())
            dataSource.append(MhcOtherTests.ram.getDescriptionModel())
            dataSource.append(MhcOtherTests.battery.getDescriptionModel())
            dataSource.append(MhcOtherTests.storage.getDescriptionModel())
        }
    }
}

// MARK:- Table DataSource
extension OtherTestVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mhcTestDescriptionModel = dataSource[indexPath.row]
        if  mhcTestDescriptionModel.state == .notStarted {
            let cell: RemainingTestDescriptionCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.mhcTestCategory = mhcTestCategory
            cell.setViewModel(mhcTestDescriptionModel)
            return cell
        } else {
            let cell: TestDescriptionCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.mhcTestCategory = mhcTestCategory
            cell.setViewModel(mhcTestDescriptionModel)
            return cell
        }
    }
}

// MARK:- Table Delegate
extension OtherTestVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.MHCTestExecutionHeader) as! TestExecutionHeader
        if mhcTestCategory == .otherTests {
            headerView.setViewModel(with: headerTextForOtherCategory)
        } else if mhcTestCategory == .button {
            headerView.setViewModel(with: headerTextForButtonCategory)
        }
        return headerView
    }

}

extension OtherTestVC: HealthTestDelegate {
    
    func testFinished() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.dismiss(animated: true) {
                self.testDelegate?.testFinished()
            }
        }
    }
    
    func testPass() {
        
        OAAlertVC.forceRemoveAlertView()
        
        otherHealthTest?.timer?.invalidate()
        dataSource[currentTestIndex].state = .completed
        if currentTestIndex < dataSource.count - 1 {
            currentTestIndex += 1
            dataSource[currentTestIndex].state = .started
            startNextTest()
            
            tableView.reloadRows(at: [IndexPath(row: currentTestIndex, section: 0)], with: .none)
            tableView.reloadRows(at: [IndexPath(row: currentTestIndex-1, section: 0)], with: .none)
        } else {
            checkFailedStatus()
            tableView.reloadRows(at: [IndexPath(row: currentTestIndex, section: 0)], with: .none)
        }
    }
    
    func testFailed(error: Error?) {
        dataSource[currentTestIndex].testResultImage = #imageLiteral(resourceName: "crosssIcon")
        testPass()
    }
    
    func checkFailedStatus() {
        let testResult = MHCResult.shared.deviceDetails
        if mhcTestCategory == .otherTests {
            if [testResult.Q_WIFI.testResult, testResult.Q_BLUETOOTH.testResult, testResult.Q_RAM.testResult, testResult.Q_STORAGE.testResult, testResult.Q_ANTENNA.testResult].contains(HealthCheckStatus.result_fail.rawValue) {
                openRetryScreen()
            } else {
                testFinished()
            }
        } else {
            timer?.invalidate()
            if [testResult.Q_VOLUMEUPBUTTON.testResult, testResult.Q_VOLUMEDOWNBUTTON.testResult].contains(HealthCheckStatus.result_fail.rawValue) {
                openRetryScreen()
            } else {
                testFinished()
            }
        }
    }
    
    private func openRetryScreen() {
        let vc = RetryTestVC()
        
        if mhcTestCategory == .otherTests {
            vc.testTitle = MhcStrings.OtherTest.retryTitle
        } else {
            vc.testTitle = MhcStrings.VolumeButton.startTitle
        }
        
        isReloadScreen = false
        vc.delegate = self
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension OtherTestVC: RetryTestDelegate {
    
    func tappedOnRetry() {
        isReloadScreen = true
        currentTestIndex = 0
        populateTestData()
        tableView.reloadData()
        startNextTest()
    }
    
    func tappedOnSkipped() {
        isReloadScreen = true
        dismiss(animated: true) {
            self.testDelegate?.testFinished()
        }
    }
}
