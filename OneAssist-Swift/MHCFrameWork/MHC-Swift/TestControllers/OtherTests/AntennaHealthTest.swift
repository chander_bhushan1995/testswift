//
//  AntennaHealthTest.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 23/07/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import CoreTelephony

class AntennaHealthTest: BaseHealthTest {

    override func startHealthTest() {
        super.startHealthTest()
        
        let netInfo = CTTelephonyNetworkInfo().subscriberCellularProvider
        if let _ = netInfo?.mobileNetworkCode, let _ = netInfo?.mobileCountryCode {
            testSuccess()
        } else {
            getTestResult(result: .result_fail)
        }
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_ANTENNA.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_ANTENNA.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_ANTENNA.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_ANTENNA.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
        healthTestDelegate = nil
    }
    
}
