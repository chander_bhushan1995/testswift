//
//  OtherHealthTest.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 13/06/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class OtherHealthTest: BaseHealthTest {
    
    var healthTest = BaseHealthTest()
    unowned var context: OtherTestVC
    var timer: Timer?
    var totalTime = 10
    var volTest: VolumeHealthTest!
    
    init(vc: OtherTestVC) {
        self.context = vc
    }
    
    func startTest(test: MhcOtherTests) {
        totalTime = 10
        timer?.invalidate()
        switch test {
        case .wifi:
            healthTest = WifiHealthTest()
            healthTest.healthTestDelegate = context
            testWifi()
        case .bluetooth:
            healthTest = BluetoothHealthTest()
            healthTest.healthTestDelegate = context
            testBluetooth()
        case .ram:
            healthTest = RamHealthTest()
            healthTest.healthTestDelegate = context
            testRam()
        case .storage:
            healthTest = StorageHealthTest()
            healthTest.healthTestDelegate = context
            testStorage()
        case .volumeUp:
            volTest = VolumeHealthTest(testType: .volumeUp)
            healthTest = volTest
            healthTest.healthTestDelegate = context
            healthTest.startHealthTest()
            startTimer()
        case .volumeDown:
            healthTest = volTest
            healthTest.healthTestDelegate = context
            volTest.testType = .volumeDown
            startTimer()
        case .antenna:
            healthTest = AntennaHealthTest()
            healthTest.healthTestDelegate = context
            healthTest.startHealthTest()
        case .battery:
            healthTest = BatteryHealthTest()
            healthTest.healthTestDelegate = context
            healthTest.startHealthTest()
        }
    }
    
    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(1.0), target: self, selector: #selector(timerStarted), userInfo: nil, repeats: true)
    }
    
    @objc func timerStarted() {
        totalTime -= 1
        if totalTime <= 0  {
            timer?.invalidate()
            healthTest.getTestResult(result: .result_fail)
        }
    }
    
    func testWifi() {
        if let wifiHealthTest = healthTest as? WifiHealthTest {
            if wifiHealthTest.checkWiFi() {
                startTimer()
                wifiHealthTest.startHealthTest()
            } else {
                context.showAlert(title: MhcStrings.AlertMessage.wifiTitle, message: MhcStrings.AlertMessage.wifi, primaryButtonTitle: MhcStrings.Buttons.continue, secondaryButtonTitle: MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                    self.startTimer()
                    wifiHealthTest.startHealthTest()
                }) {
                    wifiHealthTest.getTestResult(result: .result_skipped)
                }
            }
        }
    }
    
    func testBluetooth() {
        if let bluetoothHealthTest = healthTest as? BluetoothHealthTest {
            bluetoothHealthTest.checkBluetooth(handler: { (isOn) in
                if isOn {
                    self.startTimer()
                    bluetoothHealthTest.startHealthTest()
                } else {
                    self.context.showAlert(title: MhcStrings.AlertMessage.bluetoothTitle, message: MhcStrings.AlertMessage.bluetooth, primaryButtonTitle: MhcStrings.Buttons.continue, secondaryButtonTitle: MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                        self.startTimer()
                        bluetoothHealthTest.startHealthTest()
                    }) {
                        bluetoothHealthTest.getTestResult(result: .result_skipped)
                    }
                }
            })
        }
    }
    
    func testRam() {
        healthTest.startHealthTest()
    }
    
    func testStorage() {
        healthTest.startHealthTest()
    }
}
