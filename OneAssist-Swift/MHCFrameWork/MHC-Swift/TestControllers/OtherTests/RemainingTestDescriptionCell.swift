//
//  RemainingTestDescriptionCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class RemainingTestDescriptionCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var testName: H3RegularGreyLabel!
    @IBOutlet weak var testDesc: BodyTextRegularGreyLabel!
    
    var mhcTestCategory: MHCTestCategory = .otherTests
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setViewModel(_ model: MHCTestDescriptionModel) {
        // write logic here
        if mhcTestCategory == .button {
            testDesc?.removeFromSuperview()
        }
        
        imgView.image = model.image
        testName.text = model.testName
        testDesc?.text = model.testDescription
    }
}
