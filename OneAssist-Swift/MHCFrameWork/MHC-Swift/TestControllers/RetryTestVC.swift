//
//  RetryTestVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 17/06/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol RetryTestDelegate: class {
    func tappedOnRetry()
    func tappedOnSkipped()
}

class RetryTestVC: UIViewController {
    
    @IBOutlet weak var labelTitle: H2BoldLabel!
    @IBOutlet weak var labelSubtitle: BodyTextRegularGreyLabel!
    
    var testTitle: String = ""
    weak var delegate: RetryTestDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        labelTitle.text = "Oops! \(testTitle) test failed."
    }
    
    
    @IBAction func clickedOnRetry(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .MHCRetry, [.location: BuybackQuestionManager.shared.isFromBuyback ? Event.EventParams.buyback : Event.EventParams.mhcHome, .screenName: testTitle])
        dismiss(animated: true) {
            self.delegate?.tappedOnRetry()
        }
    }
    
    @IBAction func clickedOnSkipped(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.tappedOnSkipped()
        }
    }
}
