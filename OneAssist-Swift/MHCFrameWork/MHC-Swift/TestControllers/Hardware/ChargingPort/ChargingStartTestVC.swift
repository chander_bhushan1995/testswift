//
//  BatteryTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class ChargingStartTestVC: TestStartVC {
    
    var spinnerView: SpinnerView?
    fileprivate let chargingTest = ChargingHealthTest()
    private var imgViewCharger = UIImageView()
    private var isFirstLoad = true
    fileprivate var isNotTopVC = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isNotTopVC {
            openRetryScreen()
            isNotTopVC = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirstLoad {
            isFirstLoad = false
            addAnimationView()
        }
    }
    
    private func addAnimationView() {
        imageViewTest.image = #imageLiteral(resourceName: "deviceCharger")
        outerView.setNeedsLayout()
        outerView.layoutIfNeeded()
        
        imgViewCharger = UIImageView(frame: imageViewTest.frame)
        imgViewCharger.image = #imageLiteral(resourceName: "charger-1")
        imgViewCharger.contentMode = .scaleAspectFit
        outerView.addSubview(imgViewCharger)
        imgViewCharger.transform = CGAffineTransform(translationX: 0.0, y: 20.0)
        outerView.addSubview(imageViewTest)
        startAnimation()
    }
    
    private func startAnimation() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [], animations: { [weak self] in
            self?.imgViewCharger.transform = CGAffineTransform(translationX: 0.0, y: -3.0)
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
                self?.imgViewCharger.transform = CGAffineTransform(translationX: 0.0, y: 20.0)
                self?.startAnimation()
            }
        }
    }
    
    func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_charging")
        labelTestName.text = MhcStrings.ChargingPort.startTitle
        labelTestDescription.text = MhcStrings.ChargingPort.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        checkCharger()
    }
    
    fileprivate func checkCharger() {
        chargingTest.healthTestDelegate = self
        if chargingTest.isChargerConnected() {
            startTimer()
            chargingTest.startHealthTest()
        } else {
            showAlert(title: MhcStrings.AlertMessage.chargerTitle, message: MhcStrings.AlertMessage.charger, primaryButtonTitle: MhcStrings.Buttons.gotIt, nil, primaryAction: {
                
                self.viewTestStatus.isHidden = false
                self.spinnerView = SpinnerView.init(view: self.viewTestStatus)
                self.spinnerView?.startAnimating()
                
                self.startTimer()
                self.chargingTest.startHealthTest()
            }, nil)
        }
    }
    
    override func testTimerStarted() {
        super.testTimerStarted()
        if totalTime <= 0  {
            chargingTest.testTimeout()
        }
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_CHARGINGPORT.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
    
}

extension ChargingStartTestVC: HealthTestDelegate {
    
    func testFinished() {
        testTimer?.invalidate()
        self.testDelegate?.testFinished()
    }
    
    func testPass() {
        spinnerView?.stopAnimating()
        testTimer?.invalidate()
        viewTestStatus.isHidden = false
        viewSuccess.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.testFinished()
        }
    }
    
    func testFailed(error: Error?) {
        testTimer?.invalidate()
        openRetryScreen()
    }
    
    func openRetryScreen() {
        if (Utilities.topMostPresenterViewController as? MHCMainVC)?.children.first?.children.first is ChargingStartTestVC {
            let vc = RetryTestVC()
            vc.testTitle = MhcStrings.ChargingPort.startTitle
            vc.delegate = self
            self.presentInFullScreen(vc, animated: true, completion: nil)
        } else {
            isNotTopVC = true
        }
    }
}

extension ChargingStartTestVC: RetryTestDelegate {
    
    func tappedOnRetry() {
        totalTime = 20
        startTimer()
        chargingTest.startHealthTest()
    }
    
    func tappedOnSkipped() {
        self.testDelegate?.testFinished()
    }
}
