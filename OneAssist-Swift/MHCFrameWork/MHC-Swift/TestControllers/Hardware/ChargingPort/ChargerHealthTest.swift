//
//  ChargerHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 14/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

class ChargingHealthTest: BaseHealthTest {
    
    private var timer: Timer?
    
    func isChargerConnected() -> Bool {
        return checkBatteryStatus()
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(checkBatteryStatus), userInfo: nil, repeats: true)
    }
    
    @objc private func checkBatteryStatus() -> Bool {
        let myDevice = UIDevice.current
        UIDevice.current.isBatteryMonitoringEnabled = true
        MHCResult.shared.deviceDetails.Q_CHARGINGPORT.batteryLevel = myDevice.batteryLevel
        
        let batteryState = myDevice.batteryState
        var flag = false
        
        switch batteryState {
        case .charging, .full:
            testSuccess()
            timer?.invalidate()
            flag = true
        case .unplugged, .unknown:
            break
        }
        UIDevice.current.isBatteryMonitoringEnabled = false
        return flag
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_CHARGINGPORT.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_CHARGINGPORT.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    func testTimeout() {
        timer?.invalidate()
        MHCResult.shared.deviceDetails.Q_CHARGINGPORT.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_CHARGINGPORT.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
    }
}
