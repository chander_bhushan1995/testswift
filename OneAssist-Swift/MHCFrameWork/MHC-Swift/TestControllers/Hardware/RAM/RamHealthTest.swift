//
//  RamHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 12/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

class RamHealthTest: BaseHealthTest {
    
    override func startHealthTest() {
        super.startHealthTest()
        let ramSize = ProcessInfo.processInfo.physicalMemory/1024
        
        guard ramSize > 0 else {
            getTestResult(result: .result_fail)
            return
        }
        
        MHCResult.shared.deviceDetails.Q_RAM.totalRam = ramSize
        var host_port: mach_port_t
        var host_size: mach_msg_type_number_t
        var pagesize = vm_size_t()
        
        host_port = mach_host_self()
        host_size = mach_msg_type_number_t(MemoryLayout<vm_statistics_data_t>.size / MemoryLayout<integer_t>.size)
        host_page_size(host_port, &pagesize)
        
        var vm_stat = vm_statistics_data_t()
        
        let err: kern_return_t = withUnsafeMutableBytes(of: &vm_stat) {
            let boundBuffer = $0.bindMemory(to: Int32.self)
            return host_statistics(host_port, HOST_VM_INFO, boundBuffer.baseAddress, &host_size)
        }
        
        if err != KERN_SUCCESS {
            // Error, failed to get Virtual memory info
            getTestResult(result: .result_fail)
            return
        }
        
        let usedMemory = UInt64(vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * UInt64(pagesize)
        let freeMemory = UInt64(vm_stat.free_count) * UInt64(pagesize)
        let totalMem = usedMemory + freeMemory
        freeMemory > 0 ? testSuccess() : getTestResult(result: .result_fail)
        
        print("used: \(usedMemory) free: \(freeMemory) total: \(totalMem)")
        
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_RAM.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_RAM.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_RAM.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_RAM.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
        healthTestDelegate = nil
    }

}
