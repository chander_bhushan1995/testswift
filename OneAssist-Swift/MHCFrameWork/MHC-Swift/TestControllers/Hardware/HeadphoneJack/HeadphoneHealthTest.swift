//
//  HeadphoneHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 14/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import AVFoundation

class HeadphoneHealthTest: BaseHealthTest {
    
    private var timer: Timer?
    
    func isHeadphoneConnected() -> Bool {
        return !AVAudioSession.sharedInstance().currentRoute.outputs.filter { $0.portType == AVAudioSession.Port.headphones }.isEmpty
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(checkHeadphone), userInfo: nil, repeats: true)
    }
    
    @objc private func checkHeadphone() {
        if isHeadphoneConnected() {
            testSuccess()
        }
    }
    
    private func testSuccess() {
        timer?.invalidate()
        MHCResult.shared.deviceDetails.Q_HEADPHONEJACK.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_HEADPHONEJACK.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    func testTimeout() {
        timer?.invalidate()
        MHCResult.shared.deviceDetails.Q_HEADPHONEJACK.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_HEADPHONEJACK.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
    }
    
}
