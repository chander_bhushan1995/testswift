//
//  HeadphoneTestStartVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class HeadphoneTestStartVC: TestStartVC {
    
    var spinnerView: SpinnerView?
    fileprivate let headphoneTest = HeadphoneHealthTest()
    private var imgViewCharger = UIImageView()
    private var isFirstLoad = true
    fileprivate var isNotTopVC = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isNotTopVC {
            openRetryScreen()
            isNotTopVC = false
        }
    }
    
    func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_earphones")
        labelTestName.text = MhcStrings.Headphone.startTitle
        labelTestDescription.text = MhcStrings.Headphone.startDescription
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirstLoad {
            isFirstLoad = false
            addAnimationView()
        }
    }
    
    private func addAnimationView() {
        imageViewTest.image = #imageLiteral(resourceName: "earphoneDevice")
        outerView.setNeedsLayout()
        outerView.layoutIfNeeded()
        
        imgViewCharger = UIImageView(frame: imageViewTest.frame)
        imgViewCharger.image = #imageLiteral(resourceName: "earphones")
        imgViewCharger.contentMode = .scaleAspectFit
        outerView.addSubview(imgViewCharger)
        imgViewCharger.transform = CGAffineTransform(translationX: 0.0, y: 20.0)
        outerView.addSubview(imageViewTest)
        startAnimation()
    }
    
    private func startAnimation() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [], animations: { [weak self] in
            self?.imgViewCharger.transform = CGAffineTransform(translationX: 0.0, y: -3.0)
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
                self?.imgViewCharger.transform = CGAffineTransform(translationX: 0.0, y: 20.0)
                self?.startAnimation()
            }
        }
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        
        headphoneTest.healthTestDelegate = self
        
        if headphoneTest.isHeadphoneConnected() {
            startTimer()
            headphoneTest.startHealthTest()
        } else {
            showAlert(title: MhcStrings.AlertMessage.headphoneTitle, message: MhcStrings.AlertMessage.headphone, primaryButtonTitle: MhcStrings.Buttons.gotIt, nil, primaryAction: {
                
                self.viewTestStatus.isHidden = false
                self.spinnerView = SpinnerView.init(view: self.viewTestStatus)
                self.spinnerView?.startAnimating()
                
                self.startTimer()
                self.headphoneTest.startHealthTest()
            }, nil)
        }
    }
    
    override func testTimerStarted() {
        super.testTimerStarted()
        if totalTime <= 0  {
            headphoneTest.testTimeout()
        }
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_HEADPHONEJACK.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
}

extension HeadphoneTestStartVC: HealthTestDelegate {
    
    func testFinished() {
        testTimer?.invalidate()
        self.testDelegate?.testFinished()
    }
    
    func testPass() {
        spinnerView?.stopAnimating()
        viewTestStatus.isHidden = false
        testTimer?.invalidate()
        viewSuccess.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.testFinished()
        }
    }
    
    func testFailed(error: Error?) {
        testTimer?.invalidate()
        openRetryScreen()
    }
    
    func openRetryScreen() {
        if (Utilities.topMostPresenterViewController as? MHCMainVC)?.children.first?.children.first is HeadphoneTestStartVC {
            let vc = RetryTestVC()
            vc.testTitle = MhcStrings.Headphone.startTitle
            vc.delegate = self
            self.presentInFullScreen(vc, animated: true, completion: nil)
        } else {
            isNotTopVC = true
        }
    }
}

extension HeadphoneTestStartVC: RetryTestDelegate {
    
    func tappedOnRetry() {
        totalTime = 20
        startTimer()
        headphoneTest.startHealthTest()
    }
    
    func tappedOnSkipped() {
        self.testDelegate?.testFinished()
    }
}
