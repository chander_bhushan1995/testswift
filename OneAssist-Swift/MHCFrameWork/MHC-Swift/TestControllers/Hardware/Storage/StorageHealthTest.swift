//
//  StorageHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 15/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

class StorageHealthTest: BaseHealthTest {
    
    private var fileName = "test1.txt"
    private var dataString = "OneAssist1 Tesing string"
    
    override func startHealthTest() {
        super.startHealthTest()
        writeFile()
    }
    
    private func writeFile() {
        do {
            let fileManager = FileManager.default
            var filePath = try  fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            filePath.appendPathComponent(fileName)
            
            if fileManager.fileExists(atPath: filePath.path) {
                try fileManager.removeItem(at: filePath)
            }
            
            let data = dataString.data(using: .utf8) ?? Data()
            
            let startedTime = Date()
            fileManager.createFile(atPath: filePath.path, contents: nil, attributes: nil)
            let fileHandle = try FileHandle(forWritingTo: filePath)
            defer {
                fileHandle.closeFile()
            }
            
            for _ in 0..<500 {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
            }
            
            let exeTime = startedTime.timeIntervalTillNowInMilliSeconds()
            let writtingSpeed = Double(data.count*500)/exeTime
            
            print("Writting speed: \(writtingSpeed)")
            if writtingSpeed > 0 {
                readFile()
            } else {
                getTestResult(result: .result_fail)
            }
        } catch {
            getTestResult(result: .result_fail)
        }
    }
    
    private func readFile() {
        do {
            let fileManager = FileManager.default
            var filePath = try  fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            filePath.appendPathComponent(fileName)
            
            let startedTime = Date()
            let dataString = try String(contentsOf: filePath, encoding: .utf8)
            let exeTime = startedTime.timeIntervalTillNowInMilliSeconds()
            let size = dataString.data(using: .utf8)?.count ?? 0
            let readingSpeed = Double(size)/exeTime
            print("reading speed: \(readingSpeed)")
            if readingSpeed > 0 {
                testSuccess()
            } else {
                getTestResult(result: .result_fail)
            }
        } catch {
            getTestResult(result: .result_fail)
        }
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_STORAGE.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_STORAGE.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_STORAGE.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_STORAGE.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
        healthTestDelegate = nil
    }
    
}
