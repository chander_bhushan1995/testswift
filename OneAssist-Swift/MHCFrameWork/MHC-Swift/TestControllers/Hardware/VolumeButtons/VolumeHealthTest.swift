//
//  VolumeHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 11/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import AVFoundation

class VolumeHealthTest: BaseHealthTest {
    
    private var previousValue: Float = AVAudioSession.sharedInstance().outputVolume
    var testType: MhcOtherTests = .volumeUp
    
    init(testType:MhcOtherTests) {
        self.testType = testType
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        
        try? AVAudioSession.sharedInstance().setActive(true)
        previousValue = AVAudioSession.sharedInstance().outputVolume
//        try? AVAudioSession.sharedInstance().setActive(false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(volumeCallback(notification:)), name: NSNotification.Name("AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
    }
    
    @objc private func volumeCallback(notification: NSNotification) {
        guard UIApplication.shared.applicationState == .active else {
            return
        }
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    print("value changed")

                    let level = userInfo["AVSystemController_AudioVolumeNotificationParameter"] as? Float
                    guard let volLevel = level else {
                        return
                    }
                    if self.testType == .volumeUp && (volLevel > self.previousValue || (self.previousValue == 1.0 && self.previousValue == volLevel)) {
                        self.voulumeUpTestSuccess()
                    } else if self.testType == .volumeDown && (volLevel < self.previousValue || (self.previousValue == 0.0 && self.previousValue == volLevel)) {
                        self.voulumeDownTestSuccess()
                    }
                    self.previousValue = volLevel
                }
            }
        }
    }
    
    private func voulumeUpTestSuccess() {
        MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
    }
    
    private func voulumeDownTestSuccess() {
        MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        super.getTestResult(result: result)
        if testType == .volumeUp {
            MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON.testResult = result.rawValue
            MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON.testTime = getTimeConsumedInTest()
        } else {
            MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON.testResult = result.rawValue
            MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON.testTime = getTimeConsumedInTest()
        }
        healthTestDelegate?.testFailed(error: nil)
    }
    
    deinit {
        try? AVAudioSession.sharedInstance().setActive(false)
        NotificationCenter.default.removeObserver(self, name:  NSNotification.Name("AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
    }
}
