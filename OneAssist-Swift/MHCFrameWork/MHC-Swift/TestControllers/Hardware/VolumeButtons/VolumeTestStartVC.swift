//
//  VolumeTestStartVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 11/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class VolumeTestStartVC: TestStartVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_buttons")
        labelTestName.text = MhcStrings.VolumeButton.startTitle
        labelTestDescription.text = MhcStrings.VolumeButton.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        let vc = OtherTestVC()
        vc.mhcTestCategory = .button
        vc.testDelegate = testDelegate
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
}
