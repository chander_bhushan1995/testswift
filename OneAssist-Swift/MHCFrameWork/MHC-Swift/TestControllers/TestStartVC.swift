//
//  BaseTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class TestStartVC: UIViewController {

    @IBOutlet weak var labelTestName: UILabel!
    @IBOutlet weak var labelTestDescription: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imageViewTest: UIImageView!
    @IBOutlet weak var buttonStartTest: UIButton!
    @IBOutlet weak var viewTestStatus: UIView!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var labelSuccess: H3BoldLabel!
    
    weak var testDelegate: TestResultDelegate?
    var testName = ""
    var testTimer: Timer?
    var totalTime = 20
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "TestStartVC", bundle: Bundle(for: type(of: self)))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTestName.text = testName
        labelSuccess.textColor = UIColor.seaGreen
    }

    func startTimer() {
        testTimer?.invalidate()
        testTimer = Timer.scheduledTimer(timeInterval: TimeInterval(1.0), target: self, selector: #selector(testTimerStarted), userInfo: nil, repeats: true)
    }
    
    @objc func testTimerStarted() {
        totalTime -= 1
        if totalTime <= 0  {
            testTimer?.invalidate()
        }
    }
    
    @IBAction func clickedBtnStartTest(_ sender: Any) { }
    
    @IBAction func clickedBtnSkipTest(_ sender: Any) {
        testDelegate?.testFinished()
    }
}
