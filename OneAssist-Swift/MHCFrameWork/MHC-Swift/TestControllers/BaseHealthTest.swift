//
//  BaseHealthTest.Swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 23/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation

protocol HealthTestDelegate: class {
    func testFinished()
    func testPass()
    func testFailed(error: Error?)
    func testStarted()
    func testCompleted(_ percentageRemaining:Int)
}

extension HealthTestDelegate {
    func testPass() {}
    func testFailed(error: Error?) {
        print("error in failed method")
    }
    func testStarted() {}
    func testCompleted(_ percentageRemaining:Int) {}
}

class BaseHealthTest: NSObject {
    
    weak var healthTestDelegate: HealthTestDelegate?
    var startedTime: Date
    
    override init() {
        startedTime = Date()
        super.init()
    }
    
    func startHealthTest() {
       
    }
    
    func getTimeConsumedInTest() -> Double {
        return startedTime.timeIntervalTillNowInMilliSeconds()
    }
    
    func getTestResult(result: HealthCheckStatus) {}

}
