//
//  CommonTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 23/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class CommonTestVC: BaseTestVC {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelRemainingTime: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var viewForBackImage: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelSuccess: H3BoldLabel!
    @IBOutlet weak var labelInstruction: H3RegularBlackLabel!
    
    var isReloadView = true
    var testName = ""
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "CommonTestVC", bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelSuccess.textColor = UIColor.seaGreen
    }
    
    override func timerStarted() {
        super.timerStarted()
        labelRemainingTime.text = "\(totalTime)s"
    }

}

extension CommonTestVC: HealthTestDelegate {
    
    @objc func testFinished() {
        timer?.invalidate()
        dismiss(animated: true) {
            self.testDelegate?.testFinished()
        }
    }
    
    @objc func testFailed(error: Error?) {
        openRetryScreen()
    }
    
    @objc func openRetryScreen() {
        let vc = RetryTestVC()
        vc.testTitle = testName
        vc.delegate = self
        isReloadView = false
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }

    @objc func testPass() {
        timer?.invalidate()
        labelRemainingTime.isHidden = true
        viewSuccess.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.testFinished()
        }
    }
}

extension CommonTestVC: RetryTestDelegate {
    
   @objc func tappedOnRetry() {
        isReloadView = true
    }
    
   @objc func tappedOnSkipped() {
        isReloadView = true
        dismiss(animated: true) {
            self.testDelegate?.testFinished()
        }
    }
}
