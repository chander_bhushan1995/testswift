//
//  WifiHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 15/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import Reachability
import ReachabilityManager
class WifiHealthTest: BaseHealthTest, ConnectionListener {
    var reachability: Reachability?
    
    override init() {
        do {
            self.reachability = try Reachability.init()
        } catch {
            print("Unable to create Reachability")
            return
        }
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        if checkWiFi() {
            getTestResult(result: .result_pass)
        } else {
            ReachabilityManager.shared.addListener(listener: self)
        }
    }
    
    func checkWiFi() -> Bool {
        
        let networkStatus = reachability?.connection
        switch networkStatus {
        case .unavailable:
            return false
        case .cellular: // For Mobile data
            print("Connected via WWAN")
            return false
        case .wifi: // For Wifi
            print("Connected via WiFi")
            getTestResult(result: .result_pass)
            return true
        default:
            return false
        }
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_WIFI.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_WIFI.testTime = getTimeConsumedInTest()
        if result == .result_pass {
            healthTestDelegate?.testPass()
        } else {
            healthTestDelegate?.testFailed(error: nil)
        }
        healthTestDelegate = nil
        ReachabilityManager.shared.removeListener(listener: self)
    }
    
    func connectionChanged(status: Reachability.Connection) {
        if checkWiFi() {
            getTestResult(result: .result_pass)
        }
     }
}

