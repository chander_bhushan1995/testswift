//
//  GPSHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 08/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import CoreLocation

class GPSHealthTest: BaseHealthTest {
    
    private var locationManager = Permission.shared.locationManager
    
    deinit {
        Permission.shared.resetData()
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        if let coordinates = Permission.shared.location?.coordinate, CLLocationCoordinate2DIsValid(coordinates) {
            MHCResult.shared.deviceDetails.Q_GPS.latitude = coordinates.latitude
            MHCResult.shared.deviceDetails.Q_GPS.longitude = coordinates.longitude
            getTestResult(result: .result_pass)
        } else {
            Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.mhcGPS) { (status) in
                if status == .authorizedAlways || status == .authorizedWhenInUse {
                    self.findUserLocation()
                } else if status == .restricted {
                    self.getTestResult(result: .result_fail)
                }
            }
        }
    }
    
    private func findUserLocation() {
        locationManager.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_GPS.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_GPS.testTime = getTimeConsumedInTest()
        if result == .result_pass {
            healthTestDelegate?.testPass()
        } else if result == .result_fail {
            healthTestDelegate?.testFailed(error: nil)
        } else {
            healthTestDelegate?.testFinished()
        }
        healthTestDelegate = nil
    }
        
}

extension GPSHealthTest: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last
        if let coordinates = location?.coordinate, CLLocationCoordinate2DIsValid(coordinates) {
            MHCResult.shared.deviceDetails.Q_GPS.latitude = coordinates.latitude
            MHCResult.shared.deviceDetails.Q_GPS.longitude = coordinates.longitude
            getTestResult(result: .result_pass)
        } else {
            getTestResult(result: .result_fail)
        }
        Permission.shared.location = location
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        getTestResult(result: .result_fail)
    }
    
}
