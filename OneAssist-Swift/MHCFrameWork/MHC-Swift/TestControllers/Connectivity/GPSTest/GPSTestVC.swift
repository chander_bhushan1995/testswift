//
//  GPSTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 08/02/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class GPSTestVC: CommonTestVC {

    private let gpsTest = GPSHealthTest()
    private var locationImageView: UIImageView?
    
    deinit {
        Permission.shared.resetData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isReloadView {
            checkPermission()
        }
    }
    
    private func checkPermission() {
        Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.mhcGPS) {[weak self] (status) in
            guard let self = self else {return}
            
            self.gpsTest.healthTestDelegate = self
            switch status {
                
            case .denied:
                self.addObserverActiveApp()
                
                Utilities.showAlertForSetting(on: self, title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting, secondAction: {
                    self.removeObserverActiveApp()
                    self.gpsTest.getTestResult(result: .result_skipped)
                    
                    EventTracking.shared.eventTracking(name: .locationSetting, [.location: Event.EventParams.mhcGPS, .action: Event.EventParams.close])
                }) {
                    //primary button tapped
                    EventTracking.shared.eventTracking(name: .locationSetting, [.location: Event.EventParams.mhcGPS, .action: Event.EventParams.settings])
                }
            default:
                self.startTimer()
                self.gpsTest.startHealthTest()
            }
        }
    }
    
    override func appBecomeActive() {
        super.appBecomeActive()
        checkPermission()
    }
    
    private func addLocationView() {
        let width = UIScreen.main.bounds.width - (50*2)
        
        if locationImageView == nil {
            locationImageView = UIImageView(frame: CGRect(x: width/2 - 35 , y: width/2 - 88 + (42/2), width: 70, height: 88))
            viewForBackImage.addSubview(locationImageView!)
        }
        
        locationImageView?.contentMode = .scaleAspectFit
        locationImageView?.image = #imageLiteral(resourceName: "gps")
    }
    
    private func initializeView() {
        testName = MhcStrings.GPS.startTitle
        imageView.image = #imageLiteral(resourceName: "landscape")
        addLocationView()
        labelTitle.text = MhcStrings.GPS.fetchingLocation
        labelDescription.text = MhcStrings.waitAMoment
        totalTime = 20
        labelRemainingTime.text = "\(totalTime)s"
    }
    
    private func animateLocationView() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 15, initialSpringVelocity: 10, animations: {
            self.locationImageView?.frame.origin.y -= 40
            self.locationImageView?.alpha = 0.3
        }) { _ in
            UIView.animate(withDuration: 0.5, delay: 0.05, options: [.autoreverse,.repeat], animations: {
                self.locationImageView?.frame.origin.y += 40
                self.locationImageView?.alpha = 1
            })
        }
    }
    
    override func timerStarted() {
        super.timerStarted()
        animateLocationView()
        if totalTime <= 0  {
            gpsTest.getTestResult(result: .result_fail)
        }
    }
    
    override func tappedOnRetry() {
        super.tappedOnRetry()
        initializeView()
        checkPermission()
    }

}

