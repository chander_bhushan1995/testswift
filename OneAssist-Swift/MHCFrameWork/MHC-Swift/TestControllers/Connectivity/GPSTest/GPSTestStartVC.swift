//
//  GPSTestStartVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 21/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class GPSTestStartVC: TestStartVC {
    
    deinit {
        Permission.shared.resetData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        imageViewTest.image = #imageLiteral(resourceName: "ic_gps")
        labelTestName.text = MhcStrings.GPS.startTitle
        labelTestDescription.text = MhcStrings.GPS.startDescription
    }
    
    override func clickedBtnStartTest(_ sender: Any) {
        super.clickedBtnStartTest(sender)
        checkPermission()
    }
    
    private func checkPermission() {
        Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.mhcGPS) {[weak self] (status) in
            guard let self = self else {return}
            switch status {
            case .denied:
                
                Utilities.showAlertForSetting(on: self, title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting, secondAction: {
                    EventTracking.shared.eventTracking(name: .locationSetting, [.location: Event.EventParams.mhcGPS, .action: Event.EventParams.close])
                    self.clickedBtnSkipTest(self.buttonStartTest)
                }, primaryAction: {
                    EventTracking.shared.eventTracking(name: .locationSetting, [.location: Event.EventParams.mhcGPS, .action: Event.EventParams.settings])
                })
            default:
                let vc = GPSTestVC()
                vc.testDelegate = self.testDelegate
                self.presentInFullScreen(vc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction override func clickedBtnSkipTest(_ sender: Any) {
        MHCResult.shared.deviceDetails.Q_GPS.testResult = HealthCheckStatus.result_skipped.rawValue
        super.clickedBtnSkipTest(sender)
    }
    
}
