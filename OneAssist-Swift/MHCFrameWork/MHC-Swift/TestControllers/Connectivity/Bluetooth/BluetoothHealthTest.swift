//
//  BluetoothHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 15/03/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import CoreBluetooth

class BluetoothHealthTest: BaseHealthTest {

    typealias handler = (_ isOnline: Bool) -> Void
    fileprivate lazy var manager: CBCentralManager = CBCentralManager()
    fileprivate var bluetoothStatus: handler?
    
    func checkBluetooth(handler: @escaping handler) {
        bluetoothStatus = handler
        manager.delegate = self
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        if manager.state == .poweredOn {
            getTestResult(result: .result_pass)
        }
    }
    
    override func getTestResult(result: HealthCheckStatus) {
        MHCResult.shared.deviceDetails.Q_BLUETOOTH.testResult = result.rawValue
        MHCResult.shared.deviceDetails.Q_BLUETOOTH.testTime = getTimeConsumedInTest()
        if result == .result_pass {
            healthTestDelegate?.testPass()
        } else {
            healthTestDelegate?.testFailed(error: nil)
        }
        healthTestDelegate = nil
    }
}

extension BluetoothHealthTest: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            getTestResult(result: .result_pass)
            bluetoothStatus?(true)
            bluetoothStatus = nil
        case .poweredOff:
            bluetoothStatus?(false)
            bluetoothStatus = nil
        case .unknown:
            break
        default:
            getTestResult(result: .result_fail)
        }
    }
}
