//
//  VibrationTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 30/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class VibrationTestVC: CommonTestVC {
    
    private var vibrationTest = VibrationHealthTest()

    private var lock = 0
    private var count = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func timerStarted() {
        super.timerStarted()
        if totalTime <= 0 {
            vibrationTest.testFailed()
        }
    }
    
    private func initializeView() {
        testName = MhcStrings.Vibration.startTitle
        imageView.image = #imageLiteral(resourceName: "nonVibMobile")
        labelTitle.text = MhcStrings.Vibration.inProgress
        labelDescription.text = MhcStrings.waitAMoment
        vibrationTest.healthTestDelegate = self
        showFlatSurfaceAlert()
    }
    
    private func showFlatSurfaceAlert() {
        vibrationTest.checkDeviceIsOnFlatSurface { (flag) in
            if flag {
                self.totalTime = 10
                self.labelRemainingTime.text = "\(self.totalTime)s"
                self.startTimer()
                if self.lock == 0 {
                    self.count -= 1
                    self.animateMobile()
                    self.lock += 1
                }
                self.vibrationTest.startHealthTest()
            } else {
                self.showAlert(title: MhcStrings.AlertMessage.vibrationTitle, message: MhcStrings.AlertMessage.vibration, primaryButtonTitle: MhcStrings.Buttons.continue, secondaryButtonTitle: MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                    self.showFlatSurfaceAlert()
                }) {
                    self.vibrationTest.testSkipped()
                }
            }
        }
    }
    
    func rotate(duration: TimeInterval, delay: TimeInterval) {
        UIView.animate(withDuration: duration, delay: delay, options: [], animations: {
            self.imageView.image = #imageLiteral(resourceName: "vibratingMobile")
            self.imageView.transform = CGAffineTransform(rotationAngle: 10.0 * CGFloat.pi/180.0 * -1)
        }, completion: { _ in
            UIView.animate(withDuration: duration, delay: 0, options: [], animations: {
                self.imageView.transform = CGAffineTransform(rotationAngle: 20.0 * CGFloat.pi/180.0)
            },completion: { _ in
                UIView.animate(withDuration: duration, delay: 0, options: [], animations: {
                    self.imageView.transform = CGAffineTransform(rotationAngle: 10.0 * CGFloat.pi/180.0 * -1)
                }, completion: { _ in
                    self.imageView.transform = CGAffineTransform(rotationAngle: 0.0 * CGFloat.pi/180.0 * -1)
                    self.imageView.image = #imageLiteral(resourceName: "nonVibMobile")
                    if self.totalTime > 0 && self.count > 0 {
                        self.delay(1) {
                            self.count -= 1
                            self.rotate(duration: 0.1, delay: 0)
                        }
                    } else {
                        self.lock = 0
                        self.count = 4
                    }
                    self.view.layoutIfNeeded()
                })
            })
        })
    }
    
    private func animateMobile() {
        imageView.setAnchorPoint(CGPoint(x: 0.5, y: 0.5))
        rotate(duration: 0.1, delay: 0.1)
    }
    
    override func tappedOnRetry() {
        super.tappedOnRetry()
        initializeView()
        showFlatSurfaceAlert()
    }

}
