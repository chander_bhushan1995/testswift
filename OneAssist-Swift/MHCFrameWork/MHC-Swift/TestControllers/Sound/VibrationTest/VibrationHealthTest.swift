//
//  VibrationHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 30/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import AudioToolbox

class VibrationHealthTest: BaseHealthTest {
    
    private var motionManager = MotionManager.shared
    private var arrXAxisVibration = [Float](),
    arrXAxisNonVibration = [Float](),
    arrYAxisVibration = [Float](),
    arrZAxisVibration = [Float](),
    arrYAxisNonVibration = [Float](),
    arrZAxisNonVibration = [Float]()
    
    override func startHealthTest() {
        arrXAxisVibration = [Float]()
        arrXAxisNonVibration = [Float]()
        arrYAxisVibration = [Float]()
        arrZAxisVibration = [Float]()
        arrYAxisNonVibration = [Float]()
        arrZAxisNonVibration = [Float]()
        super.startHealthTest()
        if Platform.isSimulator {
            testResultNotAvailable()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                self.checkDeviceIsOnFlatSurface(handler: { (flag) in
                    if flag {
                        self.startCollectingVibrationData()
                    } else {
                        self.testFailed()
                    }
                })
            }
        }
    }
    
    func checkDeviceIsOnFlatSurface(handler: ((_ isFlatSurface: Bool) -> Void)?) {
        var completion = handler
        if motionManager.isAccelerometerAvailable {
            motionManager.getAccelerationUpdate { (accData, error) in
                self.motionManager.stopAccelerometerUpdates()
                if let data = accData {
                    completion?((data.0 > -0.1 && data.0 <= 0.1) && (data.1 >= -0.1 && data.1 <= 0.1))
                } else {
                    self.testFailed()
                }
                completion = nil
            }
        } else {
            testResultNotAvailable()
        }
    }
    
    private func startCollectingVibrationData() {

        if motionManager.isAccelerometerAvailable {
            motionManager.updateInterval = 0.01
            motionManager.getAccelerationUpdate { (accData, error) in
                if let data = accData {
                    self.arrXAxisVibration.append(Float(data.0))
                    self.arrYAxisVibration.append(Float(data.1))
                    self.arrZAxisVibration.append(Float(data.2))
                } else {
                    self.testFailed()
                }
            }
        }
        
        AudioServicesPlayAlertSoundWithCompletion(kSystemSoundID_Vibrate) {
            AudioServicesPlayAlertSoundWithCompletion(kSystemSoundID_Vibrate) {
                self.motionManager.stopAccelerometerUpdates()
                DispatchQueue.main.async {
                    self.startCollectingNonVibrationData();
                }
            }
        }
    }
    
    private func startCollectingNonVibrationData() {
        
        if motionManager.isAccelerometerAvailable {
            motionManager.updateInterval = 0.01
            motionManager.getAccelerationUpdate { (accData, error) in
                if let data = accData {
                    if self.arrXAxisNonVibration.count < self.arrXAxisVibration.count {
                        self.arrXAxisNonVibration.append(Float(data.0))
                        self.arrYAxisNonVibration.append(Float(data.1))
                        self.arrZAxisNonVibration.append(Float(data.2))
                    } else if self.arrXAxisNonVibration.count  == self.arrXAxisVibration.count {
                        self.getTestStatus()
                    }
                } else {
                    self.testFailed()
                }
            }
        }
    }
    
    private func getTestStatus() {
        
        var xCalculated: Float = 0, yCalculated: Float = 0, zCalculated: Float = 0
        let critical: Float  = 1.26334034057
        motionManager.stopAccelerometerUpdates()
        // Get varience for all axis
        if let varianceXAxisVibration = calculateVariance(arrXAxisVibration),
            let varianceXAxisNonVibration = calculateVariance(arrXAxisNonVibration) {
            xCalculated = varianceXAxisVibration/varianceXAxisNonVibration
            MHCResult.shared.deviceDetails.Q_VIBRATION.varianceX = varianceXAxisVibration
        }
        
        if let varianceYAxisVibration = calculateVariance(arrYAxisVibration),
            let varianceYAxisNonVibration = calculateVariance(arrYAxisNonVibration) {
            yCalculated = varianceYAxisVibration/varianceYAxisNonVibration
            MHCResult.shared.deviceDetails.Q_VIBRATION.varianceY = varianceYAxisVibration
        }
        
        if let varianceZAxisVibration = calculateVariance(arrZAxisVibration),
            let varianceZAxisNonVibration = calculateVariance(arrZAxisNonVibration) {
            zCalculated = varianceZAxisVibration/varianceZAxisNonVibration
            MHCResult.shared.deviceDetails.Q_VIBRATION.varianceZ = varianceZAxisVibration
        }

        var successCount: Int = 0
        print("x: \(xCalculated)\ny: \(yCalculated)\nz: \(zCalculated)")
        // check if varience is greater then critical
        if xCalculated > critical {
            successCount += 1
        }
        if yCalculated > critical {
            successCount += 1
        }
        if zCalculated > critical {
            successCount += 1
        }
        
        // Check for test status
        if successCount >= 2 {
            testSuccess()
        } else {
            testFailed()
        }
    }
    
    private func calculateVariance(_ arrValues: [Float]) -> Float? {
        guard arrValues.count > 0 else {
            return nil
        }
        let  mean = calculateMean(arrValues)
        var sumOfSquaredDifferences: Float = 0
        for number in arrValues
        {
            let difference = number - mean
            sumOfSquaredDifferences += difference*difference
        }
        return sumOfSquaredDifferences/Float(arrValues.count)
    }
    
    private func calculateMean(_ arrValues: [Float]) -> Float {
        let sum: Float = arrValues.reduce(0.0) { (result, new) -> Float in
            return result + new
        }
        return sum/Float(arrValues.count)
    }
    
    func testFailed() {
        
        MHCResult.shared.deviceDetails.Q_VIBRATION.testResult = HealthCheckStatus.result_fail.rawValue
        MHCResult.shared.deviceDetails.Q_VIBRATION.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
    }
    
    func testSkipped() {
        MHCResult.shared.deviceDetails.Q_VIBRATION.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_VIBRATION.testTime = getTimeConsumedInTest()
        testFinished()
    }
    
    private func testResultNotAvailable() {
        MHCResult.shared.deviceDetails.Q_VIBRATION.testResult = HealthCheckStatus.result_not_available.rawValue
        testFinished()
    }
    
    private func testSuccess() {
        MHCResult.shared.deviceDetails.Q_VIBRATION.testResult = HealthCheckStatus.result_pass.rawValue
        MHCResult.shared.deviceDetails.Q_VIBRATION.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testPass()
        healthTestDelegate = nil
    }
    
    private func testFinished() {
        MHCResult.shared.deviceDetails.Q_VIBRATION.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFinished()
        healthTestDelegate = nil
    }
    
}
