//
//  SpeakerHealthTest.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 23/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import Foundation
import AVFoundation

class SpeakerHealthTest: BaseHealthTest {
    
    private var audioPlayerHigh: AVAudioPlayer?
    private var audioPlayerLow: AVAudioPlayer?
    private lazy var audioSession = AVAudioSession()
    private var isLoudSpeakerDetected = false
    private var isLoudSpeakerPlaying = false
    private var isReceiverDetected = false
    private var isMicWorking = true
    private var recorder: AVAudioRecorder?
    private var playCount = 0
    private var timer: Timer?
    
    func isVolumeFull() -> Bool {
        try? audioSession.setCategory(AVAudioSession.Category.soloAmbient, mode: AVAudioSession.Mode.measurement, options: [.mixWithOthers])
        try? AVAudioSession.sharedInstance().setActive(true)
        return AVAudioSession.sharedInstance().outputVolume >= 0.7
    }
    
    override func startHealthTest() {
        super.startHealthTest()
        if isVolumeFull() {
            Permission.checkMicroPhonePermission { (status) in
                if status != .authorized {
                    self.isMicWorking = false
                }
            }
            initializeAudioSessionPlayer()
            initializeAudioRecorder()
            startTest()
        }  else {
            getTestStatus()
        }
    }
    
    private func initializeAudioSessionPlayer() {
        
        // Set Audio Session
        try? audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.measurement, options: [.mixWithOthers])
        try? audioSession.setActive(true)
        
        // Set Audio Player
        let bundle = Bundle(for: type(of: self))
        let musicPathHigher = bundle.path(forResource: "beep-4000Hz", ofType: "mp3")
        let musicUrlHigher = URL(fileURLWithPath: musicPathHigher ?? "")
        audioPlayerHigh = try? AVAudioPlayer(contentsOf: musicUrlHigher)
        audioPlayerHigh?.delegate = self
        let musicPathLower = bundle.path(forResource: "Beep", ofType: "mp3")
        let musicUrlLower = URL(fileURLWithPath: musicPathLower ?? "")
        audioPlayerLow = try? AVAudioPlayer(contentsOf: musicUrlLower)
        audioPlayerLow?.delegate = self
    }
    
    private func initializeAudioRecorder() {

        // Set audio recorder
        let docDir = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = docDir.appendingPathComponent("MyAudioMemo.m4a")
        
        var recordSetting = Dictionary() as [String: Any]
        recordSetting[AVFormatIDKey] = kAudioFormatMPEG4AAC
        recordSetting[AVSampleRateKey] = 44100.0
        recordSetting[AVNumberOfChannelsKey] = 2
        
        do {
            recorder = try AVAudioRecorder(url: fileURL, settings: recordSetting)
        } catch {
            isMicWorking = false
            print(error)
        }
        
        recorder?.delegate = self
        recorder?.isMeteringEnabled = true
        recorder?.prepareToRecord()
    }
    
    
    /// Start Speaker test
    private func startTest() {
        playCount = 0
        if isMicWorking {
            try? audioSession.setActive(true)
            recorder?.record()
            isLoudSpeakerPlaying = true
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(timerStarted), userInfo: nil, repeats: true)
            
            try? audioSession.overrideOutputAudioPort(.speaker)
            playSound(withHigherFrequency: false)
        } else {
            testResultNotAvailable()
        }
    }
    
    @objc private func timerStarted() {
        recorder?.updateMeters()
        let peakValue: Float = recorder?.peakPower(forChannel: 1) ?? 0.0
        print(peakValue)
        if isLoudSpeakerPlaying {
            if peakValue >= -30 && peakValue <= 0 {
                isLoudSpeakerDetected = true
                MHCResult.shared.deviceDetails.Q_SPEAKER.speakerOutputInDb = peakValue
            }
        } else {
            if peakValue >= -50 && peakValue <= -10 {
                isReceiverDetected = true
                audioPlayerLow?.stop()
                MHCResult.shared.deviceDetails.Q_SPEAKER.speakerOutputInDb = peakValue
                getTestStatus()
                timer?.invalidate()
            }
        }
    }
    
    private func playSound(withHigherFrequency: Bool) {
        let _ = withHigherFrequency ? audioPlayerHigh?.play() : audioPlayerLow?.play()
    }
    
    func getTestStatus() {
        
        timer?.invalidate()
        // Calculate and save the result
        audioPlayerLow?.stop()
        audioPlayerHigh?.stop()
        recorder?.stop()
        
        try? audioSession.setActive(false)
        
        if isLoudSpeakerDetected && isReceiverDetected {

            MHCResult.shared.deviceDetails.Q_MIC.testResult = HealthCheckStatus.result_pass.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.loudSpaekerResult = HealthCheckStatus.result_pass.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.receiverSpeakerResult = HealthCheckStatus.result_pass.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = HealthCheckStatus.result_pass.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.testTime = getTimeConsumedInTest()
            MHCResult.shared.deviceDetails.Q_MIC.testTime = getTimeConsumedInTest()
            healthTestDelegate?.testPass()
            healthTestDelegate = nil
            return
        } else if isLoudSpeakerDetected {
            MHCResult.shared.deviceDetails.Q_SPEAKER.loudSpaekerResult = HealthCheckStatus.result_pass.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.receiverSpeakerResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_MIC.testResult = HealthCheckStatus.result_pass.rawValue
        } else if isReceiverDetected {
            MHCResult.shared.deviceDetails.Q_SPEAKER.loudSpaekerResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.receiverSpeakerResult = HealthCheckStatus.result_pass.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_MIC.testResult = HealthCheckStatus.result_pass.rawValue
        } else {

            MHCResult.shared.deviceDetails.Q_MIC.testResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.loudSpaekerResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.receiverSpeakerResult = HealthCheckStatus.result_fail.rawValue
            MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = HealthCheckStatus.result_fail.rawValue
        }
        MHCResult.shared.deviceDetails.Q_SPEAKER.testTime = getTimeConsumedInTest()
        MHCResult.shared.deviceDetails.Q_MIC.testTime = getTimeConsumedInTest()
        healthTestDelegate?.testFailed(error: nil)
    }
    
    func testSkipped() {
        recorder?.stop()
        try? audioSession.setActive(false)
        MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_SPEAKER.testTime = getTimeConsumedInTest()
        MHCResult.shared.deviceDetails.Q_MIC.testResult = HealthCheckStatus.result_skipped.rawValue
        MHCResult.shared.deviceDetails.Q_MIC.testTime = getTimeConsumedInTest()
        
        healthTestDelegate?.testFinished()
        healthTestDelegate = nil
    }
    
    private func testResultNotAvailable() {
        try? audioSession.setActive(false)
        MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = HealthCheckStatus.result_not_available.rawValue
        MHCResult.shared.deviceDetails.Q_MIC.testResult = HealthCheckStatus.result_not_available.rawValue
        healthTestDelegate?.testFinished()
        healthTestDelegate = nil
    }
    
    fileprivate func handleTest() {
        playCount = playCount == 0 ? 1 : playCount
        if playCount < 6 {
            
            if isLoudSpeakerDetected && isReceiverDetected {
                getTestStatus()
                timer?.invalidate()
            } else if isLoudSpeakerDetected && isLoudSpeakerPlaying {
                changeSpeaker()
            } else if playCount % 2 == 0 {
                playCount += 1
                playSound(withHigherFrequency: false)
            } else {
                playCount += 1
                playSound(withHigherFrequency: true)
            }
        } else {
            if isLoudSpeakerPlaying {
                changeSpeaker()
            } else {
                getTestStatus()
                timer?.invalidate()
            }
        }
    }
    
    private func changeSpeaker() {
        try? audioSession.overrideOutputAudioPort(.none)
        isLoudSpeakerPlaying = false
        playCount = 1
        playSound(withHigherFrequency: false)
        
    }
    
    deinit {
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.soloAmbient, mode: AVAudioSession.Mode.measurement, options: [.mixWithOthers])
    }
}

extension SpeakerHealthTest: AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        handleTest()
    }
}
