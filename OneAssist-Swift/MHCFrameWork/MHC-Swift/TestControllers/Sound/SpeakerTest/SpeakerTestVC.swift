//
//  SpeakerTestVC.swift
//  MHCFrameWork
//
//  Created by Chanchal Chauhan on 23/01/19.
//  Copyright © 2019 com.chanchal. All rights reserved.
//

import UIKit

class SpeakerTestVC: CommonTestVC {
    
    private let speakerTest = SpeakerHealthTest()
    private var waveImageView: UIImageView?
    
    private var lock = 0
    private var count = 5

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isReloadView {
            return
        }
        checkPermission()
    }
    
    private func checkPermission() {
        Permission.checkMicroPhonePermission { (status) in
            if status == .denied {
                self.addObserverActiveApp()
                Utilities.showAlertForSetting(on: self, title: MhcStrings.AlertMessage.micTitle, message: MhcStrings.AlertMessage.micSetting, secondAction: {
                    
                    EventTracking.shared.eventTracking(name: .micSetting, [.location: Event.EventParams.mhcMic, .action: Event.EventParams.close])
                    
                    self.removeObserverActiveApp()
                    self.speakerTest.testSkipped()
                    self.testFinished()
                }) {
                    EventTracking.shared.eventTracking(name: .micSetting, [.location: Event.EventParams.mhcMic, .action: Event.EventParams.settings])
                }
            } else {
                self.showVolumeAlert()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        waveImageView?.alpha = 0
    }
    
    func initializeView() {
        testName = MhcStrings.Speaker.startTitle
        imageView.image = #imageLiteral(resourceName: "speakerDevice")
        labelTitle.text = MhcStrings.Speaker.playingSound
        labelDescription.text = MhcStrings.waitAMoment
        speakerTest.healthTestDelegate = self
        totalTime = 20
        labelRemainingTime.text = "\(totalTime)s"
        addWaveImageView()
    }
    
    private func addWaveImageView() {
        // Same is the height
        let width = UIScreen.main.bounds.width - (50*2)
        
        if waveImageView == nil {
            waveImageView = UIImageView(frame: CGRect(x: width/6 + (width - 220)/3 , y: width/2 - 10, width: 15, height: 20))
            viewForBackImage.addSubview(waveImageView!)
        }
        
        waveImageView?.contentMode = .scaleAspectFit
        waveImageView?.image = #imageLiteral(resourceName: "wave")
        waveImageView?.alpha = 0
    }
    
    override func appBecomeActive() {
        super.appBecomeActive()
        checkPermission()
    }
    
    private func showVolumeAlert() {
        if speakerTest.isVolumeFull() {
            startTimer()
            if self.lock == 0 {
                self.count -= 1
                self.animateWave()
                self.lock += 1
            }
            speakerTest.startHealthTest()
        } else {
            
            showAlert(title: MhcStrings.AlertMessage.speakerTitle, message: MhcStrings.AlertMessage.speaker, primaryButtonTitle: MhcStrings.Buttons.continue, secondaryButtonTitle: MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
                self.showVolumeAlert()
            }) {
                self.speakerTest.testSkipped()
            }
        }
    }
    
    func animateWave() {
        UIView.animateKeyframes(withDuration: 1.5, delay: 0, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {
                self.waveImageView?.frame.origin.x += 90
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/3, animations: {
                self.waveImageView?.alpha = 1
                self.waveImageView?.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            })
            
            UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3, animations: {
                self.waveImageView?.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
            })
            
            UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3, animations: {
                self.waveImageView?.alpha = 0
                self.waveImageView?.transform = CGAffineTransform(scaleX: 2, y: 2)
            })
        }, completion: { _ in
            self.waveImageView?.frame.origin.x -= 90
            self.waveImageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
            if self.totalTime > 0 && self.count > 0 {
                self.delay(0.1) {
                    self.count -= 1
                    self.animateWave()
                }
            } else {
                self.lock = 0
                self.count = 5
            }
        })
    }
    
    override func timerStarted() {
        super.timerStarted()
        if totalTime <= 0  {
            speakerTest.getTestStatus()
        } else if totalTime > 3 {
            labelTitle.text = MhcStrings.Speaker.playingSound
        } else {
            labelTitle.text = MhcStrings.Speaker.capturingSound
        }
    }
    
    override func tappedOnRetry() {
        super.tappedOnRetry()
        initializeView()
        showVolumeAlert()
    }

}
