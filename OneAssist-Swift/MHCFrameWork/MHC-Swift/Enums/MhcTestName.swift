//
//  MhcTestNameList.swift
//  MHCFrameWork
//
//  Created by Varun Dudeja on 06/09/18.
//  Copyright © 2018 Sudhir.Kumar. All rights reserved.
//

import Foundation
#if canImport(CoreNFC)
import CoreNFC
#endif

enum MhcTestName: String {
    
    case gps = "GPS"
    case speaker = "Speaker"
    case frontCamera = "Front Camera"
    case backCamera = "Back Camera"
    case autoFocus = "Auto Focus"
    case flash = "Flash"
    case vibration = "Vibration"
    case chargingPort = "Charging Port"
    case storage = "Storage"
    case mic = "Microphone"
    case multitouch = "Multi Touch"
    case accelerometer = "Accelerometer"
    case gyroscope = "Gyroscope"
    case magnetometer = "Magnetometer"
    case proximity = "Proximity Sensor"
    case headphoneJack = "Headphone Jack"
    case volumeUpButton = "Volume Up Button"
    case volumeDownButton = "Volume Down Button"
    case ram = "RAM"
    case wifi = "Wi-Fi"
    case bluetooth = "Bluetooth"
    case fingerprint = "Fingerprint Sensor"
    case nfc = "Near-field communication"
    case antenna = "GSM Test"
    case battery = "Battery"
    
    var testName: String {
        switch self {
        case .fingerprint where FingerprintHealthTest.biometricType() == .faceId:  return "FaceId Sensor"
        default: return self.rawValue
        }
    }
    
    var isAvailable: Bool {
        switch self {
        case .fingerprint:
            if FingerprintHealthTest.biometricType() != .none {
                return true
            } else {
                self.setTestStatus(.result_not_available)
                return false
            }
        case .nfc:
            #if canImport(CoreNFC)
            if NFCNDEFReaderSession.readingAvailable {
                return true
            } else {
                self.setTestStatus(.result_not_available)
                return false
            }
            #endif
        default: return true
        }
    }
    
    func questionId() -> String {
        switch self {
        case .mic: return "Q_MIC"
        case .speaker: return "Q_SPEAKER"
        case .vibration: return "Q_VIBRATION"
        case .multitouch: return "Q_MULTITOUCH"
        case .frontCamera: return "Q_FRONTCAMERA"
        case .backCamera: return "Q_BACKCAMERA"
        case .autoFocus: return "Q_AUTOFOCUS"
        case .flash: return "Q_FLASH"
        case .gps: return "Q_GPS"
        case .accelerometer: return "Q_ACCELEROMETER"
        case .gyroscope: return "Q_GYROSCOPE"
        case .magnetometer: return "Q_MAGNETOMETER"
        case .proximity: return "Q_PROXIMITY"
        case .headphoneJack: return "Q_HEADPHONEJACK"
        case .chargingPort: return "Q_CHARGINGPORT"
        case .storage: return "Q_STORAGE"
        case .volumeUpButton: return "Q_VOLUMEUPBUTTON"
        case .volumeDownButton: return "Q_VOLUMEDOWNBUTTON"
        case .ram: return "Q_RAM"
        case .bluetooth: return "Q_BLUETOOTH"
        case .wifi: return "Q_WIFI"
        case .fingerprint: return "Q_FINGERPRINT"
        case .nfc: return "Q_NFC"
        case .antenna: return "Q_ANTENNA"
        case .battery: return "Q_BATTERY"
        }
    }
    
    static func test(forQuestionId questionId: String?) -> MhcTestName? {
        switch questionId {
        case "Q_MIC": return MhcTestName.mic
        case "Q_SPEAKER": return MhcTestName.speaker
        case "Q_VIBRATION": return MhcTestName.vibration
        case "Q_MULTITOUCH": return MhcTestName.multitouch
        case "Q_FRONTCAMERA": return MhcTestName.frontCamera
        case "Q_BACKCAMERA": return MhcTestName.backCamera
        case "Q_AUTOFOCUS": return MhcTestName.autoFocus
        case "Q_FLASH": return MhcTestName.flash
        case "Q_GPS": return MhcTestName.gps
        case "Q_ACCELEROMETER": return MhcTestName.accelerometer
        case "Q_GYROSCOPE": return MhcTestName.gyroscope
        case "Q_MAGNETOMETER": return MhcTestName.magnetometer
        case "Q_PROXIMITY": return MhcTestName.proximity
        case "Q_HEADPHONEJACK": return MhcTestName.headphoneJack
        case "Q_CHARGINGPORT": return MhcTestName.chargingPort
        case "Q_STORAGE": return MhcTestName.storage
        case "Q_VOLUMEUPBUTTON": return MhcTestName.volumeUpButton
        case "Q_VOLUMEDOWNBUTTON": return MhcTestName.volumeDownButton
        case "Q_RAM": return MhcTestName.ram
        case "Q_BLUETOOTH": return MhcTestName.bluetooth
        case "Q_WIFI": return MhcTestName.wifi
        case "Q_FINGERPRINT": return MhcTestName.fingerprint
        case "Q_NFC": return MhcTestName.nfc
        case "Q_ANTENNA": return MhcTestName.antenna
        case "Q_BATTERY": return MhcTestName.battery
        default: return nil
        }
    }
    
    static func testStatus(forQuestionId questionId: String) -> String? {
        return test(forQuestionId: questionId)?.getStatus().testResult
    }
    
    static let values: [MhcTestName] =  [.gps, .speaker, .mic, .vibration, .frontCamera, .backCamera, .autoFocus, .flash, .headphoneJack, .storage, .chargingPort, .multitouch, .accelerometer, .gyroscope, .magnetometer, .proximity, .volumeUpButton, .volumeDownButton, .ram, .wifi, .bluetooth, .fingerprint, .nfc, .antenna, .battery].filter({$0.isAvailable})
    
    func category() -> MhcTestCategory {
        switch self {
        case .multitouch:
            return .screen
        case .frontCamera, .backCamera, .flash, .autoFocus:
            return .camera
        case .mic, .speaker, .vibration:
            return .sound
        case .gps, .nfc:
            return .connectivity
        case .volumeUpButton, .volumeDownButton:
            return .buttons
        case .gyroscope, .accelerometer, .magnetometer, .proximity, .fingerprint:
            return .sensors
        case .headphoneJack, .chargingPort:
            return .hardware
        case .wifi, .bluetooth, .ram, .storage, .antenna, .battery:
            return .other
        }
    }
    
    func getTestController() -> TestStartVC {
        switch self {
        case .mic, .speaker:
            return SpeakerTestStartVC()
        case .vibration:
            return VibrationTestStartVC()
        case .multitouch:
            return TouchTestStartVC()
        case .frontCamera, .backCamera, .autoFocus, .flash:
            return CameraTestStartVC()
        case .gps:
            return GPSTestStartVC()
        case .accelerometer, .magnetometer:
            return AccelerometerTestStartVC()
        case .gyroscope:
            return GyroscopeTestStartVC()
        case .proximity:
            return ProximityTestStartVC()
        case .headphoneJack:
            return HeadphoneTestStartVC()
        case .chargingPort:
            return ChargingStartTestVC()
        case .volumeUpButton, .volumeDownButton:
            return VolumeTestStartVC()
        case .bluetooth, .wifi, .ram, .storage, .antenna, .battery:
            return OtherTestStartVC()
        case .fingerprint:
            return FingerprintTestStartVC()
        case .nfc:
            return NFCTestStartVC()
        }
    }
    
    func getStatus() -> TestStatus {
        switch self {
        case .mic:
            return MHCResult.shared.deviceDetails.Q_MIC
        case .speaker:
            return MHCResult.shared.deviceDetails.Q_SPEAKER
        case .vibration:
            return MHCResult.shared.deviceDetails.Q_VIBRATION
        case .multitouch:
            return MHCResult.shared.deviceDetails.Q_MULTITOUCH
        case .frontCamera:
            return MHCResult.shared.deviceDetails.Q_FRONTCAMERA
        case .backCamera:
            return MHCResult.shared.deviceDetails.Q_BACKCAMERA
        case .autoFocus:
            return MHCResult.shared.deviceDetails.Q_AUTOFOCUS
        case .flash:
            return MHCResult.shared.deviceDetails.Q_FLASH
        case .gps:
            return MHCResult.shared.deviceDetails.Q_GPS
        case .accelerometer:
            return MHCResult.shared.deviceDetails.Q_ACCELEROMETER
        case .gyroscope:
            return MHCResult.shared.deviceDetails.Q_GYROSCOPE
        case .magnetometer:
            return MHCResult.shared.deviceDetails.Q_MAGNETOMETER
        case .proximity:
            return MHCResult.shared.deviceDetails.Q_PROXIMITY
        case .headphoneJack:
            return MHCResult.shared.deviceDetails.Q_HEADPHONEJACK
        case .chargingPort:
            return MHCResult.shared.deviceDetails.Q_CHARGINGPORT
        case .storage:
            return MHCResult.shared.deviceDetails.Q_STORAGE
        case .volumeUpButton:
            return MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON
        case .volumeDownButton:
            return MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON
        case .ram:
            return MHCResult.shared.deviceDetails.Q_RAM
        case .bluetooth:
            return MHCResult.shared.deviceDetails.Q_BLUETOOTH
        case .wifi:
            return MHCResult.shared.deviceDetails.Q_WIFI
        case .fingerprint:
            return MHCResult.shared.deviceDetails.Q_FINGERPRINT
        case .nfc:
            return MHCResult.shared.deviceDetails.Q_NFC
        case .antenna:
            return MHCResult.shared.deviceDetails.Q_ANTENNA
        case .battery:
            return MHCResult.shared.deviceDetails.Q_BATTERY
        }
    }
    
    
    func setStatus(status:TestStatus) {
        switch self {
        case .mic:
            MHCResult.shared.deviceDetails.Q_MIC = status as! BaseResult
        case .speaker:
            MHCResult.shared.deviceDetails.Q_SPEAKER = status as! Speaker
        case .vibration:
            MHCResult.shared.deviceDetails.Q_VIBRATION = status as! Vibration
        case .multitouch:
            MHCResult.shared.deviceDetails.Q_MULTITOUCH = status as! BaseResult
        case .frontCamera:
            MHCResult.shared.deviceDetails.Q_FRONTCAMERA = status as! FrontCamera
        case .backCamera:
            MHCResult.shared.deviceDetails.Q_BACKCAMERA = status as! BackCamera
        case .autoFocus:
            MHCResult.shared.deviceDetails.Q_AUTOFOCUS = status as! AutoFocus
        case .flash:
            MHCResult.shared.deviceDetails.Q_FLASH = status as! Flash
        case .gps:
            MHCResult.shared.deviceDetails.Q_GPS = status as! GPS
        case .accelerometer:
            MHCResult.shared.deviceDetails.Q_ACCELEROMETER = status as! BaseResult
        case .gyroscope:
            MHCResult.shared.deviceDetails.Q_GYROSCOPE = status as! BaseResult
        case .magnetometer:
            MHCResult.shared.deviceDetails.Q_MAGNETOMETER = status as! BaseResult
        case .proximity:
            MHCResult.shared.deviceDetails.Q_PROXIMITY = status as! BaseResult
        case .headphoneJack:
            MHCResult.shared.deviceDetails.Q_HEADPHONEJACK = status as! BaseResult
        case .chargingPort:
            MHCResult.shared.deviceDetails.Q_CHARGINGPORT = status as! Battery
        case .storage:
            MHCResult.shared.deviceDetails.Q_STORAGE = status as! Storages
        case .volumeUpButton:
            MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON = status as! BaseResult
        case .volumeDownButton:
            MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON = status as! BaseResult
        case .ram:
            MHCResult.shared.deviceDetails.Q_RAM = status as! RAM
        case .bluetooth:
            MHCResult.shared.deviceDetails.Q_BLUETOOTH = status as! BaseResult
        case .wifi:
            MHCResult.shared.deviceDetails.Q_WIFI = status as! BaseResult
        case .fingerprint:
            MHCResult.shared.deviceDetails.Q_FINGERPRINT = status as! BaseResult
        case .nfc:
            MHCResult.shared.deviceDetails.Q_NFC = status as! BaseResult
        case .antenna:
            MHCResult.shared.deviceDetails.Q_ANTENNA = status as! BaseResult
        case .battery:
            MHCResult.shared.deviceDetails.Q_BATTERY = status as! BaseResult
        }
    }
    
    func setTestStatus(_ testStatus:HealthCheckStatus) {
        switch self {
        case .mic:
            MHCResult.shared.deviceDetails.Q_MIC.testResult = testStatus.rawValue
        case .speaker:
            MHCResult.shared.deviceDetails.Q_SPEAKER.testResult = testStatus.rawValue
        case .vibration:
            MHCResult.shared.deviceDetails.Q_VIBRATION.testResult = testStatus.rawValue
        case .multitouch:
            MHCResult.shared.deviceDetails.Q_MULTITOUCH.testResult = testStatus.rawValue
        case .frontCamera:
            MHCResult.shared.deviceDetails.Q_FRONTCAMERA.testResult = testStatus.rawValue
        case .backCamera:
            MHCResult.shared.deviceDetails.Q_BACKCAMERA.testResult = testStatus.rawValue
        case .autoFocus:
            MHCResult.shared.deviceDetails.Q_AUTOFOCUS.testResult = testStatus.rawValue
        case .flash:
            MHCResult.shared.deviceDetails.Q_FLASH.testResult = testStatus.rawValue
        case .gps:
            MHCResult.shared.deviceDetails.Q_GPS.testResult = testStatus.rawValue
        case .accelerometer:
            MHCResult.shared.deviceDetails.Q_ACCELEROMETER.testResult = testStatus.rawValue
        case .gyroscope:
            MHCResult.shared.deviceDetails.Q_GYROSCOPE.testResult = testStatus.rawValue
        case .magnetometer:
            MHCResult.shared.deviceDetails.Q_MAGNETOMETER.testResult = testStatus.rawValue
        case .proximity:
            MHCResult.shared.deviceDetails.Q_PROXIMITY.testResult = testStatus.rawValue
        case .headphoneJack:
            MHCResult.shared.deviceDetails.Q_HEADPHONEJACK.testResult = testStatus.rawValue
        case .chargingPort:
            MHCResult.shared.deviceDetails.Q_CHARGINGPORT.testResult = testStatus.rawValue
        case .storage:
            MHCResult.shared.deviceDetails.Q_STORAGE.testResult = testStatus.rawValue
        case .volumeUpButton:
            MHCResult.shared.deviceDetails.Q_VOLUMEUPBUTTON.testResult = testStatus.rawValue
        case .volumeDownButton:
            MHCResult.shared.deviceDetails.Q_VOLUMEDOWNBUTTON.testResult = testStatus.rawValue
        case .ram:
            MHCResult.shared.deviceDetails.Q_RAM.testResult = testStatus.rawValue
        case .bluetooth:
            MHCResult.shared.deviceDetails.Q_BLUETOOTH.testResult = testStatus.rawValue
        case .wifi:
            MHCResult.shared.deviceDetails.Q_WIFI.testResult = testStatus.rawValue
        case .fingerprint:
            MHCResult.shared.deviceDetails.Q_FINGERPRINT.testResult = testStatus.rawValue
        case .nfc:
            MHCResult.shared.deviceDetails.Q_NFC.testResult = testStatus.rawValue
        case .antenna:
            MHCResult.shared.deviceDetails.Q_ANTENNA.testResult = testStatus.rawValue
        case .battery:
            MHCResult.shared.deviceDetails.Q_BATTERY.testResult = testStatus.rawValue
        }
    }
    
    func weightage() -> Int {
        switch self {
        case .mic, .speaker, .multitouch, .storage, .ram, .antenna, .battery:
            return 4
        case .gps, .headphoneJack, .chargingPort, .volumeUpButton, .volumeDownButton, .frontCamera, .backCamera, .wifi, .fingerprint:
            return 2
        case .bluetooth, .nfc, .vibration, .autoFocus, .flash, .accelerometer, .gyroscope, .magnetometer, .proximity:
            return 1
        }
    }
    
    func relatedTests() -> [MhcTestName] {
        switch self {
        case .backCamera, .frontCamera, .autoFocus, .flash:
            return [.backCamera, .frontCamera, .autoFocus, .flash]
        case .mic, .speaker:
            return [.mic, .speaker]
        case .volumeUpButton, .volumeDownButton:
            return [.volumeUpButton, .volumeDownButton]
        case .accelerometer, .magnetometer:
            return [.accelerometer, .magnetometer]
        case .wifi, .bluetooth, .ram, .storage, .antenna, .battery:
            return [.wifi, .bluetooth, .ram, .storage, .antenna, .battery]
        default:
            return [self]
        }
    }
    
    static func testResult(_ tests:[MhcTestName] = MhcTestName.values, isFromBuyback: Bool = false) -> Float {
        var numerator = 0
        var denominator = 0
        for test in tests {
            let testResult = test.getStatus().testResult
            if testResult == HealthCheckStatus.result_pass.rawValue {
                numerator += test.weightage()
                denominator += test.weightage()
            }
            
            if testResult == HealthCheckStatus.result_fail.rawValue ||  testResult == HealthCheckStatus.result_skipped.rawValue {
                denominator += test.weightage()
            }
            
            if isFromBuyback && (testResult == HealthCheckStatus.result_not_picked.rawValue || testResult == HealthCheckStatus.result_not_attempted.rawValue) {
                denominator += test.weightage()
            }
        }
        
        if denominator == 0 {
            denominator = 1
        }
        
        return Float(numerator)/Float(denominator)
    }
    
    static func isRetryEnabled() -> Bool {
        return values.contains(where: {[HealthCheckStatus.result_fail.rawValue, HealthCheckStatus.result_skipped.rawValue].contains($0.getStatus().testResult)})
    }
    
    static func filtered(tests: [MhcTestName], with status:[HealthCheckStatus], includingStatus: Bool) -> [MhcTestName] {
        if includingStatus {
            return tests.filter({status.map({$0.rawValue}).contains($0.getStatus().testResult)})
        } else {
            return tests.filter({!status.map({$0.rawValue}).contains($0.getStatus().testResult)})
        }
    }
    
    static func passed(tests: [MhcTestName]) -> [MhcTestName] {
        return filtered(tests: tests, with: [.result_pass], includingStatus: true)
    }
    
    static func notPassed(tests: [MhcTestName]) -> [MhcTestName] {
        return filtered(tests: tests, with: [.result_pass], includingStatus: false)
    }
    
    func testSuccessTitle() -> NSAttributedString {
        switch self {
        case .mic:
            return NSAttributedString(string: "Mic is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .speaker:
            return NSAttributedString(string: "Speaker is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .vibration:
            return NSAttributedString(string: "The vibration is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .multitouch:
            let text = NSMutableAttributedString(string: "Touch function is working. Your screen display is \(UIDevice.currentDeviceDiagonal)\".", attributes: [.foregroundColor: UIColor.gray])
            text.setAttributes([.foregroundColor: UIColor.darkGray], range: (text.string as NSString).range(of: "\(UIDevice.currentDeviceDiagonal)\""))
            return text
        case .frontCamera:
            let text = NSMutableAttributedString(string: "Front camera is working fine. And the resolution is \(UIDevice.currentDeviceCameraResolution.front)MPs.", attributes: [.foregroundColor: UIColor.gray])
            text.setAttributes([.foregroundColor: UIColor.darkGray], range: (text.string as NSString).range(of: "\(UIDevice.currentDeviceCameraResolution.front)MPs."))
            return text
        case .backCamera:
            let text = NSMutableAttributedString(string: "Rear camera is working fine. And the resolution is \(UIDevice.currentDeviceCameraResolution.back)MPs.", attributes: [.foregroundColor: UIColor.gray])
            text.setAttributes([.foregroundColor: UIColor.darkGray], range: (text.string as NSString).range(of: "\(UIDevice.currentDeviceCameraResolution.back)MPs"))
            return text
        case .autoFocus:
            return NSAttributedString(string: "Auto-focus is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .flash:
            return NSAttributedString(string: "Flash is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .gps:
            var text = NSMutableAttributedString(string: "GPS is working fine.", attributes: [.foregroundColor: UIColor.gray])
            if let locationAddress = Permission.shared.locationAddress {
                text = NSMutableAttributedString(string: "GPS is working fine. Your location is \(locationAddress).", attributes: [.foregroundColor: UIColor.gray])
                text.setAttributes([.foregroundColor: UIColor.darkGray], range: (text.string as NSString).range(of: locationAddress))
            }
            return text
        case .accelerometer:
            return NSAttributedString(string: "Accelerometer is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .gyroscope:
            return NSAttributedString(string: "Gyroscope is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .magnetometer:
            return NSAttributedString(string: "Magnetometer is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .proximity:
            return NSAttributedString(string: "Proximity sensor is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .headphoneJack:
            return NSAttributedString(string: "Earphones work fine.", attributes: [.foregroundColor: UIColor.gray])
        case .chargingPort:
            let text = NSMutableAttributedString(string: "Charging port is working. Current battery level is \(Int((MHCResult.shared.deviceDetails.Q_CHARGINGPORT.batteryLevel ?? 0) * 100))%", attributes: [.foregroundColor: UIColor.gray])
            text.setAttributes([.foregroundColor: UIColor.darkGray], range: (text.string as NSString).range(of: "\(Int((MHCResult.shared.deviceDetails.Q_CHARGINGPORT.batteryLevel ?? 0) * 100))%"))
            return text
        case .storage:
            return NSAttributedString(string: "Storage test successful.", attributes: [.foregroundColor: UIColor.gray])
        case .volumeUpButton:
            return NSAttributedString(string: "Volume up key works.", attributes: [.foregroundColor: UIColor.gray])
        case .volumeDownButton:
            return NSAttributedString(string: "Volume down key works.", attributes: [.foregroundColor: UIColor.gray])
        case .ram:
            return NSAttributedString(string: "RAM test successful.", attributes: [.foregroundColor: UIColor.gray])
        case .bluetooth:
            return NSAttributedString(string: "Bluetooth is working.", attributes: [.foregroundColor: UIColor.gray])
        case .wifi:
            return NSAttributedString(string: "Wifi is working.", attributes: [.foregroundColor: UIColor.gray])
        case .fingerprint:
            return NSAttributedString(string: "\(self.testName) is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .nfc:
            return NSAttributedString(string: "NFC feature is working.", attributes: [.foregroundColor: UIColor.gray])
        case .antenna:
            return NSAttributedString(string: "GSM is working fine.", attributes: [.foregroundColor: UIColor.gray])
        case .battery:
            UIDevice.current.isBatteryMonitoringEnabled = true
            let level = Int(UIDevice.current.batteryLevel * 100)
            UIDevice.current.isBatteryMonitoringEnabled = false
            let text = NSMutableAttributedString.init(string: "\(level)%", attributes: [.foregroundColor: UIColor.darkGray])
            if level > 70 {
                text.append(NSAttributedString(string: " charged, is going to run longer.", attributes: [.foregroundColor: UIColor.gray]))
            } else if level > 32 {
                text.append(NSAttributedString(string: " charged, sufficient backup.", attributes: [.foregroundColor: UIColor.gray]))
            } else {
                text.append(NSAttributedString(string: " charged, needs immediate charging.", attributes: [.foregroundColor: UIColor.gray]))
            }
            let returnText = NSMutableAttributedString(string: "Your battery is healthy. ", attributes: [.foregroundColor: UIColor.gray])
            returnText.append(text)
            return returnText
            
        }
    }
    
    func testFailedTitle() -> String {
        switch self {
        case .mic:
            return "Mic is not working."
        case .speaker:
            return "Speaker is not working."
        case .vibration:
            return "Vibration is not working."
        case .multitouch:
            return "Screen touch doesn't work."
        case .frontCamera:
            return "Front camera doesn't work."
        case .backCamera:
            return "Rear camera doesn't work."
        case .autoFocus:
            return "Auto-focus doesn't work."
        case .flash:
            return "Flash doesn't work."
        case .gps:
            return "GPS is not working."
        case .accelerometer:
            return "Accelerometer is not working."
        case .gyroscope:
            return "Gyroscope is not working."
        case .magnetometer:
            return "Magnetometer is not working."
        case .proximity:
            return "Proximity sensor is not working."
        case .headphoneJack:
            return "Earphones do not work."
        case .chargingPort:
            return "Charging port doesn't work."
        case .storage:
            return "Storage test failed."
        case .volumeUpButton:
            return "Volume up key doesn't work."
        case .volumeDownButton:
            return "Volume down key doesn't work."
        case .ram:
            return "RAM test failed."
        case .bluetooth:
            return "Bluetooth doesn't work."
        case .wifi:
            return "Wifi is not connecting."
        case .fingerprint:
            return "\(self.testName) is not working."
        case .nfc:
            return "NFC feature doesn't work."
        case .antenna:
            return "GSM is not working."
        case .battery:
                return "Battery test failed."
        }
    }
    
    func testSkippedTitle() -> String {
        switch self {
        case .mic:
            return "Mic test skipped."
        case .speaker:
            return "Speaker test skipped."
        case .vibration:
            return "Vibration test skipped."
        case .multitouch:
            return "Screen touch skipped."
        case .frontCamera:
            return "Front camera not tested."
        case .backCamera:
            return "Rear camera not tested."
        case .autoFocus:
            return "Auto-focus not tested."
        case .flash:
            return "Flash not tested."
        case .gps:
            return "GPS test skipped."
        case .accelerometer:
            return "Accelerometer not tested."
        case .gyroscope:
            return "Gyroscope not tested."
        case .magnetometer:
            return "Magnetometer not tested."
        case .proximity:
            return "Proximity sensor not tested."
        case .headphoneJack:
            return "Earphones not tested."
        case .chargingPort:
            return "Charging port not tested."
        case .storage:
            return "Storage test skipped. It affects the score."
        case .volumeUpButton:
            return "Volume up key not tested."
        case .volumeDownButton:
            return "Volume down key not tested."
        case .ram:
            return "RAM test skipped. It can affect the score."
        case .bluetooth:
            return "Bluetooth test skipped."
        case .wifi:
            return "Wifi test skipped."
        case .fingerprint:
            return "\(self.testName) not tested."
        case .nfc:
            return "NFC test skipped."
        case .antenna:
            return "GSM test skipped."
        case .battery:
            return "Battery test skipped. It largely impacts the score."
        }
    }
    
    
}
