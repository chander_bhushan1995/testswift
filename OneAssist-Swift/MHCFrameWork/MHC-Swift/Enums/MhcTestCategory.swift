//
//  MhcTestCategoryList.swift
//  MHCFrameWork
//
//  Created by Varun Dudeja on 06/09/18.
//  Copyright © 2018 Sudhir.Kumar. All rights reserved.
//

import Foundation
enum MoreTapableContent: String {
    case twoMore = "+2 more"
    case oneMore = "+1 more"
    public init?(value: String) {
        self.init(rawValue: value)
    }
}

public enum MhcTestCategory: String {
    
    case screen
    case camera
    case sound
    case connectivity
    case buttons
    case sensors
    case hardware
    case other
    
    static let values: [MhcTestCategory] = [.screen, .camera, .sound, .connectivity, .buttons, .sensors, .hardware, .other]
    
    static let testsMapping: [MhcTestCategory : [MhcTestName]] = [
        .screen: [.multitouch],
        .camera: [.frontCamera, .backCamera, .flash, .autoFocus],
        .sound: [.speaker, .mic, .vibration],
        .connectivity: [.gps, .nfc].filter({$0.isAvailable}),
        .buttons: [.volumeUpButton, .volumeDownButton],
        .sensors: [.gyroscope, .accelerometer, .magnetometer, .proximity, .fingerprint].filter({$0.isAvailable}),
        .hardware: [.headphoneJack, .chargingPort],
        .other: [.wifi, .bluetooth, .ram, .storage, .antenna, .battery]
    ]
    
    func tests() -> [MhcTestName] {
        return MhcTestCategory.testsMapping[self] ?? []
    }
    
    func remainingTest() -> [MhcTestName] {
        return tests().filter({$0.getStatus().testResult == HealthCheckStatus.result_not_attempted.rawValue})
    }
    
    func completedTests() -> [MhcTestName] {
        return tests().filter({[HealthCheckStatus.result_pass.rawValue, HealthCheckStatus.result_skipped.rawValue, HealthCheckStatus.result_fail.rawValue].contains($0.getStatus().testResult)})
    }
    
    func retryTests() -> [MhcTestName] {
        return tests().filter({[HealthCheckStatus.result_fail.rawValue, HealthCheckStatus.result_skipped.rawValue].contains($0.getStatus().testResult)})
    }
    
    func isAllNotAttempted() -> Bool {
        return !tests().contains(where: {![HealthCheckStatus.result_not_attempted.rawValue, HealthCheckStatus.result_not_picked.rawValue].contains($0.getStatus().testResult)})
    }
    
    func isNotPicked() -> Bool {
        return tests().contains(where: {$0.getStatus().testResult == HealthCheckStatus.result_not_picked.rawValue})
    }
    
    func isPicked() -> Bool {
        return tests().contains(where: {$0.getStatus().testResult != HealthCheckStatus.result_not_picked.rawValue})
    }
    
    func isAnySkipped() -> Bool {
        return tests().contains(where: {$0.getStatus().testResult == HealthCheckStatus.result_skipped.rawValue})
    }
    
    func name() -> String {
        switch self {
        case .screen:
            return "Touch Screen"
        case .camera:
            return "Camera"
        case .sound:
            return "Sound"
        case .connectivity:
            return "Connectivity"
        case .buttons:
            return "Buttons"
        case .sensors:
            return "Sensors"
        case .hardware:
            return "Hardware"
        case .other:
            return "Other tests"
        }
    }
    
    func categoryName() -> String {
        return self.rawValue
    }
    
    func testDescription() -> String {
        switch self {
        case .screen:
            return "Single touch & Multi touch."
        case .camera:
            return "Back & front camera, Flash and Auto-focus"
        case .sound:
            return "Microphone, Speaker and Vibration."
        case .connectivity:
            return "GPS/Location services and NFC."
        case .buttons:
            return "Volume up & down keys and back key."
        case .sensors:
            if self.tests().count == 5 {
               return "Accelerometer, Magnetometer, Gyroscope …" + MoreTapableContent.twoMore.rawValue
            } else {
                return "Accelerometer, Magnetometer, Gyroscope …" + MoreTapableContent.oneMore.rawValue
            }
        case .hardware:
            return "Earphones jack & Charging port."
        case .other:
            return "WiFi, Bluetooth, RAM, Storage, GSM & Battery."
        }
    }
    
    func categoryImage() -> UIImage {
        switch self {
        case .screen:
            return #imageLiteral(resourceName: "result_touch_screen")
        case .camera:
            return #imageLiteral(resourceName: "result_camera")
        case .sound:
            return #imageLiteral(resourceName: "result_sound")
        case .connectivity:
            return #imageLiteral(resourceName: "result_connectivity")
        case .buttons:
            return #imageLiteral(resourceName: "result_button")
        case .sensors:
            return #imageLiteral(resourceName: "sensors")
        case .hardware:
            return #imageLiteral(resourceName: "usb")
        case .other:
            return #imageLiteral(resourceName: "result_settings")
        }
    }
    
    func totalTest() -> Int {
        return tests().count
    }
    
    func buybackMandatoryTests() -> [MhcTestName] {
        return BuybackQuestionManager.shared.mhcTestList.filter({tests().contains($0)})
    }
    
    func buybackRemainingTests() -> [MhcTestName] {
        return BuybackQuestionManager.shared.mhcTestList.filter({tests().contains($0) && $0.getStatus().testResult != HealthCheckStatus.result_pass.rawValue})
    }
    
    func buybackCompletedTests() -> [MhcTestName] {
        return BuybackQuestionManager.shared.mhcTestList.filter({tests().contains($0) && $0.getStatus().testResult == HealthCheckStatus.result_pass.rawValue})
    }
    
    func buybackFailedTests() -> [MhcTestName] {
        return BuybackQuestionManager.shared.mhcTestList.filter({tests().contains($0) && $0.getStatus().testResult == HealthCheckStatus.result_fail.rawValue})
    }
    
}
