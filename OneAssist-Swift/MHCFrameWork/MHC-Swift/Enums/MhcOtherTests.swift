
//
//  MhcOtherTests.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 11/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

let testCompleted = "Test completed."
let wiFi = "WiFi"
let wifiDesc = "Checking WiFi..."

let blueTooth = "Bluetooth"
let bluetoothDesc = "Checking Bluetooth"

let cellulr = "Cellular"
let cellulrDesc = "Signal Strength"

let ramName = "RAM"
let ramDesc = "Memory Speed"

let battry = "Battery"
let battryDesc = "Battery charge cycle"

let storge = "Storage"
let storageDesc = "Storage capacity"

let volUp = "vol_plus"
let volDown = "vol_minus"

let pressVolUp = "Press volume up button"
let pressVolDown = "Press volume down button"

let antennaTest = "GSM Test"
let batteryTest = "Battery"
let batteryDes = "Battery charge cycle"

enum MhcOtherTests {
    case wifi
    case bluetooth
    case ram
    case storage
    case antenna
    case battery
    case volumeUp
    case volumeDown
    
    func getDescriptionModel() -> MHCTestDescriptionModel {
        
        let mhcTestDescriptionModel = MHCTestDescriptionModel()
        
        switch self {
        case .wifi:
            mhcTestDescriptionModel.image = UIImage(named: wiFi) ?? UIImage()
            mhcTestDescriptionModel.state = .started
            mhcTestDescriptionModel.testName = wiFi
            mhcTestDescriptionModel.testDescription = wifiDesc
            mhcTestDescriptionModel.testType = .wifi
        case .bluetooth:
            mhcTestDescriptionModel.image = UIImage(named: blueTooth) ?? UIImage()
            mhcTestDescriptionModel.state = .notStarted
            mhcTestDescriptionModel.testName = blueTooth
            mhcTestDescriptionModel.testDescription = blueTooth
            mhcTestDescriptionModel.testType = .bluetooth
        case .ram:
            mhcTestDescriptionModel.image = UIImage(named: ramName) ?? UIImage()
            mhcTestDescriptionModel.state = .notStarted
            mhcTestDescriptionModel.testName = ramName
            mhcTestDescriptionModel.testDescription = ramDesc
            mhcTestDescriptionModel.testType = .ram
        case .storage:
            mhcTestDescriptionModel.image = UIImage(named: storge) ?? UIImage()
            mhcTestDescriptionModel.state = .notStarted
            mhcTestDescriptionModel.testName = storge
            mhcTestDescriptionModel.testDescription = storageDesc
            mhcTestDescriptionModel.testType = .storage
        case .volumeUp:
            mhcTestDescriptionModel.image = UIImage(named: volUp) ?? UIImage()
            mhcTestDescriptionModel.state = .started
            mhcTestDescriptionModel.testName = pressVolUp
            mhcTestDescriptionModel.testType = .volumeUp
        case .volumeDown:
            mhcTestDescriptionModel.image = UIImage(named: volDown) ?? UIImage()
            mhcTestDescriptionModel.state = .notStarted
            mhcTestDescriptionModel.testName = pressVolDown
            mhcTestDescriptionModel.testType = .volumeDown
        case .antenna:
            mhcTestDescriptionModel.image = #imageLiteral(resourceName: "Cellular")
            mhcTestDescriptionModel.state = .notStarted
            mhcTestDescriptionModel.testName = antennaTest
            mhcTestDescriptionModel.testType = .antenna
        case .battery:
            mhcTestDescriptionModel.image = #imageLiteral(resourceName: "Battery")
            mhcTestDescriptionModel.state = .notStarted
            mhcTestDescriptionModel.testName = batteryTest
            mhcTestDescriptionModel.testType = .battery
            mhcTestDescriptionModel.testDescription = batteryDes
        }
        
        return mhcTestDescriptionModel
    }
}
