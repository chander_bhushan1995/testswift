//
//  HealthCheckStatus.swift
//  MHCFrameWork
//
//  Created by Varun Dudeja on 06/09/18.
//  Copyright © 2018 Sudhir.Kumar. All rights reserved.
//

import Foundation

public enum HealthCheckStatus: String {
    
    case result_pass
    case result_fail
    case result_skipped
    case result_not_available
    case result_not_attempted
    case result_not_picked

    public init?(value: String) {
        self.init(rawValue: value)
    }
    
}
