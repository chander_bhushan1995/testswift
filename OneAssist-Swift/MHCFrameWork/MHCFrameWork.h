//
//  MHCFrameWork.h
//  MHCFrameWork
//
//  Created by Sudhir.Kumar on 04/07/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MHCFrameWork.
FOUNDATION_EXPORT double MHCFrameWorkVersionNumber;

//! Project version string for MHCFrameWork.
FOUNDATION_EXPORT const unsigned char MHCFrameWorkVersionString[];
