//
//  OAAlertVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 21/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class OAAlertAction {
    
    private var _title: String!
    fileprivate var _handler: ((OAAlertAction) -> Swift.Void)?
    
    var title: String {
        return _title
    }
    
    init(title: String, handler: ((OAAlertAction) -> Swift.Void)? = nil) {
        _title = title
        _handler = handler
    }
}

class OAAlert {
    
    private var _actions: [OAAlertAction] = []
    
    var title: String?
    
    var contentVC: UIViewController?
    
    func addAction(_ action: OAAlertAction) {
        _actions.append(action)
    }
    
    var actions: [OAAlertAction] {
        return _actions
    }
}

extension UIViewController {
    
    class func forceRemoveAlertView() {
        if let windowView = UIApplication.shared.keyWindow, let alert = windowView.viewWithTag(12345) {
            alert.removeFromSuperview()
        }
    }
    
    func dismissAlert(_ alert: ABAlertView){
        alert.dismissClicked(nil)
    }
    
    private func showOAAlert(alertRef: OAAlert) {
        
        let vc = OAAlertVC()
        vc.alertRef = alertRef
        present(vc, from: nil, isAlertType: true)
    }
    
    func showAlert(title: String = "", message: String, tag: Int) {
        let alertView = ABAlertView(title: title, message: message, image: nil) {
        }
        alertView.titleFont   = DLSFont.h3.bold
        alertView.messageFont = DLSFont.bodyText.regular
        alertView.tag = tag
        alertView.addButton(title: "Ok", backgroundColor: .dodgerBlue, fontColor: UIColor.white) {
            
        }
        
        // Show
        alertView.show()
    }

    
    func showAlert(title: String = "", message: String) {
        let alertView = ABAlertView(title: title, message: message, image: nil) {
        }
        alertView.titleFont   = DLSFont.h3.bold
        alertView.messageFont = DLSFont.bodyText.regular
        
        alertView.addButton(title: "Ok", backgroundColor: .dodgerBlue, fontColor: UIColor.white) {
            
        }
        
        // Show
        alertView.show()
    }
    
    func showAlert(title:String = "" , message:String , withFeedback:Bool, completion:@escaping (_ cancel:Bool)->()){
        
        // alert with text field
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            completion(true)
            return
        })
        
        let submitButton = UIAlertAction(title: "Submit", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            guard let textFields = alert.textFields,
                textFields.count > 0 else {
                    // Could not find textfield
                    return
            }
            
            let field = textFields[0]
            // store your data
            let trimString = field.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimString?.count == 0{
                let errorAlert = UIAlertController(title: "Error", message: "Please input the feedbeack", preferredStyle: .alert)
                
                errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                    self.presentInFullScreen(alert, animated: true, completion: nil)
                }))

                self.presentInFullScreen(errorAlert, animated: true, completion: nil)
                }
            UserDefaults.standard.set(field.text, forKey: "comment")
            UserDefaults.standard.synchronize()
            completion(false)
            return
            
        })
        
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter here"
            let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            textField.addConstraint(heightConstraint)
        }
        alert.addAction(submitButton)
        alert.addAction(cancelAction)
        presentInFullScreen(alert, animated: true, completion: nil)
    }
    

    func showOpenSettingsAlert(title: String? = nil, message: String,_ popUp:(() -> Void)? = nil) {
        let alertView = ABAlertView(title: title, message: message, image: nil) {
            if let pop = popUp {
                pop()
            }
        }
        alertView.titleFont   = DLSFont.h3.bold
        alertView.messageFont = DLSFont.bodyText.regular
        
        alertView.addButton(title: "Settings", backgroundColor: .dodgerBlue, fontColor: UIColor.white) {
            Utilities.openAppSettings()
        }
        
        alertView.addButton(title: "Cancel", backgroundColor: .white, fontColor: UIColor.dodgerBlue) {
            if let pop = popUp{
                pop()
            }
        }
        alertView.show()
    }

    func showAlert(title: String = "", message: String?, primaryButtonTitle: String, _ secondaryButtonTitle: String?, isCrossEnable:Bool = true,logoImage: UIImage? = nil, isLargeLogo: Bool = false, primaryAction: @escaping AlertAction, _ secondaryAction: AlertAction?) {
        
        var alertView: ABAlertView? = nil
        
        if isCrossEnable == false {
            alertView = ABAlertView(title: title, message: message, image: logoImage, isLargeLogo: isLargeLogo, action: nil)
        }else {
            alertView = ABAlertView(title: title, message: message, image: logoImage, isLargeLogo: isLargeLogo) {
            }
        }
        
        alertView?.titleFont   = DLSFont.h3.bold
        alertView?.messageFont = DLSFont.bodyText.regular
        
        alertView?.addButton(title: primaryButtonTitle, backgroundColor: .dodgerBlue, fontColor: UIColor.white) {
            primaryAction()
        }
        if let secondaryButtonTitle = secondaryButtonTitle {
            alertView?.addButton(title: secondaryButtonTitle, backgroundColor: .white, fontColor: UIColor.dodgerBlue) {
                secondaryAction?()
            }
        }
        alertView?.show()
    }
    
    func showAlertWithCrossAction(title: String = "", message: String, primaryButtonTitle: String, primaryAction: @escaping AlertAction, _ crossAction: AlertAction?) {
        
        var alertView: ABAlertView? = nil
        
        alertView = ABAlertView(title: title, message: message, image: nil) {
            crossAction?()
        }
        
        alertView?.titleFont   = DLSFont.h3.bold
        alertView?.messageFont = DLSFont.bodyText.regular
        
        alertView?.addButton(title: primaryButtonTitle, backgroundColor: .dodgerBlue, fontColor: UIColor.white) {
            primaryAction()
        }
        
        alertView?.show()
    }
 
    func showAlert(title: String? = nil, message: String, primaryButtonTitle: String, secondaryButtonTitle: String,isCrossEnable:Bool = true, logoImage: UIImage? = nil, isLargeLogo: Bool = false, primaryAction: @escaping () -> (), secondaryAction: AlertAction?) {

        var alertView: ABAlertView? = nil
        
        if isCrossEnable == false {
            alertView = ABAlertView(title: title, message: message, image: logoImage, isLargeLogo: isLargeLogo, action: nil)
        }else {
            alertView = ABAlertView(title: title, message: message, image: logoImage, isLargeLogo: isLargeLogo) {
            }
        }
        alertView?.titleFont   = DLSFont.h3.bold
        alertView?.messageFont = DLSFont.bodyText.regular
        
        alertView?.addButton(title: primaryButtonTitle, backgroundColor: .dodgerBlue, fontColor: UIColor.white) {
            primaryAction()
        }
        alertView?.addButton(title: secondaryButtonTitle, backgroundColor: .white, fontColor: UIColor.dodgerBlue) {
            secondaryAction?()
        }
        alertView?.show()
    }
    
    func showOAAlert(title: String, message: String) {
        
        let vc = AlertMessageVC(nibName: nil, bundle: nil)
        vc.message = message
        
        let alert = OAAlert()
        alert.title = title
        
        let action = OAAlertAction(title: "OK")
        alert.addAction(action)
        
        alert.contentVC = vc
        
        showOAAlert(alertRef: alert)
    }
    
    func showOAAlert(title: String, list: [CardOptionsModel],buttonTitle:String = "OK") {
        
        let vc = CardOptionsVC()
        vc.dataSource = list
        
        let alert = OAAlert()
        alert.title = title
        
        let action = OAAlertAction(title: buttonTitle)
        alert.addAction(action)
        
        alert.contentVC = vc
        
        showOAAlert(alertRef: alert)
    }
    
    func showApplianceList(title: String, list: [String], buttonTitle:String = "Okay") {
        
        let vc = ViewApplianceListVC()
        vc.dataSource = list
        
        let alert = OAAlert()
        alert.title = title
        
        let action = OAAlertAction(title: buttonTitle)
        alert.addAction(action)
        
        alert.contentVC = vc
        
        showOAAlert(alertRef: alert)
    }
}


class OAAlertVC: BaseVC, PopoverPresented {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewTitleUnderLine: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewHorizontalSeperator: UIView!
    @IBOutlet weak var viewButtonsContainer: UIView!
    
    var alertRef: OAAlert!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initializeButtons()
    }
    
    private func initializeView() {
        
        labelTitle.font = DLSFont.h3.bold
        viewTitleUnderLine.backgroundColor = UIColor.black
        viewHorizontalSeperator.backgroundColor = UIColor.lightGray
        viewButtonsContainer.backgroundColor = UIColor.lightGray
        
        guard alertRef != nil else {
            print("no alert object found")
            return
        }
        
        labelTitle.text = alertRef.title
        
        addContentView()
    }
    
    private func addContentView() {
        
        guard alertRef.contentVC != nil else {
            print("no description view controller found")
            return
        }
        
        addChild(alertRef.contentVC!)
        viewContainer.addSubview(alertRef.contentVC!.view)
        alertRef.contentVC!.view.frame = viewContainer.bounds
        alertRef.contentVC?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        alertRef.contentVC!.didMove(toParent: self)
    }
    
    private func initializeButtons() {
        
        let verticalSeperatorWidth: CGFloat = 1
        let buttonWidth = (viewButtonsContainer.frame.width - (CGFloat(alertRef.actions.count - 1) * verticalSeperatorWidth)) / CGFloat(alertRef.actions.count)
        var xPos: CGFloat = 0
        let yPos = viewButtonsContainer.bounds.origin.y
        let height = viewButtonsContainer.bounds.height
        
        for (index,value) in alertRef.actions.enumerated() {
            
            let frame = CGRect(x: xPos, y: yPos, width: buttonWidth, height: height)
            let button = UIButton(type: .custom)
            
            button.backgroundColor = UIColor.white
            button.setTitleColor(UIColor.dodgerBlue, for: .normal)
            button.setTitle(value.title, for: .normal)
            button.titleLabel?.font = DLSFont.h3.bold
            button.tag = index
            button.addTarget(self, action: #selector(OAAlertVC.clickedButton(button:)), for: .touchUpInside)
            viewButtonsContainer.addSubview(button)
            button.frame = frame
            
            xPos = xPos + buttonWidth + verticalSeperatorWidth
        }
    }
    
    @objc private func clickedButton(button: UIButton) {
        let action = alertRef.actions[button.tag]
        action._handler?(action)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - PopOver Protocol Conformance
    
    let containerSize: CGSize = CGSize(width: UIScreen.main.bounds.width - 40, height: 320)
    
    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        print(container.preferredContentSize)
        preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 40, height: container.preferredContentSize.height + self.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
