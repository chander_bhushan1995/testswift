//
//  AlertMessageVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 21/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class AlertMessageVC: BaseVC {
    
    
    @IBOutlet weak var labelMessage: UILabel!
    
	var message: String?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		initializeView()
        // Do any additional setup after loading the view.
    }
    
	private func initializeView() {
        labelMessage.font = DLSFont.bodyText.regular
		labelMessage.text = message
        labelMessage.textAlignment = .center
	}
    
    override func viewDidLayoutSubviews() {
        labelMessage.sizeToFit()
        var frame = labelMessage.frame
        frame.size.width = view.frame.maxX - 40 // 20 margin from each side
        let gaps = self.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height - self.labelMessage.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        preferredContentSize = CGSize(width: 300, height: self.labelMessage.frame.height + gaps)
        labelMessage.frame = frame
    }

}
