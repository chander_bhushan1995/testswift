//
//  AddressRegistrationInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 23/08/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AddressRegistrationPresentationLogic {
    func responseRecieved(response: StateCityResponseDTO?, error: Error?)
}

class AddressRegistrationInteractor: BaseInteractor, AddressRegistrationBusinessLogic {
    var presenter: AddressRegistrationPresentationLogic?
    // MARK: Business Logic Conformance
    
    func getStateCityInfo(pincode: String) {
        let request = StateCityRequestDTO(pinCode: pincode)
        let _ = StateCityRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.responseRecieved(response: response, error: error)
        }
    }
}
