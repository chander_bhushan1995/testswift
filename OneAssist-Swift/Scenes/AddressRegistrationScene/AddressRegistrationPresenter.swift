//
//  AddressRegistrationPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 23/08/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AddressRegistrationDisplayLogic: class {
    func addressInfoRecieved(addressInfo: StateCityResponseDTO)
    func errorRecieved(_ error: String)
}

class AddressRegistrationPresenter: BasePresenter, AddressRegistrationPresentationLogic {
    weak var viewController: AddressRegistrationDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
    func responseRecieved(response: StateCityResponseDTO?, error: Error?) {
        if let error = error {
            viewController?.errorRecieved(error.localizedDescription)
        } else if response?.status == Constants.ResponseConstants.success {
            viewController?.addressInfoRecieved(addressInfo: response!)
        } else if let _ = response?.responseMessage {
            viewController?.addressInfoRecieved(addressInfo: setNonAddressValue(response: response!))
        } else {
            viewController?.errorRecieved(Strings.Global.somethingWrong)
        }
    }
    
    private func setNonAddressValue(response: StateCityResponseDTO) -> StateCityResponseDTO {
        let response = response
        response.addressTmp = AddressTmp(dictionary: [:])
        response.addressTmp?.city = ""
        response.addressTmp?.cityName = Strings.AddressRegistrationScene.others
        response.addressTmp?.stateCode = ""
        response.addressTmp?.stateName = Strings.AddressRegistrationScene.others
        return response
    }
}
