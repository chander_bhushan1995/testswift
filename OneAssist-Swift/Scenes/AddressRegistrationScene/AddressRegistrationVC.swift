//
//  AddressRegistrationVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 23/08/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AddressRegistrationBusinessLogic {
    func getStateCityInfo(pincode: String)
}

// MARK: - Only used for schedule inspection of WHC, EW code removed
class AddressRegistrationVC: BaseVC {
    var interactor: AddressRegistrationBusinessLogic?
    weak var delegate: AddressRegistrationDelegate?
    @IBOutlet weak var scrollViewObj: UIScrollView!
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var fieldAddress1: TextFieldView!
    @IBOutlet weak var fieldPincode: TextFieldView!
    @IBOutlet weak var fieldCity: TextFieldView!
    @IBOutlet weak var fieldState: TextFieldView!
    @IBOutlet var constraintPincodeTopAddress: NSLayoutConstraint!
    @IBOutlet weak var constraintFieldViewTopLabel: NSLayoutConstraint!
    
    @IBOutlet weak var buttonNext: ArrowButton!
    
    var fieldHandler: TextFieldViewDelegateHandler?
    var addressInfo:  StateCityResponseDTO?
    
    @IBOutlet weak var labelPresentedTitle: UILabel!
    var activationCode = ""
    var addressDetail: AddressDetail = AddressDetail()
    var customerDetails: TempCustomerDetail?
    var purchasedDate: Date?
    var pincode: String?
    var isAdding = false
    @IBOutlet weak var buttonCross: UIButton!
    @IBOutlet weak var constraintScrollTopView: NSLayoutConstraint!
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        scrollViewObj.setNeedsLayout()
    }
    
    private func hideTopLabel() {
        labelHeading.isHidden = true
        constraintFieldViewTopLabel.isActive = false
    }
    
    private func hideAddress() {
        fieldAddress1.isHidden = true
        constraintPincodeTopAddress.isActive = false
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        addTap()
        
        title = Strings.AddressRegistrationScene.title
        
        if (navigationController == nil) {
            buttonNext.setTitle(Strings.Global.save, for: .normal)
            labelPresentedTitle.text = Strings.AddressRegistrationScene.presentedTitle
            labelPresentedTitle.font = DLSFont.h3.bold
            buttonCross.isHidden = false
            constraintScrollTopView.isActive = true
        } else {
            buttonNext.setTitle(Strings.AddressRegistrationScene.nextAction, for: .normal)
            labelPresentedTitle.text = nil
            buttonCross.isHidden = true
            constraintScrollTopView.isActive = false
        }
        
        //fieldAddress1.imageField = #imageLiteral(resourceName: "address")
        fieldAddress1.descriptionText = Strings.AddressRegistrationScene.address
        
        //fieldPincode.imageField = #imageLiteral(resourceName: "pinCode")
        fieldPincode.descriptionText = Strings.AddressRegistrationScene.pincode
        fieldPincode.fieldType = OTPFieldType.self
        
        //fieldCity.imageField = #imageLiteral(resourceName: "cityState")
        fieldCity.descriptionText = Strings.AddressRegistrationScene.city
        fieldCity.textFieldObj.isEnabled = false
        fieldCity.textFieldObj.textColor = UIColor.bodyTextGray
        
        //fieldState.imageField = #imageLiteral(resourceName: "cityState")
        fieldState.descriptionText = Strings.AddressRegistrationScene.state
        fieldState.textFieldObj.isEnabled = false
        fieldState.textFieldObj.textColor = UIColor.bodyTextGray
        
        var reqFields: [TextFieldView]!
        
        labelHeading.text = "Please enter the address where you want us to send the technician for inspection"
        labelHeading.textAlignment = .left
        fieldPincode.fieldText = pincode
        fieldPincode.isUserInteractionEnabled = false
        EventTracking.shared.eventTracking(name: .Address,[.location: "WHC Inspection"])
        reqFields = [fieldAddress1]
        
        buttonNext.isEnabled = true
        fieldHandler = TextFieldViewDelegateHandler(requiredFields: reqFields, validationHandler: {[weak self] (isValid,tuple) in
            self?.buttonNext.isEnabled = true
        })
        
        if isAdding {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
            interactor?.getStateCityInfo(pincode: pincode ?? "")
        } else {
            fieldCity.fieldText = addressDetail.city
            fieldState.fieldText = addressDetail.state
            fieldAddress1.fieldText = addressDetail.address
            buttonNext.isEnabled = true
        }
    }
    
    private func validateForm() throws {
        try Validation.validatePincode(text: fieldPincode.fieldText)
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnCross(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickedButtonSubmit(_ sender: Any) {
        if !validateEmptyForm() {
            return
        }
        
        if navigationController == nil {
            if isAdding {
                addressDetail.pincode = pincode ?? ""
                addressDetail.address = fieldAddress1.fieldText ?? ""
                addressDetail.state = addressInfo?.addressTmp?.stateName ?? ""
                addressDetail.city = addressInfo?.addressTmp?.cityName ?? ""
                addressDetail.cityCode = addressInfo?.addressTmp?.city ?? ""
                addressDetail.stateCode = addressInfo?.addressTmp?.stateCode ?? ""
                delegate?.newAddressAdded(newAddress: addressDetail)
            } else {
                addressDetail.address = fieldAddress1.fieldText ?? ""
                delegate?.addressEdited(editedAddress: addressDetail)
            }
            
            dismiss(animated: true, completion: nil)
            return
        }
        
        addressDetail.pincode = pincode ?? ""
        addressDetail.address = fieldAddress1.fieldText ?? ""
        addressDetail.state = addressInfo?.addressTmp?.stateName ?? ""
        addressDetail.city = addressInfo?.addressTmp?.cityName ?? ""
        addressDetail.cityCode = addressInfo?.addressTmp?.city ?? ""
        addressDetail.stateCode = addressInfo?.addressTmp?.stateCode ?? ""
        routeToScheduleVisit()
    }
}

extension AddressRegistrationVC: TextFieldViewDelegate {
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

// MARK:- Display Logic Conformance
extension AddressRegistrationVC: AddressRegistrationDisplayLogic {
    
    func addressInfoRecieved(addressInfo: StateCityResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.addressInfo = addressInfo
        
        fieldState.fieldText = addressInfo.addressTmp?.stateName
        fieldCity.fieldText = addressInfo.addressTmp?.cityName
    }
    
    func errorRecieved(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
        fieldPincode.fieldText = ""
    }
}

// MARK:- Configuration Logic
extension AddressRegistrationVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = AddressRegistrationInteractor()
        let presenter = AddressRegistrationPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension AddressRegistrationVC {
    func routeToScheduleVisit() {
        let scheduleVisitVC = ScheduleVisitVC()
        scheduleVisitVC.serviceRequestSourceType = Strings.SODConstants.nonSOD
        scheduleVisitVC.buCode = Constants.SchemeVariables.partnerBuCode
        scheduleVisitVC.bpCode = Constants.SchemeVariables.partnerCode
        if let customerInfo = self.customerDetails {
            if let partnercode = customerInfo.partnerCode {
                scheduleVisitVC.bpCode = partnercode
            }
            
            if let partnerbucode = customerInfo.partnerBuCode {
                scheduleVisitVC.buCode = partnerbucode
            }
        }
        scheduleVisitVC.activationCode = activationCode
        scheduleVisitVC.addressDetail = addressDetail
        scheduleVisitVC.customerDetails = customerDetails
        scheduleVisitVC.getAPiCalledFirstTime = true
        scheduleVisitVC.purchasedDate = purchasedDate ?? Date.currentDate()
        navigationController?.pushViewController(scheduleVisitVC, animated: true)
    }
}

extension AddressRegistrationVC {
    fileprivate func validateEmptyForm() -> Bool {
        var isValid = true
        if fieldAddress1.isEmpty() {
            fieldAddress1.setError(Strings.EmptyTextFieldError.emptyAddress)
            isValid = false
        }
        if fieldPincode.isEmpty() {
            fieldPincode.setError(Strings.EmptyTextFieldError.emptyPin)
            isValid = false
        }
        return isValid
    }
}
