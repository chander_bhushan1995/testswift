//
//  BankSelectionPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//


import UIKit

protocol BankSelectiponDisplayLogic: class {
    func displayBankCard(list: [CardDetail])
    func displayError(_ error: String)
}

class BankSelectionPresenter: BasePresenter, BankSelectionPresentationLogic {
    weak var viewController: BankSelectiponDisplayLogic?
    
    func responseRecieved(userCards: [Card], response: BankCardResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            if let bankCards = response?.data {
                for cards in userCards {
                    bankCards.filter{$0.cardIssuerCode == cards.cardIssuerCode && Int($0.cardTypeId ?? "0") == Int(truncating: cards.cardTypeId ?? 0) }.first?.isSelected = true
                }
//                viewController?.displayBankCard(list: bankCards.sorted(by: { (obj1, obj2) -> Bool in
//                    let Obj1_Name = obj1.cardIssuerName ?? ""
//                    let Obj2_Name = obj2.cardIssuerName ?? ""
//                    return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
//                }))
                
                viewController?.displayBankCard(list: bankCards)
                
            }else {
                viewController?.displayError(Strings.Global.somethingWrong)
                
            }
        } catch {
            viewController?.displayError(error.localizedDescription)
            
        }
    }
    
}
