//
//  BankSelectionVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol BankSelectionVCDelegate: class {
    func applyFilter(filterCards: [Card])
}

protocol BankSelectionBusinessLogic {
    func getBankCard(userCards: [Card])
}

let defaultTagValue = 100

class BankSelectionVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var emptyAlertView: UIView!
    @IBOutlet weak var tagViewheight: NSLayoutConstraint!
    @IBOutlet weak var buttonApply: OAPrimaryButton!
    @IBOutlet weak var tagStyledView: TagStyledView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    weak var delegate: BankSelectionVCDelegate?
    var interactor: BankSelectionBusinessLogic?
    var userCards: [Card] = []
    var selectedFilteredCards: [Card] = []
    var cardList: [CardDetail] = []
    var uniqueCards: [UniqueCards] = []
    var filteredUniqueCards: [UniqueCards] = []
    var isSelectBankCards = false
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK:- private methods
    private func initializeView() {
        setupTagView()
        self.emptyAlertView.isHidden = true
        buttonApply.setTitle(Strings.Global.applyAction, for: .normal)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.register(cell: BankSelectionCell.self)
        tableView.reloadData()
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        interactor?.getBankCard(userCards: selectedFilteredCards)
        customizeSearchbar()
    }
    
    private func setupTagView() {
        tagStyledView.cellForItem = { item in
            if let cell = item.cell as? BankTagCell {
                cell.delegate = self
                cell.crossButton.tag = item.indexPath.row + defaultTagValue
            }
        }
        tagStyledView.didSelectItemAt = { indexPath in
        }
        
        tagStyledView.containerSize = {[unowned self] size in
            if size.height<100 {
                self.tagViewheight.constant = size.height
            }else {
                self.tagViewheight.constant = 100.0
            }
        }
        
        tagStyledView.register(UINib(nibName: BankTagCell.Identifier.identifier, bundle: nil), forCellWithReuseIdentifier: BankTagCell.Identifier.identifier)
        
        tagStyledView.options = TagStyledView.Options(sectionInset: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0),
                                                      lineSpacing: 5.0,
                                                      interitemSpacing: 5.0,
                                                      align: TagStyledView.Options.Alignment.left)
        
    }
    
    
    func addTags() {
        let filterCard = self.uniqueCards.filter {$0.isSelected == true}
        self.tagStyledView.tags = filterCard
    }
    
    // MARK: - UIButton Action
    
    @IBAction func onClickDismissAction(_ sender: UIButton?){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickApplyAction(_ sender: UIButton?) {
        if isSelectBankCards {
            let filterCard = self.cardList.filter {$0.isSelected == true}
            let cardNames = Array(Set(filterCard.map {String($0.cardIssuerName ?? "")}))
            let cards = cardNames.joined(separator: ",")
            EventTracking.shared.eventTracking(name: .bankSelected, [.location: Event.EventParams.allOffers, .bankName: cards ])
            delegate?.applyFilter(filterCards: filterCard.map { Card(cardDetail: $0) })
        }
        self.onClickDismissAction(nil)
    }
    
}

// MARK:- Table DataSource
extension BankSelectionVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUniqueCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BankSelectionCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setupModel(model: filteredUniqueCards[indexPath.row])
        return cell
    }
}

// MARK:- Table Delegate
extension BankSelectionVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! BankSelectionCell
        let cardObject = filteredUniqueCards[indexPath.row]
        cardObject.isSelected = !cardObject.isSelected
        cardObject.cards?.forEach { cardObj in
            cardObj.isSelected = cardObject.isSelected
        }
        
        cell.setupModel(model: cardObject)
        isSelectBankCards = true
        addTags()
    }
}

extension BankSelectionVC: BankTagCellDelegate {
    func didClickCrossAction(_ tag: Int?) {
        guard let tagvalue = tag else {
            return
        }
        if let cardObject = self.tagStyledView.tags?[tagvalue-defaultTagValue] as? UniqueCards {
            cardObject.isSelected = false
            cardObject.cards?.forEach { cardObj in
                cardObj.isSelected = false
            }
        }
        isSelectBankCards = true
        self.tableView.reloadData()
        self.tagStyledView.tags?.remove(at: tagvalue-defaultTagValue)
    }
}

// MARK:- Display Logic Conformance
extension BankSelectionVC: BankSelectiponDisplayLogic {
    func displayBankCard(list: [CardDetail]) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.cardList = list
        self.cardList.forEach { card in
            if uniqueCards.count > 0, let availableCard = uniqueCards.first(where: {$0.cardIssuerCode == card.cardIssuerCode}) {

                if card.isSelected {
                    availableCard.isSelected = true
                    availableCard.cards?.forEach { cardObj in
                        cardObj.isSelected = true
                    }
                }
                availableCard.cards?.append(card)

            }else {
                let cardObj = UniqueCards(dictionary: [:])
                cardObj.cardIssuerName = card.cardIssuerName
                cardObj.cardIssuerCode = card.cardIssuerCode
                cardObj.isSelected = card.isSelected
                cardObj.cards = [card]
                uniqueCards.append(cardObj)
            }
        }
        
        self.filteredUniqueCards = uniqueCards
        addTags()
        self.searchBar.delegate = self
        self.tableView.reloadData()
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

extension BankSelectionVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredUniqueCards = uniqueCards
            self.emptyAlertView.isHidden = true
            self.tableView.isHidden = false
            tableView.reloadData()
            return
        }
        filteredUniqueCards = uniqueCards.filter({(card: UniqueCards) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return card.cardIssuerName?.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        if filteredUniqueCards.count > 0 {
            self.emptyAlertView.isHidden = true
            self.tableView.isHidden = false
            
        }else {
            self.emptyAlertView.isHidden = false
            self.tableView.isHidden = true
        }
        tableView.reloadData()
    }
}

extension BankSelectionVC {
    private func customizeSearchbar(){
        // hide magnifying glass
        var searchField:UITextField? = nil;
        for subview in searchBar.subviews {
            if subview is UITextField {
                searchField = subview as? UITextField
                break
            }
        }
        
        searchField?.leftViewMode = .never;
    }
    
}

// MARK:- Configuration Logic
extension BankSelectionVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = BankSelectionInteractor()
        let presenter = BankSelectionPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
