//
//  BankSelectionCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 14/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class BankSelectionCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var titleLabel: H3RegularBlackLabel!
    @IBOutlet weak var selectionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupModel(model: UniqueCards) {
        titleLabel.text = model.cardIssuerName
        selectionButton.isSelected = model.isSelected
    }
    
}
