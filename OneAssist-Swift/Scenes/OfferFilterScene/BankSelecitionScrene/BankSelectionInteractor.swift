//
//  BankSelectionInteractor.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol BankSelectionPresentationLogic {
   func responseRecieved(userCards: [Card], response: BankCardResponseDTO?, error: Error?)
}

class BankSelectionInteractor: BaseInteractor, BankSelectionBusinessLogic {
    
    var presenter: BankSelectionPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getBankCard(userCards: [Card]){
        let _ = BankCardUseCase.service { (_, response, error) in
            self.presenter?.responseRecieved(userCards: userCards, response: response, error: error)
        }
    }
}

