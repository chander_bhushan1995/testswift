//
//  BankTagCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 14/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol BankTagCellDelegate: class {
    func didClickCrossAction(_ tag: Int?)
}

extension BankTagCell {
    struct Identifier {
        static let identifier = String(describing: BankTagCell.self)
    }
}

class BankTagCell: UICollectionViewCell, TagStyling {
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var crossButton: UIButton!
    weak var delegate: BankTagCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tagLabel.font = DLSFont.bodyText.regular
        tagLabel.textColor = UIColor.charcoalGrey
        containerView.layer.cornerRadius = 2.0
        // Initialization code
    }
    
    @IBAction func onClickcrossAction(_ sender: UIButton?){
        delegate?.didClickCrossAction(sender?.tag)
    }

    func updateStyle(_ isViewMore: Bool) { }
}
