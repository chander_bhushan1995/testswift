//
//  OfferFilterPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 03/08/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfferFilterDisplayLogic: class {
    func displayCategoryError(_ error: String)
    func displayFilterCategoryList(_ list: [FilterViewModel])
}

class OfferFilterPresenter: BasePresenter, OfferFilterPresentationLogic {
    weak var viewController: OfferFilterDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func categoryResponseRecieved(response: CategoryListResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            // save CategoryListResponseDTO in db
            if let list = response?.categories {
                viewController?.displayFilterCategoryList(getViewModels(list))
            }
            
        } catch {
            viewController?.displayCategoryError(error.localizedDescription)
        }
    }
    
    func getViewModels(_ list: [OfferCategoryCD]) -> [FilterViewModel] {
        return list.map({ (listModel) -> FilterViewModel in
            return FilterViewModel(text: listModel.category ?? "", image: UIImage())
        })
    }
}
