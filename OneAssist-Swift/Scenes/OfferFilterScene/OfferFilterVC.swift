//
//  OfferFilterVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 03/08/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfferFilterBusinessLogic {
    func getFilterCategories()
}

struct FilterViewModel {
	let text: String
	let image: UIImage
	var selected: Bool
	
	init(text: String, image: UIImage, selected: Bool = false) {
		self.text = text
		self.image = image
		self.selected = selected
	}
}

protocol OfferFilterVCDelegate: class {
    func applyFilter(_ selectedFilter: String?)
}

class OfferFilterVC: BaseVC {

    var interactor: OfferFilterBusinessLogic?
	
    static var list: [FilterViewModel]!
    var hasSingleSelection: Bool = true
    
	weak var delegate: OfferFilterVCDelegate?
    
	@IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonReset: TransparentBodyTextBoldButton!
    @IBOutlet weak var buttonApply: OAPrimaryButton!
    var filterName: String?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        
//        view.backgroundColor = UIColor.whiteSmoke
        
        buttonCancel.setImage(#imageLiteral(resourceName: "cross"), for: .normal)
        buttonCancel.tintColor = UIColor.bodyTextGray
        buttonCancel.setTitleColor(UIColor.bodyTextGray, for: .normal)
        
        buttonReset.setTitle(Strings.OfferFilterScene.reset, for: .normal)
        buttonApply.setTitle(Strings.OfferFilterScene.apply, for: .normal)
        
        collectionView.register(nib: Constants.CellIdentifiers.offerFilter, forCellReuseIdentifier: Constants.CellIdentifiers.offerFilter)
        collectionView.register(nib: Constants.NIBNames.offerFilterHeader, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Constants.NIBNames.offerFilterHeader)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: (collectionView.frame.size.width / 2 - 10), height: 44)
        flowLayout.minimumLineSpacing = 15
        flowLayout.scrollDirection = .vertical
        flowLayout.headerReferenceSize = CGSize(width: collectionView.frame.size.width, height: 40)
        collectionView.collectionViewLayout = flowLayout
        
        checkForFilters()
    }
    
    func checkForFilters() {
        if OfferFilterVC.list != nil {
            selectFilter(name: filterName)
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.reloadData()
        } else {
            getFilterCategories()
        }
    }
    
    func getFilterCategories() {
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        interactor?.getFilterCategories()
    }
    
    // MARK:- Filter Methods
    
    func reset() {
        OfferFilterVC.list = OfferFilterVC.list?.map { (model) in
            var modelCopy = model
            modelCopy.selected = false
            return modelCopy
        }
    }
    
    func toggleSelection(index: Int) {
        OfferFilterVC.list[index].selected = !(OfferFilterVC.list[index].selected)
        
        guard hasSingleSelection else {
            return
        }
        
        for tuple in OfferFilterVC.list.enumerated() {
            if tuple.offset != index {
                OfferFilterVC.list[tuple.offset].selected = false
            }
        }
    }
    
    func selectFilter(name: String?) {
        reset()
        
        for tuple in OfferFilterVC.list.enumerated() {
            if tuple.element.text == name {
                OfferFilterVC.list[tuple.offset].selected = true
            }
        }
    }

    // MARK:- Action Methods
    
    @IBAction func clickedCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedReset(_ sender: Any) {
        self.reset()
        collectionView.reloadData()
    }
    
    @IBAction func clickedApply(_ sender: Any) {
        
        let selectedModels = OfferFilterVC.list!.filter { (model) -> Bool in
            return model.selected
        }
        
        delegate?.applyFilter(selectedModels.first?.text)
        self.dismiss(animated: true, completion: nil)
    }
}

extension OfferFilterVC: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return OfferFilterVC.list!.count
	}
    
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.offerFilter, for: indexPath) as! OfferFilterCell
        cell.configureCell(viewModel: OfferFilterVC.list[indexPath.row])
		return cell
	}
}

extension OfferFilterVC: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.toggleSelection(index: indexPath.row)
		collectionView.reloadData()
	}
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constants.NIBNames.offerFilterHeader, for: indexPath) as! OfferFilterSectionHeaderView
        headerView.configureView(header: Strings.OfferFilterScene.header)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let vw = (self.collectionView.frame.width - 10) / 2
        return CGSize(width: vw, height: 44)
    }

}

// MARK:- Display Logic Conformance
extension OfferFilterVC: OfferFilterDisplayLogic {
    
    func displayCategoryError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        showAlert(message: error)
        buttonApply.isEnabled = false
        buttonReset.isEnabled = false
    }
    
    func displayFilterCategoryList(_ list: [FilterViewModel]) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        OfferFilterVC.list = list
        selectFilter(name: filterName)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
    }
}

// MARK:- Configuration Logic
extension OfferFilterVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = OfferFilterInteractor()
        let presenter = OfferFilterPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension OfferFilterVC {
    
}
