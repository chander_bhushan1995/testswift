//
//  OfferFilterInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 03/08/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfferFilterPresentationLogic {
    func categoryResponseRecieved(response: CategoryListResponseDTO?, error: Error?)
}

class OfferFilterInteractor: BaseInteractor, OfferFilterBusinessLogic {
    var presenter: OfferFilterPresentationLogic?
    
   
    // MARK: Business Logic Conformance
    func getFilterCategories() {
        
        // get from db
        
        // else from api
        let _ = CategoryListRequestUseCase.service(requestDTO: nil) {[weak self] (usecase, response, error) in
            self?.presenter?.categoryResponseRecieved(response: response, error: error)
        }
    }
}
