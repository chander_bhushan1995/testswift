//
//  TabBarVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/28/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
import StoreKit

protocol RefreshMembershipResponse {
    func getMembershipRefreshedResponse(response: CustomerMembershipDetailsResponseDTO?)
    func recievedErrorOnRefreshing()
}

class TabBarVC: UITabBarController, HideNavigationProtocol, NibLoadableView {
    
    // Navigation bar notification count labels.
    var countLabels = [(UIView,UIView)]()
    var membershipUseCase: MembershipListUseCase?
    var boolMembershipLoading : Bool = false
    var boolMembershipWalletTabLoading : Bool = false
    var boolMembershipHomeApplianceLoading : Bool = false
    var webengageEventTrackingDict = [Event.EventKeys:String]()
    var url:URL!
    var tabs: [BaseNavigationController] = []
    var customerMembershipResponse: CustomerMembershipDetailsResponseDTO?
    var isFromActivateVouchers = false
    var isShowScratchCard = false
    var userSawTutorial = false
    var downloadManager = TFLiteModelsDownloadManager.sharedInstance
    
    // AIM:-
    // 1. TabBar : inherit color, height: bgViewHeight + bottom_padding
    // 2. bgView : padding left, right, bottom, color:white, height: fixed as per DLS
    let myTabBar :DLSTabBar
    
    override var tabBar: UITabBar {
        return myTabBar
    }
    
    init(){
        self.myTabBar = DLSTabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-barHeight-marginBottom, width: UIScreen.main.bounds.width, height: barHeight))
        super.init(nibName: nil, bundle: nil)
        setupTabbar()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.myTabBar = DLSTabBar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height-barHeight-marginBottom, width: UIScreen.main.bounds.width, height: barHeight))
        super.init(coder: aDecoder)
        setupTabbar()
    }
    
    func setupTabbar() {
        self.tabBar.delegate = self
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        self.myTabBar.frame.origin.y = UIScreen.main.bounds.height-barHeight-marginBottom - bottomSafeArea
        self.view.addSubview(self.tabBar)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActiveOnViewContoller), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDeeplink), name: Notification.Name.handleDeeplink, object: nil)
        
    }
    
    @objc func handleDeeplink() {
        DeepLinkManager.checkCatalystDeeplinkonTab(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // this method should call before checkCatalystDeeplink so that tutorial will be visible or not could be decided
        showTabbarTutorial()
        DeepLinkManager.checkCatalystDeeplinkonTab(self)
        
        if isShowScratchCard && RemoteConfigManager.shared.rewardFeatureOn {
            isShowScratchCard = false
            Utilities.openScratchCard(state: .LOCKED) { [unowned self] in
                self.showTabbarTutorial()
            }
        }
    }
    
    func showTabbarTutorial() {
        if(DeepLinkManager.anyDeeplinkAvailable()){ // if any deeplink available then don't show tutorial so that another screen will be open over tabbar
               userSawTutorial = true
               UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue)
               UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
               return
           }
        
        if isNewVersion() &&
            Utilities.isCustomerVerified() &&
            !UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue) &&
            !isShowScratchCard {
            let bottomSheetView = UserUpdateView()
            var model = UserUpdateModel()
            if let firstname = CustomerDetailsCoreDataStore.currentCustomerDetails?.firstName {
                model.title = "\(Strings.Global.welcomeUser), \(firstname)!"
            }else {
                model.title = Strings.Global.hiUser
            }
            model.detail = Strings.Global.welcomeMessage
            
            bottomSheetView.setUpValue(model)
            bottomSheetView.delegate = self
            Utilities.presentPopover(view: bottomSheetView, height:UserUpdateView.height(forModel: model))
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            userSawTutorial = true
            
        } else if !UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue) && !isShowScratchCard {
            // TODO: Anand Make changes here!
            userSawTutorial = true
            //show tab bar tutorial
            let tabBarTutorialScreen = TabBarTutorialScreen()
            let tutorialNav = BaseNavigationController(rootViewController: tabBarTutorialScreen)
            tutorialNav.view.backgroundColor = .clear
            tutorialNav.modalPresentationStyle = .overFullScreen
            self.definesPresentationContext = true
            self.present(tutorialNav, animated: true, completion: nil)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            
        }else if !AppUserDefaults.isShowPushPromptOnTab {
            AppUserDefaults.isShowPushPromptOnTab = true
            showNotificationPrompt(from: nil, eventLocation: Event.EventParams.home)
        }        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .handleDeeplink, object: nil)
    }
    
    func getFeedbacks() {
        
        let membershipsIDs = self.customerMembershipResponse?.data?.memberships?.map({ $0.memId?.description ?? "" })
        
        guard membershipsIDs != nil else {
            return
        }
        
        ServiceRequestGetFeedback().getFeedbackWithCompletion(membershipsIDs:membershipsIDs!,completionHandler: {[weak self] (response, error) in
            var isMultipleAppliances = false
            if (error == nil){
                
                if let firstSR = response?.data?.first {
                    
                    var productName:String = ""
                    var productImage : String?
                    var headerText: String?
                    
                    if firstSR.serviceRequestType == Constants.PlanServices.whcInspection {
                        productName = "HomeAssist Plan"
                        productImage = "homeServ"
                        headerText = "Please give us a feedback on inspection for your HomeAssist Plan"
                    } else {
                        if let assests = firstSR.assets{
                            if assests.count > 1{
                                isMultipleAppliances = true
                            }
                            for item in assests{
                                if let prodCode = item.productCode {
                                    let model = Utilities.getProduct(productCode: prodCode)
                                    productImage = model?.subCatImageUrl
                                    productName = productName + "\(item.make ?? "") \(model?.subCategoryName ?? "")" + ", "
                                }
                                productName.removeLast()
                                productName.removeLast()
                            }
                            
                        }
                        headerText = "Please give us a feedback for your previous service request"
                    }
                    
                    let srType = Constants.Services(rawValue: firstSR.serviceRequestType  ?? "") ?? .extendedWarranty
                    
                    let vc = FeedbackRatingVC()
                    vc.serviceRequestNumber = firstSR.refPrimaryTrackingNo?.description
                    vc.serviceID = firstSR.serviceRequestId?.description
                    vc.productName = productName
                    vc.isMultipleAssests = isMultipleAppliances
                    vc.productImageUrl = productImage
                    vc.headerText = headerText
                    vc.srType = srType
                    self?.presentInFullScreen(vc, animated: true, completion: nil)
                }
            }
        })
    }
    
    func getQuestionAnswerList(completionBlock: @escaping (Bool) -> Void) {
        let requestDTO = GetQuestionAnswerRequestDTO()
        requestDTO.categoryCode = "BOARDING"
        requestDTO.questionCode = "ASSET_HA,ASSET_PE,ASSET_F"
        requestDTO.custId = UserCoreDataStore.currentUser?.cusId ?? ""
        
        let quesAnsUsecase = UserQuestionAnswerUseCase()
        quesAnsUsecase.getRequest(requestDto: requestDTO) { (resposne, error) in
            completionBlock(true)
        }
    }
    
    @objc func didBecomeActiveOnViewContoller() {
        //         getFeedbacks()
    }
    
    func refreshMembership() {
        
        if (!Utilities.isCustomerVerified()) {
             return
        }
        
        if boolMembershipLoading {
            return
        }
        
        boolMembershipLoading = true
        membershipUseCase = MembershipListUseCase()
        
        membershipUseCase?.getMembershipArrayForLoggedInUserWith(completionHandler: {[weak self] (response, error) in
            
            if let _ = error {
                self?.refreshTabBar(membershipResponse: nil)
            } else {
                
                let mem = response?.data?.memberships, pendingMem = response?.data?.pendingMemberships
                if (mem?.isEmpty ?? true) && (pendingMem?.isEmpty ?? true) {
                    self?.refreshTabBar(membershipResponse: response)
                } else if let status = response?.status, status == Constants.ResponseConstants.success {
                    self?.refreshTabBar(membershipResponse: response)
                } else {
                    do {
                        try Utilities.checkError(response, error: error)
                        self?.refreshTabBar(membershipResponse: response)
                    } catch {
                        self?.refreshTabBar(membershipResponse: nil)
                    }
                }
            }
            
            self?.boolMembershipLoading = false
            self?.boolMembershipWalletTabLoading = false
            self?.boolMembershipHomeApplianceLoading = false
        })
    }
    
    func refreshTabBar(membershipResponse: CustomerMembershipDetailsResponseDTO?) {
        if membershipResponse != nil {
            self.customerMembershipResponse = membershipResponse
        }
        
        if let vcs = viewControllers {
            for vc in vcs {
                if let navVC = vc as? BaseNavigationController {
                    if let refreshVC = navVC.children.first as? RefreshMembershipResponse {
                        if let membershipResponse = membershipResponse {
                            refreshVC.getMembershipRefreshedResponse(response: membershipResponse)
                        } else {
                            refreshVC.recievedErrorOnRefreshing()
                        }
                    }
                }
            }
            ChatBridge.updateMyAccount()
            ChatBridge.refreshHomeScreen()
        }
    }
    
    private func initializeViews() {
        updateRegistrationDetails()
        Publisher.NewPushNotificationReceived.observe(observer: self, selector: #selector(newPushNotificationReceived))
    }
    
    @objc private func newPushNotificationReceived(){
        setBellNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBellNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let from = sharedAppDelegate?.forInternalDeeplink, from {
            selectedIndex = TabIndex.newHome.rawValue
            setButtonStates(itemTag: selectedIndex)
            sharedAppDelegate?.forInternalDeeplink = false
        }
    }
    
    private func setBellNotification(){
        if let count = NotificationsCD.getCount(predicateString: "read == \(NSNumber(value: false))", inManagedObjectContext:CoreDataStack.sharedStack.mainContext) {
            self.setBellNotification(count: count)
        }
    }
    
    private func updateRegistrationDetails() {
        sharedAppDelegate?.registrationDetail()
    }
    
    @objc func notificationClicked(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .inBox)
        let notificationsVC = NotificationsVC()
        navigationController?.pushViewController(notificationsVC, animated: true)
    }
    
    @objc func chatClicked(_ sender: Any) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.chatWithUs.message, subTitle:nil, forScreen: "Chat") {[unowned self] (status) in
            if status {
                EventTracking.shared.eventTracking(name: .chat ,[.location: "Home Screen"])
                EventTracking.shared.eventTracking(name: .chatOnHomeTapped)
                self.present(ChatVC(), animated: true, completion: nil)
                EventTracking.shared.addScreenTracking(with: .chat)
            }
        }
    }
    
    fileprivate func downloadJavascriptFile() {
        let timestamp = "\(Date().timeIntervalSince1970)".replacingOccurrences(of: ".", with: "")
        
        let timelineJSURL = Constants.WHC.URL.getTimeLineJSURL
        let timelineJSURLWithRandomParam = timelineJSURL + timestamp
        
        if RemoteConfigManager.shared.forceUpdateJS{
            JavaScriptJsonHandler.downloadJSFile(from: timelineJSURLWithRandomParam)
        }else{
            // for download js file after 24(firbaseTime) hour no mater value is true or false in firbase
            if let savedTimeInSec = UserDefaults.standard.value(forKey: UserDefaultsKeys.jsDownloadTime.rawValue) as? Int64,let currentTime = Date().toSecond(),let  firbaseTime = Int64(RemoteConfigManager.shared.javascriptUpdateInterval){
                if currentTime - savedTimeInSec > firbaseTime*3600{
                    JavaScriptJsonHandler.downloadJSFile(from: timelineJSURLWithRandomParam)
                    UserDefaults.standard.set(currentTime, forKey: UserDefaultsKeys.jsDownloadTime.rawValue)
                }
                
            }else{
                //app install first time
                let time = Date().toSecond()
                UserDefaults.standard.set(time, forKey: UserDefaultsKeys.jsDownloadTime.rawValue)
                JavaScriptJsonHandler.downloadJSFile(from: timelineJSURLWithRandomParam)
            }
        }
    }
    
    func isNewVersion() -> Bool {
        var isNewVersion = false
        
        if let _ = UserDefaults.standard.value(forKey: UserDefaultsKeys.appUpdateed.rawValue) {
            isNewVersion = true
        }
        
        return isNewVersion
    }
    
    fileprivate func initializeTabBars(_ customerMembershipResponse: CustomerMembershipDetailsResponseDTO?) {
        
        downloadJavascriptFile()
        var viewControllers = [UIViewController]()
        
        let homeTabVC = HomeScreenVC()
        var homeScreenTitle = Strings.TabBarScene.home
        if Utilities.isCustomerVerified(), let username = UserCoreDataStore.currentUser?.userName, !username.isEmpty {
            homeScreenTitle = "\(Strings.TabBarScene.hi) \(username)"
        }
            
        homeTabVC.title = homeScreenTitle
        homeTabVC.membershipResponse = customerMembershipResponse
        let questionStatus =  homeTabVC.checkQuestionStatus()
        var barheight = self.tabBar.frame.size.height
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        barheight = barheight + bottomSafeArea
        
        
        //handling deeplink on react view, if login is not required on deeplink
        if let deeplink = DeepLinkManager.getDeeplinkForHomeTab() {
            if DeepLinkManager.getListOfDeeplinksForWhichLoginRequired().contains(deeplink){
                if Utilities.isCustomerVerified(){
                    homeTabVC.addToInitialProperties(["deeplinkPath": deeplink.getUrl() ?? deeplink.rawValue])
                    homeTabVC.currentDeeplink = deeplink
                }
            }else{
                homeTabVC.addToInitialProperties(["deeplinkPath": deeplink.getUrl() ?? deeplink.rawValue])
                homeTabVC.currentDeeplink = deeplink
            }
        }
        
        
        let(isRenew,isLead,haServices,peProducts,fProducts) = homeTabVC.getLeadAndInReviewStatus()
        
        let isIDFenceInMem = customerMembershipResponse?.checkCustomerHaveAnyIDFencePlan() ?? false
        homeTabVC.addToInitialProperties(["bottomHeight": barheight,"isLead": isLead,"isRenew": isRenew,"availableHAServices": Array(haServices),"availablePEProducts":peProducts,"availableFProducts":Array(fProducts),"isAnsweredAllQuestions": !questionStatus,"isIDFenceInMem":isIDFenceInMem,"rewardScratchDate":UserDefaults.standard.string(forKey: "rewardScratchDate"),"isNonSubscriber": UserDefaults.standard.bool(forKey: UserDefaultsKeys.isNonSubscriber.rawValue)])
       
        if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count>0 {
            let dictObject = Serialization.getDictionaryFromObject(object: responseDTO)
            if JSONSerialization.isValidJSONObject(dictObject) {
                if let data = try? JSONSerialization.data(withJSONObject: dictObject, options: []), let jsonString = String(data: data, encoding: .utf8) {
                    homeTabVC.addToInitialProperties(["onBoardingQA": jsonString as String])
                }
            }
        }
        
        // TODO: KAG is this properly handled??
//        homeTabVC.url = self.url

        let homeNav = BaseNavigationController(rootViewController: homeTabVC)
        homeNav.tabBarItem = UITabBarItem(title: Strings.TabBarScene.home, image: #imageLiteral(resourceName: "home_gray"), selectedImage: #imageLiteral(resourceName: "home_blue"))
        countLabels.append(configureNavbar(homeTabVC.navigationItem, notificationSelector: #selector(self.notificationClicked(_:)), chatSelector: #selector(self.chatClicked(_:))))
        tabs.append(homeNav)
        viewControllers.append(homeNav)
        
        let buyPlanVC = BuyTabVC()
        buyPlanVC.addToInitialProperties([Strings.Global.routeName: Strings.InitialReactRouteNames.BuyTab])
        buyPlanVC.title = Strings.TabBarScene.buyPlan
        let buyPlanNav = BaseNavigationController(rootViewController: buyPlanVC)
        buyPlanNav.tabBarItem = UITabBarItem(title: Strings.TabBarScene.buyPlan, image: #imageLiteral(resourceName: "rupee_gray").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "rupee_blue").withRenderingMode(.alwaysOriginal))
        if !UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShownBuyTab.rawValue){
            buyPlanNav.tabBarItem = UITabBarItem(title: Strings.TabBarScene.buyPlan, image: #imageLiteral(resourceName: "rupee_gray_red").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "rupee_blue_red").withRenderingMode(.alwaysOriginal))
        }
        countLabels.append(configureNavbar(buyPlanVC.navigationItem, notificationSelector: #selector(self.notificationClicked(_:)), chatSelector: #selector(self.chatClicked(_:))))
        tabs.append(buyPlanNav)
        viewControllers.append(buyPlanNav)
        
        let membershipsVC = MembershipTabVC()
        membershipsVC.title = Strings.TabBarScene.memberships
        membershipsVC.addToInitialProperties([Strings.Global.routeName: Strings.InitialReactRouteNames.MembershipTabScreen])
        membershipsVC.customerMembershipResponse = customerMembershipResponse
        
        //handling deeplink on react view,
        if let deeplink = DeepLinkManager.getDeeplinkForMemberShipTab() {
            if DeepLinkManager.getListOfDeeplinksForWhichLoginRequired().contains(deeplink){
                if Utilities.isCustomerVerified(){
                    membershipsVC.addToInitialProperties(["data":["deeplinkPath": deeplink.getUrl() ?? deeplink.rawValue]])
                    membershipsVC.currentDeeplink = deeplink
                }
            }else{
                membershipsVC.addToInitialProperties(["data":["deeplinkPath": deeplink.getUrl() ?? deeplink.rawValue]])
                membershipsVC.currentDeeplink = deeplink
            }
        }
        
        let membershipsNav = BaseNavigationController(rootViewController: membershipsVC)
        membershipsNav.tabBarItem = UITabBarItem(title: Strings.TabBarScene.memberships, image: #imageLiteral(resourceName: "membership_gray"), selectedImage: #imageLiteral(resourceName: "membership_blue"))
        countLabels.append(configureNavbar(membershipsVC.navigationItem, notificationSelector: #selector(self.notificationClicked(_:)), chatSelector: #selector(self.chatClicked(_:))))
        tabs.append(membershipsNav)
        viewControllers.append(membershipsNav)

        
        
        let moreVC = MyAccountVC()
        //handling deeplink on react view, if login is not required on deeplink
        if let deeplink = DeepLinkManager.getDeeplinkForAccountTab() {
            if DeepLinkManager.getListOfDeeplinksForWhichLoginRequired().contains(deeplink) {
                if Utilities.isCustomerVerified() {
                    moreVC.addToInitialProperties(["deeplinkPath": deeplink.rawValue])
                    moreVC.currentDeeplink = deeplink
                }
            }else{
                moreVC.addToInitialProperties(["deeplinkPath": deeplink.rawValue])
                moreVC.currentDeeplink = deeplink
            }
        }
        moreVC.addToInitialProperties(["isIDFenceInMem" : isIDFenceInMem,"isNonSubscriber": UserDefaults.standard.bool(forKey: UserDefaultsKeys.isNonSubscriber.rawValue)])
        moreVC.membershipResponse = customerMembershipResponse
        moreVC.title = Strings.TabBarScene.account
        let moreNav = BaseNavigationController(rootViewController: moreVC)

        moreNav.tabBarItem = UITabBarItem(title: Strings.TabBarScene.account, image: #imageLiteral(resourceName: "account_gray"), selectedImage: #imageLiteral(resourceName: "account_blue"))
        
        countLabels.append(configureNavbar(moreVC.navigationItem, notificationSelector: #selector(self.notificationClicked(_:)), chatSelector: #selector(self.chatClicked(_:))))
        tabs.append(moreNav)
        viewControllers.append(moreNav)
        
        self.viewControllers = viewControllers
        var _ :Destination!
        if isFromActivateVouchers == true{
            self.selectedIndex = TabIndex.membership.rawValue
            CacheManager.shared.selectedTabIndex = nil
            CacheManager.shared.isSameSession = false
            
        } else {
            if !CacheManager.shared.isSameSession {
                routeToDestination(getMembershipDestination(response: customerMembershipResponse))
            } else {
                CacheManager.shared.isSameSession = false
                if let selectedIndex = CacheManager.shared.selectedTabIndex {
                    self.selectedIndex = selectedIndex
                    CacheManager.shared.selectedTabIndex = nil
                }
            }
        }
        
        DeepLinkManager.checkDeeplinkTab(self)
        setButtonStates(itemTag: selectedIndex)
        setBellNotification()
        logSelectedTabEvent(for: TabIndex(rawValue: selectedIndex) ?? .newHome)
    }
    
    func checkForFraudDetectionMembership(for memResponse: CustomerMembershipDetailsResponseDTO?) {
        if let pendingFDMem = memResponse?.data?.pendingMemberships?.filter({ $0.isPendingMembership(with: Strings.FDetectionConstants.mobileProductCode) }).filter({ $0.plan?.trial == "N" }).filter({ $0.task?.status == Strings.PEScene.FraudDetection.Status.strPostDtlPending && Strings.Global.apple.caseInsensitiveCompare($0.assets?.filter({
            ($0.prodCode ?? "") == Strings.FDetectionConstants.mobileProductCode }).first?.brand ?? Strings.Global.apple) == .orderedSame }).first {
            let brandName = pendingFDMem.assets?.first?.brand
            let testType: String = pendingFDMem.task?.fdTestType ?? FDTestType.SDT.getAPIValue()
            
            FDCacheManager.clearCache()
            FDCacheManager.shared.isShowFullForm = pendingFDMem.glance?.boolValue ?? false
            FDCacheManager.shared.maxInsuranceValue = pendingFDMem.maxInsuranceValue
            FDCacheManager.shared.minInsuranceValue  = pendingFDMem.minInsuranceValue
            FDCacheManager.shared.minDeviceDate = pendingFDMem.minPurchaseDateForGlance
            
            if let vc = Utilities.topMostPresenterViewController as? NotificationPromptVC {
                vc.onDismiss = {
                    self.startFDFlow(for: pendingFDMem.activationCode, with: pendingFDMem.plan?.coverAmount, with: pendingFDMem.activityRefId, brand: brandName, testType: testType)
                }
            } else {
                if let topMostView = Utilities.topMostPresenterViewController as? BaseNavigationController {
                    if let _ = topMostView.topViewController as? TabBarVC {
                        self.startFDFlow(for: pendingFDMem.activationCode, with: pendingFDMem.plan?.coverAmount, with: pendingFDMem.activityRefId,brand: brandName, testType: testType)
                    }
                }
            }
        }
    }
    
    private func getMembershipDestination(response: CustomerMembershipDetailsResponseDTO?) -> Bool {
        
        // true means MembershipsTab
        // false means HomeTab
        let memberships = response?.data?.memberships ?? []
        let pendingMemberships = response?.data?.pendingMemberships ?? []

        // expiring
        let hasExpiringMembership = memberships.contains(where: { $0.isExpiring() && ($0.isPEMembership() || $0.isWalletMembership() || $0.isHAMembership())})
        if hasExpiringMembership {
            return true
        }
        
        // fd case
        let hasPEFDMemberships = pendingMemberships.contains(where: { $0.isPendingMembership(with: Strings.FDetectionConstants.mobileProductCode) && $0.plan?.trial == "N" })
        if hasPEFDMemberships {
            return true
        }

        // whc pending
        let hasHAPendingMemberships = pendingMemberships.contains(where: { $0.isHAMembership() && $0.task?.name == "INSPECTION"})
        if hasHAPendingMemberships {
            return true
        }
        
        // claim
        // TODO: for claim handling
        let hasPendingSRs = memberships.contains(where: {($0.claims?.contains(where: {[Constants.TimelineStatus.onHold, Constants.TimelineStatus.pending, Constants.TimelineStatus.inProgress].contains( $0.status ?? "" ) && !(["CS", "DS"].contains($0.workflowStage))}) ?? false ) })
        if hasPendingSRs {
            return true
        }
        
        // normal
        return false
    }
    
    private func routeToDestination(_ isMembershipDestination: Bool) {
        CacheManager.shared.selectedTabIndex = nil
        
        // 0 means HomeTab
        // 2 means MembershipsTab
        if isMembershipDestination {
            setButtonStates(itemTag: TabIndex.membership.rawValue)
        } else {
            setButtonStates(itemTag: TabIndex.newHome.rawValue)
        }
    }
    
    private func configureNavbar(_ navigationItem: UINavigationItem, notificationSelector: Selector, chatSelector: Selector) -> (UIView,UIView) {
        return addNotificationButton(navigationItem, notificationSelector: notificationSelector, chatSelector: chatSelector)
    }
    
    private func addNotificationButton(_ navigationItem: UINavigationItem, notificationSelector: Selector, chatSelector: Selector) -> (UIView,UIView) {
        let bellNotificationView = Utilities.getBadgeView1(image: #imageLiteral(resourceName: "dls_notification"), notificationSelector: notificationSelector)
        let chatNotificationView = Utilities.getBadgeView1(image: #imageLiteral(resourceName: "dls_chat"), notificationSelector: chatSelector)

        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:chatNotificationView), UIBarButtonItem(customView: bellNotificationView)]
        
        return (bellNotificationView,chatNotificationView)
    }
    
    private func backBarButtonImage(){
        
        var backButtonBackgroundImage = #imageLiteral(resourceName: "navigateBack")
        
        // The background should be pinned to the left and not stretch.
        backButtonBackgroundImage = backButtonBackgroundImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: backButtonBackgroundImage.size.width-1, bottom: 0, right: 0))
        
        let barAppearance = UINavigationBar.appearance()
        barAppearance.backIndicatorImage = backButtonBackgroundImage
        barAppearance.backIndicatorTransitionMaskImage = backButtonBackgroundImage
        navigationController?.navigationBar.barTintColor = .purple
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        
        // Provide an empty backBarButton to hide the 'Back' text present by
        // default in the back button.
        //
        // NOTE: You do not need to provide a target or action.  These are set
        //       by the navigation bar.
        // NOTE: Setting the title of this bar button item to ' ' (space) works
        //       around a bug in iOS 7.0.x where the background image would be
        //       horizontally compressed if the back button title is empty.
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton;
    }
}

extension TabBarVC: TabInitialVCDelegate {
    
    func recievedMembershipError(_ error: String) {
        
    }
    
    func recievedPlans(customerMembershipResponse: CustomerMembershipDetailsResponseDTO?) {
        self.customerMembershipResponse = customerMembershipResponse
        OfferSyncUseCase.shared.syncOffers()
        getFeedbacks()
        getQuestionAnswerList { (status) in
            if status {
                self.initializeTabBars(self.customerMembershipResponse)
                self.initLoadHomeAndAccountTabs()
            }
        }
        DeepLinkManager.checkCatalystDeeplinkonTab(self)
        
    }
    
    func initializeTabBarsWithoutMembership() {
         initializeTabBars(customerMembershipResponse)
        initLoadHomeAndAccountTabs()
    }
    
    func initLoadHomeAndAccountTabs(){
        self.viewControllers?.forEach { //CB: removes lazy loading of tabar
            if let navigationVC = $0 as? BaseNavigationController, let rootVC = navigationVC.viewControllers.first {
                if rootVC is MyAccountVC || rootVC is HomeScreenVC || rootVC is MembershipTabVC {
                    let _ = rootVC.view
                }
            }
        }
    }
    
    func setBellNotification(count:Int){
        var  stringCount = ""
        if count > 0 {
            stringCount =  String(count)
        }
        for i in 0..<countLabels.count{
            let container = (countLabels[i].0) as? BarButtonContainer
            container?.badgeCountLabel.text = stringCount
            if count > 0 {
                container?.badgeCount.isHidden = false
            }else {
                container?.badgeCount.isHidden = true
            }
        }
        
    }
    func setChatNotification(count:Int){
        var  stringCount = ""
        if count > 0 {
            stringCount =  String(count) 
        }else{
            UIApplication.shared.applicationIconBadgeNumber = 0 
        }
        for i in 0..<countLabels.count{
            let container = (countLabels[i].1) as? BarButtonContainer
            container?.badgeCountLabel.text = stringCount
            if count > 0 {
                container?.badgeCount.isHidden = false
            }else {
                container?.badgeCount.isHidden = true
            }
        }
    }
    
    //the function takes the tabBar.tag as an Int
    func setButtonStates (itemTag: Int) {
        //looping through and setting the states
        self.selectedIndex = itemTag
        var x = 0
        while x < tabs.count {
            if x == itemTag {
                tabs[x].tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: DLSFont.supportingText.bold, NSAttributedString.Key.foregroundColor: UIColor.buttonBlue], for: .normal)
            } else {
                tabs[x].tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: DLSFont.supportingText.regular, NSAttributedString.Key.foregroundColor: UIColor.bodyTextGray], for: .normal)
            }
            x += 1
        }
    }
    
    func logSelectedTabEvent(for tabIndex: TabIndex){
        switch tabIndex {
        case .newHome:
            EventTracking.shared.eventTracking(name: .homeTab)
        case .account:
            EventTracking.shared.eventTracking(name: .accountTab)
        case .buy:
            EventTracking.shared.eventTracking(name: .buyTab)
        case .membership:
            EventTracking.shared.eventTracking(name: .memberships)
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        setButtonStates(itemTag: tabBar.items?.firstIndex(of: item) ?? 0)
        guard let items = tabBar.items else { return }
        if let index = items.firstIndex(of: item) {
            if index == TabIndex.account.rawValue {
                tabBar.selectedItem?.image = #imageLiteral(resourceName: "account_gray")
            }
            else if index == TabIndex.buy.rawValue {
                if !UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShownBuyTab.rawValue){
                    tabBar.selectedItem?.image = #imageLiteral(resourceName: "rupee_gray")
                    tabBar.selectedItem?.selectedImage = #imageLiteral(resourceName: "rupee_blue")
                    UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShownBuyTab.rawValue)
                }
            }
            ChatBridge.selectedActionType(type: .TABSWITHCHED)
            logSelectedTabEvent(for: TabIndex(rawValue: index) ?? .newHome)
        }
    }
}

typealias FraudDetectionRouter = TabBarVC

extension FraudDetectionRouter {
    
    func routeToFraudDetectionVC(for activationCode: String?, fromScreen: FromScreen = .blank) {
        let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
        membershipActivationFDVC.activationCode = activationCode
        membershipActivationFDVC.fromScreen = fromScreen
        if let navVC = navigationController {
            navVC.pushViewController(membershipActivationFDVC, animated: true)
        }
    }
    
    func routeToDownloadingStatusVC(for activationCode: String?, with activityRefId: String?) {
        let vc = DownloadingStatusVC()
        TFLiteModelsDownloadManager.sharedInstance.activationCode = activationCode
        TFLiteModelsDownloadManager.sharedInstance.activityRefId = activityRefId
        TFLiteModelsDownloadManager.sharedInstance.delegate = vc
        downloadManager.startModelDownloading()
        presentOverFullScreen(vc, animated: true, completion: nil)
    }
    
    func showStartActivationBottomSheet(for activationCode: String?, with coverAmount: NSNumber?, with activityRefId: String?, brand: String?) {
        let activationBottomSheet = StartActivationBottomSheet()
        activationBottomSheet.delegate = self
        activationBottomSheet.activationCode = activationCode
        activationBottomSheet.activityRefId = activityRefId
        activationBottomSheet.setViewModel(coverAmount: coverAmount?.stringValue ?? "", brand: brand ?? "")
        let basePickerVC = BasePickerVC(withView:activationBottomSheet, height:activationBottomSheet.height())
       Utilities.topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    func hitAPIToChangeTestType(for activationCode: String?,with coverAmount: NSNumber?, with activityRefId: String?, testType: String, fromScreen: FromScreen = .blank, reasonToChange: String?) {
        var loaderCount: Int = 0
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        let request = FDTestTypeChangeRequestDTO(modelType: FDTestType.SDT.getAPIValue(), reasonValue: reasonToChange, activityReferenceId: activityRefId)
        FDTestTypeChangeUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
            guard let self = self else { return }
            Utilities.removeLoader(fromView: self.view, count: &loaderCount)
            
            do {
                try self.checkError(response, error: error)
                NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
                FDCacheManager.shared.changeTestType(to: .SDT)
                TFLiteModelsDownloadManager.sharedInstance.resetEverything()
                self.routeToFraudDetectionVC(for: activationCode, fromScreen: fromScreen)
            } catch let error as LogicalError {
                self.showAlert(title: "", message: error.errorDescription!, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
                    self.hitAPIToChangeTestType(for: activationCode, with: coverAmount, with: activityRefId, testType: testType, fromScreen: fromScreen, reasonToChange: reasonToChange)
                }) { }
            }
            catch {
                fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
            }
        }
    }
    
    func startFDFlow(for activationCode: String?,with coverAmount: NSNumber?, with activityRefId: String?, brand: String?, testType: String, fromScreen: FromScreen = .blank) {
        if testType == FDTestType.MT.getAPIValue() { // if MT flow is there
            if downloadManager.isModelsDownloaded() { // models already downloaded
                if downloadManager.checkIfModelCouldLoad() {  // check if models are eligible to load with TFLite
                    self.routeToFraudDetectionVC(for: activationCode, fromScreen: fromScreen)
                } else { // else change flow to SDT
                    self.hitAPIToChangeTestType(for: activationCode, with: coverAmount, with: activityRefId, testType: testType, fromScreen: fromScreen, reasonToChange: downloadManager.mirrorTestErrorMessages?.modelLoadingRetryExceeded)
                }
            } else if let activationCode = activationCode { // need to download models for mirror test
                if downloadManager.checkIfRetryCountExceededFor() { // retry count exceeded change flow
                    self.hitAPIToChangeTestType(for: activationCode, with: coverAmount, with: activityRefId, testType: testType, fromScreen: fromScreen, reasonToChange: downloadManager.mirrorTestErrorMessages?.downloadingRetryExceeded)
                } else { // start downloading
                    TFLiteModelsDownloadManager.sharedInstance.fromScreen = .blank
                    if fromScreen == .PEPaymentSuccessScreen { // show directly downloading status screen
                        self.routeToDownloadingStatusVC(for: activationCode, with: activityRefId)
                    } else { // bottomsheet to start activation
                        self.showStartActivationBottomSheet(for: activationCode, with: coverAmount, with: activityRefId, brand: brand)
                    }
                }
            }
        } else {
            self.routeToFraudDetectionVC(for: activationCode, fromScreen: fromScreen)
        }
    }
    
    func routeToMembershipActivation() {
        let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
        self.navigationController?.pushViewController(membershipActivationFDVC, animated: true)
    }
    
    func routeToSetBasicDetailScreen() {
        let membershipDetailsFDVC = MembershipDetailsFraudDetectionVC()
        self.navigationController?.pushViewController(membershipDetailsFDVC, animated: true)
    }
    
    func routeToVerifyIMEIScreen() {
        let vc = IMEIVerificationVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToSendSMSScreen(imeinumber: String?, imeinumber2: String? = nil, mobileNo: String? = nil) {
        let vc = SendSMSScreenVC()
        vc.imeiNumber = imeinumber
        vc.imeiNumber2 = imeinumber2
        vc.mobileNo = mobileNo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
     func routeToMirrorTestVideoGuide() {
           let vc = VideoGuideViewController()
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    func routeToSendSMSSuccessScreen(mobileNumber: String?) {
        let vc = SMSSuccessSceenVC()
        vc.mobileNumber = mobileNumber
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToSuccessScreen() {
        let vc = ReuploadSuccessScreen()
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToIMEIScreen() {
        let vc = IMEIScreenVC()
        let navVC = BaseNavigationController(rootViewController: vc)
        self.presentOverFullScreen(navVC, animated: true, completion: nil)
    }
    
    
    func routeToMirrorTestRatingScreen() {
        let vc = ServiceRatingVC()
        vc.flowType = .MirrorFlow
        vc.onViewPlan = { actionUrl in
            DeepLinkManager.handleLink(URL(string: "https://www.oneassist.in/common/purchasemobileprotectionplan?category=PE&product=LAP"))
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToRefundSuccessScreen(_ orderId: String, purchaseAmount: String) {
        var model = ServiceRatingModel()
        var sections: [ServiceRatingSectionModel] = []
        var cells:[Any] = []
        let headerVM = RatingHeaderViewModel(imageRightText: Strings.RefundText.refundHeaderText, headingText: "", descriptionText: Strings.RefundText.refundHeaderDetailtext, image: #imageLiteral(resourceName: "result_success"), backgroundColor: .seaGreen)
        model.headerData = headerVM
        cells.append(RefundDetailModel(purchaseAmount: purchaseAmount, orderId: orderId))
        var section = ServiceRatingSectionModel()
        section.cells = cells
        sections.append(section)
        model.sections = sections
        
        let vc = ServiceRatingVC()
        vc.flowType = .RefundFlow
        vc.serviceRatingModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TabBarVC: UserUpdateViewDelegate, StartActivationBottomSheetDelegate {
    func gotitButtonAction() {
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
    
    func didTappedStartActivation(activationCode: String?, activityRefId: String?) {
        Utilities.topMostPresenterViewController.dismiss(animated: false) { [unowned self] in
            let vc = DownloadingStatusVC()
            TFLiteModelsDownloadManager.sharedInstance.activationCode = activationCode
            TFLiteModelsDownloadManager.sharedInstance.activityRefId = activityRefId
            TFLiteModelsDownloadManager.sharedInstance.delegate = vc
            downloadManager.startModelDownloading()
            presentOverFullScreen(vc, animated: true, completion: nil)
        }
    }
}
