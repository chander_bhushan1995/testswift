//
//  AllOffersInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 12/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AllOffersPresentationLogic {
    func presentWalletCards(response: WalletCardsResponseDTO?, error: Error?)
}

class AllOffersInteractor: BaseInteractor, AllOffersBusinessLogic {
    var presenter: AllOffersPresentationLogic?
    
    func getUserCards() {
        WalletCardsUseCase.service(requestDTO: WalletCardsRequestDTO()) { [weak self] (_, response, error) in
            self?.presenter?.presentWalletCards(response: response, error: error)
        }
    }
    
    // MARK: Business Logic Conformance
    
}
