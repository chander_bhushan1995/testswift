//
//  AllOffersPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 12/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AllOffersDisplayLogic: class {
    func fetchedWalletCards(cards: [Card])
}

class AllOffersPresenter: BasePresenter, AllOffersPresentationLogic {
    weak var viewController: AllOffersDisplayLogic?
    var walletCards: [WalletCard] = []
    
    func presentWalletCards(response: WalletCardsResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            walletCards = response?.data?.cards ?? []
        } catch {
        }
        
        viewController?.fetchedWalletCards(cards: walletCards.filter({ $0.isBankCard }).map { Card(walletCard: $0) })
    }
    // MARK: Presentation Logic Conformance
    
}
