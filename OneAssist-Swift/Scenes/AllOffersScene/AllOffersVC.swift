//
//  AllOffersVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 12/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AllOffersBusinessLogic {
    func getUserCards()
}

protocol ChildViewDelegate: class {
    func childViewScrolled(childScrollView: UIScrollView)
}

protocol ChildOffers {
    func fetchFilteredOffers(filterName: String?)
    var userCards: [Card] {set get}
    func updateWallet(cards: [Card])
}

class AllOffersVC: BaseVC/*, HideNavigationProtocol*/ {
    var interactor: AllOffersBusinessLogic?
    
    @IBOutlet weak var segmentedView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    
    var webengageEventTrackingDict:[Event.EventKeys:String] = [:]
    var navigationSelectionView: NavigationBarSelectionView!
    private var bookmarkButton: BarButtonContainer?
    var segmentIndex:Int? = 0
    var filterName: String?
    var userCards: [Card] = []
    var bankFilterCards: [Card] = []
    var headerTitle: String? = nil
    var headerMutableTitle: NSAttributedString? = nil
    var selectbankToolTip: SelectbankToolTipView?
    let popTip = PopTip()
    
    private lazy var buttonBar = UIView()
    
    var onBoardingAnswersCards: [Card] {
        if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count>0 {
           if let selectedOptionList = data.filter({$0.questionCode == "ASSET_F"}).first?.optionList?.filter({$0.selected == 1}), selectedOptionList.count > 0 {
            var cards: [Card] = []
            for options in selectedOptionList {
                cards.append(Card(creditCard: options))
                cards.append(Card(debitCard:  options))
            }
            return cards
           }
       }
        return []
    }
    
    private lazy var onlineOffersVC: OnlineOffersVC = {
        var vc = OnlineOffersVC()
        vc.childDelegate = self
        vc.userCards = self.userCards
        vc.changedFilterName = self.filterName
        vc.onBoardingCards = self.onBoardingAnswersCards
        return vc
    }()
    
    private lazy var offlineOffersVC: OfflineOffersVC = {
        var vc = OfflineOffersVC()
        vc.childDelegate = self
        vc.userCards = self.userCards
        vc.changedFilterName = self.filterName
        vc.onBoardingCards = self.onBoardingAnswersCards
        return vc
    }()
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DeepLinkManager.isValidWebEngageURL(for: "/wallet/offers") && webengageEventTrackingDict.count > 0 {
            EventTracking.shared.eventTracking(name: .alloffer, [.location: fromScreen.rawValue, .utmCampaign: self.webengageEventTrackingDict[.utmCampaign] ?? "", .utmMedium: self.webengageEventTrackingDict[.utmMedium] ?? "", .utmSource: self.webengageEventTrackingDict[.utmSource] ?? ""])
            DeepLinkManager.webEngageEventTrackingURL = nil
        } else {
            EventTracking.shared.eventTracking(name: .alloffer, [.location: fromScreen.rawValue])
        }
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationShadow()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     //   OfferSyncUseCase.shared.syncOffers()
        popTip.hide(forced: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !AppUserDefaults.selectBankToolTipShow {
           showSelectBankToolTip()
        }
        AppUserDefaults.selectBankToolTipShow = true
    }
    
     func getUserCards() {
        Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
        interactor?.getUserCards()
    }
    
    @objc func updateDataAfterLogin() {
        self.onlineOffersVC.onBoardingCards = onBoardingAnswersCards
        self.offlineOffersVC.onBoardingCards = onBoardingAnswersCards
        getUserCards()
    }
    
    // MARK:- private methods
    private func initializeView() {
        bankFilterCards = userCards
        navigationSelectionView = NavigationBarSelectionView.instanceFromNib()
        navigationSelectionView.delegate = self
        setUpNavigationText()
        navigationItem.titleView = navigationSelectionView
        
        NotificationCenter.default.addObserver(self, selector: #selector(AllOffersVC.updateLabel), name: NSNotification.Name(rawValue: "BookmarkChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AllOffersVC.updateDataAfterLogin), name: NSNotification.Name(rawValue: "updateOffers"), object: nil)
        
        if segmentIndex == 1 {
            segmentControl.selectedSegmentIndex = 1
        }
        initializeSegmentedControl()
        addBarButtons()
        updateView()
        setupToolTip()
        if let filterName = filterName, !filterName.isEmpty {
            filterChanged(filterName: filterName)
        }
    }
    
    private func addBarButtons() {
        let filterBtn = Utilities.getBadgeView1(target: self, image: #imageLiteral(resourceName: "filter"), notificationSelector: #selector(AllOffersVC.clickedOnFilter))
        let bookmarksBtn = Utilities.getBadgeView1(target: self, image: #imageLiteral(resourceName: "bookmark_select_blue"), notificationSelector: #selector(self.clickedBtnBookmark(_:)))
        bookmarkButton = bookmarksBtn
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView:filterBtn), UIBarButtonItem(customView: bookmarksBtn)]
        
    }
    
    private func setBookmarkNotification(count:Int) {
        if count > 0 {
            bookmarkButton?.badgeCount.isHidden = false
            bookmarkButton?.badgeCountLabel.text = String(count)
        } else {
            bookmarkButton?.badgeCount.isHidden = true
        }
    }
    
    private func initializeSegmentedControl() {
        segmentControl.ensureClearStyle()
        
        buttonBar = UIView()
        
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = UIColor.buttonTitleBlue
        buttonBar.layer.cornerRadius = 2.0
        segmentedView.addSubview(buttonBar)
        buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 4).isActive = true

        if segmentControl.selectedSegmentIndex == 0 {
            buttonBar.leftAnchor.constraint(equalTo: segmentControl.leftAnchor).isActive = true
        } else {
            buttonBar.rightAnchor.constraint(equalTo: segmentControl.rightAnchor).isActive = true
        }

        buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: 1 / CGFloat(segmentControl.numberOfSegments)).isActive = true
    }
 
    private func add(asChildViewController viewController: UIViewController) {
    
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    private func updateView() {

        setBookmarkNotification(count: OfferSyncUseCase.shared.getBookmarkedOffers().count)

        if segmentControl.selectedSegmentIndex == 0 {
            // online event
            remove(asChildViewController: offlineOffersVC)
            add(asChildViewController: onlineOffersVC)
           // EventTracking.shared.eventTracking(name: .onlineOffers)
            EventTracking.shared.addScreenTracking(with: .allOffersOnline)
        } else {
            // offline event
            remove(asChildViewController: onlineOffersVC)
            add(asChildViewController: offlineOffersVC)
            EventTracking.shared.addScreenTracking(with: .allOffersNear)
        }
        setUpNavigationText()
    }
    
    fileprivate func filterChanged(filterName: String?) {
        onlineOffersVC.fetchFilteredOffers(filterName: filterName)
        offlineOffersVC.fetchFilteredOffers(filterName: filterName)
        
        if let filter = filterName {
            EventTracking.shared.eventTracking(name: .offerFilter, [.category: filter])
        }
    }
    
    @objc func updateLabel() {
        setBookmarkNotification(count: OfferSyncUseCase.shared.getBookmarkedOffers().count)
    }
    
    private func setupToolTip() {
        popTip.entranceAnimation = .none
        popTip.shouldShowMask = true
        popTip.shouldDismissOnTap = false
        popTip.shouldDismissOnTapOutside = true
        popTip.shouldDismissOnSwipeOutside = false
        popTip.cornerRadius = 5.0;
        popTip.bubbleColor = UIColor.buttonBlue

    }
    
    func showSelectBankToolTip() {
        var selectbankModel = SelectbankToolTipModel()
        selectbankModel.title = Strings.SelectbankToolTip.title
        selectbankModel.detail = Strings.SelectbankToolTip.detail
        selectbankModel.fistButtonText = Strings.Global.dismiss
        selectbankModel.secondButtonText = Strings.Global.selectBank
        
        if selectbankToolTip == nil {
            selectbankToolTip = SelectbankToolTipView.loadView()
        }
        
        selectbankToolTip?.tapHandler = {[unowned self] buttonType in
            self.popTip.hide(forced: true)
            print(buttonType)
            switch buttonType {
                case ButtonAction.dismiss: do {
                    print("dismiss")
                    
                }
                break;
                case ButtonAction.selectBank: do {
                    EventTracking.shared.eventTracking(name: .bankSelect, [.location: Event.EventParams.allOffers])
                    self.clickedOnBank()
                    print("selectbank")
                }
                break;
                default:
                  print("cross")
            }
        }
        
        selectbankToolTip?.frame = CGRect(x: 0, y: 0, width: 300, height: SelectbankToolTipView.height(forModel: selectbankModel, with: 300))
        selectbankToolTip?.setupdetail(model: selectbankModel)
        var fromFrame = navigationItem.titleView?.frame ?? navigationSelectionView.frame
        fromFrame.size.height = 0.0
        fromFrame.origin.y = 2
        popTip.show(customView: selectbankToolTip!, direction: .auto, in: view, from: fromFrame)
    }
    
    // MARK:- Action Methods
    
    @IBAction func clickedSegmentIndex(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0 {
            EventTracking.shared.eventTracking(name: .onlineOffers)
        }
        UIView.animate(withDuration: 0.3) {
            self.buttonBar.frame.origin.x = (self.segmentControl.frame.width / CGFloat(self.segmentControl.numberOfSegments)) * CGFloat(self.segmentControl.selectedSegmentIndex)
        }
        updateView()
    }
    
    @objc func clickedBtnBookmark(_ sender: UIButton) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.bookmarkLoginAlert.message, subTitle:nil, forScreen: "Bookmark Offer") {[unowned self] (status) in
        if status {
             self.routeToBookmark()
             EventTracking.shared.addScreenTracking(with: .bookmarkedOffers)
            }
        }
    }

    @objc private func clickedOnFilter() {
        let filterVC = OfferFilterVC()
        filterVC.hasSingleSelection = true
        filterVC.filterName = filterName
        filterVC.delegate = self
        presentInFullScreen(filterVC, animated: true, completion: nil)
    }
    
    @objc private func clickedOnBookmark() {
        routeToBookmark()
        EventTracking.shared.addScreenTracking(with: .bookmarkedOffers)
    }
    
    @objc private func clickedOnBank() {
        let bankVC = BankSelectionVC()
        bankVC.selectedFilteredCards = self.bankFilterCards.filter{!userCards.contains($0)}
        bankVC.delegate = self
        presentInFullScreen(bankVC, animated: true, completion: nil)
    }
}

// MARK:- Filter Delegate Conformance
extension AllOffersVC: OfferFilterVCDelegate {
    func applyFilter(_ selectedFilter: String?) {
        EventTracking.shared.eventTracking(name: .filterApplyTapped)
        filterChanged(filterName: selectedFilter)
        filterName = selectedFilter
    }
}

extension AllOffersVC: BankSelectionVCDelegate {
    func applyFilter(filterCards: [Card]) {
        bankFilterCards = filterCards
        if onlineOffersVC.fetchBankFilteredOffers(filterBankCards: bankFilterCards) {
           let cardIssuerCodes = Array(Set(filterCards.map {$0.cardIssuerCode}))
            if cardIssuerCodes.count > 1 {
                guard let cardname = cardIssuerCodes[0] else { return }
                
                let titleText = "Offers on " + cardname
                let mutabletitle = NSMutableAttributedString(string: "")
                let attributeText = NSAttributedString(string: titleText, attributes: [.foregroundColor: UIColor.black])
                let otherText = " + \(cardIssuerCodes.count-1) more "
                let otherAttributeText = NSAttributedString(string: otherText, attributes: [.foregroundColor: UIColor.buttonBlue])
                mutabletitle.append(attributeText)
                mutabletitle.append(otherAttributeText)
                headerMutableTitle = mutabletitle
                headerTitle = nil
                
            }else if cardIssuerCodes.count == 1, let cardname = cardIssuerCodes[0] {
               let titleText = "Offers on " + cardname
               headerMutableTitle = nil
               headerTitle = titleText
                           
            }else {
                headerMutableTitle = nil
                headerTitle = nil
            }
       }
       setUpNavigationText()
    }
    
    func setUpNavigationText() {
        if segmentControl.selectedSegmentIndex == 0 {
            // online event
            if let headerTitle = headerMutableTitle {
                navigationSelectionView.updateUI(title: headerTitle, subtitle: Strings.AllOffersScene.showing, isButtonEnable: true)
                
            }else if let headertitle = headerTitle {
                navigationSelectionView.updateUI(title: headertitle, subtitle: Strings.AllOffersScene.showing, isButtonEnable: true)
                
            }else {
                navigationSelectionView.updateUI(title:  Strings.AllOffersScene.allOffers, subtitle: Strings.AllOffersScene.showing, isButtonEnable: true)
                
            }
            
        } else {
            // offline event
            navigationSelectionView.updateUI(title: Strings.AllOffersScene.allOffers, subtitle: Strings.AllOffersScene.showing, isButtonEnable: false)
        }
    }
}

// MARK:- Display Logic Conformance
extension AllOffersVC: AllOffersDisplayLogic {
    func fetchedWalletCards(cards: [Card]) {
        Utilities.removeLoader(count: &loaderCount)
        self.userCards = cards
        onlineOffersVC.updateWallet(cards: self.userCards)
        offlineOffersVC.updateWallet(cards: self.userCards)
    }
}

extension AllOffersVC : NavigationBarActionDelegate {
    func navigationBarActionButtonTapped() {
       popTip.hide(forced: true)
        EventTracking.shared.eventTracking(name: .bankSelectDropdown, [.location: Event.EventParams.allOffers])
       self.clickedOnBank()
    }
}

// MARK:- Child Scroll Conformance
extension AllOffersVC: ChildViewDelegate {
    func childViewScrolled(childScrollView: UIScrollView) {
        
        let yVelocity = childScrollView.panGestureRecognizer.velocity(in: self.view).y
        guard yVelocity != 0 else {
            return
        }
        let bottomDirection = yVelocity < 0
        let _: CGFloat = bottomDirection ? -44 : 5
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK:- Configuration Logic
extension AllOffersVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = AllOffersInteractor()
        let presenter = AllOffersPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension AllOffersVC {
    func routeToBookmark() {
        let bookmarkVC = BookmarkedOffersVC()
         bookmarkVC.fromScreen = .fromOfferScreen
        bookmarkVC.cards = userCards
        self.navigationController?.pushViewController(bookmarkVC, animated: true)
    }
}
