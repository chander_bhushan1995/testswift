//
//  SegmentedControlHeader.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol SegmentedControlHeaderDelegate: class {
    
    func clickedSegmentIndex(_ segmentedControl: UISegmentedControl)
}

class SegmentedControlHeader: UIView {
    
    private lazy var buttonBar = UIView()
    
    @IBOutlet weak var segmentedView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet var segmentControlLeadingConstraint: NSLayoutConstraint!
    
    weak var delegate: SegmentedControlHeaderDelegate?
    var planCategory: String = ""
    var widthConstraint: NSLayoutConstraint?
    var leftConstraint: NSLayoutConstraint?
    var segmentFrames: [CGRect]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // updateView()
    }
    
    func setupSegmentedControl(with selectedSegmentIndex: Int) {
        initializeSegmentedControl(with: selectedSegmentIndex)
    }
    
    private func initializeSegmentedControl(with selectedSegmentIndex: Int) {
        segmentControl.backgroundColor = .clear
        segmentControl.tintColor = .clear
        segmentControl.ensureClearStyle()
        segmentControl.apportionsSegmentWidthsByContent = true
        if planCategory == Constants.Category.finance {
            segmentControl.removeSegment(at: 0, animated: true)
        }
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: DLSFont.bodyText.bold, NSAttributedString.Key.foregroundColor: UIColor.buttonTitleBlue], for: .selected)
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: DLSFont.bodyText.medium, NSAttributedString.Key.foregroundColor: UIColor.bodyTextGray], for: .normal)
        
        buttonBar = UIView()

        buttonBar.backgroundColor = UIColor.buttonTitleBlue
        buttonBar.layer.cornerRadius = 2.0
        segmentedView.addSubview(buttonBar)
        
        // Add constraints:
        segmentControl.setNeedsLayout()
        segmentControl.layoutIfNeeded()
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.init(item: buttonBar, attribute: .top, relatedBy: .equal, toItem: segmentControl, attribute: .bottom, multiplier: 1.0, constant: 2.0).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 3).isActive = true
        
        segmentedView.addShadowUnderView()
    }
    
    func changeBottomView(with selectedSegmentIndex: Int) {
        
        UIView.animate(withDuration: 0.3) {
            
            let selectedSegFrame = self.segmentFrames?[selectedSegmentIndex] ?? CGRect.zero
            self.buttonBar.frame.size.width = selectedSegFrame.width
            self.buttonBar.frame.origin.x = selectedSegFrame.origin.x + self.segmentControlLeadingConstraint.constant
        }
    }
    
    func updateSize() {
        
        let newFrames = segmentControl.subviews.map({$0.frame})
        segmentFrames = newFrames.sorted(by: { (rect1, rect2) -> Bool in
            return rect1.origin.x < rect2.origin.x
        })
        
        let firstFrame = segmentFrames?[segmentControl.selectedSegmentIndex] ?? CGRect.zero
        self.buttonBar.frame.size.width = firstFrame.width
        self.buttonBar.frame.origin.x = firstFrame.origin.x + self.segmentControlLeadingConstraint.constant
        print(segmentFrames)
        
        changeBottomView(with: segmentControl.selectedSegmentIndex)
    }
    
    @IBAction func clickedSegmentIndex(_ sender: UISegmentedControl) {
        changeBottomView(with: self.segmentControl.selectedSegmentIndex)
        
        delegate?.clickedSegmentIndex(segmentControl)
        // updateView()
    }
    
}
