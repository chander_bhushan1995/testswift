//
//  InvoicePickerView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 25/09/2020.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol InvoicePickerDelegate: NSObjectProtocol {
    func selectedInvoice(_ service: Invoice?)
}

class InvoicePickerView: UIView, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet var bottomSheetView: UIView!
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var tableViewHeightConstaint: NSLayoutConstraint!
    
    var model: [Invoice] = []
    var delegate: InvoicePickerDelegate?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("InvoicePickerView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        bottomSheetView.isUserInteractionEnabled = true
        
        tableViewObj.register(cell: PlanDetailSimpleCell.self)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.separatorColor = .gray2
        tableViewObj.rowHeight = 56.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tableViewObj.isScrollEnabled = tableViewObj.contentSize.height > tableViewObj.frame.size.height
    }
    
    @IBAction func crossTapped(_ sender: Any) {
        delegate?.selectedInvoice(nil)
    }
    
    func setViewModel(_ model: [Invoice], delegate: InvoicePickerDelegate) {
        self.delegate = delegate
        self.model = model
        self.tableViewHeightConstaint.constant = InvoicePickerView.tableViewHeight(forModel: model)
        tableViewObj.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PlanDetailSimpleCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setViewModel((model[indexPath.row].docType ?? "") + " " + (model[indexPath.row].memPurchaseDate ?? ""), index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedInvoice(model[indexPath.row])
    }
    
    class func height(forModel model:[Invoice]) -> CGFloat {
        var bottomSafeArea: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            if let window = UIApplication.shared.keyWindow {
                bottomSafeArea = window.safeAreaInsets.bottom
            }
        }
        let collectiveHeigthConstains: CGFloat = 56
        let contentH = CGFloat(model.count * 56) + bottomSafeArea + collectiveHeigthConstains
        return min(UIScreen.main.bounds.height * 0.75, contentH)
    }
    
    class func tableViewHeight(forModel model:[Invoice]) -> CGFloat {
        var bottomSafeArea: CGFloat = 0.0
        if #available(iOS 11.0, *) {
            if let window = UIApplication.shared.keyWindow {
                bottomSafeArea = window.safeAreaInsets.bottom
            }
        }
        let collectiveHeigthConstains: CGFloat = 56
        let maxTableH = UIScreen.main.bounds.height * 0.75 - bottomSafeArea - collectiveHeigthConstains
        return min(maxTableH, CGFloat(model.count * 56))
    }
}
