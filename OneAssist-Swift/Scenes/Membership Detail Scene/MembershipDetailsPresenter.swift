//
//  MembershipDetailsPresenter.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 30/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MembershipDetailsDisplayLogic: class {
    func displayPlanBenefits(benefits: [String],_ isRenewal: Bool, and plan: PlanDetailAndSRModel)
    
    /* For PE and Wallet only. */
    func displayError(_ error: String)
    func displayAvailabilityForRaiseClaim(for model: PlanDetailAndSRModel, isServiceable: Bool, with assetsAvailability: [String: AssetEligibility]?, draftSRId: String?)
    func displayHistoryModel(model:[HistoryModel])
    func displayChatDialogue()
    func displaySOPAssets(plan: PlanDetailAndSRModel)
    func displayEligibilityForRaiseClaim(for model: PlanDetailAndSRModel, isClaimResumed: Bool, isEligible: Bool)
    func displayInvoices(_ invoices: [Invoice])
    func displayDocumentUrl(_ url: String)
}

class MembershipDetailsPresenter: BasePresenter, MembershipDetailsPresentationLogic {
    
    weak var viewController: MembershipDetailsDisplayLogic?
    
    func presentBenefits(response: PlanBenefitsResponseDTO?, error: Error?, _ isRenewal: Bool, with plan: PlanDetailAndSRModel) {
        do {
            try checkError(response, error: error)
            
            // I will show Quick Facts only when Benefits are available
            if let benefitsList = response?.planBenefitMappingDTO?.benefitLst {
                let benefits = benefitsList.compactMap { $0.benefit }
                
                viewController?.displayPlanBenefits(benefits: benefits, isRenewal, and: plan)
            } else {
                viewController?.displayError(Strings.Global.noBenefits)
            }
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
        
    }
    
    func presentServicedAsset(response: GetServicedAssetResponseDTO?, error: Error?, with plan: PlanDetailAndSRModel) {
        do {
            try checkError(response, error: error)
            var newPlan = plan
            if let assets = response?.data, assets.count > 0 {
                newPlan.products = assets.map({ $0.getProduct(from: $0) })
            }
            
            viewController?.displaySOPAssets(plan: newPlan)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
        
    }
    
    func availabilityReceivedForRaiseClaim(for model: PlanDetailAndSRModel, draftSRId: String?, response: CheckAvailabilityResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            
            if let assets = response?.data, assets.count > 0 {
                var isAvailable = false
                var assetsAvailability = [String: AssetEligibility]()
                for asset in assets {
                    if let assetId = asset.assetId {
                        assetsAvailability[assetId] = AssetEligibility(eligible_: asset.eligibleForRaiseClaim == "Y", reason_: asset.message ?? "")
                    }
                    if asset.eligibleForRaiseClaim == "Y" {
                        isAvailable = true
                    }
                }
                viewController?.displayAvailabilityForRaiseClaim(for: model, isServiceable: isAvailable, with: assetsAvailability, draftSRId: draftSRId)
            }
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentHistoryModel(response: SearchServiceRequestPlanResponseDTO?, error: Error?){
        do {
            try checkError(response, error: error)
            var modelList: [HistoryModel] =  []
            
            if let list = response?.data {
                modelList = list.map({ (details) -> HistoryModel in
                    let newDetails: HistoryModel = HistoryModel()
                    newDetails.serviceID = String(describing: details.serviceRequestId!)
                    newDetails.serviceType = details.serviceRequestType
                    // TODO: "Samsung refrigerator"
                    newDetails.deviceName = "Samsung refrigerator"
//                    newDetails.cellType = EHistoryCategory.eCall
                    newDetails.status = EHistoryStatus.rejected
                    newDetails.hasDownloadInvoiceButton = details.showDownloadJobsheet.boolValue
                    print(details.workflowStage ?? "")
                    
                    if let count = details.assets?.count{
                        if count == 1 {
                            newDetails.imgUrl = Utilities.getProduct(productCode: details.assets?.first?.productCode ?? "")?.subCatImageUrl ?? ""
                        } else {
                            newDetails.imageName = "homeServ"
                        }
                    }
                    var products: [String] = []
                    if let assets = details.assets {
                        for asset in assets {
                            if let productName = Utilities.getProduct(productCode: asset.productCode ?? "")?.subCategoryName {
                                products.append(productName)
                                newDetails.location = productName + "Service"
                            }
                        }
                    }
                    
                    if let count = details.assets?.count, count > 1 {
                        newDetails.hasViewAllButton = true
                        newDetails.deviceName = products.joined(separator: ",")
                    } else {
                        newDetails.hasViewAllButton = false
                        newDetails.deviceName = "\(details.assets?.first?.make ?? "") \(products.first ?? "")"
                    }
                    
                    var models: [String] = []
                    if let assets = details.assets {
                        for asset in assets {
                            let productName = Utilities.getProduct(productCode: asset.productCode ?? "")?.subCategoryName ?? ""
                            models.append("\(asset.make ?? "") \(productName)")
                        }
                    }
                    newDetails.assets = models
                    
                    //newDetails.deviceName =  (newDetails.title  ?? "") + (newDetails.subtitle ?? "")
                    
                    if details.status == Constants.TimelineStatus.completed || details.status == Constants.TimelineStatus.closeResolve || details.status == Constants.TimelineStageStatus.closed {
                        if details.workflowStageStatus == Constants.TimelineStageStatus.success || details.workflowStageStatus == Constants.TimelineStageStatus.successfully || details.status == Constants.TimelineStatus.closeResolve {  // successfull
                            newDetails.status = EHistoryStatus.completed
                            if details.serviceRequestFeedback == nil || details.serviceRequestFeedback?.feedbackRating == nil {
                                newDetails.status = .completedRatingRequired
                            }
                            else{
//                                newDetails.cellType = EHistoryCategory.eCall
                            }
                        }//cancel
                        else{
//                            newDetails.cellType = EHistoryCategory.eCall
                        }
                    }
                    
                    if (details.workflowStageStatus == Constants.TimelineStageStatus.technicianCancelledCustomerUnavailable  || details.workflowStageStatus == Constants.TimelineStageStatus.technicianCancelledOther || details.workflowStageStatus == Constants.TimelineStageStatus.cancelledCustomerCancellation || details.workflowStageStatus == Constants.TimelineStageStatus.cancelledDueToExpiry || details.workflowStageStatus == Constants.TimelineStageStatus.cancelledCustomerNotAvailable || details.workflowStageStatus == Constants.TimelineStageStatus.cancelledTechnicianNotAvailable || details.status == Constants.TimelineStatus.completeUnresolve )  {
                        newDetails.status = EHistoryStatus.cancelled
//                        newDetails.cellType = EHistoryCategory.eCall
                    }
                    else if details.workflowStageStatus  == Constants.TimelineStageStatus.inspectionFailed  || details.workflowStageStatus == Constants.TimelineStageStatus.unresolved{
//                        newDetails.cellType = EHistoryCategory.eCall
                        newDetails.status = EHistoryStatus.cancelled
                    }else if details.workflowStageStatus == Constants.TimelineStageStatus.rejected || details.workflowStageStatus == Constants.TimelineStageStatus.customerDeniedPayment  {  // rejected
//                        newDetails.cellType = EHistoryCategory.eCall
                        newDetails.status = EHistoryStatus.rejected
                    }else if /*details.workflowStageStatus == "BER" ||*/ details.workflowStageStatus == Constants.TimelineStageStatus.accidentalDamage {
//                        newDetails.cellType = EHistoryCategory.eCall
                        newDetails.status = EHistoryStatus.rejected
                    }
                    
                    newDetails.detailStatus = Utilities.getStageDescription(service: details) ?? ""
                    
                    if details.workflowStage == Constants.TimelineStage.completed.rawValue{
                        newDetails.detailStatus = details.workflowData?.completed?.stageDescription ?? ""
                    }
                    
                    if Utilities.isSrTypePE(value: details.serviceRequestType) {
                        if details.workflowStage == Constants.TimelineStage.claimSettlement.rawValue {
                            newDetails.detailStatus = details.workflowData?.claimSettlement?.stageDescription ?? ""
                        } else {
                            newDetails.detailStatus = getCloseUnresolvedDescription(data: details)
                        }
                    }
                    
                    if Utilities.isSrTypeBuyback(value: details.serviceRequestType) {
                        newDetails.detailStatus = Strings.Global.srProcessedSuccessfully
                    }
                    
                    newDetails.memId = details.referenceNo ?? ""
                    newDetails.crmTrackingNumber = details.refPrimaryTrackingNo?.description ?? ""
                    newDetails.subtitle = "Service ID \(details.refPrimaryTrackingNo?.description ?? "")"
                    
                    newDetails.srId = details.serviceRequestId?.description ?? ""
                    newDetails.claimType = Utilities.getClaimType(service: details)
                    
                    return newDetails
                })
                
                viewController?.displayHistoryModel(model:modelList)
                return
            }
            // TODO: Remove HardCoding of 10
            viewController?.displayHistoryModel(model:modelList)
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
    
    func getCloseUnresolvedDescription(data: SearchService) -> String {
        let rejectArr = [Constants.TimelineStageStatus.rejected, Constants.TimelineStageStatus.unresolved, Constants.TimelineStageStatus.berr]
        
        let arrWorkFlowStage = Utilities.getArrayofStages(fromModel: data, stageArray: [.documentUpload, .verification, .pickUp, .repairAssessment, .icDoc, .insuranceDecision, .repair, .claimSettlement])
        
        return arrWorkFlowStage.first(where: { (stage) -> Bool in
            if rejectArr.contains(stage.statusCode ?? ""){
                return true
            }
            return false
        })?.stageDescription ?? ""
    }
    
    func presentCheckClaimEligibilty(with plan: PlanDetailAndSRModel, isClaimResumed: Bool, response: CustomerMembershipDetailsResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            
            let membership = response?.data?.memberships?.first(where: {$0.memId?.stringValue == plan.membershipId})
            viewController?.displayEligibilityForRaiseClaim(for: plan, isClaimResumed: isClaimResumed, isEligible: membership?.enableFileClaim ?? false)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentInvoices(response: DownloadInvoiceResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayInvoices(response?.data ?? [])
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentDocumentUrl(response: DocumentUrlResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayDocumentUrl(response?.data ?? "")
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
}
