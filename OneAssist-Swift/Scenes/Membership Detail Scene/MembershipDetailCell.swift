//
//  MembershipDetailCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MembershipDetailCellDelegate: class {
    func clickedButtonRenewalView(_ cell: MembershipDetailCell, clickedBtnRenewalView sender: Any)
}

/* This is membership detail page. SR related information is not present here. */

class MembershipDetailCell: UITableViewCell,ReuseIdentifier,NibLoadableView {
    
    @IBOutlet weak var membershipID: TagsBoldGreyLabel!
    @IBOutlet weak var planName: H3BoldLabel!
    @IBOutlet weak var amountPaid: H3BoldLabel!
    @IBOutlet weak var infoLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var planBuyOrEndDate: SupportingTextRegularBlackLabel!
    
    @IBOutlet weak var validTillView: UIView!
    @IBOutlet weak var validTill: SupportingTextBoldBlackLabel!
    
    @IBOutlet weak var memInfoView: MembershipInfoView!
    
    @IBOutlet weak var labelServiceCount: SupportingTextRegularBlackLabel!
    @IBOutlet weak var viewServiceCount: UIView!
    
    weak var delegate: MembershipDetailCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        memInfoView.delegate = self

        validTill.textColor = UIColor.seaGreen
        infoLabel.isHidden = true
        viewServiceCount.layer.cornerRadius = 4.0
    }
    
    private func getFormattedDate(from planValid: NSString) -> String {
        let dayMonYear = Date.getDateStr(from: planValid, with: .dayMonthYear).split{$0 == " "}.map(String.init)
        let formattedDate = "\(dayMonYear[0])\(String(describing: Date.getDate(from: planValid).getSuffixFromDay(dayOfMonth: Int(dayMonYear[0]) ?? -1))) \(dayMonYear[1]), \(dayMonYear[2])"
        return formattedDate
    }
    
    private func setRenewalViewModel(with plan: PlanDetailAndSRModel) {
        var memDescription: String?
        var memAttributedDescription: NSMutableAttributedString?
        var descriptionColor = UIColor.backgroundInProgress
        
        /* In case of EW, there is no renewal case as of now. */
        if plan.category == Strings.Global.whc || plan.category == Constants.Category.personalElectronics || plan.category == Constants.Category.finance {
            if plan.amountSaved > 0.0 {
                var amountSaved = ""
                if let amtSaved = Utilities.decimalNumberFormatter.string(from: NSNumber(value:Int(round(plan.amountSaved)))) {
                    amountSaved = amtSaved
                }
                
                let myString = String(format: Strings.HATabScene.Whc.amountSavedText, amountSaved)
                let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font: DLSFont.h3.bold])
                if let range = myString.range(of: "Rs \(amountSaved)") {
                    myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGreen, range: NSRange(range, in: myString))
                }
                
                memAttributedDescription = myMutableString
            } else {
                if let endDate = plan.planValidTill as NSString? {
                    descriptionColor = UIColor.backgroundError
                    
                    let dayMon = Date.getDateStr(from: endDate, with: .dayMonth).split{$0 == " "}.map(String.init)
                    let formattedDate = "\(dayMon[0])\(String(describing: Date.getDate(from: endDate).getSuffixFromDay(dayOfMonth: Int(dayMon[0]) ?? -1))) \(dayMon[1])"
                    
                    let days = Date.getDate(from: endDate as String).days(from: Date.currentDate()) ?? 0
                    if days < 0 {
                        if (plan.graceDays?.intValue ?? 0) > 0 {
                            memDescription = String(format: Strings.HATabScene.graceMessage, formattedDate, plan.graceDays ?? 0)
                        }
                    } else {
                        memDescription = String(format: plan.warningMessage ?? "", formattedDate)
                    }
                }
            }
        }
        /*   For Renewal Phase 5
         else {
         hideLabelSubheading()
         membershipExpiryLabel.text = Strings.HATabScene.EW.membershipExp
         viewBenefitsButton.setTitle(Strings.Global.whyUpgrade, for: .normal)
         } */
        
        memInfoView.setupUI(with: .textArrowButtonCard)
        
        memInfoView.setupTextArrowWithMemStatusCard(with: memDescription, and: memAttributedDescription, and: descriptionColor, and: Strings.MembershipRevamp.renewNow)
    }
    
    func setViewModel(with plan: PlanDetailAndSRModel?) {
        if let plan = plan {
            validTillView.isHidden = false
            memInfoView.isHidden = false
            
            membershipID.text = "\(Strings.PlanDetailCell.memID) \(plan.membershipId ?? "")"
            planName.text = plan.planName
            
//            let numberFormatter = Utilities.decimalNumberFormatter
//            if let formattedPrice = numberFormatter.string(from: NSNumber(value:Int(round((plan.salesPrice as? Double) ?? 0.0)))) {
//                amountPaid.text = "₹ \(formattedPrice)"
//            }
            
            var planEndDate = "", planStartDate = "", memStartDate = ""
            if let endDate = plan.planValidTill as NSString? {
                planEndDate = "\(getFormattedDate(from: endDate))"
            }
            
            if let startDate = plan.planPurchaseDate as NSString? {
                planStartDate = "\(getFormattedDate(from: startDate))"
            }
            
            if let planDate = plan.planValidFrom as NSString? {
                memStartDate = "\(getFormattedDate(from: planDate))"
            }
            
            
            // Need to apply condition based on plan.isInspectionForRenew, in future i.e might need to change color of the text.
            
            if plan.canRenew {
                planBuyOrEndDate.text = "\(Strings.PlanDetailCell.validTill) \(planEndDate)"
                planBuyOrEndDate.textColor = UIColor.errorMessage
                validTillView.isHidden = true
                setRenewalViewModel(with: plan)
            } else {
                if (plan.category == Constants.Category.personalElectronics || plan.category == Strings.Global.whc || plan.category == Strings.Global.ew), plan.hasResumeClaim() {
                    memInfoView.setupUI(with: .textArrowButtonCard)
                    
                    memInfoView.setupTextArrowWithMemStatusCard(with: Strings.Global.resumeRaiseClaimText, and: nil, and: UIColor.backgroundInProgress, and: Strings.Global.resumeClaim)
                    
                }else {
                    planBuyOrEndDate.text = "\(Strings.PlanDetailCell.boughtOn) \(planStartDate)"
                    memInfoView.isHidden = true
                    validTill.text = String.init(format: Strings.PlanDetailCell.validFor, memStartDate, planEndDate)
                }
            }
            if plan.isABBMembership {
                infoLabel.isHidden = false
                infoLabel.text = Strings.PlanDetailCell.abbInfo
            } else {
                infoLabel?.removeFromSuperview()
                infoLabel?.isHidden = true

            }
            
            if let txt = plan.serviceText {
                labelServiceCount?.text = txt
            } else {
                viewServiceCount?.removeFromSuperview()
            }
        }
    }
}

extension MembershipDetailCell: MembershipInfoDelegate {
    func clickedTextArrowButtonView(_ view: MembershipInfoView, clickedTextArrowBtnView sender: Any) {
        delegate?.clickedButtonRenewalView(self, clickedBtnRenewalView: sender)
    }
}
