//
//  MembershipDetailsVC.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit


enum EHistoryStatus:Int{
    case completed = 0
    case rejected = 1
    case cancelled = 2
    case completedRatingRequired = 3
}

protocol MembershipDetailsBusinessLogic {
    func getPlanBenefits(with plan: PlanDetailAndSRModel , isRenewal: Bool)
    
    func checkAvailabilityForRaiseClaim(with model: PlanDetailAndSRModel, draftSRId: String?)
    func getHistoryPlans(with membershipID:String, isABB: Bool)
    func getAssetsForSOP(with plan: PlanDetailAndSRModel)
    
    func getMembershipClaimFileStatus(for plan: PlanDetailAndSRModel, isClaimResumed: Bool)
    
    func getInvoice(with plan: PlanDetailAndSRModel)

    func getEKit(with plan: PlanDetailAndSRModel)
}

protocol MembershipRefreshDelegate {
    func refreshMemberships()
}

class MembershipDetailsVC: BaseVC {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var raiseClaimOrMyCardButton: OAPrimaryButton!
    
    @IBOutlet var tableViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var noAssetCell: UITableViewCell!
    @IBOutlet var noSRCell: UITableViewCell!
    
    fileprivate var interactor: MembershipDetailsBusinessLogic?
    var refreshControl: UIRefreshControl?
    
    var plan: PlanDetailAndSRModel?
    var selectedSegmentIndex = 0
    var headerView: SegmentedControlHeader!
    
    var planBenefits: [String]?
    var historySRs: [HistoryModel]?
    var loaderCell: LoaderCell!
    
    var spinnerView: SpinnerView?
    var rView: OARatingView?
    var isSOPAssetsAvailable = false
    var refreshDelegate: MembershipRefreshDelegate? = nil
    var buybackStatusList: [BuyBackStatus] = []
    var customerMembershipResponse: CustomerMembershipDetailsResponseDTO?
    var invoices: [Invoice] = []
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initiliazeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeRatingView()
    }
    
    private func initiliazeView() {
        title = Strings.Global.membershipDetails
        
        tableView.register(cell: MembershipDetailCell.self)
        tableView.register(cell: LoaderCell.self)
        tableView.register(cell: DeviceDetailsCell.self)
        tableView.register(cell: ImageLabelCell.self)
        tableView.register(cell: ServiceRequestCard.self)
        
        tableView.estimatedRowHeight = 44
        tableView.estimatedSectionHeaderHeight = 44
        
        tableView.dataSource = self
        tableView.delegate = self
        
        if let plan = plan {
            if plan.isCombo || plan.category == Constants.Category.finance {
                raiseClaimOrMyCardButton.setTitle(Strings.HATabScene.myCards, for: .normal)
            } else {
                raiseClaimOrMyCardButton.setTitle( plan.isABBOnlyMembership ? Strings.Global.sellYourPhone : Strings.Global.raiseClaim, for: .normal)
            }
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cardMenu"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.onClickMoreAction(_:)))
        navigationItem.rightBarButtonItem?.tintColor = .dodgerBlue
    }
    
    fileprivate func addRefreshControl() {
        let planCategory = plan?.category ?? ""
        
        if ((planCategory != Constants.Category.finance) && selectedSegmentIndex == 2) || (planCategory == Constants.Category.finance && selectedSegmentIndex == 1) {
            if refreshControl == nil {
                refreshControl = UIRefreshControl()
                refreshControl?.addTarget(self, action: #selector(MembershipDetailsVC.handleRefresh), for: .valueChanged)
                tableView.addSubview(refreshControl!)
                tableView.sendSubviewToBack(refreshControl!)
            }
        } else {
            refreshControl?.removeTarget(self, action: #selector(MembershipDetailsVC.handleRefresh), for: .valueChanged)
            self.refreshControl?.removeFromSuperview()
            self.refreshControl = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerView.updateSize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.isUserInteractionEnabled = true
        addRefreshControl()
        if let plan = plan {
            if plan.isCombo || plan.category == Constants.Category.finance {
                showBottomView()
            } else {
                if plan.enableFileClaim {
                    showBottomView()
                } else {
                    hideBottomView()
                }
            }
        } else {
            hideBottomView()
        }
    }
    
    @objc func handleRefresh() {
        view.isUserInteractionEnabled = false
        historySRs = nil
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        tableView.endUpdates()
    }
    
    func endRefreshing(){
        view.isUserInteractionEnabled = true
        self.refreshControl?.endRefreshing()
    }
    
    private func showBottomView() {
        bottomView.isHidden = false
        tableViewBottomConstraint.isActive = true
    }
    
    private func hideBottomView() {
        bottomView.isHidden = true
        tableViewBottomConstraint.isActive = false
    }
    
    @objc func onClickMoreAction(_ sender: UIView?) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        var dataSource = [Strings.MembershipDetailPopupOption.chat]
        dropDown.shadowColor = .dlsShadow
        dropDown.shadowOpacity = 1
        dropDown.shadowOffset = CGSize(width: 0, height: 1)
        if plan?.showPaymentReceipt ?? false {
            dataSource.append(Strings.MembershipDetailPopupOption.planInvoice)
        }
        
        if plan?.showInsuranceCertificate ?? false {
            dataSource.append(Strings.MembershipDetailPopupOption.insuranceCertificate)
        }
        
        if let eKitDocId = plan?.membershipResponse?.ekitDocId, !eKitDocId.isEmpty {
            dataSource.append(Strings.MembershipDetailPopupOption.benefitsTnC)
        }
        
        dataSource.append(Strings.MembershipDetailPopupOption.faq)
        
        dropDown.dataSource = dataSource
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            EventTracking.shared.eventTracking(name: .threeDotMenu, [.location: Event.EventParams.membershipDetails, .value: item])
            switch item {
            case Strings.MembershipDetailPopupOption.chat:
                self.routeToChatScreen()
                break
            case Strings.MembershipDetailPopupOption.planInvoice:
                if !self.invoices.isEmpty {
                    self.displayInvoices(self.invoices)
                } else if let plan = self.plan {
                    Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
                    self.interactor?.getInvoice(with: plan)
                }
                break
            case Strings.MembershipDetailPopupOption.insuranceCertificate:
                self.downloadAndShowInsuranceCertificate()
                break
            case Strings.MembershipDetailPopupOption.benefitsTnC:
                if let plan = self.plan {
                    Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
                    self.interactor?.getEKit(with: plan)
                }
                break
            case Strings.MembershipDetailPopupOption.faq:
                self.routeToFaqScreen()
                break
            default:
                break
            }
        }
        
        dropDown.width = 254
        dropDown.direction = .bottom
        dropDown.anchorView = sender
        dropDown.show()
    }
    
    @IBAction func raiseAClaimOrMyCardsTapped(_ sender: Any) {
        if let plan = plan {
            
            if plan.isCombo || plan.category == Constants.Category.finance {
                Utilities.cardOpenCount()
                routeToMyCards(plan)
            }else if plan.category == Constants.Category.personalElectronics {
                if plan.isABBMembership {
                    if plan.isABBOnlyMembership {
                        routeToBuyback()
                    } else {
                        let serviceList: [ServicePickerOption] = [.sellYourPhone, .bookAService]
                        let servicePicker = ServicePickerView()
                        servicePicker.setViewModel(serviceList, delegate: self, index: 0)
                        Utilities.presentPopover(view: servicePicker, height: ServicePickerView.height(forModel: serviceList))
                    }
                } else {
                    handleClaimRaise(isClaimResumed: false)
                }
            } else {
                handleClaimRaiseForHA(plan: plan, draftSRId: nil)
            }
        }
    }
    
    func handleClaimRaise(isClaimResumed: Bool) {
        guard let plan = plan else { return }
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        interactor?.getMembershipClaimFileStatus(for: plan, isClaimResumed: isClaimResumed)
    }
    
    func handleClaimRaiseForHA(plan: PlanDetailAndSRModel, draftSRId: String?) {
        let subCat = Utilities.getProduct(productCode: plan.products?.first?.productCode ?? "")?.subCategoryName
        CacheManager.shared.eventProductName = subCat ?? ""
        
        EventTracking.shared.eventTracking(name: .raiseSR, [.location: Event.EventParams.membershipDetails, .category: plan.products?.first?.category ?? "", .subcategory: subCat ?? ""])
        
        if let assets = plan.membershipResponse?.assets, !assets.isEmpty {
            Utilities.addLoader(onView: view, message: "Please wait", count: &loaderCount, isInteractionEnabled: false)
            interactor?.checkAvailabilityForRaiseClaim(with: plan, draftSRId: draftSRId)
        } else {
            routeToGetStarted(for: plan, with: [:], draftSRId: draftSRId)
        }
    }
}

extension MembershipDetailsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            let planCategory = plan?.category ?? ""
            
            if (planCategory != Constants.Category.finance) && selectedSegmentIndex == 0 {
                return plan?.products?.count ?? 1
            } else if ((planCategory == Constants.Category.finance) && selectedSegmentIndex == 0) || ((planCategory != Constants.Category.finance) && selectedSegmentIndex == 1) {
                if let benefits = planBenefits {
                    return benefits.count
                } else {
                    return 1
                }
            } else if ((planCategory == Constants.Category.finance) && selectedSegmentIndex == 1) || ((planCategory != Constants.Category.finance) && selectedSegmentIndex == 2) {
                if let historySRs = historySRs {
                    if historySRs.count == 0 {
                        return 1
                    } else {
                        return historySRs.count
                    }
                } else {
                    return 1
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: MembershipDetailCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.delegate = self
            cell.setViewModel(with: plan)
            cell.layoutIfNeeded()
            return cell
        } else if indexPath.section == 1 {
            let planCategory = plan?.category ?? ""
            
            if (planCategory != Constants.Category.finance) && selectedSegmentIndex == 0 {
                
                if let count = plan?.products?.count, count > 0 {
                    let cell: DeviceDetailsCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    cell.setViewModel(with: plan?.products?[indexPath.row], and: plan?.category)
                    return cell
                } else if Utilities.isSOPServiceAvailable(membership: plan?.membershipResponse) && !isSOPAssetsAvailable {
                    
                    loaderCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    if let plan = plan {
                        spinnerView = SpinnerView(view: loaderCell.loaderView)
                        spinnerView?.startAnimating()
                        view.isUserInteractionEnabled = false
                        interactor?.getAssetsForSOP(with: plan)
                    }
                    return loaderCell
                } else {
                    return noAssetCell
                }
            } else if ((planCategory == Constants.Category.finance) && selectedSegmentIndex == 0) || ((planCategory != Constants.Category.finance) && selectedSegmentIndex == 1) {
                if let benefits = planBenefits {
                    let cell: ImageLabelCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    cell.changeImageView(for: indexPath.section)
                    cell.changeConstraints(with: 16, and: 24)
                    cell.backgroundColor = .white
                    cell.descriptionView.text = benefits[indexPath.row]
                    return cell
                } else {
                    loaderCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    if let plan = plan {
                        spinnerView = SpinnerView(view: loaderCell.loaderView)
                        spinnerView?.startAnimating()
                        view.isUserInteractionEnabled = false
                        interactor?.getPlanBenefits(with: plan, isRenewal: false)
                    }
                    return loaderCell
                }
            } else if ((planCategory == Constants.Category.finance) && selectedSegmentIndex == 1) || ((planCategory != Constants.Category.finance) && selectedSegmentIndex == 2) {
                // This is for History Timeline thing.
                if let historySRs = historySRs {
                    if historySRs.count == 0 {
                        return noSRCell
                    } else {
                        let cell: ServiceRequestCard = tableView.dequeueReusableCell(indexPath: indexPath)
                        cell.delegate = self
                        cell.setHistoryViewModel(with: historySRs[indexPath.row])
                        cell.layoutIfNeeded()
                        return cell
                    }
                } else {
                    loaderCell = tableView.dequeueReusableCell(indexPath: indexPath)
                    if let plan = plan {
                        spinnerView = SpinnerView(view: loaderCell.loaderView)
                        spinnerView?.startAnimating()
                        view.isUserInteractionEnabled = false
                        self.interactor?.getHistoryPlans(with: plan.membershipId ?? "", isABB: plan.isABBMembership)
                    }
                    return loaderCell
                }
            }
        }
        
        return UITableViewCell()
    }
}

extension MembershipDetailsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            if headerView == nil {
                UILabel.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).numberOfLines = 0
                headerView = Bundle.main.loadNibNamed(Constants.NIBNames.segmentedControlHeader, owner: nil, options: nil)?.first as? SegmentedControlHeader
                headerView.planCategory = plan?.category ?? ""
                headerView.setupSegmentedControl(with: selectedSegmentIndex)
                if plan?.enableFileClaim == false,
                    let serviceRequest = plan?.serviceRequests, serviceRequest.count == 0, !(plan?.isABBMembership ?? false) {
                    headerView?.segmentControl.removeSegment(at: 2, animated:true)
                }
                headerView?.segmentControl.selectedSegmentIndex = selectedSegmentIndex
                
                headerView?.delegate = self
            } else {
                headerView?.segmentControl.selectedSegmentIndex = selectedSegmentIndex
                headerView.changeBottomView(with: selectedSegmentIndex)
            }
            
            return headerView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
}

// MARK:- Configuration Logic
extension MembershipDetailsVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = MembershipDetailsInteractor()
        let presenter = MembershipDetailsPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- MembershipDetailCellDelegate Conformance
extension MembershipDetailsVC: MembershipDetailCellDelegate {
    func clickedButtonRenewalView(_ cell: MembershipDetailCell, clickedBtnRenewalView sender: Any) {
        if let plan = plan {
            if plan.category == Constants.Category.personalElectronics {
                if plan.hasResumeClaim() {
                    handleClaimRaise(isClaimResumed: true)
                } else {
                    showAlert(title: Strings.Global.infoMessage, message: Strings.Global.renewalNotAllowed, primaryButtonTitle: Strings.Global.chatWithUs, nil, primaryAction: {
                        self.present(ChatVC(), animated: true, completion: nil)
                    }, nil)
                }
            } else if plan.category == Constants.Category.finance {
                Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
                PaymentWebViewManager().initializeView(membershipDetail: plan, addressInfo: nil) { (url, error) in
                    Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
                    if let url = url {
                        let paymentVC = OAPaymentVC()
                        paymentVC.addToInitialProperties(["data": url, "service_Type": "wallet", "hasEW": false, "isRenewal": true])
                        self.present(paymentVC, animated: true, completion: nil)
                    } else {
                        self.showAlert(title: Strings.Global.error, message: error ?? "")
                    }
                }
            } else if plan.category == Strings.Global.whc {
                if plan.hasResumeClaim() {
                    handleClaimRaiseForHA(plan: plan, draftSRId: plan.draftMembershipID())
                } else {
                    routeToRenewalPincodeManager(with: plan)
                }
            } else if plan.category == Strings.Global.ew {
                if plan.hasResumeClaim() {
                    handleClaimRaiseForHA(plan: plan, draftSRId: plan.draftMembershipID())
                } else {
                    showAlert(message: RemoteConfigManager.shared.renewMessageHA)
                }
            }
        }
    }
}

// MARK:- ServiceRequestCardDelegate Conformance
extension MembershipDetailsVC: ServiceRequestCardDelegate {
    
    func clickedButtonViewDetails(_ cell: ServiceRequestCard, clickedBtnViewDetails sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            if let historyModel = historySRs?[indexPath.row] {
                if Utilities.isSrTypeBuyback(value: historyModel.serviceType) {
                    routeToBuyback()
                } else {
                    routeToTimeline(srId: historyModel.srId, claimType: historyModel.claimType, crmTrackingNumber: historyModel.crmTrackingNumber)
                }
            }
        }
    }
    
    func clickedTextArrowButtonView(_ cell: ServiceRequestCard, clickedTextArrowBtnView sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            if let historyModel = historySRs?[indexPath.row] {
                
                CacheManager.shared.srType = historyModel.claimType
                
                if Utilities.isSrTypeBuyback(value: historyModel.serviceType) {
                    routeToBuyback()
                } else if historyModel.status == .rejected || historyModel.status == .cancelled || historyModel.status == .completed {
                    routeToTimeline(srId: historyModel.srId, claimType: historyModel.claimType, crmTrackingNumber: historyModel.crmTrackingNumber)
                } else if historyModel.status == .completedRatingRequired {
                    // rating popup would come as bottom sheet. This could never come in wallet case because no SR concept in Wallet case.
                    if plan?.category == Strings.Global.whc {
                        showRatingView(for: historyModel.memId, and: historyModel.srId)
                    } else {
                        /* It means in PE and EW case we will not show renewal card at the time of rating if membership is in renewal period. As of now renewal is not there in EW. So only PE case is left now. May handle this in future. */
                        showRatingView(for: nil, and: historyModel.srId)
                    }
                }
            }
        }
    }
    
    func clickedButtonCall(_ cell: ServiceRequestCard, clickedBtnCall sender: Any) {
        Utilities.callCustomerCare()
    }
    
    func clickedDownloadInvoice(_ cell: ServiceRequestCard, clickedBtnDownloadInvoice sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            if let historyModel = historySRs?[indexPath.row] {
                let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(historyModel.srId)/documents?reportType=JOBSHEET"
                if let url = URL(string: completeUrl) {
                    let fileName = "INVOICE_SR_\(historyModel.srId).pdf"
                    FileDownloader.fileOpeninWebView(url: url, fileName: fileName, headerTitle: Strings.Global.invoice, message: Strings.Global.invoiceDownloaded)
                }
            }
        }
    }
    
    func downloadAndShowInvoice(_ invoice: Invoice) {
        if let plan = plan {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
            let url = Constants.MembershipUrls.invoiceDownload + "?fileType=pdf&taxType=GST&membershipType=\(plan.membershipResponse?.membershipStatus ?? "")&docType=\(invoice.docType?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")&orderId=\(invoice.orderId ?? "")"
            downloadFile(url, fileName: "Invoice_\(invoice.orderId ?? "").pdf") { (destinationPath) in
                Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
                if let destinationPath = destinationPath {
                    EventTracking.shared.eventTracking(name: .invoiceDownloaded, [.location: Event.EventParams.membershipDetails, .bpName: Constants.SchemeVariables.bussPartnerName, .buName: Constants.SchemeVariables.busUnitName, .planName: self.plan?.membershipResponse?.plan?.planName ?? "", .planCode: self.plan?.membershipResponse?.plan?.planCode ?? ""])
                    self.openWebView(url: destinationPath, name: "Invoice")
                } else {
                    self.showAlert(title: "No invoice to download", message: nil, primaryButtonTitle: "Ok", nil, isCrossEnable: false, logoImage: #imageLiteral(resourceName: "no_document"), isLargeLogo: true, primaryAction: {}, nil)
                }
            }
        }
    }
    
    func downloadAndShowInsuranceCertificate() {
        if let plan = plan {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
            let url = Constants.MembershipUrls.insuranceCertificateDownload + "?memId=\(plan.membershipId ?? "")&custId=\(UserCoreDataStore.currentUser?.cusId ?? "")"
            downloadFile(url, fileName: "InsuranceCertificate_\(plan.membershipId ?? "").zip") { (destinationPath) in
                Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
                if let destinationPath = destinationPath {
//                    self.openWebView(url: destinationPath, name: "Insurance Certificate")
                    self.shareFile(filePath: destinationPath)
                } else {
                    self.showAlert(title: "No insurance certificate to download", message: nil, primaryButtonTitle: "Ok", nil, isCrossEnable: false, logoImage: #imageLiteral(resourceName: "no_document"), isLargeLogo: true, primaryAction: {}, nil)
                }
            }
        }
    }
    
    func downloadFile(_ urlPath: String, fileName: String, callback: @escaping (String?) -> ()) {
        guard let url = URL(string: urlPath) else {
            callback(nil)
            return
        }
        
        FileDownloader.loadFileAsync(url: url, fileName: fileName) { (path, error) in
            print("PDF File downloaded to : \(path ?? "")")
            DispatchQueue.main.async {
                callback(error == nil ? path : nil)
            }
        }
    }
}

// MARK:- SegmentedControlHeaderDelegate Conformance
extension MembershipDetailsVC: SegmentedControlHeaderDelegate {
    func clickedSegmentIndex(_ segmentedControl: UISegmentedControl) {
        selectedSegmentIndex = segmentedControl.selectedSegmentIndex
        addRefreshControl()
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        tableView.endUpdates()
    }
}

// MARK:- Display Logic Conformance
extension MembershipDetailsVC: MembershipDetailsDisplayLogic {
    
    func displayChatDialogue() {
        //TODO :- get code from master if present
    }
    
    func displaySOPAssets(plan: PlanDetailAndSRModel) {
        
        spinnerView?.stopAnimating()
        view.isUserInteractionEnabled = true
        
        self.plan = plan
        isSOPAssetsAvailable = true
//        tableView.reloadData()
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        tableView.endUpdates()
    }
    
    func displayPlanBenefits(benefits: [String], _ isRenewal: Bool, and plan: PlanDetailAndSRModel) {
        spinnerView?.stopAnimating()
        view.isUserInteractionEnabled = true
        if isRenewal {
            // May be we can use in future
        } else {
            self.planBenefits = benefits
            tableView.beginUpdates()
            tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
            tableView.endUpdates()
        }
    }
    
    func displayError(_ error: String) {
        view.isUserInteractionEnabled = true
        spinnerView?.stopAnimating()
        Utilities.removeLoader(count: &loaderCount)
        
        if let refreshControl = refreshControl, refreshControl.isRefreshing {
            endRefreshing()
        }
        
        showAlert(title: Strings.Global.error, message: error)
    }
    
    func displayAvailabilityForRaiseClaim(for model: PlanDetailAndSRModel, isServiceable: Bool, with assetsAvailability: [String : AssetEligibility]?, draftSRId: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        routeToGetStarted(for: model, with: assetsAvailability!, draftSRId: draftSRId)
    }
    
    func displayHistoryModel(model:[HistoryModel]){
        spinnerView?.stopAnimating()
        view.isUserInteractionEnabled = true
        
        if let refreshControl = refreshControl, refreshControl.isRefreshing {
            endRefreshing()
        }
        
        self.historySRs = model
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        tableView.endUpdates()
    }
    
    
    func displayEligibilityForRaiseClaim(for model: PlanDetailAndSRModel, isClaimResumed: Bool, isEligible: Bool) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if isEligible {
            let chatVC = ChatVC()
            let ftrData = isClaimResumed ? [
                "membership_id": model.membershipId ?? "",
                "actionType": "RESUME_SR",
                "draftSrId": model.draftMembershipID() ?? ""
                ] : [
                    "membership_id": model.membershipId ?? "",
                    "actionType": "NEW_SR"
            ]
            chatVC.addToInitialProperties(ftrData)
            present(chatVC, animated: true, completion: nil)
        } else {
            showAlert(message: Strings.Global.claimNotAllowed, primaryButtonTitle: Strings.Global.okay, nil, primaryAction: {
                self.refreshDelegate?.refreshMemberships()
                self.navigationController?.popViewController(animated: true)
            }, nil)
        }
    }
    
    func displayInvoices(_ invoices: [Invoice]) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.invoices = invoices
        if !invoices.isEmpty {
            let invoicePicker = InvoicePickerView()
            invoicePicker.setViewModel(invoices, delegate: self)
            Utilities.presentPopover(view: invoicePicker, height: InvoicePickerView.height(forModel: invoices))
        } else {
            showAlert(title: "No invoice to download", message: nil, primaryButtonTitle: "Ok", nil, isCrossEnable: false, logoImage: #imageLiteral(resourceName: "no_document"), isLargeLogo: true, primaryAction: {}, nil)
        }
    }
    
    func displayDocumentUrl(_ url: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        downloadFile(url, fileName: "EKIT_DOC_\(plan?.membershipResponse?.ekitDocId ?? "").pdf") {(destinationPath) in
            Utilities.removeLoader(fromView: self.view, count: &self.loaderCount)
            if let destinationPath = destinationPath {
                self.openWebView(url: destinationPath, name: "Benefits and T&Cs")
                EventTracking.shared.eventTracking(name: .ekitDownloaded, [.location: Event.EventParams.membershipDetails, .bpName: Constants.SchemeVariables.bussPartnerName, .buName: Constants.SchemeVariables.busUnitName, .planName: self.plan?.membershipResponse?.plan?.planName ?? "", .planCode: self.plan?.membershipResponse?.plan?.planCode ?? ""])
            } else {
                self.showAlert(title: "No file to download", message: nil, primaryButtonTitle: "Ok", nil, isCrossEnable: false, logoImage: #imageLiteral(resourceName: "no_document"), isLargeLogo: true, primaryAction: {}, nil)
            }
        }
    }
}

extension MembershipDetailsVC: ServicePickerDelegate {
    func selectedService(_ service: ServicePickerOption?, index: Int) {
        Utilities.topMostPresenterViewController.dismiss(animated: false) {
            switch service {
            case .sellYourPhone:
                self.routeToBuyback()
            case .bookAService:
                self.handleClaimRaise(isClaimResumed: false)
            default:
                break
            }
        }
    }
}

// MARK:- Routing Logic
extension MembershipDetailsVC {
    
    /* I have to use this in case of Wallet only. */
    func routeToMyCards(_ plan: PlanDetailAndSRModel) {
        EventTracking.shared.eventTracking(name: .myCards, [.location: Event.EventParams.membershipDetails])
        present(CardManagementVC(), animated: true, completion: nil)
    }
    
    func showRatingView(for memID: String?, and srId: String) {
        rView = OARatingView()
        
        if let rView = rView {
            rView.membershipId = memID
            rView.srId = srId
            rView.ratingViewController = self
            rView.initialSetup()
            self.addOABottomView(rView, isForRating: true)
        }
    }
    
    func removeRatingView() {
        if let rView = rView {
            if let storeRv = rView.storeRv {
                self.removeOABottomView(storeRv, self.view)
            }
            
            if let renewalView = rView.rv {
                self.removeOABottomView(renewalView, self.view)
            }
            
            self.removeOABottomView(rView, self.view)
        }
    }
    
    func routeToRenewalPincodeManager(with membershipDetails: PlanDetailAndSRModel?) {
        let vc = BuyPlanJourneyVC()
        let customer = membershipDetails?.customer?.first
        let pinCode = customer?.addresses?.filter({$0.addressType == "INSPECTION"}).first?.pincode ?? customer?.addresses?.first?.pincode
        var customerInfoDict = Serialization.getDictionaryFromObject(object: customer)
        customerInfoDict["previousCustId"] = membershipDetails?.customer?.first?.custId
        vc.addToInitialProperties(["routeName":"PinCodeScreen","data":["buyPlanJourney":["category": Destination.whc.category,"service": Constants.PlanServices.whcInspection,"screenTitle":"","serviceName": Constants.PlanServices.whcInspection,"renewalData":["customerInfo": customerInfoDict,"memID":membershipDetails?.membershipId,"prevPlanCode":membershipDetails?.planCode,"prevCoverAmount":membershipDetails?.coverAmount,"pinCode": pinCode]]]])
        self.present(vc, animated: true, completion: nil)
    }
    
    func routeToGetStarted(for model: PlanDetailAndSRModel, with assetsAvailability: [String : AssetEligibility], draftSRId: String?) {
        
        let vc = GetStartedVC()
        vc.memResponse = model.membershipResponse
        if model.products?.first?.category != Constants.Category.finance {
            ClaimHomeServe.shared.membershipId = model.membershipId
            ClaimHomeServe.shared.draftSRId = draftSRId
            ClaimHomeServe.shared.productCode = model.products?.first?.productCode
            ClaimHomeServe.shared.assetIds = [model.products?.first?.assetId?.stringValue ?? ""]
            ClaimHomeServe.shared.assetAvailability = assetsAvailability
        }
        ClaimHomeServe.shared.setUpAdditionalValue(invDocMismatchL: model.getInvDocNameMismatch(), assetAtribute: model.getAssetAttributes())
        var productName : String?
        if let brand = model.products?.first?.brand,let name =  model.products?.first?.productName{
            productName = brand + " " + name
        }
        if productName == nil{
            if let brand = model.products?.first?.brand{
                productName = brand
            }
            if let name = model.products?.first?.productName {
                productName = name
            }
        }
        
        var serialNo: String?
        
        if model.products?.first?.category == Constants.Category.personalElectronics {
            if model.products?.first?.productCode == Constants.VoucherConstants.mobileMP {
                serialNo = "IMEI \(model.products?.first?.serialNo ?? "")"
            } else {
                serialNo = "Sr. No. \(model.products?.first?.serialNo ?? "")"
            }
        }
        
        if let date = Int(model.planPurchaseDate ?? "") {
            ClaimHomeServe.shared.purchaseDate = NSNumber(value: date)
        }
        
        vc.updateInfo(plan: model.planName ?? "", product: productName, applianceCount: (model.products?.count ?? 0 > 1 ? model.products?.count.description : nil), serialNo: serialNo)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToTimeline(srId: String, claimType: Constants.Services,crmTrackingNumber: String?) {
        
        EventTracking.shared.eventTracking(name: .SRDetail ,[.srNumber: srId])
        var timeline: TimelineBaseVC?
         timeline = TimelineBaseVC()

        if let timeline = timeline {
            timeline.srId = srId
            timeline.boolHistoryTimeLine = true
            timeline.crmTrackingNumber = crmTrackingNumber
            timeline.eventSubCat = CacheManager.shared.eventProductName ?? ""
            self.navigationController?.pushViewController(timeline, animated: true)
        }
        EventTracking.shared.addScreenTracking(with: .SR_Timeline)
    }
    
    func routeToBuyback() {
        let data = buybackStatusList
        if let currrentData = data.first(where: {$0.deviceInfo?.deviceIdentifier == UIDevice.uuid}), data.count == 1 {
            let isSupported = currrentData.buyBackStatus != BUYBACK_STATUS.MODEL_NOT_FOUND.rawValue
            let buyBackStatus = currrentData.buyBackStatus ?? ""
            EventTracking.shared.eventTracking(name: .mobileBuybackIntent, [.location: "Membership Detail Screen", .brandAndModel: "Apple \(UIDevice.currentDeviceModel)", .supported: isSupported ? "YES" : "NO", .Stage: buyBackStatus])
        }
        if let buyback_status_list = try? JSONParserSwift.getJSON(object: data) {
            let buybackVC = BuybackVC()
            var mems: [Any] = customerMembershipResponse?.data?.memberships ?? []
            mems.append(contentsOf: customerMembershipResponse?.data?.pendingMemberships ?? [])
            let membershipList = (try? JSONParserSwift.getJSON(object: mems)) ?? ""
            buybackVC.addToInitialProperties(["buyback_status_list": buyback_status_list, "membership_id": plan?.membershipId ?? "", "membershipList": membershipList])
            present(buybackVC, animated: true, completion: nil)
        }
    }

    func routeToChatScreen() {
        EventTracking.shared.eventTracking(name: .chat ,[.location: "Membership Detail Screen"])
        EventTracking.shared.eventTracking(name: .chatonMembershipDetailTapped)
        present(ChatVC(), animated: true, completion: nil)
        EventTracking.shared.addScreenTracking(with: .chat)
    }
    
    func routeToFaqScreen() {
        EventTracking.shared.eventTracking(name: .FAQ ,[.location: "Membership Detail Screen"])
        let webViewVC = WebViewVC()
        webViewVC.screenTitle = Strings.SalesRevamp.faq
        webViewVC.urlString = RemoteConfigManager.shared.planDetailFaq ?? ""
        navigationController?.pushViewController(webViewVC, animated: true)
    }
    
    func openWebView(url: String, name: String) {
        let webViewVC = WebViewVC()
        webViewVC.isLinkWebView = true
        webViewVC.isShowShareOption = true
        webViewVC.screenTitle = name
        webViewVC.urlString = url
        navigationController?.pushViewController(webViewVC, animated: true)
    }
    
    func shareFile(filePath: String) {
        if let url = URL(string: filePath) {
            let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            present(activityViewController, animated: true, completion: nil)
        }
    }
}

extension MembershipDetailsVC: InvoicePickerDelegate {
    func selectedInvoice(_ invoice: Invoice?) {
        Utilities.topMostPresenterViewController.dismiss(animated: false) {
            if let invoice = invoice {
                self.downloadAndShowInvoice(invoice)
            }
        }
    }
}
