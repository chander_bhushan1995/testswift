//
//  ImageLabelCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 11/01/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

private let bulletPoint = "bulletPoint"
private let tick = "tick"

class ImageLabelCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var img: UIImageView!
    //@IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionView: UITextView!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionTopConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        descriptionView.textAlignment = .left
        descriptionView.font = DLSFont.bodyText.regular
        descriptionView.textColor = UIColor.charcoalGrey
        descriptionView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
    
    override func prepareForReuse() {
        imageLeadingConstraint.constant = 30
        imageTopConstraint.constant = 9
        descriptionTopConstraint.constant = 10
        
        backgroundColor = UIColor.clear
    }
    
    func changeImageView(for section: Int) {
        if section == 0 {
            imageWidth.constant = 4
            imageHeight.constant = 4
            img.image = #imageLiteral(resourceName: "bulletPoint")
        } else {
            imageWidth.constant = 8
            imageHeight.constant = 8
            img.image = #imageLiteral(resourceName:  tick)
        }
    }
    
    func changeConstraints(with leadingConstant: Int, and topConstant: Int) {
        imageLeadingConstraint.constant = leadingConstant.cgFloat
        imageTopConstraint.constant = (topConstant - 2).cgFloat
        
        descriptionTopConstraint.constant = topConstant.cgFloat
    }
}
