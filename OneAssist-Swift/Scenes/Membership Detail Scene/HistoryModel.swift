//
//  HistoryModel.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 09/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

enum EHistoryCategory{
    case eRating
    case eCall
    case eFeedback
}

class HistoryModel: Row {
    var deviceName:String?
    var serviceID:String?
    var status:EHistoryStatus?
    var detailStatus:String?
    var isSelected:Bool = false
    var cellType:EHistoryCategory?
    var workflowStageCode :String?
    var workflowStatusCodeStatus:String?
    var subtitle:String?
    var imgName:String?
    var hasViewAllButton:Bool?
    var hasDownloadInvoiceButton: Bool = false
    var assets: [String] = []
    var srId = ""
    var memId = ""
    var claimType: Constants.Services = .accidentalDamage
    var crmTrackingNumber: String?
    var location: String?
    var serviceType: String?
}
