//
//  DeviceDetailsCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class DeviceDetailsCell: UITableViewCell,ReuseIdentifier,NibLoadableView {
    
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var deviceInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setViewModel(with productInfo: Product?, and category: String?) {
        if let product = productInfo, let category = category {
            if category == Strings.Global.whc  {
                deviceName.text = "\(product.brand ?? "") \(product.productName ?? "")"
                deviceInfo.text = "\(product.technology ?? "") - \(product.size ?? "") \(product.sizeUnit ?? ""), Serial no. \(product.serialNo ?? "")"
            } else if category == Constants.Category.personalElectronics || category == Strings.Global.ew {
                if category == Constants.Category.personalElectronics {
                    deviceName.text = "\(product.brand ?? "") \(product.model ?? "")"
                } else {
                    deviceName.text = "\(product.brand ?? "") \(product.productName ?? "")"
                }
                
                deviceInfo.text = "Serial no. \(product.serialNo ?? "")"
            }
        }
    }
}
