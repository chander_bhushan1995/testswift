//
//  LoaderCell.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 30/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class LoaderCell: UITableViewCell,ReuseIdentifier,NibLoadableView {
    
    @IBOutlet weak var loaderView: UIView!
    
}
