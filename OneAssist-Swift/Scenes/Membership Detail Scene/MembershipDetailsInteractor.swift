//
//  MembershipDetailsInteractor.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 30/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MembershipDetailsPresentationLogic {
    func presentBenefits(response: PlanBenefitsResponseDTO?, error: Error?,_ isRenewal: Bool, with plan: PlanDetailAndSRModel)
    func availabilityReceivedForRaiseClaim(for model: PlanDetailAndSRModel, draftSRId: String?, response: CheckAvailabilityResponseDTO?, error: Error?)
    func presentHistoryModel(response: SearchServiceRequestPlanResponseDTO?, error: Error?)
    func presentServicedAsset(response: GetServicedAssetResponseDTO?, error: Error?, with plan: PlanDetailAndSRModel)
    func presentCheckClaimEligibilty(with plan: PlanDetailAndSRModel, isClaimResumed: Bool, response: CustomerMembershipDetailsResponseDTO?, error: Error?)
    func presentInvoices(response: DownloadInvoiceResponseDTO?, error: Error?)
    func presentDocumentUrl(response: DocumentUrlResponseDTO?, error: Error?)
}

class MembershipDetailsInteractor: BaseInteractor, MembershipDetailsBusinessLogic {
    var presenter: MembershipDetailsPresentationLogic?
    
    func getPlanBenefits(with plan: PlanDetailAndSRModel,isRenewal: Bool) {
        let planBenefitsRequestModel = PlanBenefitsRequestDTO(planList: plan.planCode?.description ?? "")
        let _ = PlanBenefitsUseCase.service(requestDTO: planBenefitsRequestModel) {[weak self] (service, response, error) in
            self?.presenter?.presentBenefits(response: response, error: error, isRenewal, with: plan)
        }
    }
    

    func getAssetsForSOP(with plan: PlanDetailAndSRModel) {
        let assetListReq = GetServicedAssetRequestDTO(memId: plan.membershipId, prodCode: nil)
        let _ = GetServicedAssetRequestUseCase.service(requestDTO: assetListReq) {[weak self] (service, response, error) in
            self?.presenter?.presentServicedAsset(response: response, error: error, with: plan)
        }
    }
    
    func checkAvailabilityForRaiseClaim(with model: PlanDetailAndSRModel, draftSRId: String?) {
        if let memId = model.membershipId {
            let request = CheckAvailabilityRequestDTO(memId: memId)
            
            CheckAvailabilityRequestUseCase.service(requestDTO: request, completionBlock: { (usecase, response, error) in
                self.presenter?.availabilityReceivedForRaiseClaim(for: model, draftSRId: draftSRId, response: response, error: error)
            })
        }
            //TODO: I think below condition is not required because if there is not membership
        else {
            presenter?.availabilityReceivedForRaiseClaim(for: model, draftSRId: draftSRId, response: nil, error: nil)
        }
    }
    
    func getHistoryPlans(with membershipID:String, isABB: Bool) {
        let obj:HistoryServiceRequestPlanRequestDTO = HistoryServiceRequestPlanRequestDTO(memberships: [membershipID], isABB: isABB)
        
//        if historyRequestUseCase != nil {
//            historyRequestUseCase.cancel()
//        }
        
        let _ = HistoryServiceRequestPlanRequestUseCase.service(requestDTO:obj) {
            [weak self] (service,response,error) in
            self?.presenter?.presentHistoryModel(response: response,error:error)
        }
    }
    
    func getMembershipClaimFileStatus(for plan: PlanDetailAndSRModel, isClaimResumed: Bool) {
        var membershipUseCase: MembershipListUseCase? = MembershipListUseCase()
        membershipUseCase?.getMembershipDataForMembershipId(plan.membershipId) { [weak self] (response, error) in
            membershipUseCase = nil
            self?.presenter?.presentCheckClaimEligibilty(with: plan, isClaimResumed: isClaimResumed, response: response, error: error)
        }
    }
    
    func getInvoice(with plan: PlanDetailAndSRModel) {
        let request = DownloadInvoiceRequestDTO.init(orderId: plan.membershipResponse?.orderId?.stringValue ?? "", membershipType: plan.membershipResponse?.membershipStatus ?? "")
        let _ = DownloadInvoiceUseCase.service(requestDTO: request) { [weak self] (usecase, response, error) in
            self?.presenter?.presentInvoices(response: response, error: error)
        }
    }

    func getEKit(with plan: PlanDetailAndSRModel) {
        let request = DocumentUrlRequestDTO(documentId: plan.membershipResponse?.ekitDocId ?? "")
        let _ = DocumentUrlUseCase.service(requestDTO: request) { [weak self] (usecase, response, error) in
            self?.presenter?.presentDocumentUrl(response: response, error: error)
        }
    }
}
