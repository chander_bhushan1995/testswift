//
//  InspectionDestailsVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionDestailsBusinessLogic {
    func getInspectionDetails(withRefresh:Bool,andShowLoader:Bool,_ serviceID:Int)
    func getTechnicianDetail(technicianId : String)
    func getFeedbacksForRatings(rating: String)
    func submitFeedBack(_ requestID:String,_ request:FeedbackRequest)
    func getOTP(_ id:Int)
}

protocol InspectionDestailsVCDelegate {
    func positiveConfirmationTapped()
    func negativeConfirmationTapped()
}

class InspectionDestailsVC: BaseVC {
    var interactor: InspectionDestailsBusinessLogic?
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet var chatView: UIView!
    @IBOutlet var confirmationView: UIView!
    @IBOutlet weak var confirmationViewTitleLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var confirmationViewNegativeButton: OASecondaryButton!
    @IBOutlet weak var confirmationViewPositiveButton: OAPrimaryButton!
    @IBOutlet var ratingView: UIView!
    @IBOutlet weak var ratingContainerView: UIView!
    var feedbackSubmitedCompletion : (()->Void)?
    
    var delegate: InspectionDestailsVCDelegate?
    
    @IBOutlet weak var ratingSubmitButton: OAPrimaryButton!
    @IBOutlet weak var chatLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var overAllExpericeLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var onAscaleLabel: SupportingTextRegularGreyLabel!
    
    var inspectionDetails = [Any]()
    var selectedSection: Int = 0
    var selectedRating: Int = 0
    var refreshController: UIRefreshControl?
    var isConfirmationViewForRating = false
    var serviceDetail: InspectionDetail?
    var technicianDetail: TechnicialDetail?
    var technicianStatusTimer: Timer?
    var finishStatusTimer: Timer?
    var lastRefreshTime: Date?
    var isTechnicianReached = false
    var serviceRequestId = 0
    var section:Int = -1
    var expanded:Bool = false
    var request :FeedbackRequest?
    var location: String?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        setUpUI()
    }
    
    func setUpUI()
    {
        confirmationViewTitleLabel.font = DLSFont.h3.bold
        confirmationViewNegativeButton.titleLabel?.font = DLSFont.h3.regular
        confirmationViewPositiveButton.titleLabel?.font = DLSFont.h3.regular
        confirmationViewTitleLabel.textColor = UIColor.charcoalGrey
        confirmationViewNegativeButton.titleLabel?.textColor = UIColor.gray
        confirmationViewPositiveButton.titleLabel?.textColor = UIColor.dodgerBlue
        chatLabel.font = DLSFont.h3.bold
        chatLabel.textColor = UIColor.charcoalGrey
        overAllExpericeLabel.font = DLSFont.h3.bold
        overAllExpericeLabel.textColor = UIColor.charcoalGrey
        onAscaleLabel.font = DLSFont.supportingText.regular
        onAscaleLabel.textColor = UIColor.gray
        ratingSubmitButton.titleLabel?.textColor = UIColor.dodgerBlue
        
    }
    @IBAction func buttonclkLiveChat(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .chatFromInspectionTapped)
        present(ChatVC(), animated: true, completion: nil)
    }
    
    
    @IBAction func clickedBtnRating(_ sender: Any) {
        let ratingButton = sender as? UIButton
        let tag: Int = (ratingButton?.tag)!
        for i in 1...tag {
            let button = ratingContainerView.viewWithTag(i) as? UIButton
            button?.isSelected = true
        }
        if(tag+1 <= 5){
            for i in tag + 1...5 {
                let button = ratingContainerView.viewWithTag(i) as? UIButton
                button?.isSelected = false
            }
        }
        selectedRating = tag
        checkAndActivateRatingSubmitButton()
    }
    
    
    // MARK:- private methods
    private func initializeView() {
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        registerCells()
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 44
        tableViewObj.sectionFooterHeight = CGFloat.leastNormalMagnitude
        tableViewObj.tableFooterView = UIView()
        selectedSection = -1
        // Pull to refresh
        refreshController = UIRefreshControl()
        refreshController?.addSpinnerView()
        refreshController?.addTarget(self, action: #selector(self.handleRefresh), for: .valueChanged)
        tableViewObj.insertSubview(refreshController ?? UIView(), at: 0)
        self.initializeData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActiveOnViewContoller), name: UIApplication.didBecomeActiveNotification, object: nil)
        //calling api
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        self.interactor?.getInspectionDetails(withRefresh:false,andShowLoader:true, serviceRequestId)
        feedbackSubmitedCompletion = {
            self.setRatingIsSubmittedWithServiceRequest(self.serviceRequestId.description)
            self.initializeStoreRatingView()
        }
        self.title = Strings.WHC.titles.inspectionDetailsTitles
        if let srType = CacheManager.shared.srType {
            if srType == .extendedWarranty {
                location = "Home EW Service"
            } else if srType.getCategoryType() == Constants.Category.homeAppliances {
                location = "HomeAssist Service"
            } else {
                location = "Mobile Service"
                
                if let eventCat = CacheManager.shared.eventProductName {
                    location = eventCat + " Service"
                }
            }
        }
    }
    
    @objc func handleRefresh(_ sender: Any) {
        self.interactor?.getInspectionDetails(withRefresh: true, andShowLoader: false,serviceRequestId)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if technicianStatusTimer != nil {
            technicianStatusTimer?.invalidate()
            technicianStatusTimer = nil
        }
        if finishStatusTimer != nil {
            finishStatusTimer?.invalidate()
            finishStatusTimer = nil
        }
    }
    
    @objc func didBecomeActiveOnViewContoller() {
        if lastRefreshTime == nil {
            self.interactor?.getInspectionDetails(withRefresh: false, andShowLoader: false,serviceRequestId)
            return
        }
        let currentTime = Date.currentDate()
        let components: DateComponents? = gregorianCalendar.dateComponents([.minute], from: lastRefreshTime!, to: currentTime)
        if (components?.minute)! >= Strings.WHC.inspection.inspectionDetails.ForegroundRefreshRate {
            self.interactor?.getInspectionDetails(withRefresh: false, andShowLoader: false,serviceRequestId)
        }
    }
    
    
    func initializeData() {
        if serviceDetail == nil {
            let model = InspectionDetailViewModel()
            model.heading = Strings.WHC.inspection.inspectionDetails.FirstHeader
            model.detailState = .notStarted
            let model2 = InspectionDetailViewModel()
            model2.heading = Strings.WHC.inspection.inspectionDetails.SecondHeader
            model2.detailState = .notStarted
            let model3 = InspectionDetailViewModel()
            model3.heading = Strings.WHC.inspection.inspectionDetails.ThirdHeader
            model3.detailState = .notStarted
            let model4 = InspectionDetailViewModel()
            model4.heading = Strings.WHC.inspection.inspectionDetails.FourthHeader
            model4.detailState = .notStarted
            inspectionDetails = [model, model2, model3, model4]
        } else {
            // If inspection cancelled.
            if let cancelationReason = serviceDetail?.cancelationReason, !cancelationReason.isEmpty {
                showAlert(message: cancelationReason, primaryButtonTitle: Constants.WHC.alertMessages.okay, nil, primaryAction: { [weak self] in
                    self?.setTabAsRootNew()
                }, {})
                return
            }
            
            let model = InspectionDetailViewModel()
            model.heading = Strings.WHC.inspection.inspectionDetails.FirstHeader
            model.detailState = .completed
            let model2 = InspectionDetailViewModel()
            model2.heading = Strings.WHC.inspection.inspectionDetails.SecondHeader
            isTechnicianReached = !isConfirmationDialogToShow()
            if (serviceDetail?.assignee == "") {
                // No technician alloted.
                model2.detailState = .notStarted
            }
            else if (serviceDetail?.inspectionStartTime == "") {
                if isTechnicianReached {
                    // User says technician reached.
                    model2.detailState = .completed
                }
                else {
                    // Inspection not started yet.
                    model2.detailState = .started
                    checkForTechnicianAndStartTimer()
                }
            }
            else {
                // Inspection started.
                model2.detailState = .completed
                // Delete flag from user defaults
                let key = "\(self.serviceRequestId )_\(serviceDetail?.serviceScheduleStarttime ?? "")"
                UserDefaults.standard.removeObject(forKey: key)
            }
            
            let model3 = InspectionDetailViewModel()
            model3.heading = Strings.WHC.inspection.inspectionDetails.ThirdHeader
            model3.detailState = .notStarted
            if ((!(serviceDetail?.inspectionStartTime == "") && (serviceDetail?.inspectionEndTime == "")) || isTechnicianReached) {
                // Inspection started.
                model3.detailState = .started
                initializeCustomerSupportView()
            }
            let model4 = InspectionDetailViewModel()
            model4.heading = Strings.WHC.inspection.inspectionDetails.FourthHeader
            model4.detailState = .notStarted
            if !(serviceDetail?.inspectionEndTime == "") {
                // Inspection completed.
                model2.detailState = .completed
                model3.detailState = .completed
                model4.detailState = .completed
                //TODO: Format if needed.
                let completionDate: Date? = DateFormatter.scheduleFormat.date(from: (serviceDetail?.inspectionEndTime)!)
                model3.subHeadingWithNoDetails = Strings.WHC.inspection.inspectionDetails.inspectionSuccess
                model4.subHeadingWithNoDetails = "on \(DateFormatter.dayMonthFull.string(from: completionDate!))"
                // Show rating view.
                if !self.isRatingIsSubmitted(withServiceRequest: String(serviceRequestId)) {
                    initializeRatingView()
                }
                selectedSection = -1

                MembershipListUseCase().getMembershipArrayForLoggedInUserWith(completionHandler: { (_, _) in })
            }
            inspectionDetails = [model, model2, model3, model4]
        }
        tableViewObj.reloadData()
    }
    
    func isRatingIsSubmitted(withServiceRequest serviceRequestId: String) -> Bool {
        let key = "rating_\(serviceRequestId)"
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func setRatingIsSubmittedWithServiceRequest(_ serviceRequestId: String) {
        let key = "rating_\(serviceRequestId)"
        UserDefaults.standard.set(1, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    func isConfirmationDialogToShow() -> Bool {
        let key = "\(serviceRequestId)_\(serviceDetail?.serviceScheduleStarttime ?? "")"
        return UserDefaults.standard.object(forKey: key) == nil
    }
    
    func setConfirmationDialogToShow() {
        let key = "\(serviceRequestId)_\(serviceDetail?.serviceScheduleStarttime ?? "")"
        UserDefaults.standard.set(1, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func initializeCustomerSupportView() {
        addBottomView(chatView)
    }
    
    func addBottomView(_ view: UIView) {
        view.removeFromSuperview()
        self.view.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        let leading = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: view.superview, attribute: .leading, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: view.superview, attribute: .top, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: view.superview, attribute: .trailing, multiplier: 1, constant: 0)
        view.sizeToFit()
        self.view.addConstraints([leading,trailing,top])
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            let bottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: view.superview, attribute: .bottom, multiplier: 1, constant: 0)
            self.view.addConstraint(bottom)
            self.view.removeConstraint(top)
        }
    }
    
    func checkForTechnicianAndStartTimer(){
        if technicianStatusTimer != nil {
            return
        }
        let startTime: Date? = DateFormatter.scheduleFormat.date(from: (serviceDetail?.serviceScheduleStarttime)!)
        let currentTime = Date.currentDate()
        
        var components: DateComponents? = gregorianCalendar.dateComponents([.minute], from: startTime!, to: currentTime)
        // if the technician could not reach and time is less than 15 minutes
        if (components?.minute)! < Strings.WHC.inspection.inspectionDetails.TechnicianReachedCheckInterval  && (components?.minute)! >= 0 {
            components?.minute = Int(Strings.WHC.inspection.inspectionDetails.TechnicianReachedCheckInterval)
            let timerStartTime:Date? = gregorianCalendar.date(byAdding: components!, to: startTime!)
            technicianStatusTimer = Timer(fire: timerStartTime!, interval: 0, repeats: false, block: { (_ timer:Timer) -> Void in
                self.interactor?.getInspectionDetails(withRefresh: false, andShowLoader: false,self.serviceRequestId)
            })
            
            RunLoop.main.add(technicianStatusTimer!, forMode: .common)
        }//time is more than 15 minutes
        else if (components?.minute)! >= Strings.WHC.inspection.inspectionDetails.TechnicianReachedCheckInterval {
            initializeTechnicianStatusView()
        }
        
    }
    
    @objc func updateTime()
    {
        self.interactor?.getInspectionDetails(withRefresh: false, andShowLoader: false,serviceRequestId)
    }
    func initializeTechnicianStatusView() {
        if isConfirmationDialogToShow() {
            isConfirmationViewForRating = false
            confirmationViewTitleLabel.text = Strings.WHC.inspection.inspectionDetails.IsTechnicianReached
            confirmationViewNegativeButton.setTitle(Strings.WHC.inspection.inspectionDetails.NotYet, for: .normal)
            confirmationViewPositiveButton.setTitle(Strings.WHC.inspection.inspectionDetails.Yes, for: .normal)
            addBottomView(confirmationView)
        }
    }
    
    func registerCells(){
        self.tableViewObj.register(nib:Strings.WHC.inspection.inspectionDetails.InspectionAssesmentCellIdentifier, forCellReuseIdentifier: Strings.WHC.inspection.inspectionDetails.InspectionAssesmentCellIdentifier)
        self.tableViewObj.register(nib:Strings.WHC.inspection.inspectionDetails.TechnicianDetailCellIdentifier, forCellReuseIdentifier: Strings.WHC.inspection.inspectionDetails.TechnicianDetailCellIdentifier)
        self.tableViewObj.register(nib:Strings.WHC.inspection.inspectionDetails.InspectionDetailsCellIdentifier, forCellReuseIdentifier: Strings.WHC.inspection.inspectionDetails.InspectionDetailsCellIdentifier)
    }
    
    func initializeRatingView() {
        for i in 1...5 {
            let button = ratingContainerView.viewWithTag(i) as? UIButton
            button?.setImage(#imageLiteral(resourceName: "starGreen"), for: .selected)
            button?.setImage(#imageLiteral(resourceName: "starGreen"), for: [.highlighted, .selected])
        }
        //TODO:  //this is to be uncomment
        tableViewObj.isUserInteractionEnabled = false
        checkAndActivateRatingSubmitButton()
        addBottomView(ratingView)
    }
    
    func checkAndActivateRatingSubmitButton() {
        if selectedRating > 0 {
//            ratingSubmitButton.setEnabledState(true)
//            ratingSubmitButton.isUserInteractionEnabled = true
            ratingSubmitButton.isEnabled = true
        }
        else {
//            ratingSubmitButton.setEnabledState(false)
//            ratingSubmitButton.isUserInteractionEnabled = false
            ratingSubmitButton.isEnabled = false
        }
    }
    
    @IBAction func clickedRatingSubmitButton(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .ratingSubmitted)
        request = FeedbackRequest()
        request?.customerId = (UserCoreDataStore.currentUser?.cusId) ?? UserCoreDataStore.currentUser?.custIds?.first ?? ""
        request?.rating = "\(selectedRating)"
        request?.feedbackCode = ""
        request?.feedbackComment = ""
        
        if selectedRating < 4 {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
            self.interactor?.getFeedbacksForRatings(rating: (request?.rating)!)
        }
        else {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
            self.interactor?.submitFeedBack(String(serviceRequestId),request!)
        }
        
    }
    
    
    @IBAction func clickedConfirmationViewNegativeButton(_ sender: Any) {
        removeBottomView(confirmationView)
        if location != nil {
            EventTracking.shared.eventTracking(name: .SRTechnicianReached, [.value: "No", .location: location!])
        }
        
        if isConfirmationViewForRating {
            // DO NOTHING...
        }
        else {
           // TODO:  //open chat view
            // OAUtility.sharedInstance().openChat(forView: self)
            //openChat()
            
        }
        delegate?.negativeConfirmationTapped()
    }
    
    func openChat() {
        
    }
    
    @IBAction func clickedConfirmationViewPositiveButton(_ sender: Any) {
        removeBottomView(confirmationView)
        
        if location != nil {
            EventTracking.shared.eventTracking(name: .SRTechnicianReached, [.value: "Yes", .location: location!])
        }
        
        if isConfirmationViewForRating {
            // TAKE USER TO APP STORE
            if let appStoreUrl = URL(string: Constants.URL.appStoreURL) {
                Utilities.openCustomURL(url: appStoreUrl)
            }
        }
        else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
                self.setConfirmationDialogToShow()
                self.initializeData()
            })
        }
        
        delegate?.positiveConfirmationTapped()
    }
}

// MARK:- Display Logic Conformance
extension InspectionDestailsVC: InspectionDestailsDisplayLogic {
    func displayInspectionResult(isRefresh:Bool,showLoader:Bool,response:InspectionDetail?){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if isRefresh {
            self.refreshController?.endRefreshing()
        }
        if (response != nil) {
            lastRefreshTime = Date.currentDate()
            serviceDetail = response
            if isRefresh == false && showLoader == true {
                // First time, open first timeline
                selectedSection = 0
            }
            self.initializeData()
        }
        else if showLoader {
            Utilities.removeLoader(fromView: view, count: &loaderCount)
            showAlert(message: Constants.WHC.alertMessages.alertMessage, primaryButtonTitle: Constants.WHC.alertMessages.retry, Strings.Global.cancel, primaryAction: {
                self.interactor?.getInspectionDetails(withRefresh: isRefresh, andShowLoader: showLoader, self.serviceRequestId)
            }, {
                self.navigationController?.popViewController(animated: true)
            })
        }
        else if isRefresh {
            showAlert(title: Constants.WHC.alertMessages.alertTitle, message: Constants.WHC.alertMessages.alertMessage)
            
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.dismiss(animated: true, completion: nil)
        showAlert(title: Strings.Global.error, message: error)
    }
    
    func technicianDetailsRecieved(details: TechnicianDetailResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.technicianDetail = details.data
        self.technicianDetail?.technicianId = serviceDetail?.assignee
        //calling the get OTP api
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        self.interactor?.getOTP(self.serviceRequestId)
    }
    
    func displayOTPResponse(_ model:InspectionStartEndOTP){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.technicianDetail?.startOTP = model.serviceStartCode
        self.technicianDetail?.endOTP = model.serviceEndCode
        self.handleCollapse(forSection: section, expanded: expanded)
    }
    
    func technicianDetailErrorRecieved(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayFeedbackResponse(_ model:[FeedbackModel]){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        let vc = FeedbackVC()
        vc.request = request
        vc.delegate = self
        vc.serviceRequestId = String(self.serviceRequestId)
        vc.ratingSelected = String(selectedRating)
        vc.feedbacks = model
        vc.feedbackSubmitedCompletion = feedbackSubmitedCompletion
        self.presentInFullScreen(vc, animated: true)
        self.tableViewObj.isUserInteractionEnabled = true
        self.removeBottomView(self.ratingView)
    }
    
    func displaySubmitFeedBack()
    {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func initializeStoreRatingView() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        isConfirmationViewForRating = true
        confirmationViewTitleLabel.text = Strings.WHC.inspection.inspectionDetails.RateOnAppStore
        confirmationViewNegativeButton.setTitle(Strings.WHC.inspection.inspectionDetails.IWillDoLater, for: .normal)
        confirmationViewPositiveButton.setTitle(Strings.WHC.inspection.inspectionDetails.YeahSure, for: .normal)
        addBottomView(confirmationView)
    }
    
    func displaysubmitFeedbackResponse() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        setRatingIsSubmittedWithServiceRequest(serviceRequestId.description)
        self.tableViewObj.isUserInteractionEnabled = true
        self.removeBottomView(self.ratingView)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            self.initializeStoreRatingView()
        })
    }
    
}

// MARK:- Configuration Logic
extension InspectionDestailsVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = InspectionDestailsInteractor()
        let presenter = InspectionDestailsPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}


extension InspectionDestailsVC {
    
    
    func removeBottomView(_ view: UIView) {
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            let frame = CGRect(x: view.frame.origin.x, y: UIScreen.main.bounds.size.height, width: view.frame.size.width, height: view.frame.size.height)
            view.frame = frame
        }, completion: {(_ finished: Bool) -> Void in
            if finished {
                view.removeFromSuperview()
            }
        })
    }
    
    
    
    func getInspectionDetailsCell(for indexPath: IndexPath) -> InspectionDetailsCell {
        let cell = (tableViewObj.dequeueReusableCell(withIdentifier: Strings.WHC.inspection.inspectionDetails.InspectionDetailsCellIdentifier , for: indexPath)) as? InspectionDetailsCell
        cell?.delegate = self
        cell?.indexPath = indexPath
        cell?.setViewModel(serviceDetail!)
        return cell!
    }
    
    func getTechnicianDetailCell(for indexPath: IndexPath) -> TechnicianDetailCell {
        let cell = (tableViewObj.dequeueReusableCell(withIdentifier: Strings.WHC.inspection.inspectionDetails.TechnicianDetailCellIdentifier, for: indexPath)) as? TechnicianDetailCell
        cell?.setViewModel(technicianDetail!)
        return cell!
    }
    
    func checkForInspectionCompleteTimer() {
        if finishStatusTimer == nil {
            finishStatusTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(Strings.WHC.inspection.inspectionDetails.InspectionCompleteCheckInterval * 60), repeats: true, block: {(_ timer: Timer) -> Void in
                self.interactor?.getInspectionDetails(withRefresh: false, andShowLoader: false,self.serviceRequestId)
            })
        }
    }
    
    func getInspectionAssesmentCell(for indexPath: IndexPath) -> InspectionAssesmentCell {
        let cell = (tableViewObj.dequeueReusableCell(withIdentifier: Strings.WHC.inspection.inspectionDetails.InspectionAssesmentCellIdentifier , for: indexPath)) as? InspectionAssesmentCell
        let otpText = "\(Strings.WHC.inspection.inspectionDetails.shareOTPFirstPart ) \(technicianDetail?.endOTP ?? "") \(Strings.WHC.inspection.inspectionDetails.shareOTPSecondPart)"
        let attrString = NSMutableAttributedString(string: otpText)
        let boldRange = NSRange(location: 13, length: 8)
        attrString.addAttribute(NSAttributedString.Key.font, value: DLSFont.bodyText.regular, range: boldRange)
        cell?.otpLabel.attributedText = attrString
        Strings.WHC.inspection.inspectionDetails.ForegroundRefreshRate = 0
        checkForInspectionCompleteTimer()
        return cell!
    }
    
}

extension InspectionDestailsVC:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return inspectionDetails.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedSection == section ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return getInspectionDetailsCell(for: indexPath)
        case 1:
            return getTechnicianDetailCell(for: indexPath)
        default:
            break
        }
        return getInspectionAssesmentCell(for: indexPath)
    }
  
}
extension InspectionDestailsVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = Bundle.main.loadNibNamed(Strings.WHC.inspection.inspectionDetails.InspectionDetailHeaderViewIdentifier, owner: self, options: nil)?[0] as? InspectionDetailHeaderView
        if section == 0 {
            view?.topMarginView.isHidden = true
        }
        else if section == inspectionDetails.count - 1 {
            view?.bottomMarginView.isHidden = true
        }
        let model = inspectionDetails[section] as? InspectionDetailViewModel
        view?.setModel(model!, forSection: section)
        view?.toggleButtonState(selectedSection == section)
        view?.delegate = self
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 79
    }

    
}

extension InspectionDestailsVC:InspectionDetailHeaderViewDelegate{
    func collapseClicked(atSection section: Int, expanded: Bool) {
        self.expanded = expanded
        self.section = section
        if section == 1 || section == 2 {
            // Technician Section and technician detail is unavailable
            if technicianDetail == nil {
                Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                self.interactor?.getTechnicianDetail(technicianId: (serviceDetail?.assignee)!)
            }
            else if technicianDetail?.startOTP == nil {
                Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                self.interactor?.getOTP(self.serviceRequestId)
            }
            else {
                handleCollapse(forSection: section, expanded: expanded)
            }
        }
        else {
            handleCollapse(forSection: section, expanded: expanded)
        }
    }
    
    func handleCollapse(forSection section: Int, expanded: Bool) {
        let sectionsToReload = NSMutableIndexSet()
        if selectedSection > -1 {
            sectionsToReload.add(selectedSection)
            selectedSection = -1
        }
        if expanded {
            selectedSection = section
            sectionsToReload.add(selectedSection)
        }
        tableViewObj.reloadSections(sectionsToReload as IndexSet, with: .automatic)
    }

}
extension InspectionDestailsVC:InspectionDetailsCellDelegate{
    func clickedAddToCalendar(indexPath:IndexPath) {
        //EventTracking.shared.eventTracking(name: .addTocalendarTapped,[Event.Event])
         EventTracking.shared.eventTracking(name: .addTocalendarTapped  ,[.location: "SR Timeline",.type: "WHC Inspection"])
        let eventManager = EventManager()
        eventManager.delegate = self
        let startTime: Date? = DateFormatter.scheduleFormat.date(from: (serviceDetail?.serviceScheduleStarttime)!)
        eventManager.createEvent(on: startTime!, withTitle: Strings.WHC.inspection.inspectionDetails.inspectionScheduled, forHours: 2)
    }
    
    func clickedCall(indexPath:IndexPath) {
        //EventTracking.shared.eventTracking(name: .callOATapped)

        EventTracking.shared.eventTracking(name: .SRCallCC,[.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])

        Utilities.makeCall(to: Constants.WHC.InspectionDetailsCell.customerCareNumber.replacingOccurrences(of: " ", with: ""))
    }
}
extension InspectionDestailsVC:FeedbackSubmited{
}
extension InspectionDestailsVC:ShowingAlert{
    func showAlert(){
        Utilities.setCalendarEvent(srId: serviceDetail?.serviceId ?? "", scheduleStartDate: serviceDetail?.serviceScheduleStarttime ?? "")
        showAlert(title: Constants.WHC.alertMessages.alertTitle, message: Constants.WHC.alertMessages.calendarMessage)
        //tableViewObj.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        tableViewObj.reloadData()
    }
    
    func showAlertWithAction()
    {
        let reason = (serviceDetail != nil && serviceDetail!.cancelationReason != "") ? serviceDetail!.cancelationReason : Event.EventParams.allowToCreateEvent
        showAlert(message: reason, primaryButtonTitle: Constants.WHC.alertMessages.settings, Strings.Global.cancel, primaryAction: {
            Utilities.openAppSettings()
        }, {
           
        })
    }
}
