//
//  InspectionDestailsPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionDestailsDisplayLogic: class {
    func displayError(_ error:String)
    func displayInspectionResult(isRefresh:Bool,showLoader:Bool,response:InspectionDetail?)
    func technicianDetailsRecieved(details: TechnicianDetailResponseDTO)
    func technicianDetailErrorRecieved(_ error: String)
    func displayFeedbackResponse(_ model:[FeedbackModel])
    func displayOTPResponse(_ model:InspectionStartEndOTP)
    func displaysubmitFeedbackResponse()
}
/*
 func presentInspectionData(_ response: WHCInspectionResponseDTO?, error: Error?, request: WHCInspectionRequestDTO,isRefresh:Bool,showLoader:Bool)
 func responseRecieved(response: TechnicianDetailResponseDTO?, error: Error?)
 func presentFeedBackResponse(response: WHCFeedbackResponseDTO?, error: Error?)
 func submitFeedBack(_ requestID:String)
 func presentOTPResponse(response: InspectionStartEndOTPResponseDTO?, error: Error?)

 */
class InspectionDestailsPresenter: BasePresenter, InspectionDestailsPresentationLogic {
    weak var viewController: InspectionDestailsDisplayLogic?
    var isRefresh:Bool = false
    var showLoader:Bool = false
     func presentInspectionData(_ response: WHCInspectionResponseDTO?, error: Error?, request: WHCInspectionRequestDTO,isRefresh:Bool,showLoader:Bool){
        do {
            self.isRefresh = isRefresh
            self.showLoader = showLoader
            try checkError(response, error: error)
            let model =  getresponseDataModel((response?.data)!)
            viewController?.displayInspectionResult(isRefresh: isRefresh, showLoader: showLoader, response: model)
            
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    
    func responseRecieved(response: TechnicianDetailResponseDTO?, error: Error?) {
        
        if let error = error {
            viewController?.technicianDetailErrorRecieved(error.localizedDescription)
        } else if response?.status?.lowercased() == Constants.ResponseConstants.success.lowercased() {
            viewController?.technicianDetailsRecieved(details: response!)
        } else {
            viewController?.technicianDetailErrorRecieved(Strings.Global.somethingWrong)
        }
    }

    
    func getresponseDataModel(_ model:[WHCInspectionResponseModel]) -> InspectionDetail?
    {
         let inspectionModel:WHCInspectionResponseModel?
         var serviceDetailModel:InspectionDetail?
         if model.count > 0{
            inspectionModel = model.first
            serviceDetailModel = InspectionDetail(data: inspectionModel!)
        }
        return serviceDetailModel
    }
    
    func presentFeedBackResponse(response: WHCFeedbackResponseDTO?, error: Error?){
            do {
                try checkError(response, error: error)
                
               let feedbackModelArray =  filterDataForFeedbackModel((response?.data)!)
                viewController?.displayFeedbackResponse(feedbackModelArray)
                
            } catch {
                viewController?.displayError(error.localizedDescription)
            }
        }
    
    func filterDataForFeedbackModel(_ model:[WHCFeedbackModel]) -> [FeedbackModel]
    {
        
        var feedbackList:[FeedbackModel] = []
        var otherFeedbackModel : FeedbackModel?
        for element in model
        {
            let feedbackElement = FeedbackModel(element)
            
            if feedbackElement.feedbackValue.lowercased() == "other" {
                otherFeedbackModel = feedbackElement
            } else {
                feedbackList.append(feedbackElement)
            }
        }
        if otherFeedbackModel != nil {
            feedbackList.append(otherFeedbackModel!)
        }
        return feedbackList
    }
    
     func presentOTPResponse(response: InspectionStartEndOTPResponseDTO?, error: Error?){
        do {
            try checkError(response, error: error)
            
            viewController?.displayOTPResponse((response?.data)!)
            
        } catch {
            viewController?.displayError(error.localizedDescription)
        }

    }
    
    
    func presentSubmitFeedBackResponse(response: SubmitFeedbackResponseDTO?, error: Error?){
        do {
            try checkError(response, error: error)
            
            viewController?.displaysubmitFeedbackResponse()
            
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    

    
}
