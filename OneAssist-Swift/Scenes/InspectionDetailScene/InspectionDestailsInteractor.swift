//
//  InspectionDestailsInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionDestailsPresentationLogic {
    func presentInspectionData(_ response: WHCInspectionResponseDTO?, error: Error?, request: WHCInspectionRequestDTO,isRefresh:Bool,showLoader:Bool)
    func responseRecieved(response: TechnicianDetailResponseDTO?, error: Error?)
    func presentFeedBackResponse(response: WHCFeedbackResponseDTO?, error: Error?)
    func presentSubmitFeedBackResponse(response: SubmitFeedbackResponseDTO?, error: Error?)
    func presentOTPResponse(response: InspectionStartEndOTPResponseDTO?, error: Error?)
}

class InspectionDestailsInteractor: BaseInteractor, InspectionDestailsBusinessLogic {
    var presenter: InspectionDestailsPresentationLogic?
    
    // MARK: Business Logic Conformance
    
    
    func getInspectionDetails(withRefresh isRefresh: Bool, andShowLoader showLoader: Bool,_ serviceID:Int) {
        let obj:WHCInspectionRequestDTO = WHCInspectionRequestDTO()
        obj.requestId = serviceID.description
        
        let _  = WHCInspectionUseCase.service(requestDTO:obj) { [weak self] (service,response,error) in
            self?.presenter?.presentInspectionData(response, error:error, request:obj, isRefresh:isRefresh, showLoader:showLoader)
        }
    }

    func getTechnicianDetail(technicianId : String) {
        let request = TechnicianDetailRequestDTO(technicianId: technicianId)
        let _ = TechnicianDetailRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.responseRecieved(response: response, error: error)
        }
    }
    
    func getFeedbacksForRatings(rating:String)
    {
        let obj:WHCFeedbackRequestDTO = WHCFeedbackRequestDTO(rating, ratingType: .sr)
        let _  = WHCFeedbackUseCase.service(requestDTO:obj){
            [weak self] (service,response,error) in
            self?.presenter?.presentFeedBackResponse(response: response,error:error)
        }
    }
    /*
     var feedbackRating:String = ""
     var feedbackCode:String = ""
     var feedbackComments:String = ""
     */
    func submitFeedBack(_ requestID:String,_ request:FeedbackRequest)
     {
       // let obj:SubmitFeedbackRequestDTO = SubmitFeedbackRequestDTO(requestID)
        let obj:SubmitFeedbackRequestDTO = SubmitFeedbackRequestDTO()
        obj.modifiedBy = request.customerId
        obj.serviceReqId = requestID
        let serviceRequestFeedback = ServiceRequestFeedback(dictionary: [:])
        
        serviceRequestFeedback.feedbackCode = request.feedbackCode
        serviceRequestFeedback.feedbackRating = request.rating
        serviceRequestFeedback.feedbackComments = request.feedbackComment
        obj.serviceRequestFeedback = serviceRequestFeedback
        
        let _  = WHCSubmitFeedbackUseCase.service(requestDTO:obj) {
            [weak self] (service,response,error) in
            self?.presenter?.presentSubmitFeedBackResponse(response: response,error:error)
        }
    }
    
    func getOTP(_ id: Int) {
        let obj:InspectionStartEndOTPRequestDTO = InspectionStartEndOTPRequestDTO(serviceRequestId: String(id))
        let _  = InspectionStartEndOTPRequestUseCase.service(requestDTO:obj){
            [weak self] (service,response,error) in
            self?.presenter?.presentOTPResponse(response: response,error:error)
        }

    }
    
}




