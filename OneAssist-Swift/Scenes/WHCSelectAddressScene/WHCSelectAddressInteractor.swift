//
//  WHCSelectAddressInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WHCSelectAddressPresentationLogic {
    
}

class WHCSelectAddressInteractor: BaseInteractor, WHCSelectAddressBusinessLogic {
    var presenter: WHCSelectAddressPresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
