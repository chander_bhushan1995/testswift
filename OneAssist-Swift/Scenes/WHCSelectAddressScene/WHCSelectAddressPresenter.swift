//
//  WHCSelectAddressPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WHCSelectAddressDisplayLogic: class {
    
}

class WHCSelectAddressPresenter: BasePresenter, WHCSelectAddressPresentationLogic {
    weak var viewController: WHCSelectAddressDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
}
