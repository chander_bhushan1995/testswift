//
//  WHCSelectAddressVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WHCSelectAddressBusinessLogic {
    
}

protocol AddressRegistrationDelegate: class {
    func newAddressAdded(newAddress: AddressDetail)
    func addressEdited(editedAddress: AddressDetail)
}

class WHCSelectAddressVC: BaseVC {
    
    var interactor: WHCSelectAddressBusinessLogic?
    var modelArray:[AddressDetail] = []
    var selectedIndex :Int = 0
    @IBOutlet weak var tableviewObj: UITableView!
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var buttonPrimary: OAPrimaryButton!
    
    var activationCode = ""
    var customerDetails: TempCustomerDetail?
    var purchasedDate: Date?
    var pinCode: String?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        title = Strings.WHC.selectAddress.title
        labelHeading.text = Strings.WHC.selectAddress.heading
        labelHeading.font = DLSFont.h3.bold
        buttonPrimary.setTitle(Strings.Global.next, for: .normal)
        
        tableviewObj.register(nib: Constants.NIBNames.whcSelectAdressCell, forCellReuseIdentifier: Constants.CellIdentifiers.whcSelectAdressCell)
        tableviewObj.register(nib: Constants.NIBNames.whcSelectNewAdressCell, forCellReuseIdentifier: Constants.CellIdentifiers.whcSelectNewAdressCell)
        tableviewObj.tableFooterView = UIView()
        tableviewObj.separatorStyle = .none
        tableviewObj.estimatedRowHeight = CGFloat(Constants.CellHeight.myCardsCell)
        tableviewObj.rowHeight = UITableView.automaticDimension
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnPrimary(_ sender: Any) {
        routeToScheduleVisit()
    }
    
}

// MARK:- Address Registratio Delegate Conformance
extension WHCSelectAddressVC: AddressRegistrationDelegate {
    
    func newAddressAdded(newAddress: AddressDetail) {
        modelArray.append(newAddress)
        selectedIndex = 1
        tableviewObj.reloadData()
    }
    
    func addressEdited(editedAddress: AddressDetail) {
        tableviewObj.reloadData()
    }
}

// MARK:- Display Logic Conformance
extension WHCSelectAddressVC: WHCSelectAddressDisplayLogic {
    
}

// MARK:- Configuration Logic
extension WHCSelectAddressVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = WHCSelectAddressInteractor()
        let presenter = WHCSelectAddressPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension WHCSelectAddressVC {
    func presentAddressRegistration(isAdding: Bool, addressDetail: AddressDetail) {
        let vc = AddressRegistrationVC()
        vc.delegate = self
        vc.pincode = pinCode
        vc.addressDetail = addressDetail
        vc.isAdding = isAdding
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToScheduleVisit() {
        
        let scheduleVisitVC = ScheduleVisitVC()
        scheduleVisitVC.activationCode = activationCode
        scheduleVisitVC.addressDetail = modelArray[selectedIndex]
        scheduleVisitVC.customerDetails = customerDetails
        scheduleVisitVC.purchasedDate = purchasedDate ?? Date.currentDate()
        scheduleVisitVC.getAPiCalledFirstTime = true
        scheduleVisitVC.serviceRequestSourceType = Strings.SODConstants.nonSOD
        scheduleVisitVC.buCode = Constants.SchemeVariables.partnerBuCode
        scheduleVisitVC.bpCode = Constants.SchemeVariables.partnerCode
        if let customerInfo = self.customerDetails {
            if let partnercode = customerInfo.partnerCode {
                scheduleVisitVC.bpCode = partnercode
            }
            
            if let partnerbucode = customerInfo.partnerBuCode {
                scheduleVisitVC.buCode = partnerbucode
            }
        }
        self.navigationController?.pushViewController(scheduleVisitVC, animated: true)
    }
}

extension WHCSelectAddressVC:UITableViewDelegate{
    
}

extension WHCSelectAddressVC:UITableViewDataSource{
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArray.count == 1 ? 2 : modelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < (modelArray.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.whcSelectAdressCell, for: indexPath) as! WHCSelectAddressCell
            cell.delegate = self
            cell.buttonSelect.tag = indexPath.row
            cell.buttonEdit.tag = indexPath.row
            cell.setModel(modelArray[indexPath.row], isSelected: selectedIndex == indexPath.row, isEditable: indexPath.row != 0, name: "\(customerDetails?.firstName ?? "") \(customerDetails?.lastName ?? "")")
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.whcSelectNewAdressCell, for: indexPath) as! WHCSelectNewAddressCell
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        }
    }
}

extension WHCSelectAddressVC:WHCnewAddressRegistration{
    func newAddressButtonTapped(){
        EventTracking.shared.eventTracking(name: .addNewAddress)
        presentAddressRegistration(isAdding: true, addressDetail: AddressDetail())
    }
}

extension WHCSelectAddressVC: WHCSelectAddressCellDelegate
{
    
    func cellSelected(row: Int) {
        selectedIndex = row
        tableviewObj.reloadData()
    }
    
    func editButtonTapped(row: Int) {
        let model = modelArray[row]
        presentAddressRegistration(isAdding: false, addressDetail: model)
    }
}
