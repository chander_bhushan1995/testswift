//
//  OnlineOfferDetailVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OnlineOfferDetailBusinessLogic {
    func getOfferDetail(customerId: String?, offerId: String)
}

protocol OfferDetailBookmarkDelegate: class {
    func clickedDetailBookmark()
}

class OnlineOfferDetailVC: BaseVC {
    var index = 0;
    var interactor: OnlineOfferDetailBusinessLogic?
    var selectedOffer: BankCardOffers!
    var offerAddress: String = ""
    var category:String?
    var expiryDate:String?
    var webengageEventTrackingDict:[Event.EventKeys:String] = [:]
    weak var delegate: OfferDetailBookmarkDelegate?
    
    @IBOutlet weak var imageViewBrand: UIImageView!
    @IBOutlet weak var labelBrand: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var viewBookmark: UIView!
    @IBOutlet weak var buttonBookmark: UIButton!
    @IBOutlet weak var imageViewTime: UIImageView!
    @IBOutlet weak var labelExpiry: UILabel!
    @IBOutlet weak var labelTerms: UILabel!
    @IBOutlet weak var textViewTermsDetail: ReadMoreTextView!
    @IBOutlet weak var labelOfferLink: UILabel!
    @IBOutlet weak var buttonOfferLink: UIButton!
    @IBOutlet weak var tagView: TagStyledView!
    @IBOutlet weak var tagViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagViewHeight: NSLayoutConstraint!    
    
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        OfferSyncUseCase.shared.syncOffers()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventTracking.shared.addScreenTracking(with: .offerDetails)
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        title = Strings.OfferDetail.title
        buttonBookmark.isEnabled = false
        buttonBookmark.setImage(#imageLiteral(resourceName: "bookmark_select_gray"), for: .normal)
        buttonBookmark.setImage(#imageLiteral(resourceName: "bookmark_select_blue"), for: .selected)
        
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        
        let isBookmarked = OfferSyncUseCase.shared.isOfferBookmarked(offer: selectedOffer)
        buttonBookmark.isSelected = isBookmarked
        selectedOffer.isBookmarked = isBookmarked
        interactor?.getOfferDetail(customerId: UserCoreDataStore.currentUser?.cusId ?? nil, offerId: selectedOffer.offerId ?? "")
     
        labelBrand.text = selectedOffer.merchantName
        
        setupTagView()
        addBankCardTag(cardList: selectedOffer.cardDetailsList)
    }
    
    private func setupTagView() {
        tagView.containerSize = {[unowned self] size in
            self.tagViewHeight.constant = size.height
        }
        
        tagView.register(UINib(nibName: BankDetailTagCell.Identifier.identifier, bundle: nil), forCellWithReuseIdentifier: BankDetailTagCell.Identifier.identifier)
        
        tagView.maximumTags = 6
        tagView.bottomSheetTitle = Strings.OfferDetail.offerAvailableOnMoreCards
        tagView.options = TagStyledView.Options(sectionInset: UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0),
                                                      lineSpacing: 6.0,
                                                      interitemSpacing: 6.0,
                                                      align: TagStyledView.Options.Alignment.left)

    }
    
    func addBankCardTag(cardList: [CardDetail]?) {
        if let cardDetailList = cardList, cardDetailList.count > 0 {
            self.tagView.tags = cardDetailList
            tagView.isHidden = false
            tagViewTopConstraint.constant = 12.0
            
        }else {
           tagView.isHidden = true
           tagViewTopConstraint.constant = 0.0
           tagViewHeight.constant = 0.0
        }
    }
    
    
    
    func setOfferDetailView(offerDetail: OfferDetail) {
        imageViewBrand.setImageWithUrlString(offerDetail.image, placeholderImage: #imageLiteral(resourceName: "icon_offer_default"))
        labelDiscount.text = offerDetail.availableDiscount ?? selectedOffer.offerTitle
        
        viewBookmark.layer.cornerRadius = 5.0
        viewBookmark.layer.borderColor = UIColor.dlsBorder.cgColor
        viewBookmark.layer.borderWidth = 2.0
        
        imageViewTime.image = #imageLiteral(resourceName: "date")
        
        let date = Date(string: offerDetail.endDate, formatter: DateFormatter.offerDateFormat)
        let dateString = date?.string(with: DateFormatter.dayMonth)
        labelExpiry.text = dateString != nil ? "\(Strings.AllOffersScene.endsOn) \(dateString!)" : nil
        
        labelTerms.text = Strings.OfferDetail.termsconds
        textViewTermsDetail.setText(text: offerDetail.termsConditions ?? "")
        
        labelOfferLink.text = Strings.OfferDetail.OnlineOfferDetailScene.availOffer
        buttonOfferLink.setTitle(offerDetail.merchantLocation, for: .normal)
        offerAddress = offerDetail.merchantLocation ?? ""
        
        let item = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(OnlineOfferDetailVC.clickedButtonShare))
        self.navigationItem.rightBarButtonItem = item
    }

    // MARK:- Action Methods
    @objc @IBAction func clickedButtonBookmark(_ sender: UIButton) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.bookmarkOffer.message, subTitle:nil, forScreen: "Bookmark Offer") {[unowned self] (status) in
            if status {
                sender.isSelected = !sender.isSelected
                self.selectedOffer.isBookmarked = !self.selectedOffer.isBookmarked
                self.delegate?.clickedDetailBookmark()
                if(self.selectedOffer.isBookmarked == true){
                EventTracking.shared.eventTracking(name: .markFavourite ,
                                                   [.merchantName: self.selectedOffer.merchantName ?? "",
                                                    .type: Event.EventParams.online,
                                                    .position: NSNumber.init(value:self.index),
                                                    .category: self.category ?? "",
                                                    .expiryDate: (DateFormatter.serverDateFormat.date(from: self.expiryDate ?? "") ?? ""),
                                                    .location: FromScreen.offerDetailScreen.rawValue,
                                                    .offerID:self.selectedOffer.offerId ?? ""])
                    if Utilities.offersFavoritedCount() > 1 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                            guard let self = self else { return }
                            UserDefaultsKeys.Location.set(value: Event.EventParams.favouriteOffer)
                            Utilities.checkForRatingPopUp(viewController: self)
                        }
                    }
                }
                OfferSyncUseCase.shared.storeOfferInDb(offer: self.selectedOffer)

                let bookmarkDataDict : [String: String] = [Constants.KeyValues.offerId : self.selectedOffer.offerId ?? "", Constants.KeyValues.offerCode : self.selectedOffer.offerCode ?? "", Constants.KeyValues.outletCode : self.selectedOffer.outletCode ?? "", Constants.KeyValues.status : self.selectedOffer.isBookmarked ? "1" : "0"]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookmarkStatusChanged"), object: nil, userInfo: bookmarkDataDict)
                
            }
        }
        
    }
    
    @IBAction func clickedButtonOfferAddress(_ sender: Any) {
        openOfferAddress()
        EventTracking.shared.eventTracking(name: .offerUrl,
                                           [.merchant: selectedOffer.merchantName ?? "",
                                            .category: category ?? ""
                                            ])
    }
    
    @objc func clickedButtonShare() {
        EventTracking.shared.eventTracking(name: .shareOfferTapped)
        EventTracking.shared.eventTracking(name: .shareOnOffer)
        self.presentShareActivityView(shareObject: Utilities.getOfferDetailShareText())
    }
    
}

// MARK:- Display Logic Conformance
extension OnlineOfferDetailVC: OnlineOfferDetailDisplayLogic {
    
    func displayOfferDetailError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        EventTracking.shared.eventTracking(name: .offerDetailsApiFailed)
        showAlert(message: error)
    }
    
    func displayOfferDetail(_ offerDetail: OfferDetail) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        addBankCardTag(cardList: offerDetail.cardDetailsList)
        self.category = offerDetail.category
        self.expiryDate = offerDetail.endDate
        if self.webengageEventTrackingDict.count > 0 {
            EventTracking.shared.eventTracking(name: .offerDetails,
                                               [.merchantName: selectedOffer.merchantName ?? "",
                                                .type: Event.EventParams.online,
                                                .position: NSNumber.init(value:index),
                                                .category: category ?? "", .expiryDate:DateFormatter.serverDateFormat.date(from: selectedOffer.endDate ?? "") ?? "",
                                                .location : fromScreen.rawValue,
                                                .offerID: selectedOffer.offerId ?? "",
                                                .utmCampaign: self.webengageEventTrackingDict[.utmCampaign] ?? "",
                                                .utmMedium: self.webengageEventTrackingDict[.utmMedium] ?? "",
                                                .utmSource: self.webengageEventTrackingDict[.utmSource] ?? ""])
        } else {
            EventTracking.shared.eventTracking(name: .offerDetails,
                                               [.merchantName: selectedOffer.merchantName ?? "",
                                                .type: Event.EventParams.online,
                                                .position: NSNumber.init(value:index),
                                                .category: category ?? "", .expiryDate:DateFormatter.serverDateFormat.date(from: selectedOffer.endDate ?? "") ?? "",.location : fromScreen.rawValue,
                                                .offerID: selectedOffer.offerId ?? ""])
        }
        buttonBookmark.isEnabled = true
        setOfferDetailView(offerDetail: offerDetail)
        if Utilities.offersClickedCount() > 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                guard let self = self else { return }
                UserDefaultsKeys.Location.set(value: Event.EventParams.viewOffer)
                Utilities.checkForRatingPopUp(viewController: self)
            }
        }
    }
}

// MARK:- Configuration Logic
extension OnlineOfferDetailVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = OnlineOfferDetailInteractor()
        let presenter = OnlineOfferDetailPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension OnlineOfferDetailVC {
    
    func openOfferAddress() {
        
        let vc = WebViewVC()
        vc.isLinkWebView = true
        vc.urlString = offerAddress
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
