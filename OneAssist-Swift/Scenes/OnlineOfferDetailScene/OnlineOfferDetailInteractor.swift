//
//  OnlineOfferDetailInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OnlineOfferDetailPresentationLogic {
    func recievedOfferDetailResponse(response: OnlineOfferDetailResponseDTO?, error: Error?)
}

class OnlineOfferDetailInteractor: BaseInteractor, OnlineOfferDetailBusinessLogic {
    var presenter: OnlineOfferDetailPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getOfferDetail(customerId: String?, offerId: String) {
        let req = OnlineOfferDetailRequestDTO(customerId: customerId, offerId: offerId)
        let _ = OnlineOfferDetailRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedOfferDetailResponse(response: response, error: error)
        }
    }
}
