//
//  BankDetailTagCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 08/04/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

extension BankDetailTagCell {
    struct Identifier {
        static let identifier = String(describing: BankDetailTagCell.self)
    }
}


class BankDetailTagCell: UICollectionViewCell, TagStyling {
     @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var curvedView: DLSCurvedView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        curvedView.layer.cornerRadius = 2.0
        curvedView.layer.borderWidth = 1.0
        curvedView.layer.borderColor = UIColor.buttonBlue.cgColor
        tagLabel.font = DLSFont.tags.bold
        tagLabel.textColor = UIColor.buttonBlue
    }

    func updateStyle(_ isViewMore: Bool) {
        if isViewMore {
            curvedView.layer.cornerRadius = 0
            curvedView.layer.borderWidth = 0
            curvedView.layer.borderColor = UIColor.clear.cgColor
            tagLabel.font = DLSFont.supportingText.bold
        } else {
            curvedView.layer.cornerRadius = 2.0
            curvedView.layer.borderWidth = 1.0
            curvedView.layer.borderColor = UIColor.buttonBlue.cgColor
            tagLabel.font = DLSFont.tags.bold
        }
    }
}
