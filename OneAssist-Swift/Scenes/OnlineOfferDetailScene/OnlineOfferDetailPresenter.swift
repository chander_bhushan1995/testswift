//
//  OnlineOfferDetailPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OnlineOfferDetailDisplayLogic: class {
    func displayOfferDetail(_ offerDetail: OfferDetail)
    func displayOfferDetailError(_ error: String)
}

class OnlineOfferDetailPresenter: BasePresenter, OnlineOfferDetailPresentationLogic {
    weak var viewController: OnlineOfferDetailDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func recievedOfferDetailResponse(response: OnlineOfferDetailResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            if let detail = response?.data {
                viewController?.displayOfferDetail(detail)
            } else {
                viewController?.displayOfferDetailError(Strings.Global.somethingWrong)
            }
            
        } catch {
            viewController?.displayOfferDetailError(error.localizedDescription)
        }
    }
}
