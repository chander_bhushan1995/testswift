//
//  EnterDescriptionVC.swift
//  OneAssist-Swift
//
//  Created by Varun Dudeja on 13/08/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol EnterDescriptionVCDelegate: class {
    func descriptionAdded(_ description: String)
}


class EnterDescriptionVC: BaseVC {
    
    weak var delegate: EnterDescriptionVCDelegate?
    var prevDescription: String?
    fileprivate let minimumCharacters = Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140
    fileprivate var accesoryView: LabelInputAccesoryView!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var buttonSave: OAPrimaryButton!
    @IBOutlet weak var charactersLabel: UILabel!
    @IBOutlet weak var placeholderLabel: BodyTextRegularGreyLabel!
    
    let placeholder = "For ex; How? Where? When? Explain in detail"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if descriptionTextView.text.isEmpty {
            descriptionTextView.becomeFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- private methods
    private func initializeView() {
        title = "Enter Description"
        
        //        charactersLabel.text = "\(prevDescription?.count ?? 0)/\(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400)"
        let descCount = (prevDescription?.count ?? 0)
        let minChar = (Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140)
        if descCount < minChar {
            let characters = "character".validateSingularity(count: minChar - descCount)
            charactersLabel.text = "\(characters) remaining"
        } else {
            charactersLabel.text = "Maximum of \(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400) characters"
        }
        placeholderLabel.text = placeholder
        descriptionTextView.text = prevDescription
        placeholderLabel.isHidden = !descriptionTextView.text.isEmpty
        //        descriptionTextView.font = DLSFont.h3.regular
        //        descriptionTextView.tintColor = UIColor.dodgerBlue
        //        descriptionTextView.textColor = prevDescription == nil ? UIColor.lightGray : UIColor.black
        
        accesoryView = Bundle.main.loadNibNamed("LabelInputAccesoryView", owner: self, options: nil)?.first as! LabelInputAccesoryView
        accesoryView.btnDone.addTarget(self, action: #selector(EnterDescriptionVC.doneClicked), for: .touchUpInside)
        accesoryView.setUpdateUserInterface(isValid: descriptionTextView.text.count >= minimumCharacters, title: "Should be atleast 140 characters")
        accesoryView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 35)
        descriptionTextView.inputAccessoryView = accesoryView
        descriptionTextView.delegate = self
        descriptionTextView.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
        buttonSave.isEnabled = descriptionTextView.text.count >= minimumCharacters
        addTap()
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnSave(_ sender: Any) {
        
        delegate?.descriptionAdded(descriptionTextView.text)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func doneClicked() {
        descriptionTextView.resignFirstResponder()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EnterDescriptionVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //        if descriptionTextView.textColor == UIColor.lightGray {
        //            descriptionTextView.text = ""
        //            descriptionTextView.textColor = UIColor.black
        //        }
        //
        //        lineView.backgroundColor = UIColor.dodgerBlue
        charactersLabel.textColor = textView.text.count > 0 ?  UIColor.dodgerBlue : UIColor.bodyTextGray
        descriptionTextView.styleTextView(with: .white, borderColor: UIColor.buttonTitleBlue, shadowColor: UIColor.buttonTitleBlue)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //        if descriptionTextView.text == "" {
        //            descriptionTextView.text = placeholder
        //            descriptionTextView.textColor = UIColor.lightGray
        //        }
        //
        //        lineView.backgroundColor = UIColor.darkGray
        charactersLabel.textColor = UIColor.bodyTextGray
        descriptionTextView.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        do {
            try Validation.validateDescription(text: text)
        } catch {
            return false
        }
        
        let str = textView.text as NSString
        let strNew = str.replacingCharacters(in: range, with: text)
        
        guard strNew.count <= Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400 else {
            return false
        }
        
        let minChar = (Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140)
        if strNew.count < minChar {
            let characters = "character".validateSingularity(count: minChar - strNew.count)
            charactersLabel.text = "\(characters) remaining"
        } else {
            charactersLabel.text = "Maximum of \(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400) characters"
        }
        //        charactersLabel.text = "\(strNew.count)/\(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400)"
        charactersLabel.textColor = strNew.count > 0 ? UIColor.dodgerBlue : UIColor.bodyTextGray
        
        buttonSave.isEnabled = strNew.count >= minimumCharacters
        accesoryView.isValid = strNew.count >= minimumCharacters
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}

