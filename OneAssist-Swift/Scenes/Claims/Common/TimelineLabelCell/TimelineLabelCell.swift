//
//  TimelineLabelCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/7/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class TimelineLabelCell: UITableViewCell,ReuseIdentifier,NibLoadableView {

    @IBOutlet weak var titleLabel: BodyTextRegularGreyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setViewModel(_ model:TimeLineViewModel.TimelineLabelCellModel){
        titleLabel.attributedText = model.title
    }
    static var i = 10
}
