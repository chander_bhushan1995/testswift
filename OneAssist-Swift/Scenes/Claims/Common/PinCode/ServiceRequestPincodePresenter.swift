//
//  ServiceRequestPincodePresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 21/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import UIKit

protocol ServiceRequestPincodeDisplayLogic: class {
    func displayError(_ error:String, errorCode: String?)
    func displayPincodeResult(_ isServiciable:String,cityName:String?,stateName:String?,address:String?)
    func displayNoServiceAvailable(_ isServiciable:String?,cityName:String?,stateName:String?,address:String?)

}

class ServiceRequestPincodePresenter: BasePresenter, ServiceRequestPincodePresentationLogic {
    weak var viewController: ServiceRequestPincodeDisplayLogic?
    var totalAvailableService : AvailabilityViewModel?
    
    func checkServiceability(_ response: WHCPincodeResponseDTO?, error: Error?, request: WHCPincodeRequestDTO, category: Constants.Services){
        do {
            try checkError(response, error: error)
            
            if let data = response?.data {
                
                var array: [ServiceDescription] = []
                if category.isPEClaimType() {   // if service related to PE category
                    if let totalAvailableServices = ClaimHomeServe.shared.availability?.totalAvailableServices {
                        array = Array(totalAvailableServices)
                    }
                } else {
                    if let totalAvailableServices = totalAvailableService?.totalAvailableServices, let pincodeSuppoertedServices = data.supportedRequestTypes?.compactMap({(element : String) -> ServiceDescription? in
                        //                    if Constants.AssetServices(rawValue: element) == .buglary {
                        //                        return nil
                        //                    }
                        return ServiceDescription(service: Constants.Services(rawValue: element))
                    }) {
                        
                        // Intersection of total available services for this membership and available services for this pincode
                        
                        let supportedTypes = totalAvailableServices.intersection(pincodeSuppoertedServices)
                        array = Array(supportedTypes)
                    }
                }
                // supported services on pin code
                ClaimHomeServe.shared.supportedRequestTypes = array
            }
            
            if ClaimHomeServe.shared.supportedRequestTypes?.count == 0 {
                var customerAddress :String?
                if let address = response?.data?.customerAddress{
                    customerAddress = ((address.first?.addressLine1) ?? "") + ((address.first?.addressLine2) ?? "")
                }
                viewController?.displayNoServiceAvailable(response?.data?.pincodeServicable, cityName:response?.data?.cityName, stateName:response?.data?.stateName,address:customerAddress)
            } else {
                var customerAddress :String?
                if let address = response?.data?.customerAddress{
                    customerAddress = ((address.first?.addressLine1) ?? "") + ((address.first?.addressLine2) ?? "")
                }
                viewController?.displayPincodeResult(response?.data?.pincodeServicable ?? "", cityName:response?.data?.cityName, stateName:response?.data?.stateName,address:customerAddress)
            }
        } catch let pinCodeError as LogicalError{
            viewController?.displayError(pinCodeError.localizedDescription, errorCode: pinCodeError.errorCode)
        }
        catch {
          fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
    
    
    }
 
