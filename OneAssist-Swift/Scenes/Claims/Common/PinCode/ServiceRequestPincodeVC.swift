//
//  ServiceRequestPincodeVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
enum PinCodeError : Error {
    case invalidPincode(error:String)
    case invalidAddress(error:String)
}
enum EnumServiceAvailable{
    case eAvailable
    case eNotAvailableHomeServe
    case eNotAvailableEW
}

protocol ServiceRequestVerifyPinCodeDelegate:class{
    func didDismissController(vc:ServiceRequestPincodeVC,category: Constants.Services)
    func selfRepairButtonTapped()
}

protocol ServiceRequestPincodeBusinessLogic {
    func checkPincodeServiciablity(pincode:String, category: Constants.Services)
}


class ServiceRequestPincodeVC: BaseVC {
    
    var interactor: ServiceRequestPincodeBusinessLogic?
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var scrollView:UIScrollView!    
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var pinTF: TextFieldView!
    
    @IBOutlet weak var lblNewAddress: UILabel!
    @IBOutlet weak var btnPincode: OAPrimaryButton!
    
    @IBOutlet weak var lblErrorSubTitle: UILabel!
    @IBOutlet weak var indicatorView: UIView!
    
    @IBOutlet weak var lblIndicator: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var addressTF: TextFieldView!
    @IBOutlet weak var addressView: UIView!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var pincodeImgview: UIImageView!
    
    @IBOutlet weak var lblCompleteAddress: UILabel!
    
    @IBOutlet weak var pincodeStateView: UIView!
    @IBOutlet weak var btnAddress: OAPrimaryButton!
    
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var lblTitleError: UILabel!
    
    @IBOutlet weak var btnContactUs: UIButton!
    @IBOutlet weak var lblSelfRepairErrorSubtitle: UILabel!
    @IBOutlet weak var btnSelfRepair: UIButton!
    
    @IBOutlet weak var adressTV: UITextView!
    
//    @IBOutlet weak var placeholderLabel: BodyTextRegularGreyLabel!

    @IBOutlet weak var adressLabel: UILabel!
    
    var state : String = ""
    var city : String = ""
    var pinCode : String = ""
    var address : String? = ""
    //TODO:Remove hardcode
    var category:Constants.Services! = .fire
    var isAvailable:Bool = false
    var totalAvailableService : AvailabilityViewModel?
    
    @IBOutlet weak var lblPincode: UILabel!
    var isFromCrm:Bool = false
    
    static let pincodeCharactersLimit = 6
    //    var type:EnumServiceAvailable!
    weak var delegate:ServiceRequestVerifyPinCodeDelegate?
    
    
    @IBOutlet weak var labelError: UILabel!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDependencyConfigurator()
        initialiseView()
        EventTracking.shared.addScreenTracking(with: .SR_Pincode)
        // Do any additional setup after loading the view.
    }
    
    func initialiseView(){
        addTap()
        setText()
        addressTF.imageField = #imageLiteral(resourceName: "home_gray")
        pinTF.fieldType = OTPFieldType.self
        addressTF.delegate = self
        pinTF.delegate = self
        btnPincode.isEnabled  = true
        btnAddress.isEnabled  = true
        indicatorView.isHidden = true
        addressView.isHidden = true
        adressTV.delegate = self
        pinTF.isUserInteractionEnabled  = (isFromCrm == false || category.isPEClaimType()) ? true : false
        labelError.textColor = UIColor.errorTextFieldBorder
        labelError.isHidden = true
        setUI()
    }
    
    func setText(){
        
        if category.isPEClaimType() {
            lblTitle.text =  Strings.ServiceRequest.Pincode.peTitle
            lblSubtitle.text =  Strings.ServiceRequest.Pincode.peSubTitle
            lblCompleteAddress.text = Strings.ServiceRequest.Pincode.peCompleteAddress
        } else {
            lblTitle.text =  Strings.ServiceRequest.Pincode.title
            lblSubtitle.text =  Strings.ServiceRequest.Pincode.subTitle
            lblCompleteAddress.text = Strings.ServiceRequest.Pincode.addressComplete
        }
        
        lblIndicator.text = Strings.ServiceRequest.Pincode.indicator
        pinTF.placeholderFieldText = Strings.ServiceRequest.Pincode.pincodePlaceholder
        pinTF.descriptionText = Strings.ServiceRequest.Pincode.pincode
        btnPincode.setTitle(Strings.ServiceRequest.Pincode.continueText, for: .normal)
        btnAddress.setTitle(Strings.ServiceRequest.Pincode.continueText, for: .normal)
        addressTF.placeholderFieldText = Strings.ServiceRequest.Pincode.enterAddress
        addressTF.descriptionText = Strings.ServiceRequest.Pincode.address
        lblErrorSubTitle.text = Strings.ServiceRequest.Pincode.errorSubtitle
        lblTitleError.text = Strings.ServiceRequest.Pincode.errorTitle
        scrollView.bounces = false
    }
    
    func setUI(){
        if isFromCrm && !(category.isPEClaimType()) {
            btnEdit.isHidden = true;
        }
        pincodeStateView.backgroundColor = UIColor.veryLightGray
        pincodeStateView.layer.borderWidth = 1
        pincodeStateView.layer.borderColor = UIColor.gray2.cgColor
        pincodeStateView.layer.cornerRadius = 3
        
        if pinCode.count >= 5 {
            pinTF.textFieldObj.text = pinCode
//             btnPincode.isEnabled = true
//            self.viewSeperator.isHidden = true
 //           clickPincode(btnPincode)
        }
        lblTitleError.textColor = UIColor.errorTextFieldBorder
        adressTV.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
//        adressTV.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
//        [self.adressTV setTextContainerInset:UIEdgeInsetsMake(0, 12, 0, 12)];

    }
    func validatePincodeDetails(_ text:String?) throws {
        
        if let text = text
        {
            if(text == "")
            {
                throw PinCodeError.invalidPincode(error: Strings.ServiceRequest.Pincode.Error.pincode)
            }
        }
    }
    func validateAddressDetails(_ text:String?) throws {
        
        if let text = text
        {
            if(text == "")
            {
                throw PinCodeError.invalidAddress(error: Strings.ServiceRequest.Pincode.Error.address)
            }
        }
        
    }
    
    func showCorrespondngView(category: Constants.Services){
        //self.btnAddress.type = type
        
        if isAvailable || category.isPEClaimType() {
            self.errorView.isHidden = true
            self.btnSelfRepair.isHidden = true
            self.btnContactUs.isHidden = true
            self.lblSelfRepairErrorSubtitle.isHidden = true
            self.btnAddress.setTitle(Strings.ServiceRequest.Pincode.selfRepair, for: .normal)
            
            if adressTV.text.count > 0 {
                if Utilities.isAddressTextFieldNew {
                    Utilities.isAddressTextFieldNew = true
                var tipFrame = view.convert(adressTV.frame, from: adressTV.superview)
                tipFrame.size.width = 70
                tipFrame.size.height += 10
                
                Utilities.showTip(text: "Tap to edit this address", direction: .down, in: view, from: tipFrame, margin: 72)
              }
            }
//            self.viewSeperator.isHidden = false
            return
        }
        
        if category.isEWClaimType() {
            
            // DO NOT DELETE THIS COMMENTED CODE - THIS IS FOR SELF REPAIR
            
            //            self.btnAddress.setTitle(Strings.ServiceRequest.Pincode.selfRepair, for: .normal)
            //            self.errorView.isHidden = false
            //            self.btnAddress.isEnabled = true
            //            self.btnSelfRepair.isHidden = false
            //            self.btnContactUs.isHidden = false
            //            lblErrorSubTitle.font = UIFont.h2MediumBold
            //            lblErrorSubTitle.text = "Don't Worry"
            //            lblSelfRepairErrorSubtitle.text = "We can still cover you for cost of repairs with our self-repair option."
            //            self.lblSelfRepairErrorSubtitle.isHidden = false
            //            self.lblErrorSubTitle.isHidden = false
            //            self.btnAddress.isEnabled = true
            //            return
            
            // DELETE THIS CODE - WHEN SELF REPAIR IS ENABLED
            self.btnAddress.setTitle(Strings.ServiceRequest.Pincode.contactUs, for: .normal)
            self.errorView.isHidden = false
            self.btnSelfRepair.isHidden = true
            self.btnContactUs.isHidden = true
            lblTitleError.text = Strings.ServiceRequest.Pincode.errorTitle
//            lblErrorSubTitle.font = DLSFont.tags.bold15
            lblErrorSubTitle.text  = Strings.ServiceRequest.Pincode.errorSubtitle
            lblErrorSubTitle.isHidden = false
            self.lblSelfRepairErrorSubtitle.isHidden = true
            self.btnAddress.isEnabled = true
            return
            
        } else if category.isPEClaimType() {
            
        } else if category.isHAClaimType() {
            self.btnAddress.setTitle(Strings.ServiceRequest.Pincode.contactUs, for: .normal)
            self.errorView.isHidden = false
            self.btnSelfRepair.isHidden = true
            self.btnContactUs.isHidden = true
            lblTitleError.text = Strings.ServiceRequest.Pincode.errorTitle
//            lblErrorSubTitle.font = DLSFont.tags.bold15
            lblErrorSubTitle.text  = Strings.ServiceRequest.Pincode.errorSubtitle
            lblErrorSubTitle.isHidden = false
            self.lblSelfRepairErrorSubtitle.isHidden = true
            self.btnAddress.isEnabled = true
            return
            
        } else {
            self.btnAddress.setTitle(Strings.ServiceRequest.Pincode.contactUs, for: .normal)
            self.errorView.isHidden = false
            self.btnSelfRepair.isHidden = true
            self.btnContactUs.isHidden = true
            lblTitleError.text = Strings.ServiceRequest.Pincode.errorTitle
//            lblErrorSubTitle.font = DLSFont.tags.bold15
            lblErrorSubTitle.text  = Strings.ServiceRequest.Pincode.errorSubtitle
            lblErrorSubTitle.isHidden = false
            self.lblSelfRepairErrorSubtitle.isHidden = true
            self.btnAddress.isEnabled = true
        }
        
    }
    
    //MARK: - SCREEN ACTIONS
    
    @IBAction func clickPincode(_ sender: Any) {
        if !validateEmptyPin(){
            return
        }
        do{
            try validatePincodeDetails(pinTF.fieldText)
           
//            viewSeperator.isHidden = true
//            EventTracking.shared.eventTracking(name: .SRPincodeContinue)
            EventTracking.shared.eventTracking(name: .SRPincodeContinue, [.subcategory: CacheManager.shared.eventProductName ?? ""])
            EventTracking.shared.addScreenTracking(with: .SR_Address)
//            btnPincode.isHidden  = true
//            indicatorView.isHidden = false
            btnPincode.tag = OAButtonTagType.Primary.loaderButton.rawValue
            
            if category.isPEClaimType() {
                lblIndicator.isHidden = true
            }
            
            //self.addressTF.fieldText = ""
            self.adressTV.text = ""
            self.interactor?.checkPincodeServiciablity(pincode: pinTF.fieldText!, category: category)
            
        }
        catch let validationError as PinCodeError
        {
            switch validationError {
            case .invalidPincode(let error):
                pinTF.setError(error)
            case .invalidAddress(let error):
                addressTF.setError(error)
                
            }
        }
        catch{}
        
    }
    
    @IBAction func clickSelfRepair(_ sender: Any) {
        let list = [CardOptionsModel(title: "1. Visit the nearest service center"), CardOptionsModel(title: " 2.Get an Estimation Invoice for your appliance"), CardOptionsModel(title: "3.Upload it for verification on our App"), CardOptionsModel(title: "4.After verification,get it repaired & we will reimburse the total cost.")]
        showOAAlert(title: Strings.Global.selfRepair, list: list,buttonTitle : "Start with Self Repair")
    }
    
    @IBAction func clickContactKnow(_ sender: Any) {
        EventTracking.shared.eventTracking(name:.SRTellUsMore, [.subcategory: ClaimHomeServe.shared.productName ?? ""])
        
        contactUs()
    }
    @IBAction func clickCrossBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if(addressView.isHidden == true){
            EventTracking.shared.eventTracking(name: .SRCrossButton,
                                               [.location: "SR Pincode"])
        }
        else{
        EventTracking.shared.eventTracking(name: .SRCrossButton,
                                           [.location: "SR Address"])
        }
    }
    
    @IBAction func clickEditBtn(_ sender: Any) {
        EventTracking.shared.eventTracking(name:.SRPincodeEdit, [.subcategory: CacheManager.shared.eventProductName ?? ""])
        addressView.isHidden = true
        lblNewAddress.isHidden = true
        indicatorView.isHidden = true
        btnPincode.tag = OAButtonTagType.Primary.enabledButton.rawValue
//        btnPincode.isHidden = false
//        viewSeperator.isHidden = true
    }
    
    func dismissVC(){
        self.delegate?.didDismissController(vc: self, category: self.category)
        self.dismiss(animated: true, completion: {
        })
    }
    
    @IBAction func clickAddressBtn(_ sender: OAPrimaryButton) {
        if !validateEmptyAddress(){
            return
        }
        //let type = sender.type as! EnumServiceAvailable
        EventTracking.shared.eventTracking(name: .SRAddressContinue, [.subcategory: CacheManager.shared.eventProductName ?? ""])
        
        if category.getCategoryType() == Constants.Category.personalElectronics {
             dismissVC()
        } else if category.isEWClaimType() { // if service type is extended warranty

            if self.isAvailable == true{
                self.category = .extendedWarranty //change services category to extended warranty
            } else {
                self.contactUs()
            }
            dismissVC()
        } else if category.getCategoryType() == Constants.Category.homeAppliances {
            if isAvailable == true {
                dismissVC()
            } else {
                self.contactUs()
            }
        }
    }

    func contactUs() {
        EventTracking.shared.eventTracking(name: .SRContactUs, [.subcategory: CacheManager.shared.eventProductName ?? ""])
        Utilities.callCustomerCare()
    }
    
}


extension ServiceRequestPincodeVC:TextFieldViewDelegate{
    func textFieldViewDidBeginEditing(_ textFieldView: TextFieldView) {
    }
    
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var substring: String = textFieldView.textFieldObj.text!
        substring = substring.lowercased()
        substring = (substring as NSString).replacingCharacters(in: range, with: string)
        if textFieldView == self.pinTF{
            if substring.count <= ServiceRequestPincodeVC.pincodeCharactersLimit{
//                if(substring.count == ServiceRequestPincodeVC.pincodeCharactersLimit){
//                    btnPincode.isEnabled = true
//                }
//                else{
//                    btnPincode.isEnabled = false
//                }
                return true
            }else if substring.count == 0{
//                btnPincode.isEnabled = false
                return true
            }
            return false
        }else if textFieldView == self.addressTF{
            if substring.count == 0{
//                btnAddress.isEnabled = false
                return true
            }else{
//                btnAddress.isEnabled = true
                return true
            }
        }
        return true
    }
}


extension ServiceRequestPincodeVC:ServiceRequestPincodeDisplayLogic {
    
    //    func displayAddressResult(address: String) {
    //        self.addressTF.fieldText = address
    //        adressTV.text = address
    //    }
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = ServiceRequestPincodeInteractor()
        let presenter = ServiceRequestPincodePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
        presenter.totalAvailableService = self.totalAvailableService
    }
}
extension ServiceRequestPincodeVC{
    func displayError(_ error: String, errorCode: String?){
        
        if errorCode == "ACC_000012" {
//            viewSeperator.isHidden = false
//            EventTracking.shared.eventTracking(name: .SRPincodeContinue)
//            btnPincode.isHidden = false
//            indicatorView.isHidden = true
            btnPincode.tag = OAButtonTagType.Primary.enabledButton.rawValue
            showAlert(message: "Invalid Pincode")
        } else {
            displayPincodeResult("N",cityName:nil,stateName:nil,address:nil)
        }
    }
    
    func setCityStateName(cityName:String?,stateName:String?,address:String?){
        self.state = stateName ?? ""
        self.city = cityName ?? ""
        var pincodeText : String = self.pinTF.fieldText!.count == 0 ? pinCode : self.pinTF.fieldText!
        if city != "" && state != "" {
            pincodeText =   pincodeText + " (" + self.city + ", " + self.state + ")"
        }
        self.lblPincode.text = pincodeText
        self.addressView.isHidden = false
        if let address1 = self.address , address1.count > 0 {
            self.adressTV.text = address1
        }else {
            self.adressTV.text = address ?? ""
        }
        
        self.showCorrespondngView(category: self.category)
        if let address = address, address.trimmingCharacters(in: .whitespaces).count > 0 {
            btnAddress.isEnabled = true
        }
        if let address = self.address, address.trimmingCharacters(in: .whitespaces).count > 0 {
            btnAddress.isEnabled = true
        }
        
        if !btnAddress.isEnabled {
            adressTV.becomeFirstResponder()
        }
    }
    func displayNoServiceAvailable(_ isServiciable:String?,cityName:String?,stateName:String?,address:String?) {
        // TODO: no service available display logic
        isAvailable = false
        setCityStateName(cityName: cityName,stateName: stateName,address: address)
        
    }
    
    func displayPincodeResult(_ isServiciable:String,cityName:String?,stateName:String?,address:String?){
        if(isServiciable == "Y"){
            isAvailable = true
        }
        else{
            isAvailable = false
        }
        setCityStateName(cityName: cityName, stateName: stateName, address: address)
        
    }
}
extension ServiceRequestPincodeVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        labelError.isHidden = true
        adressTV.styleTextView(with: .white, borderColor: UIColor.buttonTitleBlue, shadowColor: UIColor.buttonTitleBlue)

        
//        adressLabel.textColor = UIColor.dodgerBlue
        }
    
    func textViewDidEndEditing(_ textView: UITextView) {
//        viewSeperator.backgroundColor = UIColor.darkGray
        adressTV.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
//        adressLabel.textColor = UIColor.charcoalGrey
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let str = textView.text as NSString
        let strNew = str.replacingCharacters(in: range, with: text)
        
        // Had fun reading this code :D
        if strNew.count == 0{
//            btnAddress.isEnabled = false
            return true
        }else{
//            btnAddress.isEnabled = true
            return true
        }
        
    }
}

//validation for empty form elements
extension ServiceRequestPincodeVC{
    fileprivate func validateEmptyPin()->Bool{
        var isValid = true
        if (pinTF.isEmpty()){
            pinTF.setError(Strings.EmptyTextFieldError.emptyPin)
            isValid = false
        }
        if(pinTF.textFieldObj.text!.count < ServiceRequestPincodeVC.pincodeCharactersLimit){
            pinTF.setError(Strings.EmptyTextFieldError.emptySixDigitPin)
            isValid = false
        }
        return isValid
    }
    fileprivate func validateEmptyAddress()->Bool{
        if (!adressTV.isHidden && (adressTV == nil || adressTV.text == "")){
            adressTV.styleTextView(with: .white, borderColor: UIColor.errorTextFieldBorder, shadowColor: UIColor.errorTextFieldBorderShadow)
            labelError.text = Strings.EmptyTextFieldError.emptyAddress
             labelError.isHidden = false
            return false
        }
        return true
    }
}
