//
//  ServiceRequestPincodeInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 21/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import UIKit

protocol ServiceRequestPincodePresentationLogic {
    func checkServiceability(_ response: WHCPincodeResponseDTO?, error: Error?, request: WHCPincodeRequestDTO, category: Constants.Services)
}

class ServiceRequestPincodeInteractor: BaseInteractor, ServiceRequestPincodeBusinessLogic {
    var presenter: ServiceRequestPincodePresentationLogic?
    

    func checkPincodeServiciablity(pincode:String, category: Constants.Services) {
        let obj:WHCPincodeRequestDTO = WHCPincodeRequestDTO()
        obj.pincode = pincode
        let _  = ClaimPincodeRequestUseCase.service(requestDTO:obj){
            [weak self] (service,response,error) in
            self?.presenter?.checkServiceability(response,error:error,request:obj, category: category)
        }
    }
}
