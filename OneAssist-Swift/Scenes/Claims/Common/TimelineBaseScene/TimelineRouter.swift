//
//  TimelineRouter.swift
//  OneAssist-Swift
//
//  Created by Varun Dudeja on 10/08/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class TimelineRouter {
    static func getAssociatedTimeline(serviceType: String) -> TimelineBaseVC? {
        
        var timeline: TimelineBaseVC?
        guard let type = Constants.Services.init(rawValue: serviceType) else {
            return nil
        }
        
        switch type {
        case .extendedWarranty:
         //   timeline = EWTimelineVC()
           timeline = TimelineBaseVC()
        case .selfRepair:
        //    timeline = SelfRepairTimelineVC()
            timeline = TimelineBaseVC()
        case .accidentalDamage:
//            timeline = AccidentalTimelineVC()
            timeline = TimelineBaseVC()

        case .breakdown:
//            timeline = BreakdownTimelineVC()
              timeline = TimelineBaseVC()
        case .fire:
//            timeline = FireTimelineVC()
            timeline = TimelineBaseVC()
        case .buglary:
          //  timeline = BurglaryTimelineVC()
            timeline = TimelineBaseVC()
        case .peADLD, .peExtendedWarranty:
//            timeline = ADLDTimelineVC()
             timeline = TimelineBaseVC()
            
        case .peTheft:
           // timeline = TheftTimelineVC()
            timeline = TimelineBaseVC()
        default:
            break
        }
        
        return timeline
    }
}

