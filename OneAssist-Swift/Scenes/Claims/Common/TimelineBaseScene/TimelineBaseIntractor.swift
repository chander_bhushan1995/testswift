//
//  TimelineBaseIntrector.swift
//  OneAssist-Swift
//
//  Created by Raj on 15/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import JavaScriptCore
import Alamofire
protocol BaseTimelinePresentationLogic {

/// This method is used to prepare ADLD/PE_EW timeline data from response to display on Timeline screen
///
/// - Parameters:
///   - stages: All stages for ADLD/PE_EW
///   - timelineData: Data received from backend
///   - error: Error in API
    func presentJSTimelineDetails(for timelineData:JavaScriptTimeineResponceDTO?,serviceData:SearchService?, homeApplianceCategoryResponseDTO: HomeApplianceCategoryResponseDTO?,error:Error?)
    func presentPayLaterStatus(responseDTO:PayLaterResponseDTO?,error:Error?)
    func presentServiceCenterList(responseDTO:ServiceCenterListResponceDTO?,error:Error?)
    func receivedServiceCenterDistance(distanceModel:GoogleDistanceResponseDTO?)
}
class TimelineBaseInteractor: BaseInteractor {
    var presenter: BaseTimelinePresentationLogic?
}
extension TimelineBaseInteractor:TimelineBusinessLogic
{
    func getTimelineDetails(srId: String, deeplinkData: DeeplinkModel?) {
        let request = SearchServiceRequestPlanRequestDTO()
        
        if deeplinkData != nil {        // Came from direct url (Deeplink)
            request.serviceRequestTypes = deeplinkData?.srType
            request.refPrimaryTrackingNo = deeplinkData?.srNumber
        } else {        // Normal flow
            request.serviceRequestId = srId
        }
        SearchServiceRequestPlanRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            if let serviceRequestResponse = response{
                let _ = JavaScriptJsonHandler.getTimeLineData(response: serviceRequestResponse,completionHandler: { (jsresponse, error) in
                    let _ = SODAppliancesUseCase.service(requestDTO: nil) { (usecase, response, error) in
                        self.presenter?.presentJSTimelineDetails(for: jsresponse,serviceData: serviceRequestResponse.data?[0], homeApplianceCategoryResponseDTO: response,error: error)
                    }
                })
            }
        }
    }
    func getPayLaterStatus(srId:String){
        let payment  = ADLDPayment(status:"CPL")
        let addReapir = ADLDRepair(payment:payment)
        let workflowData = ADLDWorkflowData(repair:addReapir)
        let request = PayLaterRequestDTO(workflowData:workflowData,serviceRequestId:NSNumber(value: Int(srId) ?? 0))
        //request.serviceRequestId = NSNumber(value: Int(srId) ?? 0)
        PayLaterRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            
            self.presenter?.presentPayLaterStatus(responseDTO:response,error:error)
            }
        }
    func getServiceCenterList(for pincode:String){
        let request = ServiceCenterListRequestDTO(pincode: pincode)
        let _ = ServiceCenterListRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
          //  print(response?.data)
            self.presenter?.presentServiceCenterList(responseDTO: response, error: error)
        }
    }
    func getServiceCenterDistance(for origin:String,and destinationString:String){
        let requestDto = GoogleDistanceRequestDTO(origins: origin, destinations: destinationString)
        let _ = GoogleDistanceRequestUseCase.service(requestDTO: requestDto) { (usecase, response, error) in
            self.presenter?.receivedServiceCenterDistance(distanceModel: response)
        }
    }
    
}
