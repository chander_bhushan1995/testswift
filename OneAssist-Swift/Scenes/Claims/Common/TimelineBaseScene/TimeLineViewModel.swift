//
//  TimeLineCellsModel.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 16/07/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation
struct  TimeLineViewModel {
    
    var sections:[TimeLineSectionsModel] = []
    var serviceData:SearchService?
    var headerData: RatingHeaderViewModel?
    //Misc
    var showRating:Bool = false
    var floatingCard:(message:String, items:[String])?
    var tempTimingDetails:[TempPartnerBUOperationHour]?
    var currentStateIsWalkin:Bool = false
    
    var tooltipVM: RoundedTooltipVM?
    var showDownloadJobSheet: Bool = false
    //MARK:- Cells view models
    struct DocumentVerificationDetailsCellModel{
        var title:String = ""
        var image:String?
        var badgeCount:Int = 0
        var description:String?
        var descriptionTitle:String?
        var buttonEnabled:Bool?
        var buttonTitle:String?
        var indexPath:IndexPath
        var action1Id:String?
    }
    
    struct DocumentVerificationTimelineCellModel{
        var image: String? = nil
        var title: String = ""
    }
    
    struct ReuploadDocumentCellModel{
        var topTitle:String? = nil
        var title: String? = nil
        var actionStr: String = ""
        var reuploadDesc: String? = nil
        var reasons: [(title: String, desc: String)]? = nil
        var buttonEnabled:Bool = true
        var indexPath:IndexPath
        var action1Id:String?
        var incompletePendenciesCount:Int?
        var otherpendenciesCount:Int?
        var otherpendenciesValuesFirst: String?
        var documentPendencyStorageRefIdFirst:String?
        var documentPendencyDocTypeIdFirst:String?
        var docIdFirst: String?
        var navigationAction: String?
    }
    
    struct RejectedClaimCellModel{
        var badgeText: String? = nil
        var titleText: String = ""
        var descriptionText: String? = nil
        var contactUsTitleText: String = ""
        var contactUsNumber: String = ""
        var enable:Bool? = nil
        var actionKey:String? = nil
        var indexPath:IndexPath
    }
    
    struct InsuranceDecisionCellModel{
        var badgeText:String? = nil
        var completeTitle: String? = ""
        var boldText: String? = ""
        var bankDescription: String? = nil
        var whyIsItBerButtonText:String? = nil
        var whyIsItBerButtonEnable:Bool? = nil
        var buttonText:String? = nil
        var buttonEnable:Bool? = nil
   
        var button2Text:String? = nil
        var button2Enable:Bool? = nil

        var claimBERModel:ClaimBERViewModel? = nil
        var stageAction1Id:String?
        var cardAction1Id:String?
        var cardAction2Id:String?
        var cardActionParam1:String?
        var cardTitleText:String?
        var cardCount:Int? = nil
        
        var indexPath:IndexPath
    }
    
    struct InspectionAssesmentCellModel{
        var otp:String = ""
        var headerText:String = ""
        var indexPath:IndexPath
    }
    
    struct InspectionDetailsCellModel{
        var serviceDetailsText:String? = nil
        var serviceScheduleStarttime: String = ""
        var serviceScheduleEndtime: String = ""
        var action1:Action? = nil
        var action2:Action? = nil
        var action3:Action? = nil
        var buttonEnabled:Bool = true
        var indexPath:IndexPath
        var addedToCalender:Bool
        var serviceId:String?
    }
    
    struct TechnicianDetailCellModel{
        var startOTP: String? = nil
        var technicianDetail:String? = nil
        var technicianIdString:String? = nil
        var imageUrl: String? = nil
        var firstName: String? = nil
        var middleName: String? = nil
        var lastName: String? = nil
        var experience: String? = nil
        var mobileNumber: String? = nil
        var action:Action? = nil
        var indexPath:IndexPath
    }
    
    struct TimelineLabelCellModel{
        var title:NSAttributedString?
        var indexPath:IndexPath
    }
    
    struct RepairAssessmentCellModel{
        var badgeText: String? = nil
        var badgeBackground: UIColor
        var badgeBorderColor: UIColor
        var badgeTextColor: UIColor
        var titleText: NSAttributedString? = nil
        var descriptionText: NSAttributedString? = nil
        var buttonTitle: String? = nil
        var isButtonEnabled: Bool = true
        var partList: [ExpandModel]? = nil
        var secondBtnName:String? = nil
        var secondBtnEnable:Bool? = nil
        var indexPath:IndexPath
        var action1Id:String?
        var action2Id:String?
    }
    
    struct WalkInCellModel{
        var pincodeText: String? = nil
        var serviceCenterText: String? = nil
        var descriptionText: String? = nil
        var subDescriptionText:String? = nil
        var seeListBtnText:String = ""
        var seeListBtnEnable:Bool = false
        var indexPath:IndexPath
        var action1Id:String?
    }
    
    struct DevicePickupCellModel{
        var heighliterText:String? = nil
        var addressText:String? = nil
        var titleText:String? = nil
        var callNumber:NSNumber? = nil
        var latLong:String? = nil
        var displayList:[DisplayList]? = nil
        var currentPosition:CLLocationCoordinate2D? = nil
        var directionBtnName:String? = nil
        var directionBtnEnable:Bool? = false
        var callBtnEnable:Bool? = false
        var callBtnName:String? = nil
        var openTodayText:String? = nil
        var indexPath:IndexPath
    }
    
    
    struct TimeLineSectionsModel {
        var name:String?
        var state:NSNumber?
        var isExpanded:Bool?
        var stageDescription:String?
        var rows:[Any]
        
        var srNumber:String?
        var productCode:String?
        var customerId:NSNumber?
        var srId:String?
        var srType:String?
        var pincode:String?
        var serviceDate:(start:String?, end:String?)? = nil

    }
    struct ServiceCenterViewModel {
        var detinationString:String?
        
    }
    enum CardTemplate:String{
        
        case docDescription = "DOC_DESCRIPTION" //DocumentVerificationDetailsCellModel
        case reasonList = "REASON_LIST"         //ReuploadDocumentCellModel
        case singleImage = "SINGLE_IMAGE"       //DocumentVerificationTimelineCellModel
        case dcategory = "D_CATEGORY"           //RejectedClaimCellModel
        case singleAction = "SINGLE_ACTION"     //InsuranceDecisionCellModel
        case cancelled = "CANCELLED"            //RejectedClaimCellModel
        case descriptionWithInfo = "DESCRIPTION_WITH_INFO" //InspectionAssesmentCellModel
        case serviceScheduled = "SERVICE_SCHEDULED"        //InspectionDetailsCellModel
        case technicianVisit = "TECHNICIAN_VISIT"          //TechnicianDetailCellModel
        case feedback = "FEEDBACK"                         //TimelineLabelCellModel
        case sectionListWithAction = "SECTION_LIST_WITH_ACTION" //RepairAssessmentCellModel
        case walkin = "WALKIN"                                  //WalkInCellModel
        case dualAction = "DUAL_ACTION" // SPARE PART           //InsuranceDecisionCellModel
        case location = "LOCATION"                              //DevicePickupCellModel
        case `default`                                          //TimelineLabelCellModel
        
    }
}
