//
//  TimelineBaseVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/6/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
import CoreLocation

protocol AlwaysVisibleCell { }
protocol TimelineBusinessLogic {
    /// This method used to get Timeline details from SRId or DeeplinkData
    ///
    /// - Parameters:
    ///   - srId: service request ID
    ///   - deeplinkData: Deeplink data
    func getTimelineDetails(srId: String, deeplinkData: DeeplinkModel?)
    func getPayLaterStatus(srId:String)
    func getServiceCenterList(for pincode:String)
    func getServiceCenterDistance(for origin:String,and destinationString:String)
}

extension TimelineBusinessLogic {
    func getPayLaterStatus(srId:String) {
        
    }
    func getServiceCenterList(for pincode:String){}
    func getServiceCenterDistance(for origin:String,and destinationString:String){}
}

protocol ShowScheduleVisit {
    
}

typealias DeeplinkModel = (srNumber: String, srType: String)

class TimelineBaseVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var chatView: UIView!
    @IBOutlet var ratingView: UIView!
    @IBOutlet weak var ratingContainerView: UIView!
    @IBOutlet weak var ratingSubmitButton: OAPrimaryButton!
    @IBOutlet var confirmationView: UIView!
    @IBOutlet weak var overAllExpericeLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var onAscaleLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var bottonConstraints: NSLayoutConstraint!
    @IBOutlet var renewalView: UIView!
    @IBOutlet weak var savedAmountLabel: H3RegularBlackLabel!
    @IBOutlet weak var expiringLabel: BodyTextRegularGreyLabel!
    
    let locationManager = Permission.shared.locationManager
    private var isWalkInStage: Bool = false
    fileprivate var currentPosition:CLLocationCoordinate2D?
    var refreshControl : UIRefreshControl!
    var ratingHeaderView: RatingHeaderView!
    
    var cacheResponseDTO : SearchServiceRequestPlanResponseDTO?
    
    var eventSubCat: String = ""
    var selectedRating: Int = 0
    var serviceData: SearchService?
    
    var sections:[TimeLineViewModel.TimeLineSectionsModel] = []
    var tempTimingDetails:[TempPartnerBUOperationHour]?
    var timeLineHeaderVM: RatingHeaderViewModel?
    var ratingTooltipVM: RoundedTooltipVM?
    var roundedTooltip: RoundedTooltip?
    var boolHistoryTimeLine : Bool = false
    fileprivate let eventManager = EventManager()
    var location: String?
    
    //FIXME:  Remove hardcoded value
    public var srId = "112843"
    private var srType: Constants.Services?
    
    var membershipId: String?
    var crmTrackingNumber: String?
    var deeplinkModel: DeeplinkModel?
    
    // Renewal Claim Amount
    var claimAmount: Float?
    var membership: CustomerMembershipDetails?
    let group = DispatchGroup()
    var subCategories: [HomeApplianceSubCategories] = []
    
    // Added to handle sod header view case
    var serviceRequestSourceType: String?
    var isSODServiceRated: Bool = false
    
    fileprivate var interactor: TimelineBusinessLogic?
    
    var rView: OARatingView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        setupDependencyConfigurator()
        
        EventTracking.shared.addScreenTracking(with: .SR_Timeline)
    }
    convenience init() {
        self.init(nibName: "TimelineBaseVC", bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        Permission.shared.resetData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isWalkInStage {
            setupLocationEnable {}
        }
        
        if let sourceType = self.serviceRequestSourceType, sourceType == Strings.SODConstants.categoryName{
            UIApplication.shared.makeStatusBarView(with: .violet)
            UIApplication.shared.statusBarStyle = .lightContent
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        }else{
            Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
        }
        interactor?.getTimelineDetails(srId: srId, deeplinkData: deeplinkModel)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.makeStatusBarView(with: .white)
        UIApplication.shared.statusBarStyle = .default
    }
    
    private func setupLocationEnable(_ completion: @escaping ()->()) {
        Permission.shared.checkLocationPermission {[weak self] (status) in
            guard let self = self else {return}
            if status == .authorizedAlways || status == .authorizedWhenInUse {
                self.locationManager.delegate = self
                self.locationManager.distanceFilter = kCLDistanceFilterNone
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.startUpdatingLocation()
                completion()
            }else if status ==  .notDetermined{
            
            }else {
               // self.showOpenSettingsAlert(title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting)
                completion()
            }
        }
    }
    
    private func initializeView() {
        navigationItem.title = Strings.ServiceRequest.Titles.serviceDetails
        tableView.estimatedRowHeight = 44
        tableView.estimatedSectionHeaderHeight = 44
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        
        tableView.sectionFooterHeight = .leastNormalMagnitude
        
        if let sourceType = self.serviceRequestSourceType, sourceType == Strings.SODConstants.categoryName{
            ratingHeaderView = RatingHeaderView.loadView()
            updateHeaderView()
            tableView.addSubview(ratingHeaderView)
        }
        
        self.addRefreshControl(message: Strings.LoaderMessage.refreshData)
        
        if (serviceRequestSourceType ?? "" != Strings.SODConstants.categoryName){
            showChatView()
        }
        tableView.register(cell: RejectedClaimCell.self)
        tableView.register(cell: WalkInCell.self)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.documentVerificationDetailsCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.documentVerificationDetailsCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.timelineLabelCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.timelineLabelCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.insuranceDecisionCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.insuranceDecisionCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.repairAssessmentCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.repairAssessmentCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.reuploadDocumentCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.reuploadDocumentCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.spExcessPaymentCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.spExcessPaymentCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.devicePickupCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.devicePickupCell)
        tableView.register(cell: InspectionDetailsCell.self)
        tableView.register(cell: TechnicianDetailCell.self)
        tableView.register(cell: InspectionAssesmentCell.self)
        tableView.register(cell: DocumentVerificationTimelineCell.self)
        
        if let claimType = CacheManager.shared.srType {
            //            if claimType is EnumClaimType.EnumHomeServe.EnumWholeHomeCover {
            //                location = "HomeAssist Service"
            //            } else if claimType is EnumClaimType.EnumHomeServe.EnumWholeHomeCover {
            //                location = "Home EW Service"
            //            } else {
            //                location = (CacheManager.shared.eventProductName ?? "") + " Service"
            //            }
            if claimType == .extendedWarranty {
                location = "Home EW Service"
            } else if claimType.getCategoryType() == Constants.Category.homeAppliances {
                location = "HomeAssist Service"
            } else {
                location = (CacheManager.shared.eventProductName ?? "") + "Service"
            }
        }
    }
    
    private func setupRatingView() {
        if let claimAmount = claimAmount, let amountSaved = Utilities.decimalNumberFormatter.string(from: NSNumber(value:Int(round(claimAmount)))) {
            savedAmountLabel.text = String(format: Strings.RenewMembershipScene.savedText, amountSaved)
        }
        
        if let endDate = membership?.endDate as NSString? {
            let dayMon = Date.getDateStr(from: endDate, with: .dayMonth).split{$0 == " "}.map(String.init)
            let formattedDate = "\(dayMon[0])\(String(describing: Date.getDate(from: endDate).getSuffixFromDay(dayOfMonth: Int(dayMon[0]) ?? -1))) \(dayMon[1])"
            
            let days = Date.getDate(from: endDate as String).days(from: Date.currentDate()) ?? 0
            if days < 0 {
                if (membership?.graceDays?.intValue ?? 0) > 0 {
                    expiringLabel.text = String(format: Strings.HATabScene.graceMessage, formattedDate, "\(membership?.graceDays?.intValue ?? 0)")
                }
            } else {
                expiringLabel.text = String(format: Strings.RenewMembershipScene.memExp, formattedDate)
            }
        }
    }
    
    func addRefreshControl(message: String)  {
        let refreshControl = UIRefreshControl()
        //        refreshControl.addSpinnerView()
        refreshControl.addTarget(self, action: #selector(didStartRefreshing), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: message)
        tableView.addSubview(refreshControl)
        if serviceRequestSourceType != Strings.SODConstants.categoryName{
            tableView.sendSubviewToBack(refreshControl)
        }
        self.refreshControl = refreshControl
    }
    
    /// This method is used to Refresh Timeline
    @objc dynamic func didStartRefreshing() {
        tableView.isUserInteractionEnabled = false
        interactor?.getTimelineDetails(srId: srId, deeplinkData: deeplinkModel)
    }
    
    /// This method is used to Stop refreshing
    func didEndRefreshing() {
        tableView.isUserInteractionEnabled = true
        refreshControl.endRefreshing()
        if loaderCount > 0 {
            Utilities.removeLoader(fromView:view, count: &loaderCount)
        }
    }
    
    func removeChatView() {
        bottonConstraints.constant = 0
        chatView.removeFromSuperview()
    }
    
    func submitFeedback(for serviceRequest: String, rating: Int, with reasons: [String]?, completionHandler: @escaping (SubmitFeedbackResponseDTO?, Error?)->()) {
        
        let feedbackRequest = ServiceRequestFeedback(dictionary: [:])
        feedbackRequest.feedbackCode = reasons?.joined(separator: ",")
        feedbackRequest.feedbackRating = rating.description
        
        let request = SubmitFeedbackRequestDTO()
        request.serviceReqId = serviceRequest
        // FIXME: remove default value
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "suren"
        request.serviceRequestFeedback = feedbackRequest
        
        let _ = WHCSubmitFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            completionHandler(response, error)
        }
    }
    
    private func parseClaim(with membership: CustomerMembershipDetails?) {
        if let membership = membership {
            self.claimAmount = membership.claims?.filter({
                Date.getDate(from: membership.startDate ?? "") <= (DateFormatter.scheduleFormat.date(from: $0.createdOn ?? "") ?? Date.currentDate())
            }).reduce(0.0, { $0 + (($1.workflowData?.repairAssessment?.costToCompany?.floatValue) ?? 0.0) }) ?? 0.0
        }
    }
    
    fileprivate func getCustomerMembershipResponse() {
        let request = CustomerMembershipDetailsRequestDTO()
        request.memId = membershipId
        request.membershipType = "A"
        request.assetDocRequired = "true"
        request.assetScopeRequired = "true"
        request.checkClaimEligibility = "true"
        request.assetServicesRequired = "true"
        request.renewalEligibilityRequired = "true"
        request.customerInfoRequired = "true"
        request.assetRenewalEligibilityRequired = "true"
        request.assetAttributesRequired = "true"
        
        let _ = CustomerMembershipDetailsRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            if error == nil {
                self.membership = response?.data?.memberships?.first
                self.parseClaim(with: self.membership)
                self.setupRatingView()
                self.group.leave()
            }
        }
    }
    
    func updateHeaderView() {
        if self.isVisible {
            UIApplication.shared.makeStatusBarView(with: .violet)
            UIApplication.shared.statusBarStyle = .lightContent
        }
        ratingHeaderView?.setViewModel(timeLineHeaderVM, delegate: self)
    }
    
    @IBAction func clickedBtnLiveChat(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .chatFromInspectionTapped)
        EventTracking.shared.eventTracking(name: .SRChat, [.location: location ?? ""])
        present(ChatVC(), animated: true, completion: nil)
        
    }
    
    @IBAction func clickedBtnRating(_ sender: Any) {
        
        let ratingButton = sender as? UIButton
        let tag: Int = (ratingButton?.tag)!
        for i in 1...tag {
            let button = ratingContainerView.viewWithTag(i) as? UIButton
            button?.isSelected = true
        }
        if(tag+1 <= 5){
            for i in tag + 1...5 {
                let button = ratingContainerView.viewWithTag(i) as? UIButton
                button?.isSelected = false
            }
        }
        selectedRating = tag
        checkAndActivateRatingSubmitButton()
    }
    
    @IBAction func clickedRatingSubmitButton(_ sender: Any) {
        
        EventTracking.shared.eventTracking(name: .ratingSubmitted, [.location: "Timeline", .starRating: selectedRating.description])
        
        if selectedRating < 4 {
            //present
            
            let feedbackVC = FeedbackTableViewVC()
            feedbackVC.rating = selectedRating
            feedbackVC.delegate = self
            feedbackVC.claimType = srType
            presentInFullScreen(feedbackVC, animated: true, completion: nil)
            
        } else {
            Utilities.addLoader(message: "Please wait...", count: &loaderCount, isInteractionEnabled: false)
            
            group.enter()
            
            if let _ = membershipId {
                group.enter()
                getCustomerMembershipResponse()
                
                group.enter()
                let _ = HomeApplianceCategoryRequestUseCase.service(requestDTO: nil) { (usecase, response, error) in
                    self.subCategories.append(contentsOf: response?.subCategories ?? [])
                    self.group.leave()
                }
            }
            
            group.leave()
            
            group.notify(queue: .main) {
                self.submitFeedback(for: self.srId, rating: self.selectedRating, with: nil) { (response, error) in
                    Utilities.removeLoader(count: &self.loaderCount)
                    if error == nil {
                        self.ratingView.removeFromSuperview()
                        
                        if let membership = self.membership {
                            if let status = membership.renewalStatus, let activity = membership.renewalActivity {
                                if status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected {
                                    if activity == Constants.RenewalActivityFlags.renewalWindow {
                                        self.showRenewalView()
                                    }
                                }
                            }
                        } else {
                            self.showAppStoreRatingView()
                        }
                    } else {
                        self.displayError(error: error!)
                    }
                }
            }
        }
    }
    
    func displayError(error: Error) {
        showAlert(title: Strings.Global.error, message: error.localizedDescription)
    }
    
    @IBAction func clickedConfirmationViewNegativeButton(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .notNow, [.location: "Timeline"])
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedConfirmationViewPositiveButton(_ sender: Any) {
        
        EventTracking.shared.eventTracking(name: .rateNow, [.location: "Timeline"])
        removeOABottomView(confirmationView, self.view)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        Utilities.openReview()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func renewNowTapped(_ sender: PrimaryButton) {
        // Need to route to pincode screen
        EventTracking.shared.eventTracking(name: .renew , [.location: Strings.Global.serviceTimeline])
        if let viewController = BuyPlanJourneyVC.routeToRenewalFlow(with: PlanDetailAndSRModel(membership!, and: subCategories)){
            presentInFullScreen(viewController, animated: true, completion: nil)
        }
    }
    
    @objc func onClickMoreAction(_ sender: UIView?) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = [Strings.Global.downloadInvoice]
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            EventTracking.shared.eventTracking(name: .threeDotMenu, [.location: Event.EventParams.membershipDetails, .value: item])
            switch item {
            case Strings.Global.downloadInvoice:
                self.downloadInvoice()
                break
            default:
                break
            }
        }
        
        dropDown.width = 254
        dropDown.direction = .bottom
        dropDown.show(beforeTransform: nil, anchorPoint: CGPoint(x: 0.6, y: 0.1))
    }
    
    func downloadInvoice() {
        let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(srId)/documents?reportType=JOBSHEET"
        if let url = URL(string: completeUrl) {
            let fileName = "INVOICE_SR_\(srId).pdf"
            FileDownloader.fileOpeninWebView(url: url, fileName: fileName, headerTitle: Strings.Global.invoice, message: Strings.Global.invoiceDownloaded)
        }
    }
}

extension TimelineBaseVC: FeedbackTableViewVCDelegate {
    func clickedSubmitRating(_ reasons: [String]) {
        
        Utilities.addLoader(message: "Please wait...", count: &loaderCount, isInteractionEnabled: false)
        submitFeedback(for: srId, rating: selectedRating, with: reasons) { (response, error) in
            Utilities.removeLoader(count: &self.loaderCount)
            
            if error == nil {
                self.ratingView.removeFromSuperview()
                NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
                self.dismiss(animated: true) {
                    self.view.makeToast(Strings.ToastMessage.feedbak)
                }
                self.navigationController?.popViewController(animated: false)
                
            } else {
                self.displayError(error: error!)
            }
        }
    }
}

extension TimelineBaseVC: UITableViewDataSource {
    final func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    final func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let isExpanded = sections[section].isExpanded, isExpanded==true{
            return sections[section].rows.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = sections[indexPath.section]
        let row = section.rows[indexPath.row]
        
        switch row {
        case let model as TimeLineViewModel.DocumentVerificationDetailsCellModel:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.documentVerificationDetailsCell, for: indexPath) as! DocumentVerificationDetailsCell
            cell.setViewModel(model, self)
            return cell
            
        case let model as TimeLineViewModel.ReuploadDocumentCellModel:
            let cell: ReuploadDocumentCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model, self)
            return cell
            
        case let model as TimeLineViewModel.InsuranceDecisionCellModel:
            let cell: InsuranceDecisionCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.insuranceDecisionCell, for: indexPath) as! InsuranceDecisionCell
            cell.setViewModel(model, self)
            return cell
            
        case let model as TimeLineViewModel.RejectedClaimCellModel:
            let cell: RejectedClaimCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model)
            return cell
            
        case let model as TimeLineViewModel.InsuranceDecisionCellModel:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.insuranceDecisionCell, for: indexPath) as! InsuranceDecisionCell
            cell.setViewModel(model, self)
            return cell
            
        case let model as TimeLineViewModel.RejectedClaimCellModel:
            let cell: RejectedClaimCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model)
            return cell
            
        case let model as TimeLineViewModel.InspectionAssesmentCellModel:
            let cell: InspectionAssesmentCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model)
            return cell
            
        case let model as TimeLineViewModel.InspectionDetailsCellModel:
            let cell: InspectionDetailsCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel1(model, self)
            return cell
            
        case let model as TimeLineViewModel.TechnicianDetailCellModel:
            let cell: TechnicianDetailCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel1(model)
            return cell
            
        case let model as TimeLineViewModel.TimelineLabelCellModel:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.timelineLabelCell, for: indexPath) as! TimelineLabelCell
            cell.setViewModel(model)
            return cell
            
        case let model as TimeLineViewModel.RepairAssessmentCellModel:
            let cell :RepairAssessmentCell  = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model, self)
            return cell
            
        case let model as TimeLineViewModel.WalkInCellModel:
            let cell: WalkInCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model, self)
            return cell
            
        case let model as TimeLineViewModel.InsuranceDecisionCellModel:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.insuranceDecisionCell, for: indexPath) as! InsuranceDecisionCell
            cell.setViewModel(model,self)
            return cell
            
        case let model as TimeLineViewModel.DevicePickupCellModel:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.devicePickupCell, for: indexPath) as! DevicePickupCell
            cell.setViewModel( model, self)
            cell.currentPosition = self.currentPosition
            return cell
            
        case let model as TimeLineViewModel.DocumentVerificationTimelineCellModel:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.documentVerificationTimelineCell, for: indexPath) as! DocumentVerificationTimelineCell
            cell.updateUserInterface(image: model.image, title: model.title)
            //                cell.setViewModel( model, self)
            return cell
        default: break
            
        }
        return UITableViewCell()
        
    }
}

extension TimelineBaseVC: UITableViewDelegate {
    final func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = Bundle.main.loadNibNamed(Strings.WHC.inspection.inspectionDetails.timeLineHeaderView, owner: self, options: nil)?[0] as? TimeLineHeaderView
        if section == 0 {
            view?.hideTopMarginView()
        } else if section == sections.count-1 {
            view?.hideBottomMarginView()
        }
        let sectionModel = sections[section]//dataSource.sections[section]
        view?.set(state: TimelineJSHeaderState(rawValue: Int(sectionModel.state ?? 0))?.toTimelineHeaderState() ?? .notStarted, title:sectionModel.name ?? "", subHeading: nil, isShowHideView: sectionModel.state == 1 || sectionModel.state == 3, forSection: section)
        view?.toggleButtonState( sectionModel.isExpanded ??
            true)
        view?.delegate = self
        return view
    }
}

extension TimelineBaseVC: TimelineBaseDisplayLogic {
    
    func showRatingView() {
        //        for i in 1...5 {
        //            let button = ratingContainerView.viewWithTag(i) as? UIButton
        //            button?.setImage(UIImage(named: Strings.WHC.inspection.inspectionDetails.RatingGreenImage ), for: .selected)
        //            button?.setImage(UIImage(named: Strings.WHC.inspection.inspectionDetails.RatingGreenImage), for: [.highlighted, .selected])
        //        }
        //        checkAndActivateRatingSubmitButton()
        //        addOABottomView(ratingView)
        
        
        rView = OARatingView()
        //data pass
        //        var selectedRating: Int = 0
        //        let group = DispatchGroup()
        //        var membershipId: String?
        //        var srId = "112843"
        //
        //        // Renewal Claim Amount
        //        var claimAmount: Float?
        //
        //        var membership: CustomerMembershipDetails?
        //
        //        var subCategories: [HomeApplianceSubCategories] = []
        //        rView.selectedRating = selectedRating
        
        if serviceRequestSourceType == Strings.SODConstants.categoryName,let model = self.ratingTooltipVM, !isSODServiceRated{
            roundedTooltip = RoundedTooltip.loadView()
            roundedTooltip?.setViewModel(model: model, delegate: self)
            if let roundedTootip = roundedTooltip{
                self.addOABottomView(roundedTootip)
            }
        }else if let rView = rView, serviceRequestSourceType != Strings.SODConstants.categoryName {
            rView.membershipId = membershipId
            rView.srId = srId
            rView.claimType = srType
            //        rView.claimAmount = claimAmount
            //        rView.membership = membership
            //        rView.subCategories = subCategories
            rView.initialSetup()
            rView.ratingViewController = self
            self.addOABottomView(rView, isForRating: true)
        }
    }
    
    func checkAndActivateRatingSubmitButton() {
        if selectedRating > 0 {
            //            ratingSubmitButton.setEnabledState(true)
            //            ratingSubmitButton.isUserInteractionEnabled = true
            ratingSubmitButton.isEnabled = true
        }
        else {
            //            ratingSubmitButton.setEnabledState(false)
            //            ratingSubmitButton.isUserInteractionEnabled = false
            ratingSubmitButton.isEnabled = false
        }
    }
    
    func showChatView() {
        
        addOABottomView(chatView)
        bottonConstraints.constant = chatView.bounds.height
    }
    
    func showMobileEmailView(_ floatingCard:(message:String, items:[String])){ //showingText:String,items:[String]
        let vc = MobileEmailAddView.instanceFromNib() as! MobileEmailAddView
        let charArray = Array(floatingCard.message)
        let attrString = NSMutableAttributedString(string: floatingCard.message + " Add")
        let boldRange = NSRange(location: charArray.count, length: 4)
        attrString.addAttribute(NSAttributedString.Key.font, value: DLSFont.supportingText.bold, range: boldRange)
        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.dodgerBlueLight, range: boldRange)
        vc.mobileEmailDescriptionLabel.attributedText = attrString
        vc.delegate = self
        vc.items = floatingCard.items
        
        self.view.layoutIfNeeded()
        let floatingFrameHeight:CGFloat = 50
        let floatingFrameHorizontalPadding:CGFloat = 20
        let floatingFrameY:CGFloat = chatView.frame.origin.y - (floatingFrameHeight+8)
        let floatingFrame = CGRect(x: floatingFrameHorizontalPadding, y: floatingFrameY, width: self.view.frame.width - 2*floatingFrameHorizontalPadding, height: floatingFrameHeight)
        vc.frame = floatingFrame
        self.view.subviews.filter({$0.tag == 501}).forEach({$0.removeFromSuperview()})
        self.view.addSubview(vc)
        self.view.bringSubviewToFront(vc)
    }
    
    func showAppStoreRatingView() {
        addOABottomView(confirmationView)
    }
    
    func showRenewalView() {
        addOABottomView(renewalView)
    }
    
    //MARK: display timeline
    func  displayTimeLines(_ viewModel:TimeLineViewModel){
        self.isWalkInStage = false
        serviceData = viewModel.serviceData
        sections = viewModel.sections
        tempTimingDetails = viewModel.tempTimingDetails
        self.ratingTooltipVM = viewModel.tooltipVM
        if let sourceType = self.serviceRequestSourceType, sourceType == Strings.SODConstants.categoryName{
            timeLineHeaderVM = viewModel.headerData
            updateHeaderView()
            EventTracking.shared.eventTracking(name: .sodSRDetail, [.srNumber: serviceData?.serviceRequestId ?? "", .appliance: serviceData?.assets?[0].productCode ?? "", .serviceType: serviceData?.serviceRequestType ?? ""])
        }
        //if current state is walk-in
        if viewModel.currentStateIsWalkin, let pincode  = sections[1].pincode{
            self.isWalkInStage = true
            self.setupLocationEnable {}
            Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
            CacheManager.shared.pincodeSelectedForServiceCenter = pincode
            interactor?.getServiceCenterList(for:pincode)
        } else{
            //PE: ADLD
            if let srType = sections.first?.srType, srType == "PE_ADLD" {
                eventSubCat = Utilities.getProduct(productCode: sections.first?.productCode ?? "")?.subCategoryName ?? ""
            }
            // tableHeaderView
            if !boolHistoryTimeLine, let serviceDate = sections.first?.serviceDate, let startDate = serviceDate.start, let endDate = serviceDate.end{
                let dateString = "Expected service closure between \(startDate) - \(endDate)"
                let dateView = Bundle.main.loadNibNamed("ClosureDateView", owner: nil, options: nil)?.first as! ClosureDateView
                dateView.updateLabel(dateString: dateString)
                tableView.tableHeaderView = dateView
            }
        }
        //miscellaneous:
        
        srType = Constants.Services(rawValue: sections.first?.srType ?? "")
        
        //1. floating card
        if let floatingCard = viewModel.floatingCard {
            showMobileEmailView(floatingCard)
        }
        //2. Rating
        if viewModel.showRating  {
            showRatingView()
        }
        
        tableView.reloadData()
        didEndRefreshing()
        
        // show More option
        if viewModel.showDownloadJobSheet {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cardMenu"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.onClickMoreAction(_:)))
            navigationItem.rightBarButtonItem?.tintColor = .dodgerBlue
        }
    }
    
    func displayTimeLinesError(_ error:Error?){
        let alert = UIAlertController(title: Strings.ServiceRequest.AlertMessage.title.technicalError, message: Strings.ServiceRequest.AlertMessage.message.cantShowSR, preferredStyle: .alert)
        let okayAction2 = UIAlertAction(title: "OK", style: .default) { (alert) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okayAction2)
        self.presentInFullScreen(alert, animated: true, completion: nil)
    }
    
    //Mark:- displsay updated pay later status
    func payLaterStatusUpdated(){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
        interactor?.getTimelineDetails(srId: srId, deeplinkData: deeplinkModel)
    }
    func payLaterStatusUpdatedError(_ error: Error?){
        let alert = UIAlertController(title: Strings.ServiceRequest.AlertMessage.title.technicalError, message: error?.localizedDescription ?? Strings.ServiceRequest.AlertMessage.message.canNotChoosePayLater, preferredStyle: .alert)
        let okayAction2 = UIAlertAction(title: "OK", style: .default) { (alert) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okayAction2)
        self.presentInFullScreen(alert, animated: true, completion: nil)
    }
    
    //MARK:- display logic: Service Center list
    func displayFinalServiceCenterList(){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        tableView.reloadData()
        didEndRefreshing()
    }
    
    func displayServiceCenterList(_ viewModel:TimeLineViewModel.ServiceCenterViewModel?){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if currentPosition != nil {
            CacheManager.shared.currentLocation = currentPosition
            Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
            interactor?.getServiceCenterDistance(for: "\(currentPosition!.latitude),\(currentPosition!.longitude)", and: viewModel?.detinationString ?? "" )
            
        }else{
            tableView.reloadData()
            didEndRefreshing()
//            let alert = UIAlertController(title: Strings.ServiceRequest.AlertMessage.title.technicalError, message: Strings.ServiceRequest.AlertMessage.message.cantShowSR, preferredStyle: .alert)
//            let okayAction2 = UIAlertAction(title: "OK", style: .default) { (alert) in
//                //FIXME: was enabled, it will popback to previous screen
//                // self.navigationController?.popViewController(animated: true)
//            }
//            alert.addAction(okayAction2)
//            self.presentInFullScreen(alert, animated: true, completion: nil)
        }
    }
    func displayServiceCenterListError(_ error: Error?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
//        let alert = UIAlertController(title: Strings.ServiceRequest.AlertMessage.title.technicalError, message: Strings.ServiceRequest.AlertMessage.message.cantShowSR, preferredStyle: .alert)
//        let okayAction2 = UIAlertAction(title: "OK", style: .default) { (alert) in
//            self.navigationController?.popViewController(animated: true)
//        }
//        alert.addAction(okayAction2)
//        self.presentInFullScreen(alert, animated: true, completion: nil)
    }
    
}

extension TimelineBaseVC: TimeLineHeaderViewDelegate {
    final func collapseClicked(at section: Int, expanded: Bool) {
        //        var sectionModel = sections[section] //value type
        sections[section].isExpanded = expanded
        tableView.reloadSections([section], with: .automatic)
    }
}

extension TimelineBaseVC:WalkInCellDelegate{
    func seeListClicked(indexPath:IndexPath){
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.WalkInCellModel else {return}
        
        if model.action1Id == Strings.ServiceRequest.ActionKey.seeAll{
            let vc = ListMapContainrVC()
            vc.pincode = model.pincodeText 
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension TimelineBaseVC: ReuploadDocumentCellDelegate {
    func clickedBtnReupload(indexPath:IndexPath) {
        let section = sections[indexPath.section]
        let row = section.rows[indexPath.row]
        guard let model = row as? TimeLineViewModel.ReuploadDocumentCellModel else {return}
        
        EventTracking.shared.eventTracking(name: .srReupload, [.service: sections.first?.srType ?? "", .subcategory: eventSubCat])
        if  model.action1Id  == Strings.ServiceRequest.ActionKey.submitDetails {
            
            if let navigationAction = model.navigationAction,  navigationAction == Strings.ServiceRequest.navigationAction.redirectView {
                let vc = WhcSalesWebViewVC()
                vc.screenTitle = model.actionStr
                vc.urlString = "\(Constants.SchemeVariables.baseDomainOld)/apigateway/auth-service/auth/loginRedirect?requestType=CLAIM_PENDENCY&subscriberNo=\(section.srId ?? srId)&linkRefNo=\(AppUserDefaults.sessionToken)&mediumCode=app"
                navigationController?.pushViewController(vc, animated: true)
                
            }else {
                if model.incompletePendenciesCount == 0 && model.otherpendenciesCount == 1 {
                    // Only request description is pending.
                    let editDescriptionVC = EditDescriptionVC()
                    editDescriptionVC.prevDescription = model.otherpendenciesValuesFirst
                    editDescriptionVC.delegate = self
                    navigationController?.pushViewController(editDescriptionVC, animated: true)
                } else {
                    // Multiple documents or date and description
                    let pendencyVC = DocumentPendencyVC()
                    pendencyVC.delegate = self
                    pendencyVC.pendency = serviceData
                    pendencyVC.srId = section.srId  ?? srId
                    navigationController?.pushViewController(pendencyVC, animated: true)
                }
            }
        }
    }
}

extension TimelineBaseVC: UploadMultipleImageVCDelegate {
    func clickedBtnSubmit() {
        EventTracking.shared.eventTracking(name: .srDetailSubmit, [.service: sections.first?.srType ?? "", .subcategory: CacheManager.shared.eventProductName ?? ""])
        
        navigationController?.popViewController(animated: true)
        Utilities.addLoader(onView: view, message: Strings.LoaderMessage.pleaseWait, count: &self.loaderCount, isInteractionEnabled: false)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        didStartRefreshing()
    }
    
    func clickedBtnBack(refreshNeeded: Bool) {
        navigationController?.popViewController(animated: true)
        if refreshNeeded {
            Utilities.addLoader(onView: view, message: Strings.LoaderMessage.pleaseWait, count: &self.loaderCount, isInteractionEnabled: false)
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            didStartRefreshing()
        }
    }
}

extension TimelineBaseVC: DocumentPendencyVCDelegate {
    func pendencySubmitted() {
        
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        Utilities.addLoader(onView: view, message: Strings.LoaderMessage.pleaseWait, count: &self.loaderCount, isInteractionEnabled: false)
        didStartRefreshing()
    }
}

extension TimelineBaseVC: ShowingAlert {
    func showAlert() {
        Utilities.setCalendarEvent(srId: sections.first?.srId ?? "", scheduleStartDate: serviceData?.scheduleSlotStartDateTime ?? "")
        
        showAlert(title: Constants.WHC.alertMessages.alertTitle, message: Constants.WHC.alertMessages.calendarMessage)
        tableView.reloadData()
    }
    
    func showAlertWithAction() {
        // FIXME: cancellation reason?
        showAlert(message: Event.EventParams.allowToCreateEvent , primaryButtonTitle: Constants.WHC.alertMessages.settings, Strings.Global.cancel, primaryAction: {
            Utilities.openAppSettings()
        }, {
            
        })
    }
}

extension TimelineBaseVC: InspectionDetailsCellDelegate {
    func clickedAddToCalendar(indexPath:IndexPath) {
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.InspectionDetailsCellModel else {return}
        if model.action1?.id == Strings.ServiceRequest.ActionKey.addToCalender{
            EventTracking.shared.eventTracking(name: .addToCalendarClicked)
            eventManager.delegate = self
            eventManager.createEvent(on: Date(string: serviceData?.scheduleSlotStartDateTime, formatter: DateFormatter.scheduleFormat) ?? Date(), withTitle: "Service Scheduled for OneAssist", forHours: 2)
            EventTracking.shared.eventTracking(name: .SR_calendar ,[.location : location ?? ""])
        }
    }
    
    func clickedCall(indexPath:IndexPath) {
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.InspectionDetailsCellModel else {return}
        if model.action3?.id == Strings.ServiceRequest.ActionKey.chat{
            EventTracking.shared.eventTracking(name: .chatButtonTapped)
            self.navigationController?.pushViewController(ChatVC(), animated: true)
        }else if model.action2?.id == Strings.ServiceRequest.ActionKey.call{
            EventTracking.shared.eventTracking(name: .callButtonForCustomerCareTapped)
            Utilities.callCustomerCare()
        }
    }
}

extension TimelineBaseVC: EditDescriptionVCDelegate {
    func descriptionAdded(_ description: String, indexPath: IndexPath?) {
        
        EventTracking.shared.eventTracking(name: .srDetailSubmit, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
        
        
        Utilities.addLoader(onView: view, message: Strings.LoaderMessage.pleaseWait, count: &self.loaderCount, isInteractionEnabled: false)
        
        let request = ClaimNotifyAllDocumentUploadedRequestDTO()
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "\(sections.first?.customerId ?? 0)"
        request.serviceID = sections.first?.srId ?? srId
        request.dateOfIncident = serviceData?.dateOfIncident
        request.requestDescription = description
        request.srType = sections.first?.srType
        
        ClaimNotifyAllDocumentUploadedRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            self.didStartRefreshing()
        }
    }
}

extension TimelineBaseVC{
    fileprivate func setupDependencyConfigurator() {
        let interactor = TimelineBaseInteractor()
        let presenter = TimelineBasePresenter()
        self.interactor = interactor
        presenter.boolHistoryTimeLine = self.boolHistoryTimeLine
        interactor.presenter = presenter
        presenter.baseViewController = self
    }
}

extension TimelineBaseVC: RepairAssessmentCellDelegate {
    func repairAssessmentCell(_ cell: RepairAssessmentCell, clickedBtnPrimary sender: Any,indexPath:IndexPath) {
        print(sections[indexPath.section].rows[indexPath.row])
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.RepairAssessmentCellModel else {return}
        
        if model.action1Id == Strings.ServiceRequest.ActionKey.reshedule{
            showScheduleVisit()
        }else if model.action1Id == Strings.ServiceRequest.ActionKey.pay {
            var adviceId: String?
            if RemoteConfigManager.shared.isScreenProtectionEnabled {
                guard let advices = serviceData?.workflowData?.repair?.advices else {
                    return
                }
                adviceId = advices.flatMap { (item) -> String? in
                    return item.adviceId?.description
                }.joined(separator: ",")
            } else {
                adviceId = cell.adviceID
            }
            // Navigate to Excess Payment Webview
            EventTracking.shared.eventTracking(name: .SRExcessPay, [.service:  sections.first?.srType ?? "", .subcategory: eventSubCat])
            let vc = ExcessPaymentWebViewVC()
            vc.adviceID = adviceId
            vc.srId = self.srId
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func clickedPayLater(indexPath:IndexPath){
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.RepairAssessmentCellModel else {return}
        
        if model.action2Id == Strings.ServiceRequest.ActionKey.payLater{
            Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
            interactor?.getPayLaterStatus(srId: self.srId)
        }
    }
    func showScheduleVisit() {
        let vc = ScheduleVisitVC()
        vc.serviceReqId = srId
        vc.getAPiCalledFirstTime = true
        if serviceRequestSourceType != Strings.SODConstants.categoryName{
            vc.serviceRequestSourceType = Strings.SODConstants.nonSOD
        }else {
            vc.serviceRequestSourceType = Strings.SODConstants.categoryName
        }
        if let membershipId = self.membershipId {
            vc.memId = membershipId
        }
        
        if let assests = serviceData?.assets, assests.count>0,
           let firstIndex = assests.first {
            if let productCode = firstIndex.productCode {
                vc.productCode = productCode
            }
            
            if let assestId = firstIndex.assetReferenceNo {
                vc.assetId = assestId
            }
            
        }
        
        vc.crmTrackingNumber = crmTrackingNumber
        vc.scheduleVisitDelegate = self
        vc.serviceType =  sections.first?.srType
        vc.addressDetail = AddressDetail(pincode: sections.first?.pincode)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension TimelineBaseVC: DocumentVerificationDetailsCellDelegate {
    func documentVerificationDetailsCell(_ cell: DocumentVerificationDetailsCell, clickedBtnViewDetail sender: Any,indexPath:IndexPath) {
        
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.DocumentVerificationDetailsCellModel else {return}
        EventTracking.shared.eventTracking(name: .SRViewDetails, [.service: serviceData?.serviceRequestType ?? "", .subcategory: eventSubCat])
        if model.action1Id  == Strings.ServiceRequest.ActionKey.viewDetail, model.badgeCount  > 0 {
            let damagedImageViewVC = UploadedDamagedImageViewVC()
            damagedImageViewVC.srDetails = serviceData
            
            presentInFullScreen(damagedImageViewVC, animated: true, completion: nil)
        }else if model.action1Id == Strings.ServiceRequest.ActionKey.viewDetail {
            let descViewVC = SampleDescriptionVC()
            descViewVC.titleText = "Description given"
            descViewVC.descriptionTVText = model.descriptionTitle
            presentInFullScreen(descViewVC, animated: true, completion: nil)
        }
    }
}

extension TimelineBaseVC:ScheduleVisitDelegate{
    func clickedScheduleVisit(date: Date, slot: ScheduleVisitTimeSlotViewModel, serviceReqId: String?, crmTrackingNumber: String?) {
        
        let scheduleVisitVC : ScheduleVisitVC = self.navigationController?.viewControllers.last as! ScheduleVisitVC
        Utilities.addLoader(onView: scheduleVisitVC.view, message: Strings.LoaderMessage.pleaseWait, count: &scheduleVisitVC.loaderCount, isInteractionEnabled: false)
        
        let requestDto = ClaimUpdateScheduleVisitRequestDTO()
        requestDto.scheduleSlotEndDateTime = date.string(with: .slotDateFormat) + " " + slot.endTime! + ":00"
        requestDto.scheduleSlotStartDateTime = date.string(with: .slotDateFormat) + " " + slot.startTime! + ":00"
        requestDto.serviceID = self.srId
        ClaimUpdateScheduleVisitRequestUseCase.service(requestDTO: requestDto) { (request, response, error) in
            Utilities.removeLoader(fromView: scheduleVisitVC.view, count: &scheduleVisitVC.loaderCount)
            do {
                try Utilities.checkError(response, error: error)
                let vc = ClaimSuccessVC()
                let model = ClaimSuccessViewModel()
                model.scheduleSlotStartDateTime = requestDto.scheduleSlotStartDateTime
                model.scheduleSlotEndDateTime = requestDto.scheduleSlotEndDateTime
                model.successType = .visitScheduled
                model.srId = self.srId
                model.crmTrackingNumber = crmTrackingNumber
                vc.successModel = model
                vc .delegate = self
                self.presentInFullScreen(vc, animated: true, completion: {
                    self.navigationController?.popViewController(animated: false)
                })
            } catch {
                self.displayError(error: error)
            }
        }
    }
}
extension TimelineBaseVC:ClaimSuccessVCDelegate{
    func clickCrossButton() {
        Utilities.addLoader(onView:view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
        
        self.didStartRefreshing()
        NotificationCenter.default.post(name: Notification.Name.Claim.loadandRefreshOngoingTab, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func clickViewServiceRequest() {
        Utilities.addLoader(onView:view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
        
        self.didStartRefreshing()
        NotificationCenter.default.post(name: Notification.Name.Claim.loadandRefreshOngoingTab, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}
extension TimelineBaseVC: InsuranceDecisionCellDelegate {
    func cardSecondBtnClicked(indexPath: IndexPath) {
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.InsuranceDecisionCellModel else {return}
        if model.cardAction2Id
            == Strings.ServiceRequest.ActionKey.optOut{
            let vc = PopupVC()
            vc.srID = self.srId
            vc.delegate = self
            vc.modalPresentationStyle = .overCurrentContext
            self.presentInFullScreen(vc, animated: false, completion: nil)
            
        }else if model.cardAction2Id == Strings.ServiceRequest.ActionKey.payLater{
            Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: true)
            interactor?.getPayLaterStatus(srId: self.srId)
            
        }
    }
    
    func insuranceDecisionCell(_ cell: InsuranceDecisionCell, clickedBtnBER sender: Any,indexPath:IndexPath) {
        guard let model = sections[indexPath.section].rows[indexPath.row] as? TimeLineViewModel.InsuranceDecisionCellModel else {return}
        
        // Move to BER details screen
        if model.stageAction1Id == Strings.ServiceRequest.ActionKey.whyBer {
            let ber = BERDetailVC()
            ber.claimBERModel = model.claimBERModel//cell.claimBERModel
            presentInFullScreen(ber, animated: true, completion: nil)
        }
    }
    
    func insuranceDecisionCell(_ cell: InsuranceDecisionCell, clickedBtnEnterDetails sender: Any,indexPath:IndexPath) {
        let section = sections[indexPath.section]
        let row = section.rows[indexPath.row]
        guard let model =  row as? TimeLineViewModel.InsuranceDecisionCellModel else {return}
        
        // Move to bank details screen
        if model.cardAction1Id == Strings.ServiceRequest.ActionKey.bankDetails {
            EventTracking.shared.eventTracking(name: .srBankDetails, [.subcategory: eventSubCat])
            let bankDetailVc = BankDetailsVC()
            bankDetailVc.updateDelegate = self
            bankDetailVc.eventSubCat = eventSubCat
            bankDetailVc.srNumber =  sections[0].srNumber
            navigationController?.pushViewController(bankDetailVc, animated: true)
        }else if model.cardAction1Id  == Strings.ServiceRequest.ActionKey.pay {
            EventTracking.shared.eventTracking(name: .SRExcessPay, [.service:  sections.first?.srType ?? "", .subcategory: eventSubCat])
            
            let vc = ExcessPaymentWebViewVC()
            vc.adviceID = model.cardActionParam1//cell.adviceID
            vc.delegate = self
            vc.srId = section.srId
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else if model.cardAction1Id == Strings.ServiceRequest.ActionKey.scheduleVisit || model.cardAction1Id == Strings.ServiceRequest.ActionKey.reshedule{
            
            let vc = ScheduleVisitVC()
            EventTracking.shared.eventTracking(name: .SRReschedule)
            vc.getAPiCalledFirstTime = true
            if serviceRequestSourceType != Strings.SODConstants.categoryName{
                vc.serviceRequestSourceType = Strings.SODConstants.nonSOD
            }else {
                vc.serviceRequestSourceType = Strings.SODConstants.categoryName
            }
            if let membershipId = self.membershipId {
                vc.memId = membershipId
            }
            
            if let assests = serviceData?.assets, assests.count>0,
               let firstIndex = assests.first {
                if let productCode = firstIndex.productCode {
                    vc.productCode = productCode
                }
                
                if let assestId = firstIndex.assetReferenceNo {
                    vc.assetId = assestId
                }
            }
            
            vc.serviceReqId = section.srId//srId
            vc.crmTrackingNumber =  section.srNumber//crmTrackingNumber
            vc.scheduleVisitDelegate = self
            vc.serviceType =  sections.first?.srType
            vc.addressDetail = AddressDetail(pincode:  sections.first?.pincode)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}

extension TimelineBaseVC: UpdateBankDetailDelegate {
    func bankDetailUpdated() {
        navigationController?.popViewController(animated: true)
        Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: false)
        didStartRefreshing()
    }
}

extension TimelineBaseVC : ExcessPaymentWebViewDelegate {
    
    func paymentSuccessful() {
        Utilities.addLoader(onView: view, message: Strings.LoaderMessage.waitForStatus, count: &loaderCount, isInteractionEnabled: false)
        interactor?.getTimelineDetails(srId: srId, deeplinkData: deeplinkModel)
    }
    
    func paymentFailure() {
        
    }
    
}

extension TimelineBaseVC: SPExcessPaymentDelegate {
    func spExcessPaymentCell(_ cell: SPExcessPaymentCell, clickedBtnPrimary sender: Any) {
        
        var adviceId: String?
        
        if RemoteConfigManager.shared.isScreenProtectionEnabled {
            guard let advices = serviceData?.workflowData?.repair?.advices else {
                return
            }
            
            adviceId = advices.flatMap { (item) -> String? in
                return item.adviceId?.description
            }.joined(separator: ",")
        } else {
            adviceId = serviceData?.workflowData?.insuranceDecision?.payment?.adviceId
        }
        
        // Navigate to Excess Payment Webview
        EventTracking.shared.eventTracking(name: .SRExcessPay, [.service:  sections.first?.srType ?? "", .subcategory: eventSubCat])
        let vc = ExcessPaymentWebViewVC()
        vc.adviceID = adviceId
        vc.srId = self.srId
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func spExcessPaymentCell(_ cell: SPExcessPaymentCell, clickedBtnSecondry sender: Any) {
        
    }
    
    func sectionTapped() {
        tableView.reloadData()
    }
    
}
extension TimelineBaseVC:MobileEmailAddViewDelegate{
    func addClicked(items:[String]){
        let vc = MobileEmailVC()
        vc.srID = sections.first?.srId
        if items.count == 1{
            if items.contains("alternateMobileNo"){
                vc.mobileTFBool = true
            }else{
                vc.emailTFBool = true
            }
        }else if items.count == 2{
            vc.mobileTFBool = true
            vc.emailTFBool = true
        }
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension TimelineBaseVC:PopupVCDelegate{
    func deviceBackSuccess() {
        tableView.reloadData()
    }
}
extension TimelineBaseVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        currentPosition = locValue
        locationManager.stopUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        
    }
}
extension TimelineBaseVC:NearByCenterCellDelegate{
    func closesAtClicked(indexPath: IndexPath) {
        let vc = ServiceCenterTimeScheduleVC()
        vc.tempTimingDetails = tempTimingDetails
        let basePickerVC = BasePickerVC(withView:vc.view, height:300)
        presentInFullScreen(basePickerVC, animated: true)
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension TimelineBaseVC: RatingHeaderViewDelegate {
    func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func chatButtonTapped() {
        self.navigationController?.pushViewController(ChatVC(), animated: true)
    }
    
    func didUpdateAlpha(_ alpha: CGFloat) {
        
    }
}

extension TimelineBaseVC: RoundedTooltipDelegate {
    func tappedOnView(){
        if let serviceRequest = self.serviceData {
            let serviceRatingVC = ServiceRatingVC()
            serviceRatingVC.serviceRequestID = serviceRequest.serviceRequestId?.stringValue ?? ""
            serviceRatingVC.serviceRequestNumber = serviceRequest.refPrimaryTrackingNo
            serviceRatingVC.productCode = serviceRequest.assets?[0].productCode
            serviceRatingVC.serviceRequestType = serviceRequest.serviceRequestType
            serviceRatingVC.serviceAddress = serviceRequest.serviceAddress
            serviceRatingVC.onRatingCompletion = { [weak self] in
                self?.isSODServiceRated = true
                self?.roundedTooltip?.removeFromSuperview()
            }
            navigationController?.pushViewController(serviceRatingVC, animated: true)
        }
    }
}
