//
//  TimelineBasePresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol TimelineBaseDisplayLogic: class {
    func  displayTimeLines(_ viewModel:TimeLineViewModel)
    func displayTimeLinesError(_ error:Error?)
    func payLaterStatusUpdated()
    func payLaterStatusUpdatedError(_ error: Error?)
    func displayServiceCenterList(_ viewModel:TimeLineViewModel.ServiceCenterViewModel?)
    func displayServiceCenterListError(_ error: Error?)
    func displayFinalServiceCenterList()
}
enum TimeLineError:Error{
    case noTimeLine
    var localizedDescription: String? {
        switch self{
        case .noTimeLine:  return "Error! No timelines available."
            
        }
    }
}
class TimelineBasePresenter: BasePresenter {
    
    var boolHistoryTimeLine : Bool = false
    var boolServiceCompleted : Bool = false
    weak var baseViewController: TimelineBaseDisplayLogic?
    func isServiceCompleted(model: SearchService?) -> Bool {
        return model?.status == Constants.TimelineStatus.completed
    }
}
extension TimelineBasePresenter: BaseTimelinePresentationLogic{
    //MARK:- refactoring
    /// Will display timeline and walkin data
    func presentJSTimelineDetails(for timelineData:JavaScriptTimeineResponceDTO?,serviceData:SearchService?,homeApplianceCategoryResponseDTO: HomeApplianceCategoryResponseDTO?,error:Error?)
    {
        if let error = error {
            baseViewController?.displayTimeLinesError(error)
            return
        }
        guard let timelines = timelineData?.timeline, timelines.count > 0 else {
            baseViewController?.displayTimeLinesError(TimeLineError.noTimeLine)
            return
        }
        let viewModel =  getViewModel(timelines, serviceData, homeApplianceCategoryResponseDTO: homeApplianceCategoryResponseDTO)
        baseViewController?.displayTimeLines(viewModel)
    }
    
    //MARK:- walkin card actions
    func presentPayLaterStatus(responseDTO:PayLaterResponseDTO?,error:Error?){
        if error == nil{
            baseViewController?.payLaterStatusUpdated()
        }else{
            baseViewController?.payLaterStatusUpdatedError(error)
        }
    }
    
    /// Will display the service center list for pin code.
    func presentServiceCenterList(responseDTO:ServiceCenterListResponceDTO?,error:Error?){
        do {
            try Utilities.checkError(responseDTO, error: error)
            if responseDTO?.data?.count ?? 0 > 0 {
                CacheManager.shared.citySelectedForServiceCenter = responseDTO?.data?[0].city ?? ""
                CacheManager.shared.stateSelectedForServiceCenter = responseDTO?.data?[0].state ?? ""
                //CacheManager.shared.pincodeSelectedForServiceCenter = responseDTO?.data?[0].pincode ?? ""
                var destinationLatLong = ""
                CacheManager.shared.serviceCenterListMarkers = []
                responseDTO?.data?.forEach({ (value) in
                    if value.latitude != nil && value.longitude != nil && value.partnerBUOperationHours?.count != 0 {
                        let position = CLLocationCoordinate2D(latitude: Double(value.latitude ?? "0") ?? 0 , longitude: Double(value.longitude ?? "0") ?? 0)
                        var openToday = ""
                        var clossesAt = ""
                        destinationLatLong = destinationLatLong + "\(position.latitude),\(position.longitude)|"
                        var dateFormatter = DateFormatter()
                        dateFormatter.calendar = gregorianCalendar
                        dateFormatter = .time24period
                        if  let hourModel = value.partnerBUOperationHours?.flatMap({ (model) -> PartnerBUOperationHour? in
                            if model.weekDay == Constants.getWeekday{
                                return model
                            }
                            return nil
                        }),hourModel.count != 0 {
                            // will check if service center is open or not currently
                            if let responseToTime = dateFormatter.date(from: hourModel[0].operationTimeTo ?? "")?.time ,let responseFromTime = dateFormatter.date(from: hourModel[0].operationTimeFrom ?? "")?.time,let time = dateFormatter.date(from: dateFormatter.string(from: Constants.getCurrentDate))?.time,time <= responseToTime && time >= responseFromTime && hourModel[0].operational == "Y" {
                                let toStringDate = "06-May-2019 " + (hourModel[0].operationTimeTo ?? "") + ":00"
                                dateFormatter = .scheduleFormat
                                let toDate = dateFormatter.date(from:toStringDate)
                                openToday = "Open today:"
                                dateFormatter = .time12period
                                clossesAt = "Closes at \(dateFormatter.string(from: toDate ?? Date()))"
                            }else{
                                let fromStringDate = "06-May-2019 " + (hourModel[0].operationTimeFrom ?? "") + ":00"
                                dateFormatter = .scheduleFormat
                                //                                let fromDate = dateFormatter.date(from:fromStringDate)
                                dateFormatter = .time12period
                                clossesAt = ""//"Open at \(dateFormatter.string(from: fromDate ?? Date()))"
                                openToday = "Closed now"
                            }
                        }
                        
                        // line1, line2, landmark - pincode
                        var address  = ""
                        //                        if let gstAddress =  value.gstAddress {
                        //                            address += "\(gstAddress), "
                        //                        }
                        //                        if let city = value.city {
                        //                            address += "\(city) - "
                        //                        }
                        //                        if let pin = value.pincode{
                        //                             address += "\(pin)"
                        //                        }
                        if let line1 = value.line1 as? String {
                            address += "\(line1)"
                        }
                        if let line2 = value.line2 as? String {
                            address += ", \(line2)"
                        }
                        if let landmark = value.landmark {
                            address += ", \(landmark)"
                        }
                        if let pin = value.pincode {
                            address += " - \(pin)"
                        }
                        //save the markers, will be used later on map and service center list
                        CacheManager.shared.serviceCenterListMarkers.append(ServiceCenterClusterMarkerModel(position: position, centerName: value.businessUnitName, centerAddress: address, activeIcon: #imageLiteral(resourceName: "Group 24 Copy 10"), mobile: "\(value.mobile ?? 0)", openToday: openToday, clossesAt: clossesAt, timingDetails: value.partnerBUOperationHours, distance: 0,kmDistance:""))
                    }
                })
                var viewModel = TimeLineViewModel.ServiceCenterViewModel()
                viewModel.detinationString = String(destinationLatLong.dropLast())
                baseViewController?.displayServiceCenterList(viewModel)
            }else{
                baseViewController?.displayServiceCenterListError(TimeLineError.noTimeLine)
            }
            
        }catch{
            baseViewController?.displayServiceCenterListError(error)
        }
    }
    
    /// will display distace for service center
    func receivedServiceCenterDistance(distanceModel:GoogleDistanceResponseDTO?) {
        if let row = distanceModel?.rows, row.count > 0,
            let distinations = distanceModel?.rows?[0].elements, distinations.count > 0 {
            for (index, distination) in distinations.enumerated() {
                //Update the distance, single source(row: 0), multiple distination
                CacheManager.shared.serviceCenterListMarkers[index].distance = distination.distance?.value?.doubleValue ?? 0
                CacheManager.shared.serviceCenterListMarkers[index].kmDistance = distination.distance?.text ?? ""
            }
        }
        // sort by distance
        CacheManager.shared.serviceCenterListMarkers.sort { (a, b) -> Bool in
            return a.distance < b.distance
        }
        baseViewController?.displayFinalServiceCenterList()
    }
}

extension TimelineBasePresenter{
    
    fileprivate func getViewModel(_ timelines: [Timeline], _ serviceData:SearchService?,homeApplianceCategoryResponseDTO: HomeApplianceCategoryResponseDTO?)->TimeLineViewModel{
        
        var viewModel = TimeLineViewModel()
        viewModel.serviceData = serviceData
        
        // used in case of sod
        let productCode = serviceData?.assets?[0].productCode
        let productName = homeApplianceCategoryResponseDTO?.subCategories?.filter({$0.subCategoryCode == productCode}).first?.subCategoryName ?? ""
        let serviceNameText = (serviceData?.serviceRequestType == Constants.Services.breakdown.rawValue ? Strings.SODConstants.repairText : Strings.SODConstants.serviceText)
        
        //rating tooltip
        if let serviceRequestSourceType = serviceData?.serviceRequestSourceType, serviceRequestSourceType == Strings.SODConstants.categoryName{ // check for SOD
            
            viewModel.tooltipVM = RoundedTooltipVM(leftImage: UIImage(named: "greenCheck"), text: productName+serviceNameText+Strings.SODConstants.reqCompleted, rightImage: UIImage(named: "arrowDisable"))
            
            //header of timeline vc
            let headerVMHeading = productName + serviceNameText
            let headerVM = RatingHeaderViewModel(imageRightText: Strings.SODConstants.requestID + (serviceData?.refPrimaryTrackingNo ?? ""), headingText: headerVMHeading, descriptionText: nil, image: nil, backgroundColor: .violet)
            viewModel.headerData = headerVM
        }
        
        //show DownloadSheet option
        viewModel.showDownloadJobSheet =  serviceData?.showDownloadJobsheet.boolValue ?? false
        
        //timeline sections
        var sections:[TimeLineViewModel.TimeLineSectionsModel] = []
        
        for (sectionNumber,timeline) in timelines.enumerated() {
            
            guard let stage = timeline.stage,  let timelineMisc = timeline.misc else{continue}
            
            let startDate:String? = Date(string: timelineMisc.thirdPartyProps?.eddFromDate, formatter: .scheduleFormat)?.string(with: .dayMonth)
            let endDate:String? = Date(string: timelineMisc.thirdPartyProps?.eddToDate, formatter: .scheduleFormat)?.string(with: .dayMonth)
            
            var section = TimeLineViewModel.TimeLineSectionsModel(name: stage.name, state: stage.state, isExpanded: stage.isExpanded, stageDescription: stage.stageDescription, rows:[], srNumber: timelineMisc.srNumber, productCode: timelineMisc.productCode, customerId: timelineMisc.thirdPartyProps?.customerId, srId: timelineMisc.srId, srType:timelineMisc.srType, pincode:timelineMisc.pincode, serviceDate: (startDate, endDate))
            
            var intialRow = 0
            if let description = stage.stageDescription, description.length>0 {
                let model =  TimeLineViewModel.TimelineLabelCellModel(title: stage.stageDescription?.getAttributedDescription(), indexPath:IndexPath(row: intialRow, section: sectionNumber))
                section.rows.append(model)
                intialRow = intialRow + 1
            }
            
            //sections rows
            if let cards = timeline.cards,  cards.count > 0{
                for (rowNumber, card) in cards.enumerated(){
                    
                    guard let template = card.template, let type = TimeLineViewModel.CardTemplate(rawValue: template) else { continue }
                    
                   // let stageDescription = stage.stageDescription ?? ""
                    let stageDescription = ""
                    let stageMisc  = stage.miscData
                    let indexPath = IndexPath(row: intialRow + rowNumber, section: sectionNumber)
                    
                    switch type {
                    case .docDescription:
                        let docDescription = card.cardDocDescription
                        let model = TimeLineViewModel.DocumentVerificationDetailsCellModel(title: stageDescription,
                                                                                           image: docDescription?.files?.first?.storageRefId , badgeCount: docDescription?.files?.count ?? 0, description: docDescription?.stageDescription, descriptionTitle: docDescription?.contentHeader ?? "", buttonEnabled:docDescription?.action1?.enable, buttonTitle:docDescription?.action1?.name ?? "", indexPath:indexPath, action1Id: docDescription?.action1?.id)
                        section.rows.append(model)
                        
                    case .reasonList:
                        
                        let reasonList = card.cardReasonList
                        let reason1: [PendencyCustomClass] = reasonList?.pendency?.doucmentPendency ?? []
                        let reason2: [PendencyCustomClass] = reasonList?.pendency?.otherPendencies ?? []
                        
                        let finalReason = reason1 + reason2 //.append(reason2)
                        
                        let reasons = finalReason.compactMap({ (value) -> (title:String,desc:String)? in
                            return (title:value.name ?? "",desc:value.reason ?? "")
                        })
                        let incompletePendenciesCount = reason1.filter{$0.status == Constants.PendencyStatus.incomplete}.count
                        let otherpendenciesCount = reason2.filter { $0.name == "Incident description" }.count
                        
                        let model = TimeLineViewModel.ReuploadDocumentCellModel(topTitle:stageDescription, title: reasonList?.contentHeader, actionStr: reasonList?.action1?.name ?? "", reuploadDesc: reasonList?.highlights, reasons: reasons,buttonEnabled:reasonList?.action1?.enable ?? true, indexPath:indexPath, action1Id: reasonList?.action1?.id, incompletePendenciesCount: incompletePendenciesCount, otherpendenciesCount: otherpendenciesCount, otherpendenciesValuesFirst: reason2.first?.value, documentPendencyStorageRefIdFirst: reason1.first?.storageRefId, documentPendencyDocTypeIdFirst: reason1.first?.docTypeId, docIdFirst: reason1.first?.docId, navigationAction: reasonList?.action1?.navigationAction)
                        section.rows.append(model)
                        
                    case .singleImage:
                        
                        let model =  TimeLineViewModel.DocumentVerificationTimelineCellModel(image: card.cardDocDescription?.files?.first?.storageRefId, title: stageDescription)
                        section.rows.append(model)
                        
                    case .dcategory:
                        
                        let dcategory = card.cardDCategory
                        let model =  TimeLineViewModel.RejectedClaimCellModel(badgeText: nil, titleText: dcategory?.address ?? "", descriptionText: dcategory?.stageDescription, contactUsTitleText: dcategory?.action1?.name ?? "", contactUsNumber: dcategory?.phoneNo ?? "",enable:dcategory?.action1?.enable,actionKey:dcategory?.action1?.id, indexPath:indexPath)
                        section.rows.append(model)
                        
                    case .singleAction, .dualAction:
                        var bermodel = ClaimBERViewModel()
                        bermodel.approvedAmount = stageMisc?.berData?[1].breakUp?.key1?.value
                        bermodel.totalRefundAmount = stageMisc?.berData?[1].breakUp?.key2?.value
                        bermodel.sumAssured = stageMisc?.berData?[1].breakUp?.key3?.value
                        bermodel.marketValue = stageMisc?.berData?[1].breakUp?.key4?.value
                        bermodel.repairEstimate = stageMisc?.berData?[1].breakUp?.key5?.value
                        
                        var berBreakupModels = [BERBreakupViewModel]()
                        stageMisc?.berData?[1].breakUp?.subItems?.forEach({ (value) in
                            berBreakupModels.append(BERBreakupViewModel(breakupAmountTitle: value.name ?? "", breakupAmountValue: value.value))
                        })
                        bermodel.berBreakUpModel = berBreakupModels
                        
                        if type == .singleAction{
                            
                            let singleAction = card.cardSingleAction
                            
                            // if cost to breakup is available then show complete cost table
                            if let costToBreakUp = card.cardSingleAction?.costBreakup {
                                
                                var expandModel = [ExpandModel]()
                                var pricingRows = [PartListModel]()
                                
                                if let subTotal = costToBreakUp.subtotal {
                                    pricingRows.append(PartListModel(title: Strings.Global.subTotal, value: (Strings.Global.rupeeSymbol)+subTotal, font: DLSFont.bodyText.regular))
                                }
                                
                                if let discount = costToBreakUp.discount {
                                    pricingRows.append(PartListModel(title: Strings.Global.discount, value: ("- "+Strings.Global.rupeeSymbol) + discount, font: DLSFont.bodyText.regular, valueTextColor: .hightlightRed))
                                }
                                
                                if let total = costToBreakUp.total {
                                    pricingRows.append(PartListModel(title: Strings.Global.total, value: (Strings.Global.rupeeSymbol) + total, font: DLSFont.bodyText.regular))
                                }
                                
                                if let parts = costToBreakUp.parts, parts.count>0 {
                                    for (index , part) in parts.enumerated() {
                                        var item: [Any]? = part.items? .map{
                                            PartListModel(title: $0.stageDescription ?? "", value: Strings.Global.rupeeSymbol+($0.value ?? ""), font: DLSFont.bodyText.regular )
                                        }
                                        
                                        if (item?.last as? PartListModel) != nil {
                                            var lastModel = item?.last as? PartListModel
                                            lastModel?.showSperator = false
                                        }
                                        
                                        if index == parts.count - 1 {
                                            item?.append(StackViewGroupedCellModel(backgroundColor: UIColor.gray3, rows: pricingRows))
                                        }
                                        if (item ?? []).count > 0 {
                                            expandModel.append(ExpandModel(sectionName: part.header, rows: item))
                                        }
                                    }
                                }
                                
//                                if pricingRows.count > 0 {
//                                    expandModel.append(ExpandModel(sectionName: nil, rows: [StackViewGroupedCellModel(backgroundColor: UIColor.gray3, rows: pricingRows)]))
//                                }
                                
                                let model =  TimeLineViewModel.RepairAssessmentCellModel(badgeText: singleAction?.highlight, badgeBackground:  UIColor.orangePeel.withAlphaComponent(0.12),badgeBorderColor: .orangePeel,badgeTextColor: .orangePeel, titleText: singleAction?.stageDescription != nil ? NSAttributedString(string: singleAction?.stageDescription ?? "") : nil  , descriptionText:nil, buttonTitle: singleAction?.action1?.name, isButtonEnabled: singleAction?.action1?.enable ?? false ,partList:expandModel,secondBtnName:nil,secondBtnEnable:nil, indexPath:indexPath, action1Id: singleAction?.action1?.id, action2Id: nil)
                                
                                section.rows.append(model)
                            } else {
                                let model =  TimeLineViewModel.InsuranceDecisionCellModel(badgeText:singleAction?.highlight, completeTitle: stageDescription, boldText: "", bankDescription: singleAction?.subDescription ,whyIsItBerButtonText: stage.action1?.name, whyIsItBerButtonEnable: stage.action1?.enable, buttonText: singleAction?.action1?.name, buttonEnable: singleAction?.action1?.enable,  button2Text: nil, button2Enable:nil, claimBERModel:bermodel, stageAction1Id: stage.action1?.id, cardAction1Id: singleAction?.action1?.id, cardAction2Id: nil, cardActionParam1: singleAction?.actionParam1, cardTitleText: singleAction?.stageDescription, cardCount: cards.count, indexPath: indexPath)
                                section.rows.append(model)
                            }
                        }
                        
                        if type == .dualAction  {
                            
                            let dualAction = card.cardDualAction
                            let model =  TimeLineViewModel.InsuranceDecisionCellModel(badgeText: nil, completeTitle: stageDescription, boldText: "", bankDescription:dualAction?.subDescription ,whyIsItBerButtonText:stage.action1?.name,whyIsItBerButtonEnable:stage.action1?.enable, buttonText: dualAction?.action1?.name, buttonEnable:  dualAction?.action1?.enable, button2Text: dualAction?.action2?.name, button2Enable: dualAction?.action2?.enable, claimBERModel:bermodel, stageAction1Id: stage.action1?.id, cardAction1Id: dualAction?.action1?.id, cardAction2Id: dualAction?.action2?.id, cardActionParam1: dualAction?.actionParam1, cardTitleText: dualAction?.stageDescription, cardCount: cards.count, indexPath: indexPath)
                            section.rows.append(model)
                        }
                        
                    case .cancelled:
                        
                        let cancelled = card.cardCancelled
                        let model =  TimeLineViewModel.RejectedClaimCellModel(badgeText: cancelled?.highlight, titleText: "", descriptionText: cancelled?.stageDescription, contactUsTitleText: cancelled?.action1?.name ?? "", contactUsNumber: cancelled?.phoneNo ?? "",enable:cancelled?.action1?.enable,actionKey:cancelled?.action1?.id, indexPath:indexPath)
                        section.rows.append(model)
                        
                    case .descriptionWithInfo:
                        
                        let model =  TimeLineViewModel.InspectionAssesmentCellModel(otp: card.cardSingleAction?.subDescription ?? card.cardSingleAction?.highlight ?? "", headerText: card.cardSingleAction?.stageDescription ?? "", indexPath:indexPath)
                        section.rows.append(model)
                        
                    case .serviceScheduled:
                        let serviceScheduled = card.cardVisitScheduled
                        let model =  TimeLineViewModel.InspectionDetailsCellModel(serviceDetailsText:serviceScheduled?.contentHeader,serviceScheduleStarttime: serviceScheduled?.startDateTime ?? "", serviceScheduleEndtime: serviceScheduled?.endDateTime ?? "", action1:serviceScheduled?.action1,action2:serviceScheduled?.action2,action3: serviceScheduled?.action3, buttonEnabled:true, indexPath:indexPath, addedToCalender: false, serviceId: timelineMisc.srId) //self.srId
                        section.rows.append(model)
                        
                    case .technicianVisit:
                        let technicianVisit  = card.cardTechVisit
                        let model =  TimeLineViewModel.TechnicianDetailCellModel(startOTP: technicianVisit?.stageDescription,technicianDetail:technicianVisit?.contentHeader, technicianIdString:nil, imageUrl: technicianVisit?.imageUrl, firstName: technicianVisit?.heading, middleName: nil, lastName: nil, experience: technicianVisit?.subHeading, mobileNumber:technicianVisit?.phoneNo,action: technicianVisit?.action1, indexPath:indexPath)
                        section.rows.append(model)
                        
                    case .feedback:
                        //Rating
                        viewModel.showRating = true
                        
                       // let model =  TimeLineViewModel.TimelineLabelCellModel(title: stage.stageDescription?.getAttributedDescription(), indexPath:indexPath)
                       // section.rows.append(model)
                        
                    case .sectionListWithAction:
                        let sectionListWithAction = card.cardSectionListAction
                        
                        var expandModel = [ExpandModel]()
                        
                        if let parts = sectionListWithAction?.parts, parts.count>0 {
                            for part in parts {
                                let item = part.items?.map{
                                    PartListModel(title: $0.stageDescription ?? "", value: Strings.Global.rupeeSymbol+($0.value ?? ""), font: DLSFont.bodyText.regular )
                                }
                            
                                expandModel.append(ExpandModel(sectionName: part.header, rows: item))
                            }
                        }
                        
                        let model =  TimeLineViewModel.RepairAssessmentCellModel(badgeText: sectionListWithAction?.highlight, badgeBackground:  UIColor.orangePeel.withAlphaComponent(0.12),badgeBorderColor: .orangePeel,badgeTextColor: .orangePeel, titleText: sectionListWithAction?.stageDescription != nil ? NSAttributedString(string: sectionListWithAction?.stageDescription ?? "") : nil  , descriptionText:nil, buttonTitle: sectionListWithAction?.action1?.name, isButtonEnabled: sectionListWithAction?.action1?.enable ?? false ,partList:expandModel,secondBtnName:sectionListWithAction?.action2?.name,secondBtnEnable:sectionListWithAction?.action2?.enable, indexPath:indexPath, action1Id: sectionListWithAction?.action1?.id, action2Id: sectionListWithAction?.action2?.id)
                        section.rows.append(model)
                        
                    case .walkin:
                        
                        let walkin = card.cardWalkin
                        if CacheManager.shared.serviceCenterListMarkers.count > 1{
                            let model = TimeLineViewModel.WalkInCellModel(pincodeText: walkin?.highlight, serviceCenterText: "\(CacheManager.shared.serviceCenterListMarkers.count) service centers found near you.", descriptionText: walkin?.stageDescription, subDescriptionText: walkin?.subDescription, seeListBtnText: "See List", seeListBtnEnable:walkin?.action1?.enable ?? false , indexPath:indexPath, action1Id: walkin?.action1?.id)
                            section.rows.append(model)
                        }else if CacheManager.shared.serviceCenterListMarkers.count == 1{
                            let model = TimeLineViewModel.WalkInCellModel(pincodeText: walkin?.highlight, serviceCenterText: "\(CacheManager.shared.serviceCenterListMarkers.count) service center found near you.", descriptionText: walkin?.stageDescription, subDescriptionText: walkin?.subDescription, seeListBtnText: "See Center", seeListBtnEnable: walkin?.action1?.enable ?? false, indexPath:indexPath, action1Id: walkin?.action1?.id)
                            section.rows.append(model)
                        }else {
                            let model = TimeLineViewModel.WalkInCellModel(pincodeText: walkin?.highlight, serviceCenterText: "\(CacheManager.shared.serviceCenterListMarkers.count) service center found near you.", descriptionText: walkin?.stageDescription, subDescriptionText: walkin?.subDescription, seeListBtnText: "See Center", seeListBtnEnable: walkin?.action1?.enable ?? false, indexPath:indexPath, action1Id: walkin?.action1?.id)
                            section.rows.append(model)
                        }
                        
                        viewModel.currentStateIsWalkin = true
                        
                        
                    case .location:
                        
                        let location = card.cardLocation
                        let model =  TimeLineViewModel.DevicePickupCellModel(heighliterText: location?.highlight, addressText:  location?.subDescription, titleText: location?.stageDescription, callNumber:location?.actionParam2,latLong:location?.actionParam1,displayList:location?.displayList, currentPosition: nil, directionBtnName:location?.action1?.name, directionBtnEnable: location?.action1?.enable, callBtnEnable: location?.action2?.enable, callBtnName: location?.action2?.name ,openTodayText:location?.bottomMessage ?? "", indexPath:indexPath)
                        section.rows.append(model)
                        
                        //temp timing details
                        let displayList = card.cardLocation?.displayList?.map{ (list) -> TempPartnerBUOperationHour in
                            return TempPartnerBUOperationHour(operationTimeFrom:list.key2 , operationTimeTo: nil, weekDay:list.key1)
                        }
                        viewModel.tempTimingDetails = displayList
                        
                    case .`default`: break
                        
                        //                        let model =  TimeLineViewModel.TimelineLabelCellModel(title: stage.stageDescription?.getAttributedDescription(), indexPath:indexPath)
                        //                        section.rows.append(model)
                    } //end of switch
                } //for loop cards
            }
            
            // floating card for adding alternate email/mobile
            if let items = timelineMisc.updateContact?.items, items.count > 0, let message = timelineMisc.updateContact?.message{
                viewModel.floatingCard  = (message, items)
                if  timeline.stage?.name ==  "Service completed", timeline.stage?.status == "CO"{
                    viewModel.floatingCard = nil
                }
            }
            
            sections.append(section)
        }//for loop sections
        
        viewModel.sections = sections
        return viewModel
    }
}
