//
//  TimeLineHeaderView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/6/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

enum TimelineHeaderState {
    case notStarted
    case started
    case onHold
    case completed
}
enum TimelineJSHeaderState: Int {
    case S1 = 0,S2,S3,S4
    func toTimelineHeaderState() -> TimelineHeaderState {
        switch self {
        case .S1: return TimelineHeaderState.notStarted
        case .S2: return TimelineHeaderState.started
        case .S3: return TimelineHeaderState.onHold
        case .S4: return TimelineHeaderState.completed
        }
    }
}

protocol TimeLineHeaderViewDelegate: class {
    func collapseClicked(at section: Int, expanded: Bool)
}

class TimeLineHeaderView: UIView {
    
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var headingLabel: H3BoldLabel!
    @IBOutlet weak var subHeadingLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var showHideButton: OASecondaryButton!
    
    @IBOutlet weak var bottomMarginView: UIView!
    @IBOutlet weak var topMarginView: UIView!
    @IBOutlet weak var collapseArrowImage: UIImageView!
    @IBOutlet weak var subtitleLabel: BodyTextRegularGreyLabel!
    
    weak var delegate: TimeLineHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        showHideButton.setTitle(Strings.WHC.inspection.inspectionDetailHeaderView.showDetails, for: .normal)
        showHideButton.setTitle(Strings.WHC.inspection.inspectionDetailHeaderView.hideDetails, for: .selected)
//        headingLabel.font = DLSFont.h3.regular
//        subHeadingLabel.font = DLSFont.bodyText.regular
//        showHideButton.titleLabel?.font = DLSFont.bodyText.regular
//
//        headingLabel.textColor = UIColor.charcoalGrey
//        subHeadingLabel.textColor = UIColor.lightGray
//        showHideButton.setTitleColor(UIColor.dodgerBlue, for: .normal)
    }
    
    func set(state: TimelineHeaderState, title: String, subHeading: String?, isShowHideView: Bool, forSection section: Int) {
        
        tag = section
        
        showHideButton.isHidden = !isShowHideView
        collapseArrowImage.isHidden = !isShowHideView
        subHeadingLabel.textColor = UIColor.charcoalGrey
        isUserInteractionEnabled = isShowHideView
        
        switch state {
        case .notStarted:
            setNotStaredView(title: title)
        case .started:
            setStartedView(title: title)
        case .onHold:
            setOnHoldView(title: title)
        case .completed:
            setCompletedView(title: title)
        }
        
        checkSubtitle(subtitle: subHeading)
    }
    
    func toggleButtonState(_ selected: Bool) {
        showHideButton.isSelected = selected
        let angle: CGFloat = selected ? .pi : 0
        collapseArrowImage.transform = CGAffineTransform(rotationAngle: angle)
    }
    
    func hideTopMarginView() {
        topMarginView.isHidden = true
    }
    //static let notStartedTimeLineImageName = #imageLiteral(resourceName:"timelineInactive")
    //                static let startedTimeLineImageName = #imageLiteral(resourceName:"timelineInProgress");
    //                static let completedTimeLineImageName = #imageLiteral(resourceName:"timelineDone")
    //                static let onHoldTimelineImageName = #imageLiteral(resourceName:"pendingInProgress")
    
    func hideBottomMarginView() {
        bottomMarginView.isHidden = true
    }
    
    private func setNotStaredView(title: String) {
        headingLabel.isHidden = true
        subHeadingLabel.text = title
        subHeadingLabel.textColor = UIColor.bodyTextGray
        statusImageView.image = #imageLiteral(resourceName: "timelineInactive")
        
        showHideButton.isHidden = true
        collapseArrowImage.isHidden = true
        
        isUserInteractionEnabled = false
    }
    
    private func setStartedView(title: String) {
        subHeadingLabel.isHidden = true
        headingLabel.text = title
        statusImageView.image = #imageLiteral(resourceName: "timelineInProgress")
    }
    
    private func setOnHoldView(title: String) {
        subHeadingLabel.isHidden = true
        headingLabel.text = title
        statusImageView.image = #imageLiteral(resourceName:"pendingInProgress")
    }
    
    private func setCompletedView(title: String) {
        subHeadingLabel.isHidden = true
        headingLabel.text = title
        statusImageView.image = #imageLiteral(resourceName:"timelineDone")
    }
    
    private func checkSubtitle(subtitle: String?) {
        if let subtitle = subtitle {
            showHideButton.isUserInteractionEnabled = false
            collapseArrowImage.isHidden = true
            showHideButton.isHidden = true
            
            subtitleLabel.isHidden = false
            
            subtitleLabel.text = subtitle
        } else {
            subtitleLabel.isHidden = true
        }
    }
    
    @IBAction func clickedBtnCollapse(_ sender: Any) {
        delegate?.collapseClicked(at: tag, expanded: !showHideButton.isSelected)
    }
}
