//
//  GifterOptionsHeaderView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 12/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

class GifterOptionsHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var radioIconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var selectionClosure: (() -> Void)?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(headerTapped)))
    }
    
    func setupModel(_ model: TableViewSection<GifterInputCellModel>, closure: (() -> Void)? = nil){
        titleLabel.text = model.title
        selectionClosure = closure
        if model.isSelected {
            radioIconImageView.image = #imageLiteral(resourceName: "radioButtonActive")
        }
        else {
            radioIconImageView.image = #imageLiteral(resourceName: "radioButtonInactive")
        }
        titleLabel.setSelectedOrNot(model.isSelected)
    }
    
    @IBAction func onClickSelectView(_ sender: UIButton?){
        selectionClosure?()
    }
    
    @objc func headerTapped() {
        selectionClosure?()
    }

}
