//
//  GifterDropdownInputCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 12/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

protocol GifterDropdownInputDelegate:class {
    func updateDropdoenInputValue()
}

class GifterDropdownInputCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var dropdownInputView: TextFieldView!
    @IBOutlet weak var inputTextField: TextFieldView!
    var rowModel: GifterInputCellModel?
    var dropdownOptions: [LanguageData] = []
    weak var delegate: GifterDropdownInputDelegate?
    var tableView: UITableView? {
        return parentView(of: UITableView.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dropdownInputView.fieldType = NameFieldType.self
        dropdownInputView.delegate = self
        inputTextField.fieldType = NameFieldType.self
        inputTextField.delegate = self
    }
    
    func setupModel(model: GifterInputCellModel, dropdownOptions: [LanguageData]){
        rowModel = model
        self.dropdownOptions = dropdownOptions
        dropdownInputView.descriptionText = model.title
        dropdownInputView.fieldText = model.dropDownOption
        dropdownInputView.enableFieldViewButton(true, showRightImage: true)
        inputTextField.isHidden = true
        inputTextField.descriptionText = Strings.GifterScenario.pleasespecifyRelationship
        if let dropdownvalue = model.dropDownOption, dropdownvalue == Strings.RelationShips.other {
            inputTextField.isHidden = false
            inputTextField.fieldText = model.inputValue
        }
    }
    
    func presentSelectLanguage() {
        let timePicker = MHCCategoryBottomSheetView()
        timePicker.setRelationDataModel(dropdownOptions, delegate: self)
        Utilities.presentPopover(view: timePicker, height: MHCCategoryBottomSheetView.languageHeight(forModel: dropdownOptions))
    }
}

extension GifterDropdownInputCell: TextFieldViewDelegate {
    func textFieldViewShouldBeginEditing(_ textFieldView: TextFieldView) -> Bool {
        if textFieldView == inputTextField {
            return true
        }
        return false
    }
    
    func textFieldViewDidEndEditing(_ textFieldView: TextFieldView) {
        if textFieldView == inputTextField {
            self.delegate?.updateDropdoenInputValue()
        }
    }
    
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textFieldView == inputTextField {
            let str = (textFieldView.textFieldObj.text ?? "") as NSString
            let strNew = str.replacingCharacters(in: range, with: string)
          //  let isInvalid = !(textFieldView.fieldType.validateTextLimit(text: strNew))
            self.rowModel?.inputValue = strNew
        }
        return true
    }
    

    func textViewBtnClicked(_ textFieldView: TextFieldView) {
        textFieldView.animateTextField(state: .normal)
        presentSelectLanguage()
    }
}

extension GifterDropdownInputCell: MHCCategoryBottomSheetViewDelegate {
    
    func dismiss() {
           Utilities.topMostPresenterViewController.dismiss(animated: true)
       }
    
    func didSelectLanguage(language: LanguageData) {
        self.rowModel?.dropDownOption = language.language
        dropdownInputView.fieldText = self.rowModel?.dropDownOption
        if language.language == Strings.RelationShips.other {
            inputTextField.isHidden = false
            tableView?.beginUpdates()
            tableView?.endUpdates()
        }else {
            inputTextField.isHidden = true
            tableView?.beginUpdates()
            tableView?.endUpdates()
        }
        self.delegate?.updateDropdoenInputValue()
    }
}
