//
//  GifterInputCellTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 12/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

protocol GifterInputCellDelegate:class {
    func updateInputValue()
}

class GifterInputCellTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var inputTextField: TextFieldView!

    var rowModel: GifterInputCellModel?
    weak var delegate: GifterInputCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        inputTextField.fieldType = NameFieldType.self
        inputTextField.delegate = self
        // Initialization code
    }
    
    func setupModel(_ model: GifterInputCellModel){
        self.rowModel = model
        inputTextField.descriptionText = model.title
        inputTextField.fieldText = model.inputValue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension GifterInputCellTableViewCell : TextFieldViewDelegate {
    func textFieldViewDidEndEditing(_ textFieldView: TextFieldView) {
        self.delegate?.updateInputValue()
    }
    
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textFieldView.textFieldObj.text ?? "") as NSString
        let strNew = str.replacingCharacters(in: range, with: string)
      //  let isInvalid = !(textFieldView.fieldType.validateTextLimit(text: strNew))
        self.rowModel?.inputValue = strNew
        return true
    }
}
