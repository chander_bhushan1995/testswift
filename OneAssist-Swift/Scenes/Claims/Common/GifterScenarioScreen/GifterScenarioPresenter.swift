//
//  GifterScenarioPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 11/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

protocol GifterScenarioPresenterDisplayLogic: class {
    func displayOptions(section: [TableViewSection<GifterInputCellModel>])
    func displayRelationShipOptions(relationship: [LanguageData])
}

class GifterScenarioPresenter: BasePresenter, GifterScenarioPresentationLogic {
    weak var delegate : GifterScenarioPresenterDisplayLogic?
    
    func getRelationShipOptions() {
        var relationShips: [LanguageData] = []
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.father, order: NSNumber(value: 1)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.mother, order: NSNumber(value: 2)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.brother, order: NSNumber(value: 3)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.sister, order: NSNumber(value: 4)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.child, order: NSNumber(value: 5)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.husband, order: NSNumber(value: 6)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.wife, order: NSNumber(value: 7)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.friend, order: NSNumber(value: 8)))
        relationShips.append(setupRelationShip(relationship: Strings.RelationShips.other, order: NSNumber(value: 9)))
        
        self.delegate?.displayRelationShipOptions(relationship: relationShips)
    }
    
    func setupRelationShip(relationship: String, order: NSNumber) -> LanguageData {
        let relation = LanguageData(dictionary: [:])
        relation.order = order
        relation.language = relationship
        return relation
    }
    
    func getOptions() {
        var dataSource: [TableViewSection<GifterInputCellModel>] = []
        
        dataSource.append(self.setNameMismatch())
        dataSource.append(self.setgiftedSomeone())
        dataSource.append(self.setloanedSomeone())
        dataSource.append(self.setOthers())
        
        self.delegate?.displayOptions(section: dataSource)
    }
    
    func setNameMismatch() -> TableViewSection<GifterInputCellModel> {
        let section: TableViewSection<GifterInputCellModel> = TableViewSection<GifterInputCellModel>()
        section.title = Strings.GifterScenario.spellingMistake
        section.titleType = Strings.GifterScenario.GifterTypeValue.spellingMistakeType
        section.isSelected = false
        
        let row = GifterInputCellModel()
        row.title = Strings.GifterScenario.enterName
        row.type = GifterCellType.inputType
        row.keyName = Strings.GifterScenario.assetAttributeKeyType.mismatchName
        
        //setup data
        if let additionalDetail = ClaimHomeServe.shared.additionalDetails {
            if let mismatchCustomerName = additionalDetail.mismatchCustomerName {
                row.inputValue = mismatchCustomerName
            }
        }
        
        section.rows = [row]
        setupSectionSelectionData(section: section)
        return section
    }
    
    func setupSectionSelectionData(section: TableViewSection<GifterInputCellModel>) {
        if let additionalDetail = ClaimHomeServe.shared.additionalDetails {
            if let customerNameInvoiceMismatch = additionalDetail.customerNameInvoiceMismatch,
               customerNameInvoiceMismatch == "Y" {
                if  let invoiceNameVariationReason = additionalDetail.invoiceNameVariationReason,
                    invoiceNameVariationReason == section.titleType {
                    section.isSelected = true
                }
            }
        }
    }
    
    func setgiftedSomeone() -> TableViewSection<GifterInputCellModel> {
        let section: TableViewSection<GifterInputCellModel> = TableViewSection<GifterInputCellModel>()
        section.title = Strings.GifterScenario.giftedSomeone
        section.titleType = Strings.GifterScenario.GifterTypeValue.gifterSomeoneType
        section.isSelected = false
        
        let row = GifterInputCellModel()
        row.title = Strings.GifterScenario.gifterName
        row.type = GifterCellType.inputType
        row.keyName = Strings.GifterScenario.assetAttributeKeyType.gifterName
        
        let row1 = GifterInputCellModel()
        row1.title = Strings.GifterScenario.relationshipGifter
        row1.type = GifterCellType.dropDowmType
        row1.keyName = Strings.GifterScenario.assetAttributeKeyType.gifterRelation
        
        //setup data
        if let additionalDetail = ClaimHomeServe.shared.additionalDetails {
            if let gifterName = additionalDetail.gifterName {
                row.inputValue = gifterName
                
            }
            
            if let relation = additionalDetail.customerGifterRelation {
                if Strings.RelationShips.allValues.contains(relation.uppercased()){
                    row1.dropDownOption = relation.capitalized
                }else {
                    row1.dropDownOption = Strings.RelationShips.other
                    row1.inputValue = relation
                }
            }
        }
        
        section.rows = [row, row1]
        setupSectionSelectionData(section: section)
        
        return section
    }
    
    func setloanedSomeone() -> TableViewSection<GifterInputCellModel> {
        let section: TableViewSection<GifterInputCellModel> = TableViewSection<GifterInputCellModel>()
        section.title = Strings.GifterScenario.loaned
        section.titleType = Strings.GifterScenario.GifterTypeValue.loanedType
        section.isSelected = false
        
        setupSectionSelectionData(section: section)
        
        return section
    }
    
    func setOthers() -> TableViewSection<GifterInputCellModel> {
        let section: TableViewSection<GifterInputCellModel> = TableViewSection<GifterInputCellModel>()
        section.title = Strings.GifterScenario.others
        section.titleType = Strings.GifterScenario.GifterTypeValue.othersType
        section.isSelected = false
        
        let row = GifterInputCellModel()
        row.title = Strings.GifterScenario.pleasespecify
        row.type = GifterCellType.inputType
        row.keyName = Strings.GifterScenario.assetAttributeKeyType.otherReason
        
        //setup data
        if let additionalDetail = ClaimHomeServe.shared.additionalDetails {
            if let otherReason = additionalDetail.customerNameMismatchOtherReason {
                row.inputValue = otherReason
            }
        }
        
        section.rows = [row]
        setupSectionSelectionData(section: section)
        
        return section
    }
    
}

