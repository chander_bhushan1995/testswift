//
//  GIfterScenarioVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 11/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

struct GifterScenarioModel {
    var selectedSection: Int?
    var sections: [TableViewSection<GifterInputCellModel>] = []
}

class GifterInputCellModel: Row {
    var inputValue: String?
    var dropDownOption: String?
    var keyName: String!
}

enum GifterCellType: String {
    case inputType = "inputType"
    case dropDowmType = "dropDowmType"
}

protocol GifterScenarioBusinessLogic {
    func getOptions()
    func getRelationShipOptions()
}

protocol GIfterScenarioDelegate: class {
    func clickOnGifterScenario(with aditionalInfo: AdditionalDetail?)
}

class GIfterScenarioVC: BaseVC, PagingProtocol {
    var currentPage: Int = 0
    var interactor: GifterScenarioBusinessLogic?
    weak var delegate: GIfterScenarioDelegate?
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var controlButton: CustomSegmentControl!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitButton: OAPrimaryButton!
    
    let headerView = UIView()
    var dataSource: GifterScenarioModel = GifterScenarioModel()
    var relationShipData: [LanguageData] = []
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func initializeView() {
        let customerName = UserCoreDataStore.currentUser?.userName ?? ""
        titleLable.text =  Strings.GifterScenario.nameConfirmation +  " \(customerName)?"
        
        if let additionalDetail = ClaimHomeServe.shared.additionalDetails,
           let customerNameInvoiceMismatch = additionalDetail.customerNameInvoiceMismatch {
            controlButton.selectedSegmentIndex = customerNameInvoiceMismatch == "Y" ? 1 : 0
        }else {
            controlButton.selectedSegmentIndex = -1
        }
        
        submitButton.isEnabled = false
        
        let headerTitleLabel = UILabel()
        headerTitleLabel.numberOfLines = 0
        headerTitleLabel.font  = DLSFont.h3.bold
        headerTitleLabel.textColor = UIColor.charcoalGrey
        headerTitleLabel.text = Strings.GifterScenario.reasonHeading
        headerTitleLabel.frame = CGRect(x: 16, y: 8, width: UIScreen.main.bounds.width-32, height: headerTitleLabel.frame.height)
        headerTitleLabel.sizeToFit()
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: headerTitleLabel.frame.height + 16)
        headerView.addSubview(headerTitleLabel)
        tableView.tableHeaderView = headerView
        headerView.isHidden = true
        
        tableView.register(cell: GifterInputCellTableViewCell.self)
        tableView.register(cell: GifterDropdownInputCell.self)
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.GifterOptionsHeaderView)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 48
        
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 24
        
        tableView.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.white
        
        interactor?.getOptions()
        interactor?.getRelationShipOptions()
        
        sumitButtonEnable()
    }
    
    @IBAction func onClickSubmitAction(_ sender: UIButton?) {
        let aditionalData: AdditionalDetail = AdditionalDetail(dictionary: [:])
        aditionalData.customerNameInvoiceMismatch = controlButton.selectedSegmentIndex == 0 ? "N" : "Y"
        if controlButton.selectedSegmentIndex == 1 {
            let selectedOption = dataSource.sections.filter({$0.isSelected})
            if selectedOption.count > 0 {
                aditionalData.invoiceNameVariationReason = selectedOption.first?.titleType
                if let rows = selectedOption.first?.rows, rows.count > 0 {
                    rows.forEach { (model) in
                        if model.keyName == Strings.GifterScenario.assetAttributeKeyType.mismatchName {
                            aditionalData.mismatchCustomerName = model.inputValue
                            
                        }else if model.keyName == Strings.GifterScenario.assetAttributeKeyType.gifterName {
                            aditionalData.gifterName = model.inputValue
                            
                        }else if model.keyName == Strings.GifterScenario.assetAttributeKeyType.otherReason {
                            aditionalData.customerNameMismatchOtherReason = model.inputValue
                            
                        }else if model.keyName == Strings.GifterScenario.assetAttributeKeyType.gifterRelation {
                            var value = model.inputValue
                            if let option = model.dropDownOption {
                                if option != Strings.RelationShips.other {
                                    value = model.dropDownOption
                                }
                            }
                            aditionalData.customerGifterRelation = value
                        }
                    }
                }
            }
        }
        self.delegate?.clickOnGifterScenario(with: aditionalData)
    }
    
    @IBAction func OnClickSegmentAction(_ sender: UISegmentedControl?){
        headerView.isHidden = true
        if controlButton.selectedSegmentIndex == 1 {
            headerView.isHidden = false
        }
        self.tableView.reloadData()
        sumitButtonEnable()
    }
    
    func sumitButtonEnable() {
        submitButton.isEnabled = validateForm()
    }
    
    func validateForm()->Bool {
        var isValid = true
        if controlButton.selectedSegmentIndex == -1 {
            isValid = false
            
        }else if controlButton.selectedSegmentIndex == 1 {
            let selectedOption = dataSource.sections.filter({$0.isSelected})
            if selectedOption.count > 0 {
                if let rows = selectedOption.first?.rows, rows.count > 0 {
                    rows.forEach { (model) in
                        if model.type as! GifterCellType == GifterCellType.inputType {
                            if let inputValue = model.inputValue, !inputValue.isEmpty {} else {
                                isValid = false
                                return
                            }
                        }else if model.type as! GifterCellType == GifterCellType.dropDowmType {
                            if let option = model.dropDownOption {
                                if option == Strings.RelationShips.other {
                                    if let inputValue = model.inputValue, !inputValue.isEmpty {} else {
                                        isValid = false
                                        return
                                    }
                                }
                            }else {
                                isValid = false
                            }
                        }
                        
                    }
                }
            }else {
                isValid = false
            }
        }
        return isValid
    }
}


// MARK:- Table DataSource
extension GIfterScenarioVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if controlButton.selectedSegmentIndex == 1 {
            return dataSource.sections.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if controlButton.selectedSegmentIndex == 1 {
            let sectionData = dataSource.sections[section]
            if sectionData.isSelected == true {
                return sectionData.rows.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource.sections[indexPath.section].rows[indexPath.row]
        if model.type as! GifterCellType == GifterCellType.inputType{
            let cell: GifterInputCellTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.delegate = self
            cell.setupModel(model)
            return cell
            
        }else if model.type as! GifterCellType == GifterCellType.dropDowmType{
            let cell: GifterDropdownInputCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.setupModel(model: model, dropdownOptions: relationShipData)
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
        
    }
}

// MARK:- Table Delegate
extension GIfterScenarioVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let model = dataSource.sections[section]
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.GifterOptionsHeaderView) as! GifterOptionsHeaderView
        headerView.setupModel(model) {
            if !model.isSelected {
                self.dataSource.sections.forEach { (object) in
                    object.isSelected = false
                }
                model.isSelected = true
                tableView.reloadData()
                self.sumitButtonEnable()
            }
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}

extension GIfterScenarioVC: GifterInputCellDelegate {
    func updateInputValue() {
        sumitButtonEnable()
    }
}

extension GIfterScenarioVC: GifterDropdownInputDelegate {
    func updateDropdoenInputValue() {
        sumitButtonEnable()
    }
}

extension GIfterScenarioVC: GifterScenarioPresenterDisplayLogic {
    func displayOptions(section: [TableViewSection<GifterInputCellModel>]) {
        self.dataSource.sections = section
        tableView.reloadData()
    }
    
    func displayRelationShipOptions(relationship: [LanguageData]) {
        self.relationShipData = relationship
    }
}

// MARK:- Configuration Logic
extension GIfterScenarioVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = GifterScenarioInteractor()
        let presenter = GifterScenarioPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.delegate = self
    }
}
