//
//  GifterScenarioInteractor.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 11/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

protocol GifterScenarioPresentationLogic {    
     func getOptions()
     func getRelationShipOptions()
}

class GifterScenarioInteractor: BaseInteractor, GifterScenarioBusinessLogic {
    var presenter: GifterScenarioPresentationLogic?
    
    
    func getOptions() {
        presenter?.getOptions()
    }
    
    func getRelationShipOptions() {
        presenter?.getRelationShipOptions()
    }
    
}
