//
//  PickerView.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 28/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol PickerMethods:class {
    func clickDoneBtn(_ date:String)
}
class PickerView: UIView,NibLoadableView {
    var startDate :Date? {
        didSet {
            self.datePicker.minimumDate = startDate
        }
    }
    var endDate:Date? {
        didSet {
            self.datePicker.maximumDate = endDate
        }
    }

    //@IBOutlet weak var segmentController: UISegmentedControl!
    
   // @IBOutlet weak var picker: UIPickerView!
    weak var delegate:PickerMethods?
   // var pickerData: [[String]] = [[String]]()
//    var AMorPM :String = "AM"
//    var hour = "1"
//    var minutes = "00"
//@IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblDate.text = "Today, " + "\(DateFormatter.time12period.string(from: Date.currentDate()))"
        endDate = Date.currentDate()
        datePicker.maximumDate = Date.currentDate()
        let date = gregorianCalendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
        let sevenDaysAgo = gregorianCalendar.date(byAdding: .day, value: -7, to: date)
        //var puchaseDate = Date(timeIntervalSince1970:ClaimHomeServe.shared.purchaseDate as! TimeInterval)
        
        if let date = ClaimHomeServe.shared.purchaseDate {
            let timeinterval : TimeInterval =  TimeInterval(date.doubleValue) // convert it in to NSTimeInteral
            let purchaseDate = Date(timeIntervalSince1970:timeinterval/1000) // you can the Date object from here
            startDate = sevenDaysAgo
    //        datePicker.minimumDate = sevenDaysAgo
            
            if purchaseDate.compare(sevenDaysAgo!) == .orderedDescending {
                startDate = purchaseDate as Date
    //            datePicker.minimumDate = purchaseDate
            }
        }
        
        initialise()
    }
    
    func initialise(){
    }
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        let date1 = gregorianCalendar.date(bySettingHour: 0, minute: 0, second: 0, of: datePicker.date)!
        let date2 = gregorianCalendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date.currentDate())!
        if date1 == date2 {
            lblDate.text = "Today, " + "\(DateFormatter.time12period.string(from: datePicker.date))"
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = gregorianCalendar
        dateFormatter.dateFormat = "E d MMM yy,h:mm a"
        let strDate = dateFormatter.string(from: datePicker.date)
        self.lblDate.text = strDate
    }
    
    @IBAction func clickDoneButton(_ sender: Any) {
        let dateForDelegate = DateFormatter.scheduleFormat.string(from: datePicker.date)
        self.delegate?.clickDoneBtn(dateForDelegate)
    }
    
    func setDate(date:String?){
        if let date1 = date{
        let ConvertedDate: Date? = DateFormatter.scheduleFormat.date(from: date1)
            if ((startDate! ... endDate!).contains(ConvertedDate!)) {
                self.delegate?.clickDoneBtn(date1)
            }
    }
}
}

