//
//  WhenHappenVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 28/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

/// This delegate is used to give callback on click of when happened buttons
protocol ClaimWhenHappenDelegate: class {
    
    /// clicked button when happened
    ///
    /// - Parameter date: date of incident
    func clickWhenHapppen(date : String)
}

class WhenHappenVC: BaseVC ,PagingProtocol{
    var category : Constants.Services! = .extendedWarranty
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    var dateToSend:String?
    @IBOutlet weak var BtnEnterAddress: OAPrimaryButton!
    @IBOutlet weak var calendarView: OACalendarView!
    var prefetchedDateSelected : String?
    var createdOnDate:String?    // if claim raised from CRM
    var currentPage: Int = 0
    weak var delegate:ClaimWhenHappenDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialise()
        calendarView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        EventTracking.shared.addScreenTracking(with: .SR_Issue_DateTime)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    private func initialise() {
        
        //Useless code...
        BtnEnterAddress.setTitle("Submit", for: .normal)
        BtnEnterAddress.setTitle("Submit", for: .disabled)
        setUpBoundaryDate()
        
        if let homeCover = self.category {
            switch homeCover {
            case .accidentalDamage:
                self.lblTitle.text = String(format: Strings.ServiceRequest.WhenHappend.titleEW, ClaimHomeServe.shared.productName ?? "appliance")
                self.lblSubtitle.text = Strings.ServiceRequest.WhenHappend.subtitle
                BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .normal)
                BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .disabled)
            case .breakdown :
                self.lblTitle.text = String(format: Strings.ServiceRequest.WhenHappend.titleEW, ClaimHomeServe.shared.productName ?? "appliance")
                self.lblSubtitle.text = Strings.ServiceRequest.WhenHappend.subtitle
                BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.scheduleAction, for: .normal)
                BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.scheduleAction, for: .disabled)
            case .fire :
                fallthrough
            case .buglary:
                self.lblTitle.text = Strings.ServiceRequest.WhenHappend.titleBGFR
                self.lblSubtitle.text = Strings.ServiceRequest.WhenHappend.subtitle
                BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .normal)
                BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .disabled)
            case .pms:
                break
            case .extendedWarranty:
                self.lblTitle.text = String(format: Strings.ServiceRequest.WhenHappend.titleEW, "appliance")
                self.lblSubtitle.text = Strings.ServiceRequest.WhenHappend.subtitle
                self.BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.tellusMore, for: .normal)
                self.BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.tellusMore, for: .disabled)
                break
            default:
                self.lblTitle.text = String(format: Strings.ServiceRequest.WhenHappend.titleEW, "appliance")
                self.lblSubtitle.text = Strings.ServiceRequest.WhenHappend.subtitle
                self.BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .normal)
            }
        } else {
            self.lblTitle.text = String(format: Strings.ServiceRequest.WhenHappend.titleEW, "appliance")
            self.lblSubtitle.text = Strings.ServiceRequest.WhenHappend.subtitle
            self.BtnEnterAddress.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .normal)
        }
        
        if let cat = category {
            switch cat {
            case .peADLD:
                BtnEnterAddress.setTitle("Add Details", for: .normal)
                BtnEnterAddress.setTitle("Add Details", for: .disabled)
            default:
                break
            }
        }
        
        self.BtnEnterAddress.isEnabled = false
        if let prefetchedDateString = prefetchedDateSelected {
            self.BtnEnterAddress.isEnabled = true
            let dateFormatter = DateFormatter.scheduleFormat
            let prefetchedDate = dateFormatter.date(from: prefetchedDateString) ?? Date()
            dateToSend =  dateFormatter.string(from: prefetchedDate)
        }
    }
    
    func setBorders(_ button:UIButton){
        button.layer.borderColor = UIColor.dodgerBlue.cgColor
        button.layer.borderWidth = 1
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.dodgerBlue, for: .normal)
    }
    @IBAction func clickEnterAddress(_ sender: Any) {
        //  BtnEnterAddress.isUserInteractionEnabled = false
        self.delegate?.clickWhenHapppen(date : dateToSend!) //e.g. "09-May-2019 10:43:00"
    }
    @IBAction func clickPickerBtn(_ sender: Any) {
    }
    
    func setButtonUI(_ sender:UIButton){
        let tag = sender.tag
        for i in 1..<3{
            if(i != tag){
                setBorders(self.view.viewWithTag(i) as! UIButton)
            }else{
                self.view.viewWithTag(i)?.backgroundColor = UIColor.dodgerBlue
                (self.view.viewWithTag(i) as! UIButton).setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
}

extension WhenHappenVC: OACalendarViewDelegate{
    func dateChanged(_ date: Date, view: OACalendarView) {
        self.BtnEnterAddress.isEnabled = true
        let dateFormatter = DateFormatter.scheduleFormat
        dateToSend =  dateFormatter.string(from: date)
        let maxDateCouldSelect = createdOnDate ?? Date().string(with: dateFormatter)
        if Date.compareDate(fromdate: dateFormatter.date(from: dateToSend ?? "") ?? Date(), todate: dateFormatter.date(from: maxDateCouldSelect) ?? Date()) == 1{
            self.BtnEnterAddress.isEnabled = false
            let window = UIApplication.shared.keyWindow
            window?.rootViewController?.view.makeToast(Strings.ToastMessage.invalidInputDate)
        }
    }
}

extension WhenHappenVC {
    fileprivate func setUpBoundaryDate(){
        if self.category.isPEClaimType() {
            let dateFormatter = DateFormatter.incidentTimeFormat
            dateFormatter.setLocal()
            if let prefetchedDateSelected = prefetchedDateSelected, let selectedDate12 = dateFormatter.date(from: prefetchedDateSelected){
                calendarView.setInitialDate(date: selectedDate12)
            }
            calendarView.minimumDate = Date(string: Strings.Global.minDateString, formatter: .dayMonthAndYear)
            calendarView.date = Date()
        } else {
            let dateFormatter = DateFormatter.scheduleFormat
            if let prefetchedDateSelected = prefetchedDateSelected, let selectedDate12 = dateFormatter.date(from: prefetchedDateSelected){
                calendarView.setInitialDate(date: selectedDate12)
                calendarView.date = selectedDate12
            }
            if let createdOn = createdOnDate, let maxSelectData12 = dateFormatter.date(from: createdOn){
                calendarView.maximumDate = maxSelectData12
            }
            calendarView.dateFooterNote = Strings.ServiceRequest.WhenHappend.incidentDataCondition
        }
    }
}
