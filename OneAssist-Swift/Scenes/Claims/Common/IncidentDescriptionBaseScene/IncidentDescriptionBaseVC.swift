//
//  IncidentDescriptionBaseVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/02/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol HowAndWhereHappenedModel {
    var titleText: String {set get}
    var subtitleTxtViewText: NSAttributedString {set get}
    var linkText: String {get}
    var describeHeadingText: String {set get}
    var charactersText: String {set get}
    var whereLblText: String {set get}
    var fieldPlaceText: String {set get}
    var fieldPlacePlaceholderText: String {set get}
    var actionTitle: String {set get}
    var inputAccesoryText: String {set get}
}

/// This delegate is used to give callback on click of incident description buttons
protocol IncidentDescriptionBaseVCDelegate {
    
    /// clicked on tell us more button
    ///
    /// - Parameters:
    ///   - descripition: entered description 
    ///   - placeOfIncident: place of incident
    func clickTellUsMore(descripition: String , placeOfIncident: String?)
}

class IncidentDescriptionBaseModel: HowAndWhereHappenedModel {
    
    
    var titleText: String = "Please brief us a bit more on how did this happen"
    
    var subtitleTxtViewText: NSAttributedString = {
        let atrStr = NSMutableAttributedString(string: "This is important and will take only a minute of your time. Narrate the incident in detail which led to damages to your device ", attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray])
        let atrStr2 = NSAttributedString(string: "(Atleast \(RemoteConfigManager.shared.minCharacterIncidentDescription) characters)  \n", attributes: [NSAttributedString.Key.font: UIFont(name: "Lato-Bold", size: 15)!, NSAttributedString.Key.foregroundColor: UIColor.gray])
        //  let atrStr3 = NSAttributedString(string: "See sample description", attributes: [NSAttributedString.Key.font: DLSFont.supportingText.regular, NSAttributedString.Key.foregroundColor: UIColor.dodgerBlue])
        //atrStr.append(atrStr3)
        atrStr.append(atrStr2)
        
        return atrStr
    } ()
    
    var linkText: String { return "See sample description"}
    
    var describeHeadingText: String = "DESCRIBE THE INCIDENT"
    
    var charactersText: String = "0/\(RemoteConfigManager.shared.maxCharacterIncidentDescription)"
    
    var whereLblText: String = "And, where did this happen?"
    
    var fieldPlaceText: String = "Place of incident"
    
    var fieldPlacePlaceholderText: String = "For eg; Golf Course Road, Gurgaon"
    
    var actionTitle: String = "Tell us more"
    
    var inputAccesoryText: String = "should be atleast \(RemoteConfigManager.shared.minCharacterIncidentDescription) characters"
}

class IncidentDescriptionBaseVC: BaseVC {
    
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var lblTitle: H3BoldLabel!
    @IBOutlet weak var txtViewSubtitle: LinkTextView!
    @IBOutlet weak var lblDescribeHeading: BodyTextRegularBlackLabel!

    @IBOutlet var topSpaceToDescriptionLabel: NSLayoutConstraint!
    @IBOutlet weak var enterDescBtn: TransparentH3BoldButton!
    @IBOutlet weak var lblWhere: H3BoldLabel!
    @IBOutlet weak var fieldViewPlace: TextFieldView!
    @IBOutlet weak var btnPrimary: OAPrimaryButton!
    
    @IBOutlet weak var descriptionContainerView: UIView!
    var delegate : IncidentDescriptionBaseVCDelegate?
    var handler: TextFieldViewDelegateHandler!
    var howAndWhereHappenedModel: HowAndWhereHappenedModel {
        return IncidentDescriptionBaseModel()
    }

    var minimumCharacters = Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140
    var maxCharacters =  Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400
    var descriptionText: String?
    var prefetchIncidentPlace: String?
    var isWhere: Bool {
        return true
    }
    
    var enterDescriptionText: String? {
        didSet {
            let text = enterDescriptionText != nil ? "Edit incident description" : "Enter incident description"

            self.arrowImage.isHidden = enterDescriptionText != nil
            self.enterDescBtn.setTitle(text, for: .normal)
            
            self.lblDescribeHeading.text = enterDescriptionText
            
//            var bool = false
//            if !self.fieldViewPlace.isHidden {
//                bool = self.handler.invalidFieldCount == 0 && validateDescription(enterDescriptionText)
//            } else {
//                bool = validateDescription(enterDescriptionText)
//            }
//
//            self.btnPrimary.isEnabled = bool
            topSpaceToDescriptionLabel.isActive = enterDescriptionText != nil
        }
    }
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    func hidePlaceOfIncident(hide:Bool){
        
    }
    
    // MARK:- private methods
    
    private func initializeView() {
        
        addTap()
        
        lblWhere.isHidden = !isWhere
        fieldViewPlace.isHidden = !isWhere
        lblDescribeHeading.numberOfLines = isWhere ? 1 : 6
//        lblDescribeHeading.textColor = UIColor.darkGray
        
        fieldViewPlace.hasImage = false
        btnPrimary.isEnabled = true
        
        lblTitle.text = howAndWhereHappenedModel.titleText
        txtViewSubtitle.setAttributedLink(completeText: howAndWhereHappenedModel.subtitleTxtViewText, linkText: howAndWhereHappenedModel.linkText)
        txtViewSubtitle.linkDelegate = self
        
        lblWhere.text = howAndWhereHappenedModel.whereLblText
        
        fieldViewPlace.descriptionText = howAndWhereHappenedModel.fieldPlaceText
        fieldViewPlace.placeholderFieldText = howAndWhereHappenedModel.fieldPlacePlaceholderText
        
        handler = TextFieldViewDelegateHandler(requiredFields: [fieldViewPlace], validationHandler: {[weak self] (isValid, tuple) in
            
            guard self != nil else {
                return
            }
            
            self!.btnPrimary.isEnabled = true //self!.validateDescription(self!.enterDescriptionText) && isValid
        })
        
        btnPrimary.setTitle(howAndWhereHappenedModel.actionTitle, for: .normal)
        
        fieldViewPlace.textFieldObj.text = prefetchIncidentPlace
        fieldViewPlace.textFieldDidEndEditing(fieldViewPlace.textFieldObj)
        
        if let txt = descriptionText, txt.isEmpty {
            enterDescriptionText = nil
        } else {
            enterDescriptionText = descriptionText
        }
    }
    
    @IBAction func clickTellUsMore(btn :UIButton){
        if !validateEmptyDetails(){
            return
        }
         //EventTracking.shared.eventTracking(name: .SRIncidentDecriptionSubmit)

        EventTracking.shared.eventTracking(name: .SRDescription, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])


        self.delegate?.clickTellUsMore(descripition:enterDescriptionText ?? "", placeOfIncident: fieldViewPlace.textFieldObj.text)
    }
    
    fileprivate func validateDescription(_ description: String?) -> Bool {
        do {
            try Validation.validateDescription(text: description)
            if let count = description?.count {
                return count >= minimumCharacters && count <= maxCharacters
            }
        } catch {}
        return false
    }
    
    // MARK:- Action Methods
    func doneClicked() {
    
    }
    
    @IBAction func clickedBtnEnterDesc(_ sender: Any) {
        openEnterDescription(description: enterDescriptionText)
    }
    
}

// MARK:- Routing Logic
extension IncidentDescriptionBaseVC {
    func openEnterDescription(description: String?) {
        let enterDescVC = EnterDescriptionVC()
        enterDescVC.prevDescription = description
        enterDescVC.delegate = self
        navigationController?.pushViewController(enterDescVC, animated: true)
    }
}

// MARK:- Enter Description Conformance
extension IncidentDescriptionBaseVC: EnterDescriptionVCDelegate {
    func descriptionAdded(_ description: String) {
        self.enterDescriptionText = description
    }
}

extension IncidentDescriptionBaseVC: LinkTextViewDelegate {
    func linkClicked(linkText: String) {
        let vc = SampleDescriptionVC()
        vc.descriptionTVText = linkText
        presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension IncidentDescriptionBaseVC {
    fileprivate func validateEmptyDetails()->Bool{
        var isValid = true
        if (fieldViewPlace.isEmpty()){
            fieldViewPlace.setError(Strings.EmptyTextFieldError.emptyPlaceOfIncident)
            isValid = false
        }
        if !self.validateDescription(enterDescriptionText){
            self.showAlert(message: Strings.EmptyTextFieldError.invalidDescription)
            isValid = false
        }
        return isValid
    }
}
