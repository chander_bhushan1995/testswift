//
//  EditDescriptionInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 4/3/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol EditDescriptionPresentationLogic {
    
}

class EditDescriptionInteractor: BaseInteractor, EditDescriptionBusinessLogic {
    var presenter: EditDescriptionPresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
