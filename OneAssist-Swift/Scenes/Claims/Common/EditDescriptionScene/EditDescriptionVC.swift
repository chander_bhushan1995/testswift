//
//  EditDescriptionVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 4/3/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol EditDescriptionVCDelegate: class {
    func descriptionAdded(_ description: String, indexPath: IndexPath?)
}

protocol EditDescriptionBusinessLogic {
    
}

class EditDescriptionVC: BaseVC {
    weak var delegate: EditDescriptionVCDelegate?
    
    var interactor: EditDescriptionBusinessLogic?
    var prevDescription: String?
    var pendencyCellPath: IndexPath?
    
    fileprivate let minimumCharacters = Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140
    fileprivate var accesoryView: LabelInputAccesoryView!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var buttonSave: OAPrimaryButton!
//    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var charactersLabel: UILabel!
    @IBOutlet weak var placeholderLabel: BodyTextRegularGreyLabel!

      let placeholder = "For ex; How? Where? When? Explain in detail"
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if descriptionTextView.text.isEmpty{
            self.descriptionTextView.becomeFirstResponder()
        }
    }
    
    // MARK:- private methods
    private func initializeView() {
        title = "Edit Description"
//        lineView.backgroundColor = UIColor.dlsBorder
//        charactersLabel.text = "\(prevDescription?.count ?? 0)/\(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400)"
        let descCount = (prevDescription?.count ?? 0)
        let minChar = (Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140)
        if descCount < minChar {
            let characters = "character".validateSingularity(count: minChar - descCount)
            charactersLabel.text = "\(characters) remaining"
        } else {
            charactersLabel.text = "Maximum of \(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400) characters"
        }
        placeholderLabel.text = placeholder
        if prevDescription != nil {
            descriptionTextView.text = prevDescription
        }
        placeholderLabel.isHidden = !descriptionTextView.text.isEmpty
//
//        descriptionTextView.font = DLSFont.h3.regular
//        descriptionTextView.tintColor = UIColor.buttonTitleBlue
        
        accesoryView = Bundle.main.loadNibNamed("LabelInputAccesoryView", owner: self, options: nil)?.first as! LabelInputAccesoryView
        accesoryView.btnDone.addTarget(self, action: #selector(EditDescriptionVC.doneClicked), for: .touchUpInside)
        accesoryView.setUpdateUserInterface(isValid: descriptionTextView.text.count >= minimumCharacters, title: "Should be atleast 140 characters")
        accesoryView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 35)
        descriptionTextView.inputAccessoryView = accesoryView
        descriptionTextView.delegate = self
        descriptionTextView.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
        buttonSave.isEnabled = descriptionTextView.text.count >= minimumCharacters
        addTap()
//        descriptionTextView.becomeFirstResponder()
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnSave(_ sender: Any) {
        delegate?.descriptionAdded(descriptionTextView.text, indexPath: pendencyCellPath)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func doneClicked() {
        self.view.endEditing(true)
        //descriptionTextView.resignFirstResponder()
    }
}

// MARK:- Display Logic Conformance
extension EditDescriptionVC: EditDescriptionDisplayLogic {
    
}

// MARK:- Configuration Logic
extension EditDescriptionVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = EditDescriptionInteractor()
        let presenter = EditDescriptionPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension EditDescriptionVC {
    
}

extension EditDescriptionVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
//        lineView.backgroundColor = UIColor.buttonTitleBlue
        charactersLabel.textColor = textView.text.count > 0 ? UIColor.dodgerBlue : UIColor.bodyTextGray
        descriptionTextView.styleTextView(with: .white, borderColor: UIColor.buttonTitleBlue, shadowColor: UIColor.buttonTitleBlue)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
//        lineView.backgroundColor = UIColor.dlsBorder
        charactersLabel.textColor = UIColor.bodyTextGray
        descriptionTextView.styleTextView(with: .white, borderColor: UIColor.bodyTextGray, shadowColor: UIColor.bodyTextGray)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        do {
            try Validation.validateDescription(text: text)
        } catch {
            return false
        }
        
        let str = textView.text as NSString
        let strNew = str.replacingCharacters(in: range, with: text)
        
        guard strNew.count <= Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400 else {
            return false
        }
        
        let minChar = (Int(RemoteConfigManager.shared.minCharacterIncidentDescription) ?? 140)
        if strNew.count < minChar {
            let characters = "character".validateSingularity(count: minChar - strNew.count)
            charactersLabel.text = "\(characters) remaining"
        } else {
            charactersLabel.text = "Maximum of \(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400) characters"
        }
//        charactersLabel.text = "\(strNew.count)/\(Int(RemoteConfigManager.shared.maxCharacterIncidentDescription) ?? 400)"
        charactersLabel.textColor = strNew.count > 0 ? UIColor.dodgerBlue : UIColor.bodyTextGray
        
        buttonSave.isEnabled = strNew.count >= minimumCharacters
        accesoryView.isValid = strNew.count >= minimumCharacters
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
