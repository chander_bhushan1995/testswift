//
//  EditDescriptionPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 4/3/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol EditDescriptionDisplayLogic: class {
    
}

class EditDescriptionPresenter: BasePresenter, EditDescriptionPresentationLogic {
    weak var viewController: EditDescriptionDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
}
