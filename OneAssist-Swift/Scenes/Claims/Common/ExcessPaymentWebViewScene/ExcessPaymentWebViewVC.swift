//
//  ExcessPaymentWebViewVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 3/27/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ExcessPaymentWebViewBusinessLogic {
    func  getPaymentUrl(srID: String,adviceID:String)
}

/// This protocol contains all methods to give callback on success or failure of payment
protocol ExcessPaymentWebViewDelegate : class {
    
    /// Called when payment successful
    func paymentSuccessful()
    
    /// Called when payment fails
    func paymentFailure()
}

class ExcessPaymentWebViewVC: SecureWebViewVC, WKHTTPCookieStoreObserver {
    var interactor: ExcessPaymentWebViewBusinessLogic?
    weak var delegate : ExcessPaymentWebViewDelegate?
    
    var srId: String? 
    var adviceID: String?
    
    fileprivate var isVCPopped = false
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    deinit {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
  
    // MARK:- private methods
    override func initializeView() {
        title = Strings.PaymentWebViewScene.title
        handleBackButton()
        if srId != nil && adviceID != nil {
            Utilities.addLoader(onView: view, message: "Please wait", count: &loaderCount, isInteractionEnabled: true)
            self.createDopaymentUrl()
        }
    }
    
    func createDopaymentUrl() {
        webView.navigationDelegate = self
        let urlString = "\(Constants.SchemeVariables.baseDomainOld)/apigateway/OASYS/doPaymentViaLink?payToken=\(adviceID ?? "")&srNo=\(srId ?? "")&subscriptionType=NS&paymentType=excess&mediumCode=app"
        let newUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        if let url = URL(string: newUrl){
            var request = URLRequest(url: url)
            for (key, value) in RemoteDataSource.apiGatewayHeadersWithToken() {
                request.setValue(value, forHTTPHeaderField: key)
            }
            webView.load(request)
        }
    }
    
    // MARK:- Action Methods
    @objc override func handleNavBackButton() {
        showAlert(message: Strings.PaymentWebViewScene.warningCancel, primaryButtonTitle: Strings.Global.no, secondaryButtonTitle: Strings.Global.yes, primaryAction: {
        }, secondaryAction: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("URL Finished: \(webView.url?.absoluteString ?? "")")
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let urlStr = webView.url?.absoluteString.lowercased() {
            if urlStr.contains(Constants.PaymentURLs.paymentSuccess) {
                //This is the shit code i have wrote because of lack of time, I made this trick to start loading previous data 1 Second ago.
                DispatchQueue.main.async {
                    UserDefaults.standard.set(true, forKey: "ExcessPayment_" + self.srId!)
                    self.delegate?.paymentSuccessful()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        if !self.isVCPopped {
                            self.isVCPopped = true
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            } else if urlStr.contains(Constants.PaymentURLs.paymentFailure) {
                UserDefaults.standard.set(false, forKey: "ExcessPayment_" + self.srId!)
                showAlert(message: Strings.PaymentWebViewScene.paymentFailed)
            }
        }
    }
}


// MARK:- Display Logic Conformance
extension ExcessPaymentWebViewVC: ExcessPaymentWebViewDisplayLogic {
    func errorReceived(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, primaryAction: {
            self.interactor?.getPaymentUrl(srID: self.srId!, adviceID: self.adviceID!)
        }) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func urlReceived(response: ExcessPaymentResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    
    }
}

// MARK:- Configuration Logic
extension ExcessPaymentWebViewVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = ExcessPaymentWebViewInteractor()
        let presenter = ExcessPaymentWebViewPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
