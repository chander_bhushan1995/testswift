//
//  ExcessPaymentDeeplinkVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 3/27/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

class ExcessPaymentDeeplinkVC: SecureWebViewVC {
    
    private var isVCPopped = false
    
    // MARK:- private methods
    override func initializeView() {
        title = Strings.PaymentWebViewScene.title
        handleBackButton()
        if !urlString.isEmpty {
            urlString = urlString + "&mediumCode=app"
        }
        if let url = URL(string: urlString) {
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url))
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        }
    }
    
    // MARK:- Action Methods
    @objc override func handleNavBackButton() {
        showAlert(message: Strings.PaymentWebViewScene.warningCancel, primaryButtonTitle: Strings.Global.no, secondaryButtonTitle: Strings.Global.yes, primaryAction: {
        }, secondaryAction: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("URL Finished: \(webView.url?.absoluteString ?? "")")
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let urlStr = webView.url?.absoluteString.lowercased() {
            if urlStr.contains(Constants.PaymentURLs.paymentSuccess) {
                DispatchQueue.main.async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        if !self.isVCPopped {
                            self.isVCPopped = true
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            } else if urlStr.contains(Constants.PaymentURLs.paymentFailure) {
                showAlert(message: Strings.PaymentWebViewScene.paymentFailed)
            }
        }
    }
}
