//
//  ExcessPaymentWebViewInteractor.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 3/27/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ExcessPaymentWebViewPresentationLogic {
    func responseRecieved(response: ExcessPaymentResponseDTO?, error: Error?)
}

class ExcessPaymentWebViewInteractor: BaseInteractor, ExcessPaymentWebViewBusinessLogic {
    var presenter: ExcessPaymentWebViewPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getPaymentUrl(srID: String,adviceID:String) {
        let excessPaymentRequest = ExcessPaymentRequestDTO()
        excessPaymentRequest.srId = srID
        excessPaymentRequest.adviceId = adviceID
        ExcessPaymentRequestUseCase.service(requestDTO: excessPaymentRequest) { (usecase, response, error) in
            self.presenter?.responseRecieved(response: response, error: error)
        }
        
    }
}
