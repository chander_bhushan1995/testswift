//
//  ExcessPaymentWebViewPresenter.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 3/27/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ExcessPaymentWebViewDisplayLogic: class {
    func errorReceived(error: String)
    func urlReceived(response: ExcessPaymentResponseDTO)
}

class ExcessPaymentWebViewPresenter: BasePresenter, ExcessPaymentWebViewPresentationLogic {
    weak var viewController: ExcessPaymentWebViewDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func responseRecieved(response: ExcessPaymentResponseDTO?, error: Error?) {
        
        if let error = error {
            viewController?.errorReceived(error: error.localizedDescription)
        } else if response?.status?.lowercased() == Constants.ResponseConstants.success  || response?.status?.lowercased() == Constants.ResponseConstants.redirect {
            viewController?.urlReceived(response: response!)
        } else {
            viewController?.errorReceived(error: Strings.Global.somethingWrong)
        }
    }
}
