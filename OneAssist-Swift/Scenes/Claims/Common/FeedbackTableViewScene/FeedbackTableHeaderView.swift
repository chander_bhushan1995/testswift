//
//  FeedbackTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol FeedbackTableHeaderViewDelegate: class {
    func feedbackTableHeaderView(_ cell: FeedbackTableHeaderView, isSelected: Bool)
}

class FeedbackTableHeaderView: UIView {

    @IBOutlet weak var feedbackLabel: UILabel!
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    
    weak var delegate: FeedbackTableHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        seperatorView.backgroundColor = UIColor.dlsBorder
        seperatorView.isHidden = true
    }
    
    func setFeedback(_ feedback: String, isSelected: Bool = false) {
        feedbackLabel.text = feedback
        selectionButton.isSelected = isSelected
        
        feedbackLabel.textColor = isSelected ? UIColor.charcoalGrey : UIColor.gray
    }
    
    @IBAction func clickedBtnSelect(_ sender: UIButton) {
        selectionButton.isSelected = !selectionButton.isSelected
        delegate?.feedbackTableHeaderView(self, isSelected: selectionButton.isSelected)
        
        feedbackLabel.textColor = selectionButton.isSelected ? UIColor.charcoalGrey : UIColor.gray
    }
}
