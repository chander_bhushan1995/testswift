//
//  FeedbackTableViewPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/15/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol FeedbackTableViewDisplayLogic: class {
    func displayFeedbacks(datasource: TableViewDataSource<Row, FeedbackTableViewSection>)
    func displayError(error: String)
}

class FeedbackTableViewPresenter: BasePresenter, FeedbackTableViewPresentationLogic {
    weak var viewController: FeedbackTableViewDisplayLogic?
    
    func feedbackListReceived(response: WHCFeedbackResponseDTO?, error: Error?) {
        if let feedbackList = response?.data {
            var sections: [FeedbackTableViewSection] = [FeedbackTableViewSection]()
            for feedback in feedbackList {
                let section = FeedbackTableViewSection()
                section.feedbackId = feedback.valueId?.stringValue
                section.title = feedback.value
                
                sections.append(section)
            }
            
            let dataSource = TableViewDataSource<Row, FeedbackTableViewSection>()
            dataSource.tableSections = sections
            
            self.viewController?.displayFeedbacks(datasource: dataSource)
        } else {
            self.viewController?.displayError(error: error?.localizedDescription ?? "")
        }
    }
}
