//
//  FeedbackTableViewInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/15/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol FeedbackTableViewPresentationLogic {
    func feedbackListReceived(response: WHCFeedbackResponseDTO?, error: Error?)
}

class FeedbackTableViewInteractor: FeedbackTableViewBusinessLogic {
    
    var presenter: FeedbackTableViewPresentationLogic?
    
    func getFeedbacks(for rating: Int, ratingType: Constants.RatingType?) {
        let request = WHCFeedbackRequestDTO(rating.description, ratingType: ratingType)
        
        let _ = WHCFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.feedbackListReceived(response: response, error: error)
        }
    }
}
