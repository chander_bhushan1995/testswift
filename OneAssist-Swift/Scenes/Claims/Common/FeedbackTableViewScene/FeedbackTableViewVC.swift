//
//  FeedbackTableViewVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol FeedbackTableViewBusinessLogic {
    func getFeedbacks(for rating: Int, ratingType: Constants.RatingType?)
}

protocol FeedbackTableViewVCDelegate: class {
    func clickedSubmitRating(_ reasons: [String])
}

class FeedbackTableViewVC: BaseVC, HideNavigationProtocol {

    @IBOutlet weak var tableView: AutoresizingTableView!
    @IBOutlet weak var sendFeedbackButton: OAPrimaryButton!
    @IBOutlet weak var crossButton: UIButton!
    
    fileprivate var interactor: FeedbackTableViewBusinessLogic?
    var isCloseButtonHidden = false
    var rating: Int!
    var claimType: Constants.Services?
    weak var delegate: FeedbackTableViewVCDelegate?
    fileprivate var dataSource: TableViewDataSource<Row, FeedbackTableViewSection>?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         EventTracking.shared.addScreenTracking(with: .SR_Negative_Feedback)
    }
    
    func refreshFeedbacks() {
        dataSource = nil
        tableView.reloadData()
        
        initializeView()
    }
    
    private func initializeView() {
        
        checkButtonEnabled()
        if isCloseButtonHidden {
            hideCrossButton()
        }
        tableView.estimatedSectionHeaderHeight = CGFloat(50)
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        Utilities.addLoader(onView: view, message: "Please wait", count: &loaderCount, isInteractionEnabled: true)
        
        var ratingType: Constants.RatingType?
        if let claimType = claimType ?? CacheManager.shared.srType {
            let category = claimType.getCategoryType()
            switch category {
            case Constants.Category.personalElectronics:
                ratingType = .pe
            case Constants.Category.homeAppliances:
                ratingType = .ha
            default:
                ratingType = .sr
            }
        }
        interactor?.getFeedbacks(for: rating, ratingType: ratingType)
    }
    
    fileprivate func checkButtonEnabled() {
        
        if let sections = dataSource?.tableSections {
            for section in sections {
                if section.isSelected {
                    sendFeedbackButton.isEnabled = true
                    return
                }
            }
        }
        
        sendFeedbackButton.isEnabled = false
    }
    
    private func hideCrossButton() {
        crossButton.isHidden = true
        crossButton.setZero(for: [.height, .bottom])
    }
    
    @IBAction func clickedBtnClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnSendFeedback(_ sender: Any) {
          EventTracking.shared.eventTracking(name: .ratingFeedbackTapped)
        var reasons = [String]()
        var reasonText = [String]()
        if let sections = dataSource?.tableSections {
            for section in sections {
                if section.isSelected {
                    reasons.append(section.feedbackId ?? "")
                    reasonText.append(section.title ?? "")
                }
            }
        }
        
        let allReasons = reasonText.joined(separator: ", ")
        var location = ""
        if let srType = claimType ?? CacheManager.shared.srType {
            if srType == .extendedWarranty {
                location = "Home EW Service"
            } else if srType.getCategoryType() == Constants.Category.homeAppliances {
                location = "HomeAssist Service"
            } else {
                location = ((CacheManager.shared.eventProductName != nil) ? CacheManager.shared.eventProductName! : "Mobile") + " Service"
            }
            
        }
        EventTracking.shared.eventTracking(name: .ratingfeedback, [.feedback: allReasons, .location: location])
        delegate?.clickedSubmitRating(reasons)
    }
}

// MARK:- Configuration Logic
extension FeedbackTableViewVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = FeedbackTableViewInteractor()
        let presenter = FeedbackTableViewPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension FeedbackTableViewVC: FeedbackTableViewDisplayLogic {
    func displayFeedbacks(datasource: TableViewDataSource<Row, FeedbackTableViewSection>) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        dataSource = datasource
        tableView.reloadData()
    }
    
    func displayError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

extension FeedbackTableViewVC: FeedbackTableHeaderViewDelegate {
    func feedbackTableHeaderView(_ cell: FeedbackTableHeaderView, isSelected: Bool) {
        let section = cell.tag
        dataSource?.sectionModel(for: section).isSelected = isSelected
        checkButtonEnabled()
    }
}

extension FeedbackTableViewVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.numberOfRows(in: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

extension FeedbackTableViewVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = Bundle.main.loadNibNamed(Constants.CellIdentifiers.WHCService.feedbackTableHeaderView, owner: nil, options: nil)?.first as? FeedbackTableHeaderView
        
        let model = dataSource?.sectionModel(for: section)
        view?.tag = section
        view?.delegate = self
        view?.setFeedback(model?.title ?? "", isSelected: model?.isSelected ?? false)
        return view
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UITableView.automaticDimension
//    }
}

class FeedbackTableViewSection: TableViewSection<Row> {
    var feedbackId: String?
}
