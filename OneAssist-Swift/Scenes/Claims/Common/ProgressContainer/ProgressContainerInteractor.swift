//
//  ProgressContainerInteractor.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 04/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol ProgressContainerPresentationLogic {
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?)
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?)
    func presentAvailableServiceTypes(category: Constants.Services)
    func presentAssetList(serviceType: Constants.Services, assetID:[String]?)
}

class ProgressContainerInteractor: ProgressContainerBusinessLogic {
    
    var presenter: ProgressContainerPresentationLogic?

    func createServiceRequest(for type: Constants.Services) {
        
        var request = CreateServiceRequestPlanRequestDTO()
        
        switch type {
        case .peADLD:
            // SR type PE_ADLD
            let requestADLD = CreatePEADLDServiceRequestPlanRequestDTO()
            requestADLD.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEADLDDamageDetail()
                dtl.damagePart = ClaimHomeServe.shared.damagePartName
                dtl.damageType = ClaimHomeServe.shared.damageType
                dtl.damagePlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestADLD
            
        case .peExtendedWarranty:
            // SR type PE EW
            let requestEW = CreatePEEWServiceRequestPlanRequestDTO()
            requestEW.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEEWBreakDownDetail()
                dtl.breakDownPart = ClaimHomeServe.shared.damagePartName
                dtl.breakDownType = ClaimHomeServe.shared.damageType
                dtl.breakDownPlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestEW
            
        case .peTheft:
            // SR type PE Theft
            let requestTheft = CreatePETheftServiceRequestPlanRequestDTO()
            requestTheft.incidentDetail = {
                let dtl = PETheftIncidentDetail()
                dtl.simBlockRefNo = ClaimHomeServe.shared.blockReferenceNo
                dtl.simBlockReqDate = ClaimHomeServe.shared.blockReferenceDate
                return dtl
            }()
            
            request = requestTheft
            
        case .fire:
            request.workFlowValue = "2"
            
        default: break
        }
        
        if type.isHAClaimType() {
            if let flag = ClaimHomeServe.shared.supportedRequestTypes?.filter({ $0.service == type }).first?.isInsuranceBacked, !flag {
                
                if type == Constants.Services.breakdown {
                    request.insuranceBacked = "N"
                }
                
                if let pc = ClaimHomeServe.shared.productCode {
                    let product = Product(dictionary: [:])
                    product.productCode = pc
                    request.assets = [product]
                }
            }
            
            if let date = ClaimHomeServe.shared.scheduledDate, let slot = ClaimHomeServe.shared.scheduledTimeslot {
                request.scheduleSlotStartDateTime = date.string(with: .slotDateFormat) + " " + slot.startTime! + ":00" //
                request.scheduleSlotEndDateTime = date.string(with: .slotDateFormat) + " " + slot.endTime! + ":00" //
            }
            
            if let issueId = ClaimHomeServe.shared.issueId {
                let issue = IssueReportedByCustomer(dictionary: [:])
                issue.issueId = issueId.stringValue //
                issue.issueDescription = ClaimHomeServe.shared.issueDescription //
                
                request.issueReportedByCustomer = [issue]
            }
        }
        
        let currentUser = UserCoreDataStore.currentUser
        request.requestDescription = ClaimHomeServe.shared.incidentDescription
        request.placeOfIncident = ClaimHomeServe.shared.address
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen
        request.serviceRequestType = type.rawValue
        request.referenceNo = ClaimHomeServe.shared.membershipId
        request.createdBy = currentUser?.cusId
        request.insurancePartnerCode = ClaimHomeServe.shared.insurancePartnerCode
        request.customerId = currentUser?.cusId
        
        if let assetIds = ClaimHomeServe.shared.assetIds, !assetIds.isEmpty {
            request.refSecondaryTrackingNo = assetIds.joined(separator: ",")
        }
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName
        addressDetails.countryCode = "IND"
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com"
        addressDetails.mobileNo = CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber
        addressDetails.pincode = ClaimHomeServe.shared.pinCode
        addressDetails.addressLine1 = ClaimHomeServe.shared.address
        
        addressDetails.district = ""
        addressDetails.landmark = ""
        
        request.serviceRequestAddressDetails = addressDetails
        request.additionalDetails = ClaimHomeServe.shared.additionalDetails?.createRequestParameters()
        request.draftId = ClaimHomeServe.shared.draftSRId
        
        CreateServiceRequestPlanRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
    }
    
    func updateServiceRequest(for type: Constants.Services) {
        
        let currentUser = UserCoreDataStore.currentUser
        var request = UpdateServiceRequestDTO()
        
        switch type {
        case .peADLD:
            
            // SR type PE ADLD
            let requestADLD = UpdatePEADLDServiceRequestPlanRequestDTO()
            
            // prepare incident detail model
            requestADLD.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEADLDDamageDetail()
                dtl.damagePart = ClaimHomeServe.shared.damagePartName
                dtl.damageType = ClaimHomeServe.shared.damageType
                dtl.damagePlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestADLD
            
        case .peExtendedWarranty:
            
            // SR type PE EW
            let requestEW = UpdatePEEWServiceRequestPlanRequestDTO()
            
            // prepare incident detail model
            requestEW.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEEWBreakDownDetail()
                dtl.breakDownPart = ClaimHomeServe.shared.damagePartName
                dtl.breakDownType = ClaimHomeServe.shared.damageType
                dtl.breakDownPlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestEW
            
        case .peTheft:
            
            // SR type PE Theft
            let requestTheft = UpdatePETheftServiceRequestPlanRequestDTO()
            requestTheft.incidentDetail = {
                let dtl = PETheftIncidentDetail()
                dtl.simBlockRefNo = ClaimHomeServe.shared.blockReferenceNo
                dtl.simBlockReqDate = ClaimHomeServe.shared.blockReferenceDate
                return dtl
            }()
            
            request = requestTheft
        case .extendedWarranty:
            request.workFlowValue = "1"
        case .fire:
            request.workFlowValue = "2"
        default: break
        }
        
        request.requestDescription = ClaimHomeServe.shared.incidentDescription
        request.placeOfIncident = ClaimHomeServe.shared.address
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen
        request.referenceNo = ClaimHomeServe.shared.membershipId
        request.serviceRequestId = ClaimHomeServe.shared.serviceRequestID
        request.modifiedBy = currentUser?.cusId
        request.refSecondaryTrackingNo = ClaimHomeServe.shared.assetIds?.joined(separator: ",")
        
        if let date = ClaimHomeServe.shared.scheduledDate, let slot = ClaimHomeServe.shared.scheduledTimeslot {
            request.scheduleSlotStartDateTime = date.string(with: .slotDateFormat) + " " + slot.startTime! + ":00"
            request.scheduleSlotEndDateTime = date.string(with: .slotDateFormat) + " " + slot.endTime! + ":00"
        }
        
        if let issueId = ClaimHomeServe.shared.issueId {
            let issue = IssueReportedByCustomer(dictionary: [:])
            issue.issueId = issueId.stringValue
            issue.issueDescription = ClaimHomeServe.shared.issueDescription
            request.issueReportedByCustomer = [issue]
        }
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName
        addressDetails.countryCode = "IND"
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com"
        addressDetails.mobileNo = CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber
        addressDetails.pincode = ClaimHomeServe.shared.pinCode
        addressDetails.addressLine1 = ClaimHomeServe.shared.address
                
        request.serviceRequestAddressDetails = addressDetails
        request.additionalDetails = ClaimHomeServe.shared.additionalDetails?.createRequestParameters()
        
        UpdateServiceRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
    }
    
    func getAvailableServiceTypes(category: Constants.Services)  {
        presenter?.presentAvailableServiceTypes(category: category)
    }
    
    func getAssetList(serviceType: Constants.Services, assetID:[String]?) {
        self.presenter?.presentAssetList(serviceType: serviceType, assetID: assetID)
    }
}
