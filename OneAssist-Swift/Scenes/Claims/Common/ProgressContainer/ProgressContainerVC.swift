//
//  ProgressContainerVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 04/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol ProgressContainerBusinessLogic {
    func createServiceRequest(for type: Constants.Services)
    func updateServiceRequest(for type: Constants.Services)
    func getAvailableServiceTypes(category: Constants.Services)
    func getAssetList(serviceType: Constants.Services, assetID:[String]?)
}

class ProgressContainerVC: BaseVC {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var pagingView: UIView!
    
    var interactor: ProgressContainerBusinessLogic?
    var animating : Bool = false
    var currentIndex:Int = 0
    var raisedFromCrm = false
    var dictCategory : [Constants.Services: [AnyClass]]!
    var pageViewController: UIPageViewController!
    var arrViewController :[UIViewController] = []
    var category: Constants.Services!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    fileprivate func getStartingViewController()->[UIViewController] {
        let vc = WhatHappenedVC()
        vc.currentPage = 0
        vc.delegate = self
        vc.category = category
        if category == .extendedWarranty && ClaimHomeServe.shared.isCombo == false {
            vc.productCode = ClaimHomeServe.shared.productCode!
            vc.productVariantId = ClaimHomeServe.shared.productVariantId
            if raisedFromCrm {
                vc.preSelectedIssueId = ClaimHomeServe.shared.issueId
            }
        }
        return [vc]
    }
    
    func initialiseView() {
        initializePagingController()
        self.lblProgress.font = DLSFont.supportingText.regular
        self.dictCategory = self.initialiseCategoryDictionaryForViewController()
        currentIndex = 0
        if category == .extendedWarranty && ClaimHomeServe.shared.isCombo == false {
            interactor?.getAvailableServiceTypes(category: category)
        }else if ClaimHomeServe.shared.isCombo == true {
            self.getApplainceNotWorkingData()
        }
        else {
            setPageControllerData()
        }
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style:.done , target: self, action: #selector(self.clickBackButton))
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = Strings.ServiceRequest.Titles.raiseARequest
    }
    
    func setPageControllerData() {
        arrViewController = getStartingViewController() // initial view controller for array
        pageViewController.setViewControllers([arrViewController[0]], direction: .forward, animated: false, completion: nil)
        self.setProgress(index: 0)
    }
    
    fileprivate func getApplainceNotWorkingData() {
        interactor?.getAssetList(serviceType: category, assetID: ClaimHomeServe.shared.assetIds)
    }
    
    func initializePagingController() {
        currentIndex = 0
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.frame = pagingView.bounds
        pageViewController.delegate = self
        addChild(pageViewController)
        pagingView.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        
    }
    
    @objc func clickBackButton() {
        if currentIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            currentIndex = currentIndex - 1
            setProgress(index : currentIndex)
            let nextVC = arrViewController[currentIndex]
            self.pageViewController.setViewControllers([nextVC], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func setProgress(index:Int) {
        if index == 0 {
            self.setProgress(index: 0, totalCount: nil)
        } else {
            let arrClass = getArrayViewControllersClass(category: category)
            let totalVC =  arrClass.count
            self.setProgress(index: index, totalCount: totalVC)
        }
    }
    
    func setProgressLastStep() {
        let arrClass = getArrayViewControllersClass(category: category)
        let totalVC = arrClass.count
        self.setProgress(index: totalVC, totalCount: totalVC)
    }
    
    func setProgress(index: Int , totalCount: Int?)  {
        if index  == 0 {
            self.progressView.setProgress(0.15, animated: true)
        } else {
            self.progressView.setProgress(Float(Double(index+1))/Float(totalCount! + 2), animated: true)
            
        }
        
        self.lblProgress.text = getProgressLabelText(index : index, totalCount: totalCount)
    }
    
    func getProgressLabelText(index : Int , totalCount : Int?) ->String
    {
        if index == 0 {
            return "Step 1"
        }
        if index == totalCount!-1 && totalCount! >= 4 {
            return "Only 2 steps left"
        }
        if index == totalCount && totalCount! >= 1 {
            
            return "Great! You're on the last step!"
        }
        return "Step " + "\(index + 1) " + "of " + "\(totalCount! + 1)"
    }
    
    func isInsuranceBacked() -> Bool {
        return !Utilities.isSOPServiceAvailable(from: ClaimHomeServe.shared.totalServices)
    }
    
    func getArrayViewControllersClass(category: Constants.Services) -> [AnyClass] {
        var arrClass:[AnyClass] = []
        switch category {
            
        // HA Plans:
        case .breakdown:
            arrClass = isInsuranceBacked() ? [AppliancesNotWorkingVC.self, WhatHappenedVC.self, WhenHappenVC.self, ScheduleVisitVC.self] : [AppliancesNotWorkingVC.self, WhatHappenedVC.self, ScheduleVisitVC.self]
        case .accidentalDamage:
            arrClass = [AppliancesNotWorkingVC.self, WhatHappenedVC.self, BrieflyDescVC.self, WhenHappenVC.self]
        case .fire:
            arrClass = [AppliancesNotWorkingVC.self, BrieflyDescVC.self, WhenHappenVC.self]
        case .buglary:
            arrClass = [AppliancesNotWorkingVC.self, BrieflyDescVC.self, WhenHappenVC.self]
        case .pms:
            arrClass = [AppliancesNotWorkingVC.self, ScheduleVisitVC.self]
            
        // PE Plans:
        case .peADLD:
            arrClass = [HowAndWhereHappenedVC.self, WhenHappenVC.self, PartDamageVC.self]
        case .peExtendedWarranty:
            arrClass = [HowAndWhereHappenedVC.self, WhenHappenVC.self]
        case .peTheft:
            arrClass = [HowAndWhereHappenedVC.self, WhenHappenVC.self]
            
        // EW Plans:
        case .extendedWarranty:
            if ClaimHomeServe.shared.isCombo {
                //arrClass = [AppliancesNotWorkingVC.self, WhatHappenedVC.self, WhenHappenVC.self, GIfterScenarioVC.self]
                arrClass = [WhatHappenedVC.self, WhenHappenVC.self, GIfterScenarioVC.self]
            }else {
                arrClass = [ WhenHappenVC.self, GIfterScenarioVC.self]
            }
           //
            
            
        default:
            arrClass = []
        }
        return arrClass
    }
    
    func getViewController(forClass :AnyClass , category: Constants.Services) -> UIViewController? {
        var vc : BaseVC?
        
        if forClass is AppliancesNotWorkingVC.Type {
            let baseVC = (forClass as! AppliancesNotWorkingVC.Type).init()
            baseVC.category = category
            if  raisedFromCrm {
                baseVC.assestID = ClaimHomeServe.shared.assetIds
            }
            baseVC.membershipId = ClaimHomeServe.shared.membershipId
            
            baseVC.delegate = self
            vc = baseVC
        } else if forClass is BrieflyDescVC.Type{
            let baseVC = BrieflyDescVC()
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.descriptionText = ClaimHomeServe.shared.incidentDescription
            }
            vc = baseVC
        } else if forClass is WhenHappenVC.Type {
            let baseVC = (forClass as! WhenHappenVC.Type).init()
            baseVC.currentPage = currentIndex
            baseVC.category = category
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.prefetchedDateSelected = ClaimHomeServe.shared.dateWhenHappen
                baseVC.createdOnDate = ClaimHomeServe.shared.createdOn
            }
            vc = baseVC
        } else if forClass is GIfterScenarioVC.Type {
            let baseVC = (forClass as! GIfterScenarioVC.Type).init()
            baseVC.currentPage = currentIndex
            baseVC.delegate = self
            vc = baseVC
        }else if forClass is WhatHappenedVC.Type{
            let baseVC = WhatHappenedVC()
            baseVC.category = category
            baseVC.productCode = ClaimHomeServe.shared.productCode!
            baseVC.productVariantId = ClaimHomeServe.shared.productVariantId
            if raisedFromCrm {
                baseVC.preSelectedIssueId = ClaimHomeServe.shared.issueId
            }
            baseVC.delegate = self
            vc = baseVC
        } else if forClass is ScheduleVisitVC.Type {
            let baseVC = ScheduleVisitVC()
            if raisedFromCrm {
                baseVC.selectedSlot = ClaimHomeServe.shared.scheduledTimeslot
                if let date = ClaimHomeServe.shared.scheduledDate {
                    let model = ScheduleVisitCollectionViewModel()
                    model.date = date
                    baseVC.selectedItem = model
                }
            }
            baseVC.serviceType = category.rawValue
            baseVC.serviceRequestSourceType = Strings.SODConstants.nonSOD
            baseVC.memId = ClaimHomeServe.shared.membershipId
            baseVC.productCode = ClaimHomeServe.shared.productCode
            if let assestId = ClaimHomeServe.shared.assetIds, assestId.count>0 {
                baseVC.assetId = assestId.joined(separator: ",")
            }
        //    baseVC.bpCode = ClaimHomeServe.shared.bpCode
        //    baseVC.buCode = ClaimHomeServe.shared.buCode
            baseVC.getAPiCalledFirstTime = false
            baseVC.addressDetail = AddressDetail(pincode: ClaimHomeServe.shared.pinCode)
            baseVC.scheduleVisitDelegate = self
            vc = baseVC
        } else if forClass is HowAndWhereHappenedVC.Type{
            
            // What & where happened screen
            let baseVC = HowAndWhereHappenedVC()
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.descriptionText = ClaimHomeServe.shared.incidentDescription
                baseVC.prefetchIncidentPlace = ClaimHomeServe.shared.placeOfIncident
            }
            vc = baseVC
        } else if  forClass is PartDamageVC.Type {
            
            // Damage part class
            let baseVC = (forClass as! PartDamageVC.Type).init()
            baseVC.delegate = self
            baseVC.productCode = ClaimHomeServe.shared.productCode!
            baseVC.category = category
            if raisedFromCrm {
                baseVC.prefetchedDamagePart = ClaimHomeServe.shared.damagePartName
                baseVC.prefetchedConditions = ClaimHomeServe.shared.damageConditions
            }
            vc = baseVC
        } else if  forClass is BlockReferenceVC.Type {
            
            // Sim block screen
            // Not used in any case
            let baseVC = (forClass as! BlockReferenceVC.Type).init()
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.prefetchedRefNo = ClaimHomeServe.shared.blockReferenceNo
                baseVC.prefetchedDateSelected = ClaimHomeServe.shared.blockReferenceDate
            }
            vc = baseVC
        }
        
        return vc
    }
    
    
    func initialiseCategoryDictionaryForViewController()->[Constants.Services: [AnyClass]] {
        
        var dictCategory = [Constants.Services: [AnyClass]]()
        
        // HA Plans:
        dictCategory[.fire] = self.getArrayViewControllersClass(category: .fire)
        dictCategory[.accidentalDamage] = self.getArrayViewControllersClass(category: .accidentalDamage)
        dictCategory[.breakdown] = self.getArrayViewControllersClass(category: .breakdown)
        dictCategory[.pms] = self.getArrayViewControllersClass(category: .pms)
        dictCategory[.buglary] = self.getArrayViewControllersClass(category: .buglary)
        
        // PE Plans:
        dictCategory[.peADLD] = self.getArrayViewControllersClass(category: .peADLD)
        dictCategory[.peExtendedWarranty] = self.getArrayViewControllersClass(category: .peExtendedWarranty)
        dictCategory[.peTheft] = self.getArrayViewControllersClass(category: .peTheft)
        
        //EW Plans:
        dictCategory[.extendedWarranty ] = self.getArrayViewControllersClass(category:.extendedWarranty)
        
        return dictCategory
    }
    
    func getClass(category : Constants.Services, dictCategory:[Constants.Services:[AnyClass]],index : Int) -> AnyClass? {
        let newIndex = index - 1
        let arrClass = dictCategory[category]!
        if newIndex <= arrClass.count - 1 {
            return arrClass[newIndex]
        }
        return nil;
    }
    
    func moveToNext(controllerToLoad:((_ controller : UIViewController) -> Void)?, onCompletion:((_ controller : UIViewController) -> Void)? = nil) {
        
        if animating == true {
            return
        }
        animating = true
        let nextIndexToLoad = self.currentIndex + 1
        if nextIndexToLoad >= arrViewController.count {
            guard let classToLoad = getClass(category: category, dictCategory: self.dictCategory, index: nextIndexToLoad) else {
                return
            }
            let nextVC = getViewController(forClass: classToLoad , category: category)!
            if let closure = controllerToLoad {
                closure(nextVC)
            }
            setProgress(index: nextIndexToLoad)
            
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true){ _ in
                self.animating = false
                onCompletion?(nextVC)
            }
            arrViewController.append(nextVC)
        } else {
            let nextVC = arrViewController[nextIndexToLoad]
            if let closure = controllerToLoad {
                closure(nextVC)
            }
            setProgress(index: nextIndexToLoad)
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true){ _ in
                self.animating = false
                onCompletion?(nextVC)
                
            }
        }
        self.currentIndex = nextIndexToLoad
    }
}

// MARK:- Configuration Logic
extension ProgressContainerVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = ProgressContainerInteractor()
        let presenter = ProgressContainerPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension ProgressContainerVC: ProgressContainerDisplayLogic {
    func displayApplianceLogin(model: AppliancesNotWorkingModel?) {
        CacheManager.shared.eventProductName = model?.name
        ClaimHomeServe.shared.productCode = model?.productCode
        ClaimHomeServe.shared.productName = model?.name
        ClaimHomeServe.shared.productVariantId = model?.productVariantId
        ClaimHomeServe.shared.assetIds = [model?.assetId ?? ""]
        
        setPageControllerData()
    }
    
    func displayError(_ error: String) {
        self.setProgressLastStep()
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {[weak self] in
            self?.raiseServiceRequest(category: (self?.category)!)
        }) {}
    }
    
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?) {
        
        EventTracking.shared.eventTracking(name: .srInProgress, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?) {
        
        EventTracking.shared.eventTracking(name: .srInProgress, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? "", .srId: serviceDetails?.data?.serviceRequestId ?? ""])
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if let cat = category {
            switch cat {
            case .breakdown, .pms:
                let vc = ClaimSuccessVC()
                let model = ClaimSuccessViewModel()
                model.scheduleSlotStartDateTime = serviceDetails?.data?.scheduleSlotStartDateTime ?? ""
                model.scheduleSlotEndDateTime = serviceDetails?.data?.scheduleSlotEndDateTime ?? ""
                model.successType = .srSuccessWithVisit
                model.srId = serviceDetails?.data?.serviceRequestId?.description
                model.crmTrackingNumber = serviceDetails?.data?.refPrimaryTrackingNo
                vc.successModel = model
                vc.delegate = self
                presentInFullScreen(vc, animated: true, completion: nil)
                
            case .fire:
                let vc = ClaimSuccessVC()
                let model = ClaimSuccessViewModel()
                model.successType = .srSuccess
                model.srId = serviceDetails?.data?.serviceRequestId?.description
                model.crmTrackingNumber = serviceDetails?.data?.refPrimaryTrackingNo
                vc.successModel = model
                vc.delegate = self
                presentInFullScreen(vc, animated: true, completion: nil)
                
            case .extendedWarranty:
                let vc = ClaimSuccessVC()
                let model = ClaimSuccessViewModel()
                model.successType = .srSuccessHAEW
                model.srId = serviceDetails?.data?.serviceRequestId?.description
                model.crmTrackingNumber = serviceDetails?.data?.refPrimaryTrackingNo
                vc.successModel = model
                vc.delegate = self
                presentInFullScreen(vc, animated: true, completion: nil)
                
            default:
                // All PE & EW cases
                NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    func displayWhatHappenData(model: WhatHappenSection?) {
        if let selectedOption = model {
            let selectedCategory = selectedOption.type ?? category
            if selectedCategory != category {
                self.category = selectedCategory
            }
            self.setupWhatHappendData(model: selectedOption, category: selectedCategory ?? category) // we are passing selected service type
        }
        self.getApplainceNotWorkingData()
    }
    
    private func selectedModel(dataSource:TableViewDataSource<AppliancesNotWorkingModel,TableViewSection<AppliancesNotWorkingModel>>) -> [AppliancesNotWorkingModel]{
        var arrModel = [AppliancesNotWorkingModel]()
        for section in dataSource.tableSections {
            for model in section.rows {
                if model.isSelected == true {
                    arrModel.append(model)
                }
            }
        }
        return arrModel
    }
}

// MARK: UIPageViewControllerDelegate

extension ProgressContainerVC: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            return
        }
        
        setProgress(index:currentIndex)
    }
}

// MARK: WhatHappenedVCDelegate

extension ProgressContainerVC: WhatHappenedVCDelegate {
    func whatHappenedButtonTapped(category: Constants.Services) {
        if self.category != category {
//            if self.category != nil {
//                ClaimHomeServe.shared.assetIds = nil
//            }
            self.category = category
            if arrViewController.count > 1 {
                arrViewController.removeSubrange(ClosedRange(uncheckedBounds: (lower: 1, upper: arrViewController.count - 1)))
            }
        }
    }
    
    func clickSubmitButton(model: WhatHappenSection, category: Constants.Services) {
        setupWhatHappendData(model: model, category: category)
        
        moveToNext(controllerToLoad: {(controller : UIViewController) in
             if let scheduleVisitVC = controller as? ScheduleVisitVC {
                scheduleVisitVC.productCode = ClaimHomeServe.shared.productCode
                if let assestId = ClaimHomeServe.shared.assetIds, assestId.count>0 {
                    scheduleVisitVC.assetId = assestId.joined(separator: ",")
                }else {
                    scheduleVisitVC.assetId = nil
                }
                scheduleVisitVC.getAPiCalledFirstTime = true
            }
        } , onCompletion: {(controller : UIViewController)in
            if !(controller is ScheduleVisitVC) {
                controller.viewWillAppear(true)
            }
        })
    }
    
    func setupWhatHappendData(model: WhatHappenSection, category: Constants.Services) {
        if let _ = model.issueId {
            ClaimHomeServe.shared.issueId = model.issueId
            ClaimHomeServe.shared.issueDescription = model.title
        }
        
        if category == .breakdown || category == .accidentalDamage {
            ClaimHomeServe.shared.incidentDescription = model.subtitle
        } else if category.isPEClaimType() {
            ClaimHomeServe.shared.insurancePartnerCode = model.partnerCode
        }
    }
}

// MARK: AppliancesNotWorkingDelegate

extension ProgressContainerVC: AppliancesNotWorkingDelegate {
    func applianceNotWorkingButtonTapped(models: [AppliancesNotWorkingModel]) {
        
//        if category == .accidentalDamage ||
//            category == .breakdown ||
//            category == .pms {
                ClaimHomeServe.shared.productCode = models.first?.productCode
                ClaimHomeServe.shared.productName = models.first?.name
                ClaimHomeServe.shared.productVariantId = models.first?.productVariantId
//        }
        
        ClaimHomeServe.shared.assetIds = models.compactMap{$0.assetId}
        moveToNext(controllerToLoad: {(controller : UIViewController) in
            if let whatHappenController = controller as? WhatHappenedVC {
                whatHappenController.productCode = ClaimHomeServe.shared.productCode!
                whatHappenController.productVariantId = ClaimHomeServe.shared.productVariantId
                
            }else if let scheduleVisitVC = controller as? ScheduleVisitVC {
                scheduleVisitVC.productCode = ClaimHomeServe.shared.productCode
                if let assestId = ClaimHomeServe.shared.assetIds, assestId.count>0 {
                    scheduleVisitVC.assetId = assestId.joined(separator: ",")
                }else {
                    scheduleVisitVC.assetId = nil
                }
                scheduleVisitVC.getAPiCalledFirstTime = true
            }
        } , onCompletion: {(controller : UIViewController) in
            if !(controller is ScheduleVisitVC) {
                controller.viewWillAppear(true)
            }
        })
    }
}

// MARK:- ScheduleVisitDelegate

extension ProgressContainerVC: ScheduleVisitDelegate {
    func clickedScheduleVisit(date: Date, slot: ScheduleVisitTimeSlotViewModel, serviceReqId: String?, crmTrackingNumber: String?) {
        ClaimHomeServe.shared.scheduledDate = date
        ClaimHomeServe.shared.scheduledTimeslot = slot
        raiseServiceRequest(category: category)
    }
}

// MARK: IncidentDescriptionBaseVCDelegate

extension ProgressContainerVC : IncidentDescriptionBaseVCDelegate {
    func clickTellUsMore(descripition: String, placeOfIncident: String?) {
        ClaimHomeServe.shared.incidentDescription = descripition
        ClaimHomeServe.shared.placeOfIncident = placeOfIncident
        moveToNext(controllerToLoad: nil)
    }
}

// MARK: ClaimWhenHappenDelegate
extension ProgressContainerVC: ClaimWhenHappenDelegate {
    func clickWhenHapppen(date: String) {
        ClaimHomeServe.shared.dateWhenHappen = date
        
        let newDate = Date(string: date, formatter: .scheduleFormat) ?? Date()
        
        let dateTime = date.split(separator: " ")
        if category == .breakdown ||
            category == .peADLD ||
            category == .extendedWarranty {
            if dateTime.count == 2 {
                EventTracking.shared.eventTracking(name: .srDtSubmit, [.date: newDate, .time: dateTime[1], .subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
            }
            moveToNext(controllerToLoad: {(controller : UIViewController) in
                 if let scheduleVisitVC = controller as? ScheduleVisitVC {
                    scheduleVisitVC.productCode = ClaimHomeServe.shared.productCode
                    if let assestId = ClaimHomeServe.shared.assetIds, assestId.count>0 {
                        scheduleVisitVC.assetId = assestId.joined(separator: ",")
                    }else {
                        scheduleVisitVC.assetId = nil
                    }
                    scheduleVisitVC.getAPiCalledFirstTime = true
                }
            } , onCompletion: {(controller : UIViewController)in
                if !(controller is ScheduleVisitVC) {
                    controller.viewWillAppear(true)
                }
            })
        } else {
            if dateTime.count == 2 {
                EventTracking.shared.eventTracking(name: .SRVisitDT_Submit, [.date: newDate, .time: dateTime[1], .subcategory: ClaimHomeServe.shared.productName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
            }
            raiseServiceRequest(category: self.category)
        }
    }
    
    func raiseServiceRequest(category: Constants.Services) {
        
        self.progressView.setProgress(1.0, animated: true)
        
        if raisedFromCrm {
            Utilities.addLoader(onView: view, message: "Please wait while we are updating your service request", count: &loaderCount, isInteractionEnabled: true)
            interactor?.updateServiceRequest(for: category)
        }else {
            Utilities.addLoader(onView: view, message: "Please wait while we are creating your service request", count: &loaderCount, isInteractionEnabled: true)
            interactor?.createServiceRequest(for: category)
        }
    }
}

extension ProgressContainerVC: ClaimSuccessVCDelegate {
    func clickCrossButton() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: false)
        })
    }
    
    func clickViewServiceRequest() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: false)
        })
    }
    
}

// MARK: PartDamageVCDelegate
extension ProgressContainerVC: PartDamageVCDelegate {
    
    func selectedDamages(selectedPart: CardOptionsModel, reasons: [PartDamageViewModel]) {
        ClaimHomeServe.shared.damagePartName = selectedPart.title
        ClaimHomeServe.shared.damageConditions = reasons
        raiseServiceRequest(category: category ?? .peADLD)
    }
}

// MARK: PartDamageVCDelegate
// Not In use
extension ProgressContainerVC: BlockReferenceVCDelegate {
    
    func blockReferenceSubmitted(refNo: String, pickedTime: String) {
        ClaimHomeServe.shared.blockReferenceNo = refNo
        ClaimHomeServe.shared.blockReferenceDate = pickedTime
        raiseServiceRequest(category: category ?? .peADLD)
    }
}

//MARK: GIfterScenarioDelegate
extension ProgressContainerVC: GIfterScenarioDelegate {
    func clickOnGifterScenario(with aditionalInfo: AdditionalDetail?) {
        ClaimHomeServe.shared.additionalDetails = aditionalInfo
        raiseServiceRequest(category: self.category)
    }
}
