//
//  ProgressContainerPresenter.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 04/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

protocol ProgressContainerDisplayLogic: class {
    func displayError(_ error: String)
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?)
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?)
    func displayWhatHappenData(model: WhatHappenSection?)
    func displayApplianceLogin(model: AppliancesNotWorkingModel?)
}

class ProgressContainerPresenter: BasePresenter, ProgressContainerPresentationLogic {
    
    weak var viewController: ProgressContainerDisplayLogic?

    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayCreatedServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayUpdateServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentAvailableServiceTypes(category: Constants.Services) {
        let model = getWhatHappenDefaultData()
        viewController?.displayWhatHappenData(model: model)
    }
    
    private func getWhatHappenDefaultData() -> WhatHappenSection? {
        if let totalAvailableServices = ClaimHomeServe.shared.availability?.totalAvailableServices, totalAvailableServices.count > 0 {
            let service = totalAvailableServices.first
            let section = getSection(title: service?.serviceDescription?.first?.displayValue, rows: service?.serviceDescription?.first?.subDescription)
            section.isSelected = true
            section.isEnabled = true
            section.type = service?.service
           
            return section
        }
        
        return nil
    }
    
    private func getSection(title: String?, subtitle: String? = nil, rows: [String]?) -> WhatHappenSection {
        let section = WhatHappenSection()
        section.title = title
        section.subtitle = subtitle
        
        section.rows = rows?.compactMap({ (row) -> WhatHappenRowModel? in
            let rowStolen = WhatHappenRowModel()
            rowStolen.title = row
            rowStolen.imageName = "dot"
            return rowStolen
        }) ?? []
        
        section.isSelected = false
        return section
    }
    
    
    func presentAssetList(serviceType: Constants.Services, assetID:[String]?) {
        if let services = ClaimHomeServe.shared.totalServices, let serviceModel = services.filter({ $0.service == serviceType }).first {
            
            var totalAssets: [Asset]? = ClaimHomeServe.shared.totalAssets
            if serviceModel.isInsuranceBacked == false {
                totalAssets = serviceModel.assets
            }
            if let allAssets = totalAssets {
                let firstAsset = allAssets.first
                let assetName = "\(firstAsset?.brand ?? "") \(firstAsset?.name ?? "")"
                let model = AppliancesNotWorkingModel(name: assetName, features: nil, deviceImageName: nil)
                model.productCode = firstAsset?.prodCode
                model.productVariantId = firstAsset?.productVariantId
                if let assetId = firstAsset?.assetId?.description {
                    model.assetId = assetId
                }
                
                viewController?.displayApplianceLogin(model: model)
            }
        }
    }
}
