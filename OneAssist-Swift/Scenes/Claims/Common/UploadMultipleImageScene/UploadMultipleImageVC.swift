//
//  UploadMultipleImageVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol UploadMultipleImageVCDelegate: class {
    func clickedBtnSubmit()
    func clickedBtnBack(refreshNeeded: Bool)
}

protocol UploadMultipleImageBusinessLogic {
    func uploadClaimInvoice(srId: String, documentTypeId: String?, file: Data?, documentId: String?, index: IndexPath)
    func getDocumentImage(docId: String?, storageRefId: String?, index: IndexPath)
    func updateSR(srId: String, srType: String?, serviceDocuments: [ServiceDocument]?)
}

extension UploadMultipleImageBusinessLogic {
    func updateSR(srId: String) {
        updateSR(srId: srId, srType: "", serviceDocuments: nil)
    }
}

class UploadMultipleImageViewModel {
    var title = ""
    var headerText = ""
    var primaryActionTitle = ""
    var srId = ""
    var srType: String? = ""
    var serviceDocuments: [ServiceDocument] = []
    var attributedText: NSAttributedString?
    var crmTrackingNumber: String?
}

class UploadMultipleImageVC: UploadDamagedImageVC {
    var interactor: UploadMultipleImageBusinessLogic?
    
    weak var delegate: UploadMultipleImageVCDelegate?
    // MARK:- Object lifecycle
    var viewModel: UploadMultipleImageViewModel = UploadMultipleImageViewModel()
    var category: Constants.Services = .extendedWarranty
    
    override var headerText: NSAttributedString {
//        return "Please upload 2 images of your Refrigerator for verification purposes"
        return viewModel.attributedText ?? NSAttributedString(string: viewModel.headerText, attributes: [NSAttributedString.Key.font: DLSFont.h3.bold])

    }

    override var primaryActionText: String {
//        return "SUBMIT REQUEST"
        return viewModel.primaryActionTitle
    }
    
    init() {
        super.init(nibName: "UploadDamagedImageVC", bundle: nil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        title = viewModel.title
        
        handleBackButton()
        
        if isReupload {
            srIdLabel.text = nil
            self.srIdView.isHidden = true
        }else {
            self.srIdView.isHidden = false
            if let crmTrackingNumber = viewModel.crmTrackingNumber {
                srIdLabel.text = "Great! Your service request ID \(crmTrackingNumber) has been initiated."
            }else {
                srIdLabel.text = "Great! Your service request has been initiated."
            }
        }
      //  srIdLabel.text = isReupload ? nil : "Great! Your service request ID \(viewModel.crmTrackingNumber ?? "") has been initiated."

        viewTick.isHidden = isReupload        
        
        imageSelectedCallBack = { (image,path) in
            
            self.dataSource[path.row].uploaded = false
            self.toggleSubmitButton()
            self.addLoaderOnCollection(path: path)
            self.interactor?.uploadClaimInvoice(srId: self.viewModel.srId, documentTypeId: self.dataSource[path.row].docType , file: image?.jpegData(compressionQuality: 0.9), documentId: self.dataSource[path.row].docId, index: path)
        }
        
        submitClosure = {
            button in
            self.updateSRDocuments()
        }
        
        if isReupload {
            
            let models = viewModel.serviceDocuments.map { (service) -> UploadDamagedImageModel in
                let model = UploadDamagedImageModel()
                model.docName = service.documentName ?? "Please upload the document"
                model.docType = service.documentTypeId?.description
                model.docDescription = "Please make sure image is not blurry."
                model.docId = service.documentId
                return model
            }
            dataSource = models
           // heightOfCells = dataSource.count > 1 ?  CGSize(width: (UIScreen.main.bounds.width - 65)/2, height: 180) : CGSize(width: (UIScreen.main.bounds.width - 46), height: 180)
            collectionView.reloadData()
        }
    }
    
    override func displayDocTypeResponse(serviceDocTypes: [ServiceDocumentType]) {
        super.displayDocTypeResponse(serviceDocTypes: serviceDocTypes)
        
        var dataModels = [UploadDamagedImageModel]()
        serviceDocTypes.forEach { (docTypeModel) in
            if let docKey = docTypeModel.docKey, docKey == "GIFTER_ID_PROOF" {
                if self.isGifterPrrofRequired {
                    let model = UploadDamagedImageModel()
                    model.docName = docTypeModel.displayName ?? "Please upload the document"
                    model.docType = docTypeModel.docTypeId?.description ?? ""
                    model.docDescription = docTypeModel.displaytext ?? "Please make sure image is not blurry."
                    model.isMandatory = true
                    model.docId = viewModel.serviceDocuments.filter({$0.documentTypeId?.description == model.docType}).first?.documentId
                    dataModels.append(model)
                }
            } else {
                let model = UploadDamagedImageModel()
                model.isMandatory = docTypeModel.isMandatory?.lowercased() == "y"
                model.docName = docTypeModel.displayName ?? "Please upload the document"
                model.docName = (model.docName ?? "") + (!model.isMandatory ? " (optional)" : " ")
                model.docType = docTypeModel.docTypeId?.description ?? ""
                model.docDescription = docTypeModel.displaytext ?? "Please make sure image is not blurry."
                model.docId = viewModel.serviceDocuments.filter({$0.documentTypeId?.description == model.docType}).first?.documentId
                dataModels.append(model)
            }
        }
        
        dataSource = dataModels
        
        headerLabel.text = viewModel.headerText
        headerLabel.isHidden = !((headerLabel.text?.isEmpty) != nil)
       // heightOfCells = dataSource.count > 1 ?  CGSize(width: (UIScreen.main.bounds.width - 65)/2, height: 180) : CGSize(width: (UIScreen.main.bounds.width - 46), height: 180)
        collectionView.reloadData()
        setUploadedDocuments()
        toggleSubmitButton()
    }
    
    fileprivate func updateSRDocuments() {
        
        let subCat = Utilities.getProduct(productCode: productCode ?? "")?.subCategoryName ?? ""
        if isReupload {
            // reupload event
            EventTracking.shared.eventTracking(name: .submitClickedAfterDocReupload , nil, withGI: false)
        } else {
            EventTracking.shared.eventTracking(name: .submitDocumentsClickedAfterUploadingImage , nil, withGI: false)
            EventTracking.shared.eventTracking(name: .SRDocSubmit, [.service: category.rawValue, .subcategory: subCat])

        }
        
        // TODO: add service Documents here
        Utilities.addLoader(onView: view, message: "submitting", count: &self.loaderCount, isInteractionEnabled: false)
        interactor?.updateSR(srId: viewModel.srId, srType: viewModel.srType, serviceDocuments: nil)
//        interactor?.updateSR(srId: viewModel.srId)
    }
    
    private func setUploadedDocuments() {
        
        for (index,value) in dataSource.enumerated() {
            if value.docId != nil {
                let path = IndexPath(row: index, section: 0)
                addLoaderOnCollection(path: path)
                
                // TODO: Pass storage ref id
                interactor?.getDocumentImage(docId: value.docId, storageRefId: nil, index: path)
            }
        }
    }
    
    private func handleBackButton() {
        let backButton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleNavBackButton))
        backButton.imageInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = backButton
    }
    
    // MARK:- Action Methods
    @objc func handleNavBackButton() {
        
        var refreshNeeded = false
        
        for item in dataSource {
            if item.uploaded == true {
                refreshNeeded = true
                break
            }
        }
        
        delegate?.clickedBtnBack(refreshNeeded: refreshNeeded)
    }
    
    override func displayDocTypeError(error: String) {
        showAlert(message: error)
        buttonHowHappen.isEnabled = false
    }
}

// MARK:- Display Logic Conformance
extension UploadMultipleImageVC: UploadMultipleImageDisplayLogic {
    
    func displayDocDetails(response: [DocumentDetails]?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func displayDocDetailsError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func displayUploadSuccess(index: IndexPath, docId: String?, storageRefId: String?, docName: String?) {
        removeLoaderFromCollection(path: index)
        dataSource[index.row].uploaded = true
        dataSource[index.row].docId = docId
        toggleSubmitButton()
    }
    
    func displayUploadError(error: String, index: IndexPath) {
        
        let model = dataSource[index.row]
        model.image = nil
        
        removeLoaderFromCollection(path: index)
        
        guard let alert = self.presentedViewController as? UIAlertController, alert.view.tag == 1001 else {
            showAlert(message: error, tag: 1001)
            return
        }
        print("already presented")
        // do nothing
    }
    
    func displayGetDocSuccess(image: UIImage?, path: IndexPath) {
        dataSource[path.row].image = image
        dataSource[path.row].uploaded = true
        removeLoaderFromCollection(path: path)
        toggleSubmitButton()
    }
    
    func displayGetDocError(path: IndexPath) {
        removeLoaderFromCollection(path: path)
    }
    
    func displayUpdateSRError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {[weak self] in
            self?.updateSRDocuments()
        }) {}
    }
    
    func displayUpdateSRSuccess(response: ClaimNotifyAllDocumentUploadedResponseDTO?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)

        delegate?.clickedBtnSubmit()
        
        if !isReupload {
            routeToDocumentSuccess()
        }
    }
}

extension UploadMultipleImageVC: ClaimSuccessVCDelegate {
    
    func clickCrossButton() {
        dismiss(animated: true, completion: nil)
        delegate?.clickedBtnBack(refreshNeeded: false)
    }
    
    func clickViewServiceRequest() {
        dismiss(animated: true, completion: nil)
        delegate?.clickedBtnBack(refreshNeeded: false)
    }
}

// MARK:- Configuration Logic
extension UploadMultipleImageVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = UploadMultipleImageInteractor()
        let presenter = UploadMultipleImagePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension UploadMultipleImageVC {
    func routeToDocumentSuccess() {
        
        let model = ClaimSuccessViewModel()
        model.successType = .upload
        let vc = ClaimSuccessVC()
        vc.delegate = self
        vc.successModel = model
        presentInFullScreen(vc, animated: true, completion: nil)
    }
}
