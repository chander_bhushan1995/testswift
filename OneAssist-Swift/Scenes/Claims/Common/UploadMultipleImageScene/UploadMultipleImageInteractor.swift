//
//  UploadMultipleImageInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol contains method prototype for presentation logic of Document upload
protocol UploadMultipleImagePresentationLogic {
    
    /// Update claim document
    ///
    /// - Parameters:
    ///   - response: response
    ///   - error: error
    ///   - index: index path
    func uploadClaimResponseRecieved(response: UploadClaimInvoiceResponseDTO?, error: Error?, index: IndexPath)
    
    /// Get uploaded document
    ///
    /// - Parameters:
    ///   - response: response
    ///   - error: error
    ///   - index: index path of image
    func documentResponseRecieved(response: GetClaimDocumentResponseDTO?, error: Error?, index: IndexPath)
    
    /// used to Update SR
    ///
    /// - Parameters:
    ///   - response: response
    ///   - error: error
    func updateSRResponseRecieved(response: ClaimNotifyAllDocumentUploadedResponseDTO?, error: Error?)
    
    func getDocumentDetails(response: GetDocumentDetailsResponseDTO?, error: Error?)
}

class UploadMultipleImageInteractor: BaseInteractor, UploadDocumentsBusinessLogic {
    
    var presenter: UploadMultipleImagePresentationLogic?
    
    // MARK: Business Logic Conformance
    func uploadClaimInvoice(srId: String, documentTypeId: String?, file: Data?, documentId: String?, index: IndexPath) {
        let req = UploadClaimInvoiceRequestDTO(serviceRequestId: srId, documentTypeId: documentTypeId, documentId: documentId, file: file)
        let _ = UploadClaimInvoiceRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.uploadClaimResponseRecieved(response: response, error: error, index: index)
        }
    }
    
    func getDocumentImage(docId: String?, storageRefId: String?, index: IndexPath) {
        let req = GetClaimDocumentRequestDTO(documentId: docId, storageRefIds: storageRefId, claimDocType: .thumbnail)
        let _ = GetClaimDocumentRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.documentResponseRecieved(response: response, error: error, index: index)
        }
    }
    
    func updateSR(srId: String, srType: String?, serviceDocuments: [ServiceDocument]?) {
        let req = ClaimNotifyAllDocumentUploadedRequestDTO()
        req.serviceID = srId
        req.srType = srType
        req.serviceDocuments = serviceDocuments
        
        req.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "Test"
        
        let _ = ClaimNotifyAllDocumentUploadedRequestUseCase.service(requestDTO: req) { (usecase, response, error) in
            self.presenter?.updateSRResponseRecieved(response: response, error: error)
        }
    }
    
    func getDocumentDetails(custId: String?, docCategory: String?, assetId: String?, assetCategory: String?, memId: String?) {
        let req = GetDocumentDetailsRequestDTO(custId: custId, docCategory: docCategory, assetId: assetId, assetCategory: assetCategory, memId: memId)
        
        let _ = GetDocumentDetailsRequestUseCase.service(requestDTO: req) { (usecase, response, error) in
            self.presenter?.getDocumentDetails(response: response, error: error)
        }
    }
}
