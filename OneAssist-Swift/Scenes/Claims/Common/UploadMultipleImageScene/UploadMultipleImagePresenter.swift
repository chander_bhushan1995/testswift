//
//  UploadMultipleImagePresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol is used to give methods prototype for upload documents.
protocol UploadMultipleImageDisplayLogic: class {
    
    /// Display error while image upload
    ///
    /// - Parameters:
    ///   - error: error message
    ///   - index: index path of image
    func displayUploadError(error: String, index: IndexPath)
    
    /// Display image uploaded successfully
    ///
    /// - Parameters:
    ///   - index: index path
    ///   - docId: docID
    func displayUploadSuccess(index: IndexPath, docId: String?, storageRefId: String?, docName: String?)
    
    /// Display error while getting image
    ///
    /// - Parameter path: index path of image
    func displayGetDocError(path: IndexPath)
    
    /// Display already uploaded document.
    ///
    /// - Parameters:
    ///   - image: uploaded image
    ///   - path: index path
    func displayGetDocSuccess(image: UIImage?, path: IndexPath)
    
    /// Display that SR is updated successfully
    ///
    /// - Parameter response: response model
    func displayUpdateSRSuccess(response: ClaimNotifyAllDocumentUploadedResponseDTO?)
    
    /// Display error in SR update
    ///
    /// - Parameter error: error message
    func displayUpdateSRError(error: String)
    
    func displayDocDetails(response: [DocumentDetails]?)
    func displayDocDetailsError(error: String)
    
}

/// This class is responsible to present the success or failure in get and upload image or update SR
class UploadMultipleImagePresenter: BasePresenter, UploadMultipleImagePresentationLogic {
    weak var viewController: UploadMultipleImageDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func uploadClaimResponseRecieved(response: UploadClaimInvoiceResponseDTO?, error: Error?, index: IndexPath) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUploadSuccess(index: index, docId: response?.data?.documentId, storageRefId: response?.data?.storageRefId, docName: response?.data?.documentName)
        } catch {
            viewController?.displayUploadError(error: error.localizedDescription, index: index)
        }
    }
    
    func documentResponseRecieved(response: GetClaimDocumentResponseDTO?, error: Error?, index: IndexPath) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayGetDocSuccess(image: response?.image, path: index)
        } catch {
            viewController?.displayGetDocError(path: index)
        }
    }
    
    func updateSRResponseRecieved(response: ClaimNotifyAllDocumentUploadedResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUpdateSRSuccess(response: response)
        } catch {
            viewController?.displayUpdateSRError(error: error.localizedDescription)
        }
    }
    
    func getDocumentDetails(response: GetDocumentDetailsResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayDocDetails(response: response?.data)
        } catch {
            viewController?.displayDocDetailsError(error: error.localizedDescription)
        }
    }
}
