//
//  UploadPhotoVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 10/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ServiceRequestUploadInvoiceDelegate:class{
    func clickServiceRequestUploadInvoiceBtn(vc:ServiceRequestUploadInvoiceVC)
}


class RowUploadImage : Row {
    
    var image : UIImage?
}

class ServiceRequestUploadInvoiceVC: BaseVC,PagingProtocol {

    @IBOutlet weak var tableView: UITableView!
    
   // @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
   // @IBOutlet weak var labelDot1: UILabel!
    
    @IBOutlet weak var lblDot1: UILabel!
    //@IBOutlet weak var labelSubtitle1: UILabel!
    @IBOutlet weak var lblSubtitle1: UILabel!
    
  //  @IBOutlet weak var labelSubtitle2: UILabel!
    
    @IBOutlet weak var lblSubtitle2: UILabel!
    //@IBOutlet weak var lablelDot2: UILabel!
    @IBOutlet weak var lblDot2: UILabel!
    
    @IBOutlet weak var photoOuterView: UIView!
    
    @IBOutlet weak var photoInnerView: UIView!
    
    //@IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    
   // @IBOutlet weak var labelUploadImage: UILabel!
    @IBOutlet weak var lblUploadImage: UILabel!
    
   // @IBOutlet weak var buttonHowHappened: PrimaryButton!
    
    @IBOutlet weak var btnHowHappened: PrimaryButton!
   // @IBOutlet weak var imageViewPhotoUpload: UIImageView!
    @IBOutlet weak var imgPhotoUpload: UIImageView!
    
    
    var arrTableView:[TableViewSection<RowUploadImage>]!
    
    
    weak var delegate:ServiceRequestUploadInvoiceDelegate?
    
    var currentPage: Int = 0
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        initialise()
//
//    }
//
//    func initialise(){
//        self.lblTitle.text = Strings.ServiceRequest.uploadPhoto.title
//        self.lblSubtitle1.text = Strings.ServiceRequest.uploadPhoto.subtitle1
//        self.lblSubtitle2.text  = Strings.ServiceRequest.uploadPhoto.subtitle2
//        self.lblDot1.layer.cornerRadius = self.lblDot1.frame.size.height/2
//        self.lblDot1.layer.masksToBounds = true
//        self.lblDot2.layer.cornerRadius = self.lblDot1.frame.size.height/2
//        self.lblDot2.layer.masksToBounds = true
//        self.btnHowHappened.isEnabled = false
//        self.photoOuterView.layer.cornerRadius = 5
//        self.photoOuterView.layer.borderColor = UIColor.lightGray.cgColor
//        self.photoOuterView.layer.borderWidth = 1
//    }
//
//    @IBAction func buttonHowHappedClk(_ sender: Any) {
//        self.delegate?.clickServiceRequestUploadInvoiceBtn(vc:self)
//    }
//    @IBAction func clickUploadBtn(_ sender: Any) {
//        Utilities.presentAlertController(vc: self) {[weak self] (image, error) in
//            if image != nil {
//                let data = UIImageJPEGRepresentation(image!, 0.9)
//                self?.imgPhotoUpload.image = UIImage(data:data!)
//                self?.photoInnerView.isHidden = true
//                self?.btnHowHappened.isEnabled = true
//            }
//            else{
//
//            }
//
//        }
//    }
    
    
    
    
    
   
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    func sectionTitle() -> String {
        return ""
    }
    // MARK:- private methods
    private func initializeView() {
        let section = TableViewSection<RowUploadImage>()
            section.title = sectionTitle()
            section.rows = createArrayForTableView()
            self.arrTableView = [section]

        tableView.register(nib: Constants.NIBNames.WHCService.serviceRequestUploadInvoiceTextCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.serviceRequestUploadInvoiceTextCell)
        tableView.register(nib: Constants.NIBNames.WHCService.ServiceRequestUploadInvoicePhotoCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.ServiceRequestUploadInvoicePhotoCell)
        tableView.register(nib: "UploadEstimationInVoiceCell", forCellReuseIdentifier: "UploadEstimationInVoiceCell")

        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        // tableview.rowHeight = CGFloat(Constants.CellHeight.whcServiceCell)
        tableView.estimatedRowHeight = CGFloat(Constants.CellHeight.whcServiceCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 40
       // self.selectAppliancesButton.isEnabled = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    func createArrayForTableView() -> [RowUploadImage] {
        let arrTableView : [RowUploadImage] = [getRow(title: "Heloo afjalkjfd kjasfkajsd laksdjf asdkljf", image:"",type:ServiceRequestUploadInvoiceTextCell.self) ,getRow(title: Strings.ServiceRequest.HappenedToAppliances.notWorking, image:"",type:ServiceRequestUploadInvoiceTextCell.self),getRow(title: "", image:"",type:ServiceRequestUploadInvoicePhotoCell.self)]
        return arrTableView
    }
    
    func getRow(title:String , image:String,type:Any) -> RowUploadImage {
        let row = RowUploadImage()
        row.title = title
        row.imageName = image
        row.type = type
        return row
    }
    // MARK:- Action Methods
    
    @IBAction func clickApplianceButton(_ sender: Any) {
       // self.delegate?.whatHappenedButtonTapped(category)
    }
}

// MARK:- Display Logic Conformance
extension ServiceRequestUploadInvoiceVC {
    
}

// MARK:- Configuration Logic
extension WhatHappenedVC {
    fileprivate func setupDependencyConfigurator() {
//        let interactor = WhatHappenedInteractor()
//        let presenter = WhatHappenedPresenter()
//        self.interactor = interactor
//        interactor.presenter = presenter
//        presenter.viewController = self
    }
}

// MARK:- Routing Logic


extension ServiceRequestUploadInvoiceVC:UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = self.arrTableView[indexPath.section]
        let row = section.rows[indexPath.row]
        
        if row.type! is ServiceRequestUploadInvoicePhotoCell.Type {
            
            let cell = tableView.cellForRow(at: indexPath) as! ServiceRequestUploadInvoicePhotoCell
            Utilities.presentAlertController(vc: self) { (image, error) in
                            if image != nil {
                                let data = image!.jpegData(compressionQuality: 0.9)
                                row.image = UIImage(data: data!)
                                cell.imgPhotoUpload.image = row.image
                                cell.imgPhotoUpload.isHidden = false
                                cell.imgEdit.isHidden = false
                                cell.btnEdit.isHidden = false
                            }
                            else{
                                cell.imgPhotoUpload.isHidden = true
                                cell.imgEdit.isHidden = true
                                cell.btnEdit.isHidden = true
                            }
                        }
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection sectionTableView: Int) -> UIView? {
    
        let view = Bundle.main.loadNibNamed(Constants.NIBNames.WHCService.ServiceRequestUploadInvoiceHeaderView, owner: self, options: nil)?[0] as? ServiceRequestUploadInvoiceHeaderView
        let section = self.arrTableView[sectionTableView]
        
        view?.lblTitle.text = section.title?.localized()
      
        
        return view
    }
    
    
    
}
extension ServiceRequestUploadInvoiceVC : ServiceRequestUploadInvoicePhotoDelegate {
    
    func clickBtn(){
        
    }
    func clickEditBtn(){
        
    }
}

extension ServiceRequestUploadInvoiceVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrTableView.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.arrTableView[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = self.arrTableView[indexPath.section]
        let row = section.rows[indexPath.row] 
        let rowType = row.type
        
        //UploadEstimationInVoiceCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UploadEstimationInVoiceCell", for: indexPath) as! UploadEstimationInVoiceCell
//        cell.lblTitle.text  = row.title
//        cell.imgView.image = #imageLiteral(resourceName: row.imageName)
//        cell.selectionStyle = .none
        return cell
        
        if rowType is ServiceRequestUploadInvoiceTextCell.Type{
         let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.serviceRequestUploadInvoiceTextCell, for: indexPath) as! ServiceRequestUploadInvoiceTextCell
            cell.lblTitle.text  = row.title
            cell.imgView.image = UIImage(named:row.imageName)
            cell.selectionStyle = .none
            return cell
        }
        else {
             let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.ServiceRequestUploadInvoicePhotoCell, for: indexPath) as! ServiceRequestUploadInvoicePhotoCell
            if let image = row.image {
                cell.imageView?.image = image;
                cell.imgPhotoUpload.isHidden = false
                cell.imgEdit.isHidden = false
                cell.btnEdit.isHidden = false
            }else {
                cell.imgPhotoUpload.isHidden = true
                cell.imgEdit.isHidden = false
                cell.btnEdit.isHidden = false
            }
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
}

