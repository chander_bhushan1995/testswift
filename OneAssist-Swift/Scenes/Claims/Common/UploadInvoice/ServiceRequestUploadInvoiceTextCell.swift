//
//  ServiceRequestUploadInvoiceTextCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 02/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class ServiceRequestUploadInvoiceTextCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseView()
        // Initialization code
    }

    func initialiseView(){
        lblTitle.font = DLSFont.bodyText.regular
        lblTitle.textColor = UIColor.gray
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
