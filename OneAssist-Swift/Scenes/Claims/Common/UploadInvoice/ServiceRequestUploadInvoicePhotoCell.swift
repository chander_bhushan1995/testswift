//
//  ServiceRequestUploadInvoicePhotoCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 02/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
protocol  ServiceRequestUploadInvoicePhotoDelegate:class{
    func clickBtn()
    func clickEditBtn()
}

class ServiceRequestUploadInvoicePhotoCell: UITableViewCell {

    @IBOutlet weak var photoOuterView: UIView!
    
    @IBOutlet weak var photoInnerView: UIView!
    
    //@IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var imgCamera: UIImageView!
    
    // @IBOutlet weak var labelUploadImage: UILabel!
    @IBOutlet weak var lblUploadImage: UILabel!
    
    // @IBOutlet weak var buttonHowHappened: PrimaryButton!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var btnUploadImage: UIButton!
    @IBOutlet weak var btnHowHappened: PrimaryButton!
    // @IBOutlet weak var imageViewPhotoUpload: UIImageView!
    @IBOutlet weak var imgPhotoUpload: UIImageView!
    
    weak var delegate:ServiceRequestUploadInvoicePhotoDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseView()
        // Initialization code
    }
    func initialiseView(){
        lblUploadImage.font = DLSFont.bodyText.regular
        lblUploadImage.textColor = UIColor.dodgerBlue
        }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func clickUploadImage(_ sender: Any) {
        
    }
    
    @IBAction func clickEditBtn(_ sender: Any) {
        delegate?.clickEditBtn()
    }
}
