//
//  ServiceRequestUploadInvoiceHeaderView.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 02/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ServiceRequestUploadInvoiceHeaderViewDelegate:class{
    func clickBtn(headerView:ServiceRequestUploadInvoiceHeaderView)
}

class ServiceRequestUploadInvoiceHeaderView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    weak var delegate:ServiceRequestUploadInvoiceHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialise()
    }
    func initialise(){
        lblTitle.textColor = UIColor.charcoalGrey
        lblTitle.font = DLSFont.h3.bold
    }
    func setModel(title:String)  {
        self.lblTitle.text = title
    }
    func setUI(_ activeType:Bool){
    }
    
    @IBAction func clickBtn(_ sender: Any) {
        delegate?.clickBtn(headerView:  self)
    }
}
