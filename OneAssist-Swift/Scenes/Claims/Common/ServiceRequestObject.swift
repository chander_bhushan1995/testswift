//
//  ServiceRequestObject.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 02/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

enum ServiceRequestType {
    case home
    case EW
}

class ServiceRequestObject {
    var boolPedingSubmission : Bool = false
    var type :ServiceRequestType!
    var membershipId: String!
    var draftSRId: String?
    var serviceRequestID : String!//This will be used to update SR
    var address:String!
    var pinCode:String?
    var dateWhenHappen:String!
    var createdOn: String!  // This will be used if claim raised from CRM
    var issueId: NSNumber?
    var issueDescription: String?
    var assetIds: [String]?// These assest id are those for which user created sr from Crm or going to create SR from Client
    var availability: AvailabilityViewModel?
    var placeOfIncident : String?
    var incidentDescription : String?
    var isCombo: Bool = false
    var bpCode: String?
    var buCode: String?
    
    // This is total number of services which will be shown on WhatHappened screen
    var totalServices: [ServiceDescription]?
    
    // This is total number of assets which will be shown on further screens.
    var totalAssets: [Asset]?
    
    fileprivate init() {}
    
    private static let _shared = ServiceRequestObject()
    
    class var shared: ServiceRequestObject {
        return _shared
    }
}

class AssetEligibility {
    var eligible : Bool
    var reason : String
    init(eligible_ : Bool,reason_ : String) {
        reason = reason_
        eligible = eligible_
    }
}


class AdditionalDetail: ParsableModel {
    var gifterName: String?
    var customerGifterRelation: String?
    var invoiceNameVariationReason: String?
    var mismatchCustomerName: String?
    var customerNameInvoiceMismatch: String?
    var customerNameMismatchOtherReason: String?
    
    func createRequestParameters() -> [String : Any]? {
        let parameters = JSONParserSwift.getDictionary(object: self)
         return parameters
    }
}

class ClaimHomeServe: ServiceRequestObject {
    var productCode : String?
    var productName: String?
    var assetServiceType : Constants.Services?
    var supportedRequestTypes:[ServiceDescription]? // it holds services available on pincode
    var assetAvailability: [String: AssetEligibility]?
    var scheduledDate: Date?
    var scheduledTimeslot: ScheduleVisitTimeSlotViewModel?
    var damageType : String?
    var damagePartName : String?
    var damageConditions : [PartDamageViewModel]?
    var blockReferenceNo : String?
    var blockReferenceDate : String?
    var insurancePartnerCode : NSNumber?
    var productVariantId: NSNumber?
    var additionalDetails:AdditionalDetail?
    var purchaseDate:NSNumber?
    private static var _shared: ClaimHomeServe!
    
    override static var shared: ClaimHomeServe {
        if _shared == nil {
            _shared = ClaimHomeServe()
        }
        return _shared
    }
    
    static func clean() {
        _shared = nil
    }
    
    func setUpAdditionalValue(invDocMismatchL: String?, assetAtribute: [AssetAttributes]?) {
        if let invDocNameMismatch = invDocMismatchL {
            let aditionalData: AdditionalDetail = AdditionalDetail(dictionary: [:])
            if invDocNameMismatch.uppercased() != Strings.GifterScenario.GifterTypeValue.verified {
                aditionalData.customerNameInvoiceMismatch = "Y"
                
                if invDocNameMismatch.uppercased() == Strings.GifterScenario.GifterTypeValue.loanedType {
                    aditionalData.invoiceNameVariationReason = Strings.GifterScenario.GifterTypeValue.loanedType
                    
                }else if invDocNameMismatch.uppercased() == Strings.GifterScenario.GifterTypeValue.othersType {
                    aditionalData.invoiceNameVariationReason = Strings.GifterScenario.GifterTypeValue.othersType
                    
                }else if invDocNameMismatch.uppercased() == Strings.GifterScenario.GifterTypeValue.gifterSomeoneType {
                    aditionalData.invoiceNameVariationReason = Strings.GifterScenario.GifterTypeValue.gifterSomeoneType
                    
                }else if invDocNameMismatch.uppercased() == Strings.GifterScenario.GifterTypeValue.spellingMistakeType {
                    aditionalData.invoiceNameVariationReason = Strings.GifterScenario.GifterTypeValue.spellingMistakeType
                    
                }
                
            }else {
                aditionalData.customerNameInvoiceMismatch = "N"
            }
            
            if let assetAttributes = assetAtribute {
                assetAttributes.forEach { (attributeModel) in
                    if let keyname = attributeModel.key, let value = attributeModel.value {
                        if keyname == Strings.GifterScenario.assetAttributeKeyType.mismatchName {
                            aditionalData.mismatchCustomerName = value
                            
                        } else if keyname == Strings.GifterScenario.assetAttributeKeyType.gifterName {
                            aditionalData.gifterName = value
                            
                        } else if keyname == Strings.GifterScenario.assetAttributeKeyType.gifterRelation {
                            aditionalData.customerGifterRelation = value
                            
                        } else if keyname == Strings.GifterScenario.assetAttributeKeyType.otherReason {
                            aditionalData.customerNameMismatchOtherReason = value
                            
                        }
                    }
                }
            }
            
            ClaimHomeServe.shared.additionalDetails = aditionalData
        }
        
    }
}

