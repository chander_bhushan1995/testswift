//
//  GetStartedPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/20/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol GetStartedDisplayLogic: class {
    //    func displayServices(with availability: AvailabilityViewModel)
    //    func displayError(error: String)
    
    func displayAllServices(with availability: AvailabilityViewModel)
}

class GetStartedPresenter: BasePresenter, GetStartedPresentationlogic {
    
    weak var viewController: GetStartedDisplayLogic?
    var raisedFromCrm = false
    var crmServiceType:Constants.Services = .fire
    
    func presentAllServices(for memResponse: CustomerMembershipDetails?) {
        
        if let allServices = memResponse?.plan?.services {
            
            var isRestrictedPMS = true
            
            // Create ServiceDescription model from Services
            var services = allServices.compactMap { (serviceResponse) -> ServiceDescription? in
                if let service = Constants.Services(rawValue: serviceResponse.serviceDescription?.first?.claimType ?? "") {
                    var serviceDescription =  ServiceDescription(service, description: serviceResponse.serviceDescription)
                    serviceDescription.partnerCode = serviceResponse.partnerCode
                    
                    // This field has significance only in case of PMS and SOP.
                    serviceDescription.noOfVisitsLeft = serviceResponse.noOfVisitsLeft
                    
                    // In case of Insurance backed --> assets will be used, otherwise products
                    
                    // It is used in case of PMS and SOP. And it is also used to differentiate HA_BD and SOP. In general, PMS service is not insurance backed.
                    serviceDescription.isInsuranceBacked = serviceResponse.insuranceBacked
                    
                    // Check if restricted PMS
                    if serviceDescription.service == Constants.Services.pms {
                        isRestrictedPMS = serviceResponse.attributes?.contains(where: { $0.attributeName == "RESTRICT_ASSETS" && $0.attributeValue == "Y"}) ?? false
                        serviceDescription.isRestrictedPMS = isRestrictedPMS
                    }
                    
                    /* assets will be populted from products. products are only available for those services which are not insurance backed. So this below population is for non insurance backed services.
                     
                     Add products as Assets for non-insurance backed services
                     basically these products are which are support with specific services */
                    serviceDescription.assets = serviceResponse.products?.compactMap({ (product) -> Asset? in
                        if let prodCode = product.code {
                            let asset = Asset(dictionary: [:])
                            asset.name = product.displayValue ?? ""
                            asset.prodCode = prodCode
                            return asset
                        }
                        return nil
                    })
                    return serviceDescription
                }
                return nil
            }
            
            // Add eligible assets for services
            mapAssetsServices(services: &services, allAssets: memResponse?.assets, isRestrictedPMS: isRestrictedPMS)
            if services.count > 0 {
                
                // Save all services. These are all services which we will show on Whathappened screen. But enabled would only be supported services. Supported services will be calculated on PincodeVC based intersection with pincode & its applicable services.
                ClaimHomeServe.shared.totalServices = services
            }
            
            // Save all assets. These all assets will be shown in the screen after "WhatHappened" screen. Enabled assets will only be those which are mapped to that corresponding service, others would be disabled.
            ClaimHomeServe.shared.totalAssets = memResponse?.assets
        }
        
        // This is a mapping of services from asset. means which services are available for which asset
        var assetViewModels = [String: Set<ServiceDescription>]()
        if let assets = memResponse?.assets {
            for product in assets {  // iterate all asets
                if let assetId = product.assetId?.description,
                    //retrieve services for each asset and prepare a view model
                    let services = product.services?.compactMap ({ (serviceResponse) -> ServiceDescription? in
                        if let service = Constants.Services(rawValue: serviceResponse.serviceDescription?.first?.claimType ?? "") {
                            var serviceDescription =  ServiceDescription(service, description: serviceResponse.serviceDescription)
                            serviceDescription.partnerCode = serviceResponse.partnerCode
                            return serviceDescription
                        }
                        return nil
                    })
                {
                    let serviceSet = Set(services)
                    assetViewModels[assetId] = serviceSet
                }
            }
        }
        
        viewController?.displayAllServices(with: AvailabilityViewModel(availableAssets: assetViewModels))
    }
    
    func mapAssetsServices( services: inout [ServiceDescription], allAssets: [Asset]?, isRestrictedPMS: Bool) {
        var isServiceTenureActive = true
        if let assets = allAssets {
            let serviceCount = services.count
            
            // Iterate for all services
            for i in 0..<serviceCount {
                
                // Get eligible assets for that service
                let assetList = assets.filter { (asset) -> Bool in
                    return asset.services?.contains(where: { (assetService) -> Bool in
                        
                        // check if asset is eligible for claim
                        var flagEligible = false
                        
                        // Check Eligibility API response is also used here.
                        if let assetId = asset.assetId?.description, let assetEligibilty =  ClaimHomeServe.shared.assetAvailability?[assetId] {
                            flagEligible = assetEligibilty.eligible
                        }
                        
                        // check if service is avaible in asset and asset is eligible for claim
                        if Constants.Services.init(rawValue: assetService.serviceName ?? "") == services[i].service && flagEligible {
                            isServiceTenureActive = assetService.isServiceTenureActive
                            return true
                        }
                        
                        
                        return Constants.Services.init(rawValue: assetService.serviceName ?? "") == services[i].service && flagEligible
                    }) ?? false
                }
                
                // Here assets for all those services which will be updated which are insurance backed.
                
                // If service is insurance backed, add asset list in service
                if (services[i].isInsuranceBacked ?? true) {
                    services[i].assets = assetList
                    services[i].isServiceTenureActive = isServiceTenureActive
                }
                
                // Delete extra products in case of restricted PMS
                if isRestrictedPMS && services[i].service == Constants.Services.pms {
                    services[i].assets = services[i].assets?.filter({ (asset) -> Bool in
                        return assets.contains(where: { $0.prodCode == asset.prodCode })
                    })
                }
            }
        }
    }
}

struct ServiceDescription: Hashable {
    var hashValue: Int
    
    static func ==(lhs: ServiceDescription, rhs: ServiceDescription) -> Bool {
        return lhs.service == rhs.service
    }
    
    let service: Constants.Services
    var partnerCode: NSNumber?
    var noOfVisitsLeft: NSNumber?
    let serviceDescription: [Description]?
    let serviceEnable: Bool = false
    var isInsuranceBacked: Bool? = true
    var isServiceTenureActive: Bool? = true // enable warranty service request once the manufacturing warranty ends
    var isRestrictedPMS: Bool? = false
    var assets: [Asset]?
    
    init?(service: Constants.Services?) {
        if let service = service {
            self.init(service)
        } else {
            return nil
        }
    }
    
    init(_ service: Constants.Services, description: [Description]? = nil) {
        self.service = service
        serviceDescription = description
        hashValue = service.hashValue
    }
}

struct AvailabilityViewModel {
    var availableAssets: [String: Set<ServiceDescription>]
    var totalAvailableServices: Set<ServiceDescription>
    
    init(availableAssets: [String: Set<ServiceDescription>]) {
        self.availableAssets = availableAssets
        
        // Union of servicelist for all available assets
        totalAvailableServices = availableAssets.reduce(Set<ServiceDescription>()) { $0.union($1.value) }
        if let allServices = ClaimHomeServe.shared.totalServices {
            totalAvailableServices = Set(allServices).union(totalAvailableServices)
        }
        
    }
}
