//
//  getStartedVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol GetStartedBusinessLogic {
//    func getServices(for membershipId: String, with availability: [String: AssetEligibility]?)
    func getAllServices(for memResponse: CustomerMembershipDetails?)
}

class GetStartedVC: BaseVC {

    //outlets
    @IBOutlet private weak var lblName: H3BoldLabel!
    @IBOutlet private weak var lblSubtitle: H3BoldLabel!
    @IBOutlet private weak var lblAnswer: BodyTextRegularGreyLabel!
    @IBOutlet weak var lblSerial: H3BoldLabel!
    @IBOutlet private weak var btnBegin: OAPrimaryButton!
    
    var srId = ""
    var memId: String!
    var raisedFromCrm = false
    var crmServiceType: Constants.Services = .fire
    var memResponse: CustomerMembershipDetails?
    
    private var planName : String! = ""
    private var productName : String?
    private var numofAppliance : String! = ""
    private var serialNo : String?
    
    var interactor: GetStartedBusinessLogic?
    fileprivate var category : Constants.Services!
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDependencyConfigurator()

        initialiseView()
        memId = ClaimHomeServe.shared.membershipId
        CacheManager.shared.memResponse = memResponse
        
        if let bpCode = memResponse?.partnerCode {
            ClaimHomeServe.shared.bpCode = bpCode.stringValue
            
        }else {
            ClaimHomeServe.shared.bpCode = Constants.SchemeVariables.partnerCode
           
        }
        
        if let buCode = memResponse?.partnerBUCode  {
            ClaimHomeServe.shared.buCode = buCode.stringValue
        }else {
            ClaimHomeServe.shared.buCode = Constants.SchemeVariables.partnerBuCode
        }
    }
    
    func updateInfo(plan : String, product : String?, applianceCount : String?, serialNo: String?) {
        planName = plan
        productName = product
        numofAppliance = applianceCount
        self.serialNo = serialNo
    }
    
  private  func initialiseView(){
    
        self.lblSerial.isHidden =  true
        self.title = Strings.ServiceRequest.Titles.raiseARequest
        if let user = CustomerDetailsCoreDataStore.currentCustomerDetails?.firstName {
            self.lblName.text = Strings.ServiceRequest.hi + user + "!"
        } else {
            self.lblName.text = Strings.ServiceRequest.hi + "User" + "!"
        }
        self.lblSubtitle.text = Strings.ServiceRequest.dontWorry
        
        setPlanLabelText()
        self.view.backgroundColor = UIColor.whiteSmoke
        btnBegin.setTitle(Strings.ServiceRequest.letsBegin, for: .normal)
    
    if let membership = self.memResponse {
        ClaimHomeServe.shared.setUpAdditionalValue(invDocMismatchL: membership.getInvDocNameMismatch(), assetAtribute: membership.getAssetAttributes())
    }
    }
  private  func setPlanLabelText(){
    var firstText:String
    var planText:String?
    var thirdText :String = "appliance"
    if productName != nil {
        thirdText = productName!
    }
    var text : String
    
    if raisedFromCrm {
        firstText = "You are about to update a Service Request raised from "
        planText = "Customer Care"
        if let numofAppliance = numofAppliance {
            thirdText = String(numofAppliance)
            text = firstText + planText! + " for your " + thirdText + " appliances"
        }else   {
            text = firstText + planText! + " for your " + thirdText
        }
        
    } else {
        firstText = "You are about to raise a service request for your "
        planText = planName
        
        if let memRes = memResponse, Utilities.isSOPServiceAvailable(membership: memRes) {
            text = firstText + planText!
        } else {
            if let numofAppliance = numofAppliance {
                thirdText = String(numofAppliance)
                text = firstText + planText! + " plan covering " + thirdText + " appliances"
            } else {
                text = firstText + planText! + " plan for your " + thirdText
            }
        }
    }
    
    if serialNo != nil {
        text = text + " with \n\n" + "\(serialNo!)"
    }
       
//        let attributedText = NSMutableAttributedString(string: text)
//        var planTextRange: NSRange? = (text as NSString).range(of: planText!)
//        // * Notice that usage of rangeOfString in this case may cause some bugs
//        // I use it here only for demonstration
//        if let aRange = planTextRange {
//            attributedText.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: DLSFont.tags.bold15], range: aRange)
//        }
//        planTextRange = (text as NSString).range(of: thirdText)
//        if let aRange = planTextRange {
//            attributedText.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: DLSFont.tags.bold15], range: aRange)
//        }
        self.lblAnswer.setAttributedString(text, bodyColor: UIColor.bodyTextGray, bodyFont: DLSFont.bodyText.regular,
                                           attributedParts: [planText!, thirdText, serialNo ?? ""],
                                           attributedTextColors: [UIColor.bodyTextGray, UIColor.bodyTextGray, UIColor.bodyTextGray],
                                           attributedFonts: [DLSFont.h3.bold, DLSFont.h3.bold, DLSFont.h3.bold])
       
        
    }
    
    @IBAction func clickButton(_ sender: Any) {
//        EventTracking.shared.eventTracking(name: .SRLetsBegin)

        EventTracking.shared.eventTracking(name: .SRLetsBegin, [.subcategory: CacheManager.shared.eventProductName ?? ""])
//        Utilities.addLoader(onView: view, message: "Please wait while we are fetching your information", count: &loaderCount, isInteractionEnabled: false)
//        interactor?.getServices(for: memId, with: ClaimHomeServe.shared.assetAvailability)
        interactor?.getAllServices(for: memResponse)
    }
}

// MARK:- Configuration Logic
extension GetStartedVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = GetStartedInteractor()
        let presenter = GetStartedPresenter()
        self.interactor = interactor
        interactor.raisedFromCrm = self.raisedFromCrm
        interactor.crmServiceType = self.crmServiceType
        interactor.presenter = presenter
        presenter.crmServiceType = self.crmServiceType
        presenter.raisedFromCrm = self.raisedFromCrm
        presenter.viewController = self
    }
}

extension GetStartedVC: GetStartedDisplayLogic {
    func displayError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayAllServices(with availability: AvailabilityViewModel) {
        if availability.totalAvailableServices.count > 0 {
            ClaimHomeServe.shared.availability = availability
            
            if availability.totalAvailableServices.contains(ServiceDescription(.extendedWarranty)) {
                category = .extendedWarranty
                if availability.totalAvailableServices.count > 1 {
                    ClaimHomeServe.shared.isCombo = true
                }
            } else if availability.totalAvailableServices.contains(where: { [ServiceDescription(.fire), ServiceDescription(.buglary), ServiceDescription(.accidentalDamage), ServiceDescription(.breakdown)].contains($0) }) {
                category = .accidentalDamage            // HA Category
            } else if availability.totalAvailableServices.contains(where: { [ServiceDescription(.peADLD), ServiceDescription(.peExtendedWarranty), ServiceDescription(.peTheft)].contains($0) }) {
                category = .peADLD                  // PE Category
            }
            
            if  raisedFromCrm {
                var assetViewModels = [String: Set<ServiceDescription>]()
                if let arrAssestID = ClaimHomeServe.shared.assetIds{
                    if let assestId = arrAssestID.first{
                        assetViewModels[assestId] = Set([ServiceDescription(self.crmServiceType)])
                    }
                }
                routeToPincode(pinCode: ClaimHomeServe.shared.pinCode, address: ClaimHomeServe.shared.address,availabilityModel: ClaimHomeServe.shared.availability)
            } else {
                routeToPincode(pinCode: nil, address: nil ,availabilityModel: ClaimHomeServe.shared.availability)
            }
        } else {
            //TODO: No assets available for service
        }
    }
}

extension GetStartedVC {
    func routeToPincode(pinCode : String? ,address : String? , availabilityModel: AvailabilityViewModel? ) {
        let vc = ServiceRequestPincodeVC()
        vc.delegate = self
        vc.category = self.category
        vc.isFromCrm = raisedFromCrm
        if let pinCode  = pinCode {
            vc.pinCode = pinCode
        }
        if let address = address {
            vc.address = address
        }
        
        vc.totalAvailableService = availabilityModel
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}

extension GetStartedVC:ServiceRequestVerifyPinCodeDelegate{
    
    func selfRepairButtonTapped(){
        
    }
    
    func didDismissController(vc: ServiceRequestPincodeVC, category: Constants.Services) {
        ClaimHomeServe.shared.address = vc.adressTV.text
        ClaimHomeServe.shared.pinCode = vc.pinTF.fieldText
        
        let vc = ProgressContainerVC()
        vc.raisedFromCrm = raisedFromCrm
        vc.category = category
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
