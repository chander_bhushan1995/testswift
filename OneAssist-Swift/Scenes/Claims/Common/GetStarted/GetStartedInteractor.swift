//
//  GetStartedInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/20/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol GetStartedPresentationlogic {
    func presentAllServices(for memResponse: CustomerMembershipDetails?)
}

class GetStartedInteractor: GetStartedBusinessLogic {
    var presenter: GetStartedPresentationlogic?
    var raisedFromCrm = false
    var crmServiceType:Constants.Services = .fire
    
    func getAllServices(for memResponse: CustomerMembershipDetails?) {
        presenter?.presentAllServices(for: memResponse)
    }
}
