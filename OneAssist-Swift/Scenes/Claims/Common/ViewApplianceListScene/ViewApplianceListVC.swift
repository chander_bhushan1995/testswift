//
//  ViewApplianceListVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 3/27/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This class used to display Appliance list.
class ViewApplianceListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [String]?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- private methods
    private func initializeView() {
        tableView.register(cell: DocumentImageViewLabelCell.self)
        tableView.rowHeight = 30
        tableView.bounces = false
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        
        guard let dataSource = dataSource, dataSource.count <= 4 else {
            return
        }
            
        preferredContentSize = CGSize(width: 300, height: tableView.contentSize.height)
        tableView.frame = CGRect(origin: tableView.frame.origin, size: tableView.contentSize)
    }
}

extension ViewApplianceListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataSource != nil {
            return (dataSource?.count)!
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: DocumentImageViewLabelCell = tableView.dequeueReusableCell(indexPath: indexPath)
        
        cell.descriptionLabel.textAlignment = .center
        cell.descriptionLabel.font = DLSFont.bodyText.regular
        cell.descriptionLabel.text = dataSource?[indexPath.row]

        return cell;
    }
}
