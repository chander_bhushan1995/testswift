//
//  DocumentTypeBaseInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 31/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol DocumentTypeBasePresentationLogic {
    func responseRecieved(response: GetServiceDocumentTypeResponseDTO?, brand: String?, srType: Constants.Services, error: Error?)

}

class DocumentTypeBaseInteractor: BaseInteractor, DocumentTypeBaseBusinessLogic {
    
    var presenter: DocumentTypeBasePresentationLogic?
    
    // MARK: Business Logic Conformance
    func getRequiredDocumentTypes(serviceRequestType: Constants.Services, productCode: String?, insurancePartnerCode: String?, brand: String?) {
        let req = GetServiceDocumentTypeRequestDTO(srType: serviceRequestType, productCode: productCode, insurancePartnerCode: insurancePartnerCode, isMandatory: nil)
        let _ = GetServiceDocumentTypeRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.responseRecieved(response: response, brand: brand, srType: serviceRequestType, error: error)
        }
    }
    
}
