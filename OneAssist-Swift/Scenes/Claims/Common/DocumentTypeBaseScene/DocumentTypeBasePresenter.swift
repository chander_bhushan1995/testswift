//
//  DocumentTypeBasePresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 31/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol used to declare methods to display success or failure of Documents API
protocol DocumentTypeBaseDisplayLogic: class {
    
    /// Used to display error while getting documents
    ///
    /// - Parameter error: error message
    func displayDocTypeError(error: String)
    
    /// Used to display documents on screen
    ///
    /// - Parameter serviceDocTypes: all documents
    func displayDocTypeResponse(serviceDocTypes: [ServiceDocumentType])
    
}

/// This class is used to confirm the methods of *DocumentTypeBasePresentationLogic* protocol. It contains methods to prepare documents to upload.
class DocumentTypeBasePresenter: BasePresenter, DocumentTypeBasePresentationLogic {
    
    weak var viewController: DocumentTypeBaseDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func responseRecieved(response: GetServiceDocumentTypeResponseDTO?, brand: String?, srType: Constants.Services, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            var responseData = response?.data ?? []
           
            // In PE, show only "Y" and "N documents"
            if Utilities.isSrTypePE(value: srType.rawValue) {

                let yDoc = responseData.filter{$0.isMandatory == "Y"}
                var nDoc = responseData.filter{$0.isMandatory == "N"}
                if let addDoc = nDoc.first(where: {$0.docKey == "ADDITIONAL_DOCUMENT1"}) {
                    let ind = nDoc.firstIndex(of: addDoc)
                    nDoc.swapAt(0, ind ?? 0)
                }
                responseData = yDoc + nDoc
            }
            
            viewController?.displayDocTypeResponse(serviceDocTypes: responseData)
        } catch {
            viewController?.displayDocTypeError(error: error.localizedDescription)
        }
    }
    
}
