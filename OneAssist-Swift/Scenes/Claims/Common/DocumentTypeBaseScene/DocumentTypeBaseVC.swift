//
//  DocumentTypeBaseVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 31/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol DocumentTypeBaseBusinessLogic {
    func getRequiredDocumentTypes(serviceRequestType: Constants.Services, productCode: String?, insurancePartnerCode: String?, brand: String?)
    
}
/**
 this class is used for getting the document type ,doucment id and document name
 - isReupload is used when reupload is done .
 
 */
class DocumentTypeBaseVC: BaseVC {
    
    var interactorBase: DocumentTypeBaseBusinessLogic?
    var serviceRequestType: Constants.Services = .extendedWarranty
    var isReupload = false
    var insurancePartnerCode: String?
    var productCode: String?
    var brand: String? = nil
    var isGifterPrrofRequired: Bool = false
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        if serviceRequestType == .buglary {
            EventTracking.shared.addScreenTracking(with: .SR_Upload_FIR)
        } else {
            EventTracking.shared.addScreenTracking(with: .SR_Upload_Image)
        }
        
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        if !isReupload {
            Utilities.addLoader(onView: view, message: "getting document types", count: &loaderCount, isInteractionEnabled: false)
            interactorBase?.getRequiredDocumentTypes(serviceRequestType: serviceRequestType, productCode: productCode, insurancePartnerCode: insurancePartnerCode, brand: brand)
        }
    }
    
    // MARK:- Action Methods
    
}

// MARK:- Display Logic Conformance
@objc extension DocumentTypeBaseVC: DocumentTypeBaseDisplayLogic {
    
    func displayDocTypeError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func displayDocTypeResponse(serviceDocTypes: [ServiceDocumentType]) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }

}

// MARK:- Configuration Logic
extension DocumentTypeBaseVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = DocumentTypeBaseInteractor()
        let presenter = DocumentTypeBasePresenter()
        self.interactorBase = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension DocumentTypeBaseVC {
    
}
