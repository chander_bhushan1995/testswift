//
//  WhatHappenedCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class WhatHappenedCell: UITableViewCell,ReuseIdentifier,NibLoadableView {

    
    @IBOutlet weak var imageRadio: UIImageView!
    @IBOutlet weak var lblReason: BodyTextRegularGreyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialise()
        // Initialization code
    }
    
    func initialise()
    {
        lblReason.numberOfLines = 0
    }
    
    func setModel(title:String , imageName : String)
    {
        self.lblReason.text = title
        self.imageRadio.image = UIImage(named:imageName)
    }
}
