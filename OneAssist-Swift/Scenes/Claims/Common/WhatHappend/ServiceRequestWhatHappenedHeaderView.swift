//
//  ServiceRequestWhatHappenedHeaderView.swift
//  OneAssist-Swift
//
//  Created by Sudhir.Kumar on 31/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import UIKit
protocol ServiceRequestWhatHappenedHeaderViewDelegate:class {
    func clickBtn(headerView : ServiceRequestWhatHappenedHeaderView)
}
class ServiceRequestWhatHappenedHeaderView: UIView,NibLoadableView {
    
    var delegate:ServiceRequestWhatHappenedHeaderViewDelegate?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var viewMask : UIView!
    @IBOutlet weak var lblSubtitle: SupportingTextRegularGreyLabel!
    
    var section : Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialise()
    }
    func initialise(){
        lblTitle.textColor = UIColor.charcoalGrey
//        lblTitle.font = DLSFont.h3.regular
    }
    func setModel(title:String, subtitle: String?)  {
        self.lblTitle.text = title;
        
        if let sub = subtitle {
            lblSubtitle?.text = sub
        }
    }
    func setUI(_ activeType:Bool){
        self.imgAdd.image = activeType ? #imageLiteral(resourceName: "radioButtonActive"):#imageLiteral(resourceName:"radioButtonInactive")
        lblTitle.setSelectedOrNot(activeType)
    }
    
    @IBAction func clickBtn(_ sender: Any) {
        delegate?.clickBtn(headerView:  self)
    }
}
