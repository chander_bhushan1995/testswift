//
//  WhatHappenedPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WhatHappenedPresenterDisplayDelegate: class {
    func displayLogic(dataSource:[WhatHappenSection],unavailableService : Int)
    func displayError(_ error:String)
}

class WhatHappenedPresenter: BasePresenter, WhatHappenedPresentationLogic {
    
    weak var delegate : WhatHappenedPresenterDisplayDelegate?
    func presentData(_ response: WhatHappendResponseDTO?, error: Error?, request: WhatHappendRequestDTO , preselectedIssueId : NSNumber?)
    {
        
        do {
            try checkError(response, error: error)
            delegate?.displayLogic(dataSource: createIssueList(response: response!,preselectedIssueId: preselectedIssueId) , unavailableService: 0)
            
        }
        catch {
            delegate?.displayError(error.localizedDescription)
        }
        
    }
    
    
    func presentData(category: Constants.Services){
        
        if category.isHAClaimType() {
            //create model of type section from the response
            let tuple = createArrayForWholeHomeCover()
            delegate?.displayLogic(dataSource:tuple.0 , unavailableService: tuple.1) // just change the unavailable to the available
            
            
        } else if category.isPEClaimType() {
            //create model of type section from the response
            delegate?.displayLogic(dataSource: createArrayForPE() , unavailableService:  0)
            
            
        }
    }
    
    // MARK: Presentation Logic Cletormance
    func createArrayForPE() -> [WhatHappenSection] {
        var arrTableView : [WhatHappenSection] = []
        if let supportedTypes = ClaimHomeServe.shared.supportedRequestTypes {
            for service in supportedTypes {
                if let descriptions = service.serviceDescription {
                    for description in descriptions {
                        let section = getSection(title: description.displayValue, rows: description.subDescription)
                        section.type = Constants.Services(rawValue: description.claimType ?? "")
                        section.damageType = description.damageType
                        section.partnerCode = service.partnerCode
                        arrTableView.append(section)
                    }
                }
            }
            if ClaimHomeServe.shared.boolPedingSubmission {
                let sectArray = arrTableView.filter({ $0.type.rawValue == ClaimHomeServe.shared.assetServiceType!.rawValue })
                sectArray.first?.isSelected = true
              return sectArray
            }
        }
        return arrTableView
    }
    
    func createArrayForWholeHomeCover() ->([WhatHappenSection] , Int){
       
        var arrTableView : [WhatHappenSection] = []
        var countUnavailableService = 0
        var countSupportedRequestType = 0

        if let totalAvailableServices1 = ClaimHomeServe.shared.availability?.totalAvailableServices {
            
            // Set services priority wise
            let totalAvailableServices = totalAvailableServices1.sorted(by: { (one, two) -> Bool in
                if let priority1 = one.serviceDescription?.first?.priority?.intValue, let priority2 = two.serviceDescription?.first?.priority?.intValue {
                    
                    if priority1 < priority2 {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }
            })
            
            if let supportedRequestTypes = ClaimHomeServe.shared.supportedRequestTypes {
                countUnavailableService =  totalAvailableServices.count - supportedRequestTypes.count
                countSupportedRequestType =  supportedRequestTypes.count
            }
            // check if claim is already raised somewhere else except iOS app
            if ClaimHomeServe.shared.boolPedingSubmission {
                for service in totalAvailableServices {
                    if let crmRaisedService =  ClaimHomeServe.shared.assetServiceType {
                        if service.service == crmRaisedService {
                            let section = getSection(title: service.serviceDescription?.first?.displayValue, rows: service.serviceDescription?.first?.subDescription)
                            section.isSelected = true
                            section.isEnabled = true
                            section.type = service.service
                            arrTableView.append(section)
                        }
                    }
                }
            } else { // if claim is raising from iOS app
                for service in totalAvailableServices {
                    let rows = service.serviceDescription?.first?.subDescription
                    var subtitle: String? = nil
                    
                    if let count = service.noOfVisitsLeft, Int(truncating: count) <= 0  {
                            subtitle = "Note: As your free services have exhausted, you will be charged for this service"
                    }
                    
                    let section = getSection(title: service.serviceDescription?.first?.displayValue, subtitle: subtitle, rows: rows)
                    if let supportedRequestTypes = ClaimHomeServe.shared.supportedRequestTypes { // if services available for pincode
                        var enabled = false
                        for serviceSupported in supportedRequestTypes {
                            // Check if service is available or not
                            if service == serviceSupported {
                                if let crmRaisedService =  ClaimHomeServe.shared.assetServiceType, service.service == crmRaisedService {
                                    section.isSelected = true
                                }
                                enabled = true
                                break
                            }
                        }
                        
                        section.isEnabled = enabled
                    }
                    
                    section.type = service.service
                    
                    // Check if restricted PMS & PMS Assets are not boarded then disable service
                    if ((service.isRestrictedPMS ?? false) && (service.assets?.count ?? 0) == 0) || !(service.isServiceTenureActive ?? true){
                        section.subtitle = !(service.isServiceTenureActive ?? true) ? Strings.ServiceRequest.WhatHappened.isServiceTenureActiveText : Strings.ServiceRequest.WhatHappened.noBoardedAssets
                        section.isEnabled = false
                    }
                    
                    arrTableView.append(section)
                }
            }
        }
        
        return (arrTableView,countUnavailableService == 0 ? 0 : countSupportedRequestType)
    }
    
    func createIssueList(response: WhatHappendResponseDTO , preselectedIssueId : NSNumber?) -> [WhatHappenSection] {
        var arrTableView = [WhatHappenSection]()
        if let arrData = response.data {
            for model in arrData {
                let section = WhatHappenSection()
                section.title = model.taskDescription ?? model.taskName ;
//                section.type = .extendedWarranty
//                let row = WhatHappenRowModel()
//                row.imageName = "dot"
//                row.title = model.taskDescription
//                section.rows = [row]
                section.issueId = model.serviceTaskId
                if section.issueId?.intValue == preselectedIssueId?.intValue {
                    section.isSelected = true
                }
                arrTableView.append(section)
            }
        }
        return arrTableView
        
        
    }
    
    func getSection(title: String?, subtitle: String? = nil, rows: [String]?) -> WhatHappenSection {
        let section = WhatHappenSection()
        section.title = title
        section.subtitle = subtitle
        
        section.rows = rows?.compactMap({ (row) -> WhatHappenRowModel? in
            let rowStolen = WhatHappenRowModel()
            rowStolen.title = row
            rowStolen.imageName = "dot"
            return rowStolen
        }) ?? []
        
        section.isSelected = false
        return section
    }
}
