//
//  WhatHappenedInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WhatHappenedPresentationLogic {
    func presentData(category: Constants.Services)
    func presentData(_ response: WhatHappendResponseDTO?, error: Error?, request: WhatHappendRequestDTO , preselectedIssueId : NSNumber?)
    

}

class WhatHappenedInteractor: BaseInteractor, WhatHappenedBusinessLogic {
    var presenter: WhatHappenedPresentationLogic?
    
    // MARK: Business Logic Conformance
    
    func  getApplianceIssue(productCode : String , taskType :String , preselectedIssueId : NSNumber?, productVariantId: NSNumber?){
        let request = WhatHappendRequestDTO()
        request.productCode = productCode
        request.taskType = taskType
        request.productVariantId = productVariantId

        let _  = WhatHappendRequestUseCase.service(requestDTO:request){
            [weak self] (service,response,error) in
            self?.presenter?.presentData(response,error:error,request:request,preselectedIssueId : preselectedIssueId)
        }
    }
    
    func getAvailableServiceTypes(category: Constants.Services)  {
        presenter?.presentData(category: category)
    }
}
