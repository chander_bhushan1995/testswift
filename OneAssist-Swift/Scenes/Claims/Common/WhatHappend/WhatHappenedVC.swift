
//
//  WhatHappenedVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WhatHappenedBusinessLogic {
    
    func getAvailableServiceTypes(category: Constants.Services)
    func getApplianceIssue(productCode : String , taskType :String , preselectedIssueId : NSNumber?, productVariantId: NSNumber?)
}

class WhatHappenSection :TableViewSection<WhatHappenRowModel>{
    var type: Constants.Services!
    var issueId : NSNumber?
    var damageType: String?
    var partnerCode: NSNumber?
}

/// This delegate used to give callback for button clicked on What happened screen
protocol WhatHappenedVCDelegate:class {
    
    /// Clicked on submit button on what happened screen
    ///
    /// - Parameters:
    ///   - model: what happened section model
    ///   - category: category
    func clickSubmitButton(model:WhatHappenSection, category: Constants.Services)
    
    /// clicked on what happened button
    ///
    /// - Parameter category: category
    func whatHappenedButtonTapped(category: Constants.Services)
}
class WhatHappenRowModel:Row{
   
}

class WhatHappenedVC: BaseVC ,PagingProtocol{
    
    var interactor: WhatHappenedBusinessLogic?
   
    @IBOutlet weak var txtViewSubtitle: LinkTextView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: AutoresizingTableView!
    @IBOutlet weak var selectAppliancesButton: ArrowButton!
    var currentPage: Int = 0
    var preSelectedIndex:IndexPath?
    var preSelectedIssueId : NSNumber?
    var delegate:WhatHappenedVCDelegate?
    var category: Constants.Services!
    var productCode : String?
    var categoryText:String = ""
    var productVariantId: NSNumber?
    fileprivate var previousProductCode: String?
    var dataSource = TableViewDataSource<WhatHappenRowModel, WhatHappenSection>()
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //selectAppliancesButton.isUserInteractionEnabled = true
        refreshData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // selectAppliancesButton.isUserInteractionEnabled = true
    }
    fileprivate func refreshData() {
        if let productCode = productCode {
            if productCode != previousProductCode {
                selectAppliancesButton.isEnabled = false
                Utilities.addLoader(onView: view, message: "Please wait", count: &loaderCount, isInteractionEnabled: true)
                EventTracking.shared.addScreenTracking(with: .SR_Issue)
                self.interactor?.getApplianceIssue(productCode : productCode , taskType :"ISSUE" ,preselectedIssueId: preSelectedIssueId, productVariantId: productVariantId)
            }
        } else {
            
            if self.dataSource.numberOfSections == 0 {
                interactor?.getAvailableServiceTypes(category: category)
            }
        }
    }
    
    // MARK:- private methods
    private func initializeView() {
        //self.arrTableView = createArrayForTableView()
        self.title = Strings.ServiceRequest.Titles.raiseARequest
        if category.isPEClaimType() {
            self.lblTitle.text = Strings.ServiceRequest.whatHappendToDevice
        } else {
            if (self.productCode != nil){
                self.lblTitle.text = Strings.ServiceRequest.pleaseSelectaReason
            } else {
                self.lblTitle.text = Strings.ServiceRequest.selectServiceType
            }
        }
        
        lblTitle.font  = DLSFont.h3.bold
        lblTitle.textColor = UIColor.charcoalGrey
        tableView.register(nib: Constants.NIBNames.WHCService.whatHappenedCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.whatHappenedCell)
        tableView.separatorStyle = .none
        tableView.estimatedSectionHeaderHeight = CGFloat(50)
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        self.selectAppliancesButton.isEnabled = false
        setTitle(category: self.category)
        
        if productCode != nil {
            EventTracking.shared.addScreenTracking(with: .SR_Issue)
        } else {
            EventTracking.shared.addScreenTracking(with: .SR_ServiceType)
        }
        
    }
    
    func setTitle(category: Constants.Services) {
        
        if category.getCategoryType() == Constants.Category.personalElectronics {
            selectAppliancesButton.setTitle("How did this happen", for: .normal)
        } else if category.isEWClaimType() {
            selectAppliancesButton.setTitle("Tell us more", for: .normal)
        } else if category.getCategoryType() == Constants.Category.homeAppliances {
            switch category {
            case .breakdown:
                
                if Utilities.isSOPServiceAvailable(from: ClaimHomeServe.shared.totalServices) {
                    selectAppliancesButton.setTitle(Strings.ScheduleVisitScene.scheduleAction, for: .normal)
                } else {
                    selectAppliancesButton.setTitle("Tell us more", for: .normal)
                }
                categoryText = "BreakDown"
            case .accidentalDamage:
//                selectAppliancesButton.setTitle("How did this happen", for: .normal)
                selectAppliancesButton.setTitle("Select Appliance", for: .normal)
                categoryText = "AD"
            case .buglary:
                selectAppliancesButton.setTitle("Select Appliance", for: .normal)
                categoryText = "Bulgary"
            case .fire:
                selectAppliancesButton.setTitle("Select Appliance", for: .normal)
                categoryText = "Fire"
            default:
                selectAppliancesButton.setTitle("Select Appliance", for: .normal)
            }
        }
    }
    // MARK:- Action Methods
    
    @IBAction func clickApplianceButton(_ sender: Any) {
       
       
        let selectedModel = dataSource.tableSections.filter ({ $0.isSelected }).first
        if selectedModel != nil{
            self.delegate?.clickSubmitButton(model: selectedModel!, category: selectedModel?.type ?? category) // we are passing selected service type
            
            if productCode != nil {
                EventTracking.shared.eventTracking(name: .SRIssueSubmit , [.issueName: selectedModel?.title ?? "", .appliancesName: CacheManager.shared.eventProductName ?? "", .service: category.rawValue])
            } else {
                CacheManager.shared.srType = category
                EventTracking.shared.eventTracking(name: .SRServiceTypeSubmit , [.service: category.rawValue, .category: category.getCategoryType(), .subcategory: CacheManager.shared.eventProductName ?? ""])
            }
            
        }
    }
}


// MARK:- Configuration Logic
extension WhatHappenedVC {
        fileprivate func setupDependencyConfigurator() {
        let interactor = WhatHappenedInteractor()
        let presenter = WhatHappenedPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.delegate = self
    }
}

// MARK:- Routing Logic
extension WhatHappenedVC {
    
}


extension WhatHappenedVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionTableView = dataSource.sectionModel(for: section)
        let view = Bundle.main.loadNibNamed(ServiceRequestWhatHappenedHeaderView.nibName, owner: self, options: nil)?[0] as? ServiceRequestWhatHappenedHeaderView
        view?.setModel(title: sectionTableView.title ?? "", subtitle: sectionTableView.subtitle)
        view?.setUI(sectionTableView.isSelected)
        if sectionTableView.isEnabled {
            view?.viewMask.isUserInteractionEnabled = false
            view?.viewMask.backgroundColor = UIColor.clear
        }else {
            view?.viewMask.isUserInteractionEnabled = true
            view?.viewMask.backgroundColor = UIColor.init(white: 1.0, alpha: 0.5)
        }
        view?.section = section
        view?.delegate = self
        return view
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
}

extension WhatHappenedVC : ServiceRequestWhatHappenedHeaderViewDelegate {
    func clickBtn(headerView : ServiceRequestWhatHappenedHeaderView){
        var i = 0
        for section in dataSource.tableSections {
            if i == headerView.section {
                section.isSelected = !section.isSelected
                self.selectAppliancesButton.isEnabled = section.isSelected ? true : false
                if selectAppliancesButton.isEnabled {
                    self.delegate?.whatHappenedButtonTapped(category: section.type ?? category)
                }
            } else {
                section.isSelected = false
            }
            i = i + 1;
        }
        tableView.reloadData()
    }
}

extension WhatHappenedVC:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
          let tableSection = dataSource.sectionModel(for: section)
                if tableSection.isSelected == false {
                    return 0
                }
        return dataSource.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:WhatHappenedCell = tableView.dequeueReusableCell(indexPath: indexPath)
        let row = dataSource.rowModel(for: indexPath)
        cell.setModel(title: row.title, imageName: row.imageName)
        cell.selectionStyle = .none
        return cell
    }
}


// MARK:- Display Logic Conformance
extension WhatHappenedVC: WhatHappenedPresenterDisplayDelegate {
    func displayLogic(dataSource:[WhatHappenSection],unavailableService : Int){
       
        previousProductCode = productCode
        let tableViewModel = TableViewDataSource<WhatHappenRowModel, WhatHappenSection>()
        tableViewModel.tableSections = dataSource
        if unavailableService == 0 {
            
            if self.productCode != nil{
                let subtitleTxletewText: NSAttributedString = {
                    let atrStr = NSMutableAttributedString(string: Strings.ServiceRequest.whatHappedSubTitle, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                    return atrStr
                } ()
                txtViewSubtitle.attributedText = subtitleTxletewText
            } else {
                let subtitleTxletewText: NSAttributedString = {
                    let atrStr = NSMutableAttributedString(string: Strings.ServiceRequest.help, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                    return atrStr
                } ()
                txtViewSubtitle.attributedText = subtitleTxletewText
            }
        } else {
            let subtitleTxtViewText: NSAttributedString = {
                let atrStr = NSMutableAttributedString(string: "Only \(unavailableService) services are covered under pincode(\(ClaimHomeServe.shared.pinCode ?? "")). For Further help, ", attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                let atrStr3 = NSAttributedString(string: "chat with us.", attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.dodgerBlue])
                
                atrStr.append(atrStr3)
                return atrStr
            } ()
            txtViewSubtitle.setAttributedLink(completeText: subtitleTxtViewText, linkText: "chat with us")
            txtViewSubtitle.linkDelegate = self
        }
        
        
       
        self.dataSource = tableViewModel
        tableView.reloadData()
        
        for section in dataSource {
            if section.isSelected {
                self.selectAppliancesButton.isEnabled = true
                self.delegate?.whatHappenedButtonTapped(category : section.type ?? category)

                break
            }
        }
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, primaryAction: {
            self.refreshData()
        }) { }
    }
}

extension WhatHappenedVC: LinkTextViewDelegate {
    func linkClicked(linkText: String) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.chatWithUs.message, subTitle:nil, forScreen: nil) {[unowned self] (status) in
            if status {
                EventTracking.shared.eventTracking(name: .chatTapped)
                EventTracking.shared.eventTracking(name: .chatOnHomeTapped)
                self.present(ChatVC(), animated: true, completion: nil)
            }
        }
    }
}

