//
//  AwaitingSurveyerCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/5/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol AwaitingSurveyerCellDelegate: class {
    func awaitingSurveyerCell(_ cell: AwaitingSurveyerCell, clickedBtnCall sender: Any)
}

class AwaitingSurveyerCellViewModel {
    var imageUrl: String!
    var products: [String]!
    var serviceId: String!
    var raisedDate: String!
    var infoText: String?
    var contactNumber: String!
}

class AwaitingSurveyerCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var serviceIdLabel: UILabel!
    @IBOutlet weak var raisedDateLabel: UILabel!
    @IBOutlet weak var infoTextLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet var moreButton: UIButton!
    
    weak var delegate: AwaitingSurveyerCellDelegate?
    
    var model: AwaitingSurveyerCellViewModel! {
        didSet {
            setViewModel(model)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        containerView.layer.cornerRadius = 6.0
        containerView.layer.borderColor = UIColor.gray2.cgColor
        containerView.layer.borderWidth = 1.0
        contactLabel.text = RemoteConfigManager.shared.contactNumberCustomerCare
    }
    
    private func setViewModel(_ model: AwaitingSurveyerCellViewModel) {
        
        productImageView.setImageWithUrlString(model.imageUrl, placeholderImage: #imageLiteral(resourceName: "loudspeaker_img"))
        
        if model.products.count > 1 {
            titleLabel.text = "\(model.products[0]), \(model.products[1].first?.description.uppercased() ?? "")..."
        } else {
            titleLabel.text = model.products[0]
            moreButton.removeFromSuperview()
        }
        
        serviceIdLabel.text = "Service ID \(model.serviceId)"
        raisedDateLabel.text = "Raised on \(model.raisedDate)"
        
        if let info = model.infoText {
            infoTextLabel.text = info
        }
        
        contactLabel.text = model.contactNumber
    }
    
    @IBAction func clickedMoreButton(_ sender: Any) {
        moreButton.removeFromSuperview()
        titleLabel.text = model.products.joined(separator: ", ")
        layoutIfNeeded()
    }
    
    @IBAction func clickedCallButton(_ sender: Any) {
        delegate?.awaitingSurveyerCell(self, clickedBtnCall: sender)
    }
}
