//
//  RepairAssessmentCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/5/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This Delegate contains callback method of Repair Assessment cell button.
protocol RepairAssessmentCellDelegate: ExpandViewDelegate {
    
    /// Clicked on Repair Assessment's primary button
    ///
    /// - Parameters:
    ///   - cell: Cell object which tapped
    ///   - sender: Button object
    func repairAssessmentCell(_ cell: RepairAssessmentCell, clickedBtnPrimary sender: Any,indexPath:IndexPath)
    func clickedPayLater(indexPath:IndexPath)
}

/// Used To view Repair assessment cell
class RepairAssessmentCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var secondButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var expandView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: OABadgeLabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var primaryButton: OAPrimaryButton!
    
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var primaryButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var primaryButtonHeightConstraint: NSLayoutConstraint!
    var adviceID:String?
     var delegate:  RepairAssessmentCellDelegate?
    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    /// Used to initialize cell view
    private func initializeView() {
//        containerView.layer.cornerRadius = 6.0
//        containerView.layer.borderColor = UIColor.gray2.cgColor
//        containerView.layer.borderWidth = 1.0
        expandView.isHidden = true
//        badgeView.layer.cornerRadius = 2.0
//        badgeView.layer.borderWidth = 1.0
    }
    
    /// Used to update view for that cell according to data
    ///
    /// - Parameters:
    ///   - badgeText: Badge text i.e. Cancelled/Rejected/On hold etc
    ///   - badgeBackground: Badge background color
    ///   - badgeBorderColor: Badge border color
    ///   - badgeTextColor: Badge text color
    ///   - titleText: Heading text in Bold
    ///   - descriptionText: Description text
    ///   - buttonTitle: Button title
    ///   - isButtonEnabled: true if button enable else false
    func setViewModel(_ model:TimeLineViewModel.RepairAssessmentCellModel, _ delegate: RepairAssessmentCellDelegate?){
        
        self.delegate = delegate
        self.indexPath = model.indexPath
        
        if let text = model.badgeText {
            // If badge is to be shown
            badgeView.isHidden = false
            badgeLabel.isHidden = false
            badgeLabel.text = text

        } else {
            // Badge hidden, adjust constraitn
            badgeView.isHidden = true
            badgeLabel.isHidden = true

        }
        
        if let text = model.titleText {
            titleLabel.isHidden = false
            titleLabel.attributedText = text
        } else {
            // Title hidden, adjust constraitn
            titleLabel.isHidden = true
        }
        
        if let text = model.descriptionText {
            descriptionLabel.isHidden = false
            descriptionLabel.attributedText = text
        } else {
            // Description hidden, adjust constraitn
            descriptionLabel.isHidden = true
        }
        
        if descriptionLabel.isHidden,
            titleLabel.isHidden,
            badgeView.isHidden  {
            self.stackViewTopConstraint.constant = 0.0
        }else {
            self.stackViewTopConstraint.constant = 16.0
        }
        
        if let text = model.buttonTitle {
            primaryButton.isHidden = false
            primaryButtonTopConstraint.constant = 15.0
            primaryButtonHeightConstraint.constant = 44.0
            
            primaryButton.setTitle(text, for: .normal)
            
            primaryButton.isEnabled = model.isButtonEnabled
        } else {
            // Button hidden, adjust constraitn
            primaryButton.isHidden = true
            primaryButtonTopConstraint.constant = 0
            primaryButtonHeightConstraint.constant = 0
        }
        if let secondBtnName = model.secondBtnName{
            secondButton.setTitle(secondBtnName, for: .normal)
            secondButton.isEnabled = model.secondBtnEnable ?? false
        }else{
            secondButtonHeightConstraint.constant = 0
            secondButton.isHidden = true
        }
        addPartList(partList: model.partList ?? [ExpandModel]())
    }

    private func addPartList(partList: [ExpandModel]) {
       if partList.count != 0 {
            self.expandView.isHidden = false
        }
        // check if view is already added
        guard expandView.subviews.count == 0 else {
            return
        }
        
        //TODO: Remove height constraint from refund view
        // Add List view
        let partView = Bundle.main.loadNibNamed("ExpandView", owner: nil, options: nil)?.first as! ExpandView
        
        partView.dataSource = partList
        partView.updateUI()
        partView.sectionDelegate = delegate
        expandView.addSubview(partView)
        expandView.frame = partView.frame
        
        // Add constraints
        partView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.init(item: partView, attribute: .top, relatedBy: .equal, toItem: expandView, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint.init(item: partView, attribute: .bottom, relatedBy: .equal, toItem: expandView, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint.init(item: partView, attribute: .leading, relatedBy: .equal, toItem: expandView, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint.init(item: partView, attribute: .trailing, relatedBy: .equal, toItem: expandView, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        expandView.needsUpdateConstraints()
        
    }
    @IBAction func clickedSecondBtn(_ sender: Any) {
      delegate?.clickedPayLater(indexPath: indexPath)
    }
    
    /// Clicked primary button
    @IBAction func clickedBtnPrimary(_ sender: Any) {
        delegate?.repairAssessmentCell(self, clickedBtnPrimary: sender,indexPath: self.indexPath)
    }
}
