//
//  DocumentPendencyVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/29/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol DocumentPendencyBusinessLogic {
    func getModels(_ pendency: SearchService?)
    func uploadClaimInvoice(srId: String, docModel: UploadDocumentsViewModel.UploadCellModel)
    func updateSR(srId: String, incidentDate: Date?, description: String?, srType: String?)
}

extension DocumentPendencyBusinessLogic {
    func updateSR(srId: String, incidentDate: Date?, description: String?) {
        updateSR(srId: srId, incidentDate: incidentDate, description: description, srType: "")
    }
}

protocol DocumentPendencyVCDelegate: class {
    func pendencySubmitted()
}

class DocumentPendencyVC: BaseVC {
    var interactor: DocumentPendencyBusinessLogic?
    
    @IBOutlet weak var topHeaderView: UIView!
    @IBOutlet weak var topHeaderLabel: H3BoldLabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitButton: OAPrimaryButton!
    
    var pendency: SearchService?
    var srId: String!
    
    weak var delegate: DocumentPendencyVCDelegate?
    
    fileprivate var selectedDate: Date?
    fileprivate var editedDescription: String?
    fileprivate var dataSource: TableViewDataSource<Row, TableViewSection<Row>>!
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        tableView.register(cell: UploadDocumentsCell.self)
        tableView.register(cell: EditDescriptionCell.self)
        tableView.register(cell: TimeOfIncidentCell.self)
        
        tableView.estimatedSectionHeaderHeight = 0
        tableView.sectionHeaderHeight = 0
        
        addTap()
        title = "Submit Details"
        topHeaderView.isHidden = true
        submitButton.isEnabled = false
        
        interactor?.getModels(pendency)
    }
    
    func toggleSubmitButton() {
        for section in dataSource.tableSections {
            for row in section.rows {
                if let row = row as? UploadDocumentsViewModel.UploadCellModel, !row.isUploaded {
                    submitButton.isEnabled = false
                    return
                } else if let _ = row as? EditDescriptionViewModel, editedDescription == nil {
                    submitButton.isEnabled = false
                    return
                } else if let _ = row as? TimeOfIncidentCellViewModel, selectedDate == nil {
                    submitButton.isEnabled = false
                    return
                }
            }
        }
        
        submitButton.isEnabled = true
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnSubmit(_ sender: Any) {
        // event
        Utilities.addLoader(onView: view, message: "updating service request...", count: &loaderCount, isInteractionEnabled: false)
        interactor?.updateSR(srId: srId, incidentDate: selectedDate, description: editedDescription, srType: pendency?.serviceRequestType)
        
//        EventTracking.shared.eventTracking(name: .SRDetailSubmit)

        EventTracking.shared.eventTracking(name: .srDetailSubmit, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])

    }
}

// MARK:- Display Logic Conformance
extension DocumentPendencyVC: DocumentPendencyDisplayLogic {
    func displayPendency(_ header: NSAttributedString?, _ viewModel: TableViewDataSource<Row, TableViewSection<Row>>) {
        if header == nil {
            topHeaderView.isHidden = true
        } else {
            topHeaderLabel.attributedText = header
            topHeaderView.isHidden = false
        }
        
        self.dataSource = viewModel
        
        toggleSubmitButton()
        tableView.reloadData()
    }
    
    func displayUploadSuccess(for model: UploadDocumentsViewModel.UploadCellModel) {
        model.isUploading = false
        model.isUploaded = true
        
        toggleSubmitButton()
        tableView.reloadData()
    }
    
    func displayUploadError(error: String, for model: UploadDocumentsViewModel.UploadCellModel) {
        model.isUploading = false
        model.isUploaded = false
        
        toggleSubmitButton()
        tableView.reloadData()
        
        showAlert(message: error, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, primaryAction: {
            model.isUploading = true
            self.tableView.reloadData()
            self.interactor?.uploadClaimInvoice(srId: self.srId, docModel: model)
        }) { }
    }
    
    func displayUpdateSRError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayUpdateSRSuccess(response: ClaimNotifyAllDocumentUploadedResponseDTO?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        delegate?.pendencySubmitted()
        navigationController?.popViewController(animated: true)
    }
}

// MARK:- Configuration Logic
extension DocumentPendencyVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = DocumentPendencyInteractor()
        let presenter = DocumentPendencyPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension DocumentPendencyVC {
    
}

extension DocumentPendencyVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource.rowModel(for: indexPath)
        
        if let model = model as? UploadDocumentsViewModel.UploadCellModel {
            let cell: UploadDocumentsCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.updateUserInterface(title: model.title, subtitle: model.description ?? "", image: model.image, isUploaded: model.isUploaded, margin: 23, isUploading: model.isUploading)
            cell.delegate = self
            return cell
        } else if let model = model as? EditDescriptionViewModel {
            let cell: EditDescriptionCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.set(viewModel: model)
            cell.delegate = self
            return cell
        } else if let model = model as? TimeOfIncidentCellViewModel {
            let cell: TimeOfIncidentCell = tableView.dequeueReusableCell(indexPath: indexPath)
            cell.set(viewModel: model)
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
    }
}

extension DocumentPendencyVC: TimeOfIncidentCellDelegate {
    func dateChanged(_ date: Date, cell: TimeOfIncidentCell) {
        
        selectedDate = date
        if let indexPath = tableView.indexPath(for: cell) {
            if let model = dataSource.rowModel(for: indexPath) as? TimeOfIncidentCellViewModel {
                model.selectedDate = selectedDate
            }
        }
        toggleSubmitButton()
    }
    
    func dateClicked(cell: TimeOfIncidentCell) -> Bool {
//        if selectedDate == nil {
//            dateChanged(Date(), cell: cell)
//            return true
//        }
        return false
    }
}

extension DocumentPendencyVC: EditDescriptionCellDelegate {
    func editDescriptionCell(_ cell: EditDescriptionCell, clickedBtnEditDescription sender: Any) {
        let editDescriptionVC = EditDescriptionVC()
        editDescriptionVC.pendencyCellPath = tableView.indexPath(for: cell)
        editDescriptionVC.prevDescription = editedDescription ?? pendency?.requestDescription
        editDescriptionVC.delegate = self
        navigationController?.pushViewController(editDescriptionVC, animated: true)
    }
}

extension DocumentPendencyVC: EditDescriptionVCDelegate {
    func descriptionAdded(_ description: String, indexPath: IndexPath?) {
        editedDescription = description
        if let path = indexPath, let model = dataSource.rowModel(for: path) as? EditDescriptionViewModel {
            model.descriptionText = editedDescription
            tableView.reloadRows(at: [path], with: .none)
        }
        toggleSubmitButton()
    }
}

extension DocumentPendencyVC: UploadDocumentsCellDelegate {
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clickedBtnSelect sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            pickImage(for: indexPath)
        }
    }
    
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clikedBtnEdit sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            pickImage(for: indexPath)
        }
    }
    
    private func pickImage(for indexPath: IndexPath) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: Strings.Global.uploadImage, style: .default) { (action) in
            ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(for: indexPath, image, error)
            })
        }
        
        let action2 = UIAlertAction(title: Strings.Global.clickPhoto, style: .default) { (action2) in
            ImagePickerManager.shared.pickImageFromCamera(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(for: indexPath, image, error)
            })
        }
        
        let action3 = UIAlertAction(title: Strings.Global.cancel, style: .cancel) { (action3) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action)
        alertController.addAction(action2)
        alertController.addAction(action3)
        presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    private func handlePickedImage(for indexPath: IndexPath, _ image: UIImage?, _ error: Error?) {
        if let error = error {
            print(error)
        } else {
            if let model = dataSource.rowModel(for: indexPath) as? UploadDocumentsViewModel.UploadCellModel {
                model.image = image
                model.isUploading = true
                tableView.reloadRows(at: [indexPath], with: .none)
                interactor?.uploadClaimInvoice(srId: srId, docModel: model)
            }
        }
    }
}
