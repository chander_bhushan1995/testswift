//
//  DocumentPendencyInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/29/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol DocumentPendencyPresentationLogic {
    func presentModels(_ pendency: SearchService?)
    func uploadClaimResponseRecieved(response: UploadClaimInvoiceResponseDTO?, error: Error?, for model: UploadDocumentsViewModel.UploadCellModel)
    func updateSRResponseRecieved(response: ClaimNotifyAllDocumentUploadedResponseDTO?, error: Error?)
}

class DocumentPendencyInteractor: BaseInteractor, DocumentPendencyBusinessLogic {
    var presenter: DocumentPendencyPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getModels(_ pendency: SearchService?) {
        presenter?.presentModels(pendency)
    }
    
    func uploadClaimInvoice(srId: String, docModel: UploadDocumentsViewModel.UploadCellModel) {
        let req = UploadClaimInvoiceRequestDTO(serviceRequestId: srId, documentTypeId: "\(docModel.docTypeId ?? 0)", documentId: docModel.docId, file: docModel.image?.jpegData(compressionQuality: 0.9))
        let _ = UploadClaimInvoiceRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.uploadClaimResponseRecieved(response: response, error: error, for: docModel)
        }
    }
    
    func updateSR(srId: String, incidentDate: Date?, description: String?, srType: String? = "") {
        let req = ClaimNotifyAllDocumentUploadedRequestDTO()
        req.serviceID = srId
        req.srType = srType
        req.dateOfIncident = incidentDate?.string(with: .scheduleFormat)
        req.requestDescription = description
        req.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "test"
        
        let _ = ClaimNotifyAllDocumentUploadedRequestUseCase.service(requestDTO: req) { (usecase, response, error) in
            self.presenter?.updateSRResponseRecieved(response: response, error: error)
        }
    }
}
