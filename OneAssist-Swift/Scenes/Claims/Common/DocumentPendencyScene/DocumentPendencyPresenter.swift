//
//  DocumentPendencyPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/29/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol DocumentPendencyDisplayLogic: class {
    func displayPendency(_ header: NSAttributedString?, _ viewModel: TableViewDataSource<Row, TableViewSection<Row>>)
    func displayUploadError(error: String, for model: UploadDocumentsViewModel.UploadCellModel)
    func displayUploadSuccess(for model: UploadDocumentsViewModel.UploadCellModel)
    func displayUpdateSRSuccess(response: ClaimNotifyAllDocumentUploadedResponseDTO?)
    func displayUpdateSRError(error: String)
}

class DocumentPendencyPresenter: BasePresenter, DocumentPendencyPresentationLogic {
    weak var viewController: DocumentPendencyDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func presentModels(_ pendency: SearchService?) {
        
        var titleText: NSAttributedString?
        
        var rows: [Row] = []
        
        if let pendingDocs = pendency?.pendency?.documentUpload?.documents {
            titleText = "Please re-upload and submit the following document within 2 days to continue".getAttributedString(bodyColor: .black, bodyFont: DLSFont.h3.bold, attributedParts: ["within 2 days"], attributedTextColors: [.orangePeel], attributedFonts: [DLSFont.h3.bold])
            
            for docDetail in pendingDocs {
                
                guard docDetail.documentStatus?.status == Constants.PendencyStatus.incomplete else {
                    continue
                }
                let docCell = UploadDocumentsViewModel.UploadCellModel()
                
                docCell.title = docDetail.documentName ?? ""
                docCell.description = "Please make sure image is not blurry"
                if let documentKey = docDetail.documentKey {
                    if documentKey == "INVOICE_IMAGE" {
                        docCell.title = "Appliance Purchase Invoice"
                        docCell.description = "Make sure invoice has appliance details, buyer’s name and is dully stamped (if purchased offline)"
                    }else if documentKey == "GIFTER_ID_PROOF" {
                        docCell.title = "Government ID of Gifter"
                        docCell.description = "Upload any one: Aadhar card, PAN Card, Driving License or Passport, Voter ID"
                    }else if documentKey == "ADDITIONAL_DOCUMENT1" {
                        docCell.title = "Any Additional Document"
                        docCell.description = "Provide any extra document supporting your claim"
                    }
                }
                
                docCell.docId = docDetail.documentId
                docCell.docTypeId = Int(docDetail.documentTypeId ?? "")
                
                rows.append(docCell)
            }
        }
        
        if pendency?.pendency?.documentUpload?.incidenceDate?.status == Constants.PendencyStatus.incomplete {
            let timeCell = TimeOfIncidentCellViewModel()
            timeCell.titleText = "Edit date of incident"
            if let startDate = rootTabVC?.customerMembershipResponse?.data?.memberships?.filter({$0.memId?.description == pendency?.referenceNo}).first?.startDate, let startDateObj = Date(string: startDate, formatter: DateFormatter.serverDateFormat) {
                
                if let createdDate = Date(string: pendency?.createdOn, formatter: .scheduleFormat) {
                    if let minAllowed =  gregorianCalendar.date(byAdding: .day, value: -7, to: createdDate) {
                        if minAllowed < startDateObj {
                            timeCell.allowedMinDate = startDateObj
                        } else {
                            timeCell.allowedMinDate = minAllowed
                        }
                    }
                    timeCell.allowedMaxDate = createdDate
                }
                
            }
            
            timeCell.selectedDate = Date(string: pendency?.dateOfIncident, formatter: .scheduleFormat)
            
            rows.append(timeCell)
        }
        
        if pendency?.pendency?.documentUpload?.incidenceDescription?.status == Constants.PendencyStatus.incomplete, let _ = pendency?.pendency?.documentUpload?.incidenceDescription {
            let descriptionCell = EditDescriptionViewModel()
            descriptionCell.headerText = "Please edit the previously given incident description"
            descriptionCell.descriptionText = pendency?.requestDescription
            
            rows.append(descriptionCell)
        }
        
        let section = TableViewSection<Row>()
        section.rows = rows
        
        let dataSource = TableViewDataSource<Row, TableViewSection<Row>>()
        dataSource.tableSections = [section]
        
        viewController?.displayPendency(titleText, dataSource)
    }
    
    func uploadClaimResponseRecieved(response: UploadClaimInvoiceResponseDTO?, error: Error?, for model: UploadDocumentsViewModel.UploadCellModel) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUploadSuccess(for: model)
        } catch {
            viewController?.displayUploadError(error: error.localizedDescription, for: model)
        }
    }
    
    func updateSRResponseRecieved(response: ClaimNotifyAllDocumentUploadedResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayUpdateSRSuccess(response: response)
        } catch {
            viewController?.displayUpdateSRError(error: error.localizedDescription)
        }
    }
}
