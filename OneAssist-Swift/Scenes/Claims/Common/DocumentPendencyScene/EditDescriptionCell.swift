//
//  EditDescriptionCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/29/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class EditDescriptionViewModel: Row {
    var headerText: String?
    var descriptionText: String?
}

protocol EditDescriptionCellDelegate: class {
    func editDescriptionCell(_ cell: EditDescriptionCell, clickedBtnEditDescription sender: Any)
}

class EditDescriptionCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var headerlabel: H3BoldLabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    
    weak var delegate: EditDescriptionCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        containerView.layer.cornerRadius = 2.0
    }
    
    func set(viewModel model: EditDescriptionViewModel) {
        headerlabel.text = model.headerText
        descriptionLabel.text = model.descriptionText
    }
    
    @IBAction func clickedBtnEditDescription(_ sender: Any) {
        delegate?.editDescriptionCell(self, clickedBtnEditDescription: sender)
    }
}
