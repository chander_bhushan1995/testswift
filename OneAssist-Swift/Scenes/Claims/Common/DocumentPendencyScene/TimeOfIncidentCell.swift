//
//  TimeOfIncidentCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/29/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class TimeOfIncidentCellViewModel: Row {
    var titleText: String?
    var selectedDate: Date?
    var allowedMaxDate: Date?
    var allowedMinDate: Date?
}

protocol TimeOfIncidentCellDelegate: class {
    func dateChanged(_ date: Date, cell: TimeOfIncidentCell)
//    func dateClicked(cell: TimeOfIncidentCell) -> Bool
}

class TimeOfIncidentCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    weak var delegate: TimeOfIncidentCellDelegate?
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var calendarView: OACalendarView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        calendarView.delegate = self
    }
    
    func set(viewModel model: TimeOfIncidentCellViewModel) {
        setUpBoundaryDate(model: model)
        titleLbl.text = model.titleText
        calendarView.setInitialDate(date: model.selectedDate)
    }
}

extension TimeOfIncidentCell: OACalendarViewDelegate{
    func dateChanged(_ date: Date, view: OACalendarView) {
        delegate?.dateChanged(date, cell: self)
    }
}

extension TimeOfIncidentCell {
    fileprivate func setUpBoundaryDate(model: TimeOfIncidentCellViewModel){
        calendarView.maximumDate = model.allowedMaxDate
        calendarView.minimumDate = model.allowedMinDate
        calendarView.date = Date()
        calendarView.dateFooterNote = Strings.ServiceRequest.WhenHappend.incidentDataCondition
    }
}
