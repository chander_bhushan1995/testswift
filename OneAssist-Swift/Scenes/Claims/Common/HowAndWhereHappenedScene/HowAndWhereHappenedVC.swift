//
//  HowAndWhereHappenedVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 19/02/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol HowAndWhereHappenedBusinessLogic {
    
}

class HowAndWhereHappenedVC: IncidentDescriptionBaseVC {
    
    var interactor: HowAndWhereHappenedBusinessLogic?    
    
    convenience init() {
        self.init(nibName: "IncidentDescriptionBaseVC", bundle: nil)
    }
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        EventTracking.shared.addScreenTracking(with: .SR_Decription)
    }
    
    // MARK:- private methods
    private func initializeView() {
    }
    
    // MARK:- Action Methods
    
}

// MARK:- Display Logic Conformance
extension HowAndWhereHappenedVC: HowAndWhereHappenedDisplayLogic {
    
}

// MARK:- Configuration Logic
extension HowAndWhereHappenedVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = HowAndWhereHappenedInteractor()
        let presenter = HowAndWhereHappenedPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension HowAndWhereHappenedVC {
    
}
