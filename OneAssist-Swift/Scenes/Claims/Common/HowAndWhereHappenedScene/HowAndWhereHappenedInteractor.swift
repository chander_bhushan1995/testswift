//
//  HowAndWhereHappenedInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 19/02/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol HowAndWhereHappenedPresentationLogic {
    
}

class HowAndWhereHappenedInteractor: BaseInteractor, HowAndWhereHappenedBusinessLogic {
    var presenter: HowAndWhereHappenedPresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
