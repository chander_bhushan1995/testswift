//
//  ClaimSuccessInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ClaimSuccessPresentationLogic {
    
}

class ClaimSuccessInteractor: BaseInteractor, ClaimSuccessBusinessLogic {
    var presenter: ClaimSuccessPresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
