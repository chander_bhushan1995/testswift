//
//  ClaimSuccessVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ClaimSuccessBusinessLogic {
    
}

enum ClaimSuccessType {
    case upload
    case srRescheduled
    case visitScheduled
    case srSuccess
    case srSuccessWithVisit
    case srSuccessHAEW
}

class ClaimSuccessViewModel {
    var srId: String?
    var scheduleSlotStartDateTime: String?
    var scheduleSlotEndDateTime: String?
    var successType: ClaimSuccessType = .srSuccess
    var crmTrackingNumber: String?
    var category: Constants.Services!
}

class StatusBarConstraint: NSLayoutConstraint {
    override func awakeFromNib() {
        constant = constant + UIApplication.shared.statusBarFrame.size.height
    }
}

/// This protocol gives callback on Success of claim buttons
protocol ClaimSuccessVCDelegate: class {
    
    /// Cross button clicked
    func clickCrossButton()
    
    /// Clicked on View Service request button
    func clickViewServiceRequest()
}

class ClaimSuccessVC: BaseVC {
    var interactor: ClaimSuccessBusinessLogic?
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var techVisitLabel: UILabel!
    @IBOutlet weak var successView: AllBenefitsView!
    @IBOutlet weak var actionLabel: UIButton!
    
    weak var delegate : ClaimSuccessVCDelegate?
    fileprivate var addToCalendar: Bool?
    
    var successModel: ClaimSuccessViewModel = {
        return ClaimSuccessViewModel()
    }()
    
//    var tvLabelConstraints: [(NSLayoutConstraint,CGFloat)] = []
//    var actionLabelConstraints: [(NSLayoutConstraint,CGFloat)] = []
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        successView.bodyColor = UIColor.forestGreen
        
        let startDate: Date? = DateFormatter.scheduleFormat.date(from: successModel.scheduleSlotStartDateTime ?? "")
        let endDate: Date? = DateFormatter.scheduleFormat.date(from: successModel.scheduleSlotEndDateTime ?? "")
        let scheduledDate: String = DateFormatter.dayMonth.string(from: startDate ?? Date())
        let scheduledStartTime: String = DateFormatter.scheduleTimeFormat.string(from: startDate ?? Date())
        let scheduledEndTime: String = DateFormatter.scheduleTimeFormat.string(from: endDate ?? Date())
        let visitDate = scheduledDate
        let visitTime = "\(scheduledStartTime) - \(scheduledEndTime)"
        
        switch successModel.successType {
        case .upload:
//            if successModel.category is aEnumClaimPersonalElectronics{
//            heading.text = "Great! we are uploading your documents, it may only take a minute."
//            subHeadingLabel.text = "Once uploaded, we will be verifying the documents. The process may take up to 2 working days."
//                actionLabel.setTitle("Okay, got it", for: .normal)
//            }else{
            headingLabel.text = "Your documents have been successfully submitted and are under verification"
            subHeadingLabel.text = "We're verifying your documents. The process may take upto 2 working days"
            actionLabel.setTitle("Okay, got it", for: .normal)
            //}
        case .srRescheduled:
            headingLabel.text = "Service Request rescheduled successfully"
            subHeadingLabel.text = "Technician will visit you on \(visitDate) between \(visitTime)"
            actionLabel.setTitle("Add to calendar", for: .normal)
            addToCalendar = false
        case .visitScheduled:
            headingLabel.text = "Technician visit scheduled successfully"
            subHeadingLabel.text = "Technician will visit you on \(visitDate) between \(visitTime)"
            actionLabel.setTitle("Add to calendar", for: .normal)
            addToCalendar = false
        case .srSuccess:
            headingLabel.text = "Service Request has been received successfully"
            if let crmTrackingNumber = successModel.crmTrackingNumber {
                subHeadingLabel.text = "Your service request ID is \(crmTrackingNumber)"
            }else {
                subHeadingLabel.text = nil
            }
            techVisitLabel.text = "We will appoint a surveyor very soon."
            actionLabel.setTitle("Okay, got it", for: .normal)
        case .srSuccessHAEW:
            headingLabel.text = "We have received your service request"
            subHeadingLabel.text = "On confirmation, we will send an SMS/Email for uploading mandatory documents"
            techVisitLabel.text = ""
            actionLabel.setTitle("Okay, got it", for: .normal)
        case .srSuccessWithVisit:
            headingLabel.text = "Service Request scheduled successfully"
            if let crmTrackingNumber = successModel.crmTrackingNumber {
                subHeadingLabel.text = "Your service request ID is \(crmTrackingNumber)"
            }else {
                subHeadingLabel.text = nil
            }
            techVisitLabel.text = "Technician will visit you on \(visitDate) between \(visitTime)"
            actionLabel.setTitle("Add to calendar", for: .normal)
            addToCalendar = false
        }
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnCross(_ sender: Any) {
       delegate?.clickCrossButton()
    }
    
    @IBAction func clickedBtnAction(_ sender: Any) {
        if let addToCalendar = addToCalendar, !addToCalendar {
            handleCalendarEvent()
            var location = ""
            if let claimType = CacheManager.shared.srType {
//                if claimType is EnumClaimType.EnumHomeServe.EnumWholeHomeCover {
//                    location = "HomeAssist Service"
//                } else if claimType is EnumClaimType.EnumHomeServe.EnumWholeHomeCover {
//                    location = "Home EW Service"
//                } else {
//                    location = (CacheManager.shared.eventProductName ?? "") + "Service"
//                }
                if claimType == .extendedWarranty {
                    location = "Home EW Service"
                } else if claimType.getCategoryType() == Constants.Category.homeAppliances {
                    location = "HomeAssist Service"
                } else {
                    location = (CacheManager.shared.eventProductName ?? "") + "Service"
                }
            }
            EventTracking.shared.eventTracking(name: .SR_calendar ,[.location : location])
        } else {
            handleAction()
        }
    }
    
    func handleCalendarEvent() {
        
        EventTracking.shared.eventTracking(name: .addTocalendarTapped)
        
        let eventManager = EventManager()
        eventManager.delegate = self
        let startTime = DateFormatter.scheduleFormat.date(from: (successModel.scheduleSlotStartDateTime ?? "")) ?? Date()
        eventManager.createEvent(on: startTime, withTitle: "Service Scheduled", forHours: 2)
    }
    
    func handleAction() {
        delegate?.clickViewServiceRequest()
    }
}

extension ClaimSuccessVC: ShowingAlert{
    func showAlert() {
        
        addToCalendar = true
        
        switch successModel.successType {
        case .upload:
            break
        case .srRescheduled:
            successView.setContent(items: ["Added to your calendar"], image: #imageLiteral(resourceName: "tick"), textAlignment: .center)
            actionLabel.isHidden = true
            Utilities.setCalendarEvent(srId: successModel.srId ?? "", scheduleStartDate: successModel.scheduleSlotStartDateTime ?? "")
        case .visitScheduled:
            successView.setContent(items: ["Added to your calendar"], image: #imageLiteral(resourceName: "tick"), textAlignment: .center)
            actionLabel.isHidden = true
            Utilities.setCalendarEvent(srId: successModel.srId ?? "", scheduleStartDate: successModel.scheduleSlotStartDateTime ?? "")
        case .srSuccess: break
        case .srSuccessHAEW:
            break
        case .srSuccessWithVisit:
            successView.setContent(items: ["Added to your calendar"], image: #imageLiteral(resourceName: "tick"), textAlignment: .center)
            actionLabel.isHidden = true
            Utilities.setCalendarEvent(srId: successModel.srId ?? "", scheduleStartDate: successModel.scheduleSlotStartDateTime ?? "")
        }
    }
    
    func showAlertWithAction()
    {
        // FIXME: cancellation reason?
        showAlert(message: Event.EventParams.allowToCreateEvent , primaryButtonTitle: Constants.WHC.alertMessages.settings, Strings.Global.cancel, primaryAction: {
            Utilities.openAppSettings()
        }, {
//
        })
    }
}

// MARK:- Display Logic Conformance
extension ClaimSuccessVC: ClaimSuccessDisplayLogic {
    
}

// MARK:- Configuration Logic
extension ClaimSuccessVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = ClaimSuccessInteractor()
        let presenter = ClaimSuccessPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension ClaimSuccessVC {
    
}
