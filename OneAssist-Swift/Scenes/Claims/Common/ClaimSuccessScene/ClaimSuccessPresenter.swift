//
//  ClaimSuccessPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/03/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ClaimSuccessDisplayLogic: class {
    
}

class ClaimSuccessPresenter: BasePresenter, ClaimSuccessPresentationLogic {
    weak var viewController: ClaimSuccessDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
}
