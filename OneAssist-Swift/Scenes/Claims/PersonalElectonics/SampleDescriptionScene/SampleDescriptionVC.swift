//
//  SampleDescriptionVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/19/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This class used to show sample description screen
class SampleDescriptionVC: BaseVC {

   @IBOutlet private weak var lblTitle: H2RegularLabel!
   @IBOutlet  weak var descTV: UITextView!
    
    var category: Constants.Services! = .accidentalDamage
    var titleText:String?
    var descriptionTVText:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
    }
    
    /// Used to initialize view
    private func initialiseView(){
        lblTitle.text = titleText ?? "Please take reference from this sample description".localized()
        descTV.text = descriptionTVText ?? ""
    }
    
    /// clicked on cross button
    @IBAction func clickCross(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
    }
    
}
