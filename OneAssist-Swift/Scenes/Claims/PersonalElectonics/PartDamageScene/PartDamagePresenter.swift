//
//  PartDamagePresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/02/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// Model that contains damage part
class PartDamageViewModel : Row {
    var switchedOn: Bool
    var damageKey = ""
    
    init(title: String, switchedOn: Bool) {
        self.switchedOn = switchedOn
        super.init()
        self.title = title
    }
}

/// This protocol used to declare methods to display success or failure of Damage part request
protocol PartDamageDisplayLogic: class {
    
    /// Used to display damage part list
    ///
    /// - Parameters:
    ///   - datasource: damage part list model
    ///   - partNames: device condition view model list
    func displayPartDamages(datasource: [PartDamageViewModel], partNames: [CardOptionsModel])
    
    /// Used to display error
    ///
    /// - Parameter error: error message
    func displayError(_ error: String)
}

/// This class is used to confirm the methods of *PartDamagePresentationLogic* protocol. It contains methods to prepare Damage part list to display on screen.
class PartDamagePresenter: BasePresenter, PartDamagePresentationLogic {
    weak var viewController: PartDamageDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func receivedPartDamages(whatHappenedResponse: WhatHappendResponseDTO?, whatHappenedError: Error?, deviceConditionResponse: DeviceConditionResponseDTO?, deviceConditionError: Error?) {
        do {

            try Utilities.checkError(whatHappenedResponse, error: whatHappenedError)
            try Utilities.checkError(deviceConditionResponse, error: deviceConditionError)
            
            viewController?.displayPartDamages(datasource: receivedPartDamages(response: deviceConditionResponse), partNames: receivedPartNames(response: whatHappenedResponse))
        } catch {
           viewController?.displayError(error.localizedDescription)
        }
    }
    
    
    /// Parsed received data of damage part condition
    ///
    /// - Parameter response: response model
    /// - Returns: list of damage part model
    func receivedPartDamages(response: DeviceConditionResponseDTO?) -> [PartDamageViewModel]  {
        
        let conditions = response?.data?.map({ (deviceCondition) -> PartDamageViewModel in
            let model = PartDamageViewModel(title: deviceCondition.value ?? "", switchedOn: false)
            model.damageKey = deviceCondition.key ?? ""
            return model
        })
        
        return conditions ?? []
    }
    
    /// Parsed & Return received data of damage part
    ///
    /// - Parameter response: response of part name
    /// - Returns: part name list
    func receivedPartNames(response: WhatHappendResponseDTO?) -> [CardOptionsModel] {
        
        var list: [CardOptionsModel] = []
        if let items = response?.data {
            for item in items {
                let model = CardOptionsModel(title: item.taskName ?? "")
                list.append(model)
            }
        }
        return list
    }
}
