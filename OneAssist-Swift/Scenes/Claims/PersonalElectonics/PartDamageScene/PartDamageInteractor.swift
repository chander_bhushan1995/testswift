//
//  PartDamageInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/02/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol contains method prototype for presentation logic of Damage parts
protocol PartDamagePresentationLogic {
    
    /// This method is called when Damage part list received from backend
    ///
    /// - Parameters:
    ///   - whatHappenedResponse: response of damage part list
    ///   - whatHappenedError: error in damage part
    ///   - deviceConditionResponse: response of device condiotion
    ///   - deviceConditionError: error in device condition
    func receivedPartDamages(whatHappenedResponse: WhatHappendResponseDTO?, whatHappenedError: Error?, deviceConditionResponse: DeviceConditionResponseDTO?, deviceConditionError: Error?)
}

/// This class is used to confirm the methods of *PartDamageBusinessLogic* protocol. It contains method to get damage part related data.
class PartDamageInteractor: BaseInteractor, PartDamageBusinessLogic {
    var presenter: PartDamagePresentationLogic?
    
    // MARK: Business Logic Conformance
    func getPartDamages(productCode: String, category: Constants.Services) {
        
        let group = DispatchGroup()
        
        group.enter()
        
        let request = WhatHappendRequestDTO()
        request.productCode = productCode
        request.taskType = "ISSUE"
        
        var apiResponse: WhatHappendResponseDTO?
        var apiError: Error?
        
        let _ = WhatHappendRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            apiResponse = response
            apiError = error
            group.leave()
        }
        
        group.enter()
        
//        let conditionRequest = DeviceConditionRequestDTO()
//
//        var apiConditionResponse: DeviceConditionResponseDTO?
//        var apiConditionError: Error?
//
//        let _ = DeviceConditionRequestUseCase.service(requestDTO: conditionRequest) { (usecase, response, error) in
//            apiConditionResponse = response
//            apiConditionError = error
//            group.leave()
//        }
        
        // Hard Coded Conditions
    
        let apiConditionResponse = DeviceConditionResponseDTO(dictionary: [:])
        apiConditionResponse.status = Constants.ResponseConstants.success
        
        let model = DeviceCondition(dictionary: [:])
        model.key = category == .peADLD ? "damageIsSwitchedOn" : "breakDownIsSwitchedOn"
        model.value = "Are you able to switch on your device?"
        
        let model2 = DeviceCondition(dictionary: [:])
        model2.key = category == .peADLD ? "damageIsTouchWorks" : "breakDownIsTouchWorks"
        model2.value = "Is you device's touchscreen working"
        
        apiConditionResponse.data = [model, model2]
        
        let apiConditionError: Error? = nil
        group.leave()
        
        group.notify(queue: .main) {
            self.presenter?.receivedPartDamages(whatHappenedResponse: apiResponse, whatHappenedError: apiError, deviceConditionResponse: apiConditionResponse, deviceConditionError: apiConditionError)
        }
    }
}
