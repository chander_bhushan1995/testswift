//
//  PartDamageVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/02/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This delegate used to give callback of selected damage part
protocol PartDamageVCDelegate: class {
    
    /// Gives callback on selection of damage part.
    ///
    /// - Parameters:
    ///   - selectedPart: selected part
    ///   - reasons: all part list
    func selectedDamages(selectedPart: CardOptionsModel, reasons: [PartDamageViewModel])
}

/// This protocol contains method related to Damage part business logic.
protocol PartDamageBusinessLogic {
    
    /// Used to get damage part list
    ///
    /// - Parameters:
    ///   - productCode: product code
    ///   - category: category of SR
    func getPartDamages(productCode: String, category: Constants.Services)
}

/// This class is used to show damage part VC.
class PartDamageVC: BaseVC {
    
    var interactor: PartDamageBusinessLogic?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var fieldViewPartName: TextFieldView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnPrimary: OAPrimaryButton!
    @IBOutlet weak var imgArrow: UIImageView!
    
    var parts: [CardOptionsModel] = []
    var dataSource = TableViewDataSource<PartDamageViewModel,TableViewSection<PartDamageViewModel>>()
    weak var delegate: PartDamageVCDelegate?
    var productCode = ""
    var selectedIndex: Int?
    var category: Constants.Services =  .peADLD
    
    var prefetchedDamagePart: String?
    var prefetchedConditions: [PartDamageViewModel]?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        EventTracking.shared.addScreenTracking(with: .SR_Damaged_Part)
    }
    
    // MARK:- private methods
    /// Used to initialize view
    private func initializeView() {
        tblView.setHeaderView(viewHeader)
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 44
        tblView.register(nib: Constants.CellIdentifiers.partDamageCell, forCellReuseIdentifier: Constants.CellIdentifiers.partDamageCell)
        
        fieldViewPartName.hasImage = false
        fieldViewPartName.descriptionText = "Part name"
        fieldViewPartName.placeholderFieldText = "Select part name"
        
        btnPrimary.isEnabled = false
        btnPrimary.setTitle("Submit Request", for: .normal)
        
        imgArrow.transform = imgArrow.transform.rotated(by: .pi/2)
        
        Utilities.addLoader(onView: view, message: "getting damages types", count: &loaderCount, isInteractionEnabled: false)
        interactor?.getPartDamages(productCode: productCode, category: category)
    }
    
    /// Used to set prefatched data
    ///
    /// - Parameters:
    ///   - datasource: Damage condition model list
    ///   - partNames: Damage part name list
    fileprivate func setPrefetchedData(datasource: [PartDamageViewModel], partNames: [CardOptionsModel]) {
        
        // If data already exist
        if let conditions = prefetchedConditions {
            for condition in conditions {
                if let index = datasource.firstIndex(where: {$0.damageKey == condition.damageKey}) {
                    changedIndex(isOn: condition.switchedOn, index: index)
                }
            }
        }
        
        // Show selected part
        if let index = partNames.firstIndex(where: {$0.title == prefetchedDamagePart}) {
            clickedPart(index: index)
        }
    }
    
    // MARK:- Action Methods
    
    /// clicked button part name
    @IBAction func clickedBtnPartName(_ sender: Any) {
        showProtectionAmmountList()
    }
    
    /// clicked button submit request
    @IBAction func clickedBtnPrimary(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .srDamagePartSubmit, [.subcategory: CacheManager.shared.eventProductName ?? ""])
        if selectedIndex != nil {
            delegate?.selectedDamages(selectedPart: parts[selectedIndex!], reasons: dataSource.sectionModel(for: 0).rows)
        }
    }
    
    func clickedPart(index: Int) {
        fieldViewPartName.fieldText = parts[index].title
        selectedIndex = index
        btnPrimary.isEnabled = true
    }
}

extension PartDamageVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.partDamageCell, for: indexPath) as! PartDamageCell
        cell.segmentControlObj.tag = indexPath.row
        cell.delegate = self
        let model = dataSource.rowModel(for: indexPath)
        cell.updateUserInterface(title: model.title, isOn: model.switchedOn)
        return cell
    }
}

extension PartDamageVC: PartDamageCellDelegate {
    func changedIndex(isOn: Bool, index: Int) {
        let path = IndexPath(row: index, section: 0)
        let model = dataSource.rowModel(for: path)
        model.switchedOn = isOn
        tblView.reloadData()
    }
}

// MARK:- Display Logic Conformance
extension PartDamageVC: PartDamageDisplayLogic {
    
    func displayPartDamages(datasource: [PartDamageViewModel], partNames: [CardOptionsModel]) {
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        parts = partNames
        
        let tableDatasource = TableViewDataSource<PartDamageViewModel,TableViewSection<PartDamageViewModel>>()
        let section = TableViewSection<PartDamageViewModel>()
        section.rows = datasource
        
        tableDatasource.tableSections = [section]
        
        self.dataSource = tableDatasource
        tblView.reloadData()
        
        setPrefetchedData(datasource: datasource, partNames: partNames)
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {
            Utilities.addLoader(onView: self.view, message: "getting damages types", count: &self.loaderCount, isInteractionEnabled: false)
            self.interactor?.getPartDamages(productCode: self.productCode, category: self.category)
        }, nil)
    }
}

// MARK:- Configuration Logic
extension PartDamageVC {
    
    /// Configure interactor and presenter
    fileprivate func setupDependencyConfigurator() {
        let interactor = PartDamageInteractor()
        let presenter = PartDamagePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension PartDamageVC {
    
}
extension PartDamageVC:  ContentDelegate{
    
    
    func didSelectRow(row: Int) {
        fieldViewPartName.fieldText = parts[row].title ?? ""
        selectedIndex = row
        btnPrimary.isEnabled = true
        self.dismiss(animated: true)
    }
    
    fileprivate func showProtectionAmmountList(){
        guard parts.count > 0 else { return }
        let menuOpener = SelectMenuView()
        //   menuOpener.dataSource = self
        menuOpener.delegate = self
        for i in parts {
            if let paramName = i.title {
                menuOpener.list.append(paramName)
            }
        }
        //        menuOpener.list = self.amounts
        
        let contentH = min(self.view.bounds.height * 0.75, CGFloat(parts.count)*70.0)
        Utilities.presentPopover(view: menuOpener, height: contentH)
    }
}
