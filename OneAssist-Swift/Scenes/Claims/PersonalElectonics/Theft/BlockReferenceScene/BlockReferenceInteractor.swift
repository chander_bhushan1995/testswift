//
//  BlockReferenceInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/04/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol BlockReferencePresentationLogic {
    
}

class BlockReferenceInteractor: BaseInteractor, BlockReferenceBusinessLogic {
    var presenter: BlockReferencePresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
