//
//  BlockReferencePresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/04/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol BlockReferenceDisplayLogic: class {
    
}

class BlockReferencePresenter: BasePresenter, BlockReferencePresentationLogic {
    weak var viewController: BlockReferenceDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
}
