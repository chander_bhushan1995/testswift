//
//  BlockReferenceVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/04/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol BlockReferenceBusinessLogic {
    
}

protocol BlockReferenceVCDelegate: class {
    func blockReferenceSubmitted(refNo: String, pickedTime: String)
}

class BlockReferenceVC: BaseVC, PickerMethods {
    var interactor: BlockReferenceBusinessLogic?
    
    @IBOutlet weak var fieldViewRefNumber: TextFieldView!
    @IBOutlet weak var lblPickDate: UILabel!
    @IBOutlet weak var buttonSubmit: PrimaryButton!
    
    weak var delegate: BlockReferenceVCDelegate?
    var handler: TextFieldViewDelegateHandler?
    var pickedTime: String?
    var prefetchedDateSelected: String?
    var prefetchedRefNo: String?
    
    var picker:PickerView?
    var window :UIWindow!
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        addTap()
        window = UIApplication.shared.keyWindow
        if let pickerView = Bundle.main.loadNibNamed(PickerView.nibName, owner: self, options: nil)?.first as? PickerView {
            picker = pickerView
            picker?.frame = CGRect(x: 0, y: self.window.frame.size.height, width: self.window.frame.size.width, height: (self.window?.frame.size.height)!)
            picker?.backgroundColor = UIColor.clear
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
            picker?.addGestureRecognizer(tapGesture)
            picker?.delegate = self
        }
        
        buttonSubmit.setTitle("SUBMIT REQUEST", for: .normal)
        buttonSubmit.isEnabled = false
        lblPickDate.textColor = UIColor.dodgerBlue
        
        fieldViewRefNumber.hasImage = false
        fieldViewRefNumber.descriptionText = "Enter reference number".uppercased()
        fieldViewRefNumber.placeholderFieldText = "Enter reference number".uppercased()
        
        handler = TextFieldViewDelegateHandler(requiredFields: [fieldViewRefNumber], validationHandler: { (isValid, tuple) in
            if self.pickedTime != nil && isValid {
                self.buttonSubmit.isEnabled = true
            } else {
                self.buttonSubmit.isEnabled = false
            }
        })
        
        if let refNo = prefetchedRefNo {
            let range = NSRange(location: 0, length: 0)
            fieldViewRefNumber.textField(fieldViewRefNumber.textFieldObj, shouldChangeCharactersIn: range, replacementString: refNo)
            fieldViewRefNumber.fieldText = refNo
        }
        picker?.setDate(date:prefetchedDateSelected)
    }
    
    // MARK:- Action Methods
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        picker?.frame = CGRect(x: 0, y: self.window.frame.size.height, width: self.window.frame.size.width, height: (self.window.frame.size.height))
        picker?.backgroundColor = UIColor.clear
    }
    
    @IBAction func clickedPickTime(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            UIApplication.shared.keyWindow?.addSubview(self.picker!)
            self.picker?.frame = CGRect(x: self.window.frame.origin.x , y: self.window.frame.origin.y , width: self.window.frame.size.width, height: (self.window.frame.size.height))
            
        }) { (true) in
            UIView.animate(withDuration: 0.1, animations: {
                self.picker?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            })
        }
    }
    
    @IBAction func clickedBtnPrimary(_ sender: Any) {
        delegate?.blockReferenceSubmitted(refNo: fieldViewRefNumber.fieldText ?? "", pickedTime: pickedTime ?? "")
    }
    
    func clickDoneBtn(_ date:String){
        buttonSubmit.isEnabled = (handler?.invalidFieldCount == 0)
        self.picker?.frame = CGRect(x: window.frame.origin.x , y: self.window.frame.size.height, width: self.window.frame.size.width, height: (self.window.frame.size.height))
        picker?.backgroundColor = UIColor.clear
        self.pickedTime = date
        let date1 = DateFormatter.scheduleFormat.date(from: date)
        let date3 = gregorianCalendar.date(bySettingHour: 0, minute: 0, second: 0, of: date1!)
        let date2 = gregorianCalendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date.currentDate())!
        if date2 == date3{
            let dateString  = "Today, " + "\(DateFormatter.time12period.string(from: date1!))"
            lblPickDate.text = dateString
            return
        }
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = gregorianCalendar
        dateFormatter.dateFormat = "MMM d,h:mm a"
        lblPickDate.text = dateFormatter.string(from: date1!)
    }
}

// MARK:- Display Logic Conformance
extension BlockReferenceVC: BlockReferenceDisplayLogic {
    
}

// MARK:- Configuration Logic
extension BlockReferenceVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = BlockReferenceInteractor()
        let presenter = BlockReferencePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension BlockReferenceVC {
    
}
