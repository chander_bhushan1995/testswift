//
//  PresentView.swift
//  MapWithAnimationPopupSwift4
//
//  Created by Raj on 22/04/19.
//  Copyright © 2019 Raj. All rights reserved.
//

import UIKit
protocol PresentViewDelegate:class{
    func mapHeight(height:CGFloat)
    func presentVC(indexPath: IndexPath)
    func showalert()
    func showSettingAlert()
}

enum PopopExpandState {
    case Hidden
//    case Quarter
    case Half
    case Full
    
    var height:CGFloat {
        let quarterHeight:CGFloat = 128
        switch self {
//        case .Quarter: return quarterHeight
        case .Half: return quarterHeight * 2
        case .Full: return  quarterHeight * 4
        case .Hidden: return 0
        }
    }
}
class PresentView: UIView, NibInstantiable {
    @IBOutlet weak var tableView: UITableView!
//    var yPosition:CGFloat!
    var expandedState:PopopExpandState = .Hidden
    var fullHeight:CGFloat!
    var dateToSend:String!
    var centerAddress:String?
    var presentModel:ServiceCenterClusterMarkerModel!
    weak  var delegate:PresentViewDelegate!
    
//    private let halfHeight:CGFloat = 100
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
        
    }
    private func commonInit(){
        loadNibContent()
        configureView()
    }
    
    private func configureView() {
        tableView.estimatedRowHeight = 200
        self.tag = 500
        let swipeGestureTop = UISwipeGestureRecognizer.init(target: self, action: #selector(SwipeGesture))
        swipeGestureTop.direction = UISwipeGestureRecognizer.Direction.up
        self.addGestureRecognizer(swipeGestureTop)
        let swipeGestureDown = UISwipeGestureRecognizer.init(target: self, action: #selector(SwipeGesture))
        swipeGestureDown.direction = UISwipeGestureRecognizer.Direction.down
        self.addGestureRecognizer(swipeGestureDown)
         tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.CellIdentifiers.setReminder , bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.setReminder)
        tableView.register(UINib(nibName: Constants.CellIdentifiers.centerList, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.centerList)
        tableView.register(UINib(nibName: Constants.CellIdentifiers.setReminderButtonBox, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.setReminderButtonBox)
        
    }
    func cellSelectedFunction(){
                if expandedState == .Half{
                    UIView.animate(withDuration: 0.2, delay: 0, options:
                        UIView.AnimationOptions.curveEaseOut, animations: {
//                            self.frame.size.height = self.height/2
//                            self.frame.origin.y =  self.height/2 + self.yPosition
                            self.frame.size.height = self.expandedState.height
                            self.layoutIfNeeded()
                            self.tableView.reloadData()
                    }, completion: { (status) in
                        self.delegate.mapHeight(height:self.expandedState.height)//self.view.frame.height/2)
                    })
                }
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: Constants.NIBNames.presentView, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @objc func SwipeGesture(_ sender:UISwipeGestureRecognizer){
        switch sender.direction {
        case .up:
            switch expandedState {
//            case .Half:
//                UIView.animate(withDuration: 0.2, delay: 0, options:
//                    UIView.AnimationOptions.curveEaseOut, animations: {
//                        //                        self.frame.size.height = self.height/2
//                        //                        self.frame.origin.y =  self.height/2 + self.yPosition
//                        self.expandedState = .Quarter
//                        self.frame.size.height = self.expandedState.height
//                        self.layoutIfNeeded()
//                        self.tableView.reloadData()
//                }, completion: { (status) in
//                    self.delegate.mapHeight(height:self.expandedState.height)
//                })
//            case .Quarter:
//                UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
//                    self.frame.size.height = self.expandedState.height
//                    self.expandedState = .Half
//                    self.tableView.reloadData()
//                    self.layoutIfNeeded()
//                }, completion: { (status) in
//                    self.delegate.mapHeight(height:self.expandedState.height)
//                })
            case .Full,.Hidden, .Half:
                break
            }
            
        case .down:
            switch expandedState {
//            case .Quarter:
//                UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
//                    self.frame.size.height = 0
//                    self.delegate.mapHeight(height:  0)
////                    self.removeFromSuperview()
//                    self.layoutIfNeeded()
//                }, completion: nil)
//            case .Half:
//                UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
////                    self.expandedState = .Quarter
//                     self.expandedState = .Hidden
//                    self.frame.size.height = self.expandedState.height
//                    self.delegate.mapHeight(height:  self.expandedState.height)
//                    self.layoutIfNeeded()
//                }, completion: nil)
            case .Full:
                UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.expandedState = .Half
                    self.delegate.mapHeight(height: self.expandedState.height)
                    self.frame.size.height = self.expandedState.height
                    self.tableView.reloadData()
                    self.layoutIfNeeded()
                }, completion: nil)
            case .Hidden, .Half:  break
                
            }
        default:
            break
        }
        
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
extension PresentView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier:
            Constants.CellIdentifiers.centerList, for: indexPath) as! CenterListCell
            if expandedState == .Half{
            cell.sepratorView.isHidden = true
            }
            cell.titleLabel.text = presentModel.centerName
            cell.addressLabel.text = presentModel.centerAddress
            cell.openTodayLabel.text = presentModel.openToday
            cell.closeTimeLabel.text = presentModel.clossesAt
            cell.mobile = presentModel.mobile ?? ""
            cell.currentPosition = CacheManager.shared.currentLocation
            cell.destinationPosition = presentModel.position
            cell.indexPath = indexPath
//            let stringDistance = String(format:"%f", presentModel.distance)
//            let charFirst = stringDistance[0 ..< 1]
//            let charSecond = stringDistance[1 ..< 2]
            cell.distanceLabel.text = presentModel.kmDistance//charFirst + "." + charSecond + " km"
            cell.delegate = self
            return cell
        }else{
            if expandedState == .Half {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.setReminderButtonBox, for: indexPath) as! SetReminderButtonBoxCell
                cell.delegate = self
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.setReminder, for: indexPath) as! SetReminderCell
                cell.delegate = self
                return cell
            }
        }
    }
    
}

extension PresentView:NearByCenterCellDelegate{
    func closesAtClicked(indexPath: IndexPath) {
        delegate.presentVC(indexPath: indexPath)
    }    
}
extension PresentView:BoxSetReminderDelegate{
    func boxSetReminderClicked() {
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.expandedState = .Full
            self.frame.size.height = self.fullHeight//expandedState.height
            self.delegate.mapHeight(height:self.fullHeight)
            self.tableView.reloadData()
            self.layoutIfNeeded()
        }, completion: nil)
    }
}
extension PresentView:SetReminderCellDelegate{
    func setReminderClicked(dateToSend:String?){
        self.dateToSend = dateToSend
        let eventManager = EventManager()
        eventManager.delegate = self
        let dateFormatter =  DateFormatter.scheduleFormat
        
        let startTime = dateFormatter.date(from: (dateToSend ?? "")) ?? Date()
        eventManager.createEvent(on: startTime, withTitle: Event.EventParams.visitServiceCenter, forHours: 2, location:self.centerAddress)
        
        //event
        EventTracking.shared.eventTracking(name: .setReminder, [.subcategory : CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? "", .dateTime : dateToSend ?? "", .serviceCenter:presentModel.centerAddress ?? ""])
    }
}
extension PresentView:ShowingAlert{
    func showAlert() {
        Utilities.setCalendarEvent(srId: "123456" , scheduleStartDate: dateToSend ?? "")
        delegate.showalert()
        }
    func showAlertWithAction() {
        print("")
        delegate.showSettingAlert()
    }
}


