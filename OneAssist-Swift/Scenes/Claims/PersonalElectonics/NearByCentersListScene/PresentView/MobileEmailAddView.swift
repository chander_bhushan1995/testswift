//
//  MobileEmailAddView.swift
//  OneAssist-Swift
//
//  Created by Raj on 21/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol MobileEmailAddViewDelegate:class{
    func addClicked(items:[String])
}

class MobileEmailAddView: UIView {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mobileEmailDescriptionLabel: H3RegularBlackLabel!
    weak  var delegate:MobileEmailAddViewDelegate!
    var items:[String]!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tag = 501
    }
//    func setHeight(){
//        UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
//            self.frame.size.height = 50
//            self.layoutIfNeeded()
//        }, completion: nil)
//    }
    @IBAction func addAction(_ sender: Any) {
         self.removeFromSuperview()
        delegate.addClicked(items:self.items)
        
    }
    @IBAction func crossAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    class func instanceFromNib() -> UIView {
        return UINib(nibName: Constants.NIBNames.mobileEmailAddView, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
