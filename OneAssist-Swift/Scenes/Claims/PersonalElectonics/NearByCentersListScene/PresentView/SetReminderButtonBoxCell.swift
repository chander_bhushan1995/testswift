//
//  SetReminderButtonBoxCell.swift
//  OneAssist-Swift
//
//  Created by Raj on 06/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol BoxSetReminderDelegate:class{
    func boxSetReminderClicked()
}

class SetReminderButtonBoxCell: UITableViewCell {
    var delegate:BoxSetReminderDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func setReminderAction(_ sender: Any) {
        delegate.boxSetReminderClicked()
    }
}
