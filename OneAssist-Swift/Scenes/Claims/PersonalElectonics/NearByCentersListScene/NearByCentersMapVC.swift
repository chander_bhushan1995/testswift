//
//  NearByCentersMapVC.swift
//  OneAssist-Swift
//
//  Created by Raj on 02/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import GoogleMaps
class NearByCentersMapVC: BaseVC {
//    @IBOutlet weak var mapBottomContsraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintBottomDetailsView: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var bottomDetailsView: PresentView!
    var userLocation : CLLocationCoordinate2D!
    fileprivate var currentSelectedMarker : GMSMarker?
    fileprivate var currentSelectedLocation : CLLocationCoordinate2D?
    fileprivate var clusterManager: GMUClusterManager!
//    var addedPresentView:UIView!
    var cellClickedBool = false
    var cellSelectedModel:ServiceCenterClusterMarkerModel!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    private func initializeView() {
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        // To shift the user location button on map a padding of 50 px is added so that directions button does not overlap
        let mapInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 40.0, right: 0.0)
        mapView.padding = mapInsets
        // Set up the cluster manager with default icon generator and renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 50, 100, 200, 1000], backgroundImages: [#imageLiteral(resourceName: "Group 24 Copy 10"), #imageLiteral(resourceName: "Group 24 Copy 10"), #imageLiteral(resourceName: "Group 24 Copy 10"), #imageLiteral(resourceName: "Group 24 Copy 10"), #imageLiteral(resourceName: "Group 24 Copy 10")])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        
        focusMapToShowAllMarkers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         initializeView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if cellClickedBool{
            let newCamera = GMSCameraPosition.camera(withTarget: cellSelectedModel.position, zoom: mapView.camera.zoom + 1)
            let update = GMSCameraUpdate.setCamera(newCamera)
            mapView.moveCamera(update)
//            showPresentView(presentModel:cellSelectedModel, toState: .Quarter)
            showPresentView(presentModel:cellSelectedModel, toState: .Half)
            self.title = cellSelectedModel.centerName ?? Strings.ServiceRequest.navigationBarText.serviceCenterNearYou
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style: .done, target: self, action: #selector(goToBack))
        }
    }
    @objc func goToBack(){
       /// self.willMove(toParentViewController: nil)//self.view.removeFromSuperview()
        // self.view.willRemoveSubview(addedPresentView)
//        bottomDetailsView.removeFromSuperview()
        bottomDetailsView.layoutIfNeeded()
        heightConstraintBottomDetailsView.constant = 0
        self.navigationController?.popViewController(animated: true)
    }
    }

extension NearByCentersMapVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let item = marker.userData as? ServiceCenterClusterMarkerModel {
            if currentSelectedMarker == nil || (currentSelectedMarker != nil && marker != currentSelectedMarker!) {
                currentSelectedMarker?.icon = #imageLiteral(resourceName: "Group 24 Copy 10")
                currentSelectedMarker = marker
                marker.icon = #imageLiteral(resourceName: "Group 24 Copy 10")
                currentSelectedLocation = marker.position
                clusterManager.cluster()
                self.title = item.centerName ?? Strings.ServiceRequest.navigationBarText.serviceCenterNearYou
//                showPresentView(presentModel:item, toState: .Quarter)
                showPresentView(presentModel:item, toState: .Half)
            }
        } else {
            print("Did tap a cluster marker")
        }
        return false
    }
    
    fileprivate func showPresentView(presentModel:ServiceCenterClusterMarkerModel,toState:PopopExpandState) {
//        let vc = PresentView.instanceFromNib() as! PresentView
//        vc.delegate = self
//        vc.presentModel = presentModel
//        vc.expandedState = fromState
        //self.mapBottomContsraint.constant = 0
//        self.view.layoutIfNeeded()
//        var frame1 = self.mapView.frame
//        frame1.origin.y = self.mapView.frame.height - 100 + self.mapView.frame.origin.y
//        frame1.size.height = 100
//        vc.frame = frame1
//        vc.yPosition = self.mapView.frame.origin.y
//        vc.height = self.mapView.frame.height
//        if self.view.subviews.count > 1, self.view.subviews[1] is PresentView{
//            self.view.subviews.filter({$0.tag == 500}).forEach({$0.removeFromSuperview()})
//            self.view.addSubview(vc)
//        }else{
//            self.view.addSubview(vc)
//        }
        bottomDetailsView.presentModel = presentModel
        bottomDetailsView.expandedState = toState
        bottomDetailsView.centerAddress = presentModel.centerAddress
        bottomDetailsView.fullHeight = self.mapView.frame.height + heightConstraintBottomDetailsView.constant
        bottomDetailsView.delegate = self
        self.heightConstraintBottomDetailsView.constant = toState.height
        bottomDetailsView.cellSelectedFunction()
//        self.addedPresentView =
        self.view.bringSubviewToFront(bottomDetailsView)
        bottomDetailsView.tableView.reloadData()
        //focusMapToShowAllMarkers()
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
         print("mapView clicked")
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        print("didTapMyLocationButton clicked")
        return false
    }
    
    func focusMapToShowAllMarkers() {
        CacheManager.shared.serviceCenterListMarkers.forEach { (item) in
            clusterManager.add(item)
        }
        clusterManager.cluster()
        clusterManager.setDelegate(self, mapDelegate: self)
        let firstLocation = CacheManager.shared.serviceCenterListMarkers.first!.position
        var bounds = GMSCoordinateBounds.init(coordinate: firstLocation, coordinate: firstLocation)
        for marker in CacheManager.shared.serviceCenterListMarkers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(15))
        self.mapView.animate(with: update)
    }
}

extension NearByCentersMapVC : GMUClusterManagerDelegate {
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        return false
    }
}

extension NearByCentersMapVC : GMUClusterRendererDelegate {
    
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        
        let marker = ServiceCenterMarkerView()
        if let model = object as? ServiceCenterClusterMarkerModel {
            marker.centerName = model.centerName
            marker.centerAddress = model.centerAddress
            if currentSelectedLocation != nil && model.position.latitude == currentSelectedLocation!.latitude && model.position.longitude == currentSelectedLocation!.longitude {
                currentSelectedMarker = marker
            }
            marker.map = mapView
            marker.icon = model.activeIcon
            marker.setIconSize(scaledToSize: .init(width: 28, height: 40))

        }
        return marker
    }
}
extension NearByCentersMapVC:PresentViewDelegate{
    func showSettingAlert() {
        showAlert(message: Event.EventParams.allowToCreateEvent , primaryButtonTitle: Constants.WHC.alertMessages.settings, Strings.Global.cancel, primaryAction: {
            Utilities.openAppSettings()
        }, {})
    }
    func showalert() {
        let window = UIApplication.shared.keyWindow
        window?.rootViewController?.view.makeToast(Strings.ToastMessage.reminder)
        self.navigationController?.popViewController(animated: true)
    }
    func mapHeight(height: CGFloat) {
        heightConstraintBottomDetailsView.constant = height
        if height == 0  {currentSelectedMarker  = nil}
}
    func presentVC(indexPath:IndexPath) {
        let vc = ServiceCenterTimeScheduleVC()
         vc.timingDetails = CacheManager.shared.serviceCenterListMarkers[indexPath.row].timingDetails
        let basePickerVC = BasePickerVC(withView:vc.view, height:300)
        presentInFullScreen(basePickerVC, animated: true)
        self.presentInFullScreen(vc, animated: true, completion: nil)

    }
    
}
