//
//  DevicePickupCell.swift
//  OneAssist-Swift
//
//  Created by Raj on 28/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import CoreLocation
class DevicePickupCell: UITableViewCell {
    weak var delegate:NearByCenterCellDelegate!
    @IBOutlet weak var highlighterView: UIView!
    
    @IBOutlet weak var getDirectionLabel: SupportingTextRegurlarBlueLabel!
    @IBOutlet weak var callLabel: SupportingTextRegurlarBlueLabel!
    
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var getDirectionBtn: UIButton!
    @IBOutlet weak var highlighterLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var timeLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var todayLabel: BodyTextRegularGreyLabel!
//    @IBOutlet weak var addressLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    
    @IBOutlet weak var addressLabelText: BodyTextRegularGreyLabel!
    @IBOutlet weak var getDirectionView:UIView!
    var displayList:[DisplayList]?
    var latLong:String?
    var indexPath:IndexPath!
    var callNumber:NSNumber?
     var currentPosition:CLLocationCoordinate2D!
    override func awakeFromNib() {
        super.awakeFromNib()
        highlighterView.layer.borderWidth = 1
        highlighterView.layer.borderColor = UIColor.dodgerBlue.cgColor
        // Initialization code
    }

    func setViewModel(_ model:TimeLineViewModel.DevicePickupCellModel, _ delegate:NearByCenterCellDelegate?){
        
        self.delegate = delegate
        self.indexPath = model.indexPath
        
        highlighterLabel.text = model.heighliterText ?? "" //"your device is repaired.Share OTP 7345 while collection device at the service center."
        addressLabelText.text = model.addressText ?? "" //"Badshahpur,phase ||| plot no. 234 gurgaon 122001"
        titleLabel.text = model.titleText ?? ""
        self.latLong = model.latLong
        self.callNumber = model.callNumber
        self.displayList = model.displayList
        self.currentPosition = model.currentPosition
        getDirectionBtn.isEnabled = model.directionBtnEnable ?? false
        callBtn.isEnabled = model.callBtnEnable ?? false
        getDirectionLabel.text = model.directionBtnName ?? ""
        callLabel.text = model.callBtnName ?? ""
        todayLabel.text = model.openTodayText ?? ""
       // timeLabel.text = closses ?? ""
        model.latLong == nil ? (getDirectionView.isHidden = true) : (getDirectionView.isHidden = false)
    }
    
    @IBAction func getDirectionClicked(_ sender: Any) {
        if let currentPosition = currentPosition,let latLong = latLong{
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?saddr=\(currentPosition.latitude),\(currentPosition.longitude)&daddr=\(latLong)&center=\(currentPosition.latitude),\(currentPosition.longitude)&zoom=10")!)
        } else {
            UIApplication.shared.openURL(URL(string:"https://www.google.com/maps/dir/?saddr=\(currentPosition.latitude),\(currentPosition.longitude)&daddr=\(latLong)")!)
        }
        }
    }
    @IBAction func callClicked(_ sender: Any) {
        if let mobile = callNumber{
            Utilities.makeCall(to: String(describing: mobile) )
        }
    }
    @IBAction func timeTableClicked(_ sender: Any) {
        delegate.closesAtClicked(indexPath:indexPath)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
