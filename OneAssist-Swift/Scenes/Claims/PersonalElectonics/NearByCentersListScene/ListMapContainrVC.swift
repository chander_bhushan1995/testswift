//
//  ListMapContainrView.swift
//  OneAssist-Swift
//
//  Created by Raj on 02/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class ListMapContainrVC: BaseVC {
    var isMap = false
    var pincode:String!
    let listVC  = NearByCenterListVC()
    let mapVC = NearByCentersMapVC()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = Strings.ServiceRequest.navigationBarText.serviceCenterNearYou
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map", style: .done, target: self, action: #selector(mapClicked))
        self.navigationItem.rightBarButtonItem?.tintColor = .dodgerBlue
        listVC.view.frame = self.view.bounds
        listVC.willMove(toParent: self)
        listVC.pincode = pincode
        listVC.delegate = self
        self.view.addSubview(listVC.view)
        self.addChild(listVC)
        listVC.didMove(toParent: self)
    }
    @objc func listClicked(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: Strings.ServiceRequest.buttonName.map, style: .done, target: self, action: #selector(mapClicked))
        isMap = false
        addViewController()
    }
    @objc func mapClicked(){
        EventTracking.shared.eventTracking(name: .SCMap)
        
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: Strings.ServiceRequest.buttonName.list, style: .done, target: self, action: #selector(listClicked))
        isMap = true
        addViewController()
    }
    func addViewController(){
        self.willMove(toParent: nil)
        //self.view.removeFromSuperview()
        //self.removeFromParentViewController()
        if isMap {
            mapVC.view.frame = self.view.bounds
            mapVC.willMove(toParent: self)
            mapVC.cellClickedBool = false
            self.view.addSubview(mapVC.view)
            self.addChild(mapVC)
            mapVC.didMove(toParent: self)
        }else{
            listVC.view.frame = self.view.bounds
            listVC.willMove(toParent: self)
            self.view.addSubview(listVC.view)
            self.addChild(listVC)
            listVC.didMove(toParent: self)
        }
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension ListMapContainrVC : NearByCenterListVCDelegate{
    func cellClicked(model:ServiceCenterClusterMarkerModel){
        mapVC.cellClickedBool = true
        mapVC.cellSelectedModel = model
        self.navigationController?.pushViewController(mapVC, animated: true)
    }
}
