//
//  CenterListCell.swift
//  OneAssist-Swift
//
//  Created by Raj on 26/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol NearByCenterCellDelegate:class{
    func closesAtClicked(indexPath:IndexPath)
}


class CenterListCell: UITableViewCell {
 weak var  delegate:NearByCenterCellDelegate!
    @IBOutlet weak var distanceLabel: TagsRegularBlackLabel!
    @IBOutlet weak var closeTimeLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var openTodayLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var addressLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var sepratorView: SeperatorView!
    @IBOutlet weak var kmView: UIView!
    @IBOutlet weak var getDirectionView:UIView!
    var mobile:String!
    var currentPosition:CLLocationCoordinate2D!
    var destinationPosition:CLLocationCoordinate2D!
    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        kmView.backgroundColor = UIColor.whiteSmoke
        // Initialization code
        
    }
    
    @IBAction func getDirectionAction(_ sender: Any) {
        
        EventTracking.shared.eventTracking(name: .getDirection, [.location : addressLabel.text ?? "", .serviceCenter: titleLabel.text ?? ""] )
        if currentPosition == nil {return}
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            // open in GoogleMap, app
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?saddr=\(currentPosition.latitude),\(currentPosition.longitude)&daddr=\(destinationPosition.latitude),\(destinationPosition.longitude)&center=\(currentPosition.latitude),\(currentPosition.longitude)&zoom=10")!)
        } else {
            //open map in browser
            UIApplication.shared.openURL(URL(string:"https://www.google.com/maps/dir/?saddr=\(currentPosition.latitude),\(currentPosition.longitude)&daddr=\(destinationPosition.latitude),\(destinationPosition.longitude)")!)

        }
    }
    
    @IBAction func callAction(_ sender: Any) {
        
        EventTracking.shared.eventTracking(name: .callServiceCenter,  [.location : addressLabel.text ?? "", .serviceCenter: titleLabel.text ?? ""])
        if let mobile = mobile{
        Utilities.makeCall(to: mobile )
        }
    }
    
    func set(title:String?,address:String?,openToday:String?,closeTime:String?,distance:String?,currentPosition:CLLocationCoordinate2D?,destinationPosition:CLLocationCoordinate2D?,mobile:String?){
        titleLabel.text = title
        addressLabel.text = address
        openTodayLabel.text = openToday
        closeTimeLabel.text = closeTime
        distanceLabel.text = distance
        self.currentPosition = currentPosition
        self.destinationPosition = destinationPosition
        self.mobile =  mobile ?? ""
        destinationPosition == nil ? (getDirectionView.isHidden = true) : (getDirectionView.isHidden = false)
    }
    
    @IBAction func clossingTimeAction(_ sender: Any) {
        delegate.closesAtClicked(indexPath: self.indexPath)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
