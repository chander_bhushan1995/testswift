//
//  SearchServiceCenterListInteractor.swift
//  OneAssist-Swift
//
//  Created by Raj on 07/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol SearchServiceCenterPresentationLogic {
    func presentServiceCenterList(responseDTO:ServiceCenterListResponceDTO?,error:Error?)
    func receivedServiceCenterDistance(distanceModel:GoogleDistanceResponseDTO?)
}

class SearchServiceCenterListInteractor: BaseInteractor {
    var presenter: SearchServiceCenterPresentationLogic?
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SearchServiceCenterListInteractor:SearchServiceCenterListBusinessLogic{
    
    func getServiceCenterList(for pincode: String) {
        let requestDto = ServiceCenterListRequestDTO(pincode: pincode)
        let _ = ServiceCenterListRequestUseCase.service(requestDTO: requestDto) { (usecase, response, error) in
            print(response?.data)
            self.presenter?.presentServiceCenterList(responseDTO: response, error: error)
        }
        print("")
    }
    
    func getServiceCenterDistance(for origin:String,and destinationString:String){
        let requestDto = GoogleDistanceRequestDTO(origins: origin, destinations: destinationString)
        let _ = GoogleDistanceRequestUseCase.service(requestDTO: requestDto) { (usecase, response, error) in
            self.presenter?.receivedServiceCenterDistance(distanceModel: response)
        }
    }
    
}
