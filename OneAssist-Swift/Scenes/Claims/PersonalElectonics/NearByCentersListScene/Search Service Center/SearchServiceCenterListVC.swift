//
//  SearchServiceListVC.swift
//  OneAssist-Swift
//
//  Created by Raj on 01/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import CoreLocation
protocol SearchServiceCenterListBusinessLogic:class {
    func getServiceCenterList(for pincode:String)
    func getServiceCenterDistance(for origin:String,and destinationString:String)
}
protocol SearchServiceCenterListDelegate:class{
    func dismissCall()
}
class SearchServiceCenterListVC: BaseVC, HideNavigationProtocol{
    var interactor: SearchServiceCenterListBusinessLogic?
    var delegate:SearchServiceCenterListDelegate?
    @IBOutlet weak var searchTextField: UITextField!
    var pincode:String!
    @IBOutlet weak var sugestedLabel: TagsRegularGreyLabel!
    @IBOutlet weak var autoDetectLabel: BodyTextRegularBlackLabel!{
        didSet{
            autoDetectLabel.textColor = .dodgerBlue
        }
    }
    var detectLocationBtnBool = false
    let locationManager = Permission.shared.locationManager
    var currentPosition:CLLocationCoordinate2D!
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    deinit {
        Permission.shared.resetData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        searchTextField.becomeFirstResponder()
        setupLocationManager()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    @IBAction func detectLocationAction(_ sender: Any) {
        detectLocationBtnBool = true
        setupLocationManager(true)
    }
    
    func setupLocationManager(_ showErrorAlert: Bool = false) {
        Permission.shared.checkLocationPermission {[weak self] (status) in
            guard let self = self else {return}
            if status == .authorizedAlways || status == .authorizedWhenInUse {
                self.locationManager.delegate = self
                self.locationManager.distanceFilter = kCLDistanceFilterNone
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.startUpdatingLocation()
                
            }else if status ==  .notDetermined{
            
            }else {
                if showErrorAlert {
                    self.showOpenSettingsAlert(title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting, nil)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SearchServiceCenterListVC:UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location == 5 && string != "" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.getList(for: textField.text ?? "")
            })
        }else if range.location  == 6 && string != ""{
            return false
        }
        return true
    }
    func getList(for pincode:String){
        self.pincode = pincode
        searchTextField.text = pincode
        Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
        interactor?.getServiceCenterList(for:pincode)
    
    }
}
extension SearchServiceCenterListVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if detectLocationBtnBool {
            manager.stopUpdatingLocation()
            manager.delegate = nil
            let userLocation:CLLocation = locations[0] as CLLocation
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
                if let placeMark: CLPlacemark = placemarks?[0] {
                    if let pincode = placeMark.postalCode{
                        self.getList(for: pincode)
                    }
                }
            }
        }else{
            guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
            currentPosition = locValue
        }
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
    }
}
extension SearchServiceCenterListVC {
    
    /// Configure interactor and presenter
    fileprivate func setupDependencyConfigurator() {
        let interactor = SearchServiceCenterListInteractor()
        let presenter = SearchServiceCenterListPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
extension SearchServiceCenterListVC:SearchServiceCenterListDisplayLogic{
   
    func displayServiceCenterList(datasource: ServiceCenterListResponceDTO?,detinationString:String) {
        if datasource?.data?.count == 0 || datasource?.data == nil{
            Utilities.removeLoader(count: &loaderCount)
            sugestedLabel.text = Strings.ServiceRequest.labelText.noResultFound
        }else{
            guard let currentPosition = currentPosition else {
                Utilities.removeLoader(count: &loaderCount)
                return
            }
            CacheManager.shared.pincodeSelectedForServiceCenter = pincode
            CacheManager.shared.currentLocation = currentPosition
            interactor?.getServiceCenterDistance(for: "\(currentPosition.latitude),\(currentPosition.longitude)", and: detinationString )
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        sugestedLabel.text = Strings.ServiceRequest.labelText.noResultFound
                print("")
    }
    func displayFinalServiceCenterList() {
        Utilities.removeLoader(count: &loaderCount)
        delegate?.dismissCall()
        self.navigationController?.popViewController(animated: false)
    }
    
    
}
