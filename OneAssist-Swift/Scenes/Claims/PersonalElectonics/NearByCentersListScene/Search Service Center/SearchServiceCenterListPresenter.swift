//
//  SearchServiceCenterListPresenter.swift
//  OneAssist-Swift
//
//  Created by Raj on 07/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol SearchServiceCenterListDisplayLogic: class {
    func displayServiceCenterList(datasource:ServiceCenterListResponceDTO?,detinationString:String)
    func displayFinalServiceCenterList()
    func displayError(_ error: String)
}

class SearchServiceCenterListPresenter: BasePresenter {
  weak var viewController: SearchServiceCenterListDisplayLogic?
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SearchServiceCenterListPresenter:SearchServiceCenterPresentationLogic{
    
    
    func presentServiceCenterList(responseDTO:ServiceCenterListResponceDTO?,error:Error?){
        do {
            try Utilities.checkError(responseDTO, error: error)
            
            if responseDTO?.data?.count ?? 0 > 0 {
            CacheManager.shared.citySelectedForServiceCenter = responseDTO?.data?[0].city ?? ""
            CacheManager.shared.stateSelectedForServiceCenter = responseDTO?.data?[0].state ?? ""
            //CacheManager.shared.pincodeSelectedForServiceCenter = responseDTO?.data?[0].pincode ?? ""
           var destinationLatLong = ""
            CacheManager.shared.serviceCenterListMarkers = []
            responseDTO?.data?.forEach({ (value) in
                if value.latitude != nil && value.longitude != nil && value.partnerBUOperationHours?.count != 0 {
                    let position = CLLocationCoordinate2D(latitude: Double(value.latitude ?? "0") ?? 0 , longitude: Double(value.longitude ?? "0") ?? 0)
                    var openToday = ""
                    var clossesAt = ""
                    destinationLatLong = destinationLatLong + "\(position.latitude),\(position.longitude)|"
                    var dateFormatter = DateFormatter()
                    dateFormatter.calendar = gregorianCalendar
                    dateFormatter = .time24period
                    if  let hourModel = value.partnerBUOperationHours?.flatMap({ (model) -> PartnerBUOperationHour? in
                        if model.weekDay == Constants.getWeekday{
                            return model
                        }
                        return nil
                    }),hourModel.count != 0 {
                        // will check if service center is open or not currently
                        if let responseToTime = dateFormatter.date(from: hourModel[0].operationTimeTo ?? "")?.time ,let responseFromTime = dateFormatter.date(from: hourModel[0].operationTimeFrom ?? "")?.time,let time = dateFormatter.date(from: dateFormatter.string(from: Constants.getCurrentDate))?.time,time <= responseToTime && time >= responseFromTime && hourModel[0].operational == "Y" {
                            let toStringDate = "06-May-2019 " + (hourModel[0].operationTimeTo ?? "") + ":00"
                            dateFormatter = .scheduleFormat
                            let toDate = dateFormatter.date(from:toStringDate)
                            openToday = "Open today:"
                            dateFormatter = .time12period
                            clossesAt = "Closes at \(dateFormatter.string(from: toDate ?? Date()))"
                        }else{
                            let fromStringDate = "06-May-2019 " + (hourModel[0].operationTimeFrom ?? "") + ":00"
                            dateFormatter = .scheduleFormat
                            let toDate = dateFormatter.date(from:fromStringDate)
                            dateFormatter = .time12period
                            clossesAt = "Open at \(dateFormatter.string(from: toDate ?? Date()))"
                            openToday = "Closed:"
                        }
                    }
                    
                    var address  = ""
                    if let line1 = value.line1 as? String {
                        address += "\(line1)"
                    }
                    if let line2 = value.line2 as? String {
                        address += ", \(line2)"
                    }
                    if let landmark = value.landmark {
                        address += ", \(landmark)"
                    }
                    if let pin = value.pincode {
                        address += " - \(pin)"
                    }
                    //save the markers, will be used later on map and service center list
                    CacheManager.shared.serviceCenterListMarkers.append(ServiceCenterClusterMarkerModel(position: position, centerName: value.businessUnitName, centerAddress: address, activeIcon: #imageLiteral(resourceName: "Group 24 Copy 10"), mobile: "\(value.mobile ?? 0)", openToday: openToday, clossesAt: clossesAt, timingDetails: value.partnerBUOperationHours, distance: 0,kmDistance:""))
                }
            })
                viewController?.displayServiceCenterList(datasource:responseDTO,detinationString:String(destinationLatLong.dropLast()))
            }else{
               viewController?.displayServiceCenterList(datasource:nil,detinationString:"")
            }
            
        }catch{
            viewController?.displayError(error.localizedDescription)
        }
    }

    func receivedServiceCenterDistance(distanceModel:GoogleDistanceResponseDTO?){
        if let distinations = distanceModel?.rows?[0].elements, distinations.count > 0 {
            for (index, distination) in distinations.enumerated() {
                //Update the distance, single source(row: 0), multiple distination
                CacheManager.shared.serviceCenterListMarkers[index].distance = distination.distance?.value?.doubleValue ?? 0
                CacheManager.shared.serviceCenterListMarkers[index].kmDistance = distination.distance?.text ?? ""
            }
        }
        // sort by distance 
        CacheManager.shared.serviceCenterListMarkers.sort { (a, b) -> Bool in
           return a.distance < b.distance
        }
        viewController?.displayFinalServiceCenterList()
    }
}
