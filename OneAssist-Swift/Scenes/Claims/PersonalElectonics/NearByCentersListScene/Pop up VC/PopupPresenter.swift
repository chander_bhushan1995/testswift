//
//  PopupPresentar.swift
//  OneAssist-Swift
//
//  Created by Raj on 28/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol PopupListDisplayLogic: class {
    func diviceBackConfirm(status:DeviceBackResponseDTO?)
    func displayError(_ error: String)
}

class PopupPresenter:BasePresenter{
    weak var viewController: PopupListDisplayLogic?
}
extension PopupPresenter:PpoupPresentationLogic{
    func receivedStatus(resposeDTO: DeviceBackResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(resposeDTO, error: error)
            viewController?.diviceBackConfirm(status:resposeDTO  )
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
   
}
