//
//  PopupVC.swift
//  OneAssist-Swift
//
//  Created by Raj on 28/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol PopupVCDelegate:class{
    func deviceBackSuccess()
}

protocol PopupBusinessLogic:class {
    func wantMyDeviceBack(for request:DeviceBackRequestDTO)
}

class PopupVC: BaseVC {
    var delegate:PopupVCDelegate!
    var interactor: PopupBusinessLogic?
    var isCheckBoxSelected = false
    var opacityComponent:CGFloat = 0.7
    @IBOutlet weak var itemView: DLSShadowView!
    @IBOutlet weak var submitBtn: OAPrimaryButton!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    var srID :String!
    var backgroundColor:UIColor = .black
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = backgroundColor.withAlphaComponent(opacityComponent)
        view.isOpaque = false
        submitBtn.isEnabled = false
        checkBoxImageView.image = #imageLiteral(resourceName: "appliance_checkbox_unselected")
        setupDependencyConfigurator()
        // Do any additional setup after loading the view.
    }
    @IBAction func crossClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func checkBoxClicked(_ sender: Any) {
        if isCheckBoxSelected{
           checkBoxImageView.image = #imageLiteral(resourceName: "appliance_checkbox_unselected")
            isCheckBoxSelected = false
            submitBtn.isEnabled = false
        }else{
            checkBoxImageView.image = #imageLiteral(resourceName: "check_blue")
            isCheckBoxSelected = true
            submitBtn.isEnabled = true
        }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        let touch = touches.first!
        let location = touch.location(in: view)
        
        if !itemView.frame.contains(location) {
            dismiss(animated: false) {
                // send data
            }
        }
    }
    @IBAction func submitClicked(_ sender: Any) {
        //event
        EventTracking.shared.eventTracking(name: .BERConsent, [.consent : "No",  .subcategory : CacheManager.shared.eventProductName ?? "", .service:CacheManager.shared.srType?.rawValue ?? ""])

        
        let delivery = Delivery2(berOptOutAcknowledgment: "Y")
        let workflowData = WorkflowData2(delivery: delivery)
        let deviceBackRequestDTo = DeviceBackRequestDTO(modifiedBy: "unite", serviceRequestId: NSNumber(value: Int(srID) ?? 0), workflowData: workflowData)
        Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
        interactor?.wantMyDeviceBack(for:deviceBackRequestDTo)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PopupVC {
    
    /// Configure interactor and presenter
    fileprivate func setupDependencyConfigurator() {
        let interactor = PopupInteractor()
        let presenter = PopupPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
extension PopupVC:PopupListDisplayLogic{
    func diviceBackConfirm(status:DeviceBackResponseDTO?){
         Utilities.removeLoader(count: &loaderCount)
        self.dismiss(animated: false) {
            self.delegate.deviceBackSuccess()
        }
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        let alert = UIAlertController(title: Strings.ServiceRequest.AlertMessage.title.technicalError, message: Strings.ServiceRequest.AlertMessage.message.cantUpdateStatus, preferredStyle: .alert)
        let okayAction2 = UIAlertAction(title: "OK", style: .default) { (alert) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okayAction2)
        self.presentInFullScreen(alert, animated: true, completion: nil)
    }
    
    
}
