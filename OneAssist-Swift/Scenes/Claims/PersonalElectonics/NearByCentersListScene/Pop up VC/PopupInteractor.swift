//
//  PopupInterector.swift
//  OneAssist-Swift
//
//  Created by Raj on 28/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol PpoupPresentationLogic {
    func receivedStatus(resposeDTO: DeviceBackResponseDTO?, error: Error?)
}
class PopupInteractor:BaseInteractor{
    var presenter: PpoupPresentationLogic?
    
}
extension PopupInteractor:PopupBusinessLogic{
    func wantMyDeviceBack(for request: DeviceBackRequestDTO) {
        
        let _ = DeviceBackRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            
            self.presenter?.receivedStatus(resposeDTO: response, error: error)
            
        }
    }
}
