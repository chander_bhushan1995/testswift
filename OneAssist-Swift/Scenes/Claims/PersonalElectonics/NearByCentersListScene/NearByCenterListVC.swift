//
//  NearByCenterListVC.swift
//  OneAssist-Swift
//
//  Created by Raj on 26/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol NearByCenterListVCDelegate:class {
    func cellClicked(model:ServiceCenterClusterMarkerModel)
}

class NearByCenterListVC: BaseVC {
    @IBOutlet weak var searchTextField: UITextField!
    var refreshControl: UIRefreshControl?
    @IBOutlet weak var tableView: UITableView!
    var delegate:NearByCenterListVCDelegate!
    @IBOutlet weak var showingResultForLabel: BodyTextBoldBlackLabel!
    var pincode:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate =  self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.CellIdentifiers.centerList, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.centerList)
        initialView()
        searchTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchTextField.text = CacheManager.shared.pincodeSelectedForServiceCenter
        showingResultForLabel.text = """
        "\(CacheManager.shared.pincodeSelectedForServiceCenter ?? ""), \(CacheManager.shared.citySelectedForServiceCenter ?? ""), \(CacheManager.shared.stateSelectedForServiceCenter ?? "")"
        """
    }
    
    func initialView(){
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl!)
        tableView.sendSubviewToBack(refreshControl!)
    }
   @objc func handleRefresh() {
    self.refreshControl?.endRefreshing()
    self.tableView.reloadData()
    }
    @objc func mapClicked(){
        let vc = NearByCentersMapVC()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NearByCenterListVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CacheManager.shared.serviceCenterListMarkers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.centerList, for: indexPath) as! CenterListCell
        cell.indexPath = indexPath
        cell.set(title: CacheManager.shared.serviceCenterListMarkers[indexPath.row].centerName, address: CacheManager.shared.serviceCenterListMarkers[indexPath.row].centerAddress, openToday: CacheManager.shared.serviceCenterListMarkers[indexPath.row].openToday, closeTime: CacheManager.shared.serviceCenterListMarkers[indexPath.row].clossesAt, distance: CacheManager.shared.serviceCenterListMarkers[indexPath.row].kmDistance, currentPosition: CacheManager.shared.currentLocation, destinationPosition: CacheManager.shared.serviceCenterListMarkers[indexPath.row].position, mobile: CacheManager.shared.serviceCenterListMarkers[indexPath.row].mobile )
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.cellClicked(model:CacheManager.shared.serviceCenterListMarkers[indexPath.row] )
        print("Selected")
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 140
//    }
}
extension NearByCenterListVC:NearByCenterCellDelegate{
    func closesAtClicked(indexPath:IndexPath) {
        let vc = ServiceCenterTimeScheduleVC()
        vc.timingDetails = CacheManager.shared.serviceCenterListMarkers[indexPath.row].timingDetails
        let basePickerVC = BasePickerVC(withView:vc.view, height:300)
        presentInFullScreen(basePickerVC, animated: true)
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
}
extension NearByCenterListVC:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc =  SearchServiceCenterListVC()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
        return false
    }
}
extension NearByCenterListVC:SearchServiceCenterListDelegate{
    func dismissCall(){
        searchTextField.text = CacheManager.shared.pincodeSelectedForServiceCenter
        showingResultForLabel.text = """
        "\(CacheManager.shared.pincodeSelectedForServiceCenter ?? ""), \(CacheManager.shared.citySelectedForServiceCenter ?? ""), \(CacheManager.shared.stateSelectedForServiceCenter ?? "")"
        """
        tableView.reloadData()
    }
}

