//
//  NearByCentersMarkerView.swift
//  OneAssist-Swift
//
//  Created by Raj on 08/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation
class ServiceCenterMarkerView: GMSMarker {
    
    var centerName : String?
    var centerAddress : String?
    var centerPhoneNumber : String?
    var centerCoordinates : CLLocationCoordinate2D?
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
extension ServiceCenterMarkerView {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}
