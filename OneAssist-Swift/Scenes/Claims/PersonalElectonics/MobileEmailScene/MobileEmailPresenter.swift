//
//  MobileEmailPresenter.swift
//  OneAssist-Swift
//
//  Created by Raj on 24/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol MobileEmailDisplayLogic: class {
    func displayUpdatedResponse(_ error: String)
    func updatesSuccessfully(customer: MobileEmailResponseDTO?)
}
class MobileEmailPresenter: BasePresenter {
 weak var viewController: MobileEmailDisplayLogic?
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MobileEmailPresenter:MobileEmailPresentationLogic{
    
    func updatedResponseRecieved(response: MobileEmailResponseDTO? , error: Error?){
        do {
            try Utilities.checkError(response, error: error)
            viewController?.updatesSuccessfully(customer:response)
        } catch {
            viewController?.displayUpdatedResponse(error.localizedDescription)
        }
    }
    
    
}
