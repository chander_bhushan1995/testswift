//
//  MobileEmailVC.swift
//  OneAssist-Swift
//
//  Created by Raj on 24/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol MobileEmailBusinessLogic:class {
    func saveAlternateMobileEmail(for request:MobileEmailRequestDTO?)
}


class MobileEmailVC: BaseVC {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var discriptionLabel: BodyTextRegularGreyLabel!
    @IBOutlet var emailTF: TextFieldView!
    @IBOutlet var mobileTF: TextFieldView!
    var mobileTFBool = false
    var emailTFBool = false
    var interactor: MobileEmailBusinessLogic?
    @IBOutlet weak var mobileHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailHeightContraint: NSLayoutConstraint!
    var srID:String!
    @IBOutlet weak var addBtn: OAPrimaryButton!
    @IBOutlet weak var addBtnTopConstraint: NSLayoutConstraint!
    var textFieldHandler: TextFieldViewDelegateHandler!
    var toastText:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    private func initializeView() {
        self.addBtn.isEnabled = false
        if mobileTFBool && emailTFBool{
            titleLabel.text = "Never miss any update on your claim status."
            discriptionLabel.text = "Add your email id & mobile number for instant and regular updates."
            emailTF.fieldType = EmailFieldType.self
            emailTF.delegate = self
            emailTF.placeholderFieldText = Strings.ServiceRequest.textFieldText.emailPlaceholder
            emailTF.descriptionText =  Strings.ServiceRequest.textFieldText.emailDescription
            emailTF.isUserInteractionEnabled  = true
            
            mobileTF.fieldType = MobileFieldType.self
            mobileTF.delegate = self
            mobileTF.placeholderFieldText =   Strings.ServiceRequest.textFieldText.mobilePlaceholder
            mobileTF.descriptionText = Strings.ServiceRequest.textFieldText.mobileDescription
            mobileTF.isUserInteractionEnabled  = true
            textFieldHandler = TextFieldViewDelegateHandler(requiredFields: [emailTF,mobileTF]) {[weak self] (isValid,_) in
                
                guard self != nil else {
                    return
                }
                self!.addBtn.isEnabled = isValid
                
            }

            //addBtnTopConstraint.constant = 208
        }else if emailTFBool{
            mobileHeightConstraint.constant = 0
            titleLabel.text = "Get every details of your claim status,right in your inbox."
            discriptionLabel.text = "Add your email ID & stay connected."
            emailTF.fieldType = EmailFieldType.self
            emailTF.delegate = self
            emailTF.placeholderFieldText = Strings.ServiceRequest.textFieldText.emailPlaceholder
            emailTF.descriptionText =  Strings.ServiceRequest.textFieldText.emailDescription
            emailTF.isUserInteractionEnabled  = true
            addBtnTopConstraint.constant = 116
            textFieldHandler = TextFieldViewDelegateHandler(requiredFields: [emailTF]) {[weak self] (isValid,_) in
                
                guard self != nil else {
                    return
                }
                self!.addBtn.isEnabled = isValid
                
            }
        }else if mobileTFBool{
            emailHeightContraint.constant = 0
            titleLabel.text = "Get your cliam status updates,right at your finger tips."
            discriptionLabel.text = "Add you alternate mobile number."
            mobileTF.fieldType = MobileFieldType.self
            mobileTF.delegate = self
            mobileTF.placeholderFieldText =   Strings.ServiceRequest.textFieldText.mobilePlaceholder
            mobileTF.descriptionText = Strings.ServiceRequest.textFieldText.mobileDescription
            mobileTF.isUserInteractionEnabled  = true
            addBtnTopConstraint.constant = 116
            textFieldHandler = TextFieldViewDelegateHandler(requiredFields: [mobileTF]) {[weak self] (isValid,_) in
                
                guard self != nil else {
                    return
                }
                self!.addBtn.isEnabled = isValid
               
            }
            
        }
        addBtn.setTitle(Strings.ServiceRequest.buttonName.add, for: .normal)
    }
    
    func validateFormField()->Bool{
        do {
            try Validation.validateEmail(text: self.emailTF.fieldText)
            return true
        }catch ValidationError.invalidEmail(let error) {
            self.emailTF.setError(error)
        }catch {}
        return false
    }
    
    @IBAction func AddAction(_ sender: Any) {
        var request:MobileEmailRequestDTO!
        let serviceRequestDetail:ServiceRequestAddressDetail2!
        
        if emailTFBool && !(validateFormField()){
            return
        }
        
        if mobileTFBool && emailTFBool{
            EventTracking.shared.eventTracking(name: .mobileEmailSubmission, [.serviceId: srID ?? "", .mobile: mobileTF.fieldText ?? "", .email: emailTF.fieldText ?? ""])
            serviceRequestDetail = ServiceRequestAddressDetail2(alternateMobileNo: NSNumber(value: Int(mobileTF.fieldText ?? "0") ?? 0), email: emailTF.fieldText)
            //request = MobileEmailRequestDTO(alternateEmail: emailTF.fieldText, alternateMobile: mobileTF.fieldText, serviceRequestId: srID)
            toastText = "Email ID & mobile number added successfully"
        }else if emailTFBool{
            EventTracking.shared.eventTracking(name: .mobileEmailSubmission, [.serviceId: srID ?? "", .email: emailTF.fieldText ?? ""])
             serviceRequestDetail = ServiceRequestAddressDetail2(alternateMobileNo: nil, email: emailTF.fieldText)
            //request = MobileEmailRequestDTO(alternateEmail: emailTF.fieldText, alternateMobile: nil, serviceRequestId: srID)
            toastText = "Email ID added successfully"
        }else{
            EventTracking.shared.eventTracking(name: .mobileEmailSubmission, [.serviceId: srID ?? "", .mobile: mobileTF.fieldText ?? ""])
             serviceRequestDetail = ServiceRequestAddressDetail2(alternateMobileNo: NSNumber(value: Double(mobileTF.fieldText ?? "0") ?? 0), email: nil)
           // request = MobileEmailRequestDTO(alternateEmail: nil, alternateMobile: mobileTF.fieldText, serviceRequestId: srID)
            toastText = "Mobile number added successfully"
        }
        request = MobileEmailRequestDTO(serviceRequestAddressDetails: serviceRequestDetail, serviceRequestId: NSNumber(value: Int(srID ?? "0") ?? 0))
         Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        interactor?.saveAlternateMobileEmail(for: request)
        print(srID ?? "")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func crossAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
extension MobileEmailVC: TextFieldViewDelegate{
    func textFieldViewDidBeginEditing(_ textFieldView: TextFieldView) {
    }
    
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textFieldView == self.emailTF {
           
        }else if textFieldView == self.mobileTF{
            
        }
        return true
    }
}
extension MobileEmailVC:MobileEmailDisplayLogic{
    func displayUpdatedResponse(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    
    func updatesSuccessfully(customer: MobileEmailResponseDTO?){
        
        self.dismiss(animated: true) {
            let window = UIApplication.shared.keyWindow
            window?.rootViewController?.view.makeToast(self.toastText)
        }
    }
    
}
extension MobileEmailVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = MobileEmailInteractor()
        let presenter = MobileEmailPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
