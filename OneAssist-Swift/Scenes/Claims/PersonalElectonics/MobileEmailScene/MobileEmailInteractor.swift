//
//  MobileEmailInteractor.swift
//  OneAssist-Swift
//
//  Created by Raj on 24/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol MobileEmailPresentationLogic {
    func updatedResponseRecieved(response: MobileEmailResponseDTO? , error: Error?)
}

class MobileEmailInteractor: BaseInteractor {
 var presenter: MobileEmailPresentationLogic?
   
    

    /* MobileEmailRequestUseCase
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MobileEmailInteractor:MobileEmailBusinessLogic{
    func saveAlternateMobileEmail(for request: MobileEmailRequestDTO?) {
        let _ = MobileEmailRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.updatedResponseRecieved(response: response, error: error)
        }
    }
    
    
}
