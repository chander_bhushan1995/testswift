//
//  InsuranceDecisionCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/21/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This Delegate contains callback method of Insurance decision cell buttons.
protocol InsuranceDecisionCellDelegate: class {
    
    /// Give callback on click of "Why BER" button
    ///
    /// - Parameters:
    ///   - cell: Current cell
    ///   - sender: Button on which tapped
    func insuranceDecisionCell(_ cell: InsuranceDecisionCell, clickedBtnBER sender: Any,indexPath:IndexPath)
    
    /// Give callback on click of "Enter bank details" button
    ///
    /// - Parameters:
    ///   - cell: Current cell
    ///   - sender: button on which tapped
    func insuranceDecisionCell(_ cell: InsuranceDecisionCell, clickedBtnEnterDetails sender: Any,indexPath:IndexPath)
    func cardSecondBtnClicked(indexPath:IndexPath)
}

/// Insurance Decision model
struct InsuranceDecisionCellViewModel {
    var completeTitle: String
    var boldText: String
    var berDetailModel: ClaimBERViewModel
    var bankDetailDescription: String
}

/// Used to display insurance decision cell view
class InsuranceDecisionCell: UITableViewCell {

//    @IBOutlet weak var cardTitleLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardTitleLabel: H3BoldLabel!
    
    @IBOutlet weak var curvedViewOptionalTop: NSLayoutConstraint!
    @IBOutlet weak var saperatorView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var berView: UIView!
    @IBOutlet weak var titleLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var curvedView: DLSShadowView!
    @IBOutlet weak var provideLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var actionButton: PrimaryButton!
    @IBOutlet weak var whyIsItBerButton: UIButton!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: OABadgeLabel!
    @IBOutlet weak var badgeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelEnterDetails: H3RegularBlackLabel!

    @IBOutlet weak var cardSecondBtn: UIButton!
    @IBOutlet weak var cardSecondBtnHeightConstraint: NSLayoutConstraint!
    
//    @IBOutlet weak var berViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var discriptionLabelTopConstraint: NSLayoutConstraint!
    var claimBERModel: ClaimBERViewModel?
    weak var delegate: InsuranceDecisionCellDelegate?
    var indexPath:IndexPath!
    var srType:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()

//        curvedView.layer.borderColor = UIColor.dlsBorder.cgColor
//        curvedView.layer.cornerRadius = 6.0
//        curvedView.layer.borderWidth = 1.0
//        badgeView.layer.cornerRadius = 2.0
//        badgeView.layer.borderWidth = 1.0
    }

    /// Used to update cell
    ///
    /// - Parameters:
    ///   - completeTitle: Cell Description text
    ///   - boldText: Text in bold
    ///   - bankDescription: Bank Description
    func setViewModel(_ model:TimeLineViewModel.InsuranceDecisionCellModel, _ delegate: InsuranceDecisionCellDelegate?){
        
        self.indexPath = model.indexPath
        self.delegate = delegate
        titleLabel.attributedText = model.completeTitle?.getAttributedDescription()
        if let string = model.bankDescription, string != "" {
            provideLabel.text = string
        }
        if let text = model.badgeText {
            
            // If badge is to be shown
            badgeLabel?.text = text
//            badgeLabel?.setNeedsLayout()
//            badgeLabel?.layoutIfNeeded()
//            badgeLabel?.textColor = .orangePeel
           // let color:UIColor = .orangePeel
            //badgeView.layer.borderColor = color.cgColor
           // badgeView.backgroundColor = UIColor.orangePeel.withAlphaComponent(0.12)
        } else {
            // Badge hidden, adjust constraitn
            badgeView?.removeFromSuperview()
//            badgeView.isHidden = true
//            badgeLabel.isHidden = true
//            badgeLabel.text = ""
//            badgeViewHeightConstraint.constant = 0
//            discriptionLabelTopConstraint.constant = -10
        }
        if let text = model.cardTitleText {
            cardTitleLabel.isHidden = false
            cardTitleLabel.text = text
//            cardTitleLabelHeightConstraint.constant = 75
            
        } else {
            cardTitleLabel.isHidden = true
//            cardTitleLabelHeightConstraint.constant = 0
        }
        if let text = model.button2Text {
            cardSecondBtn.setTitle(text, for: .normal)
            cardSecondBtn.isEnabled = model.button2Enable ?? false
            cardSecondBtnHeightConstraint.constant = 44
        } else {
            cardSecondBtnHeightConstraint.constant = 0
            cardSecondBtn.isHidden = true
        }

        whyIsItBerButton?.setTitle(model.whyIsItBerButtonText, for: .normal)
        whyIsItBerButton?.isEnabled = model.whyIsItBerButtonEnable ?? false
        actionButton.setTitle(model.buttonText, for: .normal)
        
        if let cardCount = model.cardCount,cardCount == 2  {
        hideBERView()
        saperatorView.isHidden = true
        } else if model.whyIsItBerButtonText == nil {
            hideBERView()
        }
        else{
            hideBERView(false)
        }
        indexPath = model.indexPath
        actionButton.isEnabled = model.buttonEnable ?? false
        
    }
    
    /// Clicked On Why BER button
    @IBAction func clickedBtnBER(_ sender: Any) {
        delegate?.insuranceDecisionCell(self, clickedBtnBER: sender,indexPath:indexPath)
    }
    @IBAction func clickedCardSecondBtn(_ sender: Any) {
        delegate?.cardSecondBtnClicked(indexPath:indexPath)
    }
    
    /// Clicked on Enter bank details button
    @IBAction func clickedBtnEnterDetails(_ sender: Any) {
        //event
        EventTracking.shared.eventTracking(name: .BERConsent, [.consent : "YES",  .subcategory : CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? "",])

        delegate?.insuranceDecisionCell(self, clickedBtnEnterDetails: sender,indexPath:indexPath)
    }
}

extension InsuranceDecisionCell{
    private func hideBERView(_ hide:Bool = true){
        if hide {
            berView?.removeFromSuperview()
        }

//        berView.isHidden = hide
//        curvedView.layer.borderWidth = hide ? 0 : 1
//        bottomConstraint.constant = hide ? 0 : 28
//        curvedViewOptionalTop.priority = hide ? UILayoutPriority.defaultHigh:UILayoutPriority.defaultLow
    }
}
