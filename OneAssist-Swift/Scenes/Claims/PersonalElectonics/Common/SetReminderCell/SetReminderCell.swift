

//
//  SetReminderCell.swift
//  OneAssist-Swift
//
//  Created by Raj on 06/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol SetReminderCellDelegate:class
{
    func setReminderClicked(dateToSend:String?)
}

class SetReminderCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var setReminderBtn: OAPrimaryButton!
    @IBOutlet weak var calendarView: OACalendarView!
    var currentLblDate:Date?
    var currentLblTime:String?
    var delegate:SetReminderCellDelegate?
    var dateToSend:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialise()
        // Initialization code
    }
    private func initialise(){
        setReminderBtn.isEnabled = false
        calendarView.delegate = self
        calendarView.minimumDate = Date()
        calendarView.maximumDate = Date(timeIntervalSinceNow: 24*60*60*365.25) //1 year from now
        calendarView.firstDay = .Today
        calendarView.secondDay = .Tomorrow
        calendarView.selectedDay = .Tomorrow
//        calendarView.date = Date.tomorrow
    }
    
    @IBAction func setReminderAction(_ sender: Any) {
        delegate?.setReminderClicked(dateToSend:self.dateToSend )
    }
    
}
extension SetReminderCell: OACalendarViewDelegate{
    func dateChanged(_ date: Date, view: OACalendarView) {
         setReminderBtn.isEnabled = true
        let dateFormatter = DateFormatter.scheduleFormat
        dateToSend =  dateFormatter.string(from: date)
    }
    
}
