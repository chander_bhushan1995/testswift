//
//  SPExcessPaymentCell.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 18/09/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Screen Protection Excess Payment Delegate
protocol SPExcessPaymentDelegate: ExpandViewDelegate {
    
    /// Clicked on Excess Payment's primary button in Screen protection
    ///
    /// - Parameters:
    ///   - cell: Cell object which tapped
    ///   - sender: Button object
    func spExcessPaymentCell(_ cell: SPExcessPaymentCell, clickedBtnPrimary sender: Any)
    
    /// Clicked on Excess Payment's secondry button in Screen protection
    ///
    /// - Parameters:
    ///   - cell: Cell object which tapped
    ///   - sender: Button object
    func spExcessPaymentCell(_ cell: SPExcessPaymentCell, clickedBtnSecondry sender: Any)
}

/// Screen Protection Excess Payment model
struct SPExcessPaymentModel {
    var badgeText: String?
    var stageDescription: String?
    var partList: [ExpandModel]?
    var primaryButtonName: String?
    var refundDescription: String?
}

/// Used to display screen protection excess payment cell
class SPExcessPaymentCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var expandView: ExpandView!
    @IBOutlet weak var primaryButton: OAPrimaryButton!
    @IBOutlet weak var repairMyselfButton: UIButton!
    @IBOutlet weak var refundLabel: UILabel!
    @IBOutlet weak var refundView: UIView!
    
    weak var delegate: SPExcessPaymentDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
        // Initialization code
    }
    
    func initializeView() {
        containerView.layer.borderWidth = 1.0
        containerView.layer.cornerRadius = 4.0
        containerView.layer.borderColor = UIColor.dlsBorder.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// Used to update UI
    ///
    /// - Parameter model: screen protection model
    func updateUserInterface(model: SPExcessPaymentModel) {
        
        descriptionLabel.text = model.stageDescription
        primaryButton.titleLabel?.text = model.primaryButtonName
        addPartList(partList: model.partList ?? [ExpandModel]())
        
        //TODO: Remove given code to show bottom view and uncomment the commented last line
        if refundView != nil {
            refundView.removeFromSuperview()
        }
//        refundLabel.text = model.refundDescription
    }
    
    /// Add List view in cell
    ///
    /// - Parameter partList: part list model
    private func addPartList(partList: [ExpandModel]) {
        
        // check if view is already added
        guard expandView.subviews.count == 0 else {
            return
        }
        
        //TODO: Remove height constraint from refund view
        // Add List view
        let partView = Bundle.main.loadNibNamed("ExpandView", owner: nil, options: nil)?.first as! ExpandView

        partView.dataSource = partList
        partView.updateUI()
        partView.sectionDelegate = delegate
        expandView.addSubview(partView)
        expandView.frame = partView.frame
        
        // Add constraints
        partView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.init(item: partView, attribute: .top, relatedBy: .equal, toItem: expandView, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint.init(item: partView, attribute: .bottom, relatedBy: .equal, toItem: expandView, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint.init(item: partView, attribute: .leading, relatedBy: .equal, toItem: expandView, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint.init(item: partView, attribute: .trailing, relatedBy: .equal, toItem: expandView, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        expandView.needsUpdateConstraints()

    }
    
    /// Clicked on Primary button
    @IBAction func clickedPrimaryButton(_ sender: Any) {
        delegate?.spExcessPaymentCell(self, clickedBtnPrimary: sender)
    }
    
    /// Clicked on Secondary button
    @IBAction func clickedButtonSecondry(_ sender: Any) {
        delegate?.spExcessPaymentCell(self, clickedBtnSecondry: sender)
    }
    
}
