//
//  WalkInCell.swift
//  OneAssist-Swift
//
//  Created by Raj on 25/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol WalkInCellDelegate:class{
    func seeListClicked(indexPath:IndexPath)
}
class WalkInCell: UITableViewCell , NibLoadableView, ReuseIdentifier{

    @IBOutlet weak var seeListBtn: OASecondaryButton!
    @IBOutlet weak var pincodeLabel: TagsRegularGreyLabel!
    @IBOutlet weak var serviceCenterLabel: SupportingTextBoldBlackLabel!
    weak var delegate:WalkInCellDelegate!
    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var subDescriptionLabel: TagsRegularGreyLabel!
    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setViewModel(_ model: TimeLineViewModel.WalkInCellModel, _ delegate: WalkInCellDelegate?) {
        
        self.delegate = delegate
        self.indexPath = model.indexPath
        
        pincodeLabel.text = model.pincodeText
        serviceCenterLabel.text = model.serviceCenterText
        descriptionLabel.text = model.descriptionText
        subDescriptionLabel.text = model.subDescriptionText
        seeListBtn.isEnabled = model.seeListBtnEnable
        seeListBtn.setTitle(model.seeListBtnText, for: .normal)
        refreshCell()
        }
    
    func refreshCell(){
     
        switch CacheManager.shared.serviceCenterListMarkers.count{
        case 0:
            serviceCenterLabel.text = "service center found near you."
            seeListBtn.setTitle("See center", for: .normal)
        case 1:
            serviceCenterLabel.text = "1 service center found near you."
            seeListBtn.setTitle("See center", for: .normal)
        case let count:
            serviceCenterLabel.text = "\(count) service centers found near you."
            seeListBtn.setTitle("See List", for: .normal)
        }

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func SeeListClicked(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .walkinSCList, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
        delegate.seeListClicked(indexPath:self.indexPath)
    }
    
}
