//
//  BankDetailsVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/4/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol contains all methods related to Bank details business logic.
protocol BankDetailsBusinessLogic {
    
    /// This method is used to update bank details.
    ///
    /// - Parameters:
    ///   - custId: Customar Id
    ///   - bankDetails: *BankDetails* object which contains all details of customar i.e. customar name, bank account number, IFSC code etc.
    ///   - image: image of cancelled cheque.
    func updateBankDetails(custId: String?, bankDetails: BankDetails?, image: UIImage)
    
    /// This method is used to get Bank details
    ///
    /// - Parameters:
    ///   - custId: Customar Id
    ///   - srnNumber: service request number
    ///   - custBankId: customar bank Id (we don't have it yet)
    func getBankDetails(custId: String?, srnNumber: String?, custBankId: String?)
    
    /// This method is used to get Cancelled cheque image
    ///
    /// - Parameter docId: mongoDB id of image
    func getDocumentImage(docId: String?)
}

/// This protocol is used to give callback that bank details are updated.
protocol UpdateBankDetailDelegate: class {
    /// This method tells that bank details are updated successfully.
    func bankDetailUpdated()
}

/// This class is used to show the bank details.
class BankDetailsVC: BaseVC {
    var interactor: BankDetailsBusinessLogic?
    weak var updateDelegate: UpdateBankDetailDelegate?
    
    @IBOutlet weak var accountHolderNameTF: TextFieldView!
    @IBOutlet weak var buttonFindIFSC: OASecondaryButton!
    @IBOutlet weak var accountNumberTF: TextFieldView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var ifscCodeTF: TextFieldView!
    @IBOutlet weak var viewUploadImage: UIView!
    @IBOutlet weak var uploadImageOuterView: UIView!
    @IBOutlet weak var viewEditImage: UIView!
    @IBOutlet weak var imageViewCheque: UIImageView!
    @IBOutlet weak var buttonSubmit: OAPrimaryButton!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    @IBOutlet weak var buttonUpload: UIButton!
    var spinnerView: SpinnerView?
    
    var isUploaded: Bool = false
    var handler: TextFieldViewDelegateHandler!
    var isTextFilled: Bool = false
    var srNumber: String?
    var bankDetails: BankDetails?
    var eventSubCat = ""
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        getBankDetails()
        EventTracking.shared.addScreenTracking(with: .SR_Bank_Details)
    }
    
    // MARK:- private methods
    /// Used to initialized View
    private func initializeView() {
        setTextField()
        title = Strings.BankDetailsScene.title
        circularView.layer.cornerRadius = 4
        setLayout()
        buttonSubmit.isEnabled = false
        addTap()
        handler = TextFieldViewDelegateHandler(requiredFields: [accountNumberTF, ifscCodeTF, accountHolderNameTF], validationHandler: {[weak self] (isValid, tuple) in
            guard self != nil else {
                return
            }
            self?.isTextFilled = isValid
            self?.toggleSubmitButton()
        })
        
        // add custom loader
        imageLoader.stopAnimating()
        spinnerView = SpinnerView(view: viewEditImage)
        spinnerView?.startAnimating()
    }
    
    /// Used to get bank details
    private func getBankDetails() {
        let custId = UserCoreDataStore.currentUser?.cusId
        Utilities.addLoader(onView: view, message: "Please wait...", count: &loaderCount, isInteractionEnabled: false)
        interactor?.getBankDetails(custId: custId, srnNumber: nil, custBankId: nil)
    }
    
    /// used to set layout of views
    private func setLayout() {
        
        uploadImageOuterView.layer.cornerRadius = 4.0
        uploadImageOuterView.layer.borderColor = UIColor.dlsBorder.cgColor
        uploadImageOuterView.layer.borderWidth = 1.0
        viewEditImage.layer.cornerRadius = 4.0
        viewEditImage.layer.borderColor = UIColor.dlsBorder.cgColor
        viewEditImage.layer.borderWidth = 1.0
    }
    
    /// set text fields' label, placeholder, type etc.
    private func setTextField() {
        
        accountNumberTF.placeholderFieldText = Strings.BankDetailsScene.placeHolderTF
        accountNumberTF.descriptionText = Strings.BankDetailsScene.accountNumber
//        accountNumberTF.textFieldDescriptionView.labelDescription.font = DLSFont.supportingText.regular
        accountNumberTF.fieldType = CurrencyFieldType.self
        
        accountHolderNameTF.placeholderFieldText = Strings.BankDetailsScene.placeHolderTF
        accountHolderNameTF.descriptionText = Strings.BankDetailsScene.accountHolderName
//        accountHolderNameTF.textFieldDescriptionView.labelDescription.font = DLSFont.supportingText.regular
        
        ifscCodeTF.placeholderFieldText = Strings.BankDetailsScene.placeHolderTF
        ifscCodeTF.descriptionText = Strings.BankDetailsScene.ifscCode
//        ifscCodeTF.textFieldDescriptionView.labelDescription.font = DLSFont.supportingText.regular
        
        ifscCodeTF.bringSubviewToFront(buttonFindIFSC)
    }
    
    /// Used to prepare Bank details model
    func prepareBankDetailsModel() {
        
        bankDetails = bankDetails ?? BankDetails(dictionary: [:])
        bankDetails?.custName = accountHolderNameTF.textFieldObj.text
        bankDetails?.accountNo = accountNumberTF.textFieldObj.text
        bankDetails?.bankCode = ifscCodeTF.textFieldObj.text
        bankDetails?.srNumber = srNumber
    }
    
    /// Used to enable or disable submit buttons.
    func toggleSubmitButton() {
        buttonSubmit.isEnabled = isTextFilled && isUploaded
    }
    
    // MARK:- Action Methods
    
    /// This method is called when *Upload* button clicked
    @IBAction func clickedBtnUpload(_ sender: Any) {
        handleImageUpload()
    }
    
    /// This method is called when *Edit* button clicked
    @IBAction func clickedBtnEdit(_ sender: Any) {
        handleImageUpload()
    }
    
    /// This method is called when *How to find IFSC code* button clicked
    @IBAction func clcikedBtnFindIFSCCode(_ sender: Any) {
        let vc = IFSCCodeVC()
        presentInFullScreen(vc, animated: true)
    }
    
    /// This method is called when *Submit* button clicked
    @IBAction func clickedBtnSubmit(_ sender: Any) {
        
        prepareBankDetailsModel()
        Utilities.addLoader(onView: view, message: "submitting", count: &self.loaderCount, isInteractionEnabled: false)
        let custId = UserCoreDataStore.currentUser?.cusId
        interactor?.updateBankDetails(custId: custId, bankDetails: bankDetails, image: imageViewCheque.image!)
        EventTracking.shared.eventTracking(name: .srBankDetailsSubmit, [.subcategory: eventSubCat])
    }
    
    // MARK: Image Upload methods:
    /// Used to show action sheet to upload image.
    private func handleImageUpload() {
        
        // action sheet
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: Strings.Global.uploadImage, style: .default) { (action) in
            ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(image, error)
            })
        }
        
        let action2 = UIAlertAction(title: Strings.Global.clickPhoto, style: .default) { (action2) in
            ImagePickerManager.shared.pickImageFromCamera(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(image, error)
            })
        }
        
        let action3 = UIAlertAction(title: Strings.Global.cancel, style: .cancel) { (action3) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action)
        alertController.addAction(action2)
        alertController.addAction(action3)
        presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    /// This method get callback after getting image from gallary or camera. It returns either image or error.
    ///
    /// - Parameters:
    ///   - image: Sectected image
    ///   - error: Error in image selection
    private func handlePickedImage(_ image: UIImage?, _ error: Error?) {
        if let error = error {
            // Handle error
            print(error)
        } else {
            
            imageViewCheque.image = image
            viewEditImage.isHidden = false
            isUploaded = true
            toggleSubmitButton()
        }
    }
    
    /// Used to get Cancelled cheque image
    ///
    /// - Parameter docId: MongoDB id
    fileprivate func getDocImage(docId: String) {
        
        viewEditImage.isHidden = false
        viewEditImage.isUserInteractionEnabled = false
        buttonUpload.isUserInteractionEnabled = false
//        imageLoader.startAnimating()
        spinnerView?.startAnimating()
        interactor?.getDocumentImage(docId: docId)
    }
}

// MARK:- Display Logic Conformance
extension BankDetailsVC: BankDetailsDisplayLogic {
    
    func displayImageSuccess(image: UIImage?) {
//        imageLoader.stopAnimating()
        spinnerView?.stopAnimating()
        imageViewCheque.image = image
        viewEditImage.isHidden = false
        viewEditImage.isUserInteractionEnabled = true
        buttonUpload.isUserInteractionEnabled = true
        isUploaded = true
        toggleSubmitButton()
    }
    
    func displayImageFailure(error: String?) {
//        imageLoader.stopAnimating()
        spinnerView?.stopAnimating()
        viewEditImage.isHidden = true
        viewEditImage.isUserInteractionEnabled = true
        buttonUpload.isUserInteractionEnabled = true
    }
    
    func bankDetailUpdateSuccess() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if updateDelegate == nil {
            navigationController?.popViewController(animated: true)
        } else {
            updateDelegate?.bankDetailUpdated()
        }
    }
    
    func bankDetailUpdateFailure(error: String?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error ?? "")
    }
    
    func displayBankDetails(response: BankDetails?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)

        accountHolderNameTF.textFieldObj.text = response?.custName
        ifscCodeTF.textFieldObj.text = response?.bankCode
        accountNumberTF.textFieldObj.text = response?.accountNo
        
        accountHolderNameTF.textFieldDidEndEditing(accountHolderNameTF.textFieldObj)
        ifscCodeTF.textFieldDidEndEditing(ifscCodeTF.textFieldObj)
        accountNumberTF.textFieldDidEndEditing(accountNumberTF.textFieldObj)
        
        bankDetails = response
        
        // Fetch cancelled cheque image if document reference id available
        if let docId = response?.documentRefNo {
            getDocImage(docId: docId)
        }
    }
    
    func displayBankDetails(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
}

// MARK:- Configuration Logic
extension BankDetailsVC {
    
    /// Used to configure interactor and presenter.
    fileprivate func setupDependencyConfigurator() {
        let interactor = BankDetailsInteractor()
        let presenter = BankDetailsPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension BankDetailsVC {
    
}
