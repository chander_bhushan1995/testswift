//
//  BankDetailsInteractor.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/4/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol contains method prototype for presentation logic of bank details
protocol BankDetailsPresentationLogic {
    
    /// This methods is called when bank details received from backend.
    func bankDetailsReceived(response: GetBankDetailsResponseDTO?, error: Error?)
    
    /// This methods is called when bank details updated on backend.
    func bankDetailsUpdated(response: BaseResponseDTO?, error: Error?)
    
    /// This methods is called when cancelled cheque image received
    func cancelledChequeImage(response: GetClaimDocumentResponseDTO?, error: Error?)
}

/// This class is used to confirm the methods of *BankDetailsBusinessLogic* protocol. It contains all the methods which used to send the request to get bank details, update bank details and get cancelled cheque.
class BankDetailsInteractor: BaseInteractor, BankDetailsBusinessLogic {

    var presenter: BankDetailsPresentationLogic?
    
    // MARK: Business Logic Conformance
    
    func getBankDetails(custId: String?, srnNumber: String?, custBankId: String?) {
        let req = GetBankDetailsRequestDTO(custId: custId, srnNumber: srnNumber, custBankId: custBankId)
        let _ = GetBankDetailsRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.bankDetailsReceived(response: response, error: error)
        }
    }
    
    func updateBankDetails(custId: String?, bankDetails: BankDetails?, image: UIImage) {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let req = UpdateBankDetailsRequestDTO.init(custId: custId, bankDetails: bankDetails, file: imageData)
        let _ = UpdateBankDetailsRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.bankDetailsUpdated(response: response, error: error)
        }
        
    }
    
    func getDocumentImage(docId: String?) {
        let req = GetClaimDocumentRequestDTO(documentId: docId, claimDocType: .image)
        let _ = GetBankDocumentRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.cancelledChequeImage(response: response, error: error)
        }
    }
}
