//
//  BankDetailsPresenter.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/4/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol used to declare methods to display success or failure of bank details
protocol BankDetailsDisplayLogic: class {
    
    /// This method is used to display the bank details received from backend
    ///
    /// - Parameter response: BankDetails object
    func displayBankDetails(response: BankDetails?)
    
    /// Used to display error while getting bank details
    ///
    /// - Parameter error: error message
    func displayBankDetails(error: String)
    
    /// Used to show that bank details are updated successfully
    func bankDetailUpdateSuccess()
    
    /// Used to show error in bank detail updation
    ///
    /// - Parameter error: error message
    func bankDetailUpdateFailure(error: String?)
    
    /// Used to display the cancelled cheque image
    ///
    /// - Parameter image: cancelled cheque image
    func displayImageSuccess(image: UIImage?)
    
    /// Used to display the error while getting cancelled cheque
    ///
    /// - Parameter error: error message
    func displayImageFailure(error: String?)
}

/// This class is used to confirm the methods of *BankDetailsPresentationLogic* protocol. It contains all the methods which used to pass the response or error on screen.
class BankDetailsPresenter: BasePresenter, BankDetailsPresentationLogic {
    weak var viewController: BankDetailsDisplayLogic?
    
    // MARK: Presentation Logic Conformance

    func bankDetailsReceived(response: GetBankDetailsResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayBankDetails(response: response?.data)
        } catch {
            viewController?.displayBankDetails(error: error.localizedDescription)
        }
    }
    
    func bankDetailsUpdated(response: BaseResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.bankDetailUpdateSuccess()
        } catch {
            viewController?.bankDetailUpdateFailure(error: error.localizedDescription)
        }
    }
    
    func cancelledChequeImage(response: GetClaimDocumentResponseDTO?, error: Error?) {
        do {
            try Utilities.checkError(response, error: error)
            viewController?.displayImageSuccess(image: response?.image)
        } catch {
            viewController?.displayImageFailure(error: error.localizedDescription)
        }
    }
}
