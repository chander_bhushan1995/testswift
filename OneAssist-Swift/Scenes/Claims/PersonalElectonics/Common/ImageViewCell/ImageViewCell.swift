//
//  ImageViewCell.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/11/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Used to show single image on Table view cell
class ImageViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
