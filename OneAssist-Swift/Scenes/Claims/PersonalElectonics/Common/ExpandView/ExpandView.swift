//
//  ExpandView.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 18/09/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol used to give callback of Expand view Events
protocol ExpandViewDelegate: class {
    
    /// Called when section tapped
    func sectionTapped()
}

/// This is a generic model used to display Expand View. We can display any type of data in this. It contains section name as String and list of row as array of any, so you can display any row. You just need to register that and add condition for that model in cellForRow method.
struct ExpandModel {
    var sectionName: String?
    var rows: [Any]?
}

/// This class is used to display View as Expanded and collapsed.
class ExpandView: UIView {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!
    
    weak var sectionDelegate: ExpandViewDelegate?
    fileprivate var selectedIndex = 0
    fileprivate var notCoveredSection = 0
    var dataSource: [ExpandModel] = [ExpandModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        separatorView.isHidden = true
    }
    
    override func layoutSubviews() {
        
        if self.constraintHeight?.constant != self.tableView?.contentSize.height {
            self.constraintHeight?.constant = self.tableView?.contentSize.height ?? 0
            self.sectionDelegate?.sectionTapped()
        }
    }
    
    /// Used to update UI initially
    func updateUI() {
        tableView.register(nib: Constants.CellIdentifiers.WHCService.partsCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.partsCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.stackViewGroupedCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.stackViewGroupedCell)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.isScrollEnabled = false
        notCoveredSection = dataSource.count - 1
        selectedIndex = notCoveredSection
//        if selectedIndex == dataSource.count - 1 {
//            separatorView.isHidden = false
//        } else {
//            separatorView.isHidden = true
//        }
        tableView.reloadData()
        tableView.layoutIfNeeded()
        
    }
}

extension ExpandView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard section == selectedIndex else {
            return 0
        }
        
        return dataSource[section].rows?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if let model = dataSource[indexPath.section].rows?[indexPath.row] as? StackViewGroupedCellModel {
           let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.stackViewGroupedCell, for: indexPath) as! StackViewGroupedCell
           cell.setViewModel(model)
           return cell
       }
        
        if let model = dataSource[indexPath.section].rows?[indexPath.row] as? PartListModel {
            
            // To display Parts list.
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.partsCell, for: indexPath) as! PartsCell
            cell.setViewModel(model)
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.partsCell, for: indexPath) as! PartsCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 57
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Add Header view
        let header = Bundle.main.loadNibNamed(Constants.CellIdentifiers.WHCService.berTableHeaderView, owner: nil, options: nil)?.first as? BERTableHeaderView
        header?.tag = section
        header?.delegate = self
        header?.headerLabel.text = dataSource[section].sectionName
        
        let angle: CGFloat = section == selectedIndex ? .pi : 0
        header?.arrowImageView.transform = CGAffineTransform(rotationAngle: angle)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
}

extension ExpandView: BERTableHeaderViewDelegate {
    func berTableHeaderView(_ view: BERTableHeaderView, clickedSectionButton sender: Any) {
        
        var sectionsToRefresh: IndexSet = IndexSet()
        if view.tag != selectedIndex {
            
            // Close Expanded view and show selected view
            if selectedIndex >= 0 {
                sectionsToRefresh.insert(selectedIndex)
            }
            selectedIndex = view.tag
            sectionsToRefresh.insert(selectedIndex)
        } else {
            sectionsToRefresh.insert(selectedIndex)
            selectedIndex = -1
        }
        
//        if selectedIndex == dataSource.count - 1 {
//            separatorView.isHidden = false
//        } else {
//            separatorView.isHidden = true
//        }
        
        
        tableView.reloadData()
        setNeedsLayout()
        layoutIfNeeded()
        self.perform(#selector(reloadMainTable), with: nil, afterDelay: 0.1)
    }
    
    @objc func reloadMainTable() {
        DispatchQueue.main.async {
            if self.constraintHeight?.constant != self.tableView?.contentSize.height {
                self.constraintHeight?.constant = self.tableView?.contentSize.height ?? 0
            }
            self.sectionDelegate?.sectionTapped()
        }
        
    }
}
