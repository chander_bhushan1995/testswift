//
//  LeftRightLabelView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 27/11/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class LeftRightLabelView: UIView {

    @IBOutlet weak var leftLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var rightLabel: SupportingTextRegularGreyLabel!
    
    override class func awakeFromNib() {
        
    }
    
    func setViewModel(_ model: PartListModel){
        leftLabel.text = model.title
        rightLabel.text = model.value
        leftLabel.font = model.font
        rightLabel.font = model.font
        leftLabel.textColor = model.titleTextColor
        rightLabel.textColor = model.valueTextColor
    }
    
}
