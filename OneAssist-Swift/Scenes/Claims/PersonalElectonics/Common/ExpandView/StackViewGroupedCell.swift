//
//  StackViewGroupedCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 27/11/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct StackViewGroupedCellModel {
    var backgroundColor: UIColor?
    var rows: [Any]?
}

class StackViewGroupedCell: UITableViewCell {

    @IBOutlet weak var stackContainerView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        stackContainerView.backgroundColor = .clear
        stackView.backgroundColor = .clear
        stackContainerView.layer.cornerRadius = 4
        stackContainerView.clipsToBounds = true
    }
    
    func setViewModel(_ model: StackViewGroupedCellModel){
        
        if let backgroundColor = model.backgroundColor {
            stackContainerView.backgroundColor = backgroundColor
        }
        
        if let rows = model.rows, stackView.arrangedSubviews.count <= 0 {
            for item in rows {
                if let model = item as? PartListModel {
                    let view = UINib(nibName: Constants.NIBNames.leftRightLabelView, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LeftRightLabelView
                    view.backgroundColor = .clear
                    view.setViewModel(model)
                    stackView.addArrangedSubview(view)
                }
            }
        }
    }
}
