//
//  PartsCell.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 18/09/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Part list model which contains title, value and font
struct PartListModel {
    var title: String?
    var value: String?
    var font: UIFont = DLSFont.supportingText.regular
    var valueTextColor: UIColor = .blogCellLight
    var titleTextColor: UIColor = .blogCellLight
    var showSperator: Bool = true
}

/// Used to display Parts on table view
class PartsCell: UITableViewCell {
    
    @IBOutlet weak var partNameLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var priceLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var seperatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        seperatorView.backgroundColor = .seperatorView
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /// Used to setViewModel
    ///
    /// - Parameters:
    ///   - title: title
    ///   - value: description
    ///   - font: font for both
    
    func setViewModel(_ model: PartListModel){
        partNameLabel.font = model.font
        priceLabel.font = model.font
        partNameLabel.text = model.title
        priceLabel.text = model.value
        partNameLabel.textColor = model.titleTextColor
        priceLabel.textColor = model.valueTextColor
        seperatorView.isHidden = model.showSperator
    }
}
