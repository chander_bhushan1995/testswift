//
//  PartDamageCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Used to give Callback on damage part selected
protocol PartDamageCellDelegate: class {
    
    /// Callback when index changed
    ///
    /// - Parameters:
    ///   - isOn: is damage part switch on
    ///   - index: index number
    func changedIndex(isOn: Bool, index: Int)
}

/// Used to show damage part cell
class PartDamageCell: UITableViewCell {

    @IBOutlet weak var labelTitle: H3BoldLabel!
    @IBOutlet weak var segmentControlObj: UISegmentedControl!
    
    weak var delegate: PartDamageCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        segmentControlObj.ensureCustomiOS13Style()
        segmentControlObj.addTarget(self, action: #selector(PartDamageCell.changedIndex(segment:)), for: .valueChanged)
    }
    
    /// Used to update UI
    ///
    /// - Parameters:
    ///   - title: title
    ///   - isOn: is switch on
    func updateUserInterface(title: String, isOn: Bool) {
        labelTitle.text = title
        segmentControlObj.selectedSegmentIndex = isOn ? 0 : 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// called when index changed
    @objc func changedIndex(segment: UISegmentedControl) {
        let isOn = segmentControlObj.selectedSegmentIndex == 0
        delegate?.changedIndex(isOn: isOn, index: segment.tag)
    }
    
}
