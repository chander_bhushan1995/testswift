//
//  RejectedClaimCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/21/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Rejected claim Cell model
class RejectedClaimCellViewModel {
    var title: String
    var description: String?
    var contactUsTitle: String
    var contactUsNumber: String
    var badgeText: String?
    
    init(title: String, contactUsTitle: String, contactUsNumber: String, badgeText: String? = nil, description: String? = nil) {
        self.title = title
        self.contactUsTitle = contactUsTitle
        self.contactUsNumber = contactUsNumber
        self.badgeText = badgeText
        self.description = description
    }
}

/// Model for D-Category pincode
class DCategoryPincodeModel {
    
    var stageDescription: String
    var contactUsTitle: String
    var contactUsNumber: String
    init(description: String, contactTitle: String, contactNumber: String) {
        stageDescription = description
        contactUsTitle = contactTitle
        contactUsNumber = contactNumber
    }
}

/// Cell to display rejected claim or D-category pincode
class RejectedClaimCell: UITableViewCell, NibLoadableView, ReuseIdentifier {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: OABadgeLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var titleLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var contactTitleLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var contactNumberLabel: H3RegularBlackLabel!
    var actionKey:String?
    @IBOutlet weak var callButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    /// used to initialize view
    private func initializeView() {
//        containerView.layer.cornerRadius = 6.0
//        containerView.layer.borderColor = UIColor.gray2.cgColor
        
//        badgeView.layer.cornerRadius = 2.0
//        badgeView.layer.borderColor = UIColor.badgeRed.cgColor
//        badgeView.layer.borderWidth = 1
    }
    
    /// Used to update cell
    ///
    /// - Parameters:
    ///   - badgeText: Badge text
    ///   - titleText: title of cell
    ///   - descriptionText: description of cell
    ///   - contactUsTitleText: Contact us title
    ///   - contactUsNumber: contact number
    func setViewModel(_ model:TimeLineViewModel.RejectedClaimCellModel) {
        if model.badgeText == nil {
            badgeView.isHidden = true
            badgeView.setZero(for: [.top, .height])
        } else {
            badgeView.isHidden = false
            badgeLabel.text = model.badgeText
        }
        
        if model.descriptionText == nil {
            descriptionLabel.isHidden = true
            descriptionLabel.setZero(for: [.top, .height])
        } else {
            descriptionLabel.text = model.descriptionText
        }
        self.actionKey = model.actionKey
        if model.titleText == "" {
//            titleLabel.isHidden = true
//            titleLabel.setZero(for: [.top, .height])
        } else {
            titleLabel.text = model.titleText
        }
        callButton.isEnabled = model.enable ?? false
        contactTitleLabel.text = model.contactUsTitleText
        contactNumberLabel.text = model.contactUsNumber
    }
    
    /// Clicked button call
    @IBAction func clickedBtnCall(_ sender: Any) {

        EventTracking.shared.eventTracking(name: .SRCallCC, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
        if self.actionKey == Strings.ServiceRequest.ActionKey.call{
        Utilities.callCustomerCare()
        }
        
    }
}
