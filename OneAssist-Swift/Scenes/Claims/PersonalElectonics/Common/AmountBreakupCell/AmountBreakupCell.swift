//
//  AmountBreakupCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/19/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// BER breakup amount model
struct BERBreakupViewModel {
    let breakupAmountTitle: String
    let breakupAmountValue: NSNumber?
}

/// Claim BER view model which contains all BER related info
struct ClaimBERViewModel {
    
    var approvedAmount: NSNumber?
    var berBreakUpModel: [BERBreakupViewModel]?
    var totalRefundAmount: NSNumber?
    
    var sumAssured: NSNumber?
    var marketValue: NSNumber?
    var repairEstimate: NSNumber?
    
}

/// Used to show BER Breakup amount cell
class AmountBreakupCell: UITableViewCell {

    @IBOutlet weak var approvedAmountTitleLabel: H3RegularBlackLabel!
    @IBOutlet weak var approvedAmountValueLabel: H3RegularBlackLabel!
    @IBOutlet weak var lessDeductibleTitleLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var netApprovedAmountTitleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var netApprovedAmountValueLabel: H3RegularBlackLabel!
    @IBOutlet weak var berBreakupContainerView: UIStackView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var sumAssuredLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var marketValueLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var repairEstimateLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var sumAssuredValueLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var repairEstimateValueLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var labelMarketValue: BodyTextRegularGreyLabel!
    @IBOutlet weak var descriptionLabel: SupportingTextRegularGreyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// Used to update UI of cell
    ///
    /// - Parameter claimBERModel: Claim BER model
    func updateUserInterface(claimBERModel: ClaimBERViewModel) {
        lessDeductibleTitleLabel.textColor = UIColor.bodyTextGray
        approvedAmountValueLabel.text = "\(claimBERModel.approvedAmount ?? 0)"//"₹ " + (claimBERModel.approvedAmount?.stringValue ?? "")
        netApprovedAmountValueLabel.text = "\(claimBERModel.totalRefundAmount ?? 0)"// "₹ " + ( claimBERModel.totalRefundAmount?.stringValue ?? "")
        
        sumAssuredValueLabel.text = "\(claimBERModel.sumAssured ?? 0)"//"₹ " + (claimBERModel.sumAssured?.stringValue ?? "")
        labelMarketValue.text = "\(claimBERModel.marketValue ?? 0)"//"₹ " + (claimBERModel.marketValue?.stringValue ?? "")
        repairEstimateValueLabel.text = "\(claimBERModel.repairEstimate ?? 0)"//"₹ " + (claimBERModel.repairEstimate?.stringValue ?? "")
        setBreakupView(amounts: claimBERModel.berBreakUpModel!)
        
        let descriptionText: NSString = "Repair estimate for your device is ₹ \(claimBERModel.repairEstimate ?? 0), which is more than 80% of  ₹ \(claimBERModel.marketValue ?? 0) (Market value)" as NSString
        let attributedString = NSMutableAttributedString(string: descriptionText as String)
        
        var range: NSRange = descriptionText.range(of:"\(claimBERModel.repairEstimate ?? 0)") //"₹ "+(claimBERModel.repairEstimate?.stringValue ?? ""))

        attributedString.addAttributes([.font: DLSFont.supportingText.regular], range: range)
        
        range = descriptionText.range(of: "\(claimBERModel.marketValue ?? 0)")//"₹ "+(claimBERModel.marketValue?.stringValue ?? ""))
        attributedString.addAttributes([.font: DLSFont.supportingText.regular], range: range)
        
        descriptionLabel.attributedText = attributedString
    }
    
    /// Used to set breakup views
    ///
    /// - Parameter amounts: array of amounts
    private func setBreakupView(amounts: [BERBreakupViewModel]) {
        
        guard berBreakupContainerView.arrangedSubviews.count == 0 else {
            return
        }
        
        // Add all breakup views
        for amount in amounts {
            
            let view = Bundle.main.loadNibNamed("BERBreakupView", owner: nil, options: nil)?.first as! BERBreakupView
            view.updateUserInterface(title: amount.breakupAmountTitle, value: "\(amount.breakupAmountValue ?? 0)")//"₹ " + (amount.breakupAmountValue?.stringValue ?? ""))
            berBreakupContainerView.addArrangedSubview(view)
        }
        setBottomView()
    }
    
    /// Used to set Bottom view
    private func setBottomView() {
        
        bottomView.layer.cornerRadius = 4
        setBorder(of: bottomView)
        setBorder(of: sumAssuredLabel)
        setBorder(of: sumAssuredValueLabel)
        setBorder(of: marketValueLabel)
        setBorder(of: labelMarketValue)
        setBorder(of: repairEstimateLabel)
        setBorder(of: repairEstimateValueLabel)
    }
    
    /// Used to set border of the view
    ///
    /// - Parameter view: view
    private func setBorder(of view: UIView) {
        view.layer.borderColor = UIColor.dlsBorder.cgColor
        view.layer.borderWidth = 1
    }
}
