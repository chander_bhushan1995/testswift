//
//  FindMyPhoneCell.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/9/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Used to give callback of Find my Phone cell's button.
protocol FindMyPhoneCellDelegate: class  {
    /// To get callback on click of check box button
    ///
    /// - Parameter isSelected: true if checkbox selected, else false
    func checkButtonClicked(isSelected: Bool)
    
    /// *How to find my Iphone* button clicked
    func linkClicked()
}

class FindMyPhoneCell: UITableViewCell {

    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var buttonCheckBox: UIButton!
    @IBOutlet weak var descriptionTextView: LinkTextView!
    
    weak var delegate: FindMyPhoneCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// This method used to initialize find my phone cell
    private func initializeView() {
        titleLabel.text = Strings.PersonalElectronics.UploadDocuments.titleFindPhone
        
        let atrStr = getAttrString(isBold: false)
        descriptionTextView.attributedText = atrStr
        descriptionTextView.linkDelegate = self
        descriptionTextView.setAttributedLink(completeText: atrStr, linkText: Strings.PersonalElectronics.UploadDocuments.descriptionFindPhoneLink)
        
    }
    
    private func getAttrString(isBold: Bool) -> NSMutableAttributedString {
        
        var font = DLSFont.bodyText.regular
        if isBold {
            font = DLSFont.bodyText.bold
        }
        
        let atrStr = NSMutableAttributedString(string: Strings.PersonalElectronics.UploadDocuments.descriptionFindPhone, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.bodyTextGray])
        let atrStr2 = NSAttributedString(string: Strings.PersonalElectronics.UploadDocuments.descriptionFindPhoneLink, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.dodgerBlue])
        atrStr.append(atrStr2)
        
        return atrStr
    }
    
    /// This method called when check box button clicked
    @IBAction func clickedBtnCheckBox(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.checkButtonClicked(isSelected: sender.isSelected)
        if sender.isSelected {
            descriptionTextView.viewFont = DLSFont.bodyText.bold
            descriptionTextView.setAttributedLink(completeText: getAttrString(isBold: true), linkText: Strings.PersonalElectronics.UploadDocuments.descriptionFindPhoneLink)
            
        } else {
            descriptionTextView.viewFont = DLSFont.bodyText.regular
            descriptionTextView.setAttributedLink(completeText: getAttrString(isBold: false), linkText: Strings.PersonalElectronics.UploadDocuments.descriptionFindPhoneLink)
        }
    }
}

//MARK:- Link TextView Delegate methods
extension FindMyPhoneCell: LinkTextViewDelegate {
    
    func linkClicked(linkText: String) {
        delegate?.linkClicked()
    }
}

