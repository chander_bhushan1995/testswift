//
//  UploadDocumentsHeader.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 10/07/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol UploadDocumentsHeaderDelegate:class{
    func stateChanged()
}

class UploadDocumentsHeader: UIView {
    
    weak var delegate: UploadDocumentsHeaderDelegate?
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        segmentedControl.ensureCustomiOS13Style()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func stateChanged(_ sender: Any) {
        delegate?.stateChanged()
    }
    
}
