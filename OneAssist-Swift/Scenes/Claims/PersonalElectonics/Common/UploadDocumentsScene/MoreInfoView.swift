//
//  MoreInfoView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 16/07/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class MoreInfoView: UIView {
    
    @IBOutlet var bottomSheetView: UIView!
    
    var basePickerVC: BasePickerVC?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }

    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("MoreInfoView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
    }
    
    @IBAction func crossTapped(_ sender: Any) {
        basePickerVC?.dismiss(animated: true, completion: nil)
    }
    
    /*
    class func height() -> CGFloat {
        var bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        bottomSafeArea = bottomSafeArea + 500.0 + 12.0
        return CGFloat(330) + bottomSafeArea
    } */
    
}
