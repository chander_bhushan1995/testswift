//
//  UploadDocumentsVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/20/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol will be used for business logic of Upload document in PE
protocol UploadDocumentsBusinessLogic: UploadMultipleImageBusinessLogic {
    func getDocumentDetails(custId: String?, docCategory: String?, assetId: String?, assetCategory: String?, memId: String?)
}

/// This class is used to show and upload document in PE
class UploadDocumentsVC: DocumentTypeBaseVC {
    var interactor: UploadDocumentsBusinessLogic?
    
    @IBOutlet weak var viewSRNumber: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var headerViewLabel: UILabel!
    @IBOutlet weak var submitButton: OAPrimaryButton!
    
    var dataSource: TableViewDataSource<UploadDocumentsViewModel.UploadCellModel,TableViewSection<UploadDocumentsViewModel.UploadCellModel>>!
    var viewModel: UploadMultipleImageViewModel = UploadMultipleImageViewModel()
    weak var delegate: UploadMultipleImageVCDelegate?
    var isCheckBoxSelected = false
    var serviceDocTypes: [ServiceDocumentType]?
    var assetId: String?
    var memId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDependencyConfigurator()
        initializeView()
    }
    
    /// Add back button on screen
    private func handleBackButton() {
        let backButton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleNavBackButton))
        backButton.imageInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = backButton
    }
    
    /// Clicked on back button
    @objc func handleNavBackButton() {
        
        var refreshNeeded = false
        
        for section in dataSource.tableSections {
            refreshNeeded = section.rows.first(where: {$0.isUploaded}) != nil ? true : false
            break
        }
        
        delegate?.clickedBtnBack(refreshNeeded: refreshNeeded)
    }
    
    /// used to initialize view
    private func initializeView() {
        
        handleBackButton()
        tableView.register(nib: Constants.CellIdentifiers.WHCService.uploadDocumentsCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.uploadDocumentsCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.findMyPhoneCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.findMyPhoneCell)
        submitButton.isHidden = true
        title = viewModel.title
        dataSource = TableViewDataSource()
        dataSource.tableSections = [
            {
                let section = TableViewSection<UploadDocumentsViewModel.UploadCellModel>()
                section.rows = [
                ]
                return section
            } ()
        ]
    }
    
    /// Used to display Documents to be uploaded
    ///
    /// - Parameter serviceDocTypes: all documents
    override func displayDocTypeResponse(serviceDocTypes: [ServiceDocumentType]) {
        
        super.displayDocTypeResponse(serviceDocTypes: serviceDocTypes)
        self.serviceDocTypes = serviceDocTypes
        if let _ = serviceDocTypes.first(where: { $0.docCategory == DocumentType.asset.rawValue}) {
            Utilities.addLoader(onView: view, message: "getting document types", count: &loaderCount, isInteractionEnabled: false)
            let custId = UserCoreDataStore.currentUser?.cusId
            let category = serviceRequestType.getCategoryType()
            //TODO: add assetid and category
            interactor?.getDocumentDetails(custId: custId, docCategory: DocumentType.asset.rawValue, assetId: assetId, assetCategory: category, memId: memId)
        }
    }
    
    fileprivate func showAllDocuments(docDetails: [DocumentDetails]?) {
        
        dataSource = TableViewDataSource()
        dataSource.tableSections = [TableViewSection<UploadDocumentsViewModel.UploadCellModel>()]
        
        let models = serviceDocTypes?.map { (docTypeModel) -> UploadDocumentsViewModel.UploadCellModel in
            let row = UploadDocumentsViewModel.UploadCellModel()
            
            // Append Optional in front of non mendatory documents
            if docTypeModel.isMandatory != "Y" {
                row.title = (docTypeModel.docName ?? "") + " (Optional)"
            } else {
                row.title = docTypeModel.docName
            }
            
            row.description = docTypeModel.typeDescription
            row.isMandatory = docTypeModel.isMandatory == "Y" ? true : false
            row.docTypeId = Int(docTypeModel.docTypeId!)
            //            row.docId = viewModel.serviceDocuments.filter({$0.documentTypeId == docTypeModel.docTypeId}).first?.documentId
            
            row.docCategory = docTypeModel.docCategory
            row.docKey = docTypeModel.docKey
            
            if let doc = docDetails?.filter({ $0.documentKey == docTypeModel.docKey }).first {
                row.storageRefId = doc.storageRefId
                row.imageName = doc.documentName
            }
            if let doc = viewModel.serviceDocuments.filter({$0.documentKey == docTypeModel.docKey}).first {
                row.docId = doc.documentId
                row.storageRefId = doc.storageRefId
                row.imageName = doc.documentName
            }
            
            if row.docId == nil || row.storageRefId == nil {
                row.isUploaded = false
            }
            return row
        }
        
        dataSource.tableSections.first?.rows = models ?? [UploadDocumentsViewModel.UploadCellModel]()
        tableView.dataSource = self
        tableView.setHeaderView(headerView)
        submitButton.isHidden = false
        tableView.reloadData()
        toggleSubmitButton()
        setUploadedDocuments()
        
        
    }
    
    /// Get Image which is already uploaded
    private func setUploadedDocuments() {
        
        for section in dataSource.tableSections {
            for (index,value) in section.rows.enumerated() {
                if value.docId != nil || value.storageRefId != nil {
                    let path = IndexPath(row: index, section: 0)
                    addLoaderOnImage(path: path)
                    interactor?.getDocumentImage(docId: value.docId, storageRefId: value.storageRefId, index: path)
                }
            }
        }
    }
    
    //MARK:- Action Methods:
    
    /// clicked button submit documents
    @IBAction func clickedBtnSubmit(_ sender: Any) {
        submitRequest()
    }
    
    fileprivate func submitRequest() {
        
        let subCat = Utilities.getProduct(productCode: productCode ?? "")?.subCategoryName
        
        let serviceDocuments = prepareSubmitRequestDocument()
        
        EventTracking.shared.eventTracking(name: .SRDocSubmit, [.service: CacheManager.shared.srType?.rawValue ?? "", .subcategory: subCat ?? ""])
        Utilities.addLoader(onView: view, message: "Submitting", count: &self.loaderCount, isInteractionEnabled: false)
        interactor?.updateSR(srId: viewModel.srId, srType: serviceRequestType.rawValue, serviceDocuments: serviceDocuments)
    }
    
    fileprivate func prepareSubmitRequestDocument() -> [ServiceDocument] {
//        var documents = [ServiceDocument]()

        let models = dataSource.sectionModel(for: 0).rows
        let documents = models.map { (model) -> ServiceDocument in
            let docObject = ServiceDocument(dictionary: [:])
            docObject.documentTypeId = model.docTypeId as NSNumber?
            docObject.documentId = model.docId
            docObject.storageRefId = model.storageRefId
            docObject.documentName = model.imageName
            docObject.docCategory = model.docCategory
            docObject.documentKey = model.docKey
            return docObject
        }

        return documents
    }
    
    /// Used to add loader on image view
    ///
    /// - Parameter path: index path of image view
    fileprivate func addLoaderOnImage(path: IndexPath) {
        let model = dataSource.rowModel(for: path)
        model.isUploading = true
        reloadRow(indexPath: path)
    }
    
    /// Remove loader from image view
    ///
    /// - Parameter path: index path
    fileprivate func removeLoaderFromImage(path: IndexPath) {
        let model = dataSource.rowModel(for: path)
        model.isUploading = false
        reloadRow(indexPath: path)
    }
}

// MARK:- Configuration Logic
extension UploadDocumentsVC {
    
    /// configure interactor and presenter
    fileprivate func setupDependencyConfigurator() {
        let interactor = UploadMultipleImageInteractor()
        let presenter = UploadMultipleImagePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension UploadDocumentsVC: UploadDocumentsCellDelegate {
    
    /// Used to show action sheet to upload documents
    ///
    /// - Parameter indexPath: index path on which image uploaded
    private func pickImage(for indexPath: IndexPath) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: Strings.Global.uploadImage, style: .default) { (action) in
            ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(for: indexPath, image, error)
            })
        }
        
        let action2 = UIAlertAction(title: Strings.Global.clickPhoto, style: .default) { (action2) in
            ImagePickerManager.shared.pickImageFromCamera(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(for: indexPath, image, error)
            })
        }
        
        let action3 = UIAlertAction(title: Strings.Global.cancel, style: .cancel) { (action3) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action)
        alertController.addAction(action2)
        alertController.addAction(action3)
        presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    /// Give callback when image is picked from either camera or gallary
    ///
    /// - Parameters:
    ///   - indexPath: index path for image
    ///   - image: image selected
    ///   - error: error while selecting image
    func handlePickedImage(for indexPath: IndexPath, _ image: UIImage?, _ error: Error?) {
        if let error = error {
            // Handle error
            print(error)
        } else {

            let model = dataSource.rowModel(for: indexPath)
            model.image = image
            model.isUploaded = false
            addLoaderOnImage(path: indexPath)
            reloadRow(indexPath: indexPath)
//            tableView.reloadRows(at: [indexPath], with: .none)
            
            self.interactor?.uploadClaimInvoice(srId: self.viewModel.srId, documentTypeId: self.dataSource.rowModel(for: indexPath).docTypeId?.description ?? "" , file: image?.jpegData(compressionQuality: 0.9), documentId: self.dataSource.rowModel(for: indexPath).docId?.description ?? "", index: indexPath)
        }
    }
    
    /// Enable or desable submit button
    fileprivate func toggleSubmitButton() {
        for section in dataSource.tableSections {
            for row in section.rows {
                if (!row.isUploaded && row.isMandatory) || !isCheckBoxSelected {
                    submitButton.isEnabled = false
                    return
                }
            }
        }
        
        submitButton.isEnabled = true
    }
    
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clikedBtnEdit sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            pickImage(for: indexPath)
        }
    }
    
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clickedBtnSelect sender: Any) {
        if let indexPath = tableView.indexPath(for: cell) {
            pickImage(for: indexPath)
        }
    }
    
    override func displayDocTypeError(error: String) {
        super.displayDocTypeError(error: error)
        showAlert(message: error)
    }
}

extension UploadDocumentsVC: UploadMultipleImageDisplayLogic {
    
    func displayUploadError(error: String, index: IndexPath) {
        removeLoaderFromImage(path: index)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {
            let model = self.dataSource.rowModel(for: index)
            let image = model.image
            
            self.handlePickedImage(for: index, image, nil)
        }){
            let model = self.dataSource.rowModel(for: index)
            model.image = nil
            model.isUploaded = false
            self.reloadRow(indexPath: index)
        }
    }
    
    func displayUploadSuccess(index: IndexPath, docId: String?, storageRefId: String?, docName: String?) {
        removeLoaderFromImage(path: index)
        
        let model = dataSource.rowModel(for: index)
        model.isUploaded = true
        model.docId = docId
        model.storageRefId = storageRefId
        model.imageName = docName
        reloadRow(indexPath: index)
        toggleSubmitButton()
    }
    
    func displayGetDocError(path: IndexPath) {
        removeLoaderFromImage(path: path)
    }
    
    func displayGetDocSuccess(image: UIImage?, path: IndexPath) {
        
        removeLoaderFromImage(path: path)
        dataSource.rowModel(for: path).isUploaded = true
        dataSource.rowModel(for: path).image = image
        reloadRow(indexPath: path)
        
        toggleSubmitButton()
    }
    
    /// this method is used to reload row at runtime
    ///
    /// - Parameter indexPath: index path
    fileprivate func reloadRow(indexPath: IndexPath) {
        
        if let visibleCells = tableView.indexPathsForVisibleRows, visibleCells.contains(indexPath) {
            
            if let cell = tableView.cellForRow(at: indexPath) as? UploadDocumentsCell {
                let rowModel = dataSource.rowModel(for: indexPath)
                cell.updateUserInterface(title: rowModel.title ?? "", subtitle: rowModel.description ?? "", image: rowModel.image, isUploaded: rowModel.isUploaded, isUploading: rowModel.isUploading)
            }
        }
    }
    
    func displayUpdateSRSuccess(response: ClaimNotifyAllDocumentUploadedResponseDTO?) {
        Utilities.removeLoader(count: &loaderCount)
        delegate?.clickedBtnSubmit()
        routeToDocumentSuccess()
    }
    
    func displayUpdateSRError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {[weak self] in
            self?.submitRequest()
        }) {}
    }
    
    func displayDocDetails(response: [DocumentDetails]?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAllDocuments(docDetails: response)
    }
    
    func displayDocDetailsError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    
}

// MARK:- Routing Logic
extension UploadDocumentsVC {
    
    /// navigate to Document success screen
    func routeToDocumentSuccess() {
        let model = ClaimSuccessViewModel()
        model.successType = .upload
        model.category = .peADLD
        let vc = ClaimSuccessVC()
        vc.delegate = self
        vc.successModel = model
        presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    /// Navigate to find my phone screen
    func routeToFindPhoneVC() {
        
        let vc = BERDetailVC()
        vc.headers = [Strings.PersonalElectronics.UploadDocuments.headerFindViaMobile, Strings.PersonalElectronics.UploadDocuments.headerFindViaWeb]
        let images1 = [#imageLiteral(resourceName: "group6"), #imageLiteral(resourceName: "group12"), #imageLiteral(resourceName: "group13"), #imageLiteral(resourceName: "group14"), #imageLiteral(resourceName: "group15")]
        let images2 = [#imageLiteral(resourceName: "group5"), #imageLiteral(resourceName: "group7"), #imageLiteral(resourceName: "group8"), #imageLiteral(resourceName: "group9")]
        vc.rowDetails = [images1, images2]
        vc.expandViewType = .FindPhone
        presentInFullScreen(vc, animated: true, completion: nil)
    }
}


extension UploadDocumentsVC: ClaimSuccessVCDelegate {
    
    func clickCrossButton() {
        dismiss(animated: true, completion: nil)
        delegate?.clickedBtnBack(refreshNeeded: false)
    }
    
    func clickViewServiceRequest() {
        dismiss(animated: true, completion: nil)
        delegate?.clickedBtnBack(refreshNeeded: false)
    }
}

extension UploadDocumentsVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == dataSource.numberOfSections {
            return 1
        }
        return dataSource.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == dataSource.numberOfSections {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.findMyPhoneCell, for: indexPath) as! FindMyPhoneCell
            cell.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.uploadDocumentsCell, for: indexPath) as! UploadDocumentsCell
            let rowModel = dataSource.rowModel(for: indexPath)
            cell.updateUserInterface(title: rowModel.title ?? "", subtitle: rowModel.description ?? "", image: rowModel.image, isUploaded: rowModel.isUploaded, isUploading: rowModel.isUploading)
            cell.delegate = self
            return cell
        }
    }
}

extension UploadDocumentsVC: FindMyPhoneCellDelegate {
    func checkButtonClicked(isSelected: Bool) {
        isCheckBoxSelected = isSelected
        toggleSubmitButton()
    }
    
    func linkClicked() {
        routeToFindPhoneVC()
    }
}
