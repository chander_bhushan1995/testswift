//
//  UploadDocumentsCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/20/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This delegate is used to give callback of events of Upload document cell.
@objc protocol UploadDocumentsCellDelegate: class {
    
    /// Give callback on click of Upload button
    ///
    /// - Parameters:
    ///   - cell: current cell object
    ///   - sender: button object
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clickedBtnSelect sender: Any)
    
    /// Give callback on click of Edit button
    ///
    /// - Parameters:
    ///   - cell: current cell object
    ///   - sender: button object
    func uploadDocumentsCell(_ cell: UploadDocumentsCell, clikedBtnEdit sender: Any)
    
    @objc optional func uploadDocumentsCell(_ cell: UploadDocumentsCell, clickedKnowMore sender: Any)
}

/// Used to display upload documents cell
class UploadDocumentsCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var descriptionLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var imagePickView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var pickedImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var loaderView: UIView!
    
    @IBOutlet var knowMoreButton: OASecondaryButton!
    @IBOutlet var bottomImageViewConstraint: NSLayoutConstraint!
    @IBOutlet var centreYImageViewConstraint: NSLayoutConstraint!
    @IBOutlet var descriptionLabelLeadingConstraint: NSLayoutConstraint!
    
    
    weak var delegate: UploadDocumentsCellDelegate?
    var spinnerView: SpinnerView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    /// Used to initialize view
    private func initializeView() {
        circularView.layer.cornerRadius = 4.0
        
        imagePickView.layer.cornerRadius = 4.0
        imagePickView.layer.borderColor = UIColor.buttonTitleBlue.cgColor
        imagePickView.layer.borderWidth = 1.0
        
        editView.layer.cornerRadius = 4.0
        editView.layer.borderColor = UIColor.buttonTitleBlue.cgColor
        editView.layer.borderWidth = 1.0
        
        editButton.layer.cornerRadius = 16.0
        
        editView.isHidden = true
        imagePickView.isHidden = false
        
        editView.clipsToBounds = true
        
        knowMoreButton.isHidden = true
        bottomImageViewConstraint.constant = 15
        
        // add custom loader
        activityIndicator.stopAnimating()
        spinnerView = SpinnerView(view: loaderView)
        spinnerView?.spinnerView.isUserInteractionEnabled = false
        showLoaderView(flag: true)
    }
    
    private func showLoaderView(flag: Bool) {
        if flag {
            spinnerView?.startAnimating()
            loaderView.isHidden = false
//            self.bringSubview(toFront: loaderView)
        } else {
            spinnerView?.stopAnimating()
            loaderView.isHidden = true
//            self.sendSubview(toBack: loaderView)
        }
    }
    
    private func basicSetup(with isUploaded: Bool, image: UIImage? = nil) {
        imagePickView.isHidden = true
        editView.isHidden = false
        pickedImageView.image = image
        
        if isUploaded {
            editButton.isHidden = false
        } else {
            editButton.isHidden = true
        }
    }
    
    /// Used to update cell according to given data.
    ///
    /// - Parameters:
    ///   - title: title of cell
    ///   - subtitle: subtitle of cell
    ///   - image: image of document
    ///   - isUploaded: true if image exist
    ///   - margin: default 0
    ///   - isUploading: true if image upload is in progress
    func updateUserInterface(title: String, subtitle: String, buttonTitle: String? = nil, image: UIImage?, isUploaded: Bool, isDownloading: Bool? = nil, margin: CGFloat = 0, isUploading: Bool = false, bottomConstant: CGFloat? = nil, imageViewCentreConstant: CGFloat? = nil, isHiddenButton: Bool = true, isHiddenDescriptionLabel: Bool = false, isHiddenCircularView: Bool = false, isAttributedText: Bool? = nil, editButtonHidden: Bool? = nil) {
        
        if isAttributedText != nil {
            titleLabel.setAttributedString(title, bodyColor: UIColor.charcoalGrey, bodyFont: DLSFont.bodyText.regular, attributedParts: [Strings.Global.optional], attributedTextColors: [UIColor.bodyTextGray], attributedFonts: [DLSFont.bodyText.regular])
        } else {
            titleLabel.text = title
        }
        
        descriptionLabel.text = subtitle
        
        if let isDownloading = isDownloading, isDownloading {
            basicSetup(with: isUploaded)
        } else {
            if let image = image {
                basicSetup(with: isUploaded, image: image)
            } else {
                imagePickView.isHidden = false
                editView.isHidden = true
            }
        }
        
//        if isUploading {
////            activityIndicator.isHidden = false
////            activityIndicator.startAnimating()
//            spinnerView?.startAnimating()
//        } else {
////            activityIndicator.isHidden = true
////            activityIndicator.stopAnimating()
//            spinnerView?.stopAnimating()
//        }
        
        showLoaderView(flag: isUploading)
        
        if let isDownloading = isDownloading {
            showLoaderView(flag: isDownloading)
        }
        
        leadingConstraint.constant = margin
        trailingConstraint.constant = margin
        
        if let bottomConst = bottomConstant {
            bottomImageViewConstraint.constant = bottomConst
        }
        
        if let centreConst = imageViewCentreConstant {
            centreYImageViewConstraint.constant = centreConst
        }
        
        knowMoreButton.isHidden = isHiddenButton
        
        descriptionLabel.isHidden = isHiddenDescriptionLabel
        circularView.isHidden = isHiddenCircularView
        
        if isHiddenCircularView {
            descriptionLabelLeadingConstraint.constant = -7
        } else {
            descriptionLabelLeadingConstraint.constant = 8
        }
        
        if editButtonHidden != nil {
            editButton.isHidden = true
        }
    }
    
    /// clicked button upload
    @IBAction func clickedBtnSelect(_ sender: Any) {
        delegate?.uploadDocumentsCell(self, clickedBtnSelect: sender)
    }
    
    /// clicked button edit
    @IBAction func clikedBtnEdit(_ sender: Any) {
        delegate?.uploadDocumentsCell(self, clikedBtnEdit: sender)
    }
    
    @IBAction func clickedKnowMore(_ sender: Any) {
        delegate?.uploadDocumentsCell?(self, clickedKnowMore: sender)
    }
}
