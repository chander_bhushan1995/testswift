//
//  UploadDocumentsViewModel.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/20/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

/// Upload Document view model
enum UploadDocumentsViewModel {
    
    /// Upload document cell model
    class UploadCellModel : Row {
        
        var description: String?
        var image: UIImage?
        var isUploaded = false
        var isUploading = false
        
        var docTypeId: Int?
        var docId: String?
        var isMandatory: Bool = false
        
        var docNumber: String?
        var docCategory: String?
        var storageRefId: String?
        var docKey: String?
    }
}
