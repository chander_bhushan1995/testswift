//
//  PEProgressContainerPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/04/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol used to declare methods to display success or failure of Create and update Service request
protocol PEProgressContainerBaseDisplayLogic: class {
    
    /// Display Error in API
    ///
    /// - Parameter error: Error message
    func displayError(_ error: String)
    
    /// display SR created successfully
    ///
    /// - Parameter serviceDetails: SR details
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?)
    
    /// Display SR Updated successfully
    ///
    /// - Parameter serviceDetails: SR details
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?)
}

protocol PEProgressContainerDisplayLogic: PEProgressContainerBaseDisplayLogic {
    
}

/// This class is used to confirm the methods of *PEProgressContainerPresentationLogic* protocol. It contains methods to prepare SR data to display on screen.
class PEProgressContainerPresenter: BasePresenter, PEProgressContainerPresentationLogic {
    weak var viewController: PEProgressContainerDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayCreatedServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayUpdateServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
}
