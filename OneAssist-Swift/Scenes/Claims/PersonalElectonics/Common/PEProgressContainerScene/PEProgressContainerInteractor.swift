//
//  PEProgressContainerInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/04/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol contains method prototype for presentation logic of Progress Container for PE
protocol PEProgressContainerBasePresentationLogic {
    
    /// This method is used to prepare Create SR data from response
    ///
    /// - Parameters:
    ///   - response: create SR data
    ///   - error: error
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?)
    
    /// Used to present Updated SR Data
    ///
    /// - Parameters:
    ///   - response: Update sr data
    ///   - error: error
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?)
}

protocol PEProgressContainerPresentationLogic: PEProgressContainerBasePresentationLogic {
    
}

/// This class is used to confirm the methods of *PEProgressContainerBusinessLogic* protocol. It contains method to create or update service request.
class PEProgressContainerInteractor: BaseInteractor, PEProgressContainerBusinessLogic {
    var presenter: PEProgressContainerPresentationLogic?
    
    // MARK: Business Logic Conformance
    func updateServiceRequest(for type: ClaimType) {
        
        var request = UpdateServiceRequestDTO()

        switch type {
        case aEnumClaimPersonalElectronics.eADLD:
            
            // SR type PE ADLD
            let requestADLD = UpdatePEADLDServiceRequestPlanRequestDTO()
            
            // prepare incident detail model
            requestADLD.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEADLDDamageDetail()
                dtl.damagePart = ClaimHomeServe.shared.damagePartName
                dtl.damageType = ClaimHomeServe.shared.damageType
                dtl.damagePlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestADLD
            
        case aEnumClaimPersonalElectronics.eEW:
            
            // SR type PE EW
            let requestEW = UpdatePEEWServiceRequestPlanRequestDTO()
            
            // prepare incident detail model
            requestEW.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEEWBreakDownDetail()
                dtl.breakDownPart = ClaimHomeServe.shared.damagePartName
                dtl.breakDownType = ClaimHomeServe.shared.damageType
                dtl.breakDownPlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestEW
            
        case aEnumClaimPersonalElectronics.eTheft:
            
            // SR type PE Theft
            let requestTheft = UpdatePETheftServiceRequestPlanRequestDTO()
            requestTheft.incidentDetail = {
                let dtl = PETheftIncidentDetail()
                dtl.simBlockRefNo = ClaimHomeServe.shared.blockReferenceNo
                dtl.simBlockReqDate = ClaimHomeServe.shared.blockReferenceDate
                return dtl
            }()
            
            request = requestTheft
            
        default:
            print("not a pe request")
            return
        }
        
        let currentUser = UserCoreDataStore.currentUser
        
        request.insurancePartnerCode = ClaimHomeServe.shared.insurancePartnerCode // insurance partner code
        request.requestDescription = ClaimHomeServe.shared.incidentDescription
        request.placeOfIncident = ClaimHomeServe.shared.address
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen
        request.referenceNo = ClaimHomeServe.shared.memberShipId
        request.serviceRequestId = ClaimHomeServe.shared.serviceRequestID
        //createdby
        request.modifiedBy = currentUser?.cusId
        request.refSecondaryTrackingNo = ClaimHomeServe.shared.assetIds?.joined(separator: ",")
    
        
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName
        addressDetails.countryCode = "IND"
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com"
        addressDetails.mobileNo = currentUser?.mobileNo
        addressDetails.pincode = ClaimHomeServe.shared.pinCode
        addressDetails.addressLine1 = ClaimHomeServe.shared.address
        
        request.serviceRequestAddressDetails = addressDetails
        
        // call Update SR API
        UpdateServiceRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
        
    }
    
    func createServiceRequest(for type: ClaimType) {
        
        var request: CreateServiceRequestPlanRequestDTO!
        
        switch type {
        case aEnumClaimPersonalElectronics.eADLD:
            // SR type PE ADLD
            let requestADLD = CreatePEADLDServiceRequestPlanRequestDTO()
            requestADLD.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEADLDDamageDetail()
                dtl.damagePart = ClaimHomeServe.shared.damagePartName
                dtl.damageType = ClaimHomeServe.shared.damageType
                dtl.damagePlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestADLD
        
        case aEnumClaimPersonalElectronics.eEW:
            // SR type PE EW
            let requestEW = CreatePEEWServiceRequestPlanRequestDTO()
            requestEW.incidentDetail = {
                var dict: [String: Any] = [:]
                
                let dtl = PEEWBreakDownDetail()
                dtl.breakDownPart = ClaimHomeServe.shared.damagePartName
                dtl.breakDownType = ClaimHomeServe.shared.damageType
                dtl.breakDownPlace = ClaimHomeServe.shared.address
                
                if let dtlDict = dtl.createRequestParameters() {
                    dict.merge(dtlDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                if let condDict = ClaimHomeServe.shared.damageConditions?.reduce([:], { (result, viewModel) -> [String: String] in
                    var newResult = result
                    newResult[viewModel.damageKey] = viewModel.switchedOn ? "Y" : "N"
                    return newResult
                }) {
                    dict.merge(condDict, uniquingKeysWith: { (current, _)  in current })
                }
                
                return dict
            }()
            
            request = requestEW
        
        case aEnumClaimPersonalElectronics.eTheft:
            // SR type PE Theft
            let requestTheft = CreatePETheftServiceRequestPlanRequestDTO()
            requestTheft.incidentDetail = {
                let dtl = PETheftIncidentDetail()
                dtl.simBlockRefNo = ClaimHomeServe.shared.blockReferenceNo
                dtl.simBlockReqDate = ClaimHomeServe.shared.blockReferenceDate
                return dtl
            }()
            
            request = requestTheft
            
        default:
            print("not a pe request")
            return
        }
        
        
        let currentUser = UserCoreDataStore.currentUser
        
        request.insurancePartnerCode = ClaimHomeServe.shared.insurancePartnerCode
        request.customerId = currentUser?.cusId
        request.requestDescription = ClaimHomeServe.shared.incidentDescription
        request.placeOfIncident = ClaimHomeServe.shared.address
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen
        request.serviceRequestType = type.getServiceCode()
        request.referenceNo = ClaimHomeServe.shared.memberShipId
        
        request.createdBy = currentUser?.cusId
        
        request.refSecondaryTrackingNo = ClaimHomeServe.shared.assetIds?.joined(separator: ",")

        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName
        addressDetails.countryCode = "IND"
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com"
        addressDetails.mobileNo = currentUser?.mobileNo
        addressDetails.pincode = ClaimHomeServe.shared.pinCode
        addressDetails.addressLine1 = ClaimHomeServe.shared.address
        
        addressDetails.district = "Delhi"
        addressDetails.landmark = "Delhi"
        
        request.serviceRequestAddressDetails = addressDetails
        
        // Hit API for Create SR
        CreateServiceRequestPlanRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
    }
}
