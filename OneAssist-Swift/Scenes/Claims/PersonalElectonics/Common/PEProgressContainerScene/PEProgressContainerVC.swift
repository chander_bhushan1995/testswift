//
//  PEProgressContainerVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/04/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

/// This protocol contains method related to create and update service request business logic.
protocol ProgressContaineBaseBusinessLogic {
    
    /// Used to create SR
    ///
    /// - Parameter type: claim type
    func createServiceRequest(for type: ClaimType)
    
    /// Used to update SR
    ///
    /// - Parameter type: claim type
    func updateServiceRequest(for type: ClaimType)
}

protocol PEProgressContainerBusinessLogic: ProgressContaineBaseBusinessLogic {
    
}

/// Used to Show all screen to Create/update SR in PE
class PEProgressContainerVC: BaseVC {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var pagingView: UIView!
    var pageViewController: UIPageViewController!
    var currentIndex:Int = 0
    var animating : Bool = false
    var interactor: PEProgressContainerBusinessLogic?
    var category:aEnumClaimPersonalElectronics = .eADLD
    var dictCategory : [aEnumClaimPersonalElectronics:[AnyClass]]!
    var raisedFromCrm = false
    var arrViewController :[UIViewController] = []
    var currentCategory:aEnumClaimPersonalElectronics!
    var totalVCInCategories:Int = 0
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    /// Used to initialize view
    private func initializeView() {
        initializePagingController()
        self.lblProgress.font = DLSFont.supportingText.regular
        self.dictCategory = self.initialiseCategoryDictionaryForViewController()
        currentIndex = 0
        arrViewController = getStartingViewController()
        pageViewController.setViewControllers([arrViewController[0]], direction: .forward, animated: false, completion: nil)
        self.setProgress(index: 0)
        let backButton = UIBarButtonItem(image:#imageLiteral(resourceName: "navigateBack"), style:.done , target: self, action: #selector(self.clickBackButton))
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = Strings.ServiceRequest.Titles.raiseARequest
    }
    
    /// initialize page controller to display all VC to create/update Sr
    func initializePagingController() {
        currentIndex = 0
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.frame = pagingView.bounds
        pageViewController.delegate = self
        addChild(pageViewController)
        pagingView.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        
    }
    
    /// Used to show create sr progress
    ///
    /// - Parameters:
    ///   - index: current page number
    ///   - totalCount: total pages
    func setProgress(index : Int , totalCount : Int?)  {
        
        // On first page, we don't know the number of pages. So set progress 0.15
        if index  == 0 {
            self.progressView.progress =  0.15
        } else {
            
            // Calculate progress on other then first page
            self.progressView.progress =  Float(Double(index+1))/Float(totalCount! + 1)
        }
        
        self.lblProgress.text = getProgressLabelText(index : index, totalCount: totalCount)
    }
    
    /// Back button clicked
    @objc func clickBackButton() {
        
        // pop VC on first page
        if(currentIndex == 0){
            self.navigationController?.popViewController(animated: true)
        } else {
            
            // display previous page
            currentIndex = currentIndex - 1
            setProgress(index : currentIndex)
            let nextVC = arrViewController[currentIndex]
            self.pageViewController.setViewControllers([nextVC], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    /// Used to display next page
    ///
    /// - Parameters:
    ///   - controllerToLoad: controller to load
    ///   - onCompletion: when loaded completely
    func moveToNext(controllerToLoad:((_ controller : UIViewController)->Void)?, onCompletion:((_ controller : UIViewController)->Void)? = nil){
        
        // Prevent from removing multiple pages on multiple tap of single button
        if animating == true {
            return
        }
        animating = true
        let nextIndexToLoad = self.currentIndex + 1
        
        if nextIndexToLoad >= arrViewController.count {
            guard let classToLoad =   getClass(category: currentCategory, dictCategory: self.dictCategory, index: nextIndexToLoad)else {
                return
            }
            let nextVC = getViewController(forClass: classToLoad , category : currentCategory)!
            setProgress(index: nextIndexToLoad)
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true, completion: { _ in
                self.animating = false
                onCompletion?(nextVC)
            })
            arrViewController.append(nextVC)
        } else {
            
            let nextVC = arrViewController[nextIndexToLoad]
            setProgress(index: nextIndexToLoad)
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true, completion: { _ in
                self.animating = false
                onCompletion?(nextVC)
            })
        }
        self.currentIndex = nextIndexToLoad
    }
    
    /// Used to set progress bar label to show progress
    ///
    /// - Parameters:
    ///   - index: current page index
    ///   - totalCount: total pages
    /// - Returns: String which should be display
    func getProgressLabelText(index : Int , totalCount : Int?) ->String
    {
        if index == 0 {
            return "First Step"
        }
        if index == totalCount!-1 {
            return "Only 2 steps left"
        }
        if index == totalCount {
            
            return "Great! You're on the last step!"
        }
        return "Step " + "\(index + 1) " + "of " + "\(totalCount! + 1)"
    }
    
    /// Set progress on progress bar
    ///
    /// - Parameter index: current index
    func setProgress(index:Int){
        
        // for first page, we don't know total number of pages
        if index == 0 {
            self.setProgress(index: 0, totalCount: nil)
        } else {
            
            // get total number of pages
            let arrClass = getArrayViewControllersClass(category: currentCategory)
            let totalVC =  arrClass.count
            self.setProgress(index: index, totalCount: totalVC)
        }
    }
    
    /// Get first view controller
    ///
    /// - Returns: array of first view controller
    fileprivate func getStartingViewController()->[UIViewController] {
        var arrViewController :[UIViewController] = []
        let vc = WhatHappenedVC()
        vc.currentPage = 0
        vc.delegate = self
//        vc.category = aEnumClaimPersonalElectronics.eADLD
        arrViewController.append(vc)
        return arrViewController
    }
    
    /// Return view controller via Class type
    ///
    /// - Parameters:
    ///   - forClass: class type
    ///   - category: category
    /// - Returns: view controller object of that class
    func getViewController(forClass :AnyClass , category : aEnumClaimPersonalElectronics) -> UIViewController? {
        var vc : BaseVC?
        if  forClass is HowAndWhereHappenedVC.Type{
            
            // What & where happened screen
            let baseVC = HowAndWhereHappenedVC()
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.descriptionText = ClaimHomeServe.shared.incidentDescription
                baseVC.prefetchIncidentPlace = ClaimHomeServe.shared.placeOfIncident
            }
            vc = baseVC
        } else if  forClass is WhenHappenVC.Type {
            
            // When happen class
            let baseVC = (forClass as! WhenHappenVC.Type).init()
            baseVC.currentPage = currentIndex
//            baseVC.category = currentCategory
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.prefetchedDateSelected = ClaimHomeServe.shared.dateWhenHappen
            }
            vc = baseVC
        } else if  forClass is WhatHappenedVC.Type {
            
            // What happened class
            let baseVC = (forClass as! WhatHappenedVC.Type).init()
//            baseVC.category = category
            baseVC.productCode = ClaimHomeServe.shared.productCode!
            baseVC.delegate = self
            vc = baseVC
        } else if  forClass is PartDamageVC.Type {
            
            // Damage part class
            let baseVC = (forClass as! PartDamageVC.Type).init()
            baseVC.delegate = self
            baseVC.productCode = ClaimHomeServe.shared.productCode!
//            baseVC.category = category
            if raisedFromCrm {
                baseVC.prefetchedDamagePart = ClaimHomeServe.shared.damagePartName
                baseVC.prefetchedConditions = ClaimHomeServe.shared.damageConditions
            }
            vc = baseVC
        } else if  forClass is BlockReferenceVC.Type {
            
            // Sim block screen
            // Not used in any case
            let baseVC = (forClass as! BlockReferenceVC.Type).init()
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.prefetchedRefNo = ClaimHomeServe.shared.blockReferenceNo
                baseVC.prefetchedDateSelected = ClaimHomeServe.shared.blockReferenceDate
            }
            vc = baseVC
        }
        return vc
    }
    
    /// Used to initialize category dictionary where key is category and value is class type
    func initialiseCategoryDictionaryForViewController()->[aEnumClaimPersonalElectronics:[AnyClass]] {
        var dictCategory = [aEnumClaimPersonalElectronics:[AnyClass]]()
        dictCategory[.eADLD] = self.getArrayViewControllersClass(category: .eADLD)
        dictCategory[.eEW] = self.getArrayViewControllersClass(category: .eEW)
        dictCategory[.eTheft] = self.getArrayViewControllersClass(category: .eTheft)
        return dictCategory
    }
    
    /// Used to get class type from category and index
    ///
    /// - Parameters:
    ///   - category: category
    ///   - dictCategory: dictionary of category
    ///   - index: index path
    /// - Returns: Any class object
    func getClass(category : aEnumClaimPersonalElectronics, dictCategory:[aEnumClaimPersonalElectronics:[AnyClass]],index : Int) ->AnyClass? {
        let newIndex = index - 1
        var arrClass = dictCategory[category]!
        if newIndex <= arrClass.count - 1{
            return arrClass[newIndex]
        }
        return nil;
    }
    
    /// Used to get array of view controller for perticular category
    ///
    /// - Parameter category: category
    /// - Returns: array of class type
    func getArrayViewControllersClass(category:aEnumClaimPersonalElectronics)->[AnyClass] {
        var arrClass:[AnyClass] = []
        switch category{
        case .eADLD:
            arrClass = [HowAndWhereHappenedVC.self,WhenHappenVC.self, PartDamageVC.self]
        case .eEW:
            arrClass = [HowAndWhereHappenedVC.self,WhenHappenVC.self]
        case .eTheft:
            arrClass = [HowAndWhereHappenedVC.self,WhenHappenVC.self]
        }
        return arrClass
    }
    // MARK:- Action Methods
    
}

extension PEProgressContainerVC: UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            return
        }
        
        setProgress(index:currentIndex)
    }
}

// MARK: WhatHappenedVCDelegate

extension PEProgressContainerVC: WhatHappenedVCDelegate{
    func clickSubmitButton(model: WhatHappenSection, category: Constants.Services) {
        
    }
    
    func whatHappenedButtonTapped(category: Constants.Services) {
        
    }
    
//    func whatHappenedButtonTapped(category: ClaimType) {
//        if let category = category as? aEnumClaimPersonalElectronics{
//            whatHappenedButtonTapped(category: category)
//        }
//    }
//
//    func clickSubmitButton(model:WhatHappenSection, category: ClaimType){
//        ClaimHomeServe.shared.issueId = model.issueId
//        ClaimHomeServe.shared.damageType = model.damageType
//        ClaimHomeServe.shared.insurancePartnerCode = model.partnerCode
//        moveToNext(controllerToLoad: nil)
//    }
//
    /// This button define the functionality on click of what happened button
    ///
    /// - Parameter category: category of PE
    private func whatHappenedButtonTapped(category:aEnumClaimPersonalElectronics){
        if currentCategory != category {
            totalVCInCategories = dictCategory[category]!.count
            currentCategory = category
            arrViewController.removeSubrange(ClosedRange(uncheckedBounds: (lower: 1, upper: arrViewController.count - 1)))
        }
    }
}

// MARK: IncidentDescriptionBaseVCDelegate

extension PEProgressContainerVC : IncidentDescriptionBaseVCDelegate {
    func clickTellUsMore(descripition: String, placeOfIncident: String?) {
        ClaimHomeServe.shared.incidentDescription = descripition
        ClaimHomeServe.shared.placeOfIncident = placeOfIncident
        moveToNext(controllerToLoad: nil)
    }
}

// MARK: ClaimWhenHappenDelegate
extension PEProgressContainerVC: ClaimWhenHappenDelegate {
    func clickWhenHapppen(date: String) {
        ClaimHomeServe.shared.dateWhenHappen = date
        let newDate = Date(string: date, formatter: .scheduleFormat) ?? Date()
        let dateTime = date.split(separator: " ")
        
        // In ADLD move to next screen
        if currentCategory != .eTheft && currentCategory != .eEW {
            if dateTime.count == 2 {

                EventTracking.shared.eventTracking(name: .srDtSubmit, [Constants.Event.EventKeys.date: newDate, Constants.Event.EventKeys.time: dateTime[1], Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? ""])
            }
            moveToNext(controllerToLoad: nil)
        } else {
            // In Theft and EW of PE, Request to create and update SR
            if dateTime.count == 2 {

                EventTracking.shared.eventTracking(name: .SRVisitDT_Submit, [Constants.Event.EventKeys.date: newDate, Constants.Event.EventKeys.time: dateTime[1], Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? ""])

            }
            raiseServiceRequest(category: currentCategory ?? .eADLD)
        }
    }
    
    /// Used to raise service request
    ///
    /// - Parameter category: category
    func raiseServiceRequest(category: ClaimType) {
        
        // Update SR
        if raisedFromCrm {
            Utilities.addLoader(onView: view, message: "Please wait while we are updating your service request", count: &loaderCount, isInteractionEnabled: true)
            interactor?.updateServiceRequest(for: category)
        } else {
            
            // Create SR
            Utilities.addLoader(onView: view, message: "Please wait while we are creating your service request", count: &loaderCount, isInteractionEnabled: true)
            
            interactor?.createServiceRequest(for: category)
        }
    }
}

// MARK: PartDamageVCDelegate
extension PEProgressContainerVC: PartDamageVCDelegate {
    
    func selectedDamages(selectedPart: CardOptions.ViewModel, reasons: [PartDamageViewModel]) {
        ClaimHomeServe.shared.damagePartName = selectedPart.title
        ClaimHomeServe.shared.damageConditions = reasons
        raiseServiceRequest(category: currentCategory ?? .eADLD)
    }
}

// MARK: PartDamageVCDelegate
// Not In use
extension PEProgressContainerVC: BlockReferenceVCDelegate {
    
    func blockReferenceSubmitted(refNo: String, pickedTime: String) {
        ClaimHomeServe.shared.blockReferenceNo = refNo
        ClaimHomeServe.shared.blockReferenceDate = pickedTime
        raiseServiceRequest(category: currentCategory ?? .eADLD)
    }
}

extension PEProgressContainerVC : ClaimSuccessVCDelegate {
    func clickCrossButton() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
//        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    func clickViewServiceRequest() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
//        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
}

// MARK:- Display Logic Conformance
extension PEProgressContainerVC: PEProgressContainerDisplayLogic {
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {[weak self] in
            self?.raiseServiceRequest(category: (self?.currentCategory)!)
        }) {}
    }
    
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?) {
        
        EventTracking.shared.eventTracking(name: .srInProgress, [Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? ""])
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?) {
        
        EventTracking.shared.eventTracking(name: .srInProgress, [Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? "", Constants.Event.EventKeys.srId: serviceDetails?.data?.serviceRequestId ?? ""])
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
//        presentingViewController?.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK:- Configuration Logic
extension PEProgressContainerVC {
    
    // USed to configure interactor and presenter
    fileprivate func setupDependencyConfigurator() {
        let interactor = PEProgressContainerInteractor()
        let presenter = PEProgressContainerPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension PEProgressContainerVC {
    
}
