//
//  BERDescriptionCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/19/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Used to display BER description cell
class BERDescriptionCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// Used to update Cell
    func updateUserInterface(description: String) {
        descriptionLabel.text = description
    }
}
