//
//  BERTableHeaderView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/19/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This Delegate gives callback when header of section clicked
protocol BERTableHeaderViewDelegate: class {
    
    /// Clicked on header of section in BER
    ///
    /// - Parameters:
    ///   - view: BER view
    ///   - sender: button
    func berTableHeaderView(_ view: BERTableHeaderView, clickedSectionButton sender: Any)
}

/// Used to show header view
class BERTableHeaderView: UIView {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    weak var delegate: BERTableHeaderViewDelegate?
    
    /// Clicked button section
    ///
    /// - Parameter sender: button object
    @IBAction func clickedSectionButton(_ sender: Any) {
        delegate?.berTableHeaderView(self, clickedSectionButton: sender)
    }
}
