//
//  BERDetailVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/19/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Type of Expandable View
///
/// - BERDetails: BER details
/// - FindPhone: Find my iPhone
enum ExpandViewType {
    case BERDetails
    case FindPhone
}

/// This class is used to show Expandable view
class BERDetailVC: BaseVC, HideNavigationProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var selectedIndex = 0
    var headers: [String]! = ["What is BER?", "Why is your claim BER?"]
    var expandViewType: ExpandViewType = .BERDetails
    var rowDetails: [[Any]] = [[Any]]()
    var claimBERModel: ClaimBERViewModel?
    var berInfo = [BERInfoModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    /// Used to initialize view
    private func initializeView() {
        switch expandViewType {
        case .BERDetails:
            tableView.register(nib: Constants.CellIdentifiers.WHCService.amountBreakupCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.amountBreakupCell)
            tableView.register(nib: Constants.CellIdentifiers.WHCService.berDescriptionCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.berDescriptionCell)
            tableView.register(nib: Constants.CellIdentifiers.WHCService.berInfoCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.berInfoCell)
            
            berInfo = BERInfoModel.getBERInfo()
            
        case .FindPhone:
            tableView.register(nib: Constants.CellIdentifiers.WHCService.imageViewCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.imageViewCell)
        }
    }
    
    /// Clicked cross button
    @IBAction func clickedBtnClose(_ sender: Any) {
        //TODO: Handle code accordingly
        dismiss(animated: true, completion: nil)
    }
}

extension BERDetailVC: BERTableHeaderViewDelegate {
    func berTableHeaderView(_ view: BERTableHeaderView, clickedSectionButton sender: Any) {
        var sectionsToRefresh: IndexSet = IndexSet()
        if view.tag == selectedIndex {
            
            // Close expanded view
            sectionsToRefresh.insert(selectedIndex)
            selectedIndex = -1
        } else {
            
            // Close Expanded view and show selected view
            if selectedIndex >= 0 {
                sectionsToRefresh.insert(selectedIndex)
            }
            selectedIndex = view.tag
            sectionsToRefresh.insert(selectedIndex)
        }
        tableView.reloadData()
        tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
}

extension BERDetailVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return headers.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard section == selectedIndex else {
            return 0
        }
        
        if expandViewType == .FindPhone {
            return rowDetails[section].count
        }
        
        if expandViewType == .BERDetails && section == 1 {
            return berInfo.count + 1
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch expandViewType {
        case .BERDetails:
            if indexPath.section == 0 {
                // Show BER description cell
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.berDescriptionCell, for: indexPath) as! BERDescriptionCell
                return cell
            } else {
                
                if indexPath.row == 0 {
                    
                    // Show breakup amount cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.amountBreakupCell, for: indexPath) as! AmountBreakupCell
                    if claimBERModel != nil {
                        cell.updateUserInterface(claimBERModel: claimBERModel!)
                    }
                    return cell
                } else {
                    
                    // show Breakup amount description cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.berInfoCell, for: indexPath) as! BERInfoCell
                    cell.titleLabel.text = berInfo[indexPath.row - 1].title
                    cell.descriptionLabel.text = berInfo[indexPath.row - 1].titleDescription
                    return cell
                }
            }
            
        case .FindPhone:
            
            // Show find my phone cell
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.imageViewCell, for: indexPath) as! ImageViewCell
            cell.cellImageView.image = rowDetails[indexPath.section][indexPath.row] as? UIImage
            return cell
        }
    }
}

extension BERDetailVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = Bundle.main.loadNibNamed(Constants.CellIdentifiers.WHCService.berTableHeaderView, owner: nil, options: nil)?.first as? BERTableHeaderView
        header?.tag = section
        header?.delegate = self
        header?.headerLabel.text = headers[section]
        
        let angle: CGFloat = section == selectedIndex ? .pi : 0
        header?.arrowImageView.transform = CGAffineTransform(rotationAngle: angle)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == selectedIndex ? 57 : section == headers.count - 1 ? 57 : 72
    }
}
