//
//  BERBreakupView.swift
//  OneAssist-Swift
//
//  Created by Varun on 21/05/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Used to show BER Breakup view
class BERBreakupView: UIView {

    @IBOutlet weak var titleLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var valueLabel: BodyTextRegularGreyLabel!
    
    override func awakeFromNib() {
    }
    
    /// Update BER Breakup view
    ///
    /// - Parameters:
    ///   - title: title of cell
    ///   - value: description of cell
    func updateUserInterface(title: String, value: String) {
        titleLabel.text = title
        valueLabel.text = value
    }
}
