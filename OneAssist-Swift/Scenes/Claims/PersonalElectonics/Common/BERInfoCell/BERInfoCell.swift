//
//  BERInfoCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 16/05/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// BER breakup amount info model
struct BERInfoModel {
    
    var title: String
    var titleDescription: String
    
    static func getBERInfo() -> [BERInfoModel] {
        let info = ["What is depreciation amount?": "Depreciation is loss in value of the handset with use over a period of time. The standard depreciation scale applicable for your plan is as under.",
                    "What is salvage amount?": "The insurer upon settling the claim (Incase of total loss/BER) take and keep possession of the gadget damaged and entitled for salvation charge.",
                    "What is under insurance?": "Deduction origination when sum assured opted is less than the device market value.",
                    "What is excess charges?": "Excess charges are additional claim process charges payable by customers basis claim value."]
        var arrBerInfo = [BERInfoModel]()
        for i in info {
            arrBerInfo.append(BERInfoModel(title: i.key, titleDescription: i.value))
        }
        return arrBerInfo
    }
}

/// BER breakup amount info cell
class BERInfoCell: UITableViewCell {

    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /// Used to update UI of cell
    ///
    /// - Parameters:
    ///   - title: Title of cell
    ///   - description: description of cell
    func updateUserInterface(title: String?, description:  String?) {
        titleLabel.text = title
        descriptionLabel.text = description
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
