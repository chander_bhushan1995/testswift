//
//  ClosureDateView.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 23/07/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This class is used to show the estimated closure Date of Service request.
class ClosureDateView: UIView {

    @IBOutlet weak var closureLabel: UILabel!
    
    override func awakeFromNib() {
    }
    
    /// This method used to update the label which contains estimated date.
    ///
    /// - Parameter dateString: String that contains complete message with dates.
    func updateLabel(dateString: String?) {
        closureLabel.text = dateString
    }
}
