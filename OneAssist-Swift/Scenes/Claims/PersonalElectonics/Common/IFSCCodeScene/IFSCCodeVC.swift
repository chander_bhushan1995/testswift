//
//  IFSCCodeVC.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/11/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// This class displays the *How to find IFSC code* View
class IFSCCodeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Clicked button Cancel
    @IBAction func clickedBtnCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
