//
//  DateTimeCell.swift
//  OneAssist-Swift
//
//  Created by Raj on 26/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class DateTimeCell: UITableViewCell {

    @IBOutlet weak var timeLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var dayLabel: SupportingTextRegularGreyLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
