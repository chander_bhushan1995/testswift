//
//  ServiceCenterTimeScheduleVC.swift
//  OneAssist-Swift
//
//  Created by Raj on 25/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class ServiceCenterTimeScheduleVC: BaseVC {
    @IBOutlet weak var titleLabel:H3BoldLabel!
    @IBOutlet weak var tblView: UITableView!
    var timingDetails:[PartnerBUOperationHour]?
    var tempTimingDetails:[TempPartnerBUOperationHour]?
    var dateFormatter = DateFormatter()
    var timeString:String!
    override func viewDidLoad() {
        super.viewDidLoad()
       tblView.register(UINib(nibName: Constants.CellIdentifiers.dateTime , bundle: nil), forCellReuseIdentifier: Constants.CellIdentifiers.dateTime)
        
    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        tblView.reloadData()
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension ServiceCenterTimeScheduleVC :UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timingDetails?.count ?? tempTimingDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:Constants.CellIdentifiers.dateTime, for: indexPath) as! DateTimeCell
        if let timingDetails = timingDetails{
        cell.dayLabel.text = timingDetails[indexPath.row].weekDay ?? ""
            if timingDetails[indexPath.row].operational == "Y" {
                // TODO: why harcoded date??
                let fromStringDate = "06-May-2019 " + (timingDetails[indexPath.row].operationTimeFrom ?? "") + ":00"
                dateFormatter = .scheduleFormat
                let fromDate = dateFormatter.date(from:fromStringDate)
                dateFormatter = .time12period
                timeString = "\(dateFormatter.string(from: fromDate ?? Date())) - "
                let toStringDate = "06-May-2019 " + (timingDetails[indexPath.row].operationTimeTo ?? "") + ":00"
                dateFormatter = .scheduleFormat
                let toDate = dateFormatter.date(from:toStringDate)
                dateFormatter = .time12period
                timeString = timeString + dateFormatter.string(from: toDate ?? Date())
                cell.timeLabel.text = timeString
            }else{
                cell.timeLabel.text = Strings.ServiceRequest.textFieldText.closed
            }
        }else{
            cell.dayLabel.text = tempTimingDetails?[indexPath.row].weekDay ?? ""
            cell.timeLabel.text = tempTimingDetails?[indexPath.row].operationTimeFrom
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}
