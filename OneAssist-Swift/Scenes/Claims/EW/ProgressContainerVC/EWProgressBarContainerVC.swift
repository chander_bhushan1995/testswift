//
// EWProgressBarContainerVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 02/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.


import UIKit

protocol EWProgressBarContainerBusinessLogic {
    func createServiceRequest(for type: ClaimType)
    func updateServiceRequest()
    
}

class EWProgressBarContainerVC: BaseVC {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var pagingView: UIView!
    var animating : Bool = false
    
    var arrViewController :[UIViewController] = []
    var pageViewController: UIPageViewController!

    var currentIndex:Int = 0
    
    var interactor: EWProgressBarContainerBusinessLogic?
  
    var dictCategory : [aEnumClaimHomeExtendedWarranty:[AnyClass]]!
    var category:aEnumClaimHomeExtendedWarranty!
    var raisedFromCrm = false

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
    }
    
   
    func initialiseView(){
        initializePagingController()
        self.lblProgress.font = DLSFont.supportingText.regular

        self.category = aEnumClaimHomeExtendedWarranty.eExtendedWarranty
        self.dictCategory = self.initialiseCategoryDictionaryForViewController()
                pageViewController.delegate = self
        arrViewController = getStartingViewController()
        pageViewController.setViewControllers([arrViewController[0]], direction: .forward, animated: false, completion: nil)
        self.setProgress(index: 0)
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style:.done , target: self, action: #selector(self.clickBackButton))
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = Strings.ServiceRequest.Titles.raiseARequest

    }
    
    
    func setProgress(index : Int , totalCount : Int)  {
        self.progressView.setProgress(Float(index+1)/Float(totalCount+2), animated: true)
        self.lblProgress.text = getProgressLabelText(index : index, totalCount: totalCount)
    }
    
    func initializePagingController() {
        currentIndex = 0
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.frame = pagingView.bounds
        pagingView.addSubview(pageViewController.view)
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)

    }
    
    @objc func clickBackButton(){
        if(currentIndex == 0){
            self.navigationController?.popViewController(animated: true)
        }else{
            currentIndex = currentIndex - 1
            setProgress(index : currentIndex)
            let nextVC = arrViewController[currentIndex]
            self.pageViewController.setViewControllers([nextVC], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func moveToNext() {
        if animating == true {
            return
        }
        animating = true
        
        let nextIndexToLoad = self.currentIndex + 1
        if nextIndexToLoad >= arrViewController.count {
            guard let classToLoad = getClass(category: self.category, dictCategory: self.dictCategory, index: nextIndexToLoad)else {
                return
            }
            let nextVC = getViewController(forClass: classToLoad)!
            setProgress(index: nextIndexToLoad)
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true){_ in self.animating = false}
            arrViewController.append(nextVC)
        } else {
            let nextVC = arrViewController[nextIndexToLoad]
            setProgress(index: nextIndexToLoad)
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true){_ in self.animating = false}
        }
        self.currentIndex = nextIndexToLoad
    }
    
    func getProgressLabelText(index : Int , totalCount : Int?) ->String
    {
        if index == 0 {
            return "Step 1"
        }
        return "Step " + "\(index+1) " + "of " + "\(totalCount! + 1)"
    }
    
    func setProgress(index:Int){
            let arrClass = getArrayViewControllersClass(category: self.category)
            let totalVC =  arrClass.count
            self.setProgress(index: index, totalCount: totalVC)
    }
    func  setProgressLastStep() {
        let arrClass = getArrayViewControllersClass(category: self.category)
        let totalVC =  arrClass.count
        self.setProgress(index: totalVC, totalCount: totalVC)
    }
    
    
    fileprivate func getStartingViewController()->[UIViewController] {
        
        var arrViewController :[UIViewController] = []
        let vc = WhatHappenedVC()
        vc.productCode = ClaimHomeServe.shared.productCode
        vc.preSelectedIssueId = ClaimHomeServe.shared.issueId
        vc.category = .extendedWarranty
        vc.delegate = self
        arrViewController.append(vc)
        return arrViewController
    }
    
  func getViewController(forClass :AnyClass) -> UIViewController? {
        var vc : BaseVC?
        if  forClass is WhenHappenVC.Type {
            let baseVC = (forClass as! WhenHappenVC.Type).init()
            baseVC.currentPage = currentIndex
//            baseVC.category = aEnumClaimHomeExtendedWarranty.eExtendedWarranty
            if raisedFromCrm {
                baseVC.prefetchedDateSelected = ClaimHomeServe.shared.dateWhenHappen
            }
            baseVC.delegate = self
            vc = baseVC
        }
        return vc
    }
    
    
    func initialiseCategoryDictionaryForViewController()->[aEnumClaimHomeExtendedWarranty:[AnyClass]] {
        var dictCategory = [aEnumClaimHomeExtendedWarranty:[AnyClass]]()
        dictCategory[.eExtendedWarranty ] = self.getArrayViewControllersClass(category:.eExtendedWarranty)
        dictCategory[.ePms] = self.getArrayViewControllersClass(category: .ePms)
        return dictCategory
    }
    
    func getClass(category : aEnumClaimHomeExtendedWarranty , dictCategory:[aEnumClaimHomeExtendedWarranty :[AnyClass]], index : Int) -> AnyClass? {
        let newIndex = index - 1
        var arrClass = dictCategory[category]!
        if newIndex <= arrClass.count - 1{
            return arrClass[newIndex]
        }
        return nil;
    }
    
    
    func getArrayViewControllersClass(category:aEnumClaimHomeExtendedWarranty)->[AnyClass] {
        var arrClass:[AnyClass] = []
        switch category{
        case .eExtendedWarranty:
            arrClass = [WhenHappenVC.self]
        case .ePms:
            arrClass = [AppliancesNotWorkingVC.self, ScheduleVisitVC.self]
        default :
            arrClass = [WhenHappenVC.self]
        }
        return arrClass
    }
}

// MARK: AppliancesNotWorkingDelegate

extension EWProgressBarContainerVC: AppliancesNotWorkingDelegate {
    func applianceNotWorkingButtonTapped(models: [AppliancesNotWorkingModel]) {
        if category == aEnumClaimHomeExtendedWarranty.ePms {
            ClaimHomeServe.shared.productCode = models.first?.productCode
            ClaimHomeServe.shared.productName = models.first?.name
            ClaimHomeServe.shared.productVariantId = models.first?.productVariantId
        }
        ClaimHomeServe.shared.assetIds = models.compactMap{$0.assetId}
        moveToNext()
    }
}

extension EWProgressBarContainerVC: ScheduleVisitDelegate {
    func clickedScheduleVisit(date: Date, slot: ScheduleVisitTimeSlotViewModel, serviceReqId: String?, crmTrackingNumber: String?) {
        ClaimHomeServe.shared.scheduledDate = date
        ClaimHomeServe.shared.scheduledTimeslot = slot
        raiseServiceRequest(category: category)
    }
}


// MARK:- Configuration Logic
extension EWProgressBarContainerVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = EWProgressBarContainerInteractor()
        let presenter = EWProgressBarContainerPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension EWProgressBarContainerVC: EWProgressBarContainerDisplayLogic {
    
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?){
        
//        EventTracking.shared.eventTracking(name: .srInProgress, [Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.getServiceCode() ?? ""])

        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }

    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?) {
        
//        EventTracking.shared.eventTracking(name: .srInProgress, [Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.getServiceCode() ?? "", Constants.Event.EventKeys.srId: serviceDetails?.data?.serviceRequestId ?? ""])

        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if category == aEnumClaimHomeExtendedWarranty.ePms {
            let vc = ClaimSuccessVC()
            let model = ClaimSuccessViewModel()
            model.scheduleSlotStartDateTime = serviceDetails?.data?.scheduleSlotStartDateTime ?? ""
            model.scheduleSlotEndDateTime = serviceDetails?.data?.scheduleSlotEndDateTime ?? ""
            model.successType = .srSuccessWithVisit
            model.srId = serviceDetails?.data?.serviceRequestId?.description
            model.crmTrackingNumber = serviceDetails?.data?.refPrimaryTrackingNo
            vc.successModel = model
            vc.delegate = self
            presentInFullScreen(vc, animated: true, completion: nil)
            
        } else {
            
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            //        dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func displayError(_ error: String) {
        self.setProgressLastStep()
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

// MARK: UIPageViewControllerDelegate

extension EWProgressBarContainerVC: UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            return
        }
        setProgress(index:currentIndex)
    }
    
}



// MARK: ClaimWhenHappenDelegate
extension EWProgressBarContainerVC : ClaimWhenHappenDelegate {
    func clickWhenHapppen(date: String) {
        ClaimHomeServe.shared.dateWhenHappen = date
        let newDate = Date(string: date, formatter: .scheduleFormat) ?? Date()
        let dateTime = date.split(separator: " ")
        if dateTime.count == 2 {

//            EventTracking.shared.eventTracking(name: .srDtSubmit, [Constants.Event.EventKeys.date: newDate, Constants.Event.EventKeys.time: dateTime[1], Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.getServiceCode() ?? ""])
//            EventTracking.shared.eventTracking(name: .SRVisitDT_Submit, [Constants.Event.EventKeys.date: newDate, Constants.Event.EventKeys.time: dateTime[1], Constants.Event.EventKeys.subcategory: ClaimHomeServe.shared.productName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.getServiceCode() ?? ""])

        }
        
        if category == aEnumClaimHomeExtendedWarranty.ePms {
            moveToNext()
        } else {
            raiseServiceRequest(category: category)
        }
       
    }
    
    func raiseServiceRequest(category: ClaimType) {
        
        self.progressView.setProgress(1.0, animated: true)
        if raisedFromCrm {
            Utilities.addLoader(onView: view, message: "Please wait while we are updating your service request", count: &loaderCount, isInteractionEnabled: true)
            interactor?.updateServiceRequest()
        } else {
            Utilities.addLoader(onView: view, message: "Please wait while we are creating your service request", count: &loaderCount, isInteractionEnabled: true)
            interactor?.createServiceRequest(for: category)
        }
    }
}

// MARK: WhatHappenedVCDelegate
extension EWProgressBarContainerVC: WhatHappenedVCDelegate {
    func clickSubmitButton(model: WhatHappenSection, category: Constants.Services) {
        ClaimHomeServe.shared.issueId = model.issueId
        ClaimHomeServe.shared.issueDescription = model.title
        moveToNext()
    }

    func whatHappenedButtonTapped(category: Constants.Services) {

    }
    
}

extension EWProgressBarContainerVC: ClaimSuccessVCDelegate {
    func clickCrossButton() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    func clickViewServiceRequest() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
}



