//
//  EWProgressBarContainerInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/27/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol EWProgressBarContainerPresentationLogic {
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?)
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?)

}

class EWProgressBarContainerInteractor: EWProgressBarContainerBusinessLogic {
    var presenter: EWProgressBarContainerPresentationLogic?
    
    func updateServiceRequest() {
        let currentUser = UserCoreDataStore.currentUser
        let request = UpdateServiceRequestDTO()
        request.requestDescription = ClaimHomeServe.shared.incidentDescription //
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen //
        request.referenceNo = ClaimHomeServe.shared.memberShipId //
        request.modifiedBy = currentUser?.cusId //
        request.refSecondaryTrackingNo = ClaimHomeServe.shared.assetIds?.joined(separator: ",")
        request.serviceRequestId = ClaimHomeServe.shared.serviceRequestID
        request.workFlowValue = "1"
        if let issueId = ClaimHomeServe.shared.issueId {
            let issue = IssueReportedByCustomer(dictionary: [:])
            issue.issueId = issueId.stringValue //
            issue.issueDescription = ClaimHomeServe.shared.issueDescription //
            request.issueReportedByCustomer = [issue]
        }
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName //
        addressDetails.countryCode = "IND" //
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com" //
        addressDetails.mobileNo = CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber //
        addressDetails.pincode = ClaimHomeServe.shared.pinCode //
        addressDetails.addressLine1 = ClaimHomeServe.shared.address //
        
        request.serviceRequestAddressDetails = addressDetails
        
        UpdateServiceRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
    }
    
    func createServiceRequest(for type: ClaimType) {
        let currentUser = UserCoreDataStore.currentUser
        let request = CreateServiceRequestPlanRequestDTO()
        request.placeOfIncident = ClaimHomeServe.shared.address //
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen //
        // Change
        request.requestDescription = ClaimHomeServe.shared.issueDescription
        request.serviceRequestType = Constants.AssetServices.extendedWarranty.rawValue //
        request.referenceNo = ClaimHomeServe.shared.memberShipId //
                request.createdBy = currentUser?.cusId //
        request.refSecondaryTrackingNo = ClaimHomeServe.shared.assetIds?.joined(separator: ",")
        
        if let issueId = ClaimHomeServe.shared.issueId {
            let issue = IssueReportedByCustomer(dictionary: [:])
            issue.issueId = issueId.stringValue //
            issue.issueDescription = ClaimHomeServe.shared.issueDescription //
            request.issueReportedByCustomer = [issue]
        }
        
        if let flag = ClaimHomeServe.shared.supportedRequestTypes?.filter({ $0.service.rawValue == type.getServiceCode() }).first?.isInsuranceBacked, !flag {
            if let pc = ClaimHomeServe.shared.productCode {
                let product = Product(dictionary: [:])
                product.productCode = pc
                request.assets = [product]
            }
        }
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName //
        addressDetails.countryCode = "IND" //
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com" //
        addressDetails.mobileNo = CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber //
        addressDetails.pincode = ClaimHomeServe.shared.pinCode //
        addressDetails.addressLine1 = ClaimHomeServe.shared.address //
        
        request.serviceRequestAddressDetails = addressDetails
        
        CreateServiceRequestPlanRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
        
    }
}
