//
//  EWProgressBarContainerPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/27/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol EWProgressBarContainerDisplayLogic: class {
    func displayError(_ error: String)
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?)
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?)

}

class EWProgressBarContainerPresenter: BasePresenter, EWProgressBarContainerPresentationLogic {
    weak var viewController: EWProgressBarContainerDisplayLogic?
    
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayCreatedServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayUpdateServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }

    
}
