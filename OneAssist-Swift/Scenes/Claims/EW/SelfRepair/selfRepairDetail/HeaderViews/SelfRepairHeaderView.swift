//
//  SelfRepairHeaderView.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 11/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class SelfRepairHeaderView: UIView {

    @IBOutlet weak var topMarginView: UILabel!
    
    @IBOutlet weak var lblHeading: UILabel!
 
    @IBOutlet weak var bottomMarginView: UILabel!
    @IBOutlet weak var imgViewObj: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseView()
    }
    
    func initialiseView(){
        
    }
    func setModel(_ viewModel: InspectionDetailViewModel, forSection section: Int) {
        tag = section
        switch viewModel.detailState {
        case .notStarted:
            lblHeading.isHidden = true
            //buttonDetails.isHidden = true
            //labelDisabledHeading.text = viewModel.heading
            imgViewObj.image = #imageLiteral(resourceName: "timelineInactive")
            //collapseImageView.isHidden = true
        case .started:
//            if viewModel.subHeadingWithNoDetails != nil {
//                buttonDetails.setTitle(viewModel.subHeadingWithNoDetails, for: .normal)
//                buttonDetails.setTitleColor(UIColor.lightGray, for: .normal)
//                buttonDetails.isUserInteractionEnabled = false
//                collapseImageView.isHidden = true
//            }
           // labelDisabledHeading.isHidden = true
            lblHeading.text = viewModel.heading
            imgViewObj.image = #imageLiteral(resourceName:"timelineInProgress")
        case .completed:
//            if (viewModel.subHeadingWithNoDetails != nil) {
//                buttonDetails.setTitle(viewModel.subHeadingWithNoDetails, for: .normal)
//                buttonDetails.setTitleColor(UIColor.lightGray, for: .normal)
//                buttonDetails.isUserInteractionEnabled = false
//                collapseImageView.isHidden = true
//            }
           // labelDisabledHeading.isHidden = true
            lblHeading.text = viewModel.heading
            imgViewObj.image = #imageLiteral(resourceName:"timelineDone")
        }
    }
    
    
    

}
