//
//  ServiceCenterView.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 17/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
protocol ServiceCenterDelegate:class {
 func collapseClicked(expanded: Bool)
}
class ServiceCenterView: UIView {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var labelHeading: UILabel!
   // @IBOutlet weak var buttonDetails: UIButton!
    
    @IBOutlet weak var btnDetails: UIButton!
    //@IBOutlet weak var collapseImageView: UIImageView!
    
    @IBOutlet weak var imgCollapse: UIImageView!
  //  @IBOutlet weak var labelName: UILabel!
    weak var delegate:ServiceCenterDelegate?
    
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseView()
    }
    
    func initialiseView(){
        //btnDetails.isSelected = false
    }
   
    func setModel(_ model:ServiceCenterModel){
        //lblName.text = model.name
        //lblHeading.text = Strings.ServiceRequest.SelfRepair.firstHeader
    }
    func toggleButtonState(_ selected: Bool) {
//        btnDetails.isSelected = selected
//        let angle: CGFloat = selected ? .pi : 0
//        imgCollapse.transform = CGAffineTransform(rotationAngle: angle)
    }
    
    func collapseClicked(expanded: Bool){
//     btnDetails.isSelected = !btnDetails.isSelected
//        delegate?.collapseClicked(expanded:btnDetails.isSelected)
    }

}
