//
//  FinalInvoiceVerification.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 11/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit



enum Verification{
    case estimationInvoiceVerification
    case finalInvoiceVerification
    case Refund
}

class FinalInvoiceVerification: UITableViewCell {
   
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        intialiseView()
    }
    
    func intialiseView(){
        lblDesc.text = Strings.ServiceRequest.SelfRepair.finalInvoiceVerificationTitle
    }
    
}
