//
//  TestingVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
/*
 weak var delegate : ServiceRequestUploadInvoiceCellDelegate?
 @IBOutlet weak var lblAsk: UILabel!
 @IBOutlet weak var viewInvoice: UIView!
 
 @IBOutlet weak var imgInvoice: UIImageView!
 @IBOutlet weak var viewIndicator: UIView!
 
 @IBOutlet weak var viewCompleted: UIView!
 @IBOutlet weak var viewInnerInvoice: UIView!
 
 @IBOutlet var btnHeightConstraint: NSLayoutConstraint!
 
 @IBOutlet var completedHeightConstraint: NSLayoutConstraint!
 
 @IBOutlet var IndicatorHeightConstraint: NSLayoutConstraint!
 
 @IBOutlet var btnUpload: UIButton!
 @IBOutlet var btnSubmit: PrimaryButton!
 @IBOutlet weak var lblUpload: UILabel!
 */
class TestingVC: UIViewController ,ServiceRequestUploadInvoiceCellDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellView = Bundle.main.loadNibNamed("UploadEstimationInVoiceCell", owner: self, options: nil)?[0] as? UploadEstimationInVoiceCell
        cellView?.delegate  = self
        self.view.addSubview(cellView!)
    }
    
    
    func clickPhotoBtn(cell:UploadEstimationInVoiceCell){
        Utilities.presentAlertController(vc: self) {(image, error) in
            if image != nil {
                let data = image!.jpegData(compressionQuality: 0.9)
               // row.image = UIImage(data: data!)
                cell.imgInvoice.image = UIImage(data:data!)
                cell.viewInnerInvoice.isHidden = true
                cell.btnSubmit.isEnabled = true
                }
            else{
              
            }
        }
    }

    func clickSubmitBtn(cell:UploadEstimationInVoiceCell){
        
    }
}
