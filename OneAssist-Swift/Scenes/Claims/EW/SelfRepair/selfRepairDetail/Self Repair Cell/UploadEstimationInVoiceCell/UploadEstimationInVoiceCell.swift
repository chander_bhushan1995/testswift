//
//  UploadEstimationInVoiceCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 11/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ServiceRequestUploadInvoiceCellDelegate:class{
    func clickPhotoBtn(cell:UploadEstimationInVoiceCell)
    func clickSubmitBtn(cell:UploadEstimationInVoiceCell)
}

enum UploadInvoice{
    case UploadEstimationInVoice
    case UploadFinalInVoice
}

enum EnumSelfRepairService{
    case eSubmit(image: UIImage?)
    case eProceeding(image: UIImage)
    case eCompleted(image: UIImage)
    case eNone(urlStr: String)
}

struct UploadEstimationInVoiceViewModel {
    var status: EnumSelfRepairService
    var desc: String
}

class UploadEstimationInVoiceCell: UITableViewCell {
    
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var imgUpload: UIImageView!
    @IBOutlet weak var btnUpload: UIButton!
    weak var delegate : ServiceRequestUploadInvoiceCellDelegate?
    @IBOutlet weak var lblAsk: UILabel!
    @IBOutlet weak var submittedSuccessfullyLbl: UILabel!
    @IBOutlet weak var viewInvoice: UIView!
    
    @IBOutlet weak var imgInvoice: UIImageView!
    @IBOutlet weak var viewIndicator: UIView!
    
    @IBOutlet weak var viewCompleted: UIView!
    @IBOutlet weak var viewInnerInvoice: UIView!
    
    @IBOutlet var btnTopConstraint: NSLayoutConstraint!
    //
    @IBOutlet var completedTopConstraint: NSLayoutConstraint!
    //
    @IBOutlet var IndicatorTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint!
//    
    @IBOutlet var completedHeightConstraint: NSLayoutConstraint!
//
    @IBOutlet var IndicatorHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet weak var lblUpload: UILabel!
    var spinnerView: SpinnerView?
    
    @IBAction func clickUploadBtn(_ sender: Any) {
        delegate?.clickPhotoBtn(cell:self)
    }

    @IBAction func clickSubmitBtn(_ sender: Any) {
        delegate?.clickSubmitBtn(cell:self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        intializeView()
    }
    
    func intializeView() {
        viewInvoice.layer.borderColor = UIColor.gray2.cgColor
        viewInvoice.layer.cornerRadius = 6.0
        viewInvoice.layer.borderWidth = 1.0
        
        imgInvoice.layer.borderColor = UIColor.gray2.cgColor
        imgInvoice.layer.cornerRadius = 6.0
        imgInvoice.layer.borderWidth = 1.0
        imgInvoice.clipsToBounds = true
        
        lblUpload.textColor = .dodgerBlue
        submittedSuccessfullyLbl.textColor = .seaGreen
        
        // add custom loader
        spinnerView = SpinnerView(view: indicator)
        spinnerView?.startAnimating()
        
    }
    
    func updateUserInterface(descText: String, type: EnumSelfRepairService) {
        
        switch type{
            
        case .eCompleted(let img):
            lblAsk.text = descText
            
            IndicatorTopConstraint.priority = UILayoutPriority(998)
            completedTopConstraint.priority = UILayoutPriority(999)
            btnTopConstraint.priority = UILayoutPriority(998)
            completedHeightConstraint.constant = 45
            IndicatorHeightConstraint.constant = 0
            btnHeightConstraint.constant = 0
            
            btnUpload.isEnabled = false
            lblUpload.isHidden = true
            imgUpload.isHidden = true
            imgInvoice.image = img
            imgEdit.isHidden = true
            
        case .eProceeding(let img):
            lblAsk.text = descText
            
            IndicatorTopConstraint.priority = UILayoutPriority(999)
            completedTopConstraint.priority = UILayoutPriority(998)
            btnTopConstraint.priority = UILayoutPriority(998)
            completedHeightConstraint.constant = 0
            IndicatorHeightConstraint.constant = 45
            btnHeightConstraint.constant = 0
            
            btnUpload.isEnabled = false
            lblUpload.isHidden = true
            imgUpload.isHidden = true
            imgInvoice.image = img
//            indicator.startAnimating()
            spinnerView?.startAnimating()
            imgEdit.isHidden = true
            
        case .eSubmit(let img):
            lblAsk.text = descText
            IndicatorTopConstraint.priority = UILayoutPriority(998)
            completedTopConstraint.priority = UILayoutPriority(998)
            btnTopConstraint.priority = UILayoutPriority(999)
            completedHeightConstraint.constant = 0
            IndicatorHeightConstraint.constant = 0
            btnHeightConstraint.constant = 45
            
            if img != nil {
                btnUpload.isEnabled = true
                lblUpload.isHidden = true
                imgUpload.isHidden = true
                imgInvoice.image = img
                btnSubmit.isEnabled = true
                imgEdit.isHidden = false
            } else {
                btnUpload.isEnabled = true
                lblUpload.isHidden = false
                imgUpload.isHidden = false
                imgInvoice.image = nil
                btnSubmit.isEnabled = false
                imgEdit.isHidden = true
            }
            
        case .eNone(let urlStr):
            
            lblAsk.text = descText
            completedHeightConstraint.constant = 0
            IndicatorHeightConstraint.constant = 0
            btnHeightConstraint.constant = 0
            
            btnUpload.isEnabled = false
            lblUpload.isHidden = true
            imgUpload.isHidden = true
            imgInvoice.setImageWithUrlString(urlStr ?? "")
            imgEdit.isHidden = true
        }
        
        self.layoutIfNeeded()
    }
}
