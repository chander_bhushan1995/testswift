//
//  ServiceCenterCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 11/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class ServiceCenterCell: UITableViewCell {

 //   @IBOutlet weak var labelAddress: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var labelContactNumber: UILabel!
    
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var labelPhoneNumber: UILabel!
    
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        intialiseView()
    }
    
    func intialiseView(){
        lblAddress.text = Strings.ServiceRequest.SelfRepair.estimationCelltitle
        lblContactNumber.text = Strings.ServiceRequest.SelfRepair.ServiceContactNumberTitle
    }
    
    
}
