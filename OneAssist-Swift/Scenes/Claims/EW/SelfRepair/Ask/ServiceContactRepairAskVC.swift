//
//  ServiceContactRepairAskVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 11/01/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class ServiceContactRepairAskVC: UIViewController {

    @IBOutlet weak var btnCross: UIButton!
    
 //   @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
  //  @IBOutlet weak var labelDontWorry: UILabel!
    
    @IBOutlet weak var lblDontWorry: UILabel!
    
  //  @IBOutlet weak var labelDesc: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var btnContact: PrimaryButton!
    
    
   // @IBOutlet weak var labelSelfRepair: UILabel!
    @IBOutlet weak var lblSelfRepair: UILabel!
    @IBOutlet weak var btnSelfRepair: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialise()
        // Do any additional setup after loading the view.
    }
    func initialise(){
        lblTitle.text = Strings.ServiceRequest.ContactSelfRepairAsk.labelTitle
        lblSubtitle.text = Strings.ServiceRequest.ContactSelfRepairAsk.labelSubtitle
        lblDontWorry.text = Strings.ServiceRequest.ContactSelfRepairAsk.dontWorry
        lblDesc.text = Strings.ServiceRequest.ContactSelfRepairAsk.labelDesc
        lblSelfRepair.text = Strings.ServiceRequest.ContactSelfRepairAsk.selfRepair
        lblSelfRepair.textColor = UIColor.dodgerBlue
        lblTitle.textColor = UIColor.scarlet
        lblSubtitle.textColor = UIColor.scarlet
    }
    @IBAction func clickSelfRepairBtn(_ sender: Any) {
        
    }
    @IBAction func clickContactBtn(_ sender: Any) {
        
    }
 
    @IBAction func clickCrossBtn(_ sender: Any) {
    }
    
}
