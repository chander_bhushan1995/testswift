//
//  SelfRepairVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/01/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol SelfRepairBusinessLogic {
    
}

class SelfRepairVC: BaseVC {
    var interactor: SelfRepairBusinessLogic?
    
    // deciding whether this cell should expand or not
    var shouldExpend:[Bool] = [false,true,false,false,false,false]
    var verificationType :Verification = .estimationInvoiceVerification
    var uploadType:UploadInvoice = .UploadEstimationInVoice
    var isServiceCellExpanded:Bool = false
     var selectedSection: Int = 1
     var inspectionDetails = [Any]()
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        initialiseData()
        registerCells()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func registerCells(){
//        static let finalInvoiceVerification = "FinalInvoiceVerification"
//        static let uploadEstimationInvoiceCell = "UploadEstimationInVoiceCell"
//        static let serviceCenterCall = "ServiceCenterCell"
//        static let selfRepairHeaderView = "SelfRepairHeaderView"
//        Constants.WHCService.NIBNames.finalInvoiceVerification
          tableView.register(nib: Constants.NIBNames.WHCService.finalInvoiceVerification, forCellReuseIdentifier:  Constants.NIBNames.WHCService.finalInvoiceVerification)
          tableView.register(nib: Constants.NIBNames.WHCService.uploadEstimationInvoiceCell, forCellReuseIdentifier:  Constants.NIBNames.WHCService.uploadEstimationInvoiceCell)
          tableView.register(nib: Constants.NIBNames.WHCService.serviceCenterCall, forCellReuseIdentifier:  Constants.NIBNames.WHCService.serviceCenterCall)
   
    }
    
    func initialiseData(){
        let model = InspectionDetailViewModel()
        model.heading = Strings.ServiceRequest.SelfRepair.firstHeader
        model.detailState = .completed
        let model2 = InspectionDetailViewModel()
        model2.heading = Strings.ServiceRequest.SelfRepair.secondHeader
        model2.detailState = .notStarted
        let model3 = InspectionDetailViewModel()
        model3.heading = Strings.ServiceRequest.SelfRepair.thirdHeader
        model3.detailState = .notStarted
        let model4 = InspectionDetailViewModel()
        model4.heading = Strings.ServiceRequest.SelfRepair.forthHeader
        model4.detailState = .notStarted
        let model5 = InspectionDetailViewModel()
        model5.heading = Strings.ServiceRequest.SelfRepair.fifthHeader
        model5.detailState = .notStarted
        let model6 = InspectionDetailViewModel()
        model6.heading = Strings.ServiceRequest.SelfRepair.sixthHeader
        model6.detailState = .notStarted
        inspectionDetails = [model, model2, model3, model4,model5,model6]
    }
    // MARK:- Action Methods
    
}

// MARK:- Display Logic Conformance
extension SelfRepairVC: SelfRepairDisplayLogic {
    
}

// MARK:- Configuration Logic
extension SelfRepairVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = SelfRepairInteractor()
        let presenter = SelfRepairPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension SelfRepairVC {
    
}
extension SelfRepairVC{
    func getUploadEstimationCell(for indexPath: IndexPath) -> UploadEstimationInVoiceCell{
        let cell = (tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.uploadEstimationInvoiceCell , for: indexPath)) as? UploadEstimationInVoiceCell
      
        //cell?.delegate = self
        
        //cell?.setViewModel(serviceDetail!)
        return cell!
    }

    func getInvoiceCell(for indexPath: IndexPath) -> FinalInvoiceVerification{
        let cell = (tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.finalInvoiceVerification , for: indexPath)) as? FinalInvoiceVerification
//        cell?.delegate = self
//        cell?.setViewModel(serviceDetail!)
        return cell!
    }

    func getSerViceCenterCell(for indexPath: IndexPath) -> ServiceCenterCell{
        let cell = (tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.serviceCenterCall , for: indexPath)) as? ServiceCenterCell
//        cell?.delegate = self
//        cell?.setViewModel(serviceDetail!)
        return cell!

    }
}
extension SelfRepairVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return shouldExpend.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
           return  isServiceCellExpanded == true ? 1: 0
        }else{
          return  selectedSection == section ? 1 : 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return getSerViceCenterCell(for: indexPath)
        case 1:
            uploadType = .UploadEstimationInVoice
            return getUploadEstimationCell(for: indexPath)
        case 2:
            verificationType = .estimationInvoiceVerification
            return getInvoiceCell(for:indexPath)
        case 3:
            uploadType = .UploadFinalInVoice
            return getUploadEstimationCell(for: indexPath)
        case 4:
            verificationType = .finalInvoiceVerification
            return getInvoiceCell(for: indexPath)
        case 5:
            verificationType = .Refund
            return getInvoiceCell(for: indexPath)
        default:
            break
        }
        return UITableViewCell()
    }
}
extension SelfRepairVC:UITableViewDelegate{
    
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            
            if section == 0{
                 let view = Bundle.main.loadNibNamed(Constants.NIBNames.WHCService.serviceCenterView, owner: self, options: nil)?[0] as? ServiceCenterView
                let model = ServiceCenterModel()
                view?.setModel(model)
                view?.toggleButtonState(isServiceCellExpanded)
                view?.delegate = self
                return view
            }
            
            let view = Bundle.main.loadNibNamed(Constants.NIBNames.WHCService.selfRepairHeaderView, owner: self, options: nil)?[0] as? SelfRepairHeaderView
            if section == 1 {
                view?.topMarginView.isHidden = true
            }
            else if section == shouldExpend.count - 1 {
                view?.bottomMarginView.isHidden = true
            }
            let model = inspectionDetails[section] as? InspectionDetailViewModel
            view?.setModel(model!, forSection: section)
            //view?.toggleButtonState(selectedSection == section)
            //view?.delegate = self
            return view
        }
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 79
        }
}
extension SelfRepairVC:ServiceCenterDelegate{
    func collapseClicked(expanded: Bool){
        isServiceCellExpanded = expanded
    }
}
