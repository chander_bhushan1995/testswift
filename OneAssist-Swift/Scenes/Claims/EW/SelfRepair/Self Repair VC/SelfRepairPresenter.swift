//
//  SelfRepairPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/01/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol SelfRepairDisplayLogic: class {
    
}

class SelfRepairPresenter: BasePresenter, SelfRepairPresentationLogic {
    weak var viewController: SelfRepairDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
}
