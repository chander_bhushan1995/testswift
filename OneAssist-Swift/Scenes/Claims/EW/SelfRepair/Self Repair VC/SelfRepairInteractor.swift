//
//  SelfRepairInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/01/18.
//  Copyright (c) 2018 Mukesh. All rights reserved.
//

import UIKit

protocol SelfRepairPresentationLogic {
    
}

class SelfRepairInteractor: BaseInteractor, SelfRepairBusinessLogic {
    var presenter: SelfRepairPresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
