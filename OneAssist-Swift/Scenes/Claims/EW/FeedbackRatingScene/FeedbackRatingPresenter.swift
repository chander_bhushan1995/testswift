//
//  FeedbackRatingPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/15/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol FeedbackRatingDisplayLogic: class {
    func displayFeedbackSubmitted()
    func displayError(error: String)
}

class FeedbackRatingPresenter: BasePresenter, FeedbackRatingPresentationLogic {
    weak var viewController: FeedbackRatingDisplayLogic?
    
    func feedbackSubmitted(response: SubmitFeedbackResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayFeedbackSubmitted()
        } catch {
            viewController?.displayError(error: error.localizedDescription)
        }
    }
}
