//
//  FeedbackRatingInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/15/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol FeedbackRatingPresentationLogic {
    func feedbackSubmitted(response: SubmitFeedbackResponseDTO?, error: Error?)
}

class FeedbackRatingInteractor: FeedbackRatingBusinessLogic {
    var presenter: FeedbackRatingPresentationLogic?
    
    func submitFeedback(for serviceRequest: String, rating: Int, with reasons: [String]?) {
        
        let feedbackRequest = ServiceRequestFeedback(dictionary: [:])
        
        feedbackRequest.feedbackCode =   reasons?.joined(separator: ",")
        feedbackRequest.feedbackRating = rating.description
        let request = SubmitFeedbackRequestDTO()
        request.serviceReqId = serviceRequest
        // FIXME: remove default value
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "suren"
        request.serviceRequestFeedback = feedbackRequest
        
        let _ = WHCSubmitFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.feedbackSubmitted(response: response, error: error)
        }
    }
}
