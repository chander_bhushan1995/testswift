//
//  FeedbackRatingVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol FeedbackRatingBusinessLogic {
    func submitFeedback(for serviceRequest: String, rating: Int, with reasons: [String]?)
}

class FeedbackRatingVC: BaseVC {

    @IBOutlet weak var topHeaderView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var serviceIdLabel: UILabel!
    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var ratingHeaderLabel: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var bottomContainerView: UIView!
    
    @IBOutlet var storeRatingView: UIView!
    @IBOutlet var ratingSubmitView: UIView!
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ratingStatusLabel: UILabel!
    var serviceRequestNumber: String? = "1399"
    var selectedRating: Int = 5
    var serviceID : String? = ""
    var feedbackVC: FeedbackTableViewVC!
    var productImageUrl:String?
    var productName:String?
    var headerText: String?
    var isMultipleAssests = false
    var srType: Constants.Services?
    private var location: String?
    fileprivate var interactor: FeedbackRatingBusinessLogic?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        
    }
    
    private func initializeView() {
        completedView.layer.cornerRadius = 2.0
        completedView.layer.borderWidth = 1.0
        completedView.layer.borderColor = UIColor.forestGreen.cgColor
        
        self.serviceIdLabel.text = "SERVICE ID \(serviceRequestNumber ?? "")"
        let url = URL(string: productImageUrl ?? "")
        //TODO: REMOVE THIS LINE
        if isMultipleAssests == false{
            headerImageView.kf.setImage(with: url)
        }else{
            headerImageView.image = #imageLiteral(resourceName: "homeServ")
        }
        productNameLabel.text = productName ?? ""
        if srType == .extendedWarranty {
            location = "Home EW Service"
        } else if srType?.getCategoryType() == Constants.Category.homeAppliances {
            location = "HomeAssist Service"
        } else {
            location = "Mobile Service"
        }
        initializeRatingView()
    }
    
    private func initializeFeedbackTableView() {
        self.ratingStatusLabel.text = "You rated \(selectedRating) out of 5. You can still change the rating if you like us!".localized()
        if feedbackVC != nil {
            feedbackVC.refreshFeedbacks()
            return
        }
        
        topHeaderView.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.headerViewBottomConstraint.constant = 35
            self.headerViewHeightConstraint.constant = 0
            
            self.feedbackVC = FeedbackTableViewVC()
            self.feedbackVC.delegate = self
            self.feedbackVC.isCloseButtonHidden = true
            self.feedbackVC.rating = self.selectedRating
            self.feedbackVC.claimType = self.srType
            self.addChild(self.feedbackVC)
            self.feedbackVC.didMove(toParent: self)
            self.addBottomView(self.feedbackVC.view)
            
            self.view.layoutIfNeeded()
        })
    }
    
    private func initializeRatingView() {
        for i in 1...5 {
            let button = ratingView.viewWithTag(i) as? UIButton
            button?.setImage(#imageLiteral(resourceName: "starGreen"), for: .selected)
            button?.setImage(#imageLiteral(resourceName: "starGreen"), for: [.highlighted, .selected])
        }
        ratingChanged()
    }
    
    fileprivate func addBottomView(_ view: UIView) {
        for view in bottomContainerView.subviews {
            view.removeFromSuperview()
        }
        
        bottomContainerView.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let leading = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: view.superview, attribute: .leading, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: view.superview, attribute: .top, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: view.superview, attribute: .trailing, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: view.superview, attribute: .bottom, multiplier: 1, constant: 0)
        
        bottomContainerView.addConstraints([leading, trailing, top, bottom])
        bottomContainerView.layoutIfNeeded()
    }
    
    private func ratingChanged() {
        for i in 1...selectedRating {
            let button = ratingView.viewWithTag(i) as? UIButton
            button?.isSelected = true
        }
        
        if(selectedRating+1 <= 5){
            for i in selectedRating+1...5 {
                let button = ratingView.viewWithTag(i) as? UIButton
                button?.isSelected = false
            }
        }
        
        if selectedRating > 3 {
            ratingStatusLabel.text = headerText
            if ratingSubmitView.superview == nil {
                topHeaderView.isHidden = false
                feedbackVC = nil
                UIView.animate(withDuration: 0.3, animations: {
                    self.headerViewHeightConstraint.constant = 224
                    self.headerViewBottomConstraint.constant = 0
                    
                    self.addBottomView(self.ratingSubmitView)
                    
                    self.view.layoutIfNeeded()
                })
            }
        } else {
            ratingStatusLabel.text = "You rated \(selectedRating) out of 5. You can still change the rating if you like us!"
            initializeFeedbackTableView()
        }
    }
    
    fileprivate func submitRatings(with reasons: [String]? = nil) {
        Utilities.addLoader(onView: view, message: "Submitting your rating", count: &loaderCount, isInteractionEnabled: false)
        interactor?.submitFeedback(for: serviceID ?? "", rating: selectedRating, with: reasons)
    }
    
    @IBAction func clickedBtnRating(_ sender: Any) {
        selectedRating = (sender as? UIButton)?.tag ?? 1
        ratingChanged()
    }
    
    @IBAction func clickedBtnSubmit(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .ratingSubmitted, [.location: "\(location ?? "") Fullscreen", .starRating: selectedRating.description])
        submitRatings()
    }
    
    @IBAction func clickedBtnRate(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .rateNow, [.location: "\(location ?? "") Fullscreen"])
        dismiss(animated: true) {
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            Utilities.openReview()
        }
    }
    
    @IBAction func clickedBtnNo(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .notNow, [.location: "\(location ?? "") Fullscreen"])
        dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        })
    }
}

// MARK:- Configuration Logic
extension FeedbackRatingVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = FeedbackRatingInteractor()
        let presenter = FeedbackRatingPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension FeedbackRatingVC: FeedbackRatingDisplayLogic {
    func displayFeedbackSubmitted() {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        ratingView.isUserInteractionEnabled = false
        if selectedRating < 4 {
            dismiss(animated: true, completion: {
                Utilities.topMostPresenterViewController.view.makeToast(Strings.ToastMessage.feedbak)
                NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            })
        } else {
            self.addBottomView(self.storeRatingView)
            self.view.layoutIfNeeded()
        }
    }
    
    func displayError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

extension FeedbackRatingVC: FeedbackTableViewVCDelegate {
    func clickedSubmitRating(_ reasons: [String]) {
        submitRatings(with: reasons)
    }
}
