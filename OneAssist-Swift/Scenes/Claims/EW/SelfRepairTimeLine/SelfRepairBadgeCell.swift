//
//  SelfRepairBadgeCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

struct SelfRepairBadgeCellModel: AlwaysVisibleCell {
    
}

class SelfRepairBadgeCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 2.0
        containerView.layer.borderColor = UIColor.gray2.cgColor
        containerView.layer.borderWidth = 1.0
    }
}
