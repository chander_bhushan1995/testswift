//
//  DocumentVerificationTimelineCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/15/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class DocumentVerificationTimelineCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var activityIndicatorObj: UIActivityIndicatorView!
    var spinnerView: SpinnerView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        containerView.layer.borderColor = UIColor.gray2.cgColor
        containerView.layer.cornerRadius = 6.0
        containerView.layer.borderWidth = 1.0
        
        docImageView.layer.borderColor = UIColor.gray2.cgColor
        docImageView.layer.cornerRadius = 6.0
        docImageView.layer.borderWidth = 1.0
        
        docImageView.clipsToBounds = true
        
        // add custom loader
        activityIndicatorObj.stopAnimating()
        spinnerView = SpinnerView(view: containerView)
        spinnerView?.startAnimating()
    }
    
    func updateUserInterface(image: String?, title: String) {
        
        headerLabel.text = title
        
        let request = GetClaimDocumentRequestDTO(documentId: nil, storageRefIds: image, claimDocType: .thumbnail)
        GetClaimDocumentRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
//            self.activityIndicatorObj.stopAnimating()
            self.spinnerView?.stopAnimating()
            self.docImageView.image = response?.image
        }
        
        layoutIfNeeded()
    }
    
    
}
