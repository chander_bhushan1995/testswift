//
//  DocumentVerificationDetailsCellTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/2/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Document verification model
//struct DocumentVerificationDetailsCellViewModel {
//    var headerText: String?
//    var images: [String]?
//    var descriptionHeaderText: String?
//    var descriptionText: String?
//    var isButtonEnabled : Bool = true
//}

/// This Delegate contains callback method of Document verification cell buttons.
protocol DocumentVerificationDetailsCellDelegate: class {
    
    /// Give callback on click of View Details button
    ///
    /// - Parameters:
    ///   - cell: Current cell object
    ///   - sender: Button which clicked
    func documentVerificationDetailsCell(_ cell: DocumentVerificationDetailsCell, clickedBtnViewDetail sender: Any,indexPath:IndexPath)
}

/// Used to display Document verification Pending Cell
class DocumentVerificationDetailsCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet var labelHeaderAlignTopConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var containerView: DLSShadowView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var descriptionHeaderLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var descriptionLabrl: BodyTextRegularBlackLabel!
    @IBOutlet weak var tickerView: UIView!
    @IBOutlet weak var tickerLabel: UILabel!
    @IBOutlet weak var btnViewInDetail: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var labelAction: SupportingTextBoldBlackLabel!
    @IBOutlet weak var imgArrow: UIImageView!
    

    var indexPath:IndexPath!

    var spinnerView: SpinnerView?
    weak var delegate: DocumentVerificationDetailsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    /// Initialize cell view
    private func initializeView() {
//        containerView.layer.cornerRadius = 6.0
//        containerView.layer.borderColor = UIColor.dlsBorder.cgColor

        imageContainerView.layer.cornerRadius = 2.0
        imageContainerView.layer.borderColor = UIColor.dlsBorder.cgColor
        imgArrow.image = imgArrow.image!.withRenderingMode(.alwaysTemplate)

//        tickerView.layer.cornerRadius = 10.0
//        activityIndicator.startAnimating()
        
        // add custom loader
        activityIndicator.stopAnimating()
        spinnerView = SpinnerView(view: imageContainerView)
        spinnerView?.startAnimating()
    }
    
    /// Set view for Document Verification pending
    ///
    /// - Parameter model: Document verification model
//    func setViewModel(_ model: DocumentVerificationDetailsCellViewModel) {
//        headerLabel.text = model.headerText
//        btnViewInDetail.isEnabled = model.isButtonEnabled
//        labelAction.textColor = model.isButtonEnabled == true ? UIColor.dodgerBlue : UIColor.bodyTextGray
//        imgArrow.tintColor = model.isButtonEnabled == true ? UIColor.dodgerBlue : UIColor.bodyTextGray
//
//        /// Get first image of uploaded document
//        let request = GetClaimDocumentRequestDTO(documentId: model.images?.first, claimDocType: .thumbnail)
//        GetClaimDocumentRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
////            self.activityIndicator.stopAnimating()
//            self.spinnerView?.stopAnimating()
//            self.previewImageView.image = response?.image
//        }
//        
//        descriptionHeaderLabel.text = model.descriptionHeaderText
//        descriptionLabrl.text = model.descriptionText
//        
//        if let count = model.images?.count {
//            tickerView.isHidden = false
//            tickerLabel.text = count.description
//        } else {
//        }
//    }
//    
    /// Update cell view
    ///
    /// - Parameters:
    ///   - title: Heading Text
    ///   - image: Image name
    ///   - badgeCount: Badge count
    ///   - description: Stage description
    ///   - descriptionTitle: Title description
    ///   - buttonEnabled: if button enable or not
//    func updateUserInterface(title: String, image: String?, badgeCount: Int, description:String? , descriptionTitle:String?,buttonEnabled:Bool? = true,buttonTitle:String? = "",indexPath:IndexPath) {

    func setViewModel(_ model:TimeLineViewModel.DocumentVerificationDetailsCellModel, _ delegate: DocumentVerificationDetailsCellDelegate?){
        
        self.indexPath = model.indexPath
        self.delegate = delegate
        
        self.descriptionHeaderLabel.text = model.descriptionTitle
        btnViewInDetail.isEnabled = model.buttonEnabled ?? false
        labelAction.text = model.buttonTitle ?? ""
        labelAction.textColor = model.buttonEnabled == true ? UIColor.dodgerBlue : UIColor.bodyTextGray
        imgArrow.tintColor = model.buttonEnabled == true ? UIColor.dodgerBlue: UIColor.bodyTextGray

        
        self.descriptionLabrl.text = model.description

        tickerView.isHidden = true

        if model.image == nil {
            imageViewConstraint.constant = 0
            imageContainerView.backgroundColor = .clear
//            self.activityIndicator.stopAnimating()
            spinnerView?.stopAnimating()
        } else {
            if model.badgeCount > 1 {
                tickerView.isHidden = false
                tickerLabel.text = model.badgeCount.description
            }
            let request = GetClaimDocumentRequestDTO(documentId: nil,storageRefIds:model.image, claimDocType: .thumbnail)
            GetClaimDocumentRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
//                self.activityIndicator.stopAnimating()
                self.spinnerView?.stopAnimating()
                self.previewImageView.image = response?.image
            }
        }
        
        headerLabel.text = model.title
        
        layoutIfNeeded()
    }
    
    /// Clicked on button View Details
    @IBAction func clickedBtnViewDetail(_ sender: Any) {
        delegate?.documentVerificationDetailsCell(self, clickedBtnViewDetail: sender,indexPath:self.indexPath)
        if let srType = CacheManager.shared.srType, let subCat = CacheManager.shared.eventProductName {
            EventTracking.shared.eventTracking(name: .SRViewDetails, [.service: srType.rawValue, .subcategory: subCat])

        }
    }
}
