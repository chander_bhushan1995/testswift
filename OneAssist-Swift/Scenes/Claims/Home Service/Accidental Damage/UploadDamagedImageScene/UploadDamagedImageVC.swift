//
//  UploadDamagedImageVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/1/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
/**
 this class is used for ui stuff including collection view and its loader.
 
 */
class UploadDamagedImageVC: DocumentTypeBaseVC {

    @IBOutlet weak var srIdView: UIView!
    @IBOutlet weak var srIdLabel: UILabel!
    @IBOutlet weak var imgViewTick: UIImageView!
    @IBOutlet weak var viewTick: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonHowHappen: OAPrimaryButton!
    
    @IBOutlet weak var headerLabel: H3BoldLabel!
    @IBOutlet weak var dotLabel: SupportingTextRegularGreyLabel!
    
    var applianceName: String! = "Refrigerator"
    
   var headerText: NSAttributedString {
        //        return "Please upload 2 images of your Refrigerator for verification purposes"
          return NSAttributedString(string: String(format: Strings.ServiceRequest.UploadDamageImage.header, dataSource.count, applianceName), attributes: [NSAttributedString.Key.font: DLSFont.h3.regular])
    }
    
    var primaryActionText: String {
        return "Submit"
    }
    /**
     datasource for the collectionView for displayinh images.
     */
    var dataSource = [UploadDamagedImageModel]()
    /**
     imageSelectedCallBack is called when user select the any image from gallery or camera.
     submitClosure is the closure called on tapping the submit button
     */
    var imageSelectedCallBack: ((UIImage?, IndexPath) -> (Void))?
    var submitClosure: ((UIButton) -> (Void))?
    var heightOfCells: CGSize = CGSize(width: UIScreen.main.bounds.width - 32, height: 120)
    var isFIRReport : Bool {
        return serviceRequestType == .buglary
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        initializeValues()
        srIdView.backgroundColor = UIColor.seaGreen.withAlphaComponent(0.2)
        srIdLabel.textColor = UIColor.seaGreen
        srIdLabel.font = DLSFont.supportingText.medium
        
        buttonHowHappen.isEnabled = false
        headerLabel.attributedText = headerText
        buttonHowHappen.setTitle(primaryActionText, for: .normal)
        
        collectionView.register(nib: Constants.CellIdentifiers.WHCService.uploadImageCollectionViewCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.uploadImageCollectionViewCell)
        
        viewTick.layer.cornerRadius = viewTick.bounds.width / 2
        imgViewTick.image = #imageLiteral(resourceName: "tick").withRenderingMode(.alwaysTemplate)
        imgViewTick.tintColor = UIColor.white
        
        var dotTexts: [String] = []
        if isFIRReport {
            dotTexts.append("Image should have FIR Complaint number and the police station's address")
        }
      //  dotTexts.append("Please make sure image is not blurry")
        dotLabel.attributedText = self.getAttributedText(dotTexts)
        
        toggleSubmitButton()
    }
    
    private func initializeValues() {
//        dataSource.append(UploadDamagedImageModel())
//        dataSource.append(UploadDamagedImageModel())
    }
    
    func addLoaderOnCollection(path: IndexPath) {
        let model = dataSource[path.row]
        model.isUploading = true
        collectionView.reloadItems(at: [path])
    }
    
    func removeLoaderFromCollection(path: IndexPath) {
        let model = dataSource[path.row]
        model.isUploading = false
        collectionView.reloadItems(at: [path])
    }

    @IBAction func clickedBtnHowHappen(_ sender: Any) {
        submitClosure?(sender as! UIButton)
//        EventTracking.shared.eventTracking(name: .SRUploadDoc)
    }
}

extension UploadDamagedImageVC: UploadImageCollectionViewCellDelegate {
    func uploadImageCollectionViewCell(_ cell: UploadImageCollectionViewCell, clickedBtnEdit sender: Any) {
        if let indexPath = collectionView.indexPath(for: cell) {
            handleImageUpload(indexPath)
        }
    }
    
    func uploadImageCollectionViewCell(_ cell: UploadImageCollectionViewCell, clickedBtnUpload sender: Any) {
        if let indexPath = collectionView.indexPath(for: cell) {
            handleImageUpload(indexPath)
        }
    }
    
    
    /**
     This will check the whehter all the doucments are the submited or not.It will be checked with dataSource
     */
    
    func toggleSubmitButton() {
        for model in dataSource {
            if !model.uploaded && model.isMandatory {
                buttonHowHappen.isEnabled = false
                return
            }
        }
        buttonHowHappen.isEnabled = true
    }
    
    private func handleImageUpload(_ indexPath: IndexPath) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: Strings.Global.uploadImage, style: .default) { (action) in
            ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(for: indexPath, image, error)
            })
        }
        
        let action2 = UIAlertAction(title: Strings.Global.clickPhoto, style: .default) { (action2) in
            ImagePickerManager.shared.pickImageFromCamera(editing: false, handler: {[weak self] (image, error) in
                self?.handlePickedImage(for: indexPath, image, error)
            })
        }
        
        let action3 = UIAlertAction(title: Strings.Global.cancel, style: .cancel) { (action3) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action)
        alertController.addAction(action2)
        alertController.addAction(action3)
        presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    /**
     function is called when image is selected.
     */
    
    private func handlePickedImage(for indexPath: IndexPath, _ image: UIImage?, _ error: Error?) {
        if let error = error {
            // Handle error
            print(error)
        } else {
            dataSource[indexPath.row].image = image
            collectionView.reloadItems(at: [indexPath])
            imageSelectedCallBack?(image, indexPath)
        }
    }
    
    
    private func getAttributedText(_ model: [String]) ->NSMutableAttributedString {
        let attributesDictionary = [NSAttributedString.Key.font : DLSFont.supportingText.medium]
        let fullAttributedString = NSMutableAttributedString(string: "", attributes: attributesDictionary as [NSAttributedString.Key : Any])
        var paragraphStyle: NSMutableParagraphStyle
        paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 15, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 15
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.lineSpacing = 5
        paragraphStyle.headIndent = 15
        
        for points in model {
            let bulletPoint: String = "\u{2022}\t"
            let formattedString: String = "\(bulletPoint) \(points)\n"
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: formattedString, attributes: [.foregroundColor:UIColor.gray, .font: DLSFont.supportingText.medium])
            attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributedString.length))
            fullAttributedString.append(attributedString)
        }
        return fullAttributedString
    }
}

extension UploadDamagedImageVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.WHCService.uploadImageCollectionViewCell, for: indexPath) as! UploadImageCollectionViewCell
        cell.set(viewModel: dataSource[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension UploadDamagedImageVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return heightOfCells
    }
}
