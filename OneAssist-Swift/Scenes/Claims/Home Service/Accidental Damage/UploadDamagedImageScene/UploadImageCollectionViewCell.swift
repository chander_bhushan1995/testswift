//
//  UploadImageCollectionViewCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/1/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol UploadImageCollectionViewCellDelegate: class {
    func uploadImageCollectionViewCell(_ cell: UploadImageCollectionViewCell, clickedBtnEdit sender: Any)
    func uploadImageCollectionViewCell(_ cell: UploadImageCollectionViewCell, clickedBtnUpload sender: Any)
}

class UploadImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var imageContainetView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var unUploadedView: UIView!
    @IBOutlet weak var uploadedView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var uploadedImageView: UIImageView!
    
    @IBOutlet weak var editButton: UIButton!
    
    weak var delegate: UploadImageCollectionViewCellDelegate?
    var spinnerView: SpinnerView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }

    private func initializeView() {
        imageContainetView.layer.cornerRadius = 4.0
        imageContainetView.layer.borderColor = UIColor.buttonTitleBlue.cgColor
        imageContainetView.layer.borderWidth = 1.0
        editButton.layer.cornerRadius = 16
        activityIndicator.stopAnimating()
        editButton.isHidden = true;
        spinnerView = SpinnerView(view: imageContainetView)
        spinnerView?.spinnerView.isUserInteractionEnabled = false
        spinnerView?.startAnimating()
    }
    
    func set(viewModel model: UploadDamagedImageModel) {
        titleLabel.text = model.docName
        detailLabel.isHidden = true
        
        if let detail = model.docDescription {
            detailLabel.isHidden = false
            detailLabel.attributedText = self.getAttributedText([detail])
        }
        
        if let image = model.image {
            unUploadedView.isHidden = true
            uploadedView.isHidden = false
            uploadedImageView.image = image
        } else {
            unUploadedView.isHidden = false
            uploadedView.isHidden = true
        }
        
        if model.isUploading {
            isUserInteractionEnabled = false
            editButton.isHidden = true;
            spinnerView?.startAnimating()
        } else {
            isUserInteractionEnabled = true
            editButton.isHidden = false;
            spinnerView?.stopAnimating()
        }
    }
    
    @IBAction func clickedBtnEdit(_ sender: Any) {
        delegate?.uploadImageCollectionViewCell(self, clickedBtnEdit: sender)
    }
    
    @IBAction func clickedBtnUpload(_ sender: Any) {
        delegate?.uploadImageCollectionViewCell(self, clickedBtnUpload: sender)
    }
    
    private func getAttributedText(_ model: [String]) ->NSMutableAttributedString {
        let attributesDictionary = [NSAttributedString.Key.font : DLSFont.supportingText.regular]
        let fullAttributedString = NSMutableAttributedString(string: "", attributes: attributesDictionary as [NSAttributedString.Key : Any])
        var paragraphStyle: NSMutableParagraphStyle
        paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 10, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 10
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.headIndent = 10
        
        for points in model {
            let bulletPoint: String = "\u{2022}\t"
            let formattedString: String = "\(bulletPoint) \(points)\n"
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: formattedString, attributes: [.foregroundColor:UIColor.bodyTextGray, .font: DLSFont.supportingText.regular])
            attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributedString.length))
            fullAttributedString.append(attributedString)
        }
        return fullAttributedString
    }
}
