//
//  UploadDamagedImageModel.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/1/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class UploadDamagedImageModel {
    var image: UIImage?
    var loadingCount = 0
    var docId: String?
    var docType: String?
    var uploaded = false
    var isUploading = false
    var docName: String?
    var docDescription: String?
    var isMandatory = true
}
