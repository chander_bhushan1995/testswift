//
//  UploadedDamagedImageViewScene.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/5/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class UploadedDamagedImageViewVC: DocumentImageViewBaseVC, DocumentImageViewDataSource {
    var srDetails: SearchService?
    var section: [TableViewSection<Row>]!
    
    convenience init() {
        self.init(nibName: "DocumentImageViewBaseVC", bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        dataSource = self
        headerText = "Images of your Documents"
        section = getSections()
        
        super.viewDidLoad()
    }
    
    var cellHeight: CGFloat {
        return 180
    }
    
    var itemWidth: CGFloat {
        return (UIScreen.main.bounds.width - 65)/2
    }
    
    var images: [String] {
        return srDetails?.serviceDocuments?.compactMap { $0.documentId } ?? []
    }
    
    private func getSections() -> [TableViewSection<Row>] {
        let section = TableViewSection<Row>()
        section.title = "DESCRIPTION GIVEN"
        var rows: [Row] = []
        if let incidentDetail = srDetails?.incidentDetail, let incidentDesc = incidentDetail.incidentDescription, incidentDesc.count > 0 {
            
            for incident in incidentDesc {
                let title = NSMutableAttributedString(string: incident.displayName ?? "", attributes:
                                                                                            [.foregroundColor: UIColor.charcoalGrey,
                                                                                            NSAttributedString.Key.font: DLSFont.h3.regular])
                
                title.append(NSAttributedString(string: "\n"))
                let value = NSAttributedString(string: incident.value ?? "", attributes: [.foregroundColor: UIColor.testimonialCellDark,
                                                                                          NSAttributedString.Key.font: DLSFont.h3.regular])
                title.append(value)
                let row = Row()
                row.atributedTitle = title
                rows.append(row)
            }
            
        }else {
            let row = Row()
            row.title = srDetails?.requestDescription
            rows = [row]
        }
        
        
        
        section.rows = rows
        
        return [section]
    }
}
