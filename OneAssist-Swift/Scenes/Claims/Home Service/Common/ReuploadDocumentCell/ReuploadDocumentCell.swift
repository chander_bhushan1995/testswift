//
//  ReuploadDocumentCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 16/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol ReuploadDocumentCellDelegate: class {
    func clickedBtnReupload(indexPath:IndexPath)
}

struct ReuploadDocumentCellViewModel {
    let title: String?
    let actionStr: String
    let reuploadDesc: String?
    let reasons: [(title: String, desc: String)]?
    var isButtonEnabled : Bool = true

}

class ReuploadDocumentCell: UITableViewCell,ReuseIdentifier,NibLoadableView {

    @IBOutlet weak var viewContainer: DLSShadowView!
    @IBOutlet weak var lblTitle: BodyTextBoldBlackLabel!
    @IBOutlet weak var lblReasons: H3RegularBlackLabel!
    @IBOutlet weak var lblReupload: BodyTextRegularBlackLabel!
    @IBOutlet weak var labelAction: H3RegularBlackLabel!
    @IBOutlet weak var imgArrow: UIImageView!

    @IBOutlet weak var topDescriptionLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var btnReupload : UIButton!
    weak var delegate: ReuploadDocumentCellDelegate?
    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        intializeView()
    }
    
    func intializeView() {
//        viewContainer.layer.borderColor = UIColor.dlsBorder.cgColor
//        viewContainer.layer.cornerRadius = 6.0
//        viewContainer.layer.borderWidth = 1.0
        imgArrow.image = imgArrow.image!.withRenderingMode(.alwaysTemplate)
        lblReupload.textColor = .saffronYellow

    }
    
    func setViewModel(_ model:TimeLineViewModel.ReuploadDocumentCellModel, _ delegate: ReuploadDocumentCellDelegate?){
        
        self.indexPath = model.indexPath
        self.delegate = delegate
        lblTitle.text = model.title
//        lblReupload.text = reuploadDesc
        labelAction.text = model.actionStr
        btnReupload.isEnabled = model.buttonEnabled ?? false
        labelAction.textColor = model.buttonEnabled == true ? .dodgerBlue : .bodyTextGray
        imgArrow.tintColor = model.buttonEnabled == true ? .dodgerBlue : .bodyTextGray
        
        if model.title == nil {
            lblTitle.setZero(for: [.bottom])
        }
        if model.topTitle != nil{
            topDescriptionLabel.text = model.topTitle
        }else{
            topDescriptionLabel.isHidden = true
        }
        if let reuploadDesc = model.reuploadDesc, reuploadDesc != "" {
//            lblReupload.setZero(for: [.bottom])
            lblReupload.text = reuploadDesc
        }
        
        if let reasons = model.reasons{
            setReasonLabel(reasons: reasons)
        } else {
            lblReasons.text = nil
            lblReasons.setZero(for: [.bottom, .height])
        }
        
    }
    
    private func setReasonLabel(reasons: [(title: String, desc: String)]) {
        
        let fullString = NSMutableAttributedString()
        
        let font: UIFont = DLSFont.supportingText.regular
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        paragraphStyle.paragraphSpacing = 0.25 * font.lineHeight
        let attributes = [NSAttributedString.Key.paragraphStyle:paragraphStyle,NSAttributedString.Key.baselineOffset: NSNumber(value: 0)]
        
        for reason in reasons.enumerated() {
            
            let font: UIFont = DLSFont.supportingText.regular
            let title = NSAttributedString(string: "- \(reason.element.title)\n", attributes: [NSAttributedString.Key.font: font])
            fullString.append(title)
            
            let desc = NSAttributedString(string: "    \(reason.element.desc)", attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.bodyTextGray])
            fullString.append(desc)
            
            if reason.offset != reasons.count - 1 {
                fullString.append(NSAttributedString(string:"\n\n"))
            }
        }
     
        var range = NSRange()
        range.location = 0
        range.length = fullString.length
        fullString.addAttributes(attributes, range: range)
        
        lblReasons.attributedText = fullString
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedBtnReupload(_ sender: Any) {
        delegate?.clickedBtnReupload(indexPath: self.indexPath)
    }
}
