//
//  HomeCoverProgressContainerPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/27/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol HomeCoverProgressContainerDisplayLogic: class {
    func displayError(_ error: String)
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?)
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?)

}

class HomeCoverProgressContainerPresenter: BasePresenter, HomeCoverProgressContainerPresentationLogic {
    weak var viewController: HomeCoverProgressContainerDisplayLogic?
    
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayCreatedServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayUpdateServiceRequest(response)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
}
