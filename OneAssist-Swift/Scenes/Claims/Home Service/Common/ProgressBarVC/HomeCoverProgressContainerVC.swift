//
//  HomeCoverProgressContainerVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol HomeCoverProgressContainerBusinessLogic {
    func createServiceRequest(for type: ClaimType)
    func updateServiceRequest(for type: ClaimType)
}

class HomeCoverProgressContainerVC: BaseVC {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var pagingView: UIView!
    
    var animating : Bool = false
    var pageViewController: UIPageViewController!
    var currentIndex:Int = 0
    
    var raisedFromCrm = false
    var interactor: HomeCoverProgressContainerBusinessLogic?
    var dictCategory : [aEnumClaimWholeHomeCover:[AnyClass]]!
    
    var arrViewController :[UIViewController] = []
    
    var currentCategory:aEnumClaimWholeHomeCover!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    var totalVCInCategories:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
    }
    
    func initialiseView(){
        initializePagingController()
        self.lblProgress.font = DLSFont.supportingText.regular
        self.dictCategory = self.initialiseCategoryDictionaryForViewController()
        currentIndex = 0
        arrViewController = getStartingViewController()
        pageViewController.setViewControllers([arrViewController[0]], direction: .forward, animated: false, completion: nil)
        self.setProgress(index: 0)
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style:.done , target: self, action: #selector(self.clickBackButton))
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = Strings.ServiceRequest.Titles.raiseARequest
    }
    
    func initializePagingController() {
        currentIndex = 0
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.frame = pagingView.bounds
        pageViewController.delegate = self
        addChild(pageViewController)
        pagingView.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        
    }
    
    func setProgress(index : Int , totalCount : Int?)  {
        if index  == 0 {
            self.progressView.setProgress(0.15, animated: true)
        }else {
            self.progressView.setProgress(Float(Double(index+1))/Float(totalCount! + 2), animated: true)

        }
        
        self.lblProgress.text = getProgressLabelText(index : index, totalCount: totalCount)
    }
    
    @objc func clickBackButton(){
        if(currentIndex == 0){
            self.navigationController?.popViewController(animated: true)
        }else{
            currentIndex = currentIndex - 1
            setProgress(index : currentIndex)
            let nextVC = arrViewController[currentIndex]
            self.pageViewController.setViewControllers([nextVC], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func moveToNext(controllerToLoad:((_ controller : UIViewController)->Void)?, onCompletion:((_ controller : UIViewController)->Void)? = nil){
        
        if animating {
            return
        }
        animating = true
        let nextIndexToLoad = self.currentIndex + 1
        if nextIndexToLoad >= arrViewController.count {
            guard let classToLoad =   getClass(category: currentCategory, dictCategory: self.dictCategory, index: nextIndexToLoad)else {
                return
            }
            let nextVC = getViewController(forClass: classToLoad , category : currentCategory)!
            if let closure = controllerToLoad {
                closure(nextVC)
            }
            setProgress(index: nextIndexToLoad)
           
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true){ _ in
                self.animating = false
                onCompletion?(nextVC)
            }
            arrViewController.append(nextVC)
        } else {
            let nextVC = arrViewController[nextIndexToLoad]
            if let closure = controllerToLoad {
                closure(nextVC)
            }
            setProgress(index: nextIndexToLoad)
            self.pageViewController.setViewControllers([nextVC], direction: .forward, animated: true){ _ in
                self.animating = false
                onCompletion?(nextVC)

            }
        }
        self.currentIndex = nextIndexToLoad
    }
    
    func getProgressLabelText(index : Int , totalCount : Int?) ->String
    {
        if index == 0 {
            return "Step 1"
        }
        if index == totalCount!-1 && totalCount! >= 4 {
            return "Only 2 steps left"
        }
        if index == totalCount && totalCount! >= 4 {
            
            return "Great! You're on the last step!"
        }
        return "Step " + "\(index + 1) " + "of " + "\(totalCount! + 1)"
    }
    
    func setProgress(index:Int){
        if index == 0 {
            self.setProgress(index: 0, totalCount: nil)
        }else {
            let arrClass = getArrayViewControllersClass(category: currentCategory)
            let totalVC =  arrClass.count
            self.setProgress(index: index, totalCount: totalVC)
        }
    }
    
    func  setProgressLastStep() {
        let arrClass = getArrayViewControllersClass(category: currentCategory)
        let totalVC =  arrClass.count
        self.setProgress(index: totalVC, totalCount: totalVC)
    }
    
    func isInsuranceBacked() -> Bool {
        return !Utilities.isSOPServiceAvailable(from: ClaimHomeServe.shared.totalServices)
    }
    
    fileprivate func getStartingViewController()->[UIViewController] {
        var arrViewController :[UIViewController] = []
        let vc = WhatHappenedVC()
        vc.currentPage = 0
        vc.delegate = self
//        vc.category = aEnumClaimWholeHomeCover.eFire
        arrViewController.append(vc)
        return arrViewController
    }  
    
    func getViewController(forClass :AnyClass , category : aEnumClaimWholeHomeCover) -> UIViewController? {
        var vc : BaseVC?
         if forClass is AppliancesNotWorkingVC.Type {
            let baseVC = (forClass as! AppliancesNotWorkingVC.Type).init()
//            baseVC.category = category
            if  raisedFromCrm {
                baseVC.assestID = ClaimHomeServe.shared.assetIds
            }
            baseVC.membershipId = ClaimHomeServe.shared.memberShipId

            baseVC.delegate = self
            vc = baseVC
        }else if  forClass is BrieflyDescVC.Type{
            let baseVC = BrieflyDescVC()
            baseVC.delegate = self
            if raisedFromCrm {
                baseVC.descriptionText = ClaimHomeServe.shared.incidentDescription
            }
            vc = baseVC
        }else if  forClass is WhenHappenVC.Type {
            let baseVC = (forClass as! WhenHappenVC.Type).init()
            baseVC.currentPage = currentIndex
//            baseVC.category = currentCategory
            baseVC.delegate = self
            if raisedFromCrm {
               baseVC.prefetchedDateSelected = ClaimHomeServe.shared.dateWhenHappen
            }
            vc = baseVC
        } else if  forClass is WhatHappenedVC.Type{
            let baseVC = (forClass as! WhatHappenedVC.Type).init()
//            baseVC.category = category
            baseVC.productCode = ClaimHomeServe.shared.productCode!
            if raisedFromCrm {
                baseVC.preSelectedIssueId = ClaimHomeServe.shared.issueId
            }
            baseVC.delegate = self
            vc = baseVC
         } else if forClass is ScheduleVisitVC.Type {
            let baseVC = ScheduleVisitVC()
            if raisedFromCrm {
                baseVC.selectedSlot = ClaimHomeServe.shared.scheduledTimeslot
                if let date = ClaimHomeServe.shared.scheduledDate {
                    let model = ScheduleVisitCollectionViewModel()
                    model.date = date
                    baseVC.selectedItem = model
                }
            }
            baseVC.serviceType = currentCategory.getServiceCode()
            baseVC.addressDetail = AddressDetail(pincode: ClaimHomeServe.shared.pinCode)
            baseVC.scheduleVisitDelegate = self
            vc = baseVC
        }
        
        return vc
    }
    
    
    func initialiseCategoryDictionaryForViewController()->[aEnumClaimWholeHomeCover:[AnyClass]] {
        var dictCategory = [aEnumClaimWholeHomeCover:[AnyClass]]()
        dictCategory[.eFire] = self.getArrayViewControllersClass(category: .eFire)
        dictCategory[.eAccidentalDamage] = self.getArrayViewControllersClass(category: .eAccidentalDamage)
        dictCategory[.eBreakDown] = self.getArrayViewControllersClass(category: .eBreakDown)
        dictCategory[.ePms] = self.getArrayViewControllersClass(category: .ePms)
        dictCategory[.eStolen] = self.getArrayViewControllersClass(category: .eStolen)
        return dictCategory
    }
    
    func getClass(category : aEnumClaimWholeHomeCover, dictCategory:[aEnumClaimWholeHomeCover:[AnyClass]],index : Int) ->AnyClass? {
        let newIndex = index - 1
        var arrClass = dictCategory[category]!
        if newIndex <= arrClass.count - 1{
            return arrClass[newIndex]
        }
        return nil;
    }
    
    
    func getArrayViewControllersClass(category:aEnumClaimWholeHomeCover)->[AnyClass] {
        var arrClass:[AnyClass] = []
        switch category{
        case .eBreakDown:
            arrClass = isInsuranceBacked() ? [AppliancesNotWorkingVC.self,WhatHappenedVC.self,WhenHappenVC.self,ScheduleVisitVC.self] : [AppliancesNotWorkingVC.self,WhatHappenedVC.self,ScheduleVisitVC.self]
        case .eAccidentalDamage:
            arrClass = [AppliancesNotWorkingVC.self,WhatHappenedVC.self,BrieflyDescVC.self,WhenHappenVC.self]
        case .eFire:
            arrClass = [AppliancesNotWorkingVC.self,BrieflyDescVC.self,WhenHappenVC.self]
        case .eStolen:
            arrClass = [AppliancesNotWorkingVC.self,BrieflyDescVC.self,WhenHappenVC.self]
        case .ePms:
            arrClass = [AppliancesNotWorkingVC.self, ScheduleVisitVC.self]
         }
        return arrClass
    }
}

// MARK:- Configuration Logic
extension HomeCoverProgressContainerVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = HomeCoverProgressContainerInteractor()
        let presenter = HomeCoverProgressContainerPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension HomeCoverProgressContainerVC: UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            return
        }
        
        setProgress(index:currentIndex)
    }
}


extension HomeCoverProgressContainerVC{
    func setUpViewControllerDataModel(_ index:Int){
    }
}

// MARK: IncidentDescriptionBaseVCDelegate

extension HomeCoverProgressContainerVC : IncidentDescriptionBaseVCDelegate {
    func clickTellUsMore(descripition: String, placeOfIncident: String?) {
        ClaimHomeServe.shared.incidentDescription = descripition
        moveToNext(controllerToLoad: nil)
    }
}
// MARK: WhatHappenedVCDelegate

extension HomeCoverProgressContainerVC: WhatHappenedVCDelegate{
    func clickSubmitButton(model: WhatHappenSection, category: Constants.Services) {
        
    }
    
    func whatHappenedButtonTapped(category: Constants.Services) {
        
    }
    
//    func whatHappenedButtonTapped(category: ClaimType) {
//        if let category = category as? aEnumClaimWholeHomeCover{
//            whatHappenedButtonTapped(category: category)
//        }
//    }
//    
//    func clickSubmitButton(model:WhatHappenSection, category: ClaimType){
//        if let _ = model.issueId {
//            ClaimHomeServe.shared.issueId = model.issueId
//            ClaimHomeServe.shared.issueDescription = model.title
//        }
//        if let category = category as? aEnumClaimWholeHomeCover {
//            if category == aEnumClaimWholeHomeCover.eBreakDown || category == aEnumClaimWholeHomeCover.eAccidentalDamage {
//                ClaimHomeServe.shared.incidentDescription = model.subtitle
//            }
//        }
//       
//        moveToNext(controllerToLoad: nil)
//    }
    
    private func whatHappenedButtonTapped(category:aEnumClaimWholeHomeCover) {
        if currentCategory != category {
            if currentCategory != nil {
                ClaimHomeServe.shared.assetIds = nil
            }
            totalVCInCategories = dictCategory[category]!.count
            currentCategory = category
            arrViewController.removeSubrange(ClosedRange(uncheckedBounds: (lower: 1, upper: arrViewController.count - 1)))
        }
    }
}

// MARK: ClaimWhenHappenDelegate
extension HomeCoverProgressContainerVC:ClaimWhenHappenDelegate{
    func clickWhenHapppen(date: String) {
        ClaimHomeServe.shared.dateWhenHappen = date
        
        let newDate = Date(string: date, formatter: .scheduleFormat) ?? Date()
        
        let dateTime = date.split(separator: " ")
        if currentCategory == aEnumClaimWholeHomeCover.eBreakDown {
            if dateTime.count == 2 {

                EventTracking.shared.eventTracking(name: .srDtSubmit, [Constants.Event.EventKeys.date: newDate, Constants.Event.EventKeys.time: dateTime[1], Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? ""])

            }
            moveToNext(controllerToLoad: nil)
        } else {
            if dateTime.count == 2 {

                EventTracking.shared.eventTracking(name: .SRVisitDT_Submit, [Constants.Event.EventKeys.date: newDate, Constants.Event.EventKeys.time: dateTime[1], Constants.Event.EventKeys.subcategory: ClaimHomeServe.shared.productName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? ""])
            }
            raiseServiceRequest(category: self.currentCategory)
        }
    }
    
    func raiseServiceRequest(category: ClaimType) {
        
        self.progressView.setProgress(1.0, animated: true)
        
        if raisedFromCrm {
            Utilities.addLoader(onView: view, message: "Please wait while we are updating your service request", count: &loaderCount, isInteractionEnabled: true)
            interactor?.updateServiceRequest(for: category)
        }else {
            Utilities.addLoader(onView: view, message: "Please wait while we are creating your service request", count: &loaderCount, isInteractionEnabled: true)

            interactor?.createServiceRequest(for: category)

        }
    }
}

extension HomeCoverProgressContainerVC: HomeCoverProgressContainerDisplayLogic {
    func displayError(_ error: String) {
        self.setProgressLastStep()
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error, primaryButtonTitle: "Retry", "Cancel", primaryAction: {[weak self] in
           self?.raiseServiceRequest(category: (self?.currentCategory)!)
        }) {}
    }
    
    func displayUpdateServiceRequest(_ serviceDetails: UpdateServiceRequestResponseDTO?) {
        
        EventTracking.shared.eventTracking(name: .srInProgress, [Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? ""])

        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }

    
    func displayCreatedServiceRequest(_ serviceDetails: CreateServiceRequestPlanResponseDTO?) {
       
        EventTracking.shared.eventTracking(name: .srInProgress, [Constants.Event.EventKeys.subcategory: CacheManager.shared.eventProductName ?? "", Constants.Event.EventKeys.service: CacheManager.shared.srType?.rawValue ?? "", Constants.Event.EventKeys.srId: serviceDetails?.data?.serviceRequestId ?? ""])
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        
        if currentCategory == aEnumClaimWholeHomeCover.eBreakDown || currentCategory == aEnumClaimWholeHomeCover.ePms {
            let vc = ClaimSuccessVC()
            let model = ClaimSuccessViewModel()
            model.scheduleSlotStartDateTime = serviceDetails?.data?.scheduleSlotStartDateTime ?? ""
            model.scheduleSlotEndDateTime = serviceDetails?.data?.scheduleSlotEndDateTime ?? ""
            model.successType = .srSuccessWithVisit
            model.srId = serviceDetails?.data?.serviceRequestId?.description
            model.crmTrackingNumber = serviceDetails?.data?.refPrimaryTrackingNo
            vc.successModel = model
            vc.delegate = self
            presentInFullScreen(vc, animated: true, completion: nil)
            
        } else if currentCategory == aEnumClaimWholeHomeCover.eFire {
            let vc = ClaimSuccessVC()
            let model = ClaimSuccessViewModel()
            model.successType = .srSuccess
            model.srId = serviceDetails?.data?.serviceRequestId?.description
            model.crmTrackingNumber = serviceDetails?.data?.refPrimaryTrackingNo
            vc.successModel = model
            vc.delegate = self
            presentInFullScreen(vc, animated: true, completion: nil)
        } else {
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
//            presentingViewController?.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension HomeCoverProgressContainerVC : ClaimSuccessVCDelegate{
    func clickCrossButton() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        //        self.presentingViewController?.dismiss(animated: true, completion: nil)
//        self.tabBarController?.navigationController?.dismiss(animated: true, completion: {
//
//        })
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    func clickViewServiceRequest() {
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        //        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: {
            self.navigationController?.popToRootViewController(animated: true)
        })
//        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
// MARK: AppliancesNotWorkingDelegate

extension HomeCoverProgressContainerVC:AppliancesNotWorkingDelegate {
    func applianceNotWorkingButtonTapped(models: [AppliancesNotWorkingModel]) {
        if self.currentCategory == aEnumClaimWholeHomeCover.eAccidentalDamage || self.currentCategory == aEnumClaimWholeHomeCover.eBreakDown ||
            currentCategory == aEnumClaimWholeHomeCover.eBreakDown ||
            currentCategory == aEnumClaimWholeHomeCover.ePms {
            ClaimHomeServe.shared.productCode = models.first?.productCode
            ClaimHomeServe.shared.productName = models.first?.name
            ClaimHomeServe.shared.productVariantId = models.first?.productVariantId
        }
        ClaimHomeServe.shared.assetIds = models.compactMap{$0.assetId}
        moveToNext(controllerToLoad: {(controller : UIViewController) in
            if let whatHappenController = controller as? WhatHappenedVC {
              whatHappenController.productCode = ClaimHomeServe.shared.productCode!
                whatHappenController.productVariantId = ClaimHomeServe.shared.productVariantId
            }
        } , onCompletion: {(controller : UIViewController)in controller.viewWillAppear(true)})
    }
}

extension HomeCoverProgressContainerVC: ScheduleVisitDelegate {
    func clickedScheduleVisit(date: Date, slot: ScheduleVisitTimeSlotViewModel, serviceReqId: String?, crmTrackingNumber: String?) {
        ClaimHomeServe.shared.scheduledDate = date
        ClaimHomeServe.shared.scheduledTimeslot = slot
        raiseServiceRequest(category: currentCategory)
    }
}
