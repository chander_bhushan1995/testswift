//
//  HomeCoverProgressContainerInteractor.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/27/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol HomeCoverProgressContainerPresentationLogic {
    func presentServiceRequest(_ response: CreateServiceRequestPlanResponseDTO?, _ error: Error?)
    func presentServiceRequest(_ response: UpdateServiceRequestResponseDTO?, _ error: Error?)
}

class HomeCoverProgressContainerInteractor: HomeCoverProgressContainerBusinessLogic {
    var presenter: HomeCoverProgressContainerPresentationLogic?
    func updateServiceRequest(for type: ClaimType) {
        let currentUser = UserCoreDataStore.currentUser
        
        let request = UpdateServiceRequestDTO()
        request.requestDescription = ClaimHomeServe.shared.incidentDescription //
        request.placeOfIncident = ClaimHomeServe.shared.address //
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen //
        request.referenceNo = ClaimHomeServe.shared.memberShipId //
        request.serviceRequestId = ClaimHomeServe.shared.serviceRequestID
        //createdby
        request.modifiedBy = currentUser?.cusId //
//        if let homeCover = type as? aEnumClaimWholeHomeCover ,homeCover == .eFire {
//            request.workFlowValue = "2"
//        }
        request.refSecondaryTrackingNo = ClaimHomeServe.shared.assetIds?.joined(separator: ",") //
        if let date = ClaimHomeServe.shared.scheduledDate, let slot = ClaimHomeServe.shared.scheduledTimeslot {
            request.scheduleSlotStartDateTime = date.string(with: .slotDateFormat) + " " + slot.startTime! + ":00" //
            request.scheduleSlotEndDateTime = date.string(with: .slotDateFormat) + " " + slot.endTime! + ":00" //
        }
        
        if let issueId = ClaimHomeServe.shared.issueId {
            let issue = IssueReportedByCustomer(dictionary: [:])
            issue.issueId = issueId.stringValue //
            issue.issueDescription = ClaimHomeServe.shared.issueDescription //
            request.issueReportedByCustomer = [issue]
        }
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName //
        addressDetails.countryCode = "IND" //
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com" //
        addressDetails.mobileNo = CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber //
        addressDetails.pincode = ClaimHomeServe.shared.pinCode //
        addressDetails.addressLine1 = ClaimHomeServe.shared.address //
        
       
        
        request.serviceRequestAddressDetails = addressDetails
        
        UpdateServiceRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
        
    }

    func createServiceRequest(for type: ClaimType) {
        let currentUser = UserCoreDataStore.currentUser
        
        let request = CreateServiceRequestPlanRequestDTO()
        request.requestDescription = ClaimHomeServe.shared.incidentDescription //
        request.placeOfIncident = ClaimHomeServe.shared.address //
        request.dateOfIncident = ClaimHomeServe.shared.dateWhenHappen //
        request.serviceRequestType = type.getServiceCode() //
        request.referenceNo = ClaimHomeServe.shared.memberShipId //
        
        request.createdBy = currentUser?.cusId //
        
        if let flag = ClaimHomeServe.shared.supportedRequestTypes?.filter({ $0.service.rawValue == type.getServiceCode() }).first?.isInsuranceBacked, !flag {
            
            if type.getServiceCode() == Constants.Services.breakdown.rawValue {
                request.insuranceBacked = "N"
            }
            
            if let pc = ClaimHomeServe.shared.productCode {
                let product = Product(dictionary: [:])
                product.productCode = pc
                request.assets = [product]
            }
        }
        
        if let homeCover = type as? aEnumClaimWholeHomeCover ,homeCover == .eFire {
                request.workFlowValue = "2"
        }

        if let assetIds = ClaimHomeServe.shared.assetIds, !assetIds.isEmpty {
            request.refSecondaryTrackingNo = assetIds.joined(separator: ",")
        }
        
        if let date = ClaimHomeServe.shared.scheduledDate, let slot = ClaimHomeServe.shared.scheduledTimeslot {
            request.scheduleSlotStartDateTime = date.string(with: .slotDateFormat) + " " + slot.startTime! + ":00" //
            request.scheduleSlotEndDateTime = date.string(with: .slotDateFormat) + " " + slot.endTime! + ":00" //
        }
        
        if let issueId = ClaimHomeServe.shared.issueId {
            let issue = IssueReportedByCustomer(dictionary: [:])
            issue.issueId = issueId.stringValue //
            issue.issueDescription = ClaimHomeServe.shared.issueDescription //
            
            request.issueReportedByCustomer = [issue]
        }
        
        let addressDetails = ServiceRequestAddressDetail(dictionary: [:])
        
        addressDetails.addresseeFullName = currentUser?.userName //
        addressDetails.countryCode = "IND" //
        addressDetails.email = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "abc@gmail.com" //
        addressDetails.mobileNo = CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber//
        addressDetails.pincode = ClaimHomeServe.shared.pinCode //
        addressDetails.addressLine1 = ClaimHomeServe.shared.address //
        
        //FIXME: Service issue
        addressDetails.district = "Delhi"
        addressDetails.landmark = "Delhi"
        
        request.serviceRequestAddressDetails = addressDetails
        
        CreateServiceRequestPlanRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.presentServiceRequest(response, error)
        }
    }
}
