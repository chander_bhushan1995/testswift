//
//  BrieflyDescVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 22/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit



private class BrieflyDescriptionModel: IncidentDescriptionBaseModel {
    
    override var linkText: String {
        return ""
    }
}

class BrieflyDescVC: IncidentDescriptionBaseVC{
    
    override var isWhere: Bool {
        return false
    }
    
    convenience init() {
        self.init(nibName: "IncidentDescriptionBaseVC", bundle: nil)
    }
    override var howAndWhereHappenedModel: HowAndWhereHappenedModel {
        return BrieflyDescriptionModel()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
