//
//  ImageCollectionTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/2/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class ImageCollectionTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var cells = [Int: DocumentImageCollectionViewCell]()
    
    fileprivate var cellWidth: CGFloat!, cellHeight: CGFloat!
    fileprivate var images: [String]!
    fileprivate var imageModel = [DocumentImageModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        
        collectionView.register(nib: Constants.CellIdentifiers.WHCService.documentImageCollectionViewCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.documentImageCollectionViewCell)
    }
    
    func set(images: [String], width: CGFloat, height: CGFloat) {
        self.cellWidth = width
        self.cellHeight = height
        self.images = images
        for (index, element) in images.enumerated() {
            imageModel.append(DocumentImageModel(imageStr: element, index: index))
        }
        collectionView.reloadData()
    }
}

extension ImageCollectionTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        var cell: DocumentImageCollectionViewCell
//        if let oldCell = cells[indexPath.row] {
//            cell = oldCell
//        } else {
//            cell = Bundle.main.loadNibNamed("DocumentImageCollectionViewCell", owner: nil, options: nil)?.first as! DocumentImageCollectionViewCell
//            cell.setView(image: images[indexPath.row])
//            cells[indexPath.row] = cell
//        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.WHCService.documentImageCollectionViewCell, for: indexPath) as! DocumentImageCollectionViewCell
//        cell.setView(image: images[indexPath.row])
        cell.setView(model: imageModel[indexPath.row])
        return cell
    }
}

extension ImageCollectionTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellHeight)
    }
}
