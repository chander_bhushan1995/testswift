//
//  DocumentImageCollectionViewCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/2/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class DocumentImageModel {
    var image: UIImage?
    var imageStr: String?
    var index: Int?
    
    init(imageStr: String?, index: Int?) {
        self.imageStr = imageStr
        self.index = index
    }
}

class DocumentImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var spinnerView: SpinnerView?
    
    var imageID: String = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        containerView.layer.cornerRadius = 6.0
        containerView.layer.borderColor = UIColor.dlsBorder.cgColor
//        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
//        activityIndicator.color = .black
//        activityIndicator.startAnimating()
        activityIndicator.stopAnimating()
        spinnerView = SpinnerView(view: containerView)
        spinnerView?.startAnimating()
        
    }
    
    func setView(image: String) {
        self.imageID = image
        self.imageView.image = nil
        let request = GetClaimDocumentRequestDTO(documentId: image, claimDocType: .thumbnail)
        GetClaimDocumentRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            
//            self.activityIndicator.stopAnimating()
            self.spinnerView?.stopAnimating()
            if self.imageID == request.documentIds!{
                self.imageView.image = response?.image
            }else {
                self.imageView.image = nil
            }
        }
    }
    
    func setView(model: DocumentImageModel) {
        
        if let img = model.image {
//            self.activityIndicator.stopAnimating()
            spinnerView?.stopAnimating()
            imageView.image = img
        } else {
            self.imageView.image = nil
            imageID = model.imageStr ?? ""
//            activityIndicator.startAnimating()
             spinnerView?.startAnimating()
            let request = GetClaimDocumentRequestDTO(documentId:model.imageStr ,storageRefIds:nil,claimDocType: .thumbnail)
            GetClaimDocumentRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
                
                model.image = response?.image
                if self.imageID == request.documentIds!{
            //                self.activityIndicator.stopAnimating()
            self.spinnerView?.stopAnimating()

                    self.imageView.image = response?.image
                }else {
                    self.imageView.image = nil
                }
            }
        }
    }
}
