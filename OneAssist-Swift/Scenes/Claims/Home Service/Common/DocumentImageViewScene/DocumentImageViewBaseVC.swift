//
//  DocumentImageViewBaseVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/2/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

protocol DocumentImageViewDataSource: class {
    var cellHeight: CGFloat { get }
    
    var itemWidth: CGFloat { get }
    
    var images: [String] { get }
    
    var section: [TableViewSection<Row>]! { get }
}

class DocumentImageViewBaseVC: BaseVC {

    @IBOutlet weak var headerLabel: H2RegularLabel!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: DocumentImageViewDataSource!
    var headerText: String! = "Images of your Refrigerator"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        tableView.register(nib: Constants.CellIdentifiers.WHCService.imageCollectionTableViewCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.imageCollectionTableViewCell)
        tableView.register(nib: Constants.CellIdentifiers.WHCService.documentImageViewLabelCell, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.documentImageViewLabelCell)
        headerLabel.text = headerText
    }
    
    @IBAction func clickedBtnClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension DocumentImageViewBaseVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.section.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return dataSource.section[section-1].rows.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.imageCollectionTableViewCell, for: indexPath) as! ImageCollectionTableViewCell
            cell.set(images: dataSource.images, width: dataSource.itemWidth, height: dataSource.cellHeight)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.documentImageViewLabelCell, for: indexPath) as! DocumentImageViewLabelCell
            cell.descriptionLabel.text = nil
            if let atributedString = dataSource.section[indexPath.section - 1].rows[indexPath.row].atributedTitle {
                cell.descriptionLabel.attributedText = atributedString
                
            }else {
               cell.descriptionLabel.text = dataSource.section[indexPath.section - 1].rows[indexPath.row].title
            }
            
            return cell
        }
    }
}

extension DocumentImageViewBaseVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return dataSource.cellHeight
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return .leastNormalMagnitude
        } else {
            return 35
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        } else {
            let headerView = Bundle.main.loadNibNamed(Constants.NIBNames.WHCService.tableViewLabelHeader, owner: nil, options: nil)?.first as? TableViewLabelHeader
            headerView?.textLabel.text = dataSource.section[section - 1].title
            return headerView
        }
    }
}
