//
//  DocumentImageViewLabelCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/2/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class DocumentImageViewLabelCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var descriptionLabel: H3RegularBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
