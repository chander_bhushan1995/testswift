//
//  AppliancesNotWorkingPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AppliancesNotWorkingDisplayLogic: class {
    func displayLogic(model:TableViewDataSource<AppliancesNotWorkingModel,TableViewSection<AppliancesNotWorkingModel>> , unAvailableAsset : Bool)
    func displayError(_ error:String)
}

class AppliancesNotWorkingPresenter: BasePresenter, AppliancesNotWorkingPresentationLogic {
    weak var viewController: AppliancesNotWorkingDisplayLogic?
    var costDict: [String: NSNumber]?
    
    // MARK: Presentation Logic Conformance
    func presentAssetList(serviceType: Constants.Services, assetID: [String]?, memDetails: CustomerMembershipDetails?) {
        var models: [AppliancesNotWorkingModel] = []
        
        if let services = ClaimHomeServe.shared.totalServices, let serviceModel = services.filter({ $0.service == serviceType }).first {
            
            var totalAssets: [Asset]? = ClaimHomeServe.shared.totalAssets
            if serviceModel.isInsuranceBacked == false {
                totalAssets = serviceModel.assets
            }
            
            if let allAssets = totalAssets {
                for asset in allAssets {
                    
                    let assetName = "\(asset.brand ?? "") \(asset.name ?? "")"
                    var imageName = ""
                    let product = Utilities.getProduct(productCode: asset.prodCode ?? "")
                    imageName = product?.subCatImageUrl ?? ""
                    var subTitle = ""
                    // Add subtitle of assets
                    if serviceModel.isInsuranceBacked ?? true {
                        subTitle = asset.technology ?? ""
                        if subTitle.count > 0 {
                            if let size = asset.size, size.count > 0{
                                    if let sizeUnit = asset.sizeUnit {
                                        subTitle = subTitle + " | " + size + " \(sizeUnit)"
                                    } else {
                                        subTitle = subTitle + " | " + size
                                    }
                                    if let serialNo = asset.serialNo {
                                        if serialNo.count > 0 {
                                            subTitle = subTitle + " | " + serialNo
                                        }
                                    }
                            } else if let serialNo = asset.serialNo {
                                if serialNo.count > 0 {
                                    subTitle = subTitle + " |" + serialNo
                                }
                            }
                        } else if let size = asset.size, size.count > 0 {
                                if let sizeUnit = asset.sizeUnit {
                                    subTitle =  size + " \(sizeUnit)"
                                } else {
                                    subTitle =  size
                                }
                                if let serialNo = asset.serialNo {
                                    if serialNo.count > 0 {
                                        subTitle = subTitle + " | " + serialNo
                                    }
                                }
                        } else if let serialNo = asset.serialNo {
                            if serialNo.count > 0 {
                                subTitle = serialNo
                            }
                        }
                    }
                    var feature: String? = subTitle
                    if serviceModel.isInsuranceBacked == false || subTitle.isEmpty {
                        feature = nil
                    }
                    
                    let model = AppliancesNotWorkingModel(name: assetName, features: feature, deviceImageName: imageName)
                    model.productCode = asset.prodCode
                    model.productVariantId = asset.productVariantId
                    
                    // Add text if service count is 0
                    if let cost = costDict?[asset.prodCode ?? ""]?.intValue {
                        model.costText = "Note: The price for service will be around ₹\(cost)* and might be borne by you.\n* this is a tentative price for one appliance only."
                    }
                    
                    if let assetId = asset.assetId?.description {
                        model.assetId = assetId
                        if serviceModel.isInsuranceBacked ?? true {
                            //For pending submission case we have to show already selected row.
                            if let assetIds = assetID {
                                for key in assetIds {
                                    if assetId == key {
                                        model.isSelected = true
                                    }
                                }
                            }
                            
                            // if service model does not contain the given asset
                            if serviceModel.assets?.contains(asset) == false {
                                // Service not available for this service type
                                model.isDisabled = true
                                model.isDisabledDueToServiceUnavailability = true
                            }
                            
                            if let assetEligibilty =  ClaimHomeServe.shared.assetAvailability?[assetId] {
                                if assetEligibilty.eligible == false {
                                    model.isDisabled = true
                                    model.reason = assetEligibilty.reason
                                }
                            }
                            
                        }
                    }
                    models.append(model)
                    
                }
            }
            let dataSource = TableViewDataSource<AppliancesNotWorkingModel, TableViewSection<AppliancesNotWorkingModel>>()
            let section = TableViewSection<AppliancesNotWorkingModel>()
            section.rows = models
            
            dataSource.tableSections = [section]
            
            viewController?.displayLogic(model:dataSource,unAvailableAsset: models.contains(where: {$0.isDisabledDueToServiceUnavailability == true}))
        } else {
            // TODO: Handle case properly
            viewController?.displayError("Something went wrong")
        }
    }
    
    func receivedServiceCost(request: GetServiceCostRequestDTO, response: GetServiceCostResponseDTO?, error: Error?) {
        try? checkError(response, error: error)
        costDict = [String: NSNumber]()
        if let costList = response?.data {
            
            // Get max service cost for product code
            for item in costList {
                if let prodCode = item.productCode, let cost = item.serviceCost?.intValue {
                    if let prodCost = costDict?[prodCode]?.intValue {
                        if prodCost < cost {
                            costDict?[prodCode] = cost as NSNumber
                        }
                    } else {
                        costDict?[prodCode] = cost as NSNumber
                    }
                }
            }
        }
    }
}
