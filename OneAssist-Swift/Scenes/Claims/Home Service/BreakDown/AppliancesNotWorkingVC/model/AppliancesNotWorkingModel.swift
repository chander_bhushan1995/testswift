//
//  AppliancesNotWorkingModel.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class AppliancesNotWorkingModel: Row {

    var name:String?
    var features:String?
    var deviceImageName:String?
    var assetId : String?
    var isSelected : Bool = false
    var reason : String?
    var isDisabled = false
    var isDisabledDueToServiceUnavailability = false
    var productCode:String?
    var productVariantId: NSNumber?
    var costText: String?
    
    init( name: String, features: String?, deviceImageName: String?) {
        self.name = name
        self.features = features
        self.deviceImageName = deviceImageName
    }
    
}
