//
//  AppliancesNotWorkingInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AppliancesNotWorkingPresentationLogic {
    
    func presentAssetList(serviceType: Constants.Services, assetID:[String]?, memDetails: CustomerMembershipDetails?)
    func receivedServiceCost(request: GetServiceCostRequestDTO,response: GetServiceCostResponseDTO?, error: Error?)
}

class AppliancesNotWorkingInteractor: BaseInteractor, AppliancesNotWorkingBusinessLogic {
    var presenter: AppliancesNotWorkingPresentationLogic?

    // MARK: Business Logic Conformance
    
    func getAssetList(serviceType: Constants.Services, assetID: [String]?, memDetails: CustomerMembershipDetails?, isServiceCountZero: Bool) {
        
        if isServiceCountZero {
            let req = GetServiceCostRequestDTO(serviceType: serviceType.rawValue, prodCode: nil)
            
            let _ = GetServiceCostRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
                self?.presenter?.receivedServiceCost(request: req, response: response, error: error)
                self?.presenter?.presentAssetList(serviceType: serviceType, assetID: assetID, memDetails: memDetails)
            }
        } else {
            self.presenter?.presentAssetList(serviceType: serviceType, assetID: assetID, memDetails: memDetails)
        }
    }
    
    func getCostForService(serviceType: Constants.Services) {
        let req = GetServiceCostRequestDTO(serviceType: serviceType.rawValue, prodCode: nil)
        
        let _ = GetServiceCostRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.receivedServiceCost(request: req, response: response, error: error)
        }
    }
}
