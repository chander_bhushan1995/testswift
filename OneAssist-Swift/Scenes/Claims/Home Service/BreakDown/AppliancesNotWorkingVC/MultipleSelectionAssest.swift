//
//  CaughtFireCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 21/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class MultipleSelectionAssest: UITableViewCell {

    @IBOutlet weak var imgDevice: UIImageView!
 
    @IBOutlet weak var lblDeviceName: UILabel!
   
    @IBOutlet weak var LbldeviceSpecification: UILabel!
    
    @IBOutlet weak var btnCheck: UIButton!
  
    var originalSubtitleColor : UIColor!
    override func awakeFromNib() {
        super.awakeFromNib()
        initialise()
    }
   
    func updateUserInterface(name: String, subtitle: String, image:String?,isSelected:Bool, isDisabled: Bool) {
        self.imgDevice.setImageWithUrlString(image)
        self.lblDeviceName.text = name
        self.LbldeviceSpecification.text = subtitle
        if isSelected{
            self.btnCheck.setImage(#imageLiteral(resourceName: "appliance_checkbox_selected"), for: .normal)
        }
        else{
            self.btnCheck.setImage(#imageLiteral(resourceName:"appliance_checkbox_unselected"), for: .normal)
        }
        
        lblDeviceName.setSelectedOrNot(isSelected)
        
        if isDisabled {
            
            self.imgDevice.alpha = 0.3
            self.lblDeviceName.alpha = 0.3
            self.btnCheck.alpha = 0.3
            isUserInteractionEnabled = false
        } else {
            self.imgDevice.alpha = 1.0
            self.lblDeviceName.alpha = 1.0
            self.btnCheck.alpha = 1.0
            isUserInteractionEnabled = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func initialise(){
        self.imgDevice.image = UIImage(named: Constants.WHCService.radioButtonInactive)
        self.originalSubtitleColor = self.LbldeviceSpecification.textColor
    }
    
    func setModel(_ model:AppliancesNotWorkingModel){
        self.imgDevice.image = UIImage(named:model.deviceImageName!)
        self.lblDeviceName.text = model.name
        self.LbldeviceSpecification.text = model.features
    }
    
  
   
}
