//
//  AppliancesNotWorkingCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class SingleSelectionAssest: UITableViewCell {

    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var imgAppliances: UIImageView!
    @IBOutlet weak var lblDeviceName: UILabel!
    @IBOutlet weak var lbldeviceFeatures: UILabel!
    @IBOutlet weak var labelCost: TagsRegularGreyLabel!
    
    var originalSubtitleColor : UIColor!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialise()
        // Initialization code
    }
    
    func updateUserInterface(name: String, subtitle: String?, image:String?, isSelected:Bool, isDisabled: Bool, costText: String? = nil) {
        self.imgAppliances.setImageWithUrlString(image)
        self.lblDeviceName.text = name
        self.lbldeviceFeatures?.text = subtitle
        self.imgRadio.image = isSelected ?#imageLiteral(resourceName: "radioButtonActive") :#imageLiteral(resourceName:"radioButtonInactive")
        
        lblDeviceName.setSelectedOrNot(isSelected)
        labelCost.text = isSelected ? costText : nil
        if subtitle == nil {
            self.lbldeviceFeatures?.removeFromSuperview()
        }
        
        
        
        if isDisabled {
            self.imgAppliances.alpha = 0.3
            self.lblDeviceName.alpha = 0.3
            self.imgRadio.alpha = 0.3
            isUserInteractionEnabled = false
        } else {
            self.imgAppliances.alpha = 1.0
            self.lblDeviceName.alpha = 1.0
            self.imgRadio.alpha = 1.0
            isUserInteractionEnabled = true
        }
    }
    
    func initialise(){
        self.imgRadio.image = UIImage(named: Constants.WHCService.radioButtonInactive)
        self.originalSubtitleColor = self.lbldeviceFeatures?.textColor

    }

    func setModel(_ model:AppliancesNotWorkingModel){
        self.imgAppliances.setImageWithUrlString(model.imageName)
        self.lblDeviceName.text = model.name
        self.lbldeviceFeatures?.text = model.features
    }
}
