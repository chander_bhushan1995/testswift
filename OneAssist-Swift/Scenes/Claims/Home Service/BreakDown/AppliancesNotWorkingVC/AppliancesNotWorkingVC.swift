//
//  AppliancesNotWorkingVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 12/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

fileprivate enum EnumSelectionType {
    case eSingle
    case eMultiple
}

protocol AppliancesNotWorkingBusinessLogic {
//    func getAssets(memId: String, custId: String, serviceType: Constants.Services,assestID:[String]?)
    func getAssetList(serviceType: Constants.Services, assetID:[String]?, memDetails: CustomerMembershipDetails?, isServiceCountZero: Bool)
    func getCostForService(serviceType: Constants.Services)
}

protocol AppliancesNotWorkingDelegate{
    func applianceNotWorkingButtonTapped(models:[AppliancesNotWorkingModel])
}

class AppliancesNotWorkingVC: BaseVC ,PagingProtocol{
    
   
    
    @IBOutlet weak var txtViewSubtitle: LinkTextView!
    
    var interactor: AppliancesNotWorkingBusinessLogic?
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblSubtitle: UILabel!
    @IBOutlet fileprivate weak var btnSelectIssue: PrimaryButton!
    @IBOutlet fileprivate weak var tableView: AutoresizingTableView!
    
    private var serviceType: Constants.Services!
    var assestID:[String]?
    var custId = ""
    var membershipId: String!
    var categoryName:String!
    
    var category: Constants.Services! {
        didSet {
            if let cat = category {
                switch cat {
                case .accidentalDamage:
                    serviceType = .accidentalDamage
                    self.categoryName = "Accidental damage"
                    self.selectionType = .eSingle
                case .breakdown :
                    serviceType = .breakdown
                    self.categoryName = "Breakdown"
                    self.selectionType = .eSingle
                case .fire :
                    serviceType = .fire
                    self.categoryName = "Fire"
                    self.selectionType = .eMultiple
                case .buglary:
                    serviceType = .buglary
                    self.categoryName = "Stolen"
                    self.selectionType = .eMultiple
                case .pms:
                    serviceType = .pms
                    self.categoryName = "PMS"
                    self.selectionType = .eSingle
                case .extendedWarranty:
                    serviceType = .extendedWarranty
                    self.categoryName = "Extended Warranty"
                    self.selectionType = .eSingle
                default: break
                }
            }
        }
    }
    
    var currentPage: Int = 0
    var preSelectedIndex:IndexPath?
    var delegate:AppliancesNotWorkingDelegate?
    fileprivate var selectionType:EnumSelectionType? = .eSingle
    var dataSource: TableViewDataSource<AppliancesNotWorkingModel,TableViewSection<AppliancesNotWorkingModel>> = TableViewDataSource()
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if dataSource.numberOfSections == 0 {
            refreshData()
        }
        EventTracking.shared.addScreenTracking(with: .SR_Appliance)
        //btnSelectIssue.isUserInteractionEnabled = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //btnSelectIssue.isUserInteractionEnabled = true
    }
    
    // MARK:- private methods
    fileprivate func refreshData() {

        var isCountZero = false
        
        //calculate total let visits for particular service type
        if let count = Utilities.noOfServicesLeft(from: ClaimHomeServe.shared.totalServices, for: serviceType)?.intValue, count <= 0 {
            isCountZero = true
        }
        
        Utilities.addLoader(onView: view, message: "Please wait", count: &loaderCount, isInteractionEnabled: true)
        interactor?.getAssetList(serviceType: serviceType, assetID: assestID, memDetails: CacheManager.shared.memResponse, isServiceCountZero: isCountZero)
    }
    
    private func initializeView() {
        self.title = Strings.ServiceRequest.Titles.raiseARequest
        
        var title : String
        var subTitle : NSAttributedString
        
        if let cat = self.category {
            switch cat {
            case .accidentalDamage:
                fallthrough
            case .breakdown :
                title = Strings.ServiceRequest.whichAppliance
                subTitle =  NSAttributedString(string: Strings.ServiceRequest.enable, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                btnSelectIssue.setTitle(Strings.Actions.whichAppliance, for: .normal)
            case .pms:
                title = Strings.ServiceRequest.serviceWhichAppliance
                subTitle =  NSAttributedString(string: Strings.ServiceRequest.help, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                btnSelectIssue.setTitle(Strings.ServiceRequest.scheduleVisit, for: .normal)

            case .fire:
                title = Strings.ServiceRequest.applianceCaughtFire
                subTitle =    NSAttributedString(string: Strings.ServiceRequest.selectMultipleApplainces, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                
                btnSelectIssue.setTitle(Strings.Actions.whichAppliance, for: .normal)
            case .buglary:
                title = Strings.ServiceRequest.applianceGotStolen
                subTitle =   NSAttributedString(string: Strings.ServiceRequest.selectMultipleApplainces, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                btnSelectIssue.setTitle(Strings.Actions.applianceCaughtFire, for: .normal)

            default:
                title = Strings.ServiceRequest.whichAppliance
                subTitle = NSAttributedString(string: Strings.ServiceRequest.enable, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
            }
        } else {
            title = Strings.ServiceRequest.whichAppliance
            subTitle = NSAttributedString(string: Strings.ServiceRequest.enable, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
           
        }
        
        custId = UserCoreDataStore.currentUser?.cusId ?? ""
        self.lblTitle.text = title
        self.txtViewSubtitle.attributedText = subTitle
        tableView.register(nib: Constants.NIBNames.WHCService.singleSelectionAssest, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.singleSelectionAssest)
        tableView.register(nib: Constants.NIBNames.WHCService.multipleSelectionAssest, forCellReuseIdentifier: Constants.CellIdentifiers.WHCService.multipleSelectionAssest)
        tableView.separatorStyle = .none
        self.btnSelectIssue.isEnabled = false
        lblTitle.font = DLSFont.h3.bold
        lblTitle.textColor = UIColor.charcoalGrey
        
        if let count = Utilities.noOfServicesLeft(from: ClaimHomeServe.shared.totalServices, for: serviceType), count == 0 {
            //TODO: API for service cost
        }
    }
    
    func selectedModel() -> [AppliancesNotWorkingModel]{
        var arrModel = [AppliancesNotWorkingModel]()
        for section in dataSource.tableSections {
            for model in section.rows {
                if model.isSelected == true {
                    arrModel.append(model)
                }
            }
        }
        return arrModel
    }
    
    //Mark: - Screen Actions
    @IBAction func clickIssueBtn(_ sender: Any) {
        
        //btnSelectIssue.isUserInteractionEnabled = false
        let models =  selectedModel()
        var name = ""
        for item in models {
            name = name + (item.name ?? "")
        }
        EventTracking.shared.eventTracking(name: .SRApplianceSubmit,[.appliancesName: name, .service: category.rawValue])
        CacheManager.shared.eventProductName = name
        self.delegate?.applianceNotWorkingButtonTapped(models: models)
    }
}

// MARK:- Display Logic Conformance
extension AppliancesNotWorkingVC: AppliancesNotWorkingDisplayLogic {
    func displayLogic(model:TableViewDataSource<AppliancesNotWorkingModel,TableViewSection<AppliancesNotWorkingModel>> , unAvailableAsset : Bool){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if (unAvailableAsset){
            let subtitleTxtViewText: NSAttributedString = {
                let atrStr = NSMutableAttributedString(string: "Few of your appliances are not covered under \(self.categoryName ?? ""). For further help, ", attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.gray1])
                let atrStr3 = NSAttributedString(string: "chat with us.", attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.dodgerBlue])

                atrStr.append(atrStr3)
                return atrStr
            } ()
            
            txtViewSubtitle.setAttributedLink(completeText: subtitleTxtViewText, linkText: "chat with us")
            txtViewSubtitle.linkDelegate = self
        }
        
        dataSource = model
        enableSubmitButtonIfAlreadySelected(arrSection: dataSource.tableSections)
        tableView.reloadData()
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, primaryAction: {
            self.refreshData()
        }) { }
    }
    
    private func enableSubmitButtonIfAlreadySelected(arrSection : [TableViewSection<AppliancesNotWorkingModel>]){
        
        for section in arrSection {
            for row in section.rows {
                if row.isSelected {
                    self.btnSelectIssue.isEnabled = true
                    return;
                }
            }
        }
        
    }

}

// MARK:- Configuration Logic
extension AppliancesNotWorkingVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = AppliancesNotWorkingInteractor()
        let presenter = AppliancesNotWorkingPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension AppliancesNotWorkingVC {
    
}

extension AppliancesNotWorkingVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.ChangeRowModelProperty(indexPath: indexPath)
        self.tableView.reloadData()   
    }
    
    private func ChangeRowModelProperty(indexPath: IndexPath){
        let section = dataSource.sectionModel(for: indexPath.section)
        
        if selectionType == .eSingle {
            var index = 0
            for model in section.rows {
                if index == indexPath.row {
                    model.isSelected = !model.isSelected
                    if model.isSelected == true {
                        self.btnSelectIssue.isEnabled = true
                    }else {
                        self.btnSelectIssue.isEnabled = false
                    }
                }else {
                    model.isSelected = false
                }
                index = index + 1
            }
        }else {
            let rowModel = dataSource.rowModel(for: indexPath)
            rowModel.isSelected = !rowModel.isSelected
            self.btnSelectIssue.isEnabled = false
            for model in section.rows {
                if model.isSelected == true {
                    self.btnSelectIssue.isEnabled = true
                    break;
                }
            }
        }
        
    }
    
}


extension AppliancesNotWorkingVC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectionType! {
        case .eSingle:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.singleSelectionAssest, for: indexPath) as! SingleSelectionAssest
            let rowModel = dataSource.rowModel(for: indexPath)
            
            var subtitle : String?
            cell.lbldeviceFeatures?.alpha = rowModel.isDisabled == true ? 0.3 : 1.0
            if let reason = rowModel.reason {
                subtitle = reason
                cell.lbldeviceFeatures?.textColor = UIColor.orangePeel
                cell.lbldeviceFeatures?.alpha = 1.0
            } else {
                cell.lbldeviceFeatures?.textColor = cell.originalSubtitleColor
                subtitle = rowModel.features
            }
            
            cell.updateUserInterface(name: rowModel.name ?? "", subtitle: subtitle, image: rowModel.deviceImageName, isSelected: rowModel.isSelected, isDisabled: rowModel.isDisabled, costText: rowModel.costText)
            cell.selectionStyle = .none
            return cell
            
        case .eMultiple:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.WHCService.multipleSelectionAssest, for: indexPath) as! MultipleSelectionAssest
            let rowModel = dataSource.rowModel(for: indexPath)
            var subtitle : String?
            
            cell.LbldeviceSpecification.alpha = rowModel.isDisabled == true ? 0.3 : 1.0
            if let reason = rowModel.reason {
                subtitle = reason
                cell.LbldeviceSpecification.textColor = UIColor.orangePeel
                cell.LbldeviceSpecification.alpha = 1.0

            }else{
                cell.LbldeviceSpecification.textColor = cell.originalSubtitleColor
                subtitle = rowModel.features
            }
            
            cell.updateUserInterface(name: rowModel.name ?? "" , subtitle: subtitle ?? "" , image: rowModel.deviceImageName, isSelected: rowModel.isSelected, isDisabled: rowModel.isDisabled)
            cell.selectionStyle = .none
            return cell
        }
    }
}
extension AppliancesNotWorkingVC: LinkTextViewDelegate {
    func linkClicked(linkText: String) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.chatWithUs.message, subTitle:nil, forScreen: nil) {[unowned self] (status) in
            if status {
                EventTracking.shared.eventTracking(name: .chatTapped)
                EventTracking.shared.eventTracking(name: .chatOnHomeTapped)
                self.present(ChatVC(), animated: true, completion: nil)
            }
        }
       
    }
}

