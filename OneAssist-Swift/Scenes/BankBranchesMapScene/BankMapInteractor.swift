//
//  BankMapInteractor.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

enum BankRadius : Float {
    case initialRadiusValue = 3000.0
    case increaseRadiusValue = 30.0
}

protocol BankMapPresentationLogic {
    func responseRecieved(response: NearestBankBranchResponseDTO?, error: Error?)
    func showIncreaseRadiusAlert()
    func userLocationDidUpdate(location : CLLocationCoordinate2D)
}

class BankMapInteractor: BaseInteractor, BankMapBusinessLogic {
    var presenter: BankMapPresentationLogic?
    var radius : Float = BankRadius.initialRadiusValue.rawValue
    lazy var locationManager = Permission.shared.locationManager

    // MARK: Business Logic Conformance
    func getNearestBankBranches(bankName : String, latitude : NSNumber, longitude : NSNumber) {
        let request = NearestBankBranchRequestDTO(bankName: bankName, latitude: latitude, longitude: longitude, radius: NSNumber.init(value: self.radius))
        let _ = NearestBankBranchRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.responseRecieved(response: response, error: error)
        }
    }

    func increaseRadius() {
        self.radius = BankRadius.increaseRadiusValue.rawValue
    }

    func isRadiusInitialValue() {
        if radius == BankRadius.initialRadiusValue.rawValue {
            presenter?.showIncreaseRadiusAlert()
        }
    }

    func fetchUserLocation() {
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
}

extension BankMapInteractor : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        let currentLocation = locations[0]
        presenter?.userLocationDidUpdate(location: currentLocation.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        let errors = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey: Strings.Global.somethingWrong])
        presenter?.responseRecieved(response: nil, error: errors)
    }
}

