//
//  BankMapVC.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import GoogleMaps

protocol BankMapBusinessLogic {
    func getNearestBankBranches(bankName : String, latitude : NSNumber, longitude : NSNumber)
    func increaseRadius()
    func isRadiusInitialValue()
    func fetchUserLocation()
}

class BankClusterMarkerModel: NSObject, GMUClusterItem {
    var bankName : String?
    var bankAddress : String?
    var bankLogoUrl : String?
    var bankPhoneNumber : String?
    var position: CLLocationCoordinate2D
    var inactiveIcon: UIImage?
    var activeIcon : UIImage?

    init(position: CLLocationCoordinate2D) {
        self.position = position
    }
}

class BankMapVC : BaseVC {

    var interactor: BankMapBusinessLogic?

    var issuerModel : WalletCard!

    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var bankDirectionsView : BankDirectionView!
    @IBOutlet var constraintBankDirectionsViewHeight: NSLayoutConstraint!

    var userLocation : CLLocationCoordinate2D!
    fileprivate var currentSelectedMarker : GMSMarker?
    fileprivate var currentSelectedLocation : CLLocationCoordinate2D?
    
    fileprivate var clusterManager: GMUClusterManager!
    fileprivate var markers : [BankClusterMarkerModel] = []

    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterForegroundOnViewController), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    
    @objc func didEnterForegroundOnViewController(){
        UserDefaultsKeys.Location.set(value: Event.Events.bankBranchFinder.rawValue)
        Utilities.checkForRatingPopUp(viewController: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        configureView()
    }
    
    deinit {
         Permission.shared.resetData()
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    // MARK:- private methods
    private func initializeView() {

        // TODO: Remove when connected in the flow
        self.title = issuerModel.issuerName ?? ""

        bankDirectionsView.delegate = self
        bankDirectionsView.isHidden = true

        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true

        // To shift the user location button on map a padding of 50 px is added so that directions button does not overlap
        let mapInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 40.0, right: 0.0)
        mapView.padding = mapInsets

        // Set up the cluster manager with default icon generator and renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 50, 100, 200, 1000], backgroundImages: [#imageLiteral(resourceName: "bankBranchMarkerInactive"), #imageLiteral(resourceName: "bankBranchMarkerInactive"), #imageLiteral(resourceName: "bankBranchMarkerInactive"), #imageLiteral(resourceName: "bankBranchMarkerInactive"), #imageLiteral(resourceName: "bankBranchMarkerInactive")])

        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self

        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
    }

    private func configureView() {

        constraintBankDirectionsViewHeight.constant = 0
        bankDirectionsView.updateConstraints()
        
        Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.bankBranch) {[weak self] (status) in
            guard let self = self else {return}
            if status ==  .notDetermined{
            }else if status == .authorizedAlways || status == .authorizedWhenInUse {
                if self.mapView.isMyLocationEnabled {
                    self.interactor?.fetchUserLocation()
                }
                
            }else {
                self.showOpenSettingsAlert(title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    func showBankDetailsCardView(item : BankClusterMarkerModel) {
        bankDirectionsView.isHidden = false

        if item.bankPhoneNumber == nil || item.bankPhoneNumber == "" {
            self.constraintBankDirectionsViewHeight.constant = 135
        } else {
            self.constraintBankDirectionsViewHeight.constant = 180
        }

        UIView.animate(withDuration: 0.2) {
            self.bankDirectionsView.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }

    func hideBankDetailsCardView() {
        currentSelectedMarker?.icon = #imageLiteral(resourceName: "bankBranchMarkerInactive")
        currentSelectedMarker = nil
        currentSelectedLocation = nil

        constraintBankDirectionsViewHeight.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.bankDirectionsView.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.bankDirectionsView.isHidden = true
        }
    }
}

extension BankMapVC : GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {

        if let item = marker.userData as? BankClusterMarkerModel {

            if currentSelectedMarker == nil || (currentSelectedMarker != nil && marker != currentSelectedMarker!) {

                if item.bankPhoneNumber == nil || item.bankPhoneNumber == "" {
                    bankDirectionsView.constraintBankCallViewHeight.constant = 0
                } else {
                    bankDirectionsView.constraintBankCallViewHeight.constant = 45
                }

                bankDirectionsView.bankPhoneNumber = item.bankPhoneNumber
                bankDirectionsView.directionView.bankCoordinates = item.position

                bankDirectionsView.directionView.bankNameLabel.text = item.bankName
                bankDirectionsView.directionView.bankAddressLabel.text = item.bankAddress
                let placeHolderImage = #imageLiteral(resourceName: "cardBankBranch")
                var imageUrl : String?
                if let bankData : Issuer = CoreDataStack.sharedStack.fetchObject(predicateString: "cardIssuerCode == %@", inManagedObjectContext: CoreDataStack.sharedStack.mainContext,argumentList:[issuerModel.issuerCode!])!.first {
                    imageUrl = bankData.imagePath
                }
                bankDirectionsView.directionView.bankIconImageView.setImageWithUrlString(imageUrl, placeholderImage: placeHolderImage)

                currentSelectedMarker?.icon = #imageLiteral(resourceName: "bankBranchMarkerInactive")
                currentSelectedMarker = marker
                marker.icon = #imageLiteral(resourceName: "bankBranchMarkerActive")

                currentSelectedLocation = marker.position

                clusterManager.cluster()
                showBankDetailsCardView(item : item)
                return true
            }
        } else {
            print("Did tap a cluster marker")
        }

        return false
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        hideBankDetailsCardView()
    }

    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        hideBankDetailsCardView()
        return false
    }

    func focusMapToShowAllMarkers() {
        let firstLocation = markers.first!.position
        var bounds = GMSCoordinateBounds.init(coordinate: firstLocation, coordinate: firstLocation)

        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(15))
        self.mapView.animate(with: update)
    }
}

extension BankMapVC : GMUClusterManagerDelegate {

    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        return false
    }
}

extension BankMapVC : GMUClusterRendererDelegate {

    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {

        let marker = BankMarkerView()
        if let model = object as? BankClusterMarkerModel {

            var image = model.inactiveIcon
            marker.bankName = model.bankName
            marker.bankAddress = model.bankAddress
            marker.bankPhoneNumber = model.bankPhoneNumber
            if currentSelectedLocation != nil && model.position.latitude == currentSelectedLocation!.latitude && model.position.longitude == currentSelectedLocation!.longitude {
                image = model.activeIcon
                currentSelectedMarker = marker
            }
            marker.map = mapView
            marker.icon = image
        }

        return marker
    }
}

extension BankMapVC : BankDirectionViewDelegate {

    func callButtonTapped(phoneNumber: String?) {
        UserDefaultsKeys.Location.set(value: Event.Events.bankBranchFinder.rawValue)
        Utilities.checkForRatingPopUp(viewController: self)
        if let phoneNumber = phoneNumber {
            EventTracking.shared.eventTracking(name: .callBankBranch)
            Utilities.makeCall(to: phoneNumber)
        }
    }

    func directionsButtonTapped(coordinates: CLLocationCoordinate2D) {

        //http://maps.google.com/maps?f=d&daddr=Tokyo+Tower,+Tokyo,+Japan&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1
        //let directionString = "http://maps.google.com/maps?f=d&saddr=" + "\(userLocation.latitude)" + "," + "\(userLocation.longitude)" + "&daddr=" + "\(coordinates.latitude)" + "," + "\(coordinates.longitude)" + "&nav=1"
        //let directionString = "comgooglemaps://?saddr=" + "\(userLocation.latitude)" + "\(userLocation.longitude)" + "&daddr=" + "\(coordinates.latitude)" + "\(coordinates.longitude)" + "&directionsmode=transit"
        EventTracking.shared.eventTracking(name: .getDirection,[.location : Event.Events.findBanks.rawValue , .bank : issuerModel.issuerName ?? ""])
       // EventTracking.shared.eventTracking(name: .getDirectionBankBranch)
        Utilities.openGoogleMapsDirectionsUrl(from: userLocation, to: coordinates)
    }
}

// MARK:- Display Logic Conformance
extension BankMapVC: BankMapDisplayLogic {

    func bankBranchListRecieved(branchList: NearestBankBranchResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let bankData = branchList.data {
            for bankBranchData in bankData {
                let position = CLLocationCoordinate2D(latitude: NSString(string: bankBranchData.latitude!).doubleValue, longitude: NSString(string: bankBranchData.longitude!).doubleValue)

                let item = BankClusterMarkerModel(position: position)
                item.bankName = bankBranchData.bankName
                item.bankAddress = bankBranchData.address
                //item.bankLogoUrl = bankBranchData.
                item.bankPhoneNumber = bankBranchData.phone1 ?? bankBranchData.phone2
                item.inactiveIcon = #imageLiteral(resourceName: "bankBranchMarkerInactive")
                item.activeIcon = #imageLiteral(resourceName: "bankBranchMarkerActive")
                clusterManager.add(item)
                markers.append(item)
            }

            // Call cluster() after items have been added to perform the clustering and rendering on map.
            clusterManager.cluster()

            // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
            clusterManager.setDelegate(self, mapDelegate: self)
            focusMapToShowAllMarkers()
        } else {
            interactor!.isRadiusInitialValue()
        }
    }

    func errorRecieved(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }

    func showIncreaseRadiusAlert() {
        showAlert(message: Strings.BankMapScene.increaseRadiusMessage, primaryButtonTitle: "OK".localized(), nil, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
            self.interactor?.increaseRadius()
            self.interactor?.getNearestBankBranches(bankName: self.issuerModel.issuerName ?? "", latitude: NSNumber.init(value: self.userLocation.latitude), longitude: NSNumber.init(value: self.userLocation.longitude))
        }, {

        })
    }

    func userLocationDidUpdate(location : CLLocationCoordinate2D) {
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        userLocation = location
        mapView.camera = GMSCameraPosition.camera(withTarget: userLocation, zoom: 5.0)
        interactor?.getNearestBankBranches(bankName: issuerModel.issuerName ?? "", latitude: NSNumber.init(value: userLocation.latitude), longitude: NSNumber.init(value: userLocation.longitude))
    }
}

// MARK:- Configuration Logic
extension BankMapVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = BankMapInteractor()
        let presenter = BankMapPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension BankMapVC {

}
