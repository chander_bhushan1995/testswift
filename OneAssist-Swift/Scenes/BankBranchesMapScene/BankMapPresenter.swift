//
//  BankMapPresenter.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

protocol BankMapDisplayLogic: class {
    func bankBranchListRecieved(branchList: NearestBankBranchResponseDTO)
    func errorRecieved(_ error: String)
    func showIncreaseRadiusAlert()
    func userLocationDidUpdate(location : CLLocationCoordinate2D)
}

class BankMapPresenter: BasePresenter, BankMapPresentationLogic {
    weak var viewController: BankMapDisplayLogic?

    // MARK: Presentation Logic Conformance
    func responseRecieved(response: NearestBankBranchResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.bankBranchListRecieved(branchList: response!)
        } catch {
            viewController?.errorRecieved(error.localizedDescription)
        }
    }

    func showIncreaseRadiusAlert() {
        viewController?.showIncreaseRadiusAlert()
    }

    func userLocationDidUpdate(location : CLLocationCoordinate2D) {
        viewController?.userLocationDidUpdate(location : location)
    }
}
