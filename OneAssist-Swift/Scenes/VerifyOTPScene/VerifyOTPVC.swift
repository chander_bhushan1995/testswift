//
//  VerifyOTPVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit
import UserNotifications

protocol VerifyOTPBusinessLogic {
    func handleSubmitAction(request: VerifyOTPRequestDTO)
    func handleResendOTP(param: LoginMobileResponseDTO)
}

class VerifyOTPVC: BaseVC, VerifyOTPDisplayLogic {
    var interactor: VerifyOTPBusinessLogic?
    
    @IBOutlet weak var labelMobile: H1RegularBlueLabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var viewTextField: ABOTPView!
    @IBOutlet weak var buttonSubmit: OAPrimaryButton!
    @IBOutlet weak var buttonResend: TransparentBodyTextBoldButton!
    @IBOutlet weak var labelError: BodyTextRegularBlackLabel!
    
    // properties
    var dataSource: LoginMobileResponseDTO!
    var otpValue: String = ""
    var loginActionHandler: ((Bool) -> Void)? = nil //use this when login from somewhere else
    var loginFor: String?
    // timer
    var timer: Timer?
    var secondsLeft: Int = 0
    @IBOutlet weak var resendCodeLbl: UILabel!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initiliazeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpTimer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.viewTextField.resetOTPFields()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate()
    }
    
    private func initiliazeView() {
        addTap()
        
        self.setupLableError(message: nil, iserrorMessage: false)
        labelMobile.text = dataSource.mobileNo ?? ""
        
        buttonSubmit.setTitle(Strings.VerifyOTPScene.otpAction, for: .normal)
        buttonResend.setTitle(Strings.VerifyOTPScene.resendAction, for: .normal)
        
        configureOTPView()
    }
    
    private func configureOTPView(){
        ABOTPConfig.maxOTPField = 6
        ABOTPConfig.bgColor = UIColor.white
        ABOTPConfig.lineColor = .gray2
        ABOTPConfig.padding = 10
        viewTextField.commonInit()
        viewTextField.delegate = self
        buttonSubmit.isEnabled = false
        resetTimer()
    }
    
    fileprivate func setupLableError(message: String?, iserrorMessage: Bool){
        labelError.text = message
        labelError.textColor = UIColor.errorTextFieldBorder
        if !iserrorMessage {
            labelError.textColor = UIColor.seaGreen
        }
    }
    
    private func validateForm() throws {
        
    }
    
    // MARK: Button Actions
    
    @IBAction func clickedButtonEdit(_ sender: Any) {
        routeBackToLoginMobileScreen()
    }
    
    @IBAction func clickedButtonSubmit(_ sender: Any) {
        self.setupLableError(message: nil, iserrorMessage: true)
        do {
            try validateForm()
            self.view.endEditing(true)
            buttonSubmit.addLoader()
            self.setupLableError(message: nil, iserrorMessage: false)
            
            let otp = otpValue
            let request = VerifyOTPRequestDTO(username: dataSource.mobileNo ?? "", password: otp)
            interactor?.handleSubmitAction(request: request)
            
        } catch let exception as ValidationError {
            switch exception {
            case .invalidOTP(let error):
                print(error)
                self.setupLableError(message: error, iserrorMessage: true)
                break
            default:
                break
            }
        } catch {}
    }
    
    @IBAction func clickedButtonResend(_ sender: Any) {
        self.setupLableError(message: nil, iserrorMessage: false)
        resetTimer()
        Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
        EventTracking.shared.eventTracking(name: .resendOtp, withGI: true)
        interactor?.handleResendOTP(param: dataSource)
    }
    
    // MARK: Display Logic Conformance
    
    func convertIntoJSONString(object: GetQuestionAnswerResponseDTO) -> String? {
        let dictObject = Serialization.getDictionaryFromObject(object: object)
        if JSONSerialization.isValidJSONObject(dictObject) {
            if let data = try? JSONSerialization.data(withJSONObject: dictObject, options: []), let jsonString = String(data: data, encoding: .utf8) {
                return jsonString as String
            }
        }
        return nil
    }
    
    func routeToHomeTab() {
        EventTracking.shared.eventTracking(name: .OTPVerify, [.result: "success"])
        EventTracking.shared.eventTracking(name: .afEventLogin)
        Utilities.removeLoader(count: &loaderCount)
        buttonSubmit.removeLoader()
        updateReactViewProps() // update initial props of all react module opened with view controller
        if let completionHandler = loginActionHandler {
            EventTracking.shared.eventTracking(name: .customerLoggedIn,[.location: loginFor ?? "Login Screen"])
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateOffers"), object: nil)
            Utilities.topMostPresenterViewController.dismiss(animated: false){
                completionHandler(true)
                ChatBridge.updateMyAccount()
                ChatBridge.refreshHomeScreen()
            }
        } else {
            EventTracking.shared.eventTracking(name: .customerLoggedIn,[.location: "Verify Otp Screen"])
            if RemoteConfigManager.shared.rewardFeatureOn, let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, data.count > 0 {
                routeToQuestionScreen(questionsResponse: responseDTO)
            } else {
                routeToTabbar()
            }
        }
    }
    
    func displayOtpError(_ error: String) {
        Utilities.removeLoader(count: &loaderCount)
        buttonSubmit.removeLoader()
        EventTracking.shared.eventTracking(name: .OTPVerify, [.result: "Failure"])
        setupLableError(message: error, iserrorMessage: true)
    }
    
    func displayError(_ error: String, errorCode: String?){
        Utilities.removeLoader(count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayResendSuccess(_ response: LoginMobileResponseDTO) {
        dataSource = response
        Utilities.removeLoader(count: &loaderCount)
        self.setupLableError(message: Strings.VerifyOTPScene.resendSuccess, iserrorMessage: false)
        self.viewTextField.resetOTPFields()
    }
}

extension VerifyOTPVC: ABOTPViewDelegate {
    func validate(enteredOTP:String) {
        otpValue = enteredOTP
    }
    
    func isOTPValid(status: Bool) {
        self.buttonSubmit.isEnabled = status
        if status {
            self.clickedButtonSubmit(self.buttonSubmit)
        }else {
            otpValue = ""
        }
    }
}

// MARK:- Configuration Logic
extension VerifyOTPVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = VerifyOTPInteractor()
        let presenter = VerifyOTPPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension VerifyOTPVC {
    
    func routeBackToLoginMobileScreen() {
        
        let navVC = self.navigationController
        navVC?.popViewController(animated: true)
        if let loginMobileVC = navVC?.visibleViewController as? LoginMobileVC {
            loginMobileVC.editNumber = true
        }
    }
    
    func routeToTabbar() {
        setTabAsRoot()
    }
    
    func routeToQuestionScreen(questionsResponse: GetQuestionAnswerResponseDTO = GetQuestionAnswerResponseDTO(dictionary: [:])) {
        let questionAnswersList = questionsResponse.data ?? []
        let userName = UserCoreDataStore.currentUser?.userName ?? ""
        let userEmail = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? ""
        let isFilled = !(questionAnswersList.filter { $0.answered == 0 }.count == questionAnswersList.count)
        if(!isFilled && userName.isEmpty && userEmail.isEmpty) {
            showAlert(title: Constants.WelcomeAlert.welcomeText, message: Constants.WelcomeAlert.message, primaryButtonTitle: "Continue", secondaryButtonTitle: "Skip to Home", isCrossEnable: false, logoImage: UIImage(named: "ic_rewardFreeSize"), primaryAction: {
                let userRegistrationVC = UserRegisterationVC()
                
                /* 1. name and email would not be in initialProperties if they are nil.
                 2. flowType could be CompleteProfile or UnlockReward or Edit
                 3. currentBrand and currentModel should be fetched based on current logged in device.
                 4. currentScreen and totalScreens logic should be applied by keeping UserDetails screen.
                 5. cusId is required for API call.
                 6. questionAnswer should be processed JSON (i.e based on answered questions), not direct API response.
                 */
                userRegistrationVC.addToInitialProperties([
                    "user_Name": userName,
                    "user_Email": userEmail,
                    "flowType": "UnlockReward",
                    "currentBrand": "Apple",
                    "currentModel": UIDevice.currentDeviceName,
                    "currentScreenNo": 1,
                    "totalScreens": 4,
                    "customer_id": (UserCoreDataStore.currentUser?.cusId) ?? (UserCoreDataStore.currentUser?.custIds?.first ?? ""),
                    "questionAnswer": self.convertIntoJSONString(object: questionsResponse) ?? """
                    []
                    """,
                ])
                
                self.present(userRegistrationVC, animated: true, completion: nil)
                
            }) {
                self.routeToTabbar()
            }
        } else {
            routeToTabbar()
        }
    }
    
}

extension VerifyOTPVC {
    func setUpTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter(_:)), userInfo: nil, repeats: true)
    }
    @objc func updateCounter(_ theTimer: Timer?) {
        if secondsLeft > 0 {
            secondsLeft -= 1
            resendCodeLbl.isHidden = false
            buttonResend.isHidden = true
            let resendCodeIn = Strings.VerifyOTPScene.resendOtpCounter
            resendCodeLbl.text = resendCodeIn + " (\(secondsLeft)s)"
        } else if secondsLeft == 0 {
            resendCodeLbl.isHidden = true
            buttonResend.isHidden = false
            
        }
    }
    
    fileprivate func resetTimer(){
        self.buttonResend.isHidden = true
        secondsLeft = kDefaultTimeForOTP
    }
}
