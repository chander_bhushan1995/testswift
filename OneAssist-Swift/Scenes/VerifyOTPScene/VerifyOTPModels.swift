//
//  VerifyOTPModels.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

enum VerifyOTP {
	
	struct VerifyOTPRequest {
		var otpNumber: String
	}
	
	struct VerifyOTPResponse {
		
	}
}
