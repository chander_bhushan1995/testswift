//
//  VerifyOTPInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol VerifyOTPPresentationLogic {
    func otpVerified(response: VerifyOTPResponseDTO?, error: Error?, mobileNo: String)
	func otpSent(response: LoginMobileResponseDTO?, error: Error?)
}

class VerifyOTPInteractor: BaseInteractor, VerifyOTPBusinessLogic {
	
	var presenter: VerifyOTPPresentationLogic?
    
	// MARK: Business Logic Conformance
	
	func handleSubmitAction(request: VerifyOTPRequestDTO) {
        let _ = VerifyOTPRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.otpVerified(response: response, error: error, mobileNo: request.username)
        }
    }
    
	func handleResendOTP(param: LoginMobileResponseDTO) {
        let request = LoginMobileRequestDTO(mobileNo: param.mobileNo ?? "")
        let _ = LoginRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            response?.mobileNo = param.mobileNo
            self?.presenter?.otpSent(response: response, error: error)
        }
	}
}
