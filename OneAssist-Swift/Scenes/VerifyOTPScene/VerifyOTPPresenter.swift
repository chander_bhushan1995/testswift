//
//  VerifyOTPPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit
import WebEngage

protocol VerifyOTPDisplayLogic: class {
    func routeToHomeTab()
    func displayOtpError(_ error: String)
    func displayResendSuccess(_ response: LoginMobileResponseDTO)
    func displayError(_ error:String, errorCode: String?)
}

class VerifyOTPPresenter: BasePresenter, VerifyOTPPresentationLogic {
    weak var viewController: VerifyOTPDisplayLogic?
    // MARK: Presentation Logic Conformance
    
    func otpVerified(response: VerifyOTPResponseDTO?, error: Error?, mobileNo: String) {
        do {
            try checkError(response, error: error)
            if response?.status?.lowercased() == Constants.ResponseConstants.success {
                UserDefaults.standard.set(false, forKey: UserDefaultsKeys.tempCustomer.rawValue)
                if let userDtl = response?.data {
                    UserDefaultsKeys.custType.set(value: userDtl.userType ?? CustType.nonSubscriber)
                    
                    if (userDtl.userType ?? CustType.nonSubscriber) == CustType.nonSubscriber{
                        UserDefaultsKeys.isNonSubscriber.set(value: true)
                    }
                    
                    //save user detail
                    saveUserDetails(userDtl, mobileNo: mobileNo)
                    Utilities.setWebEngageUser(mobileNumber: mobileNo)
                    Utilities.setFireBaseUserId(customerId: userDtl.custId?.stringValue)
                    Utilities.setAppsFlyerUser(customerId: userDtl.custId?.stringValue)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.getMemberships()
                    }
                }
            }
        } catch {
            viewController?.displayOtpError(error.localizedDescription)
        }
    }
    
    func otpSent(response: LoginMobileResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.displayResendSuccess(response!)
        } catch let error as LogicalError {
            viewController?.displayError(error.errorDescription!, errorCode: error.errorCode)
        } catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
    
    private func saveUserDetails(_ userDetails:LoginResponse?, mobileNo:String){
        CacheManager.shared.mobileNo = mobileNo
        
        let user = UserCoreDataStore(context: CoreDataStack.sharedStack.mainContext)
        user.mobileNo = mobileNo
        user.cusId = userDetails?.custId?.stringValue
        user.userName = userDetails?.firstName
        CoreDataStack.sharedStack.saveMainContext()
        
        //save customer detail
        let obj = GetCustomerDetailsResponseDTO(dictionary: [:])
        let userInfo = UserInfo()
        userInfo.email = userDetails?.emailId
        userInfo.mobileNumber = userDetails?.mobileNum
        if let names = userDetails?.firstName?.components(separatedBy: " ") {
            userInfo.firstName = names.first
            if names.count > 1 {
                userInfo.lastName = names[1]
            }
        }
        obj.userInfos = [userInfo]
        SaveCustomerDetailsUseCase().saveCustomerDetails(obj) {(error) in }
    }
    
    private func getMemberships(){ // this API use only for save customer detail & Question Answer
        OfferSyncUseCase.shared.syncAllBookMarksFromServer()
        let group = DispatchGroup()
        
        group.enter()
        MembershipListUseCase().getMembershipForCustomerDetail {(response, error) in
            rootTabVC?.customerMembershipResponse = response
            group.leave()
        }
        
        group.enter()
        UserQuestionAnswerUseCase().getRequest(requestDto: GetQuestionAnswerRequestDTO()) { (response, error) in
            group.leave()
        }
        
        group.enter()
        UserGroupUseCase().getRequest(requestDto: UserGroupRequestDTO()) { (response, error) in
            group.leave()
        }
        
        group.notify(queue: .main) {
            Utilities.set(name: CustomerDetailsCoreDataStore.currentCustomerDetails?.firstName, email: CustomerDetailsCoreDataStore.currentCustomerDetails?.email, phone : CustomerDetailsCoreDataStore.currentCustomerDetails?.mobileNumber)
            
            self.viewController?.routeToHomeTab()
        }
    }
}
