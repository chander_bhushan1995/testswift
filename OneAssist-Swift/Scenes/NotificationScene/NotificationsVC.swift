//
//  NotificationsVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/10/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol NotificationsBusinessLogic {
    func getNotifications()
}

class NotificationsVC: BaseVC ,UITableViewDelegate,UITableViewDataSource {
    var interactor: NotificationsBusinessLogic?
    var modelObj:[NotificationsCD]?

    
    @IBOutlet weak var noNotificationView: UIView!
    
    @IBOutlet weak var bellImageView: UIImageView!
    
    @IBOutlet weak var firstLabelInNoNotification: BodyTextRegularGreyLabel!
    
    @IBOutlet weak var comebackLabel: BodyTextRegularGreyLabel!
    
    @IBOutlet weak var tableView: UITableView!
    var isSelected:[Bool] = []
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
          }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        self.title = "Notifications"
        self.interactor?.getNotifications()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Publisher.NewPushNotificationReceived.observe(observer: self, selector: #selector(newPushNotificationReceived))
        EventTracking.shared.addScreenTracking(with: .inbox)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Publisher.NewPushNotificationReceived.remove(observer: self)
        
    }
    // MARK:- private methods
    private func initializeView() {
        self.tableView.register(nib:Constants.NIBNames.notificationCell, forCellReuseIdentifier: Constants.CellIdentifiers.notificationCell)
        //self.tableView.separatorStyle = .none
       // self.comebackLabel.font = DLSFont.bodyText.regular
       // self.firstLabelInNoNotification.font = DLSFont.bodyText.regular
       // self.firstLabelInNoNotification.textColor = UIColor.lightGray
       // self.comebackLabel.textColor = UIColor.lightGray
        
        showNotificationPrompt(from: "NOTIFICATION", eventLocation: "Notification Inbox", isShowForceFully: true)
    }
    
    @objc func newPushNotificationReceived()  {
        self.interactor?.getNotifications()
    }
    // MARK:- Action Methods
    
}

// MARK:- Display Logic Conformance
extension NotificationsVC: NotificationsDisplayLogic {
    func displayNotifications(_ list: [NotificationsCD]) {
        self.modelObj = list
        if(self.modelObj?.count == 0)
        {
            self.tableView.isHidden = true
        }
        
        else
        {
            self.tableView.isHidden = false
            
            if let selected = self.modelObj?.map({($0.read?.boolValue)!}) {
                isSelected = selected
            }
        }
       // self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.reloadData()
    }
}

// MARK:- Configuration Logic
extension NotificationsVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = NotificationsInteractor()
        let presenter = NotificationsPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension NotificationsVC {
    
}



//MARK:- TABLEVIEW DATASOURCE

extension NotificationsVC{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.modelObj?.count)!
    }
    
    
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.notificationCell, for: indexPath) as! NotificationCell
        cell.initialiseView((self.modelObj?[indexPath.row])!)
        if(self.isSelected[indexPath.row] == true){
            cell.setUpForSelected()
        }
        cell.selectionStyle = .none
             return cell
    }

}
extension NotificationsVC
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if(self.isSelected[indexPath.row] == false)
        {
             self.isSelected[indexPath.row] = true
            if let model = self.modelObj?[indexPath.row] {
                model.read = NSNumber(value: true)
            }
             CoreDataStack.sharedStack.saveMainContext()
            self.tableView.reloadData()
            showAlert(title: (self.modelObj?[indexPath.row].title)!, message: (self.modelObj?[indexPath.row].message)!)

        }
               if let model = self.modelObj?[indexPath.row]{
            
            if let userInfoData = model.userInfo  {
                let userInfo:[String : Any] = NSKeyedUnarchiver.unarchiveObject(with: userInfoData) as! [String : Any]
                DeepLinkManager.handlePushNotification(userInfo, isCameFromNotification: false)
            }
        }
    }
}
