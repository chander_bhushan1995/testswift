//
//  NotificationsInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/10/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit
import CoreData

protocol NotificationsPresentationLogic {
    func getNotifications(_ list:[NotificationsCD])
}

class NotificationsInteractor: BaseInteractor, NotificationsBusinessLogic {
    var presenter: NotificationsPresentationLogic?
    var list:[NotificationsCD]?
    // MARK: Business Logic Conformance
    
   public func getNotifications()
    {
        let context = CoreDataStack.sharedStack.mainContext
        var returnedList : [NotificationsCD]? = []
        
        let fetch = NSFetchRequest<NotificationsCD>(entityName: NSStringFromClass(NotificationsCD.self))
        let sort = NSSortDescriptor(key: #keyPath(NotificationsCD.creationDate), ascending: false)
        fetch.sortDescriptors = [sort]
        var result: [NotificationsCD]?
        do {
            result = try context.fetch(fetch)
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        
        if let list: [NotificationsCD] = result
        {
           returnedList = list
        }
        self.presenter?.getNotifications(returnedList!)
    }

   
}
