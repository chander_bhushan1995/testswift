//
//  NotificationsPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/10/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol NotificationsDisplayLogic: class {
    func displayNotifications(_ list:[NotificationsCD])
}

class NotificationsPresenter: BasePresenter, NotificationsPresentationLogic {
    weak var viewController: NotificationsDisplayLogic?
    
    
      func getNotifications(_ list:[NotificationsCD]){
        self.viewController?.displayNotifications(list)
        }
    
    // MARK: Presentation Logic Conformance
    
}
