//
//  CardOptionsVC.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class CardOptionsVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [CardOptionsModel]!
    var containerSize = CGSize(width: UIScreen.main.bounds.width, height: 320)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    private func initializeView() {
        tableView.dataSource = self
        tableView.register(nib: Constants.NIBNames.imageTitle, forCellReuseIdentifier: Constants.CellIdentifiers.cardOption)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = CGFloat(Constants.CellHeight.cardOptionCell)
        tableView.allowsSelection = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.flashScrollIndicators()
    }
}

extension CardOptionsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == dataSource.count - 1 {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: tableView.bounds.width)
        } else {
            cell.separatorInset = .zero
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.cardOption, for: indexPath) as! ImageTitleCell
        
        let viewModel = dataSource[indexPath.row]
        cell.set(text: viewModel.title)
        
        return cell
        
    }
}

extension CardOptionsVC: PopoverPresented {}
