//
//  ActivateVoucherSuccessVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 05/05/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class ActivateVoucherSuccessVC: BaseVC, HideNavigationProtocol {
    
    @IBOutlet weak var awesomeLabel: H3BoldLabel!
    @IBOutlet weak var successfulActivationLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var nextStepLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var continueLoginLabel: BodyTextRegularGreyLabel!
    @IBOutlet weak var continueButton: OAPrimaryButton!
    
    var categoryCodes:[String]? = []
    var token: String!
    var customerId: String?
    var memPhoneNo: String = ""
    var brand = ""
    var uploadDocsLastDate = ""
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnContinue(_ sender: Any) {
        if !userMobileNumber.isEmpty && (memPhoneNo.isEmpty || userMobileNumber == memPhoneNo) {
            saveUserAndRouteToTabBar()
        } else {
            Utilities.forceUserLogout(memPhoneNo)
        }
    }
    
    // MARK:- private methods
    private func initializeView() {
        UserDefaultsKeys.Location.set(value: Event.EventParams.activation)
        awesomeLabel.textColor = .seaGreen
        continueLoginLabel.text = Strings.VerifyVoucherSuccessScene.continueLoginWith + memPhoneNo
        if brand.isEmpty || brand.lowercased() == "null" {
            successfulActivationLabel.text = "\(Strings.VerifyVoucherSuccessScene.voucherSuccess) \(Strings.VerifyVoucherSuccessScene.device)"
        } else {
            successfulActivationLabel.text = "\(Strings.VerifyVoucherSuccessScene.voucherSuccess) \(brand) \(Strings.VerifyVoucherSuccessScene.device)"
        }
        
        if uploadDocsLastDate.isEmpty || uploadDocsLastDate.lowercased() == "null" {
            nextStepLabel.isHidden = true
        } else {
            let lastDocUploadDate = Date.getDate(from: uploadDocsLastDate).ordinalDate()
            nextStepLabel.text = "\(Strings.VerifyVoucherSuccessScene.uploadDocument) \(lastDocUploadDate) \(Strings.VerifyVoucherSuccessScene.avoidCancellation)"
            nextStepLabel.isHidden = false
        }
        
        if !userMobileNumber.isEmpty && (memPhoneNo.isEmpty || userMobileNumber == memPhoneNo) {
            continueButton.setTitle(Strings.Global.activateNow, for: .normal)
            continueLoginLabel.removeFromSuperview()
        } else {
            continueButton.setTitle(Strings.VerifyVoucherSuccessScene.continue, for: .normal)
            if memPhoneNo.isEmpty {
                continueLoginLabel.removeFromSuperview()
            }
        }
    }
    
    private func saveUserAndRouteToTabBar() {
        if let user = UserCoreDataStore.currentUser {
            if UserDefaults.standard.bool(forKey: UserDefaultsKeys.tempCustomer.rawValue) {
                user.userCustIds = "\(user.userCustIds ?? ""), \(customerId ?? "")"
            }
        } else {
            let user = UserCoreDataStore(context: CoreDataStack.sharedStack.mainContext)
            user.cusId = customerId
            user.sessionId = ""//UserDefaults.standard.object(forKey: UserDefaultsKeys.sessionId.rawValue) as? String
            user.mobileNo = userMobileNumber
            user.userName = CacheManager.shared.userName
            CacheManager.shared.userName = nil
            CoreDataStack.sharedStack.saveMainContext()
        }
        
        routeToTabbar()
    }
}

// MARK:- Routing Logic
extension ActivateVoucherSuccessVC {
    func routeToTabbar() {
        setTabAsRootNew()
    }
}
