//
//  ActivateVoucherVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 04/05/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class ActivateVoucherVC: SecureWebViewVC {
    
    fileprivate var isVCPresented = false
    
    // MARK:- private methods
    override func initializeView() {
        title = Strings.VerifyVoucherScene.title
        urlString = userMobileNumber.isEmpty ? Constants.Urls.activateVoucher : Constants.Urls.activateVoucher + "&loggedInNo=\(userMobileNumber)"
        if let url = URL(string: urlString) {
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url))
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        } else {
            showAlert(message: Strings.Global.somethingWrong)
        }
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("URL Finished: \(webView.url?.absoluteString ?? "")")
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        let urlStr = webView.url?.absoluteString
        if let urlStr = urlStr {
            let str = urlStr.lowercased()
            if str.contains(Constants.PaymentURLs.whcActivateVoucherSuccess) {
                EventTracking.shared.addScreenTracking(with: .activateVoucher)
                CacheManager.shared.purchaseFlowFor = .home  //change
                if isVisible, isTopViewController {
                    EventTracking.shared.eventTracking(name: .planActivated, [.result: Event.EventParams.success, .type: Event.EventParams.whc])
                }
                if let value = URLComponents(string: urlStr)?.queryItems?.filter({$0.name == "activationCode"}).first?.value {
                    routeToWhcSuccessScreen(activationCode: value)
                }
            } else if str.contains(Constants.PaymentURLs.activateVoucherSuccess) {
                let queryItems = URLComponents(string: urlStr)?.queryItems
                if let relNo = queryItems?.filter({$0.name == "relno"}).first?.value {
                    let categoryCodes = queryItems?.filter({$0.name == "categoryCode"}).first?.value
                    let memPhoneNo = queryItems?.filter({$0.name == "memPhoneNo"}).first?.value ?? ""
                    let brand = queryItems?.filter({$0.name == "brand"}).first?.value ?? ""
                    let uploadDocsLastDate = queryItems?.filter({$0.name == "uploadDocsLastDate"}).first?.value ?? ""
                    if isVisible, isTopViewController {
                        EventTracking.shared.eventTracking(name: .planActivated, [.result: Event.EventParams.success, .type: categoryCodes ?? []])
                    }
                    routeToPaymentSuccessScreen(relationshipNumber: relNo, categoryCodes: categoryCodes, memPhoneNo: memPhoneNo, brand: brand, uploadDocsLastDate: uploadDocsLastDate)
                }
            } else if str.contains(Constants.PaymentURLs.activateVoucherFailed) {
                showAlert(message: Strings.PaymentWebViewScene.paymentFailed)
            }
        }
    }
}

// MARK:- Routing Logic
extension ActivateVoucherVC {
    func routeToWhcSuccessScreen(activationCode: String) {
        guard !isVCPresented else { return }
        isVCPresented = true
        let successVC = WhcSuccessVC()
        successVC.activationCode = activationCode
        EventTracking.shared.addScreenTracking(with: .activateVoucher)
        navigationController?.pushViewController(successVC, animated: false)
    }
    
    func routeToPaymentSuccessScreen(relationshipNumber: String, categoryCodes:String? = nil, memPhoneNo: String, brand: String, uploadDocsLastDate: String) {
        guard !isVCPresented else { return }
        isVCPresented = true
        let activateVoucherSuccessVC = ActivateVoucherSuccessVC()
        activateVoucherSuccessVC.customerId = relationshipNumber
        activateVoucherSuccessVC.memPhoneNo = memPhoneNo
        activateVoucherSuccessVC.brand = brand
        activateVoucherSuccessVC.uploadDocsLastDate = uploadDocsLastDate
        if let catCodes = categoryCodes?.components(separatedBy: ",") {
            activateVoucherSuccessVC.categoryCodes = catCodes
        }
        UserDefaults.standard.set("Activation", forKey: "Location")
        navigationController?.pushViewController(activateVoucherSuccessVC, animated: true)
    }
}
