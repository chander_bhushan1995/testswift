//
//  BasePresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 24/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import Foundation

enum APIResponseType {
    case buybackResponse
    case walletplansResponse
    case offersResponse
    case membershipResponse
}

struct LogicalError: LocalizedError {
    
    private var message: String
    
    init(_ message: String,code : String? = nil) {
        self.message = message
        self.errorCode = code
    }
    var errorCode : String?
    var errorDescription: String? {
        return message
    }
    
}

class BasePresenter {
    
    func checkError(_ response: BaseResponseDTO?, error: Error? ,interruptErrorHandler : ((_ error : ApiResponseError)-> Bool)? = nil) throws {
         if let error = response?.error, response?.status?.lowercased() == Constants.ResponseConstants.failure {
            if let errorObj = error.first {
                if let errorHandler = interruptErrorHandler {
                    if errorHandler(errorObj){
                        return;
                    }
                }
            }
        }
        
        try Utilities.checkError(response, error: error)
    }
}
