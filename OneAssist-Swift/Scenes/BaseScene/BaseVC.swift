//
//  BaseVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 24/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    var loaderCount = 0
    var fromScreen: FromScreen = .blank
    
    fileprivate var gestureRecognizer: UITapGestureRecognizer?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        //self.hidesBottomBarWhenPushed = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.hidesBottomBarWhenPushed = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom, bottomSafeArea > 0 {
            for subview in view.subviews {
                if let subview = subview as? UITableView {
                    subview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
                }
            }
        }
        
        edgesForExtendedLayout = []
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.extendedLayoutIncludesOpaqueBars = true
        print("Screen Name \(self)")
        showNavigationShadow()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
}

extension BaseVC {
    
    func addTap() {
        if let gestureRecog = gestureRecognizer {
            view.removeGestureRecognizer(gestureRecog)
        }
        
        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseVC.tapped(tap:)))
        gestureRecognizer?.cancelsTouchesInView = false
        view.addGestureRecognizer(gestureRecognizer!)
    }
    
    @objc func removeTap() {
        guard let gestureRecognizer = gestureRecognizer else {
            return
        }
        
        view.removeGestureRecognizer(gestureRecognizer)
        self.gestureRecognizer = nil
    }
    
    @objc func tapped(tap: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func addLoader() {
        removeLoader()
        UIApplication.shared.beginIgnoringInteractionEvents()
        UIApplication.shared.delegate?.window??.addSubview(Utilities.loaderView)
    }
    
    func removeLoader() {
        if UIApplication.shared.isIgnoringInteractionEvents{
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        Utilities.loaderView.removeFromSuperview()
    }
    
    func presentShareActivityView(shareObject: Any) {
        Utilities.showShareActionSheet(viewController: self, dataToShare: [shareObject])
    }
}


extension UIViewController {
    
    func setTabAsRoot() {
        ChatBridge.setRootWithVC(TabVC())
    }
    
    func setTabAsRootNew() {
        // This method will be called in place of setTabAsRoot() to save memory and complete loading of app.
        if let navVC = rootNavVC, rootTabVC != nil {
            if let presentedViewController = navVC.topViewController?.presentedViewController {
                presentedViewController.dismiss(animated: false, completion: {
                    ChatBridge.routeToMembershipTab()
                })
            } else {
                ChatBridge.routeToMembershipTab()
            }
        } else {
            // This is fallback case.
            CacheManager.shared.selectedTabIndex = .membership
            setTabAsRoot()
        }
    }
    
    func popToRootView() -> UIViewController? {
        if let navVC = rootNavVC {
            navVC.popToRootViewController(animated: false)
            return navVC.viewControllers.first
        }
        return nil
    }
    
    var isPresented: Bool {
        if let index = navigationController?.viewControllers.firstIndex(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else {
            return false
        }
    }
    
    func hideNavigationShadow() {
        self.navigationController?.navigationBar.layer.cornerRadius = 0
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.clear.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 1
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.navigationController?.navigationBar.layer.shadowRadius = 0.0
    }
    
    func showNavigationShadow() {
        self.navigationController?.navigationBar.layer.cornerRadius = 6
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.dlsShadow.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 1
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.navigationController?.navigationBar.layer.shadowRadius = 4
    }
    
    func showPopupForCurrectMobileNumber(title: String = Strings.NumberVerifyAlerts.deeplinkLogin.message, subTitle: String = Strings.NumberVerifyAlerts.deeplinkLogin.detailMsg,forScreen: String?, ctaText: String = Strings.NumberVerifyAlerts.loginButtonText, completionblock: @escaping (_ isverified: Bool) -> Void) {
            let loginSheet = LoginBottomSheet()
            var model = LoginSheetModel()
            model.title = title
            model.detail = subTitle
            model.ctaText = ctaText
            loginSheet.setUpValue(model) { (status) in
                Utilities.topMostPresenterViewController.dismiss(animated: false)
                Utilities.forceUserLogout("", showBackActionInLogin: true)
                completionblock(status)
            }
            let basePickerVC = BasePickerVC(withView:loginSheet, height:LoginBottomSheet.height(forModel: model))
            basePickerVC.dismissCallback = {
                completionblock(false)
            }
           Utilities.topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    func showPopupForVerifyNumber(title: String?, subTitle: String?,forScreen: String?, ctaText: String = Strings.NumberVerifyAlerts.buttonText, completionblock: @escaping (_ isverified: Bool) -> Void) {
        if forScreen?.uppercased() == "CHAT"{
            ChatBridge.selectedActionType(type: .CHAT)
        }
        if Utilities.isCustomerVerified() {
            completionblock(true)
        }else {
            let loginSheet = LoginBottomSheet()
            var model = LoginSheetModel()
            model.title = title
            model.detail = subTitle
            model.ctaText = ctaText
            
            loginSheet.setUpValue(model) { (status) in
                Utilities.topMostPresenterViewController.dismiss(animated: false)
                if status {
                    self.showLoginView(forScreen: forScreen, completionblock: completionblock)
                }
            }
            Utilities.presentPopover(view: loginSheet, height: LoginBottomSheet.height(forModel: model))
        }
    }
    
    func showLoginView(forScreen: String?,completionblock: @escaping (_ isverified: Bool) -> Void) {
        let vc = LoginMobileVC()
        vc.loginFor = forScreen
        vc.setUpLoginAction(block: completionblock)
        presentInFullScreen(BaseNavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    
    func showNotificationPrompt(from screenName: String? = nil, eventLocation: String?, isShowForceFully: Bool = false) {
        if let pushStatus = sharedAppDelegate?.pushPermissionStatus, pushStatus == true {
            return
        }
        let showNotificationPrompt = RemoteConfigManager.shared.showNotificationPrompt
        let daysCap = RemoteConfigManager.shared.notificationAlertDaysCap
        if showNotificationPrompt == true {
            if isShowForceFully == true {
                let vc = NotificationPromptVC()
                vc.eventLocation = eventLocation
                vc.screenName = screenName
                self.presentInFullScreen(vc, animated: true, completion: nil)
                
            }else{
                if let date = AppUserDefaults.pushPromptShowDate {
                   // let datevalue = Date(timeIntervalSince1970: TimeInterval(Int(date)))
                    let diff = gregorianCalendar.dateComponents([.day], from: date, to: Date()).day
                    if (diff ?? 0) >= (Int(daysCap) ?? 0) {
                        let vc = NotificationPromptVC()
                        vc.eventLocation = eventLocation
                        vc.screenName = screenName
                        self.presentInFullScreen(vc, animated: true)
                    }
                    
                }else {
                    let vc = NotificationPromptVC()
                    vc.eventLocation = eventLocation
                    vc.screenName = screenName
                    self.presentInFullScreen(vc, animated: true, completion: nil)
                }
            }
            
            
        }else {
            sharedAppDelegate?.enablePushNotification(UIApplication.shared)
        }
    }
    
    func routeToViewController(_ viewController: UIViewController, animated: Bool) {
        if let tabVC = tabBarController {
            tabVC.navigationController?.pushViewController(viewController, animated: true)
        } else {
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func updateReactViewProps() {
        if let reactNavVC = rootNavVC { // update all react module's initial props
            reactNavVC.viewControllers.forEach { vc  in
                if let reactVC  = vc as? ReactMainVC {
                    reactVC.updateProps()
                }
            }
        }
    }
}
