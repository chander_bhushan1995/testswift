//
//  BaseNavigationController.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/4/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol HideNavigationProtocol { }

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        // self.navigationItem.title = "Back";
    }
}

extension BaseNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
         if let timelineVC = viewController as? TimelineBaseVC {
            switch timelineVC.serviceRequestSourceType {
            case "SOD":
                navigationController.setNavigationBarHidden(true, animated: animated)
                return
            default: break
            }
        }
        navigationController.setNavigationBarHidden(viewController is HideNavigationProtocol, animated: animated)
    }
}
