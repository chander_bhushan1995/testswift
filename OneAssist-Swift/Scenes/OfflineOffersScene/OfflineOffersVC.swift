//
//  OfflineOffersVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfflineOffersBusinessLogic {
    func getOffers(filterName: String?, pageNo: Int, cards: [Card],filterbankCards: [Card], onBoardingCards: [Card], location: CLLocation, radius: Int)
    
}

class OfflineOffersVC: BaseVC, ChildOffers {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var emptyLabel: H3BoldLabel!
    
    weak var childDelegate: ChildViewDelegate?
    
    var interactor: OfflineOffersBusinessLogic?
    
    var userCards: [Card] = []
    var filterBankCards: [Card] = []
    var onBoardingCards: [Card] = []
    var sectionList: [[BankCardOffers]]?
    
    var filterName: String?
    var changedFilterName: String?
    
    var pageNo: Int = 1
    var loadMoreRow = 0
    var selectedIndex:Int  = 0;
    var hasFirstResponse: Bool = false
    var bankFilterApply: Bool = false
    var apiInProgress: Bool = false
    var askedForLocationPermission = false
    var selectedBookMarkOfferCode : String? = nil // use when non-login and click bookmark
    var selectedBookMarkOutletCode: String? = nil

    private var locationManager = Permission.shared.locationManager
    var selectedDistance = Constants.OfferDistance.km10Plus
    
    var distances: [CircularCollectionViewModel] = {
        let model1 = CircularCollectionViewModel(selected: false, title: Strings.OfflineOffersScene.km2, value: Constants.OfferDistance.km2, eventName: .firstDistanceTapped)
        let model2 = CircularCollectionViewModel(selected: false, title: Strings.OfflineOffersScene.km5, value: Constants.OfferDistance.km5, eventName: .secondDistanceTapped)
        let model3 = CircularCollectionViewModel(selected: false, title: Strings.OfflineOffersScene.km10, value: Constants.OfferDistance.km10, eventName: .thirdDistanceTapped)
        // TODO: - km
        let model4 = CircularCollectionViewModel(selected: true, title: Strings.OfflineOffersScene.km10plus, value: Constants.OfferDistance.km10Plus, eventName: .fourthDistanceTapped)
        return [model1,model2,model3,model4]
    }()
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    deinit {
        Permission.shared.resetData()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(bookmarkStatusChanged(_:)), name: Notification.Name("BookmarkStatusChanged"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        EventTracking.shared.eventTracking(name: .offlineOffer)
        if !askedForLocationPermission {
            
            setupLocationManager()
            filterName = changedFilterName
            return
        }
        
        if checkForRefreshOffers() {
            invalidateData()
            getOffers()
        }
    }
    
    func setupLocationManager() {
        Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.nearbyOffer) {[weak self] (status) in
            guard let self = self else {return}
            if status == .authorizedAlways || status == .authorizedWhenInUse {
                self.locationManager.delegate = self
                self.locationManager.distanceFilter = kCLDistanceFilterNone
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.startUpdatingLocation()
                
            }else if status ==  .notDetermined{
            
            }else {
                self.showOpenSettingsAlert(title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting)
            }
        }
    }
    
    func checkForRefreshOffers() -> Bool {
        if (filterName != changedFilterName || self.bankFilterApply == true) && Permission.shared.location != nil {
            filterName = changedFilterName
            return true
        }
        return false
    }
    
    func invalidateData() {
        self.emptyLabel.text = nil
               self.emptyLabel.isHidden = true
        loadMoreRow = 0
        sectionList = nil
        pageNo = 1
        tableView.reloadData()
    }
    
    func getOffers() {
        if let currentLocation = Permission.shared.location {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
            interactor?.getOffers(filterName: changedFilterName, pageNo: pageNo, cards: userCards, filterbankCards: filterBankCards.count>0 ? filterBankCards : [], onBoardingCards: onBoardingCards, location: currentLocation, radius: selectedDistance)
        }
    }
    
   
    //MARK: - helper methods
    func fetchFilteredOffers(filterName: String?) {
        changedFilterName = filterName
    }
    
    func updateWallet(cards: [Card]) {
       self.userCards = cards
       invalidateData()
       getOffers()
     }
    
    func fetchBankFilteredOffers(filterBankCards: [Card])-> Bool {
        if self.filterBankCards.containsSameElements(as: filterBankCards) {
            self.bankFilterApply = false
            
        }else {
            self.filterBankCards = filterBankCards
            self.bankFilterApply = true
        }
        return self.bankFilterApply
    }
    

    @objc func bookmarkStatusChanged(_ notification: NSNotification) {
       
        if let offerCode = notification.userInfo?[Constants.KeyValues.offerCode] as? String, let outletCode = notification.userInfo?[Constants.KeyValues.outletCode] as? String, let status = notification.userInfo?[Constants.KeyValues.status] as? String {
            if status == "1" {
               selectedBookMarkOfferCode = offerCode
               selectedBookMarkOutletCode = outletCode
            }
            

            guard let sectionList = self.sectionList else {
                return
            }
            for onlineOffers in sectionList {
                if let offer = onlineOffers.first(where: {$0.offerCode == offerCode && $0.outletCode == outletCode}) {
                    offer.isBookmarked = status == "1" ? true : false
                    self.tableView.reloadData()
                    break
                }
            }
        }
    }
    
    // MARK:- private methods
    private func initializeView() {
        view.backgroundColor = UIColor.whiteSmoke
        
       // filterBankCards = userCards
        
        collectionView.register(nib: Constants.CellIdentifiers.circularCollectionCell, forCellReuseIdentifier: Constants.CellIdentifiers.circularCollectionCell)
        
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.OfferHeaderView)
        tableView.register(nib: Constants.CellIdentifiers.offerLoadMoreCell, forCellReuseIdentifier: Constants.CellIdentifiers.offerLoadMoreCell)
        tableView.register(nib: Constants.CellIdentifiers.offerListCell, forCellReuseIdentifier: Constants.CellIdentifiers.offerListCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 440
    }
    
    // MARK:- Action Methods
    
}

// MARK:- Collection Datasource Conformance
extension OfflineOffersVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return distances.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.circularCollectionCell, for: indexPath) as! CircularCollectionCell
        cell.set(model: distances[indexPath.row])
        return cell
    }
}

// MARK:- Collection Delegate Conformance
extension OfflineOffersVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard !distances[indexPath.row].selected else {
            return
        }
        
        distances = distances.map({ (model) -> CircularCollectionViewModel in
            var model = model
            model.selected = false
            return model
        })
        
        for tup in distances.enumerated() {
            if tup.offset == indexPath.row {
                var model = tup.element
                model.selected = true
                distances[tup.offset] = model
            }
        }
        
        collectionView.reloadData()
        
        EventTracking.shared.eventTracking(name: distances[indexPath.row].eventName)
        
        selectedDistance = distances[indexPath.row].value
        invalidateData()
        getOffers()
    }
}

// MARK:- Collection Flow Layout Delegate Conformance
extension OfflineOffersVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let vw = (self.view.frame.width - 40) / 4
        return CGSize(width: vw, height: 30)
    }
}


// MARK:- Table Datasource Conformance
extension OfflineOffersVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionList = self.sectionList else {
            return 0
        }
        return sectionList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionList = self.sectionList else {
            return 0
        }
        if section == sectionList.count-1 {
            return sectionList[section].count + loadMoreRow
        }
        return sectionList[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let sectionList = self.sectionList else {
            return UITableViewCell()
        }
        if indexPath.section == sectionList.count-1 {
            if indexPath.row == sectionList[indexPath.section].count {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.offerLoadMoreCell, for: indexPath) as! OfferLoadMoreCell
                cell.actIndicator.startAnimating()
                cell.actIndicator.isHidden = false
                return cell
            }
        }
        
        let onlineOffers = sectionList[indexPath.section]
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.offerListCell, for: indexPath) as! OfferListCell
        cell.delegate = self
        cell.configureCell(model: onlineOffers[indexPath.row], for: indexPath)
        return cell
    }
}


// MARK:- Table Delegate Conformance
extension OfflineOffersVC: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        childDelegate?.childViewScrolled(childScrollView: scrollView)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let sectionList = self.sectionList else {
            return
        }
        
        if (cell is OfferListCell) {
          let onlineOffers = sectionList[indexPath.section]
          let cellObj = (cell as! OfferListCell)
          let offer = onlineOffers[indexPath.row]
          if selectedBookMarkOfferCode != nil && selectedBookMarkOutletCode != nil && offer.offerCode == selectedBookMarkOfferCode && offer.outletCode == selectedBookMarkOutletCode {
              offer.isBookmarked = true
              selectedBookMarkOfferCode = nil
              selectedBookMarkOutletCode = nil
          }
          cellObj.configureCell(model: offer, for: indexPath)
        }

        
        
        if indexPath.section == sectionList.count-1 {
            if indexPath.row == sectionList[indexPath.section].count {
                if !apiInProgress, let currentLocation = Permission.shared.location {
                    apiInProgress = true
                    interactor?.getOffers(filterName: changedFilterName, pageNo: pageNo, cards: userCards, filterbankCards: filterBankCards.count>0 ? filterBankCards : [], onBoardingCards: onBoardingCards, location: currentLocation, radius: selectedDistance)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let sectionList = self.sectionList else {
            return
        }
        if indexPath.section == sectionList.count-1 {
            if indexPath.row == sectionList[indexPath.section].count {
                return
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
        let onlineOffers = sectionList[indexPath.section]
        routeToOfferDetail(offer: onlineOffers[indexPath.row])
        selectedIndex = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let sectionList = self.sectionList, sectionList.count == 2 else {
            return 0.01
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionList = self.sectionList, sectionList.count == 2 else {
            return UIView()
        }
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.OfferHeaderView) as! OfferHeaderView
        if section == 0 {
            headerView.titleLabel.text = "OFFER ON YOUR CARD" + (userCards.count>1 ? "S" : "")
            
        }else {
            headerView.titleLabel.text = "OTHER OFFERS"
            
        }
        return headerView
    }
    
}

// MARK:- Offer List Cell Delegate Conformance
extension OfflineOffersVC: OfferListCellDelegate {
    func clickedBookmark(for indexPath: IndexPath?) {
        self.tableView.reloadData()
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.bookmarkOffer.message, subTitle:nil, forScreen: "Bookmark Offer") {[unowned self] (status) in
            if status {
              guard let sectionList = self.sectionList, let selectionIndexPath = indexPath else {
                  return
              }
              let onlineOffers = sectionList[selectionIndexPath.section]
                let offer = onlineOffers[selectionIndexPath.row]
              if offer.isBookmarked == false {
                  EventTracking.shared.eventTracking(name: .markFavourite ,
                                                     [.merchantName: offer.merchantName ?? "",
                                                      .type: Event.EventParams.offline,
                                                      .position: NSNumber.init(value:selectionIndexPath.row),
                                                      .expiryDate: (DateFormatter.serverDateFormat.date(from: offer.endDate ?? "") ?? ""),
                                                      .location: FromScreen.fromAllOffers.rawValue,
                                                      .offerID:offer.offerId ?? ""])
                  if Utilities.offersFavoritedCount() > 1 {
                      DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                          guard let self = self else { return }
                          UserDefaultsKeys.Location.set(value: Event.EventParams.favouriteOffer)
                          Utilities.checkForRatingPopUp(viewController: self)
                      }
                  }
              }
              offer.isBookmarked = !offer.isBookmarked
              OfferSyncUseCase.shared.storeOfferInDb(offer: offer)
            }
        }
    }
}

// MARK:- Offer Detail Delegate Conformance
extension OfflineOffersVC: OfferDetailBookmarkDelegate {
    
    func clickedDetailBookmark() {
       // self.tableView.reloadData()
    }
}


// MARK:- Location Delegate Conformance
extension OfflineOffersVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        if locations.count > 0 {
            Permission.shared.location = locations[0]
            getOffers()
        }
        askedForLocationPermission = true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        if Platform.isSimulator {
            Permission.shared.location = CLLocation(latitude: 28.459497, longitude: 77.026634)
            getOffers()
            
        }else {
            
        }
        
        askedForLocationPermission = true
    }
}

// MARK:- Display Logic Conformance
extension OfflineOffersVC: OfflineOffersDisplayLogic {
    
    func displayOnlineOffers(sectionList: [[BankCardOffers]] , noOfRows: Int) {
        self.sectionList = sectionList
        self.loadMoreRow = noOfRows
        Utilities.removeLoader(count: &loaderCount)
        hasFirstResponse = true
        bankFilterApply = false
        pageNo = pageNo + 1
        self.tableView.reloadData()
        apiInProgress = false
        if let list = self.sectionList, list.count > 0 {
            self.emptyLabel.text = nil
            self.emptyLabel.isHidden = true
        }
    }
    
    func displayOnlineOffersError(_ error: String, pageNo: Int) {
        if pageNo == 1 {
            Utilities.removeLoader(count: &loaderCount)
            hasFirstResponse = true
            bankFilterApply = false
            showAlert(message: error)
            if let list = self.sectionList, list.count > 0 { }else {
                       self.emptyLabel.text = error
                       self.emptyLabel.isHidden = false
                   }
        } else {
            self.loadMoreRow = 0
            self.tableView.reloadData()
        }
        
        apiInProgress = false
    }
}

// MARK:- Configuration Logic
extension OfflineOffersVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = OfflineOffersInteractor()
        let presenter = OfflineOffersPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension OfflineOffersVC {
    
    func routeToOfferDetail(offer: BankCardOffers) {
        let vc = OfflineOfferDetailVC()
        vc.delegate = self
        vc.fromScreen = .fromAllOffers
        vc.index = selectedIndex
        vc.selectedOffer = offer
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
