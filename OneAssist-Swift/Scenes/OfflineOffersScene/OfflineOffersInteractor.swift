//
//  OfflineOffersInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfflineOffersPresentationLogic {
    func recievedOffersResponse(card:[Card],  response: OfflineCardOffersResponseDTO?, error: Error?, pageNo: Int)
}

class OfflineOffersInteractor: BaseInteractor, OfflineOffersBusinessLogic {
    var presenter: OfflineOffersPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getOffers(filterName: String?, pageNo: Int, cards: [Card],filterbankCards: [Card], onBoardingCards: [Card], location: CLLocation, radius: Int){
//        let otherCars = filterbankCards.filter({ filtercard in
//            !cards.contains(where: { usercard in
//                (filtercard.cardIssuerCode == usercard.cardIssuerCode && filtercard.cardTypeId == usercard.cardTypeId)
//            })
//        })
//        let addedCars = filterbankCards.filter({ filtercard in
//            cards.contains(where: { usercard in
//                (filtercard.cardIssuerCode == usercard.cardIssuerCode && filtercard.cardTypeId == usercard.cardTypeId)
//            })
//        })
        
        let otherCars = filterbankCards.map{ Card(card: $0) }
        
        var addedCars: [Card] = []
        if otherCars.count <= 0 {
            addedCars = onBoardingCards.filter{element in
                return !cards.contains(where: {element.cardTypeId == $0.cardTypeId && element.cardIssuerCode == $0.cardIssuerCode})
            } + cards
        }
        
        let request = OfflineCardOffersRequestDTO()
        let cusId = Int(UserCoreDataStore.currentUser?.cusId ?? "0")
        request.customerId = NSNumber(integerLiteral: cusId ?? 0)
        request.longitude = NSNumber(value: location.coordinate.longitude.rounded(toPlaces: 5))
        request.latitude = NSNumber(value: location.coordinate.latitude.rounded(toPlaces: 5))
        request.pageNo = NSNumber(integerLiteral: pageNo)
        request.pageSize = NSNumber(integerLiteral: kPageSizeForOffer)
        request.category = filterName ?? ""
       // request.cards = filterbankCards.map{ Card(card: $0) }
        request.addedCards = addedCars
        request.otherCards = otherCars
        request.maxDistance = NSNumber(integerLiteral: radius)
        
        let _ = OfflineCardOffersRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedOffersResponse(card:cards, response: response, error: error, pageNo: pageNo)
        }
    }

}
