//
//  OAConfirmationView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 14/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OAConfirmationView: UIView, NibInstantiable {
    
    var ratingViewController: UIViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
        
    }
    private func commonInit(){
        loadNibContent()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func clickedConfirmationViewNegativeButton(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .notNow, [.location: "Timeline"])
        Utilities.topMostPresenterViewController.removeOABottomView(self, self)
        if let vc = ratingViewController {
            if let timelineVC = (vc as? TimelineBaseVC) {
                timelineVC.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func clickedConfirmationViewPositiveButton(_ sender: Any) {
        
        EventTracking.shared.eventTracking(name: .rateNow, [.location: "Timeline"])
        Utilities.topMostPresenterViewController.removeOABottomView(self,self)
        
        if let vc = ratingViewController {
            if let timelineVC = (vc as? TimelineBaseVC) {
                timelineVC.navigationController?.popViewController(animated: true)
            }
        }
        Utilities.openReview()
    }
}
