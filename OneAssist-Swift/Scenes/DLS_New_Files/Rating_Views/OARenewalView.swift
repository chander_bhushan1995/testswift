//
//  OARenewalView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 14/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OARenewalView: UIView, NibInstantiable {
    
//    @IBOutlet var renewalView: UIView!
    @IBOutlet weak var savedAmountLabel: H3RegularBlackLabel!
    @IBOutlet weak var expiringLabel: BodyTextRegularGreyLabel!
    
//    var topVC:UIViewController!
    
    var claimAmount: Float?
    var membership: CustomerMembershipDetails?
    var subCategories: [HomeApplianceSubCategories] = []

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
        
    }
    
    private func commonInit(){
        loadNibContent()
        //        configureView()
    }

    @IBAction func renewNowTapped(_ sender: PrimaryButton) {
        // Need to route to pincode screen
        EventTracking.shared.eventTracking(name: .renew , [.location: Strings.Global.serviceTimeline])
        if let viewController = BuyPlanJourneyVC.routeToRenewalFlow(with: PlanDetailAndSRModel(membership!, and: subCategories)){
            Utilities.topMostPresenterViewController.presentInFullScreen(viewController, animated: true, completion: nil)
        }
    }
}

extension OARenewalView {
    func setupRatingView() {
        if let claimAmount = claimAmount, let amountSaved = Utilities.decimalNumberFormatter.string(from: NSNumber(value:Int(round(claimAmount)))) {
            savedAmountLabel.text = String(format: Strings.RenewMembershipScene.savedText, amountSaved)
        }
        
        if let endDate = membership?.endDate as NSString? {
            let dayMon = Date.getDateStr(from: endDate, with: .dayMonth).split{$0 == " "}.map(String.init)
            let formattedDate = "\(dayMon[0])\(String(describing: Date.getDate(from: endDate).getSuffixFromDay(dayOfMonth: Int(dayMon[0]) ?? -1))) \(dayMon[1])"
            
            let days = Date.getDate(from: endDate as String).days(from: Date.currentDate()) ?? 0
            if days < 0 {
                if (membership?.graceDays?.intValue ?? 0) > 0 {
                    expiringLabel.text = String(format: Strings.HATabScene.graceMessage, formattedDate, "\(membership?.graceDays?.intValue ?? 0)")
                }
            } else {
                expiringLabel.text = String(format: Strings.RenewMembershipScene.memExp, formattedDate)
            }
        }
    }
}
