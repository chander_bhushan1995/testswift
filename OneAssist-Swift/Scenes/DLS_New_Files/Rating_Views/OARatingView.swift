//
//  OARatingView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 14/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OARatingView: UIView, NibInstantiable {
    
//    @IBOutlet var ratingView: UIView!
    @IBOutlet weak var ratingContainerView: UIView!
    @IBOutlet weak var ratingSubmitButton: OAPrimaryButton!
    @IBOutlet weak var overAllExpericeLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var onAscaleLabel: SupportingTextRegularGreyLabel!
    
    var selectedRating: Int = 5
    let group = DispatchGroup()
    var membershipId: String?
    var srId = "112843"
    var ratingViewController: UIViewController?
    var claimType: Constants.Services?
    var storeRv: OAConfirmationView?
    var rv: OARenewalView?
    
    // Renewal Claim Amount
    fileprivate(set) var claimAmount: Float?
    
    var membership: CustomerMembershipDetails?
    
    fileprivate(set) var subCategories: [HomeApplianceSubCategories] = []
    
    var loaderOACount = 0
    
//    var topVC:UIViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
        
    }
    private func commonInit(){
        loadNibContent()
//        configureView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // topMostViewController()
    //    initialSetup()
    }
    
    func initialSetup() {
        for i in 1...5 {
            let button = ratingContainerView.viewWithTag(i) as? UIButton
            button?.isSelected = true

            button?.setImage(UIImage(named: Strings.WHC.inspection.inspectionDetails.RatingGreenImage ), for: .selected)
            button?.setImage(UIImage(named: Strings.WHC.inspection.inspectionDetails.RatingGreenImage), for: [.highlighted, .selected])
        }
        checkAndActivateRatingSubmitButton()
    }
    
    func checkForRedirection() {
        // TODO: KAG, Test this method
        if let vc = ratingViewController {
            vc.view.isUserInteractionEnabled = true
            if let memVC = vc as? TabVC {
                memVC.refreshMemberships()
            } else if let historyVC = vc as? MembershipDetailsVC {
                NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
                historyVC.handleRefresh()
            } else if let timelineVC = vc as? TimelineBaseVC, let viewControllers = timelineVC.navigationController?.viewControllers, viewControllers.count >= 2 {
                let parentVC = viewControllers[viewControllers.count - 2]
                if let tabBarVC = parentVC as? TabVC {
                    tabBarVC.refreshMemberships()
                } else if let historyVC = parentVC as? MembershipDetailsVC {
                    NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
                    historyVC.handleRefresh()
                }
            }
        }
    }
    
    @IBAction func clickedBtnRating(_ sender: Any) {
        let ratingButton = sender as? UIButton
        let tag: Int = (ratingButton?.tag)!
        for i in 1...tag {
            let button = ratingContainerView.viewWithTag(i) as? UIButton
            button?.isSelected = true
        }
        if(tag+1 <= 5){
            for i in tag + 1...5 {
                let button = ratingContainerView.viewWithTag(i) as? UIButton
                button?.isSelected = false
            }
        }
        selectedRating = tag
        checkAndActivateRatingSubmitButton()
    }
   
    @IBAction func clickedRatingSubmitButton(_ sender: Any) {

        // EventTracking.shared.eventTracking(name: .ratingSubmitted, [.location: "Timeline", .starRating: selectedRating.description])

        if selectedRating < 4 {
            let feedbackVC = FeedbackTableViewVC()
            feedbackVC.rating = selectedRating
            feedbackVC.delegate = self
            feedbackVC.claimType = claimType
            Utilities.topMostPresenterViewController.presentInFullScreen(feedbackVC, animated: true)
        } else {
            Utilities.addLoader(message: "Please wait...", count: &loaderOACount, isInteractionEnabled: false)

            group.enter()

            if let _ = membershipId {
                group.enter()
                getCustomerMembershipResponse()

                group.enter()
                let _ = HomeApplianceCategoryRequestUseCase.service(requestDTO: nil) { (usecase, response, error) in
                    self.subCategories.append(contentsOf: response?.subCategories ?? [])
                    self.group.leave()
                }
            }

            group.leave()

            group.notify(queue: .main) {
                self.submitFeedback(for: self.srId, rating: self.selectedRating, with: nil) { (response, error) in
                    Utilities.removeLoader(count: &self.loaderOACount)
                    if error == nil {
                        self.removeFromSuperview()
                        var isRenewal = false
                        if let membership = self.membership {
                            if let status = membership.renewalStatus, let activity = membership.renewalActivity {
                                if status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected {
                                    if activity == Constants.RenewalActivityFlags.renewalWindow {
                                        isRenewal = true
                                        self.checkForRedirection()
                                        self.showRenewalView()
                                    }
                                }
                            }
                        }
                        
                        if !isRenewal {
                            self.checkForRedirection()
                            self.showAppStoreRatingView()
                        }
                    } else {
                        self.displayError(error: error!)
                    }
                }
            }
        }
    }
}

extension OARatingView {
    
    fileprivate func checkAndActivateRatingSubmitButton() {
        if selectedRating > 0 {
            //            ratingSubmitButton.setEnabledState(true)
            //            ratingSubmitButton.isUserInteractionEnabled = true
            ratingSubmitButton.isEnabled = true
        }
        else {
            //            ratingSubmitButton.setEnabledState(false)
            //            ratingSubmitButton.isUserInteractionEnabled = false
            ratingSubmitButton.isEnabled = false
        }
    }
    
    fileprivate func getCustomerMembershipResponse() {
        let request = CustomerMembershipDetailsRequestDTO()
        request.memId = membershipId
        request.membershipType = "A"
        request.assetDocRequired = "true"
        request.assetScopeRequired = "true"
        request.checkClaimEligibility = "true"
        request.assetServicesRequired = "true"
        request.renewalEligibilityRequired = "true"
        request.customerInfoRequired = "true"
        request.assetRenewalEligibilityRequired = "true"
        request.assetAttributesRequired = "true"
        
        let _ = CustomerMembershipDetailsRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            if error == nil {
                self.membership = response?.data?.memberships?.first
                self.parseClaim(with: self.membership)
                // self.setupRatingView()
                self.group.leave()
            }
        }
    }
    
    func submitFeedback(for serviceRequest: String, rating: Int, with reasons: [String]?, completionHandler: @escaping (SubmitFeedbackResponseDTO?, Error?)->()) {
        
        let feedbackRequest = ServiceRequestFeedback(dictionary: [:])
        feedbackRequest.feedbackCode = reasons?.joined(separator: ",")
        feedbackRequest.feedbackRating = rating.description
        
        let request = SubmitFeedbackRequestDTO()
        request.serviceReqId = serviceRequest
        // FIXME: remove default value
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "suren"
        request.serviceRequestFeedback = feedbackRequest
        
        let _ = WHCSubmitFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            completionHandler(response, error)
        }
    }
}

extension OARatingView: FeedbackTableViewVCDelegate {
    
    func checkForRedirectionForFeedback() {
        Utilities.topMostPresenterViewController.dismiss(animated: true) {
            Utilities.topMostPresenterViewController.view.makeToast(Strings.ToastMessage.feedbak)
        }
        checkForRedirection()
        
        if let vc = ratingViewController {
            if let timelineVC = (vc as? TimelineBaseVC) {
                timelineVC.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func clickedSubmitRating(_ reasons: [String]) {
        Utilities.addLoader(message: "Please wait...", count: &loaderOACount, isInteractionEnabled: false)
        submitFeedback(for: srId, rating: selectedRating, with: reasons) { [unowned self] (response, error) in
            Utilities.removeLoader(count: &self.loaderOACount)
            
            if error == nil {
                self.removeFromSuperview()
//                NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
                self.checkForRedirectionForFeedback()
                //Utilities.topMostViewController.navigationController?.popViewController(animated: false)
                
            } else {
                self.displayError(error: error!)
            }
        }
    }
}

extension OARatingView{
    fileprivate func showRenewalView(){
        rv = OARenewalView()
        if let rv = rv {
            rv.claimAmount = claimAmount
            rv.membership = membership
            rv.subCategories = subCategories
            rv.setupRatingView()
            Utilities.topMostPresenterViewController.addOABottomView(rv)
        }
    }
    
    fileprivate func showAppStoreRatingView() {
        storeRv = OAConfirmationView()
        if let storeRv = storeRv {
            storeRv.ratingViewController = ratingViewController
            Utilities.topMostPresenterViewController.addOABottomView(storeRv)
        }
    }
    
    fileprivate func displayError(error: Error) {
        Utilities.topMostPresenterViewController.showAlert(title: Strings.Global.error, message: error.localizedDescription)
    }
    
    fileprivate func parseClaim(with membership: CustomerMembershipDetails?) {
        if let membership = membership {
            self.claimAmount = membership.claims?.filter({
                Date.getDate(from: membership.startDate ?? "") <= (DateFormatter.scheduleFormat.date(from: $0.createdOn ?? "") ?? Date.currentDate())
            }).reduce(0.0, { $0 + (($1.workflowData?.repairAssessment?.costToCompany?.floatValue) ?? 0.0) }) ?? 0.0
        }
    }
    
    
}
