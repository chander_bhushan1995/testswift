//
//  MembershipInfoView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 01/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum MemInfoCard {
    case memStatusDescriptionCard // both are present
    case memStatusDescriptionArrowButtonCard
    case memStatusCard // only status is present
    case memDescriptionCard // only image and description is present
    case textArrowButtonCard // renewal/Rating/Pending Submission/Awaiting Confirmation card
    case srCard
    case memStatusTextArrowButtonCard
    case memStatusSRCard // status and SR Card both are present
    case multipleSRCard
    case multipleSRPendencyCard
    case noCard
}

protocol MembershipInfoDelegate:class {
    func clickedTextArrowButtonView(_ view: MembershipInfoView, clickedTextArrowBtnView sender: Any)
    func clickedButtonSRView(_ view: MembershipInfoView, clickedBtnSRView sender: Any)
    func clickedButtonMultipleSRView(_ view: MembershipInfoView, clickedBtnMultipleSRView sender: Any)
    func clickedButtonMultipleSRPendencyView(_ view: MembershipInfoView, clickedBtnMultipleSRPendencyView sender: Any)
    func clickedButtonMemDescriptionView(_ view: MembershipInfoView, clickedMemDescriptionBtnView sender: Any)
}

extension MembershipInfoDelegate {
    func clickedTextArrowButtonView(_ view: MembershipInfoView, clickedTextArrowBtnView sender: Any){
        
    }
    
    func clickedButtonSRView(_ view: MembershipInfoView, clickedBtnSRView sender: Any) {
    }
    
    func clickedButtonMultipleSRView(_ view: MembershipInfoView, clickedBtnMultipleSRView sender: Any) {
    }
    
    func clickedButtonMultipleSRPendencyView(_ view: MembershipInfoView, clickedBtnMultipleSRPendencyView sender: Any) {
    }
    
    func clickedButtonMemDescriptionView(_ view: MembershipInfoView, clickedMemDescriptionBtnView sender: Any) {
        
    }
}

class MembershipInfoView: BaseNibView {
    
    @IBOutlet weak var membershipStatusDescriptionView: UIView!
    @IBOutlet weak var memStatusParentView: UIView!
    @IBOutlet weak var memStatusCurvedView: DLSCurvedView!
    @IBOutlet weak var memStatusTextLabel: TagsBoldGreyLabel!
    @IBOutlet weak var memDescriptionParentView: UIView!
    @IBOutlet weak var memDescriptionView: DLSCurvedView!
    @IBOutlet weak var dlsPendencyImage: UIImageView!
    @IBOutlet weak var memDescriptionText: BodyTextRegularBlackLabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet var memDescriptionTextBottomConstraint: NSLayoutConstraint!
    @IBOutlet var buttonTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textButtonParentView: UIView!
    @IBOutlet weak var textButtonView: UIView!
    @IBOutlet weak var textLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var arrowButtonView: UIView!
    @IBOutlet weak var textButton: OASecondaryButton!
    @IBOutlet var topButtonConstraint: NSLayoutConstraint!
    @IBOutlet var textBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var srParentView: UIView!
    @IBOutlet weak var srView: UIView!
    @IBOutlet weak var srIDLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var innerStackView: UIStackView!
    @IBOutlet weak var srStageLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var actionButton: OASecondaryButton!
    @IBOutlet weak var descriptionLabel: SupportingTextRegularGreyLabel!
    
    
    @IBOutlet weak var multipleSRParentView: UIView!
    @IBOutlet weak var multipleSRView: UIView!
    @IBOutlet weak var noOfSRsLabel: BodyTextBoldBlackLabel!
    
    
    @IBOutlet weak var multipleSRPendencyParentView: UIView!
    @IBOutlet weak var multipleSRPendencyView: UIView!
    @IBOutlet weak var noOfSRsPendencyLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var dlsUrgentImage: UIImageView!
    @IBOutlet weak var urgentActionLabel: SupportingTextBoldBlackLabel!
    
    weak var delegate:MembershipInfoDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /* Here you can access UI elements and set their properties. */
        memStatusCurvedView.layer.cornerRadius = 3.0
        memStatusCurvedView.layer.borderWidth = 1.0
        multipleSRView.backgroundColor = UIColor.backgroundInProgress
    }
    
    func hideArrowButtonViewForTextButtonView() {
        arrowButtonView.isHidden = true
        topButtonConstraint.isActive = false
    }
    
    func showArrowButtonViewForTextButtonView() {
        arrowButtonView.isHidden = false
        topButtonConstraint.isActive = true
    }
    
    func showComponentsInSRView() {
        progressBar.isHidden = false
        descriptionLabel.isHidden = false
    }
    
    /* Avoid sending view model or any fields in this method. Seperate methods would be created. */
    func setupUI(with cardType: MemInfoCard) {
        membershipStatusDescriptionView.isHidden = true
        memStatusParentView.isHidden = true
        memDescriptionParentView.isHidden = true
        resendButton.isHidden = true
        memDescriptionTextBottomConstraint.priority = .defaultHigh
        buttonTopConstraint.priority = .defaultLow
        
        textButtonParentView.isHidden = true
        
        srParentView.isHidden = true
        
        multipleSRParentView.isHidden = true
        multipleSRPendencyParentView.isHidden = true
        
        switch cardType {
        case .memStatusDescriptionCard:
            membershipStatusDescriptionView.isHidden = false
            memStatusParentView.isHidden = false
            memDescriptionParentView.isHidden = false
        case .memStatusDescriptionArrowButtonCard:
            membershipStatusDescriptionView.isHidden = false
            memStatusParentView.isHidden = false
            memDescriptionParentView.isHidden = false
            resendButton.isHidden = false
            memDescriptionTextBottomConstraint.priority = .defaultLow
            buttonTopConstraint.priority = .defaultHigh
        case .memStatusCard:
            membershipStatusDescriptionView.isHidden = false
            memStatusParentView.isHidden = false
        case .memDescriptionCard:
            membershipStatusDescriptionView.isHidden = false
            memDescriptionParentView.isHidden = false
        case .textArrowButtonCard:
            textButtonParentView.isHidden = false
            showArrowButtonViewForTextButtonView()
        case .srCard:
            srParentView.isHidden = false
            showComponentsInSRView()
        case .memStatusTextArrowButtonCard:
            membershipStatusDescriptionView.isHidden = false
            memStatusParentView.isHidden = false
            textButtonParentView.isHidden = false
            showArrowButtonViewForTextButtonView()
        case .memStatusSRCard:
            membershipStatusDescriptionView.isHidden = false
            memStatusParentView.isHidden = false
            srParentView.isHidden = false
            showComponentsInSRView()
        case .multipleSRCard:
            multipleSRParentView.isHidden = false
        case .multipleSRPendencyCard:
            multipleSRPendencyParentView.isHidden = false
        case .noCard:
            break
        }
    }
    
    func setupMemCard(with memStatus: String? = nil,and memDescription: String? = nil, and imageName: String? = nil,with statusColor: UIColor? = nil,and descriptionColor: UIColor? = nil, arrowButtonText: String? = nil, isNewTag: Bool = false) {
        if let status = memStatus, let color = statusColor {
            memStatusTextLabel.text = status
            memStatusCurvedView.layer.borderColor = color.cgColor
            if isNewTag {
                memStatusTextLabel.textColor = .white
                memStatusTextLabel.backgroundColor = color
                memStatusCurvedView.backgroundColor = color
            } else {
                memStatusTextLabel.textColor = color
                memStatusTextLabel.backgroundColor = .white
                memStatusCurvedView.backgroundColor = .white
            }
        }
        
        if let buttonText = arrowButtonText {
            resendButton.setTitle(buttonText, for: .normal)
        }
        
        if let name = imageName, let description = memDescription {
            dlsPendencyImage.image = UIImage(named: name) ?? UIImage()
            memDescriptionText.text = description
            
//            if let color = descriptionColor {
//                memDescriptionText.textColor = color
//            }
            
            /* Background color depends upon the image which we are showing. */
            if name == Strings.Global.dlsPendency {
                memDescriptionView.backgroundColor = UIColor.backgroundInProgress
            } else if name == Strings.Global.dlsCancelled {
                memDescriptionView.backgroundColor = UIColor.backgroundError
            }
        }
        
    }
    
    func setupTextArrowWithMemStatusCard(with text: String? = nil ,and attributedText: NSMutableAttributedString? = nil, and descriptionColor: UIColor? = nil, and buttonText: String? = nil, and memStatus: String? = nil, and statusColor: UIColor? = nil) {
        if let status = memStatus, let color = statusColor {
            memStatusTextLabel.text = status
            memStatusTextLabel.textColor = color
            memStatusCurvedView.layer.borderColor = color.cgColor
        }
        
        if let text = text {
            textLabel.text = text
        }
        
        if let attributedText = attributedText {
            textLabel.attributedText = attributedText
        }
        
        if let color = descriptionColor {
            textButtonView.backgroundColor = color
        }
        
        if let buttonText = buttonText {
            // textButton.titleLabel?.text = buttonText
            textButton.setTitle(buttonText, for: .normal)
        } else {
            hideArrowButtonViewForTextButtonView()
        }
        
    }
    
    func setupSRWithMemStatusCard(with memStatus: String? = nil,and statusColor: UIColor? = nil, and serviceID: String? = nil, and stage: String, and status: String, and descriptionText: String? = nil, and backgroundColor: UIColor) {
        
        if let status = memStatus, let color = statusColor {
            memStatusTextLabel.text = status
            memStatusTextLabel.textColor = color
            memStatusCurvedView.layer.borderColor = color.cgColor
        }
        
        srIDLabel.text = serviceID
        
        // ProgressBar color will be same as background color AND this condition will change in future.
        progressBar.isHidden = true
        
        srStageLabel.text = stage
        // actionButton.titleLabel?.text = status
        actionButton.setTitle(status, for: .normal)
        srView.backgroundColor = backgroundColor
        
        if let descriptionText = descriptionText {
            descriptionLabel.text = descriptionText
        } else {
            descriptionLabel.isHidden = true
        }
    }
    
    func setupOnGoingAndCompletedSRCard(with srViewModel: SRListCompletedViewModel?, and memStatus: String? = nil, and statusColor: UIColor? = nil) {
        
        // setupUI + configure logic
        
        if let srViewModel = srViewModel {
            
            if srViewModel.srType == .raised || srViewModel.srType == .uploadPending || srViewModel.srType == .reuploadPending || srViewModel.srType == .onHold || srViewModel.srType == .reschedule {
                if memStatus != nil {
                    setupUI(with: .memStatusSRCard)
                } else {
                    setupUI(with: .srCard)
                }
                
                var stage: String = ""
                var status: String = ""
                var backgroundColor = UIColor.backgroundInProgress
                
                if srViewModel.srType == .raised {
                    stage = Strings.Global.claimInProgress
                    status = Strings.Global.viewDetails
                    backgroundColor = UIColor.backgroundSuccess
                } else if srViewModel.srType == .uploadPending {
                    stage = Strings.Global.documentUploadPending
                    status = Strings.Global.upload
                    // same color as above
                } else if srViewModel.srType == .reuploadPending {
                    stage = Strings.Global.documentReuploadPending
                    status = Strings.Global.upload
                    // same color as above
                } else if srViewModel.srType == .onHold {
                    stage = Strings.Global.onHold
                    status = Strings.Global.viewDetails
                    // same color as above
                } else if srViewModel.srType == .reschedule {
                    stage = Strings.Global.rescheduleVisit
                    status = Strings.Global.reschedule
                    // same color as above
                }
                
                setupSRWithMemStatusCard(with: memStatus, and: statusColor, and: srViewModel.subtitle, and: stage, and: status, and: (srViewModel.infoText == "" ? nil : srViewModel.infoText) , and: backgroundColor)
            } else {
                if memStatus != nil {
                    setupUI(with: .memStatusTextArrowButtonCard)
                } else {
                    setupUI(with: .textArrowButtonCard)
                }
                
                var descriptionColor = UIColor.backgroundSuccess
                var buttonText: String?
                
                if srViewModel.srType == .awaitingConfirmation {
                    // same color as above
                } else if srViewModel.srType == .pendingSubmission {
                    descriptionColor = UIColor.backgroundError
                    buttonText = Strings.Global.verifyNow
                } else if srViewModel.srType == .ratingReqired {
                    // same color as above
                    buttonText = Strings.Global.rateUs
                }
                
                setupTextArrowWithMemStatusCard(with: srViewModel.infoText, and: nil, and: descriptionColor,and: buttonText, and: memStatus, and: statusColor)
            }
        }
    }
    
    func setupMultipleSRCard(with noOfSRsText: String) {
        noOfSRsLabel.text = noOfSRsText
    }
    
    func setupMultipleSRPendencyCard(with noOfSRsPendencyText: String) {
        noOfSRsPendencyLabel.text = noOfSRsPendencyText
    }
    
    @IBAction func tappedOnTextArrowView(_ sender: Any) {
        delegate?.clickedTextArrowButtonView(self, clickedTextArrowBtnView: sender)
    }
    
    @IBAction func tappedOnSRView(_ sender: Any) {
        delegate?.clickedButtonSRView(self, clickedBtnSRView: sender)
    }
    
    @IBAction func tappedOnMultipleSRView(_ sender: Any) {
        delegate?.clickedButtonMultipleSRView(self, clickedBtnMultipleSRView: sender)
    }
    
    @IBAction func tappedOnMultipleSRPendencyView(_ sender: Any) {
        delegate?.clickedButtonMultipleSRPendencyView(self, clickedBtnMultipleSRPendencyView: sender)
    }
    
    @IBAction func tappedOnMemDescriptionBtn(_ sender: Any) {
        delegate?.clickedButtonMemDescriptionView(self, clickedMemDescriptionBtnView: sender)
    }
    
}
