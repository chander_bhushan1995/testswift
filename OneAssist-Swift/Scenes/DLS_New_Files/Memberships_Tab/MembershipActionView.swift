//
//  MembershipActionView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum MemActionCard {
    case pendencyButtonCard
    case callCard
    case memDetailsButtonCard
    case noCard
}

protocol MembershipActionDelegate:class {
    func clickedButtonPendency(_ view: MembershipActionView, clickedBtnPendency sender: Any)
    func clickedButtonCall(_ view: MembershipActionView, clickedBtnCall sender: Any)
    func clickedButtonViewDetails(_ view: MembershipActionView, clickedBtnViewDetails sender: Any)
    func clickedButtonRaiseClaimOrMyCards(_ view: MembershipActionView, clickedBtnRaiseClaimOrMyCards sender: Any)
}

extension MembershipActionDelegate {
    
    func clickedButtonPendency(_ view: MembershipActionView, clickedBtnPendency sender: Any) {
    }
    
    func clickedButtonCall(_ view: MembershipActionView, clickedBtnCall sender: Any) {
    }
    
    func clickedButtonViewDetails(_ view: MembershipActionView, clickedBtnViewDetails sender: Any) {
    }
    
    func clickedButtonRaiseClaimOrMyCards(_ view: MembershipActionView, clickedBtnRaiseClaimOrMyCards sender: Any) {
    }
}

class MembershipActionView: BaseNibView {

    @IBOutlet weak var pendencyButtonView: UIView!
    @IBOutlet weak var pendencyButton: OAPrimaryButton!
    
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var helpLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var mobileNoLabel: H3RegularBlackLabel!
    
    @IBOutlet weak var membershipDetailsButtonView: UIView!
    @IBOutlet weak var leftButton: TransparentBodyTextBoldButton!
    @IBOutlet weak var rightButton: TransparentBodyTextBoldButton!
    
    weak var delegate:MembershipActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /* Here you can access UI elements and set their properties. */
        helpLabel.text = Strings.Global.furtherHelp
        mobileNoLabel.text = String(format: Strings.HATabScene.Whc.contact, RemoteConfigManager.shared.contactNumberCustomerCare)
    }
    
    /* More fields would be added to this function based on content which needs to be shown in the card. */
    /* Avoid sending whole view model in this. */
    func setupUI(with cardType: MemActionCard, and text1: String? = nil,and text2: String? = nil, and canRaiseClaim: Bool? = nil) {
        pendencyButtonView.isHidden = false
        callView.isHidden = false
        membershipDetailsButtonView.isHidden = false
        rightButton.isEnabled = true
        
        switch cardType {
        case .pendencyButtonCard:
            callView.isHidden = true
            membershipDetailsButtonView.isHidden = true
            if let text = text1 {
                pendencyButton.setTitle(text, for: .normal)
            }
        case .callCard:
            pendencyButtonView.isHidden = true
            membershipDetailsButtonView.isHidden = true
            if let text = text1 {
                helpLabel.text = text
            }
            if let text = text2 {
                mobileNoLabel.text = text
            }
        case .memDetailsButtonCard:
            pendencyButtonView.isHidden = true
            callView.isHidden = true
            if let leftText = text1, let rightText = text2 {
                leftButton.setTitle(leftText, for: .normal)
                rightButton.setTitle(rightText, for: .normal)
                
                if let canRaiseClaim = canRaiseClaim {
                    rightButton.isEnabled = canRaiseClaim
                }
            }
        case .noCard:
            pendencyButtonView.isHidden = true
            callView.isHidden = true
            membershipDetailsButtonView.isHidden = true
        }
    }
    
    @IBAction func pendencyButtonClicked(_ sender: Any) {
        delegate?.clickedButtonPendency(self, clickedBtnPendency: sender)
    }
    
    @IBAction func callButtonClicked(_ sender: Any) {
        delegate?.clickedButtonCall(self, clickedBtnCall: sender)
    }
    
    @IBAction func viewDetailsButtonClicked(_ sender: Any) {
        delegate?.clickedButtonViewDetails(self, clickedBtnViewDetails: sender)
    }
    
    @IBAction func raiseClaimOrMyCardsButtonClicked(_ sender: Any) {
        delegate?.clickedButtonRaiseClaimOrMyCards(self, clickedBtnRaiseClaimOrMyCards: sender)
    }
    
}
