//
//  ServicePickerView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 26/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum ServicePickerOption {
    case sellYourPhone, bookAService
    
    var title: String {
        switch self {
        case .sellYourPhone:
            return "I want to sell my phone"
        case .bookAService:
            return "I want to raise a claim"
        }
    }
    
    var index: Int {
        switch self {
        case .sellYourPhone:
            return 0
        case .bookAService:
            return 1
        }
    }
}

protocol ServicePickerDelegate: NSObjectProtocol {
    func selectedService(_ service: ServicePickerOption?, index: Int)
}

class ServicePickerView: UIView, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet var bottomSheetView: UIView!
    @IBOutlet var curvedView: UIView!
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var tableViewHeightConstaint: NSLayoutConstraint!
    
    var model: [ServicePickerOption] = [.bookAService]
    var index: Int = 0
    var delegate: ServicePickerDelegate?
    var onServiceSelection: ((_ index: Int)->Void)?

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("ServicePickerView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        curvedView.layer.cornerRadius = 16
        curvedView.clipsToBounds = true
        bottomSheetView.isUserInteractionEnabled = true
        
        tableViewObj.register(cell: PlanDetailSimpleCell.self)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.separatorColor = .gray2
        tableViewObj.isScrollEnabled = false
        let line = UIView(frame: CGRect(x: 0, y: 0, width: tableViewObj.frame.size.width, height: 1 / UIScreen.main.scale))
        line.backgroundColor = .gray2
        tableViewObj.rowHeight = 56.0
        tableViewObj.tableHeaderView = line
    }
    
    @IBAction func crossTapped(_ sender: Any) {
        delegate?.selectedService(nil, index: index)
    }
    
    func setViewModel(_ model: [ServicePickerOption], delegate: ServicePickerDelegate?, index: Int) {
        self.delegate = delegate
        self.model = model
        self.index = index
        self.tableViewHeightConstaint.constant = CGFloat(model.count * 56)
        tableViewObj.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PlanDetailSimpleCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setViewModel(model[indexPath.row].title, index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedService(model[indexPath.row], index: index)
        if let closure = onServiceSelection {
            closure(model[indexPath.row].index)
        }
    }
    
    class func height(forModel model:[ServicePickerOption]) -> CGFloat {
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        let collectiveHeigthConstains: CGFloat = 62
        
        return CGFloat(model.count * 56) + bottomSafeArea + collectiveHeigthConstains
    }
}
