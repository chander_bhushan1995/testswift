//
//  PlanDetailAndSRModel.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/3/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

struct PlanDetailAndSRModel {
    var customerId: String?
    var planCode: NSNumber?
    var planName: String?
    var maxQuantity: NSNumber?
    var coverAmount: NSNumber?
    var category: String?
    var salesPrice: NSNumber?
    var planValidFrom: String?
    var planValidTill: String?
    var warningMessage: String?
    var graceDays: NSNumber?
    var canRenew = false
    var planRenewed = false
    var isInspectionForRenew = false
    var enableFileClaim = false
    var buttonTitle: String?
    var planTypeName: String?
    var planTypeImage: UIImage?
    var planTypeImageUrlStr: String?
    var products: [Product]?
    var appliancesName: String?
    var task: [Task]?
    var activationCode: String?
    var isWhc = false
    var whcPlanStatus: WhcPlanStatus = .pending(beforeDate: "")
    var fdStageActionType: FDMembershipStageActionType = .activatedSuccessfully
    var planPurchaseDate: String?
    var amountSaved: Float = 0.0
    var rank: Int = 100
    var activity: String?
    var uploadDocsLastDate:String?
    var isCombo = false
    var planPrice: Int = 0
    var membershipResponse: CustomerMembershipDetails?
    
    /* might be shifted somewhere else in future*/
    var membershipId: String?
    var isTrial = false
    
    var isIDFence = false
    var idFenceText: String?
    var memStatus: String?
    
    var customer: [CustomerR]?
    var serviceText: String?
    
    // For FD view model population
    var tempCustomerDetail: [TempCustomerDetail]?
    var enableFDFlow: Bool?
    var paymentDone: Bool?
    var showInsuranceCertificate: Bool?
    var showPaymentReceipt: Bool?
    var preBoardingTaskStage: String?
    var siStatus: String?
    var isSIEnabled: Bool = false
    var memUUID: String?
    var partnerCode: NSNumber?
    var partnerBUCode: NSNumber?
    
    var glance: Bool = false
    var maxInsuranceValue: NSNumber?
    var minInsuranceValue: NSNumber?
    var transactionDate: String?
    var minPurchaseDateForGlance: String?

    
    // It would contain SR of all types
    var serviceRequests: [SRListCompletedViewModel] = []
    var isPendingPlan = false
    var isUpgradeable:Bool = false
    
    // For ABB
    var isABBMembership = false
    var isABBOnlyMembership = false
    var isABBTenureActive = false
    var buybackData: BuyBackStatus? = nil
    
    init(_ membership: CustomerMembershipDetails) {
        membershipResponse = membership
        customerId = membership.customers?[0].custId?.description
        membershipId = membership.memId?.description
        isTrial = membership.trial == "Y"
        planCode = membership.plan?.planCode
        planName = membership.plan?.planName
        planValidFrom = membership.startDate
        planValidTill = membership.endDate
        activationCode = nil
        showInsuranceCertificate = (membership.showInsuranceCertificate?.boolValue ?? false) && (membership.assets?.contains(where: { $0.category == Constants.Category.personalElectronics}) ?? false)
        showPaymentReceipt = membership.showPaymentReceipt?.boolValue
        isSIEnabled = membership.isSIEnabled?.boolValue ?? false
        partnerCode = membership.partnerCode
        partnerBUCode = membership.partnerBUCode
        planPrice = membership.plan?.price?.intValue ?? 0
    }
    
    init(_ membership: CustomerPendingMembershipDetails) {
        customerId = membership.customers?[0].custId?.description
        isTrial = membership.plan?.trial == "Y" // TODO: Doesn't exist in PE pending plans.
        planCode = membership.plan?.planCode
        planName = membership.plan?.planName
        planValidFrom = membership.startDate
        graceDays = membership.graceDays
        task = [membership.task ?? Task(dictionary: [:])] // TODO: Doesn't exist in PE pending plans.
        activationCode = membership.activationCode
        activity = membership.activity
        planPurchaseDate = membership.purchaseDate
        isSIEnabled = membership.isSIEnabled?.boolValue ?? false
        partnerCode = membership.partnerCode
        partnerBUCode = membership.partnerBUCode
        planPrice = membership.plan?.price?.intValue ?? 0
        glance = membership.glance?.boolValue ?? false
        maxInsuranceValue = membership.maxInsuranceValue
        minInsuranceValue = membership.minInsuranceValue
        transactionDate = membership.transactionDate
        minPurchaseDateForGlance = membership.minPurchaseDateForGlance
    }
    
    // It is for WHC active and renewal expired memberships
    init(_ membership: CustomerMembershipDetails, and applianceCategories: [HomeApplianceSubCategories]) {
        self.init(membership)
        whcPlanStatus = membership.getWhcStatus()
        isWhc = true
        customer = membership.customers
        coverAmount = membership.coverAmount
        salesPrice = membership.salesPrice
        graceDays = membership.graceDays
        planName = Strings.HATabScene.Whc.whcPlanName
        maxQuantity = membership.plan?.allowedMaxQuantity
        products = membership.assets?.map({ $0.getProduct(from: $0, with: applianceCategories) })
        planPurchaseDate = membership.purchaseDate
        planTypeName = Strings.MembershipRevamp.applianceDetails // test
        planTypeImage = #imageLiteral(resourceName: "cardProtect")
        isSIEnabled = membership.isSIEnabled?.boolValue ?? false
        partnerCode = membership.partnerCode
        partnerBUCode = membership.partnerBUCode
        planPrice = membership.plan?.price?.intValue ?? 0
        // Still things have to be added
        if let status = membership.renewalStatus, let activity = membership.renewalActivity {
            if status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected {
                if activity == Constants.RenewalActivityFlags.renewalWindow {
                    canRenew = true
                    buttonTitle = Strings.MembershipRevamp.renewNow
                    warningMessage = Strings.HATabScene.memExpiry
                }
                /*  For Renewal Phase 5
                 else if activity == Constants.RenewalActivityFlags.changePlan {
                 canRenew = true
                 buttonTitle = Strings.HATabScene.chooseAnotherPlan
                 warningMessage = Strings.HATabScene.memExpiry
                 } else if activity == Constants.RenewalActivityFlags.buyNewPlan {
                 canRenew = true
                 buttonTitle = Strings.HATabScene.buyNewPlan
                 warningMessage = Strings.HATabScene.memExpiry
                 } */
                
                amountSaved = membership.claims?.filter({ Date.getDate(from: membership.startDate ?? "") <= (DateFormatter.scheduleFormat.date(from: $0.createdOn ?? "") ?? Date.currentDate())
                }).reduce(0.0, { $0 + (($1.workflowData?.repairAssessment?.costToCompany?.floatValue) ?? 0.0) }) ?? 0.0
                
            } else if status == Constants.RenewalStatusFlags.pending || status == Constants.RenewalStatusFlags.initiated {
                isInspectionForRenew = true
            }
        }
        
        if isTrial {
            buttonTitle = Strings.MembershipRevamp.upgradeNow
            warningMessage = Strings.HATabScene.upgradeError
        }
    }
    
    mutating func updateIdFenceText() {
        let currentDate = Date.currentDate()
        let endDate = Date.getDate(from: planValidTill ?? "", with: DateFormatter.serverDateFormat)
        let graceEndDate = endDate.addingTimeInterval((graceDays?.doubleValue ?? 0) * 24 * 60 * 60)
        
        if isTrial {
            canRenew = !isSIEnabled
            isUpgradeable = !isSIEnabled
            buttonTitle = Strings.MembershipRevamp.upgradeNow
            if memStatus == Constants.MembershipStatus.cancelled {
                    idFenceText = Strings.IDFenceConstants.trailCancelledBuyNow
                    buttonTitle = Strings.SalesRevamp.buyNow
                    canRenew = false // here forcefully set to true to show secondary action doesn't matter where it trial with card
            } else if currentDate < endDate && !self.isSIEnabled {
                let dateComponents = gregorianCalendar.dateComponents([.day,.minute], from: currentDate, to: endDate)
                if let days = dateComponents.day, let minutes = dateComponents.minute {
                    var dayDiff = days
                    if minutes - (days * 24 * 60) > 0 {
                        dayDiff += 1
                    }
                    idFenceText = String(format:Strings.IDFenceConstants.trialActive, String(dayDiff))
                }
            } else if endDate < currentDate && currentDate < graceEndDate  {
                let dateComponents = gregorianCalendar.dateComponents([.day,.minute], from: currentDate, to: graceEndDate)
                if let days = dateComponents.day, let minutes = dateComponents.minute {
                    var dayDiff = days
                    if minutes - (days * 24 * 60) > 0 {
                        dayDiff += 1
                    }
                    idFenceText = !self.isSIEnabled ? String(format:Strings.IDFenceConstants.trialExpired, String(dayDiff)) : nil
                    if self.isSIEnabled && (siStatus == Constants.MembershipStatus.cancelled || siStatus == Constants.MembershipStatus.failed){
                        canRenew = false
                        idFenceText = String(format:Strings.IDFenceConstants.trialMemExpired, String(dayDiff))
                    }
                }
            } else if memStatus == Constants.MembershipStatus.inactive {
                isUpgradeable = true // here this variable forcefully set to false to upgrade plan in case of Trial with card or without card
                canRenew = true // here forcefully set to true to show secondary action doesn't matter where it trial with card or without card
                idFenceText = Strings.IDFenceConstants.trialInactive
            }
        } else {
            if memStatus == Constants.MembershipStatus.cancelled {
                canRenew = false
                buttonTitle = Strings.SalesRevamp.buyNow
                idFenceText = Strings.IDFenceConstants.trailCancelledBuyNow
            } else if endDate < currentDate && currentDate < graceEndDate  {
                if siStatus == Constants.MembershipStatus.cancelled || siStatus == Constants.MembershipStatus.failed {
                    let dateComponents = gregorianCalendar.dateComponents([.day,.minute], from: currentDate, to: graceEndDate)
                    if let days = dateComponents.day, let minutes = dateComponents.minute {
                        var dayDiff = days
                        if minutes - (days * 24 * 60) > 0 {
                            dayDiff += 1
                        }
                        
                        idFenceText = String(format:Strings.IDFenceConstants.premiumExpired, String(dayDiff))
                    }
                }
            } else if graceEndDate < currentDate {
                canRenew = true
                buttonTitle = Strings.MembershipRevamp.reactivateNow
                idFenceText = Strings.IDFenceConstants.premiumInactive
            }
        }
    }
    
    func hasUserPendencyInClaim() -> Bool {
        return serviceRequests.contains(where: {[.reuploadPending, .uploadPending, .pendingSubmission, .reschedule, .ratingReqired].contains($0.srType)})
    }
    
    func hasUserPendencyAndRenewaInClaim() -> Bool {
        return hasUserPendencyInClaim() && canRenew
    }
    
    func hasOAPendencyInClaim() -> Bool {
        return serviceRequests.contains(where: {[.raised, .awaitingConfirmation, .onHold].contains($0.srType)})
    }
    
    func hasResumeClaim() -> Bool {
        return draftMembershipID() != nil && enableFileClaim
    }
    
    func draftMembershipID() -> String? {
        return membershipResponse?.draftMemberships?.first?.id
    }
    
    func getInvDocNameMismatch() -> String? {
        var invDocNameMismatch: String? = nil
        if let assets = membershipResponse?.assets, assets.count>0,
           let firstassets = assets.first {
            invDocNameMismatch = firstassets.invDocNameMismatch
        }
        
        return invDocNameMismatch
    }
    
    func getAssetAttributes() -> [AssetAttributes]? {
        var assetAttributes: [AssetAttributes]? = nil
        if let assets = membershipResponse?.assets, assets.count>0,
           let firstassets = assets.first {
            assetAttributes = firstassets.assetAttributes
        }
        
        return assetAttributes
    }
}

class Product: ParsableModel {
    var assetId: NSNumber?
    var brand: String?
    var model: String?
    var productCode: String?
    var productName: String?
    var technology: String?
    var size: String?
    var sizeUnit: String?
    var serialNo: String?
    var canRenew: String?
    var message: String?
    var category: String?
    var serviceList: [String]?
    var productTypeImageUrlStr: String?
}

enum SRListCellType: String {
    case uploadPending = "Document Upload Pending"
    case raised = "Raised"
    case pendingSubmission = "Pending Submission"
    case reuploadPending = "Document Re-Upload Pending"
    case onHold = "On Hold"
    case reschedule = "Reschedule Visit"
    case awaitingConfirmation = "Awaiting Confirmation"
    case ratingReqired = "Rating Required"
}

class SRListCompletedViewModel: Row {
    var subtitle:String? = ""
    var caption = ""
    var srType: SRListCellType = .uploadPending
    var hasViewAllButton = false
    var hasDownloadInvoiceButton = false
    var prodCode: String?
    var srId = ""
    var memId = ""
    var claimType: Constants.Services = .accidentalDamage
    var assets: [String] = []
    var assetsDetail:[SRAsset]? = []
    var srDocuments: [ServiceDocument] = []
    var serviceAddress : ServiceAddress?
    var scheduleSlotEndDateTime: String?
    var scheduleSlotStartDateTime: String?
    var createdOn:String?
    var dateOfIncident: String?
    var requestDescription: String?
    var crmTrackingNumber: String?
    var serviceRequestType: String?
    var additionalDetails: AdditionalDetails?
    // pe details
    var documentUploadDetails: DocumentUpload?
    var insurancePartnerCode: NSNumber?
    var workflowData: WorkflowData?
    var noOfLines: Int {
        return assets.count > 1 ? 1 : 0
    }
    var thirdPartyProperties: ThirdPartyProperty?
    
    var infoText = ""
    
    init(_ service: SearchService) {
        super.init()
        if service.initiatingSystem?.intValue == 3 && service.modifiedBy != UserCoreDataStore.currentUser?.cusId && service.workflowStage == Constants.TimelineStage.documentUpload.rawValue {
            srType = .pendingSubmission
            assetsDetail = service.assets
            workflowData = service.workflowData
            infoText = Utilities.getStageDescription(service: service) ?? ""
            //                        // FIXME: due date?
            //                        if let date = DateFormatter.scheduleFormat.date(from: service.dueDateTime ?? "") {
            //                            infoText = "Please fill in the details and submit request by \(DateFormatter.dayMonth.string(from: date)) to avail service benefits"
            //                        }
        } else if service.workflowStage == Constants.TimelineStage.documentUpload.rawValue && service.workflowStageStatus == Constants.TimelineStageStatus.documentUploadPending {
            if service.refPrimaryTrackingNo == nil {
                srType = .awaitingConfirmation
                // isButtonActive = false
                infoText = "We have received your service request. On confirmation, we will send you an SMS/Email with service details."
            } else {
                srType = .uploadPending
                infoText = service.workflowData?.documentUpload?.stageDescription ?? ""
            }
        } else if service.workflowStage == Constants.TimelineStage.documentUpload.rawValue && service.workflowStageStatus == Constants.TimelineStageStatus.verificationUnsuccessful {
            // reupload case?
            srType = .reuploadPending
            //                        infoText = "Please re-upload and submit the following document within 2 days to continue"
            infoText = service.workflowData?.documentUpload?.stageDescription ?? ""
        } else if service.status == Constants.TimelineStatus.onHold {
            if service.workflowStageStatus == Constants.TimelineStageStatus.technicianCancelledOther || service.workflowStageStatus == Constants.TimelineStageStatus.technicianCancelledCustomerUnavailable {
                srType = .reschedule
                //                            infoText = "Please Re-schedule as technician was cancelled."
                infoText = Utilities.getStageDescription(service: service) ?? ""
            } else {
                //                            if service.workflowStageStatus == Constants.TimelineStageStatus.spareNeeded {
                srType = .onHold
                infoText = Utilities.getStageDescription(service: service) ?? ""
                //                            }
            }
        } else if service.status == Constants.TimelineStatus.completed || service.status == Constants.TimelineStatus.closeResolve {
            srType = .ratingReqired
            //FIXME: Need to change this
            // infoText = "Awesome! Your claim is successfully completed 👍  Please tell us your overall service experience."
            infoText = Utilities.getStageDescription(service: service) ?? ""
        } else {
            srType = .raised
            if let date = DateFormatter.scheduleFormat.date(from: service.createdOn ?? "") {
                caption = "Raised on \(DateFormatter.dayMonth.string(from: date))"
            }
            
            infoText = Utilities.getStageDescription(service: service) ?? ""
        }
        // FIXME: Remove below code
        serviceAddress = service.serviceAddress
        //TODO: Remove this hardCoding , we need this assest array only in case of pending sumbmission
        assetsDetail = service.assets
        additionalDetails = service.additionalDetails
        scheduleSlotEndDateTime = service.scheduleSlotEndDateTime
        scheduleSlotStartDateTime = service.scheduleSlotStartDateTime
        dateOfIncident = service.dateOfIncident
        requestDescription = service.requestDescription
        crmTrackingNumber = service.refPrimaryTrackingNo
        serviceRequestType = service.serviceRequestType
        prodCode = service.assets?.first?.productCode
        createdOn = service.createdOn
        if crmTrackingNumber != nil {
            subtitle = "Service ID \(service.refPrimaryTrackingNo ?? "")"
        } else {
            subtitle = "Service ID"
        }
        setProductInfo(service: service)
        thirdPartyProperties = service.thirdPartyProperties
        memId = service.referenceNo ?? ""
        srId = service.serviceRequestId?.description ?? ""
        claimType = Utilities.getClaimType(service: service)
        srDocuments = service.serviceDocuments ?? []
        documentUploadDetails = service.workflowData?.documentUpload
        insurancePartnerCode = service.insurancePartnerCode
        hasDownloadInvoiceButton = service.showDownloadJobsheet.boolValue
    }
    
    func setProductInfo(service: SearchService) {
        if let count = service.assets?.count {
            if count == 1 {
                imgUrl = Utilities.getProduct(productCode: service.assets?.first?.productCode ?? "")?.subCatImageUrl ?? ""
            } else if count > 1 {
                imageName = "homeServ"
            }
        }
        
        let products = service.assets?.compactMap({Utilities.getProduct(productCode: $0.productCode ?? "")?.subCategoryName}) ?? []
        
        if let count = service.assets?.count, count > 1 {
            hasViewAllButton = true
            title = products.joined(separator: ",")
        } else {
            hasViewAllButton = false
            title = "\(service.assets?.first?.make ?? "") \(products.first ?? "")"
        }
        
        assets = service.assets?.map({"\($0.make ?? "") \(Utilities.getProduct(productCode: $0.productCode ?? "")?.subCategoryName ?? "")"}) ?? []
    }
}
