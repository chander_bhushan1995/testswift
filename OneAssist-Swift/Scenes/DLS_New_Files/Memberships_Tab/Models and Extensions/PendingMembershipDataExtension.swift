//
//  PendingMembershipDataExtension.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 05/12/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

extension CustomerPendingMembershipDetails {
    func getWhcStatus() -> WhcPlanStatus {
        var status: WhcPlanStatus = .awaiting
        let taskStatus = Utilities.getWhcTask(tasks: [task ?? Task(dictionary: [:])])?.status ?? ""
        if taskStatus == "CEXP" {
            status = .membershipCancelled
        } else if taskStatus == "S" || taskStatus == "INCS" {
            status = .awaiting
        } else if taskStatus == "P" || taskStatus == "SCNA" || taskStatus == "TEA" || taskStatus == "INI" || taskStatus == "RSD" {
            status = .scheduled
        } else if taskStatus == "C" || taskStatus == "CACC" || taskStatus == "CTNA" || taskStatus == "CCNA" { // Inspection cancelled
            let currentDate = Date.currentDate()
            let purchased = Date.getDate(from: purchaseDate ?? "", with: DateFormatter.serverDateFormat)
            let tenthDate = gregorianCalendar.date(byAdding: .day, value: 9, to: purchased) ?? Date.currentDate()
            let suffix = tenthDate.getSuffix()
            let formatter = DateFormatter()
            formatter.calendar = gregorianCalendar
            formatter.dateFormat = String(format: "dd'%@' MMM", suffix)
            let tenthDateStr = tenthDate.string(with: formatter)
            let dayDiff = gregorianCalendar.dateComponents([.day], from: purchased, to: currentDate).day ?? 0
            status = dayDiff < 10 ? .inspectionCancelled(beforeDate: tenthDateStr) : .inspectionCancelledNonReschedule
        } else if taskStatus == "INF" {
            status = .failed
        } else {
            let currentDate = Date.currentDate()
            let purchased = Date.getDate(from: purchaseDate ?? "", with: DateFormatter.serverDateFormat)
            let dayDiff = gregorianCalendar.dateComponents([.day], from: purchased, to: currentDate).day ?? 0
            if dayDiff < 10 {
                let expiringDate = gregorianCalendar.date(byAdding: .day, value: 9, to: purchased) ?? Date.currentDate()
                let suffix = expiringDate.getSuffix()
                let formatter = DateFormatter()
                formatter.calendar = gregorianCalendar
                formatter.dateFormat = String(format: "dd'%@' MMM", suffix)
                let expiringDateStr = expiringDate.string(with: formatter)
                
                status = .pending(beforeDate: expiringDateStr)
            } else if dayDiff >= 10 && dayDiff < 23 {
                status = .pendingWithEarliestContact
            } else {
                let expiringDate = gregorianCalendar.date(byAdding: .day, value: 30, to: purchased) ?? Date.currentDate()
                _ = gregorianCalendar.dateComponents([.day], from: currentDate, to: expiringDate).day ?? 0
                //status = .pendingWithDelay(daysLeft: "\(daysLeft)")
                status = .pendingWillBeCancelled
            }
        }
        
        return status
    }
    
    func getFDActionType() -> FDMembershipStageActionType {
        var status: FDMembershipStageActionType = .activatedSuccessfully
        if let taskStatus = tempCustomerInfo?.task?.first?.status {
            if taskStatus == Strings.FDetectionConstants.strReupload {
                status = .docReupload
            } else if taskStatus == Strings.FDetectionConstants.strPostDtlPending {
                status = .welcomeScreen
            } else if taskStatus == Strings.FDetectionConstants.strComplete {
                status = .welcomeScreenForVerification
            } else {
                status = .welcomeScreenForVerification
            }
        }
        return status
    }
}

extension Array where Element: CustomerPendingMembershipDetails {
    // For PE Plans
    func getPEPlans(for applianceCategories: [HomeApplianceSubCategories], memToSRMapping:[String: [SearchService]]) -> [PlanDetailAndSRModel] {
        var pePlans: [PlanDetailAndSRModel] = []
        for membership in self {
            var plan = PlanDetailAndSRModel(membership)
            plan.isPendingPlan = true
            plan.membershipId = membership.memId?.description
            plan.salesPrice = membership.salesPrice
            plan.category = Constants.Category.personalElectronics
            plan.planTypeName = Strings.MobileTabScene.myMobileDetails
            plan.planTypeImage = #imageLiteral(resourceName: "dlsMobile")
            plan.planTypeImageUrlStr = Utilities.getImageStrForProductName(plan.products?.first?.productCode ?? "", applianceCategories: applianceCategories)
            
            if let productCode = plan.products?.first?.productCode,  let imageName = Destination(rawValue: productCode)?.iconName{
                plan.planTypeImage = UIImage(named: imageName)
            }
            
            /* Integrate SR view model */
            if let serviceReqs = memToSRMapping[plan.membershipId ?? ""] {
                if membership.enableFileClaim == false, membership.claims == nil {
                    //Do something
                }else {
                    plan.serviceRequests = serviceReqs.map({SRListCompletedViewModel($0)})
                }
            }
            plan.appliancesName = "\(membership.tempCustomerInfo?.customerDetails?[0].deviceMake ?? "Apple")"
            
            if plan.hasUserPendencyInClaim() {
                plan.rank = 4
            } else if plan.hasUserPendencyAndRenewaInClaim() {
                plan.rank = 5
            } else if plan.hasOAPendencyInClaim() {
                plan.rank = 13
            } else {
                plan.rank = 24
            }
            
            pePlans.append(plan)
        }
        
        return pePlans
    }
    
    // For Mobile Plans
    func getMobilePlans() -> [PlanDetailAndSRModel] {
        var mobilePlans: [PlanDetailAndSRModel] = []
        for membership in self {
            /* We don't keep pending memberships in case of mobile, because there is no FD flow. */
            /* Now FD flow came, so we will keep track of it. */
            // Need to write getMobilePlanStatus() method with the help of imran
            var plan = PlanDetailAndSRModel(membership)
            plan.fdStageActionType = membership.getFDActionType()
            plan.coverAmount = membership.tempCustomerInfo?.customerDetails?[0].amount
            plan.salesPrice = membership.salesPrice
            plan.category = Constants.Category.personalElectronics // "PE"
            // plan.planTypeName = Strings.MobileTabScene.myMobileDetails
            if let purchaseDate = plan.planValidFrom as NSString? {
                let dateObj = Date.getDate(from: purchaseDate)
                if let addOneYear = gregorianCalendar.date(byAdding: .year, value: 1, to: dateObj)?.millisecondsSince1970 {
                    plan.planValidTill = "\(addOneYear)"
                }
            }
            
            if membership.plan?.categories?.count ?? 0 > 1 {
                /* It is combo plan case. Conditions are added here because everything else has to be like PE only. */
                plan.isCombo = true
                plan.planTypeImage = #imageLiteral(resourceName: "combo")
            } else {
                plan.planTypeImage = #imageLiteral(resourceName: "dlsMobile")
            }
            
            plan.appliancesName = "\(membership.tempCustomerInfo?.customerDetails?[0].deviceMake ?? "Apple")"
            
            // mainly for the FD
            plan.tempCustomerDetail = membership.tempCustomerInfo?.customerDetails
            plan.enableFDFlow = membership.tempCustomerInfo?.enableFDFlow?.boolValue
            plan.paymentDone = membership.tempCustomerInfo?.paymentDone
            plan.preBoardingTaskStage = membership.tempCustomerInfo?.preBoardingTaskStage
            // plan.task = membership.tempCustomerInfo?.task ?? [Task(dictionary: [:])]
            plan.rank = 1
            mobilePlans.append(plan)
        }
        
        return mobilePlans
    }
    
    // For Wallet Plans. Also checking for IDFence.
    func getWalletPlans() -> [PlanDetailAndSRModel] {
        var walletPlans: [PlanDetailAndSRModel] = []
        for membership in self {
            /* We don't keep pending memberships in case of wallet, because there must not be any delay in backend. */
            var plan = PlanDetailAndSRModel(membership)
            plan.isPendingPlan = true
            plan.customer = membership.customers
            plan.membershipId = membership.memId?.description
            plan.isIDFence = membership.checkIsMembershipIDFence() // checking for IDFence.
            plan.category = Constants.Category.finance
            plan.maxQuantity = membership.plan?.allowedMaxQuantity
            plan.uploadDocsLastDate = membership.uploadDocsLastDate?.stringValue
            plan.planRenewed = membership.activity == "RW"
            //                    plan.products = membership.assets?.map({ $0.getProduct(from: $0, with: applianceCategories) })
            
            if plan.isIDFence {
                plan.planTypeImage = #imageLiteral(resourceName: "idfence")
                plan.appliancesName = Strings.Global.idFence
                // Will be used to open dashboard
//                        plan.memUUID = membership.memUUID
            } else {
                plan.planTypeImage = #imageLiteral(resourceName: "wallet_buy")
                plan.appliancesName = Strings.Global.wallet
            }
            
            plan.rank = 2
            walletPlans.append(plan)
        }
        
        return walletPlans
    }
    
    // For WHC Plans.
    func getWHCPlans(for applianceCategories: [HomeApplianceSubCategories]) -> [PlanDetailAndSRModel] {
        var whcPlans: [PlanDetailAndSRModel] = []
        
        for membership in self {
            // It is for WHC pending memberships
            var plan = PlanDetailAndSRModel(membership)
            plan.whcPlanStatus = membership.getWhcStatus()
            plan.isWhc = true
            plan.customer = membership.customers
            plan.membershipId = membership.memId?.description
            plan.planName = Strings.HATabScene.Whc.whcPlanName
            plan.category = Strings.Global.whc
            plan.maxQuantity = membership.plan?.allowedMaxQuantity
            plan.products = membership.assets?.map({ $0.getProduct(from: $0, with: applianceCategories) })
            plan.planPurchaseDate = membership.purchaseDate
            plan.planTypeName = Strings.MembershipRevamp.applianceDetails // test
            plan.planTypeImage = #imageLiteral(resourceName: "homeServ")
            
            /* Will have to change when SR integration is done.*/
            /* In future we might need to store these priorities in JSON based on card. */
            switch plan.whcPlanStatus {
            case .pendingWillBeCancelled:
                plan.rank = 6
            case .pendingWithEarliestContact:
                plan.rank = 7
            case .pendingWithDelay(_):
                plan.rank = 8
            case .pending(_):
                plan.rank = 9
            case .inspectionCancelled(_):
                plan.rank = 10
            case .awaiting:
                plan.rank = 14
            case .scheduled:
                plan.rank = 15
            case .membershipCancelled:
                plan.rank = 25
            case .failed:
                plan.rank = 26
            case .inspectionCancelledNonReschedule:
                plan.rank = 27
            default:
                plan.rank = 100
            }
            
            /* You can remove this condition, no need of this now. */
            //                    if plan.isTrial {
            //                        plan.buttonTitle = Strings.HATabScene.upgradeNow
            //                        plan.warningMessage = Strings.HATabScene.upgradeError
            //                    }
            whcPlans.append(plan)
        }
        return whcPlans
    }
    
    // For EW Plans.
    func getEWPlans(for applianceCategories: [HomeApplianceSubCategories], memToSRMapping:[String: [SearchService]]) -> [PlanDetailAndSRModel] {
        var ewPlans: [PlanDetailAndSRModel] = []
        for membership in self {
            var plan = PlanDetailAndSRModel(membership)
            plan.isPendingPlan = true
            plan.customer = membership.customers
            plan.membershipId = membership.memId?.description
            plan.category = Strings.Global.ew
            plan.salesPrice = membership.salesPrice
            plan.products = membership.assets?.map({ $0.getProduct(from: $0, with: applianceCategories) })
            plan.appliancesName = plan.products?.first?.productName
            plan.planTypeName = "My \(plan.products?.first?.productName ?? "Appliance") Details"
            plan.planTypeImage = #imageLiteral(resourceName: "dlsEW")
            plan.planTypeImageUrlStr = Utilities.getImageStrForProductName(plan.products?.first?.productCode ?? "", applianceCategories: applianceCategories)
            
            /* For Renewal Phase 5
             
             // There is only one case in EW i.e all N --> "X"
             if let status = membership.renewalStatus, let activity = membership.renewalActivity {
             if (status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected) {
             if activity == Constants.RenewalActivityFlags.buyNewPlan {
             plan.canRenew = true
             plan.buttonTitle = Strings.HATabScene.upgradeToHomeAssist
             plan.warningMessage = Strings.HATabScene.memExpiry
             }
             }
             } else if status == Constants.RenewalStatusFlags.pending {
             if activity == Constants.RenewalActivityFlags.renewalWindow {
             plan.isInspectionForRenew = true
             }
             } */
            
            /* Integrate SR view model */
            if let serviceReqs = memToSRMapping[plan.membershipId ?? ""] {
                if membership.enableFileClaim == false, membership.claims == nil {
                    //Do something
                }else {
                    plan.serviceRequests = serviceReqs.map({SRListCompletedViewModel($0)})
                }
            }
            
            /* You can remove this condition, no need of this now. */
            //                    if plan.isTrial {
            //                        plan.buttonTitle = Strings.MembershipRevamp.upgradeNow
            //                        plan.warningMessage = Strings.HATabScene.upgradeError
            //                    }
            
            if plan.hasUserPendencyInClaim() {
                plan.rank = 4
            } else if plan.hasUserPendencyAndRenewaInClaim() {
                plan.rank = 5
            } else if plan.hasOAPendencyInClaim() {
                plan.rank = 13
            } else {
                plan.rank = 24
            }
            
            ewPlans.append(plan)
        }
        return ewPlans
    }
}
