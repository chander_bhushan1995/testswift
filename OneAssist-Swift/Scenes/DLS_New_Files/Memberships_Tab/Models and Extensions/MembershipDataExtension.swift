//
//  MembershipDataExtension.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 05/12/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

extension CustomerMembershipDetails {
    // FIXME: Check why were there two methods for getWhcStatus??
    func getWhcStatus() -> WhcPlanStatus {
        var status: WhcPlanStatus = .awaiting
        if membershipStatus == Constants.MembershipStatus.cancelled { // if Membership Expired
            status = .membershipCancelled
        } else if membershipStatus == Constants.MembershipStatus.active { // if Membership Activate Successfully
            let day = gregorianCalendar.dateComponents([.day], from: Date.getDate(from: startDate ?? "", with: .serverDateFormat), to: .currentDate()).day ?? 0
            status = day < 2 ? .activatedSuccesfully(membershipData: self) : .details(membershipData: self)
        }
        return status
    }
    
    func getWhcStatusUpdated() -> WhcPlanStatus {
        var status: WhcPlanStatus = .awaiting
        if membershipStatus == Constants.MembershipStatus.cancelled { // if Membership Cancelled
            status = .membershipCancelled
        } else if (membershipStatus == Constants.MembershipStatus.active || membershipStatus == Constants.MembershipStatus.inactive) { // if "Membership Activate Successfully" OR "Renewal Expired Grace Period".
            let day = gregorianCalendar.dateComponents([.day], from: Date.getDate(from: startDate ?? "", with: .serverDateFormat), to: .currentDate()).day ?? 0
            status = day < 2 ? .activatedSuccesfully(membershipData: self) : .details(membershipData: self)
        }
        return status
    }
    
    func serviceCountText() -> String {
        if let services = plan?.services?.filter({$0.noOfVisitsLeft != nil}), services.count > 0 {
            var txtPMS = ""
            var txtSOP = ""
            for service in services {
                if let number = service.noOfVisitsLeft, Int(truncating: number) < Constants.unlimitedServiceCount {
                    if service.serviceName == Constants.Services.pms.rawValue {
                        txtPMS = "Maintenance services left - \(number)"
                    } else if service.serviceName == Constants.Services.breakdown.rawValue {
                        txtSOP = "Repair services left - \(number)"
                    }
                }
            }
            
            var txt = "\(txtPMS)\n\n\(txtSOP)"
            
            txt = txt.trimmingCharacters(in: .whitespacesAndNewlines)
            return txt
        } else {
            return ""
        }
    }
}

extension Array where Element: CustomerMembershipDetails {
    // For Mobile Plans
    func getMobilePlans(with memToSRMapping:[String: [SearchService]], buybackData: [BuyBackStatus]) -> [PlanDetailAndSRModel] {
        var mobilePlans: [PlanDetailAndSRModel] = []
        for membership in self {
            /* We don't keep pending memberships in case of mobile, because there is no FD flow. */
            /* Now FD flow came, so we will keep track of it. */
            var plan = PlanDetailAndSRModel(membership)
            plan.coverAmount = membership.coverAmount
            plan.salesPrice = membership.salesPrice
            plan.graceDays = membership.graceDays
            plan.category = Constants.Category.personalElectronics // "PE"
            plan.planPurchaseDate = membership.purchaseDate
            plan.enableFileClaim = membership.enableFileClaim
            plan.planTypeName = Strings.MobileTabScene.myMobileDetails
            plan.products = membership.assets?.filter({ $0.prodCode != "WP01" }).map({ $0.getProduct(from: $0,with: []) })
            plan.isABBMembership = membership.isABBMembership()
            plan.isABBOnlyMembership = membership.isABBOnlyMembership()
            plan.isABBTenureActive = membership.isABBTenureActive()
            plan.buybackData = buybackData.first(where: { $0.membershipId == plan.membershipId })
            
            if membership.categories?.count ?? 0 > 1 {
                /* It is combo plan case. Conditions are added here because everything else has to be like PE only. */
                plan.isCombo = true
                plan.planTypeImage = #imageLiteral(resourceName: "combo")
                //                        plan.appliancesName = "\(Strings.Global.walletAssist), \(plan.products?.first?.brand ?? "") \(plan.products?.first?.model ?? "")"
                plan.appliancesName = "\(Strings.Global.mobile) & \(Strings.Global.wallet)"
            } else {
                plan.planTypeImage = #imageLiteral(resourceName: "dlsMobile")
                plan.appliancesName = "\(plan.products?.first?.brand ?? "") \(plan.products?.first?.model ?? "")"
                
                if let productCode = plan.products?.first?.productCode,  let imageName = Destination(rawValue: productCode)?.iconName{
                    plan.planTypeImage = UIImage(named: imageName)
                }
            }
            
            let serviceText = membership.serviceCountText()
            if !serviceText.isEmpty {
                plan.serviceText = serviceText
                
            }
            
            if let status = membership.renewalStatus, let activity = membership.renewalActivity {
                if (status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected) && activity == Constants.RenewalActivityFlags.renewalWindow {
                    plan.canRenew = true
                    plan.buttonTitle = Strings.MembershipRevamp.renewNow
                    plan.warningMessage = Strings.MobileTabScene.planExpiredError
                }
            }
            
            /* Integrate SR view model */
            if let serviceReqs = memToSRMapping[plan.membershipId ?? ""] {
                if membership.enableFileClaim == false, membership.claims == nil {
                    //Do something
                }else {
                    plan.serviceRequests = serviceReqs.map({SRListCompletedViewModel($0)})
                }
            }
            
            /* You can remove this condition, no need of this now. */
            //                    if plan.isTrial {
            //                        plan.buttonTitle = Strings.MembershipRevamp.upgradeNow
            //                        plan.warningMessage = Strings.MobileTabScene.upgradeError
            //                    }
            
            if plan.hasUserPendencyInClaim() {
                plan.rank = 0
            } else if plan.hasUserPendencyAndRenewaInClaim() {
                plan.rank = 1
            } else if plan.hasOAPendencyInClaim() {
                plan.rank = 11
            } else if plan.canRenew {
                plan.rank = 16
            } else {
                plan.rank = 20
            }
            mobilePlans.append(plan)
        }
        
        return mobilePlans
    }
    
    // For Wallet Plans. Also checking for IDFence.
    func getWalletPlans() -> [PlanDetailAndSRModel] {
        var walletPlans: [PlanDetailAndSRModel] = []
        for membership in self {
            /* We don't keep pending memberships in case of wallet, because there must not be any delay in backend. */
            var plan = PlanDetailAndSRModel(membership)            
            plan.customer = membership.customers
            plan.memStatus = membership.membershipStatus
            plan.coverAmount = membership.coverAmount
            plan.salesPrice = membership.salesPrice
            plan.graceDays = membership.graceDays
            plan.category = Constants.Category.finance // "F"
            plan.isIDFence = membership.checkIsMembershipIDFence() // checking for IDFence.
            plan.planPurchaseDate = membership.purchaseDate
            plan.planTypeName = Strings.WalletTabScene.myCards
            plan.siStatus = membership.siStatus
            // plan.enableFileClaim = membership.enableFileClaim
            
            /* It is normal wallet case. */
            plan.products = membership.assets?.filter({ $0.prodCode != "MP01" }).map({ $0.getProduct(from: $0, with: []) })
            
            if plan.isIDFence {
                plan.planTypeImage = #imageLiteral(resourceName: "idfence")
                plan.appliancesName = Strings.Global.idFence
                plan.updateIdFenceText()
                plan.memUUID = membership.memUUID // Will be used to open dashboard
            } else {
                plan.planTypeImage = #imageLiteral(resourceName: "wallet_buy")
                plan.appliancesName = Strings.Global.wallet
                if [Constants.RenewalStatusFlags.notStarted, Constants.RenewalStatusFlags.rejected].contains(membership.renewalStatus) && membership.renewalActivity == Constants.RenewalActivityFlags.renewalWindow {
                    plan.canRenew = true
                    plan.buttonTitle = Strings.MembershipRevamp.renewNow
                    plan.warningMessage = Strings.WalletTabScene.planExpiredError
                }
            }
            
            /*
             if let prodName = plan.products?.first?.productName {
             plan.appliancesName = prodName
             } else {
             // In case there is no card added in wallet plan.
             plan.appliancesName = Strings.Global.walletAssist
             } */
            
            /* No SR view model integration in this because no raise claim in Wallet. */
            if plan.canRenew {
                plan.rank = 17
            } else {
                plan.rank = 21
            }
            
            walletPlans.append(plan)
        }
        return walletPlans
    }
    
    // For WHC Plans.
    func getWHCPlans(for applianceCategories: [HomeApplianceSubCategories], memToSRMapping:[String: [SearchService]]) -> [PlanDetailAndSRModel] {
        var whcPlans: [PlanDetailAndSRModel] = []
        for membership in self {
            // It is for WHC active and renewal expired memberships
            var plan = PlanDetailAndSRModel(membership)
            plan.whcPlanStatus = membership.getWhcStatusUpdated()
            plan.isWhc = true
            plan.customer = membership.customers
            plan.coverAmount = membership.coverAmount
            plan.salesPrice = membership.salesPrice
            plan.graceDays = membership.graceDays
            plan.category = Strings.Global.whc // "WHC"
            plan.maxQuantity = membership.plan?.allowedMaxQuantity
            plan.enableFileClaim = membership.enableFileClaim
            
            plan.products = membership.assets?.map({ $0.getProduct(from: $0, with: applianceCategories) })
            //plan.planTypeImageUrlStr = Utilities.getImageStrForProductName(plan.products?.first?.productCode ?? "", applianceCategories: applianceCategories)
            
            if let products = plan.products {
                if products.count == 1 {
                    plan.appliancesName = products[0].productName ?? ""
                } else if products.count == 2 {
                    plan.appliancesName = "\(products[0].productName ?? ""), \(products[1].productName ?? "")"
                } else {
                    plan.appliancesName = "\(products[0].productName ?? ""), \(products[1].productName ?? ""), ... +\(products.count - 2) more"
                }
            }
            
            let serviceText = membership.serviceCountText()
            if !serviceText.isEmpty {
                plan.serviceText = serviceText

            }
            
            let applianceCovered = "\((plan.products?.count ?? 0) > 1 ? "appliances are" : "appliance is") covered"
            
            plan.planName = Utilities.getSOPPlanName(membership) ??  "\(Strings.HATabScene.Whc.whcPlanName) - \(plan.products?.count ?? 0) \(applianceCovered)"
            
            plan.planPurchaseDate = membership.purchaseDate
            plan.planTypeName = Strings.MembershipRevamp.applianceDetails // test
            plan.planTypeImage = #imageLiteral(resourceName: "homeServ")
            
            // Still things have to be added
            if let status = membership.renewalStatus, let activity = membership.renewalActivity {
                if status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected {
                    if activity == Constants.RenewalActivityFlags.renewalWindow {
                        plan.canRenew = true
                        plan.buttonTitle = Strings.MembershipRevamp.renewNow
                        plan.warningMessage = Strings.HATabScene.memExpiry
                    }
                    /*  For Renewal Phase 5
                     else if activity == Constants.RenewalActivityFlags.changePlan {
                     plan.canRenew = true
                     plan.buttonTitle = Strings.HATabScene.chooseAnotherPlan
                     plan.warningMessage = Strings.HATabScene.memExpiry
                     } else if activity == Constants.RenewalActivityFlags.buyNewPlan {
                     plan.canRenew = true
                     plan.buttonTitle = Strings.HATabScene.buyNewPlan
                     plan.warningMessage = Strings.HATabScene.memExpiry
                     } */
                    
                    plan.amountSaved = membership.claims?.filter({ Date.getDate(from: membership.startDate ?? "") <= (DateFormatter.scheduleFormat.date(from: $0.createdOn ?? "") ?? Date.currentDate())
                    }).reduce(0.0, { $0 + (($1.workflowData?.repairAssessment?.costToCompany?.floatValue) ?? 0.0) }) ?? 0.0
                    
                } else if status == Constants.RenewalStatusFlags.pending || status == Constants.RenewalStatusFlags.initiated {
                    plan.isInspectionForRenew = true
                }
            }
            
            /* Integrate SR view model. */
            if let serviceReqs = memToSRMapping[plan.membershipId ?? ""] {
                if membership.enableFileClaim == false, membership.claims == nil {
                    //Do something
                }else {
                    plan.serviceRequests = serviceReqs.map({SRListCompletedViewModel($0)})
                }
            }
            
            
            if plan.hasUserPendencyInClaim() {
                plan.rank = 2
            } else if plan.hasUserPendencyAndRenewaInClaim() {
                plan.rank = 3
            } else if plan.hasOAPendencyInClaim() {
                plan.rank = 12
            } else if plan.canRenew {
                plan.rank = 18
            } else if plan.isInspectionForRenew {
                plan.rank = 19
            } else {
                switch plan.whcPlanStatus {
                case .activatedSuccesfully(_):
                    plan.rank = 22
                case .details(_):
                    plan.rank = 23
                default:
                    plan.rank = 100
                }
            }
            
            /* You can remove this condition, no need of this now. */
            //                    if plan.isTrial {
            //                        plan.buttonTitle = Strings.MembershipRevamp.upgradeNow
            //                        plan.warningMessage = Strings.HATabScene.upgradeError
            //                    }
            whcPlans.append(plan)
        }
        return whcPlans
    }
    
    // For EW Plans.
    func getEWPlans(for applianceCategories: [HomeApplianceSubCategories], memToSRMapping:[String: [SearchService]]) -> [PlanDetailAndSRModel] {
        var ewPlans: [PlanDetailAndSRModel] = []
        for membership in self {
            var plan = PlanDetailAndSRModel(membership)
            plan.customer = membership.customers
            plan.category = Strings.Global.ew
            plan.coverAmount = membership.coverAmount
            plan.salesPrice = membership.salesPrice
            plan.graceDays = membership.graceDays
            plan.enableFileClaim = membership.enableFileClaim
            plan.products = membership.assets?.map({ $0.getProduct(from: $0, with: applianceCategories) })
            plan.appliancesName = plan.products?.first?.productName
            plan.planPurchaseDate = membership.purchaseDate
            plan.planTypeName = "My \(plan.products?.first?.productName ?? "Appliance") Details"
            plan.planTypeImage = #imageLiteral(resourceName: "dlsEW")
            plan.planTypeImageUrlStr = Utilities.getImageStrForProductName(plan.products?.first?.productCode ?? "", applianceCategories: applianceCategories)
            
            let serviceText = membership.serviceCountText()
            if !serviceText.isEmpty {
                plan.serviceText = serviceText
                
            }
            
            /* For Renewal Phase 5
             
             // There is only one case in EW i.e all N --> "X"
             if let status = membership.renewalStatus, let activity = membership.renewalActivity {
             if (status == Constants.RenewalStatusFlags.notStarted || status == Constants.RenewalStatusFlags.rejected) {
             if activity == Constants.RenewalActivityFlags.buyNewPlan {
             plan.canRenew = true
             plan.buttonTitle = Strings.HATabScene.upgradeToHomeAssist
             plan.warningMessage = Strings.HATabScene.memExpiry
             }
             }
             } else if status == Constants.RenewalStatusFlags.pending {
             if activity == Constants.RenewalActivityFlags.renewalWindow {
             plan.isInspectionForRenew = true
             }
             } */
            
            /* Integrate SR view model */
            if let serviceReqs = memToSRMapping[plan.membershipId ?? ""] {
                if membership.enableFileClaim == false, membership.claims == nil {
                    //Do something
                }else {
                    plan.serviceRequests = serviceReqs.map({SRListCompletedViewModel($0)})
                }
            }
            
            /* You can remove this condition, no need of this now. */
            //                    if plan.isTrial {
            //                        plan.buttonTitle = Strings.MembershipRevamp.upgradeNow
            //                        plan.warningMessage = Strings.HATabScene.upgradeError
            //                    }
            
            
            if plan.hasUserPendencyInClaim() {
                plan.rank = 4
            } else if plan.hasUserPendencyAndRenewaInClaim() {
                plan.rank = 5
            } else if plan.hasOAPendencyInClaim() {
                plan.rank = 13
            } else {
                plan.rank = 24
            }
            
            ewPlans.append(plan)
        }
        return ewPlans
    }
}
