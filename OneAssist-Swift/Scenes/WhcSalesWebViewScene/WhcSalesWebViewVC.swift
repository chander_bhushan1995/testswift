//
//  WhcSalesWebViewVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

class WhcSalesWebViewVC: SecureWebViewVC {
    // MARK:- private methods
    override func initializeView() {
        title = screenTitle ?? Strings.PaymentWebViewScene.title
        if let url = URL(string: urlString) {
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url))
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        } else {
            showAlert(message: Strings.Global.somethingWrong)
        }
    }
    
    // MARK:- Action Methods
    override func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("shouldStartLoadWith: \(webView.url?.absoluteString ?? "")")
        if let url = webView.url, loadingUnvalidatedHTTPSPage {
            connection = NSURLConnection(request: URLRequest(url: url), delegate: self)
            connection.start()
            decisionHandler(.cancel)
        } else if let str = webView.url?.absoluteString.lowercased(), str.contains("timeline") {
            // for pendency cases
            handleNavBackButton()
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("URL Finished: \(webView.url?.absoluteString ?? "")")
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let str = webView.url?.absoluteString.lowercased(), str.contains("timeline") {
            // for pendency cases
            handleNavBackButton()
        }
    }
}
