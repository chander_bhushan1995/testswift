//
//  ScheduleVisitPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol ScheduleVisitDisplayLogic: class {
    func presentTimeSlots(_ items: [ScheduleVisitTimeSlotViewModel], dateButtonIndex: Int?, selectedDate: Date, receivedDate: Date, firstLoad: Bool)
    func displayError(_ error: String)
    func displayScheduleInspectionResponse(_ response: ScheduleInspectionResponseDTO)
    func displayRescheduleInspectionResponse(_ response: RescheduleInspectionResponseDTO)
}

class ScheduleVisitPresenter: BasePresenter, ScheduleVisitPresentationLogic {
    weak var viewController: ScheduleVisitDisplayLogic?
    func recievedTimeSlots(request:InspectionTimeSlotsRequestDTO,response: InspectionTimeSlotsResponseDTO?, error: Error?, dateButtonIndex: Int?, firstLoad: Bool) {
        do {
            try checkError(response, error: error)
            
            var items: [ScheduleVisitTimeSlotViewModel] = []

            if let slots = response?.data?.serviceSlots {
                for slot in slots {
                    let fromDate = DateFormatter.time24period.date(from: slot.startTime ?? "")
                    let toDate = DateFormatter.time24period.date(from: slot.endTime ?? "")
                    if fromDate != nil && toDate != nil {
                        let fromDateNew = DateFormatter.time12period.string(from: fromDate!)
                        let toDateNew = DateFormatter.time12period.string(from: toDate!)
                        let item = ScheduleVisitTimeSlotViewModel()
                        item.endTime = slot.endTime
                        item.startTime = slot.startTime
                        item.slot = "\(fromDateNew) - \(toDateNew)"
                        items.append(item)
                    }
                }
            }
            let selectedDate = Date(string: request.serviceRequestDate, formatter: .slotDateFormat) ?? Date.currentDate()
            let receivedDate = Date(string: response?.data?.serviceRequestDate ?? request.serviceRequestDate, formatter: .slotDateFormat) ?? selectedDate
            viewController?.presentTimeSlots(items, dateButtonIndex: dateButtonIndex, selectedDate: selectedDate, receivedDate: receivedDate, firstLoad: firstLoad)
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func recievedScheduledInspectionResponse(response: ScheduleInspectionResponseDTO?, error: Error?) {
        
        do {
            try checkError(response, error: error)
            viewController?.displayScheduleInspectionResponse(response!)
            
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
    
    func recievedRescheduledInspectionResponse(response: RescheduleInspectionResponseDTO?, error: Error?) {
        
        do {
            try checkError(response, error: error)
            viewController?.displayRescheduleInspectionResponse(response!)
            
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
}
