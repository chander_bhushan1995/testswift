//
//  ScheduleVisitVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol ScheduleVisitDelegate: class {
    func clickedScheduleVisit(date: Date, slot: ScheduleVisitTimeSlotViewModel, serviceReqId: String?, crmTrackingNumber: String?)
}

protocol ScheduleVisitBusinessLogic {
    func getTimeSlots(selectedDate: Date, dateButtonIndex: Int?, pincode : String? ,serviceType:String?, serviceRequestSourceType: String?, buCode: String?, bpCode: String?, memId: String?,assetId: String?, productCode: String?, srId: String?, firstLoad: Bool)
    func scheduleInspection(activationCode: String?, addressDetail: AddressDetail?, tempCustomerDetail: TempCustomerDetail?, date: Date?, slot: ScheduleVisitTimeSlotViewModel?)
    func rescheduleInspection(serviceReqId: String, date: Date?, slot: ScheduleVisitTimeSlotViewModel?)
}

// test
class AddressDetail {
    var name = ""
    var address = ""
    var address2 = ""
    var pincode : String?
    var city = ""
    var state = ""
    var cityCode = ""
    var stateCode = ""
    init(pincode : String? = nil) {
        self.pincode = pincode
    }
}
// test

class ScheduleVisitVC: BaseVC {
    var interactor: ScheduleVisitBusinessLogic?
    
    enum SelectionDay:String {
        case Today
        case Tomorrow
        case Yesterday
    }
    
    //MARK:- outlets
    @IBOutlet weak var btnToday: OAPrimaryButton!{
        didSet{
            btnToday.setTitle(firstDay.rawValue, for: .normal)
        }
    }
    @IBOutlet weak var btnTomorrow: OAPrimaryButton!{
        didSet{
            btnTomorrow.setTitle(secondDay.rawValue, for: .normal)
        }
    }
    
    @IBOutlet weak var labelHeading: H3BoldLabel!
    @IBOutlet weak var labelCollectionHeader: SupportingTextRegularGreyLabel!
    @IBOutlet weak var labelVisitError: BodyTextRegularGreyLabel!
    @IBOutlet weak var buttonPrimary: OAPrimaryButton!
    @IBOutlet weak var collectionViewSlots: UICollectionView!
    @IBOutlet weak var labelSlotCollectionHeader: H3RegularBlackLabel!
    
    @IBOutlet weak var btnCalendar: OAPrimaryButton!
    @IBOutlet weak var dateContainerView: UIView!
    @IBOutlet weak var dateContainerStackView: UIStackView!
    @IBOutlet weak var lblDate: BodyTextBoldBlackLabel!
    @IBOutlet weak var errorLabel: SupportingTextRegularGreyLabel!
    
    public var firstDay: SelectionDay = .Today {
        didSet {
            btnToday.setTitle(firstDay.rawValue, for: .normal)
        }
    }
    
    public var secondDay: SelectionDay = .Tomorrow {
        didSet {
            btnTomorrow.setTitle(secondDay.rawValue, for: .normal)
        }
    }
    
    public var selectedDay: SelectionDay  = .Today {
        didSet {
            setDateLabel(selectedDay)
        }
    }
    
    /// Minimum (oldest) allowed date and time
    public var minimumDate:Date?
    
    /// Maximum allowed date and time
    public var maximumDate:Date?

    //...
    var slots: [ScheduleVisitTimeSlotViewModel] = []
    var date = Date()
    var serviceReqId: String?
    var crmTrackingNumber: String?
    var serviceType : String?
    var serviceRequestSourceType: String?
    var memId: String?
    var assetId: String?
    var bpCode: String?
    var buCode: String?
    var productCode: String?
    fileprivate var previousProductCode: String?
    
    var activationCode: String?
    var addressDetail: AddressDetail?
    var customerDetails: TempCustomerDetail?
    var purchasedDate: Date = Date.currentDate()
    
    var selectedSlot: ScheduleVisitTimeSlotViewModel?
    var selectedItem: ScheduleVisitCollectionViewModel?
    var selectedDate: Date?
    var getTomorroDataOnce: Bool = false //use this if today slot is full for first time
    var getAPiCalledFirstTime: Bool = false // use this for first time called
    weak var scheduleVisitDelegate: ScheduleVisitDelegate?
    
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        EventTracking.shared.addScreenTracking(with: .SR_Schedule_Visit)
        if let productCode = self.productCode {
            if productCode == self.previousProductCode {
                getAPiCalledFirstTime = false
            }
        }
        if getAPiCalledFirstTime { //reset view state and hit API at first time
            btnToday.tag = OAButtonTagType.Primary.enabledButton.rawValue
            btnTomorrow.tag = OAButtonTagType.Primary.outlineButton.rawValue
            dateContainerStackView.isHidden = false
            dateContainerView.isHidden = true
            getTomorroDataOnce = false
            showLoader()
            self.getTimeSlotsData(selectedDate: date, dateButtonIndex: 1, firstLoad: true)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        getAPiCalledFirstTime = false //reset this for first call(using only for reaise SR scenario)
    }
    
    // MARK:- private methods
    private func initializeView() {
        collectionViewSlots.register(nib: Constants.CellIdentifiers.scheduleVisitTimeSlotCell
            , forCellReuseIdentifier: Constants.CellIdentifiers.scheduleVisitTimeSlotCell)
        
        title = Strings.ScheduleVisitScene.title
        labelHeading.text = Strings.ScheduleVisitScene.heading
        labelCollectionHeader.text = Strings.ScheduleVisitScene.collectionHeading
        labelSlotCollectionHeader.text = Strings.ScheduleVisitScene.collectionSlotHeading
        labelVisitError.text = Strings.ScheduleVisitScene.visitError
        errorLabel.text = Strings.ScheduleVisitScene.selectSlotError
        errorLabel.textColor = UIColor.errorText
        
        if serviceReqId != nil {
            buttonPrimary.setTitle(Strings.ScheduleVisitScene.title, for: .normal)
        } else {
            buttonPrimary.setTitle(Strings.ScheduleVisitScene.primaryAction, for: .normal)
        }
        buttonPrimary.isEnabled = true
        labelSlotCollectionHeader.isHidden = true
        collectionViewSlots.isHidden = true
        labelVisitError.isHidden = true
        errorLabel.isHidden = true
        setDateLabel(firstDay)
        maximumDate = date.addingTimeInterval(365*24*60*60)
        minimumDate = date
    }
    
    func getTimeSlotsData(selectedDate: Date, dateButtonIndex: Int?, firstLoad: Bool) {
        interactor?.getTimeSlots(selectedDate: selectedDate, dateButtonIndex: dateButtonIndex, pincode: self.addressDetail?.pincode, serviceType: self.serviceType, serviceRequestSourceType: self.serviceRequestSourceType, buCode: self.buCode, bpCode: self.bpCode, memId: self.memId, assetId: self.assetId, productCode: self.productCode, srId: self.serviceReqId, firstLoad: firstLoad)
    }

    
    func showLoader() {
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        labelSlotCollectionHeader.isHidden = true
        collectionViewSlots.isHidden = true
        labelVisitError.isHidden = true
    }
    
    func hideLoader(hasSlots: Bool) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        labelVisitError.isHidden = hasSlots
        labelSlotCollectionHeader.isHidden = !hasSlots
        collectionViewSlots.isHidden = !hasSlots
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnPrimary(_ sender: Any) {

        guard let selectedSlot = selectedSlot else {
            errorLabel.isHidden = false
            return
        }
        
        if serviceReqId != nil {
            EventTracking.shared.eventTracking(name: .submitRequestTappedAfterReschedule)
            
            EventTracking.shared.eventTracking(name: .ScheduleVisit, [.service: serviceType ?? "", .subcategory: CacheManager.shared.eventProductName ?? ""])
            
        } else {
            EventTracking.shared.eventTracking(name: .SRReschedule, [.service: serviceType ?? "", .subcategory: CacheManager.shared.eventProductName ?? ""])
        }
        guard scheduleVisitDelegate == nil else {
            
            scheduleVisitDelegate?.clickedScheduleVisit(date: (selectedDate) ?? date, slot: selectedSlot, serviceReqId: serviceReqId, crmTrackingNumber: crmTrackingNumber)
            // route on basis of claim enum
            return
        }
        
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        
        if serviceReqId != nil {
            interactor?.rescheduleInspection(serviceReqId: serviceReqId!, date: selectedDate, slot: selectedSlot)
        } else {
            interactor?.scheduleInspection(activationCode: self.activationCode, addressDetail: self.addressDetail, tempCustomerDetail: self.customerDetails, date: selectedDate, slot: selectedSlot)
        }
        
        var totalTime = ""
        if let dateForEvent = selectedDate {
            let hourForEvent = String(gregorianCalendar.component(.hour, from: dateForEvent))
            let minuteForEvent = String(gregorianCalendar.component(.minute,from:dateForEvent))
            let secondForEvent = String(gregorianCalendar.component(.second,from:dateForEvent))
            
            totalTime = hourForEvent + ":" + minuteForEvent + ":" + secondForEvent
            EventTracking.shared.eventTracking(name: .SRVisitDT_Submit ,[.date: dateForEvent,.location:"WHC Inspection",.time:totalTime])
        }
    }
    
    fileprivate func setDateLabel(_ day: SelectionDay) {
        var labelText = date.string(with: .slotDateFormat)
        switch day {
        case .Today: labelText = date.string(with: .slotDateFormat)
        case .Tomorrow: labelText = Date.tomorrow.string(with: .slotDateFormat)
        case .Yesterday: labelText = Date.yesterday.string(with: .slotDateFormat)
        }
        lblDate.text = labelText
    }
    
    fileprivate func presentDatePicker() {
        let datePicker = OADatePicker()
        if let maximumDate = maximumDate {datePicker.maximumDate = maximumDate}
        if let minimumDate = minimumDate {datePicker.minimumDate = minimumDate}
        if let date = DateFormatter.slotDateFormat.date(from: self.lblDate.text ?? ""){
            datePicker.date = date.toLocalTime()
        }
        //datePicker.configure()
        datePicker.delegate = self
        let basePickerVC = BasePickerVC(withView:datePicker, height: Utilities.datePickerHeight)
        Utilities.topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    @IBAction func clickBtnTomorrow(_ sender: Any) { //first btn
        btnTomorrow.tag = OAButtonTagType.Primary.enabledButton.rawValue
        if btnToday.isEnabled {
            btnToday.tag = OAButtonTagType.Primary.outlineButton.rawValue
        }
        setDateLabel(secondDay)
        showLoader()
        getTimeSlotsData(selectedDate: .tomorrow, dateButtonIndex: 2, firstLoad: false)
    }
    
    @IBAction func clickBtnToday(_ sender: Any) { //second btn
        btnToday.tag = OAButtonTagType.Primary.enabledButton.rawValue
        btnTomorrow.tag = OAButtonTagType.Primary.outlineButton.rawValue
        setDateLabel(firstDay)
        showLoader()
        getTimeSlotsData(selectedDate: date, dateButtonIndex: 1, firstLoad: false)
    }
    
    @IBAction func clickDatePickerBtn(_ sender: Any) {
        presentDatePicker()
    }
}

// MARK:- Display Logic Conformance
extension ScheduleVisitVC: ScheduleVisitDisplayLogic {
    
    func presentTimeSlots(_ items: [ScheduleVisitTimeSlotViewModel], dateButtonIndex: Int?, selectedDate: Date, receivedDate: Date, firstLoad: Bool) {
        previousProductCode = self.productCode

        self.selectedDate = firstLoad ? receivedDate : selectedDate
        self.slots = items
        let cachedIndex = slots.enumerated().first(where: { $0.element.startTime == selectedSlot?.startTime && $0.element.endTime == selectedSlot?.endTime })?.offset
        let showSlots = !slots.isEmpty && (firstLoad || selectedDate == receivedDate)
        
        if showSlots {
            if receivedDate == dateInFormat(date) {
                if dateButtonIndex == 1 {
                    btnToday.isEnabled = true
                    btnToday.tag = OAButtonTagType.Primary.enabledButton.rawValue
                    btnTomorrow.tag = OAButtonTagType.Primary.outlineButton.rawValue
                }
            } else if receivedDate == dateInFormat(.tomorrow) {
                if dateButtonIndex == 2 {
                    btnTomorrow.isEnabled = true
                    btnTomorrow.tag = OAButtonTagType.Primary.enabledButton.rawValue
                } else if dateButtonIndex == 1 && firstLoad {
                    btnTomorrow.isEnabled = true
                    btnTomorrow.tag = OAButtonTagType.Primary.enabledButton.rawValue
                    btnToday.isEnabled = false
                    btnToday.tag = OAButtonTagType.Primary.disabledButton.rawValue
                }
            } else {
                setDate(dateInFormat(receivedDate))
            }
        }
        
        hideLoader(hasSlots: showSlots)
        collectionViewSlots.reloadData()
        
        if let index = cachedIndex, showSlots {
            collectionView(collectionViewSlots, didSelectItemAt: IndexPath(row: index, section: 0))
        } else {
            selectedSlot = nil
        }
    }
    
    func dateInFormat(_ date: Date) -> Date {
        return Date(string: date.string(with: .slotDateFormat), formatter: .slotDateFormat) ?? Date.currentDate()
    }
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayScheduleInspectionResponse(_ response: ScheduleInspectionResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        routeToInspectionSuccess()
    }
    
    func displayRescheduleInspectionResponse(_ response: RescheduleInspectionResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if serviceReqId == nil {
            routeToInspectionSuccess()
        } else {
            NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
            if let srRequestType = self.serviceRequestSourceType, srRequestType == Strings.SODConstants.categoryName{
                ChatBridge.refreshHomeScreen()
            }
            navigationController?.popViewController(animated: true)
        }
    }
}

// MARK:- Collection DataSource Conformance
extension ScheduleVisitVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifiers.scheduleVisitTimeSlotCell, for: indexPath) as! ScheduleVisitTimeSlotCell
        cell.set(viewModel: slots[indexPath.row])
        return cell
    }
}

extension ScheduleVisitVC: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewSlots {
            let width = (UIScreen.main.bounds.width - 46 - 19) / 2
            return CGSize(width: width, height: 36)
        }
        return CGSize(width: 57, height: 92)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        errorLabel.isHidden = true

        let item = slots[indexPath.row]
        item.selected = true
        selectedSlot = item
        
        for tuple in slots.enumerated() {
            if tuple.offset != indexPath.row {
                slots[tuple.offset].selected = false
            }
        }
        collectionViewSlots.reloadData()
    }
}

// MARK:- Configuration Logic
extension ScheduleVisitVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = ScheduleVisitInteractor()
        let presenter = ScheduleVisitPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension ScheduleVisitVC {
    
    func routeToInspectionSuccess() {
        let vc = InspectionSuccessVC()
        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- OADatePickerDelegate
extension ScheduleVisitVC: OADatePickerDelegate {
    
    func dateSelected(_ date: Date) {
        setDate(date)
        Utilities.topMostPresenterViewController.dismiss(animated: true)
        showLoader()
        getTimeSlotsData(selectedDate: date, dateButtonIndex: nil, firstLoad: false)
    }
    
    fileprivate func setDate(_ date: Date) {
        self.dateContainerView.isHidden = false
        self.dateContainerStackView.isHidden = true
        self.lblDate.text = date.string(with: .slotDateFormat)
    }
}
