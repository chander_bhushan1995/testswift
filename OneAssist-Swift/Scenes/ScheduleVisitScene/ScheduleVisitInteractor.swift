//
//  ScheduleVisitInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol ScheduleVisitPresentationLogic {
    func recievedTimeSlots(request:InspectionTimeSlotsRequestDTO,response: InspectionTimeSlotsResponseDTO?, error: Error?, dateButtonIndex: Int?, firstLoad: Bool)
    func recievedScheduledInspectionResponse(response: ScheduleInspectionResponseDTO?, error: Error?)
     func recievedRescheduledInspectionResponse(response: RescheduleInspectionResponseDTO?, error: Error?)
}

class ScheduleVisitInteractor: BaseInteractor, ScheduleVisitBusinessLogic {
    var presenter: ScheduleVisitPresentationLogic?
   
    func getTimeSlots(selectedDate: Date, dateButtonIndex: Int?, pincode : String? ,serviceType:String?, serviceRequestSourceType: String?, buCode: String?, bpCode: String?, memId: String?,assetId: String?, productCode: String?, srId: String?, firstLoad: Bool) {
        let req = InspectionTimeSlotsRequestDTO(selectedDate: selectedDate)
        req.pincode = pincode
        req.bpCode = bpCode
        req.buCode = buCode
        req.memId = memId
        req.assetId = assetId
        req.productCode = productCode
        req.serviceRequestSourceType = serviceRequestSourceType
        req.serviceRequestId = srId
        if let service = serviceType{
            req.serviceRequestType = service
        }
        let _ = InspectionTimeSlotsRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedTimeSlots(request:req, response: response, error: error, dateButtonIndex: dateButtonIndex, firstLoad: firstLoad)
        }
    }
    
    func scheduleInspection(activationCode: String?, addressDetail: AddressDetail?, tempCustomerDetail: TempCustomerDetail?, date: Date?, slot: ScheduleVisitTimeSlotViewModel?) {
        
        let req = ScheduleInspectionRequestDTO()
        let details = PostPaymentDetails()
        
        let customerDetails = CustomerDetails()
        
        customerDetails.addrLine1 = "\(addressDetail?.address ?? "" ) \(addressDetail?.address2 ?? "")"
        customerDetails.assetInfo = nil
        customerDetails.city = addressDetail?.cityCode
        customerDetails.gender = tempCustomerDetail?.gender
        customerDetails.pinCode = tempCustomerDetail?.pinCode
        customerDetails.relationshipCode = tempCustomerDetail?.relationshipCode
        customerDetails.stateCode = addressDetail?.stateCode
        
        let task = Task(dictionary: [:])
        task.name = "INSPECTION"
        task.serviceScheduleSlot = "\(slot?.startTime ?? ""):00-\(slot?.endTime ?? ""):00"
        task.serviceScheduleStartDate = DateFormatter.serverDateFormat.string(from: date ?? Date.currentDate())
        
        details.customerDetails = [customerDetails]
        details.activationCode = activationCode
        details.task = task
        
        req.postPaymentDetails = details
        
        let _ = ScheduleInspectionRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedScheduledInspectionResponse(response: response, error: error)
        }
    }
    
    func rescheduleInspection(serviceReqId: String, date: Date?, slot: ScheduleVisitTimeSlotViewModel?) {
        
        let req = RescheduleInspectionRequestDTO()
        
        req.modifiedBy = UserCoreDataStore.currentUser?.cusId
        req.serviceReqId = serviceReqId.description
        let selectedDate = date ?? Date.currentDate()
        let selectedDateStr = selectedDate.string(with: DateFormatter.slotDateFormat)
        
        req.scheduleSlotStartDateTime = "\(selectedDateStr) \(slot?.startTime ?? ""):00"
        req.scheduleSlotEndDateTime = "\(selectedDateStr) \(slot?.endTime ?? ""):00"
        
        let _ = RescheduleInspectionRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedRescheduledInspectionResponse(response: response, error: error)
        }
    }
}
