//
//  NotificationPromptPresenter.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 01/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//


import UIKit

protocol NotificationPromptDisplayLogic: class {
    func displaySuccess(_ response: NotificationAlertResponseDTO?)
    func displayFailure(_ error: String)
}

class NotificationPromptPresenter: BasePresenter {
    weak var viewController: NotificationPromptDisplayLogic?
}

extension NotificationPromptPresenter: NotificationPromptPresenterLogic {
    func sendResponse(_ response: NotificationAlertResponseDTO?, error: Error?) {
        if let error = error {
            viewController?.displayFailure(error.localizedDescription)
        } else {
            viewController?.displaySuccess(response)
        }
    }
}

