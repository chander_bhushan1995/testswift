//
//  NotificationPromptInterector.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 01/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol NotificationPromptPresenterLogic {
    func sendResponse(_ response: NotificationAlertResponseDTO?, error: Error?)
}

class NotificationPromptInterector: BaseInteractor {
    var presenter: NotificationPromptPresenter?
}

extension NotificationPromptInterector: NotificationPromptBusinessLogic {
    
    func getNotificationAlertData() {
        let _ = OANotificationPromptUseCase.service { [weak self] (_, response, error) in
                    self?.presenter?.sendResponse(response,error:error)
               }
    }
}

