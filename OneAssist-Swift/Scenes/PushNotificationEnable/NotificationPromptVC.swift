//
//  NotificationPromptVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 01/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol NotificationPromptBusinessLogic {
    func getNotificationAlertData()
}

class NotificationPromptVC: BaseVC, HideNavigationProtocol {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var paragraphLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    var interactor: NotificationPromptBusinessLogic?
    var screenName: String?
    var dataSource: NotificationPromptModel?
    var eventLocation: String?
    var onDismiss: (()->())?
       
       // MARK:- Object lifecycle
       override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
           super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
           setupDependencyConfigurator()
       }
       
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           setupDependencyConfigurator()
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        if  self.screenName == nil {
            AppUserDefaults.pushPromptShowDate = Date()
        }
    }
    
    private func initializeView() {
        setupData()
        Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: true)
        self.interactor?.getNotificationAlertData()
    }
    
    
    private func setupData() {
        headingLabel.text = nil
        paragraphLabel.text = nil
        paragraphLabel.textColor = paragraphLabel.textColor.withAlphaComponent(0.80)
        firstButton.setTitle(nil, for: .normal)
        secondButton.setTitle(nil, for: .normal)
        firstButton.isHidden = true
        secondButton.isHidden = true
        
        if let data = dataSource {
            headingLabel.text = data.header
            paragraphLabel.text = data.paragraph
            if let firstBtnTxt = data.firstButton {
                firstButton.isHidden = false
                firstButton.setTitle(firstBtnTxt, for: .normal)
            }
            if let scndBtnTxt = data.secondButton {
                secondButton.isHidden = false
                secondButton.setTitle(scndBtnTxt, for: .normal)
            }
        }
        headingLabel.setLineSpacing(lineSpacing: 4, lineHeightMultiple: 1)
        paragraphLabel.setLineSpacing(lineSpacing: 4, lineHeightMultiple: 1)
    }
    
    //MARK:- Button Actions
    @IBAction func onClickFirstButtonAction(_ sender: UIButton?) {
        self.dismiss(animated: true, completion: nil)
        if let model = dataSource {
            if model.promptType == "setting" {
                Utilities.openAppSettings()
                onDismiss?()
            }else {
                sharedAppDelegate?.enablePushNotification(UIApplication.shared, onAuthorization: onDismiss)
            }
            if let locationEvent = self.eventLocation {
                EventTracking.shared.eventTracking(name: .pushPermissionRationale, [.location: locationEvent, .action: "Allow"])
            }
        } else {
            onDismiss?()
        }
    }
    
    @IBAction func onClickSecondButtonAction(_ sender: UIButton?) {
        if dataSource != nil {
            if let locationEvent = self.eventLocation {
                EventTracking.shared.eventTracking(name: .pushPermissionRationale, [.location: locationEvent, .action: "Deny"])
            }
        }
        self.dismiss(animated: true) {
            self.onDismiss?()
        }
    }
    
    @IBAction func onCLickCrossAction(_ sender: UIButton?) {
        if dataSource != nil {
            if let locationEvent = self.eventLocation {
                EventTracking.shared.eventTracking(name: .pushPermissionRationale, [.location: locationEvent, .action: "Deny"])
            }
        }
        self.dismiss(animated: true) {
            self.onDismiss?()
        }
    }
}
extension NotificationPromptVC: NotificationPromptDisplayLogic {
    
    func displaySuccess(_ response: NotificationAlertResponseDTO?) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if let status = sharedAppDelegate?.pushPermissionStatus, status == false {
            dataSource = response?.settingPrompt
            
        }else {
            if let screen = screenName,  screen == "NOTIFICATION" {
                dataSource = response?.enablePrompt
                
            }else {
               dataSource = response?.firstTimePrompt
                
            }
        }
        setupData()
    }
    
    func displayFailure(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        setupData()
    }
}

extension NotificationPromptVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = NotificationPromptInterector()
        let presenter = NotificationPromptPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}
