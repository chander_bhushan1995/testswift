//
//  OfflineOfferDetailPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfflineOfferDetailDisplayLogic: class {
    func displayOfferDetail(_ offerDetail: OfferDetail)
    func displayOfferDetailError(_ error: String)
}

class OfflineOfferDetailPresenter: BasePresenter, OfflineOfferDetailPresentationLogic {
    weak var viewController: OfflineOfferDetailDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func recievedOfferDetailResponse(response: OfflineOfferDetailResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            if let detail = response?.data {
                viewController?.displayOfferDetail(detail)
            } else {
                viewController?.displayOfferDetailError(Strings.Global.somethingWrong)
            }
            
        } catch {
            viewController?.displayOfferDetailError(error.localizedDescription)
        }
    }

}
