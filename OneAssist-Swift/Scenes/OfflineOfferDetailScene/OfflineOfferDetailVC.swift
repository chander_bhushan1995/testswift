//
//  OfflineOfferDetailVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfflineOfferDetailBusinessLogic {
    func getOfferDetail(offer: BankCardOffers)
}

class OfflineOfferDetailVC: BaseVC {
    var index = 0;
    var interactor: OfflineOfferDetailBusinessLogic?
    var selectedOffer: BankCardOffers!
    var offerDetails: OfferDetail?
    var category:String?
    var expiryDate:String?
    var webengageEventTrackingDict:[Event.EventKeys:String] = [:]
    weak var delegate: OfferDetailBookmarkDelegate?
    @IBOutlet weak var imageViewBrand: UIImageView!
    @IBOutlet weak var labelBrand: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var viewBookmark: UIView!
    @IBOutlet weak var buttonBookmark: UIButton!
    @IBOutlet weak var imageViewTime: UIImageView!
    @IBOutlet weak var labelExpiry: UILabel!
    @IBOutlet weak var labelTerms: UILabel!
    @IBOutlet weak var textViewTermsDetail: ReadMoreTextView!
    @IBOutlet weak var labelOfferLink: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var buttonDirections: OAPrimaryButton!
     @IBOutlet weak var tagView: TagStyledView!
       @IBOutlet weak var tagViewTopConstraint: NSLayoutConstraint!
       @IBOutlet weak var tagViewHeight: NSLayoutConstraint!
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        OfferSyncUseCase.shared.syncOffers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        EventTracking.shared.addScreenTracking(with: .offerDetails)
       
    }
    
    // MARK:- private methods
    private func initializeView() {
        title = Strings.OfferDetail.title
        buttonBookmark.isEnabled = false
        buttonDirections.isEnabled = false
        buttonBookmark.setImage(#imageLiteral(resourceName: "bookmark_select_gray"), for: .normal)
        buttonBookmark.setImage(#imageLiteral(resourceName: "bookmark_select_blue"), for: .selected)
        
        let isBookmarked = OfferSyncUseCase.shared.isOfferBookmarked(offer: selectedOffer)
        buttonBookmark.isSelected = isBookmarked
        selectedOffer.isBookmarked = isBookmarked
    
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        interactor?.getOfferDetail(offer: selectedOffer)
        
        labelBrand.text = selectedOffer.merchantName
        setupTagView()
        addBankCardTag(cardList: selectedOffer.cardDetailsList)
    }
    
    private func setupTagView() {
        tagView.containerSize = {[unowned self] size in
            self.tagViewHeight.constant = size.height
        }
        
        tagView.register(UINib(nibName: BankDetailTagCell.Identifier.identifier, bundle: nil), forCellWithReuseIdentifier: BankDetailTagCell.Identifier.identifier)
        
        tagView.maximumTags = 6
        tagView.bottomSheetTitle = Strings.OfferDetail.offerAvailableOnMoreCards
        tagView.options = TagStyledView.Options(sectionInset: UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0),
                                                      lineSpacing: 6.0,
                                                      interitemSpacing: 6.0,
                                                      align: TagStyledView.Options.Alignment.left)

    }
    
    func addBankCardTag(cardList: [CardDetail]?) {
        if let cardDetailList = cardList, cardDetailList.count > 0 {
            self.tagView.tags = cardDetailList
            tagView.isHidden = false
            tagViewTopConstraint.constant = 12.0
            
        }else {
           tagView.isHidden = true
           tagViewTopConstraint.constant = 0.0
           tagViewHeight.constant = 0.0
        }
    }
    
    func setOfferDetailView(offerDetail: OfferDetail) {
        imageViewBrand.setImageWithUrlString(offerDetail.image, placeholderImage: #imageLiteral(resourceName: "icon_offer_default"))
        labelDiscount.text = offerDetail.availableDiscount ?? selectedOffer.offerTitle
        viewBookmark.layer.cornerRadius = 5.0
        viewBookmark.layer.borderColor = UIColor.lightGray.cgColor
        viewBookmark.layer.borderWidth = 2.0
        
        imageViewTime.image = #imageLiteral(resourceName: "date")
       
        let date = Date(string: offerDetail.endDate, formatter: DateFormatter.offerDateFormat)
        let dateString = date?.string(with: DateFormatter.dayMonth)
        labelExpiry.text = dateString != nil ? "\(Strings.AllOffersScene.endsOn) \(dateString!)" : nil
        
        labelTerms.text = Strings.OfferDetail.termsconds
        textViewTermsDetail.setText(text: offerDetail.termsConditions ?? "")
        labelOfferLink.text = Strings.OfferDetail.OfflineOfferDetailScene.address
        labelAddress.text = offerDetail.merchantLocation
        
        let item = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(OfflineOfferDetailVC.clickedButtonShare))
        self.navigationItem.rightBarButtonItem = item
        
        buttonDirections.setTitle(Strings.OfferDetail.OfflineOfferDetailScene.primaryAction, for: .normal)
    }

    // MARK:- Action Methods
    @IBAction func clickedButtonBookmark(_ sender: UIButton) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.bookmarkOffer.message, subTitle:nil, forScreen: "Bookmark Offer") {[unowned self] (status) in
            if status {
                sender.isSelected = !sender.isSelected
                self.selectedOffer.isBookmarked = !self.selectedOffer.isBookmarked
                self.delegate?.clickedDetailBookmark()
                     // EventTracking.shared.eventTracking(name: .bookmarkAnOffer, [.name: offer.merchantName ?? ""])
                      
                if(self.selectedOffer.isBookmarked == true){
                          EventTracking.shared.eventTracking(name: .markFavourite ,
                                                             [.merchantName: self.selectedOffer.merchantName ?? "",
                                                              .type: Event.EventParams.offline,
                                                              .category: self.category ?? "",
                                                              .position: NSNumber.init(value:self.index),
                                                              .expiryDate: (DateFormatter.serverDateFormat.date(from: self.expiryDate ?? "") ?? ""),
                                                              .location: FromScreen.offerDetailScreen.rawValue,
                                                              .offerID:self.selectedOffer.offerCode ?? "",
                                                              .outletCode: self.selectedOffer.outletCode ?? ""])
                          if Utilities.offersFavoritedCount() > 1 {
                              DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                                  guard let self = self else { return }
                                  UserDefaultsKeys.Location.set(value: Event.EventParams.favouriteOffer)
                                  Utilities.checkForRatingPopUp(viewController: self)
                              }
                          }
                      }
                OfferSyncUseCase.shared.storeOfferInDb(offer: self.selectedOffer)
                      
                let bookmarkDataDict : [String: String] = [Constants.KeyValues.offerId : self.selectedOffer.offerId ?? "", Constants.KeyValues.offerCode : self.selectedOffer.offerCode ?? "", Constants.KeyValues.outletCode : self.selectedOffer.outletCode ?? "", Constants.KeyValues.status : self.selectedOffer.isBookmarked ? "1" : "0"]
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookmarkStatusChanged"), object: nil, userInfo: bookmarkDataDict)
            }
        }
      
    }
    
    @objc func clickedButtonShare() {
        EventTracking.shared.eventTracking(name: .shareOfferTapped)
        EventTracking.shared.eventTracking(name: .shareOnOffer)
        self.presentShareActivityView(shareObject: Utilities.getOfferDetailShareText())
    }

    @IBAction func clickedButtonDirections(_ sender: Any) {
        Utilities.openGoogleMaps(latitude: offerDetails?.latitude?.doubleValue ?? 0, longitude: offerDetails?.longitude?.doubleValue ?? 0)
        EventTracking.shared.eventTracking(name: .getDirection,[.location : fromScreen.rawValue,.category:offerDetails?.category ?? "" ,.merchantName : offerDetails?.merchantName ?? ""])
        
    }
}

// MARK:- Display Logic Conformance
extension OfflineOfferDetailVC: OfflineOfferDetailDisplayLogic {
    
    func displayOfferDetailError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        EventTracking.shared.eventTracking(name: .offerDetailsApiFailed)
        showAlert(message: error)
    }
    
    func displayOfferDetail(_ offerDetail: OfferDetail) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        addBankCardTag(cardList: offerDetail.cardDetailsList)
        buttonBookmark.isEnabled = true
        buttonDirections.isEnabled = true
        offerDetails = offerDetail
        category = offerDetail.category
        expiryDate = offerDetail.endDate
        //this event is not in working as discussed with puneet.
        //        EventTracking.shared.eventTracking(name: .offerItemTapped,
        //                                           [.merchant: offer.merchantName ?? "",
        //                                            .type: Event.EventParams.nearby,
        //                                            .position: NSNumber.init(value:index),
        //                                            .category: category ?? "", .expiryDate:DateFormatter.serverDateFormat.date(from: offer.endDate ?? "") ?? "",.location : fromScreen?.rawValue ?? ""])
        if self.webengageEventTrackingDict.count > 0 {
            
            let utmData = [.utmCampaign: webengageEventTrackingDict[.utmCampaign] ?? "",
                           .utmMedium: webengageEventTrackingDict[.utmMedium] ?? "",
                           .utmSource: webengageEventTrackingDict[.utmSource] ?? ""] as [Event.EventKeys : Any]
            let someData = [.merchantName: selectedOffer.merchantName ?? "",
                            .type: Event.EventParams.offline,
                            .position: NSNumber.init(value:index),
                            .category: category ?? "",.expiryDate: (DateFormatter.serverDateFormat.date(from: expiryDate ?? "") ?? ""),
                            .location : fromScreen.rawValue ,
                            .offerID: selectedOffer.offerCode ?? "",
                            .outletCode:selectedOffer.outletCode ?? ""] as [Event.EventKeys : Any]
            let eventData = someData.merging(utmData) { (value1, value2) -> Any in
                return value1
            }
            EventTracking.shared.eventTracking(name: .offerDetails, eventData)
        } else {
            
            EventTracking.shared.eventTracking(name: .offerDetails,
                                               [.merchantName: selectedOffer.merchantName ?? "",
                                                .type: Event.EventParams.offline,
                                                .position: NSNumber.init(value:index),
                                                .category: category ?? "",.expiryDate: (DateFormatter.serverDateFormat.date(from: expiryDate ?? "") ?? ""),
                                                .location : fromScreen.rawValue ,
                                                .offerID: selectedOffer.offerCode ?? "",
                                                .outletCode:selectedOffer.outletCode ?? ""])
        }
        setOfferDetailView(offerDetail: offerDetail)
        if Utilities.offersClickedCount() > 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                guard let self = self else { return }
                UserDefaultsKeys.Location.set(value: Event.EventParams.viewOffer)
                Utilities.checkForRatingPopUp(viewController: self)
            }
        }
    }
}

// MARK:- Configuration Logic
extension OfflineOfferDetailVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = OfflineOfferDetailInteractor()
        let presenter = OfflineOfferDetailPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension OfflineOfferDetailVC {
    
}
