//
//  OfflineOfferDetailInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfflineOfferDetailPresentationLogic {
    func recievedOfferDetailResponse(response: OfflineOfferDetailResponseDTO?, error: Error?)
}

class OfflineOfferDetailInteractor: BaseInteractor, OfflineOfferDetailBusinessLogic {
    var presenter: OfflineOfferDetailPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getOfferDetail(offer: BankCardOffers) {
        
        let lat = "1"// offer.latitude?.doubleValue.description ?? ""
        let long = "1" //offer.longitude?.doubleValue.description ?? ""
        
        let req = OfflineOfferDetailRequestDTO(latitude: lat, longitude: long, outletCode: offer.outletCode ?? "", offerCode: offer.offerCode ?? "", customerId: UserCoreDataStore.currentUser?.cusId ?? "0")
        let _ = OfflineOfferDetailRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedOfferDetailResponse(response: response, error: error)
        }
    }
}
