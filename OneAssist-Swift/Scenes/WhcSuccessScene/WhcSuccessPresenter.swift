//
//  WhcSuccessPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WhcSuccessDisplayLogic: class {
    func displayTempCustomerResponse(isTempUser: Bool, response: TemporaryCustomerInfoResponseDTO)
    func displayTempCustomerError(_ error: String)
}

class WhcSuccessPresenter: BasePresenter, WhcSuccessPresentationLogic {
    weak var viewController: WhcSuccessDisplayLogic?
    
    func tempCustomerResponseRecieved(isTempUser: Bool, response: TemporaryCustomerInfoResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            if isTempUser {
                if response?.customerDetails?.first?.mobileNo?.stringValue ==  userMobileNumber {
                    let custId = response?.customerDetails?.first?.custId ?? ""
                    let id = Int(custId) ?? 0
                    
                    // change array users model
                    if let user = UserCoreDataStore.currentUser {
                        user.custIds?.append("\(id)")
                    }
                    CoreDataStack.sharedStack.saveMainContext()
                }
            }
            viewController?.displayTempCustomerResponse(isTempUser: isTempUser, response: response!)
        } catch {
            viewController?.displayTempCustomerError(error.localizedDescription)
        }
    }
}
