//
//  WhcSuccessInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WhcSuccessPresentationLogic {
    func tempCustomerResponseRecieved(isTempUser: Bool, response: TemporaryCustomerInfoResponseDTO?, error: Error?)
}

class WhcSuccessInteractor: BaseInteractor, WhcSuccessBusinessLogic {
    var presenter: WhcSuccessPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getTempCustInfo(activationCode: String, isTempUser: Bool) {
        let req = TemporaryCustomerInfoRequestDTO()
        req.activationCode = activationCode
        let _ = TemporaryCustomerInfoRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.tempCustomerResponseRecieved(isTempUser: isTempUser, response: response, error: error)
        }
    }
}
