//
//  WhcSuccessVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WhcSuccessBusinessLogic {
    func getTempCustInfo(activationCode: String, isTempUser: Bool)
}

class WhcSuccessVC: BaseVC, HideNavigationProtocol {
    var interactor: WhcSuccessBusinessLogic?
    
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet weak var labelHeading: H2RegularLabel!
    @IBOutlet weak var labelMidHeading: BodyTextRegularBlackLabel!
    @IBOutlet weak var labelBottomHeading: BodyTextRegularBlackLabel!
    
    @IBOutlet weak var labelTransparentHeading: H2RegularLabel!
    @IBOutlet weak var labelTransparentSubHeading: BodyTextRegularBlackLabel!
    @IBOutlet weak var buttonPrimary: OAPrimaryButton!
    
    var activationCode = ""
    var closeButtonPressed = false
    //    var tempCustomer: TemporaryCustomerInfoResponseDTO?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        EventTracking.shared.eventTracking(name: .whcSuccessfullyPurchased)
    }
    
    // MARK:- private methods
    private func initializeView() {

        labelMidHeading.textColor = UIColor.gray
        
        labelHeading.text = Strings.WhcSuccessScene.heading
        labelMidHeading.text = Strings.WhcSuccessScene.midHeading
        labelBottomHeading.text = Strings.WhcSuccessScene.bottomHeading
        
        labelTransparentHeading.text = Strings.WhcSuccessScene.transparentHeading
        labelTransparentSubHeading.text = Strings.WhcSuccessScene.transparentSubHeading
        buttonPrimary.setTitle(Strings.WhcSuccessScene.primaryAction, for: .normal)
        topSpace.constant = UIApplication.shared.statusBarFrame.height
    }
    
    func routeToInspection(response: TemporaryCustomerInfoResponseDTO) {
        EventTracking.shared.eventTracking(name: .ScheduleInspection, [
            .location: Event.EventParams.activation
            ])
        
        let purchaseDate = Date.currentDate()
        let details = response.customerDetails?.first
        
        if let pincode = details?.pinCode {
            
            var isValidCityStateCode = false
            
            if let cityCode = details?.city, cityCode.count > 0, let stateCode = details?.stateCode, stateCode.count > 0 {
                isValidCityStateCode = true
            }
            
            if details?.pinCode == CustomerDetailsCoreDataStore.currentCustomerDetails?.pincode && !UserDefaults.standard.bool(forKey: UserDefaultsKeys.tempCustomer.rawValue) && isValidCityStateCode {
                
                let selectAddress = WHCSelectAddressVC()
                let userDtls = CustomerDetailsCoreDataStore.currentCustomerDetails
                let addressDetail = AddressDetail()
                addressDetail.pincode = userDtls?.pincode ?? ""
                addressDetail.address = userDtls?.addressLine1 ?? ""
                addressDetail.state = userDtls?.stateName ?? ""
                addressDetail.city = userDtls?.cityName ?? ""
                addressDetail.cityCode = userDtls?.cityCode ?? ""
                addressDetail.stateCode = userDtls?.stateCode ?? ""
                
                selectAddress.modelArray = [addressDetail]
                selectAddress.activationCode = activationCode
                selectAddress.customerDetails = details
                selectAddress.pinCode = userDtls?.pincode ?? ""
                navigationController?.pushViewController(selectAddress, animated: true)
                return
            }
            
            let addressRegistrationVC = AddressRegistrationVC()
            addressRegistrationVC.activationCode = activationCode
            addressRegistrationVC.pincode = pincode
            addressRegistrationVC.customerDetails = details
            addressRegistrationVC.purchasedDate = purchaseDate
            addressRegistrationVC.isAdding = true
            navigationController?.pushViewController(addressRegistrationVC, animated: true)
            
        } else {
            showAlert(message: Strings.Global.somethingWrong)
        }
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnClose(_ sender: Any) {
        closeButtonPressed = true
        
        if let user = UserCoreDataStore.currentUser {
            if user.custIds != nil {
                Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                interactor?.getTempCustInfo(activationCode: activationCode, isTempUser: true)
            } else {
                routeToTabBar()
            }
        } else {
            routeToLoginScreen()
        }
    }
    
    @IBAction func clickedBtnPrimary(_ sender: Any) {
        if let user = UserCoreDataStore.currentUser {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
            interactor?.getTempCustInfo(activationCode: activationCode, isTempUser: user.custIds != nil)
        } else {
            routeToLoginScreen()
        }
    }
}

// MARK:- Display Logic Conformance
extension WhcSuccessVC: WhcSuccessDisplayLogic {
    
    func displayTempCustomerResponse(isTempUser: Bool, response: TemporaryCustomerInfoResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if isTempUser && closeButtonPressed {
            routeToTabBar()
        } else {
            routeToInspection(response: response)
        }
    }
    
    func displayTempCustomerError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

// MARK:- Configuration Logic
extension WhcSuccessVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = WhcSuccessInteractor()
        let presenter = WhcSuccessPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension WhcSuccessVC {
    func routeToTabBar() {
        setTabAsRootNew()
    }
    
    func routeToLoginScreen() {
        Utilities.forceUserLogout()
    }
}
