//
//  InspectionDetailHeaderView.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionDetailHeaderViewDelegate:class{

    func collapseClicked(atSection section: Int, expanded: Bool)

}

class InspectionDetailHeaderView: UIView {
    
    @IBOutlet var imgViewObj: UIImageView!
    @IBOutlet var labelHeading: H3BoldLabel!
    @IBOutlet var labelDisabledHeading: BodyTextRegularGreyLabel!
    @IBOutlet var buttonDetails: OASecondaryButton!
    @IBOutlet var bottomMarginView: UIView!
    @IBOutlet var topMarginView: UIView!
    @IBOutlet var collapseImageView: UIImageView!
    
    weak var delegate:InspectionDetailHeaderViewDelegate?
       override func awakeFromNib() {
        super.awakeFromNib()
        buttonDetails.setTitle(Strings.WHC.inspection.inspectionDetailHeaderView.showDetails, for: .normal)
        buttonDetails.setTitle(Strings.WHC.inspection.inspectionDetailHeaderView.hideDetails, for: .selected)
//        labelHeading.font = DLSFont.h3.regular
//        labelDisabledHeading.font = DLSFont.bodyText.regular
//        buttonDetails.titleLabel?.font = DLSFont.bodyText.regular
//        setColors()
    }
//    func setColors(){
//        labelHeading.textColor = UIColor.charcoalGrey
//        labelDisabledHeading.textColor = UIColor.lightGray
//        buttonDetails.titleLabel?.textColor = UIColor.dodgerBlue
//        }
//
    func setModel(_ viewModel: InspectionDetailViewModel, forSection section: Int) {
        tag = section
        switch viewModel.detailState {
        case .notStarted:
            labelHeading.isHidden = true
            buttonDetails.isHidden = true
            labelDisabledHeading.text = viewModel.heading
            imgViewObj.image = #imageLiteral(resourceName: "timelineInactive")
            collapseImageView.isHidden = true
        case .started:
            if viewModel.subHeadingWithNoDetails != nil {
                buttonDetails.setTitle(viewModel.subHeadingWithNoDetails, for: .normal)
                buttonDetails.setTitleColor(UIColor.bodyTextGray, for: .normal)
                buttonDetails.isUserInteractionEnabled = false
                collapseImageView.isHidden = true
            }
            labelDisabledHeading.isHidden = true
            labelHeading.text = viewModel.heading
            imgViewObj.image = #imageLiteral(resourceName:"timelineInProgress")
        case .completed:
            if (viewModel.subHeadingWithNoDetails != nil) {
                buttonDetails.setTitle(viewModel.subHeadingWithNoDetails, for: .normal)
                buttonDetails.setTitleColor(UIColor.bodyTextGray, for: .normal)
                buttonDetails.isUserInteractionEnabled = false
                collapseImageView.isHidden = true
            }
            labelDisabledHeading.isHidden = true
            labelHeading.text = viewModel.heading
            imgViewObj.image = #imageLiteral(resourceName:"timelineDone")
        }
    }
    
    func toggleButtonState(_ selected: Bool) {
        buttonDetails.isSelected = selected
        let angle: CGFloat = selected ? .pi : 0
        collapseImageView.transform = CGAffineTransform(rotationAngle: angle)
    }
    
   
    @IBAction func clickButtonDetails(_ sender: Any) {
        delegate?.collapseClicked(atSection: self.tag, expanded: !self.buttonDetails.isSelected)
    }
  
  }
