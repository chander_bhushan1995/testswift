//
//  ExclusiveBenefitBottomSheetView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/08/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum BenefitCellActionType: String {
    case phone = "phone"
    case chat = "chat"
}

protocol ExclusiveBenefitBottomSheetViewDelegate: class {
    func openChat()
    func callNumber(_ number:String)
}

class ExclusiveBenefitBottomSheetView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var commentLabel: SupportingTextRegularGreyLabel!
    
    @IBOutlet var bottomSheetView: UIView!
    var model: ExclusiveBenfit?
    weak var delegate: ExclusiveBenefitBottomSheetViewDelegate?
    @IBOutlet weak var tableViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var commentTopConstriant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("ExclusiveBenefitBottomSheetView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        tableViewObj.register(cell: ExclusiveBenefitBottomSheetCell.self)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.separatorColor = .gray2
        tableViewObj.isScrollEnabled = false
        let line = UIView(frame: CGRect(x: 0, y: 0, width: tableViewObj.frame.size.width, height: 1 / UIScreen.main.scale))
        line.backgroundColor = .gray2
        tableViewObj.rowHeight = 56.0
        tableViewObj.tableHeaderView = line
    }
    
    func setViewModel(_ model: ExclusiveBenfit, delegate: ExclusiveBenefitBottomSheetViewDelegate?) {
        self.delegate = delegate
        self.model = model
        self.titleLabel.text = model.title
        self.descriptionLabel.text = model.benefitDescription
        if let condition = model.condition {
            self.commentLabel.text = "* \(condition)"
            self.commentLabel.isHidden = false
            commentTopConstriant.constant = 16
        } else {
            self.commentLabel.text = nil
            self.commentLabel.isHidden = true
            commentTopConstriant.constant = 0
        }
        self.tableViewHeightConstaint.constant = CGFloat((model.listPoints?.count ?? 0) * 56)
        tableViewObj.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.listPoints?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ExclusiveBenefitBottomSheetCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setViewModel(model?.listPoints?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = model?.listPoints?[indexPath.row] {
            if model.type == BenefitCellActionType.chat.rawValue {
                delegate?.openChat()
            } else if model.type == BenefitCellActionType.phone.rawValue, let number = model.text?.replacingOccurrences(of: " ", with: "") {
                delegate?.callNumber(number)
            }
        }
    }
    
    class func height(forModel model:ExclusiveBenfit) -> CGFloat {
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        let widthConstraints: CGFloat = 32.0
        let collectiveHeigthConstains: CGFloat = 72
        let titleHeight = model.title?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.bodyText.bold) ?? 0.0
        let descriptionHeight = model.benefitDescription?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.bodyText.regular) ?? 0.0
        var commentHeight = model.condition?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.supportingText.regular) ?? 0.0
        
        if let condition = model.condition, !condition.isEmpty {
            commentHeight += 16
        }
        
        return CGFloat((model.listPoints?.count ?? 0) * 56) + bottomSafeArea + collectiveHeigthConstains + titleHeight + descriptionHeight + commentHeight
    }
}
