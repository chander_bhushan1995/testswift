//
//  ExclusiveBenefitCellModel.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/08/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

struct ExclusiveBenefitCellModel {
    var title: String?
    var titlePosition: NSTextAlignment = .center
    var backgroundColor: UIColor = .clear
    var benefits: [ExclusiveBenfit] = []
    
    init(benefits: [ExclusiveBenfit], title: String, titlePosition: NSTextAlignment, backgroundColor: UIColor) {
        self.benefits = benefits
        self.title = title
        self.titlePosition = titlePosition
        self.backgroundColor = backgroundColor
    }
}
