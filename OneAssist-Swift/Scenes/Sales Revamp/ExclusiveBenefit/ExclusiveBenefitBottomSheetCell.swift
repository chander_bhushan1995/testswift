//
//  ExclusiveBenefitBottomSheetCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/08/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class ExclusiveBenefitBottomSheetCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var titleLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var subtitleLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var iconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        subtitleLabel.textColor = UIColor.dodgerBlue
    }

    func setViewModel(_ model: ListPoint?) {
        self.titleLabel.text = model?.title
        self.subtitleLabel.text = model?.text
        if model?.type == BenefitCellActionType.chat.rawValue {
            self.iconView.image = #imageLiteral(resourceName: "bottomsheet_chat")
        } else if model?.type == BenefitCellActionType.phone.rawValue {
            self.iconView.image = #imageLiteral(resourceName: "bottomsheet_call")
        }
    }
}
