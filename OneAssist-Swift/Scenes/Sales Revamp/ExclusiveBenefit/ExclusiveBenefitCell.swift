//
//  ExclusiveBenefitCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/08/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

@objc protocol ExclusiveBenefitCellDelegate {
    func didClickOnBenefit(_ benefit:ExclusiveBenfit?)
}

class ExclusiveBenefitCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var cellTitle: TagsBoldGreyLabel!
    
    @IBOutlet weak var benefitView1: UIView!
    @IBOutlet weak var benefitView2: UIView!
    @IBOutlet weak var benefitView3: UIView!
    @IBOutlet weak var benefitView4: UIView!
    
    @IBOutlet weak var benefitImage1: UIImageView!
    @IBOutlet weak var benefitImage2: UIImageView!
    @IBOutlet weak var benefitImage3: UIImageView!
    @IBOutlet weak var benefitImage4: UIImageView!
    
    @IBOutlet weak var benefitLabel1: TagsRegularGreyLabel!
    @IBOutlet weak var benefitLabel2: TagsRegularGreyLabel!
    @IBOutlet weak var benefitLabel3: TagsRegularGreyLabel!
    @IBOutlet weak var benefitLabel4: TagsRegularGreyLabel!
    
    weak var delegate: ExclusiveBenefitCellDelegate?
    var model: ExclusiveBenefitCellModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        benefitLabel1.textColor = UIColor.buttonBlue
        benefitLabel2.textColor = UIColor.buttonBlue
        benefitLabel3.textColor = UIColor.buttonBlue
        benefitLabel4.textColor = UIColor.buttonBlue
        benefitView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickOnBenefit1)))
        benefitView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickOnBenefit2)))
        benefitView3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickOnBenefit3)))
        benefitView4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickOnBenefit4)))
        separatorInset = .zero
    }
    
    func setViewModel(_ model: ExclusiveBenefitCellModel, delegate: ExclusiveBenefitCellDelegate) {
        self.model = model
        self.delegate = delegate
        cellTitle.text = model.title?.uppercased()
        cellTitle.textAlignment = model.titlePosition
        self.backgroundColor = model.backgroundColor
        benefitImage1.image = image(fromBenefit: model.benefits[0])
        benefitImage2.image = image(fromBenefit: model.benefits[1])
        benefitImage3.image = image(fromBenefit: model.benefits[2])
        benefitImage4.image = image(fromBenefit: model.benefits[3])
        benefitLabel1.text = title(fromBenefit: model.benefits[0])
        benefitLabel2.text = title(fromBenefit: model.benefits[1])
        benefitLabel3.text = title(fromBenefit: model.benefits[2])
        benefitLabel4.text = title(fromBenefit: model.benefits[3])
    }
    
    @objc func clickOnBenefit1() {
        delegate?.didClickOnBenefit(model?.benefits[0])
    }
    
    @objc func clickOnBenefit2() {
        delegate?.didClickOnBenefit(model?.benefits[1])
    }
    
    @objc func clickOnBenefit3() {
        delegate?.didClickOnBenefit(model?.benefits[2])
    }
    
    @objc func clickOnBenefit4() {
        delegate?.didClickOnBenefit(model?.benefits[3])
    }
    
    private func title(fromBenefit benefit:ExclusiveBenfit) -> String {
        if let name = benefit.name {
            if let condition = benefit.condition, !condition.isEmpty {
                return name + "*"
            } else {
                return name
            }
        } else {
            return ""
        }
    }
    
    private func image(fromBenefit benefit:ExclusiveBenfit) -> UIImage? {
        if let image = benefit.image {
            return UIImage(named: image)
        } else {
            return nil
        }
    }
    
}
