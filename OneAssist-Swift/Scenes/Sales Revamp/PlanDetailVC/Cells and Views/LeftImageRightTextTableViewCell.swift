//
//  LeftImageRightTextTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 10/02/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation


struct LeftImageRightTextTableViewCellVM {
    var title:String?
    var image: UIImage?
    
    init(title: String) {
        image = #imageLiteral(resourceName: "tickStatus")
        self.title = title
    }
    
    static func heightFor(model: LeftImageRightTextTableViewCellVM)->CGFloat{
        var imageWidth: CGFloat = 0
        var imageHeight: CGFloat = 0
        var labelHeight: CGFloat = 0
        if let _  = model.image {
            imageWidth += 18+8
            imageHeight = 18
        }
        if let _ = model.title {
            labelHeight = (model.title?.height(withWidth: screensize.width - 32 - imageWidth, font: DLSFont.bodyText.regular) ?? 0)
        }
        
        return max(imageHeight,labelHeight) + 12
        
    }
}


class LeftImageRightTextTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView  {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightText: BodyTextRegularBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func setViewModel(model: LeftImageRightTextTableViewCellVM){
        leftImageView.image = model.image
        leftImageView.isHidden = model.image == nil
        rightText.text = model.title
        rightText.isHidden = model.title?.isEmpty ?? true
    }
}
