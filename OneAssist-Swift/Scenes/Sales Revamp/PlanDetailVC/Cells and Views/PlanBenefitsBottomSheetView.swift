//
//  PlanBenefitsBottomSheetView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 10/02/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

struct PlanBenefitsBottomSheetVM {
    var title: String?
    var benefits: [LeftImageRightTextTableViewCellVM]?
    
    init(title: String,planBenefits: [String]) {
        self.title = title
        benefits = planBenefits.map({ (benefit) -> LeftImageRightTextTableViewCellVM in
            return LeftImageRightTextTableViewCellVM(title: benefit)
        })
    }
}

class PlanBenefitsBottomSheetView: UIView, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var closeImageView: UIImageView!
    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var bottomSheetView: UIView!
    
    var model: [LeftImageRightTextTableViewCellVM] = []
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("PlanBenefitsBottomSheetView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
        tableView.register(cell: LeftImageRightTextTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = .gray2
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 64
        tableView.isScrollEnabled = false
        closeImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismiss)))
        closeImageView.isUserInteractionEnabled = true
    }
    
    @objc func dismiss() {
        Utilities.topMostPresenterViewController.dismiss(animated: true){}
    }
    
    func setViewModel(_ model: PlanBenefitsBottomSheetVM) {
        self.titleLabel.text = model.title
        self.model = model.benefits ?? []
        tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.setScrollable()
        }
    }
    
    func setScrollable() {
        tableView.isScrollEnabled = tableView.contentSize.height > tableView.frame.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LeftImageRightTextTableViewCell = tableView.dequeueReusableCell(indexPath: indexPath)
        cell.setViewModel(model: model[indexPath.row])
        return cell
    }
    
    class func height(forModel model: PlanBenefitsBottomSheetVM) -> CGFloat {
        var cellsHeight: CGFloat = 0
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        let titleHeight: CGFloat = model.title?.height(withWidth: screensize.width - 32, font: DLSFont.h3.bold) ?? 0 + 20
        for benefit in model.benefits ?? [] {
            cellsHeight += LeftImageRightTextTableViewCellVM.heightFor(model: benefit)
        }
        let height = bottomSafeArea + titleHeight + 64 + cellsHeight + 20
        return min(height, screensize.height*0.8)
    }
}
