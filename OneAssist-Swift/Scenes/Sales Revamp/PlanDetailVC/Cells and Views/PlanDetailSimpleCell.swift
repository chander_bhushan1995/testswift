//
//  PlanDetailSimpleCell.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 11/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class PlanDetailSimpleCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    var index: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        separatorInset = .zero
    }
    
    func setViewModel(_ model: String, index: Int) {
        self.index = index
        self.titleLabel.text = model
    }
}
