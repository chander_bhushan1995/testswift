//
//  CheckBoxAndTitleCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 06/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct CheckBoxAndTitleCellVM {
    var idForCheckBox: NSNumber?
    var title: String?
    var isEnabled: Bool = false
}

class CheckBoxAndTitleCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var checkBoxTitle: BodyTextRegularBlackLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setViewModel(model: CheckBoxAndTitleCellVM?){
        if let model = model {
            checkBoxTitle.text = model.title
            checkBoxImage.image = !(model.isEnabled) ? UIImage(named: "checkBox_normal")  : UIImage(named: "checkBox_tick")
        }
    }
    
    class func heightForModel(_ model: CheckBoxAndTitleCellVM) -> CGFloat {
        let imageHeightWidth = 18
        let titleHeight = model.title?.height(withWidth: screensize.width - 50, font: DLSFont.bodyText.regular) ?? 0
        let totalHeight = min(titleHeight,CGFloat(imageHeightWidth))+24
        return totalHeight
    }
}
