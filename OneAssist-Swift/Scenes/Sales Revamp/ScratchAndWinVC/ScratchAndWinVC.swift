//
//  ScratchAndWinVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 02/12/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum RewardStates: String {
    case LOCKED = "LOCKED"
    case UNLOCKED="UNLOCKED"
    case USED="USED"
    case EARNED="EARNED"
    case EXPIRED="EXPIRED"
    case CLAIMED="CLAIMED"
}

class ScratchAndWinVC: BaseVC {
    
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var tagLabel: TagsBoldGreyLabel!
    @IBOutlet weak var titleLabel: H2BoldLabel!
    @IBOutlet weak var subtitleLabel: BodyTextRegularBlackLabel!
    
    @IBOutlet weak var couponViewTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var couponView: UIView!
    @IBOutlet weak var rewardTitleLabel: H2BoldLabel!
    @IBOutlet weak var rewardMessageLabel: BodyTextRegularBlackLabel!
    
    @IBOutlet weak var couponMessageView: UIView!
    @IBOutlet weak var couponMessageLabel: SupportingTextRegularBlackLabel!
    
//    @IBOutlet weak var claimButton: OAPrimaryButton!
    @IBOutlet weak var knowMoreBtn: OAPrimaryButton!
    
    @IBOutlet weak var shareLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var shareButton: OASecondaryButton!
    
    @IBOutlet weak var scratchImageView: ScratchImageView!
    var state:RewardStates = RewardStates.EARNED
    
    var claimRewardTapped: (() -> ())?
    var dismissTapped: (() -> ())?
    var isScratchDone: Bool = false
//    var knowMoreAttrs = [
//        NSAttributedString.Key.font : DLSFont.bodyText.regular,
//        NSAttributedString.Key.foregroundColor : UIColor.white,
//        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    var attributedString = NSMutableAttributedString(string:"")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    fileprivate func initializeView() {
        couponView.clipsToBounds = true
        couponView.layer.cornerRadius = 4.0
        
        tagLabel.backgroundColor = .saffronYellow
        tagLabel.textColor = .white
        tagLabel.layer.cornerRadius = 2
        tagLabel.clipsToBounds = true
        tagLabel.text = "REWARD"
        
        titleLabel.textColor = .white
        titleLabel.text = Strings.SalesRevamp.scratchViewTitleUnscratched
        
        subtitleLabel.textColor = UIColor.white.withAlphaComponent(0.8)
        subtitleLabel.text = Strings.SalesRevamp.scratchViewSubtitleUnscratched
        
        
        scratchImageView.delegate = self
        
        rewardTitleLabel.textColor = .buttonTitleBlue
        rewardTitleLabel.text = Strings.SalesRevamp.scratchCardMessage
        
        rewardMessageLabel.text = Strings.SalesRevamp.scratchViewPrize
    
        couponMessageLabel.text = Strings.SalesRevamp.rewardMessage
        
//        claimButton.setTitle(Strings.SalesRevamp.scratchViewButtonScratched, for: .normal)
        knowMoreBtn.setTitle(Strings.SalesRevamp.scratchViewButtonScratched, for: .normal)
        
//        let buttonTitleStr = NSMutableAttributedString(string:Strings.SalesRevamp.knowMore, attributes:knowMoreAttrs)
//        attributedString.append(buttonTitleStr)
//        knowMoreBtn.setAttributedTitle(attributedString, for: .normal)
        
        shareLabel.text = Strings.SalesRevamp.shareLabelText
        
        shareButton.setTitleColor(.white, for: .normal)
        shareButton.setTitle(Strings.SalesRevamp.shareButtonText, for: .normal)
        
        EventTracking.shared.eventTracking(name: .scratchCardSeen)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        manageViewForState()
    }
    
    @IBAction func close(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .IDScretchCardCross)
        dismiss(animated: true) {
            self.dismissTapped?()
        }
    }
    
//    @IBAction func claimButtonTapped(_ sender: Any) {
//        switch state {
//        case .UNLOCKED:
//            self.dismiss(animated: false){
//                ChatBridge.openIDFence(true, isFreeReward: true)
//            }
//        default:
//            break
//        }
//    }
    
    
    @IBAction func knowMoreTapped(_ sender: Any) {
        self.dismiss(animated: false){
            ChatBridge.openIDFence(false)
        }
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let textToShare = Strings.SalesRevamp.shareText
        let imgToShare = #imageLiteral(resourceName: "idfence_buy")
        let objectsToShare = [imgToShare,textToShare] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    fileprivate func scratchDone() {
        let scratchDate = Date().millisecondsSince1970
        EventTracking.shared.eventTracking(name: .scratched)
        EventTracking.shared.eventTracking(name: .rewardUnlocked,[.location: fromScreen.rawValue])
//        claimButton.isHidden = false
//        claimButton.isEnabled = true
        shareLabel.isHidden = false
        shareButton.isHidden = false
        state = .UNLOCKED
        ChatBridge.onRewardStateChange(state: state.rawValue,scratchDate: scratchDate.description)
//        UserDefaultsKeys.scratchCardShown
        UserDefaults.standard.setValue(state.rawValue, forKey: "rewardState")
        UserDefaults.standard.setValue(scratchDate, forKey: "rewardScratchDate")
        manageViewForState()
    }
    
    private func manageViewForState(){
        switch state {
        case .UNLOCKED:
            
            couponViewTopConstraints.constant = 10.0
            tagView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            scratchImageView.isHidden = true
            couponMessageView.isHidden = true
//            claimButton.isHidden = false
            knowMoreBtn.isHidden = false
            shareLabel.isHidden = false
            shareButton.isHidden = false
            
        case .USED:
            couponViewTopConstraints.constant = 10.0
            tagView.isHidden = true
            titleLabel.isHidden = true
            subtitleLabel.isHidden = true
            scratchImageView.isHidden = true
            couponMessageView.isHidden = false
//            claimButton.isHidden = false
            knowMoreBtn.isHidden = false
            shareLabel.isHidden = false
            shareButton.isHidden = false
            
        default:
            couponViewTopConstraints.constant = 40.0
            tagView.isHidden = false
            titleLabel.isHidden = false
            subtitleLabel.isHidden = false
            scratchImageView.isHidden = false
            
            couponMessageView.isHidden = true
//            claimButton.isHidden = true
            knowMoreBtn.isHidden = true
            shareLabel.isHidden = true
            shareButton.isHidden = true
            break
        }
    }
}

extension ScratchAndWinVC: ScratchImageViewDelegate {
    func percentScratched(_ percentScratched: Double) {
        if percentScratched > 0.4 && !isScratchDone{
            isScratchDone = true
            scratchImageView.isHidden = true
            scratchDone()
        }
    }
}
