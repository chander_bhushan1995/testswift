//
//  LoginBottomSheet.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 28/02/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct LoginSheetModel {
    var title: String?
    var detail: String?
    var ctaText: String = Strings.NumberVerifyAlerts.buttonText
}

class LoginBottomSheet: UIView {
    
    var actionHandler: ((Bool) -> Void)? = nil
    @IBOutlet var bottomSheetView: UIView!
    @IBOutlet weak var titleLabel: H2BoldLabel!
    @IBOutlet weak var detailLable: H3RegularBlackLabel!
    @IBOutlet weak var verifyButton: OAPrimaryButton!
    
    var modelObject: LoginSheetModel?
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("LoginBottomSheet", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
    }
    
    func setUpValue(_ model: LoginSheetModel?, _ handler: @escaping (Bool) -> Void) {
        actionHandler = handler
        modelObject = model
        self.titleLabel.text = modelObject?.title
        self.detailLable.text = modelObject?.detail
        verifyButton.setTitle(model?.ctaText, for: .normal)
        verifyButton.setTitle(model?.ctaText, for: .selected)
    }
    
    @IBAction func onClickVerifyNumber(_ sender: UIButton?){
        Utilities.topMostPresenterViewController.dismiss(animated: false)
        if let completionBlock = actionHandler {
            completionBlock(true)
        }
    }
    
    class func height(forModel model:LoginSheetModel) -> CGFloat {
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        let widthConstraints: CGFloat = 40.0
        let collectiveHeigthConstains: CGFloat = 40+80+20+16+40+48+20
        let titleHeight = model.title?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.h2.bold) ?? 0.0
        let descriptionHeight = model.detail?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.h3.regular) ?? 0.0
    
        return  bottomSafeArea + collectiveHeigthConstains + titleHeight + descriptionHeight + 10
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
