//
//  WebViewVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 11/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class WebViewVC: BaseVC {

    var webView = WKWebView()
    var urlString: String = ""
    var screenTitle: String?
    var isLinkWebView = false
    var isShowShareOption = false
    var isShowFromDocumentDic = false

    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView)
        initializeView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }
    
    @objc func handleNavBackButton() {
        if let str = webView.url?.absoluteString.lowercased() {
            if str.contains(Constants.URL.idFencePdf), webView.canGoBack {
                webView.goBack()
            } else {
                if self.isPresentVC {
                    self.dismiss(animated: true, completion: nil);
                }else {
                    navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @objc func handleShareButton(sender: UIBarButtonItem){
        if let url = URL(string: urlString) {
            let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
       
    }
    
    private func handleBackButton() {
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleNavBackButton))
    }
    
    private func handleShareButton() {
        navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem.init(image: UIImage(named: "shareApp"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleShareButton(sender:)))
        navigationItem.rightBarButtonItem = backButton
    }
    
    // MARK:- private methods
    func initializeView() {
        title = screenTitle ?? ""
        if isLinkWebView {
            handleBackButton()
        }
        if isShowShareOption {
            handleShareButton()
        }
        if let url = URL(string: urlString) {
            webView.navigationDelegate = self
            if isShowFromDocumentDic {
                webView.loadFileURL(url, allowingReadAccessTo: url)
                
            }else {
               webView.load(URLRequest(url: url, cachePolicy: isLinkWebView ? .reloadIgnoringLocalAndRemoteCacheData : .useProtocolCachePolicy, timeoutInterval: isLinkWebView ? 0 : 60))
                
            }
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        } else {
            showAlert(message: Strings.Global.somethingWrong)
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        let response = navigationResponse.response
      guard let url = response.url else {
        decisionHandler(.cancel)
        return
      }

      if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields as? [String: String] {
        let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
        cookies.forEach { cookie in
          webView.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
        }
      }
      
      decisionHandler(.allow)
    }
    
}

extension WebViewVC: WKNavigationDelegate {
    //Equivalent of shouldStartLoadWith Request: 
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    //Equivalent of didFailLoadWithError:
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    //Equivalent of webViewDidFinishLoad:
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if isLinkWebView {
            self.webView.scrollView.minimumZoomScale = 1.0
            self.webView.scrollView.maximumZoomScale = 5.0
        }
    }
}
