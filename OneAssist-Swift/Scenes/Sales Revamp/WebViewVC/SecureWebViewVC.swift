//
//  SecureWebViewVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 13/05/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

extension NSURLRequest {
    static func allowsAnyHTTPSCertificate(forHost host: String) -> Bool {
        return true
    }
}

class SecureWebViewVC: WebViewVC {
    
    var loadingUnvalidatedHTTPSPage = false
    var connection: NSURLConnection!
    
    override func initializeView() {
        title = screenTitle ?? Strings.PaymentWebViewScene.title
        if let url = URL(string: urlString) {
            webView.navigationDelegate = self
            webView.load(URLRequest(url: url))
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        } else {
            showAlert(message: Strings.Global.somethingWrong)
        }
    }
    
    func handleBackButton() {
        let backButton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(handleNavBackButton))
        backButton.imageInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = backButton
    }
    
    // MARK:- Action Methods
    @objc override func handleNavBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    override func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("shouldStartLoadWith: \(webView.url?.absoluteString ?? "")")
        if let url = webView.url, loadingUnvalidatedHTTPSPage {
            connection = NSURLConnection(request: URLRequest(url: url) , delegate: self)
            connection.start()
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    override func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("URL Finished: \(webView.url?.absoluteString ?? "")")
        Utilities.removeLoader(fromView: view, count: &loaderCount)
    }
    
    override func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("didFailLoadWithError: \(error)")
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        if error._code == NSURLErrorServerCertificateHasBadDate ||
            error._code == NSURLErrorServerCertificateUntrusted ||
            error._code == NSURLErrorServerCertificateHasUnknownRoot ||
            error._code == NSURLErrorServerCertificateNotYetValid {
            
            showAlert(title: Strings.PaymentWebViewScene.warning, message: Strings.PaymentWebViewScene.warningMessage, primaryButtonTitle: Strings.PaymentWebViewScene.stop, secondaryButtonTitle: Strings.PaymentWebViewScene.continue, primaryAction: {
            }, secondaryAction: {
                let urlRequest = URLRequest(url: URL(string: self.urlString)!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
                self.loadingUnvalidatedHTTPSPage = true
                self.webView.load(urlRequest)
            })
        }
        print("Error: \(error)")
    }
}

extension SecureWebViewVC: NSURLConnectionDataDelegate {
    func connection(_ connection: NSURLConnection, willSendRequestFor challenge: URLAuthenticationChallenge) {
        if let trust = challenge.protectionSpace.serverTrust {
            challenge.sender?.use(URLCredential(trust: trust), for: challenge)
        }
    }
    
    func connection(_ connection: NSURLConnection, didReceive response: URLResponse) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let response = response as? HTTPURLResponse {
            if response.statusCode == 404 {
                showAlert(message: "PAGE NOT FOUND")
            } else {
                let urlRequest = URLRequest(url: URL(string: self.urlString)!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
                loadingUnvalidatedHTTPSPage = false
                webView.load(urlRequest)
                connection.cancel()
            }
        }
    }
}
