//
//  BankBranchesListInteractor.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 19/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

protocol BankBranchesListPresentationLogic {
    func responseRecieved(response: WalletCardsResponseDTO?, error: Error?)
}

class BankBranchesListInteractor: BaseInteractor, BankBranchesListBusinessLogic {
    var presenter: BankBranchesListPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getCustomerCards() {
        let _ = WalletCardsUseCase.service(requestDTO: WalletCardsRequestDTO()) { (usecase, response, error) in
            self.presenter?.responseRecieved(response: response, error: error)
        }
    }
}
