//
//  BankBranchesListPresenter.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 19/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

protocol BankBranchesListDisplayLogic: class {
    func cardsListRecieved(cardList: WalletCardsResponseDTO)
    func errorRecieved(_ error: String)
}

class BankBranchesListPresenter: BasePresenter, BankBranchesListPresentationLogic {
    weak var viewController: BankBranchesListDisplayLogic?

    // MARK: Presentation Logic Conformance
    func responseRecieved(response: WalletCardsResponseDTO?, error: Error?) {

        if let error = error {
            viewController?.errorRecieved(error.localizedDescription)
        } else if response?.status?.lowercased() == Constants.ResponseConstants.success.lowercased() {
            viewController?.cardsListRecieved(cardList: response!)
        } else {
            viewController?.errorRecieved(Strings.Global.somethingWrong)
        }
    }
}
