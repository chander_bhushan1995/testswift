//
//  BankBranchesListVC.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 19/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

protocol BankBranchesListBusinessLogic {
    func getCustomerCards()
}

class BankBranchesListVC : BaseVC, AddCardButtonDelegate {

    var issuerList : [WalletCard]?

    var interactor: BankBranchesListBusinessLogic?

    @IBOutlet weak var bankBranchListTableView: UITableView!

    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }

    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }

    override func viewWillAppear(_ animated: Bool) {
        configureView()
    }

    // MARK:- private methods
    private func initializeView() {
        self.title = "Select bank name"
        NotificationCenter.default.addObserver(self, selector: #selector(configureView), name: .refreshViewController, object: nil)
        bankBranchListTableView.register(nib: Constants.NIBNames.bankListCell, forCellReuseIdentifier: Constants.CellIdentifiers.bankListCell)
        bankBranchListTableView.separatorStyle = .none
        EventTracking.shared.eventTracking(name: .selectBank)
    }

    @objc private func configureView() {
        if Utilities.isCustomerVerified() {
             Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
           interactor?.getCustomerCards()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .refreshViewController, object: nil)
    }
}

extension BankBranchesListVC : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issuerList?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.bankListCell, for: indexPath) as! BankListCell

        if indexPath.row == 0 {
            cell.topSeperator.isHidden = false
        }else{
            cell.topSeperator.isHidden = true
        }
        cell.issuerNameLabel.text = issuerList?[indexPath.row].issuerName
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.selectedCellDotView.isHidden = true
        return cell
    }
}

extension BankBranchesListVC : UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return BankListCell.cellHeight()
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return BankListHeaderView.viewHeight()
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return BankListFooterView.viewHeight()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = BankListHeaderView.instanceFromNib()
        headerView.title.text = Strings.nearByBankScene.selectBank
        return headerView
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = BankListFooterView.instanceFromNib()
        footerView.noBankTitle.text = Strings.nearByBankScene.noBank
        footerView.noBankDescription.text = Strings.nearByBankScene.addFirstCard
        if let issuerList = issuerList, issuerList.count > 0 {
            footerView.noBankDescription.text = Strings.nearByBankScene.addCard
        }
        footerView.delegate = self
        return footerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let issuerData = issuerList![indexPath.row]
        routeToBankMapScene(issuer : issuerData)
    }
}

// MARK:- Display Logic Conformance
extension BankBranchesListVC: BankBranchesListDisplayLogic {
    func cardsListRecieved(cardList: WalletCardsResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let cardListData = cardList.data?.cards {
            issuerList = cardListData.unique{$0.issuerCode ?? ""}.filter({ $0.isBankCard })
            bankBranchListTableView.reloadData()
        }
    }

    func errorRecieved(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

// MARK:- Configuration Logic
extension BankBranchesListVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = BankBranchesListInteractor()
        let presenter = BankBranchesListPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension BankBranchesListVC {
    func routeToBankMapScene(issuer : WalletCard) {
        let vc = BankMapVC()
        vc.issuerModel = issuer
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK:- AddCardButtonDelegate
extension BankBranchesListVC    {
    func addCardButtonTapped() {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.addCard.message, subTitle: nil, forScreen: "Add Card") { (status) in
            if status {
                EventTracking.shared.eventTracking(name: .addCard, [.location: Event.EventParams.bank])
                let vc = CardManagementVC()
                vc.addToInitialProperties(["deeplink_url_string" : "addcard/DBC", "navigatingFrom": "Bank branch finder Screen"])
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}
