//
//  CatalystVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 17/01/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit


class CatalystVC: ReactMainNavigationHideVC {

    override func moduleName() -> String {
        return "CatalystFlow"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
