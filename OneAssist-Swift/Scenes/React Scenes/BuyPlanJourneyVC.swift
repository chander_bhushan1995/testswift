//
//  ServiceDetailScreen.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 14/12/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation


class BuyPlanJourneyVC: ReactMainNavigationHideVC {
    
    var membershipResponse: CustomerMembershipDetailsResponseDTO?
    var rootViewBGColor: UIColor?
    
    override func moduleName() -> String {
        return "CommonIndexModule"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let bgColor = rootViewBGColor {
            self.reactView?.backgroundColor = bgColor
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeRatingView()
    }
    
    func removeRatingView(){
        if let tabVC = rootTabVC, let _ = tabVC.rView {
            tabVC.removeRatingView(self.view)
        }
    }
    
    class func routeToRenewalFlow(with membershipDetails: PlanDetailAndSRModel?)->BuyPlanJourneyVC?{
        if let membershipDetails = membershipDetails {
            let viewController = BuyPlanJourneyVC()
            let customer = membershipDetails.customer?.first
            let pinCode = customer?.addresses?.filter({$0.addressType == "INSPECTION"}).first?.pincode ?? customer?.addresses?.first?.pincode
            var customerInfoDict = Serialization.getDictionaryFromObject(object: customer)
            customerInfoDict["previousCustId"] = membershipDetails.customer?.first?.custId
            viewController.addToInitialProperties(["routeName":"PinCodeScreen","data":["buyPlanJourney":["category": Destination.whc.category,"service": Constants.PlanServices.whcInspection,"screenTitle":"","serviceName": Constants.PlanServices.whcInspection,"renewalData":["customerInfo": customerInfoDict,"memID":membershipDetails.membershipId,"prevPlanCode":membershipDetails.planCode,"prevCoverAmount":membershipDetails.coverAmount,"pinCode": pinCode]]]])
            return viewController
        }
        return nil
    }
}
