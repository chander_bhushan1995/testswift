//
//  FavoriteRecommendationsVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 04/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

class FavoriteRecommendationsVC: ReactMainNavigationHideVC {
    override func moduleName() -> String {
        return "FavoriteRecommendations"
    }
}
