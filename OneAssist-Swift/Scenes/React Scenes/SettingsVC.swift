//
//  SettingsVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 01/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class SettingsVC: ReactMainNavigationHideVC {

    override func moduleName() -> String {
        return "MyAccountSettings"
    }
    
    func prepareInitialProps(){
        let status = sharedAppDelegate?.pushPermissionStatus ?? false
        addToInitialProperties(["isPushEnabled": status])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventTracking.shared.addScreenTracking(with: .MyProfile)
    }
}
