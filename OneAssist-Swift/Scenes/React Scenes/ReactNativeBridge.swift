//
//  ReactNativeBridge.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 04/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation
import CodePush

class ReactNativeBridge {
    let bridge: RCTBridge

    init() {
        bridge = RCTBridge(delegate: ReactNativeBridgeDelegate(), launchOptions: nil)
    }

    deinit {
        print("ReactNativeBridge deinit called")
        bridge.invalidate()
    }
}

class ReactNativeBridgeDelegate: NSObject, RCTBridgeDelegate {

    func sourceURL(for bridge: RCTBridge!) -> URL! {
        
        #if DEBUG
        
        if EnvironmentSetting.useNodeServer {
            if let url = EnvironmentSetting.nodeServerIP, !url.isEmpty {
                return URL(string: "http://\(url):8081/index.bundle?platform=ios")
            } else {
                return URL(string: "http://localhost:8081/index.bundle?platform=ios")
            }
        } else {
//            return Bundle.main.url(forResource: "main", withExtension: "jsbundle")!
            return CodePush.bundleURL()
        }
        
        #elseif TEST
        
        if EnvironmentSetting.useNodeServer {
            if let url = EnvironmentSetting.nodeServerIP, !url.isEmpty {
                return URL(string: "http://\(url):8081/index.bundle?platform=ios")
            } else {
                return URL(string: "http://localhost:8081/index.bundle?platform=ios")
            }
        } else {
            return CodePush.bundleURL()
//            return Bundle.main.url(forResource: "main", withExtension: "jsbundle")!
        }
        
        #else
        return CodePush.bundleURL()
//        return Bundle.main.url(forResource: "main", withExtension: "jsbundle")!
        
        #endif
    }
}
