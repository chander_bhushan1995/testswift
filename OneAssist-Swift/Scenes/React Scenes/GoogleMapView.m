//
//  GoogleMapView.m
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 29/10/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTViewManager.h>
 
@interface RCT_EXTERN_MODULE(RCTGoogleMapViewManager, RCTViewManager)
RCT_EXPORT_VIEW_PROPERTY(location, NSDictionary)
RCT_EXPORT_VIEW_PROPERTY(zoomLevel, float)
RCT_EXPORT_VIEW_PROPERTY(onTappedMarker, RCTBubblingEventBlock)
@end
