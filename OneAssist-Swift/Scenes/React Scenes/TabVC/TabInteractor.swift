//
//  TabInteractor.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/04/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

protocol TabPresentationLogic {
    func tempCustomerResponseRecieved(response: TemporaryCustomerInfoResponseDTO?, error: Error?, purchaseDate: Date, activationCode: String)
    func getDocsToUploadResponseRecieved(with plan: PlanDetailAndSRModel, response: GetDocsToUploadResponseDTO?, error: Error?)
    func presentChangedFDType(_ response: BaseResponseDTO?,_ error: Error?, reasonToChange: String?, activationCode: String?, from location: FromScreen)
}

class TabInteractor: BaseInteractor {
    var presenter: TabPresentationLogic?
}

extension TabInteractor: TabBusinessLogic {
    // MARK: Business Logic Conformance
    func performStartInspection(activationCode: String, purchaseDate: Date) {
        /* This is for a temporary user. */
        let req = TemporaryCustomerInfoRequestDTO()
        req.activationCode = activationCode
        
        let _ = TemporaryCustomerInfoRequestUseCase.service(requestDTO: req) {[weak self] (usecase, response, error) in
            self?.presenter?.tempCustomerResponseRecieved(response: response, error: error, purchaseDate: purchaseDate, activationCode: activationCode)
        }
    }
    
    func getDocsToUpload(for plan: PlanDetailAndSRModel) {
        let req = GetDocsToUploadRequestDTO()
        req.activationCode = plan.activationCode ?? ""
        
        let _ = GetDocsToUploadUseCase.service(requestDTO: req) { [weak self] (usecase, response, error) in
            self?.presenter?.getDocsToUploadResponseRecieved(with: plan, response: response, error: error)
        }
    }
    
    func changeFDFlowType(testType: FDTestType, activationCode: String?, activityReferenceId: String?, reasonToChange: String?, from location: FromScreen = .blank) {
        let request = FDTestTypeChangeRequestDTO(modelType: testType.getAPIValue(), reasonValue: reasonToChange, activityReferenceId: activityReferenceId)
        FDTestTypeChangeUseCase.service(requestDTO: request) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.presentChangedFDType(response, error, reasonToChange: reasonToChange, activationCode: activationCode, from: location)
        }
    }
}
