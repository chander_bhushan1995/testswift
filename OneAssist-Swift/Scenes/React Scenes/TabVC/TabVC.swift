//
//  TabVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/04/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit
import WebEngage
import Photos
import StoreKit

enum APIState {
    case notCalled
    case inProgress
    case success
    case failed
}

enum WhcPlanStatus {
    case pending(beforeDate: String)
    case pendingWillBeCancelled
    case pendingWithDelay(daysLeft: String)
    case pendingWithEarliestContact
    case awaiting
    case scheduled
    case failed
    case inspectionCancelled(beforeDate: String)
    case inspectionCancelledNonReschedule
    case membershipCancelled
    case details(membershipData: CustomerMembershipDetails)
    case activatedSuccesfully(membershipData: CustomerMembershipDetails)
}

enum FDMembershipStageActionType {
    case welcomeScreen
    case invoiceAReupload
    case invoiceAndReupload
    case docReupload
    case docSendUploadLink
    case welcomeScreenForVerification
    case activatedSuccessfully
}

protocol TabBusinessLogic {
    func performStartInspection(activationCode: String, purchaseDate: Date)
    func getDocsToUpload(for plan: PlanDetailAndSRModel)
    func changeFDFlowType(testType: FDTestType,activationCode: String?, activityReferenceId: String?, reasonToChange: String?, from location: FromScreen)
}

//these are the actions which comes from react native membership tab screen
enum MembershipTabActions: String {
    case viewDetail = "VIEW_DETAILS"
    case myCards = "MY_CARDS"
    case startHAClaimProcess = "START_HA_RAISE_CLAIM_PROCESS"
    case rateUs = "RATE_US"
    case rescheduleVisit = "SR_RESCHEDULE_VISIT"
    case viewInspectionDetails = "VIEW_INSPECTION_DETAILS"
    case scheduleInspection = "SCHEDULE_INSPECTION"
    case selectAddressInspection = "SELECT_ADDRESS_INSPECTION"
    case fdOpenSendSMSScreen = "FD_OPEN_SEND_SMS_SCREEN"
    case peMembershipActivation = "ACTIVATE_PE_MEMBERSHIP"
    case mobileActivation = "SALES_UPLOAD_DOCUMENTS"
    case handlePendingSubmissionResponse = "HANDLE_PENDING_SUBMISSION_RESPONSE"
    case srViewTimeLine = "SR_VIEW_TIMELINE"
    case srUploadDocument = "SR_UPLOAD_DOCUMENT" // upload document
    case srUploadPendencies = "SR_UPLOAD_PENDENCIES" // reupload document
    case downloadInvoice = "DOWNLOAD_INVOICE"
    case deeplinkWHCSelectAddress = "DEEPLINK_WHC_SELECT_ADDRESS"
    case deeplinkServiceDetail = "DEEPLINK_SERVICE_DETAIL"
}

enum BridgeJSONDataKeys {
    static let tempCustInfoData = "tempCustInfoData"
    static let deeplinkParams = "deeplinkParams"
    static let membershipResponse = "membershipResponse"
    static let buybackStatusModel = "buybackStatusModel"
    static let buyBackStatusData = "buyBackStatusData"
    static let checkAvailabilityResponse = "checkAvailabilityResponse"
    static let srResponseData = "srResponseData"
    static let isPendingMembership = "isPendingMembership"
    static let srID = "SRid"
    static let srNumber = "SRNumber"
    static let srType = "SRType"
    static let activationCode = "activationCode"
    static let intimationArrayData = "intimationArr"
    static let isResumeClaim = "isResumeClaim"
}

class TabVC: ReactMainNavigationHideVC, CommonMirrorTestDelegate {
    
    var navigator: MHCMainNavigator?
    var selectedIndex: TabIndex = .newHome
    var buySubTabSelectedCategory:String? = nil
    var customerMembershipResponse: CustomerMembershipDetailsResponseDTO?
    var interactor: TabBusinessLogic?
    var validateMTSaveImage: Bool = false
    var userSawTutorial = false
    var isShowScratchCard = false
    var rView: OARatingView?
    var downloadManager = TFLiteModelsDownloadManager.sharedInstance
    var onDownloadCompletion: (()->())? = nil
    
    override func moduleName() -> String {
        return "TabBar"
    }

    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // TAB
        sharedAppDelegate?.registrationDetail()
        Publisher.NewPushNotificationReceived.observe(observer: self, selector: #selector(newPushNotificationReceived))
        
        // MEMBERSHIP
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleRefresh), name: Notification.Name.membershipTab, object: nil)
        
        // ACCOUNT
        showNavigationShadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        // TAB
        setBellNotification()
        
        if let from = sharedAppDelegate?.forInternalDeeplink, let finalTabForInternalDeeplink = sharedAppDelegate?.finalTabForInternalDeeplink, from {
            changeTabIndex(itemTag: finalTabForInternalDeeplink)
            sharedAppDelegate?.forInternalDeeplink = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // TAB
        // this method should call before checkCatalystDeeplink so that tutorial will be visible or not could be decided
        showTabbarTutorial()
        
        if isShowScratchCard && RemoteConfigManager.shared.rewardFeatureOn {
            isShowScratchCard = false
            Utilities.openScratchCard(state: .LOCKED) { [unowned self] in
                self.showTabbarTutorial()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // MEMBERSHIP
        removeRatingView(self.view)
    }
    
    @objc func handleRefresh() {
        ChatBridge.sendNotification(withName: "RefreshMembershipsOnTab", body: [:])
    }
    
    @objc private func newPushNotificationReceived(){
        setBellNotification()
    }
    
    func changeBuyPlanCategory(_ category: String) {
        ChatBridge.sendNotification(withName: "ChangeBuyPlanCategory", body: ["category": category])
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- private methods
    
    fileprivate func initializeTabBars() {
        Utilities.downloadJavascriptFile()
        
        let questionStatus = checkQuestionStatus()
        let (isRenew, isLead, haServices, peProducts, fProducts) = getLeadAndInReviewStatus()
        let isIDFenceInMem = customerMembershipResponse?.checkCustomerHaveAnyIDFencePlan() ?? false
        var newProps: [String : Any] = ["isLead": isLead,
                        "isRenew": isRenew,
                        "availableHAServices": Array(haServices),
                        "availablePEProducts": peProducts,
                        "availableFProducts": Array(fProducts),
                        "isAnsweredAllQuestions": !questionStatus,
                        "isIDFenceInMem": isIDFenceInMem,
                        "rewardScratchDate": UserDefaults.standard.string(forKey: "rewardScratchDate"),
                        "isNonSubscriber": UserDefaults.standard.bool(forKey: UserDefaultsKeys.isNonSubscriber.rawValue)]
        
        if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let data = responseDTO.data, !data.isEmpty {
            let dictObject = Serialization.getDictionaryFromObject(object: responseDTO)
            if JSONSerialization.isValidJSONObject(dictObject) {
                if let data = try? JSONSerialization.data(withJSONObject: dictObject, options: []), let jsonString = String(data: data, encoding: .utf8) {
                    newProps["onBoardingQA"] = jsonString
                }
            }
        }
        
        //handling deeplink on react view, if login is not required on deeplink
        if Utilities.isCustomerVerified() || !DeepLinkManager.isLoginRequired() {
            if let deeplink = DeepLinkManager.getDeeplinkForHomeTab() ?? DeepLinkManager.getDeeplinkForAccountTab() ?? DeepLinkManager.getDeeplinkForMemberShipTab() {
                newProps["deeplinkPath"] = currentDeeplinkUrl ?? deeplink.rawValue
            }
        }
        
        updateProps(newProps)
        
        if !DeepLinkManager.anyDeeplinkAvailable() {
            checkForFraudDetectionMembership()
        } else {
            DeepLinkManager.checkDeeplink(for: self)
        }
        setBellNotification()
    }
    
    private func setInitialTabSelected() {
        var initialIndex = TabIndex.newHome
        if let selected = CacheManager.shared.selectedTabIndex {
            initialIndex = selected
        } else {
            initialIndex = isInitialTabMembership(response: customerMembershipResponse) ? .membership : .newHome
        }
        
        CacheManager.shared.selectedTabIndex = nil
        changeTabIndex(itemTag: initialIndex)
    }
    
    private func setBellNotification() {
        if let count = NotificationsCD.getCount(predicateString: "read == \(NSNumber(value: false))", inManagedObjectContext:CoreDataStack.sharedStack.mainContext) {
            ChatBridge.updateNotificationCount(count)
        }
    }
    
    func notificationClicked() {
        EventTracking.shared.eventTracking(name: .inBox)
        routeTo(viewController: NotificationsVC())
    }
    
    @objc func chatClicked(_ sender: Any) {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.chatWithUs.message, subTitle:nil, forScreen: "Chat") {[unowned self] (status) in
            if status {
                EventTracking.shared.eventTracking(name: .chat ,[.location: "Home Screen"])
                EventTracking.shared.eventTracking(name: .chatOnHomeTapped)
                self.present(ChatVC(), animated: true, completion: nil)
                EventTracking.shared.addScreenTracking(with: .chat)
            }
        }
    }
    
    func getFeedbacks() {
        guard let membershipsIDs = customerMembershipResponse?.data?.memberships?.map({ $0.memId?.description ?? "" }) else { return }
        
        ServiceRequestGetFeedback().getFeedbackWithCompletion(membershipsIDs: membershipsIDs, completionHandler: {[weak self] (response, error) in
            if let firstSR = response?.data?.first, error == nil {
                var productName:String = ""
                var productImage : String?
                var headerText: String?
                var isMultipleAppliances = false

                if firstSR.serviceRequestType == Constants.PlanServices.whcInspection {
                    productName = "HomeAssist Plan"
                    productImage = "homeServ"
                    headerText = "Please give us a feedback on inspection for your HomeAssist Plan"
                } else {
                    if let assests = firstSR.assets{
                        if assests.count > 1{
                            isMultipleAppliances = true
                        }
                        for item in assests{
                            if let prodCode = item.productCode {
                                let model = Utilities.getProduct(productCode: prodCode)
                                productImage = model?.subCatImageUrl
                                productName = productName + "\(item.make ?? "") \(model?.subCategoryName ?? "")" + ", "
                            }
                            productName.removeLast()
                            productName.removeLast()
                        }
                        
                    }
                    headerText = "Please give us a feedback for your previous service request"
                }
                
                let srType = Constants.Services(rawValue: firstSR.serviceRequestType  ?? "") ?? .extendedWarranty
                
                let vc = FeedbackRatingVC()
                vc.serviceRequestNumber = firstSR.refPrimaryTrackingNo?.description
                vc.serviceID = firstSR.serviceRequestId?.description
                vc.productName = productName
                vc.isMultipleAssests = isMultipleAppliances
                vc.productImageUrl = productImage
                vc.headerText = headerText
                vc.srType = srType
                self?.presentInFullScreen(vc, animated: true, completion: nil)
            }
        })
    }
    
    func getQuestionAnswerList(completionBlock: @escaping () -> Void) {
        UserQuestionAnswerUseCase().getRequest(requestDto: GetQuestionAnswerRequestDTO()) { (_, _) in
            completionBlock()
        }
    }
    
    private func isInitialTabMembership(response: CustomerMembershipDetailsResponseDTO?) -> Bool {
        let memberships = response?.data?.memberships ?? []
        let pendingMemberships = response?.data?.pendingMemberships ?? []
        
        // expiring
        let hasExpiringMembership = memberships.contains(where: { $0.isExpiring() && ($0.isPEMembership() || $0.isWalletMembership() || $0.isHAMembership())})
        if hasExpiringMembership { return true }
        
        // fd case
        let hasPEFDMemberships = pendingMemberships.contains(where: { $0.isPendingMembership(with: Strings.FDetectionConstants.mobileProductCode) && $0.plan?.trial == "N" })
        if hasPEFDMemberships { return true }
        
        // whc pending
        let hasHAPendingMemberships = pendingMemberships.contains(where: { $0.isHAMembership() && $0.task?.name == "INSPECTION"})
        if hasHAPendingMemberships { return true }
        
        // claim
        // TODO: for claim handling
        let hasPendingSRs = memberships.contains(where: {($0.claims?.contains(where: {[Constants.TimelineStatus.onHold, Constants.TimelineStatus.pending, Constants.TimelineStatus.inProgress].contains( $0.status ?? "" ) && !(["CS", "DS"].contains($0.workflowStage))}) ?? false ) })
        if hasPendingSRs { return true }
        
        return false
    }
    
    func isNewVersion() -> Bool {
        var isNewVersion = false
        
        if let _ = UserDefaults.standard.value(forKey: UserDefaultsKeys.appUpdateed.rawValue) {
            isNewVersion = true
        }
        
        return isNewVersion
    }
    
    func showTabbarTutorial() {
        if(DeepLinkManager.anyDeeplinkAvailable()){ // if any deeplink available then don't show tutorial so that another screen will be open over tabbar
            userSawTutorial = true
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            return
        }
        
        if isNewVersion() &&
            Utilities.isCustomerVerified() &&
            !UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue) &&
            !isShowScratchCard {
            let bottomSheetView = UserUpdateView()
            var model = UserUpdateModel()
            if let firstname = CustomerDetailsCoreDataStore.currentCustomerDetails?.firstName {
                model.title = "\(Strings.Global.welcomeUser), \(firstname)!"
            }else {
                model.title = Strings.Global.hiUser
            }
            model.detail = Strings.Global.welcomeMessage
            
            bottomSheetView.setUpValue(model)
            bottomSheetView.delegate = self
            Utilities.presentPopover(view: bottomSheetView, height:UserUpdateView.height(forModel: model))
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            userSawTutorial = true
            
        } else if !UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue) && !isShowScratchCard {
            // TODO: KAG, Make changes here!
            userSawTutorial = true
            //show tab bar tutorial
            let tabBarTutorialScreen = TabBarTutorialScreen()
            let tutorialNav = BaseNavigationController(rootViewController: tabBarTutorialScreen)
            tutorialNav.view.backgroundColor = .clear
            tutorialNav.modalPresentationStyle = .overFullScreen
            self.definesPresentationContext = true
            self.present(tutorialNav, animated: true, completion: nil)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            
        }else if !AppUserDefaults.isShowPushPromptOnTab {
            AppUserDefaults.isShowPushPromptOnTab = true
            showNotificationPrompt(from: nil, eventLocation: Event.EventParams.home)
        }
    }
    
    func checkQuestionStatus() -> Bool {
        let userName = UserCoreDataStore.currentUser?.userName ?? ""
        let userEmail = CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? ""
        if let responseDTO = DataDiskStogare.getCustomerQuestionData(), let questionAnswersList = responseDTO.data, !questionAnswersList.isEmpty {
            let isNotFilled = questionAnswersList.contains(where: { $0.answered != 1 })
            if(isNotFilled || userName.isEmpty || userEmail.isEmpty) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func checkForFraudDetectionMembership() {
        if let pendingFDMem = customerMembershipResponse?.data?.pendingMemberships?.first(where: { $0.isPendingMembership(with: Strings.FDetectionConstants.mobileProductCode) && $0.plan?.trial == "N" && $0.task?.status == Strings.PEScene.FraudDetection.Status.strPostDtlPending && Strings.Global.apple.caseInsensitiveCompare($0.assets?.first(where: {$0.prodCode == Strings.FDetectionConstants.mobileProductCode })?.brand ?? Strings.Global.apple) == .orderedSame }) {
            
            FDCacheManager.clearCache()
            FDCacheManager.shared.isShowFullForm = pendingFDMem.glance?.boolValue ?? false
            FDCacheManager.shared.maxInsuranceValue = pendingFDMem.maxInsuranceValue
            FDCacheManager.shared.minInsuranceValue  = pendingFDMem.minInsuranceValue
            FDCacheManager.shared.minDeviceDate = pendingFDMem.minPurchaseDateForGlance
            
            let brandName = pendingFDMem.assets?.first?.brand
            let testType: String = pendingFDMem.task?.fdTestType ?? FDTestType.SDT.getAPIValue()
            
            if let vc = Utilities.topMostPresenterViewController as? NotificationPromptVC {
                vc.onDismiss = {
                    self.startFDFlow(for: pendingFDMem.activationCode, with: pendingFDMem.plan?.coverAmount, with: pendingFDMem.activityRefId, brand: brandName, testType: testType)
                }
            } else {
                if let topMostView = Utilities.topMostPresenterViewController as? BaseNavigationController {
                    if let _ = topMostView.topViewController as? TabVC {
                        self.startFDFlow(for: pendingFDMem.activationCode, with: pendingFDMem.plan?.coverAmount, with: pendingFDMem.activityRefId,brand: brandName, testType: testType)
                    }
                }
            }
        }
    }
    
    func getLeadAndInReviewStatus() -> (Bool, Bool, Set<String>, [String:[String]], Set<String>) {
        var isRenew = false
        var isLead = false
        
        var availableHAServices = Set<String>()
        var availablePEProducts = [String:Set<String>]()
        var availableFProducts = Set<String>()
        if Utilities.isCustomerVerified() {
            let data = customerMembershipResponse?.data
            let memberships = data?.memberships
            
            if memberships?.contains(where: {$0.isExpiring() && ($0.isPEMembership() || $0.isHAMembership() || $0.isHAMembership())}) ?? false { isRenew = true }
            
            memberships?.filter({ $0.isPEMembership() }).forEach({
                for product in $0.products ?? [] {
                    var serviceType = "ADLD"
                    for service in $0.plan?.services ?? [] {
                        switch service.serviceName {
                        case Constants.PlanServices.adld: serviceType = "ADLD"
                        case Constants.PlanServices.extendedWarranty :serviceType = "EW"
                        default: break
                        }
                        if var key = availablePEProducts[product] {
                            key.insert(serviceType)
                        } else {
                            availablePEProducts[product] = Set<String>(arrayLiteral: serviceType)
                        }
                    }
                }
            })
            
            memberships?.filter({ $0.isWalletMembership() }).forEach({
                if ($0.checkIsMembershipIDFence()){
                    availableFProducts.insert("IDF")
                } else {
                    for product in $0.products ?? [] {
                        availableFProducts.insert(product)
                    }
                }
            })
            
            memberships?.filter({ $0.isHAMembership() }).forEach({
                if ($0.isWhcMembership()) { availableHAServices.insert("WHC") }
                else if $0.isHAEWMembership() { availableHAServices.insert("EW") }
            })
            
            if data?.leadData != nil {
                isLead = true
            }
        }
        let availablePEProduct = availablePEProducts.mapValues { (valuesSet) -> Array<String> in
            return Array(valuesSet)
        }
        return (isRenew, isLead, availableHAServices, availablePEProduct, availableFProducts)
    }
    
    /// This method is used to parse data which came from react native for membership card actions
    /// - Parameter jsonData: json data which recieved from react native
    /// - Returns: it return multiple values in form of touple
    private func parseResponseToViewModel(jsonData: NSDictionary) ->  (buybackStatusList: [BuyBackStatus],planDetailAndSRModel: PlanDetailAndSRModel?, tempCustomerResponse: TemporaryCustomerInfoResponseDTO?, pendingSubmissionResponse: CheckAvailabilityResponseDTO?, deeplinkQueryParams: [String: Any], intimationData: [Intimation]){
        var buybackStatusList = [BuyBackStatus]()
        var membershipMappedServices = [SearchService]()
        var planDetailAndSRModel: PlanDetailAndSRModel?
        var tempCustomerInfo: TemporaryCustomerInfoResponseDTO?
        var pendingSubmissionResponse: CheckAvailabilityResponseDTO?
        let recievedApplianceResponse = HomeApplianceCategoryResponseDTO(dictionary: [:])
        var intimationData = [Intimation]()
        var memToSRMapping = [String: [SearchService]]()
        var deeplinkQueryParams = [String: Any]()
        
        if let jsonData = jsonData as? Dictionary<String,Any> {
            
            if let applianceTopCategoriesData = RemoteConfigManager.shared.homeAppliancesTopCategories.data(using: .utf8),let parsedApplianceData: HomeApplianceCategoryResponseDTO = try? JSONParserSwift.parse(data: applianceTopCategoriesData) {
                recievedApplianceResponse.subCategories?.append(contentsOf: parsedApplianceData.subCategories ?? [])
            }
            
            if let applianceOtherCategoriesData = RemoteConfigManager.shared.homeAppliancesOtherCategories.data(using: .utf8),let parsedApplianceData: HomeApplianceCategoryResponseDTO = try? JSONParserSwift.parse(data: applianceOtherCategoriesData) {
                recievedApplianceResponse.subCategories?.append(contentsOf: parsedApplianceData.subCategories ?? [])
            }
            
            // for now this data came in case of whc select address deeplink
            if let tempCustomerRes = (jsonData[BridgeJSONDataKeys.tempCustInfoData] as? Dictionary<String,Any>){
                if  let parsedTempCustomerRes: TemporaryCustomerInfoResponseDTO = try? JSONParserSwift.parse(dictionary: tempCustomerRes){
                    tempCustomerInfo = parsedTempCustomerRes
                }
            }
            
            if let deeplinkParams = (jsonData[BridgeJSONDataKeys.deeplinkParams] as? Dictionary<String,Any>) {
                deeplinkQueryParams = deeplinkParams
            }
            
            if let membershipRes = jsonData[BridgeJSONDataKeys.membershipResponse] as? Dictionary<String,Any> { // handle if we got membership for particual action
                
                // parse array buyback response data
                if let buybackListRes = membershipRes[BridgeJSONDataKeys.buybackStatusModel] as? Dictionary<String,Any>  {
                    if  let parsedBuybackRes: BuyBackStatus = try? JSONParserSwift.parse(dictionary: buybackListRes){
                        buybackStatusList = [parsedBuybackRes]
                    }
                }
                
                if let response = membershipRes[BridgeJSONDataKeys.checkAvailabilityResponse] as? Dictionary<String,Any> {
                    if  let parsedPendingSubmissionResponse: CheckAvailabilityResponseDTO = try? JSONParserSwift.parse(dictionary: response) {
                        pendingSubmissionResponse = parsedPendingSubmissionResponse
                    }
                }
                
                if let membershipServArray = (membershipRes[BridgeJSONDataKeys.srResponseData] as? [Dictionary<String,Any>]){
                    if  let parsedServicesRes: [SearchService] = try? JSONParserSwift.parse(array: membershipServArray){
                        membershipMappedServices = parsedServicesRes
                        memToSRMapping = getMemToSRMapping(from: membershipMappedServices)
                    }
                }
                // in case if action is fired from mebership tab for specific membership
                if let tempCustomerRes = (membershipRes[BridgeJSONDataKeys.tempCustInfoData] as? Dictionary<String,Any>){
                    if  let parsedTempCustomerRes: TemporaryCustomerInfoResponseDTO = try? JSONParserSwift.parse(dictionary: tempCustomerRes){
                        tempCustomerInfo = parsedTempCustomerRes
                    }
                }
                
                if let intimationDataDictionary = (membershipRes[BridgeJSONDataKeys.intimationArrayData] as? [Dictionary<String,Any>]){
                    if let parsedData: [Intimation] = try? JSONParserSwift.parse(array: intimationDataDictionary){
                        intimationData = parsedData
                    }
                }
                
                // if membership is pending membership
                if let isPendingMembership = membershipRes[BridgeJSONDataKeys.isPendingMembership] as? Bool, isPendingMembership, let parsedPendingMembershipRes: CustomerPendingMembershipDetails = try? JSONParserSwift.parse(dictionary: membershipRes)  {
                    parsedPendingMembershipRes.tempCustomerInfo = tempCustomerInfo
                    if parsedPendingMembershipRes.isPEMembership() { // handling for pe
                        if tempCustomerInfo?.enableFDFlow?.boolValue ?? false { // check for mobile/FD Flow
                            planDetailAndSRModel = [parsedPendingMembershipRes].getMobilePlans().first
                        }else {
                            planDetailAndSRModel = [parsedPendingMembershipRes].getPEPlans(for: recievedApplianceResponse.subCategories ?? [], memToSRMapping: memToSRMapping).first
                        }
                    } else if (parsedPendingMembershipRes.checkIsMembershipIDFence()) || ((parsedPendingMembershipRes.plan?.categories?.count ?? 0) <= 1 && parsedPendingMembershipRes.isPendingMembership(with: Strings.Global.walletProductCode)) {
                        planDetailAndSRModel = [parsedPendingMembershipRes].getWalletPlans().first
                    } else if parsedPendingMembershipRes.isHAMembership() {
                        if parsedPendingMembershipRes.isWhcMembership(){
                            planDetailAndSRModel = [parsedPendingMembershipRes].getWHCPlans(for: recievedApplianceResponse.subCategories ?? []).first
                        }else{
                            planDetailAndSRModel = [parsedPendingMembershipRes].getEWPlans(for: recievedApplianceResponse.subCategories ?? [], memToSRMapping: memToSRMapping).first
                        }
                    }
                }else { // if non - membership is pending membership
                    if  let parsedMembershipRes: CustomerMembershipDetails = try? JSONParserSwift.parse(dictionary: membershipRes) {
                        if (parsedMembershipRes.isPEMembership()) { // handling for PE membership
                            planDetailAndSRModel = [parsedMembershipRes].getMobilePlans(with: memToSRMapping, buybackData: buybackStatusList).first
                        }
                        else if (parsedMembershipRes.isWalletMembership()) {
                            planDetailAndSRModel = [parsedMembershipRes].getWalletPlans().first
                        }
                        else if (parsedMembershipRes.isHAMembership()) { // handling for HA membership
                            if (parsedMembershipRes.isWhcMembership()){ // handling for whc membership
                                planDetailAndSRModel = [parsedMembershipRes].getWHCPlans(for: recievedApplianceResponse.subCategories ?? [], memToSRMapping: memToSRMapping).first
                            }else if (parsedMembershipRes.isHAEWMembership()){ // handling for HA_EW membership
                                planDetailAndSRModel = [parsedMembershipRes].getEWPlans(for: recievedApplianceResponse.subCategories ?? [], memToSRMapping: memToSRMapping).first
                            }
                        }
                    }
                }
            }
            
            if let buybackStatusListData = jsonData[BridgeJSONDataKeys.buyBackStatusData] as? Dictionary<String,Any> { // handle if we got buyback data for particual action
                // parse array buyback response data
                if  let parsedBuybackRes: BuyBackStatusData = try? JSONParserSwift.parse(dictionary: buybackStatusListData), let buybackStatus = parsedBuybackRes.buybackStatus {
                    buybackStatusList = buybackStatus
                }
            }
        }
        return (buybackStatusList, planDetailAndSRModel, tempCustomerInfo, pendingSubmissionResponse, deeplinkQueryParams, intimationData)
    }
    
    private func getMemToSRMapping(from serviceRequests: [SearchService]?) -> [String: [SearchService]]{
        var memToSRMapping: [String: [SearchService]] = [:]
        if let serviceRequests = serviceRequests {
            for serviceRequest in serviceRequests {
                if let memID = serviceRequest.referenceNo {
                    if var arr = memToSRMapping[memID] {
                        arr.append(serviceRequest)
                        memToSRMapping[memID] = arr
                    } else {
                        memToSRMapping[memID] = [serviceRequest]
                    }
                }
            }
        }
        return memToSRMapping
    }
    
    private func handleRatingPopUp(model: PlanDetailAndSRModel?){
        if let model = model, let serviceRequest = model.serviceRequests.first {
            if model.category == Strings.Global.whc {
                showRatingView(for: serviceRequest.memId, and: serviceRequest.srId)
            } else {
                /* It means in PE and EW case we will not show renewal card at the time of rating if membership is in renewal period. As of now renewal is not there in EW. So only PE case is left now. May handle this in future. */
                showRatingView(for: nil, and: serviceRequest.srId)
            }
        }
    }
    
    private func handleInspectionProcess(model: PlanDetailAndSRModel?){
        if let plan = model {
            // For WHC Case.
            let status = plan.whcPlanStatus
            switch status {
            case .inspectionCancelled:
                EventTracking.shared.eventTracking(name: .rescheduleInspectionTapped)
                routeToScheduleVisitForReschedule(plan: plan)
            case .scheduled:
                EventTracking.shared.eventTracking(name: .showInspectionDetails)
                routeToInspectionDetails(plan: plan)
            default:
                Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                let purchaseDate = Date.getDate(from: plan.planPurchaseDate ?? "", with: DateFormatter.serverDateFormat)
                interactor?.performStartInspection(activationCode: plan.activationCode ?? "", purchaseDate: purchaseDate)
            }
        }
    }
    
    private func  isReuploadForFrontOrBackPanelOnly(with verificationDocs: [VerificationDocument]?) -> Bool {
        if Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs) ||
            Utilities.isReuploadCase(of: Constants.RequestKeys.backPanel, with: verificationDocs){
            return true
        }
        // Here I am assuming that if it is reupload case then there would be at least one document whose reupload would be needed
        return false
    }
    
    func getMembershipsWithDates() -> [(String,String)] {
        var membershipWithDates:[(String,String)] = []
        let mobileMemberships = customerMembershipResponse?.data?.memberships?.filter({ $0.trial != "Y" && $0.membershipStatus == "A" }) ?? []
        for membership in mobileMemberships {
            if let memId = membership.memId, let startDate = membership.startDate {
                membershipWithDates.append((memId.description, startDate.description))
            }
        }
        return membershipWithDates;
    }
    
    private func isReuploadInvoiceAdditionalImage(with docsToUploadResponse: GetDocsToUploadResponseDTO?) -> Bool {
        let verificationDocs = docsToUploadResponse?.deviceDetails?.first?.verificationDocuments
        
        if Utilities.isReuploadCase(of: Constants.RequestKeys.invoice_image, with: verificationDocs) || Utilities.isReuploadCase(of: Constants.RequestKeys.additional_invoice_image, with: verificationDocs) {
            return true
        }
        
        // Here I am assuming that if it is reupload case then there would be at least one document whose reupload would be needed
        return false
    }
    
    private func handlePEActivationFlow(model: PlanDetailAndSRModel?){
        let activationCode = model?.activationCode
        if let plan = model, plan.category == Constants.Category.personalElectronics {
            if  plan.isPendingPlan {
                if let activationCode = plan.activationCode {
                    let paymentVC = OAPaymentVC()
                    paymentVC.addToInitialProperties(["service_Type": "PE", "postboarding_screen_act_code": activationCode])
                    present(paymentVC, animated: true, completion: nil)
                }
                return
            }
            // It is FD case i.e PE pending
            let status = plan.fdStageActionType
            FDCacheManager.clearCache()
            FDCacheManager.shared.setupGlanceValue(plan)
            FDCacheManager.shared.tempCustInfoModel = getTempCustomerModel(from: plan)
            let activityRefId = getTempCustomerModel(from: plan).customerDetails?.first?.activityRefId
            
            switch status {
            case .welcomeScreen:
                routeToMembershipActivation(activationCode: activationCode, activityRefId: activityRefId)
                break
            case .welcomeScreenForVerification:
                let orderId = plan.tempCustomerDetail?.first?.orderId?.stringValue
                if let saveObject = SaveFDDataMode.getSaveData(orderId) {
                    if FDCacheManager.shared.fdTestType == .MT {
                        // if front panel is already saved
                        if saveObject.mtFrontPanelName != nil {
                            validateMTSaveImage = true
                            Utilities.addLoader(onView: view,count: &loaderCount, isInteractionEnabled: false)
                            interactor?.getDocsToUpload(for: plan)
                        } else {
                            routeToMembershipActivation(activationCode: activationCode, activityRefId: activityRefId)
                        }
                    } else {
                        if let _ = saveObject.smsNumber {
                            EventTracking.shared.eventTracking(name: .viewVerificationScreen ,[.location: "Membership Tab", .mode: FDCacheManager.shared.fdTestType.rawValue])
                            routeToFinalIMEIScreen()
                        } else {
                            routeToSendSMSScreen(imeinumber: nil)
                            // Here you need to redirect user to Send SMS screen. Here control will come only in case of FD
                        }
                    }
                    
                } else {
                    routeToMembershipActivation(activationCode: activationCode, activityRefId: activityRefId)
                }
                break
            case .docReupload:
                Utilities.addLoader(onView: view,count: &loaderCount, isInteractionEnabled: false)
                interactor?.getDocsToUpload(for: plan)
            default:
                ()
            }
        }
    }
    
    func handleSRRescheduleVisit(plan: PlanDetailAndSRModel?){
        if let plan = plan {
            if let serviceReq = plan.serviceRequests.first {
                var assestId: String?
                var productCode: String?
                if let assests = serviceReq.assetsDetail,
                   let firstAssest = assests.first {
                    productCode = firstAssest.productCode
                    assestId = firstAssest.assetReferenceNo
                }
                routeToScheduleInspection(srId: serviceReq.srId, claimType: serviceReq.claimType, crmTrackingNumber: serviceReq.crmTrackingNumber ,pincode :serviceReq.serviceAddress?.pincode,serviceRequestType: serviceReq.serviceRequestType, memId: serviceReq.memId, assestId: assestId, productCode: productCode)
            }
        }
    }
    
    func handlePendingSubmissionResponse(model: PlanDetailAndSRModel?, pendingSubmissionResponse: CheckAvailabilityResponseDTO?){
        if let plan = model, let response = pendingSubmissionResponse, let serviceReq = plan.serviceRequests.first {
            CacheManager.shared.memResponse = plan.membershipResponse
            var startDate:String = ""
            for item in getMembershipsWithDates() {
                if item.0 == serviceReq.memId {
                    startDate = item.1
                    break;
                }
            }
            // let dateFromString = DateFormatter.scheduleFormat.date(from: model.createdOn ?? "")
            let dateFromString = Date(string: startDate, formatter: .serverDateFormat)
            let timeInterval = dateFromString?.timeIntervalSince1970
            ClaimHomeServe.shared.purchaseDate = NSNumber(value:(timeInterval ?? 0)*1000)
            
            if let assets = response.data, assets.count > 0 {
                var assetsAvailability = [String: AssetEligibility]()
                for asset in assets {
                    if let assetId = asset.assetId {
                        assetsAvailability[assetId] =  AssetEligibility(eligible_: asset.eligibleForRaiseClaim == "Y", reason_: asset.message ?? "")
                    }
                }
                self.displayEligibilityForPendingSubmission(model: serviceReq, assetsAvailability: assetsAvailability)
            }
        }
    }
    
    func handleSRUploadDocuments(model: PlanDetailAndSRModel?,isReuploadCase: Bool = false){
        if let plan = model, let serviceReq = plan.serviceRequests.first {
            if isReuploadCase {
                routeToTimeline(srId: serviceReq.srId, claimType: serviceReq.claimType, crmTrackingNumber: serviceReq.crmTrackingNumber)
            } else {
                routeToUploadDocument(srId: serviceReq.srId, memId: serviceReq.memId, claimType: serviceReq.claimType, serviceDocuments: serviceReq.srDocuments, productName: serviceReq.assets.first ?? "", crmTrackingNumber: serviceReq.crmTrackingNumber, insurancePartnerCode: serviceReq.insurancePartnerCode?.description, productCode: serviceReq.assetsDetail?.first?.productCode, brand: serviceReq.assetsDetail?.first?.make ?? "", assetId: serviceReq.assetsDetail?.first?.assetReferenceNo, additionalDetail: serviceReq.additionalDetails)
                let subCat = Utilities.getProduct(productCode: serviceReq.assetsDetail?.first?.productCode ?? "")?.subCategoryName ?? ""
                
                EventTracking.shared.eventTracking(name: .SRUploadDoc, [.service: serviceReq.claimType.rawValue, .subcategory: subCat])
            }
        }
    }
    
    func downloadInvoice(model: PlanDetailAndSRModel?){
        if let plan = model, let serviceReq = plan.serviceRequests.first {
            let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(serviceReq.srId)/documents?reportType=JOBSHEET"
            if let url = URL(string: completeUrl) {
                let fileName = "INVOICE_SR_\(serviceReq.srId).pdf"
                FileDownloader.fileOpeninWebView(url: url, fileName: fileName, headerTitle: Strings.Global.invoice, message: Strings.Global.invoiceDownloaded)
            }
        }
    }
    
    func handleHAClaimProcess(model: PlanDetailAndSRModel?, avaialbilityRes: CheckAvailabilityResponseDTO?){
        if let plan = model {
            var assetsAvailability = [String: AssetEligibility]()
            if let assets = avaialbilityRes?.data, assets.count > 0 {
                for asset in assets {
                    if let assetId = asset.assetId {
                        assetsAvailability[assetId] = AssetEligibility(eligible_: asset.eligibleForRaiseClaim == "Y", reason_: asset.message ?? "")
                    }
                }
            }
            routeToGetStarted(for: plan, with: assetsAvailability, draftSRId: plan.draftMembershipID())
        }
    }
    
    func handleMembershipTabActions(action: String,data: NSDictionary) {
        if let action = MembershipTabActions(rawValue: action) {
            
            let parsedData = parseResponseToViewModel(jsonData: data)
            let deeplinkParams = parsedData.deeplinkQueryParams
            
            if let plan = parsedData.planDetailAndSRModel,let serviceReq = plan.serviceRequests.first {
                let subCat = Utilities.getProduct(productCode: serviceReq.prodCode ?? "")?.subCategoryName
                CacheManager.shared.eventProductName = subCat ?? ""
                CacheManager.shared.srType = serviceReq.claimType
            }
            
            switch action {
            case .viewDetail:
                routeToMembershipDetail(parsedData.planDetailAndSRModel, buybackStatusList: parsedData.buybackStatusList)
            case .myCards :
                routeToMyCards(parsedData.planDetailAndSRModel)
            case .startHAClaimProcess:
                if let isResumeClaim = data[BridgeJSONDataKeys.isResumeClaim] as? Bool, isResumeClaim {
                    parsedData.planDetailAndSRModel?.membershipResponse?.draftMemberships = parsedData.intimationData
                }
                handleHAClaimProcess(model: parsedData.planDetailAndSRModel, avaialbilityRes: parsedData.pendingSubmissionResponse)
            case .rateUs:
                handleRatingPopUp(model: parsedData.planDetailAndSRModel)
            case .rescheduleVisit:
                handleSRRescheduleVisit(plan: parsedData.planDetailAndSRModel)
            case .scheduleInspection,.selectAddressInspection,.viewInspectionDetails:
                if let model = parsedData.planDetailAndSRModel {
                    handleInspectionProcess(model: model)
                }
            case .fdOpenSendSMSScreen:
                routeToSMSScreen(model: parsedData.planDetailAndSRModel)
            case .peMembershipActivation, .mobileActivation:
                handlePEActivationFlow(model: parsedData.planDetailAndSRModel)
            case .handlePendingSubmissionResponse:
                handlePendingSubmissionResponse(model: parsedData.planDetailAndSRModel, pendingSubmissionResponse: parsedData.pendingSubmissionResponse)
            case .srViewTimeLine:
                if let plan = parsedData.planDetailAndSRModel, let serviceReq = plan.serviceRequests.first {
                    routeToTimeline(memId: serviceReq.memId,srId: serviceReq.srId, claimType: serviceReq.claimType, crmTrackingNumber: serviceReq.crmTrackingNumber)
                }
            case .srUploadPendencies,.srUploadDocument:
                handleSRUploadDocuments(model: parsedData.planDetailAndSRModel)
            case .downloadInvoice:
                downloadInvoice(model: parsedData.planDetailAndSRModel)
            case .deeplinkWHCSelectAddress:
                if let activationCode = deeplinkParams[BridgeJSONDataKeys.activationCode] as? String, let tempCustomer = parsedData.tempCustomerResponse {
                    routeToInspection(purchaseDate: Date.currentDate(), activationCode: activationCode, response: tempCustomer)
                }
                break
            case .deeplinkServiceDetail:
                if let srID = deeplinkParams[BridgeJSONDataKeys.srID] as? String, let srNumber = deeplinkParams[BridgeJSONDataKeys.srNumber] as? String,let srType = deeplinkParams[BridgeJSONDataKeys.srType] as? String, let claimType = Constants.Services(rawValue: srType) {
                    routeToTimeline(srId: srID, claimType: claimType, crmTrackingNumber: srNumber)
                }
            }
        }
    }
    
    func getMembershipRefreshedResponse(response: CustomerMembershipDetailsResponseDTO?) {
        let keys = UserDefaults.standard.dictionaryRepresentation().keys.filter({$0.hasPrefix("\(UIDevice.uuid)" + "_")})
        let smsData = keys.compactMap({SaveSecureData.secureValue(forKey: $0) as? SaveFDDataMode}).filter { $0.smsNumber != nil}
        let smsFDData = smsData.map({$0.orderId})
        if let data = try? JSONSerialization.data(withJSONObject: Serialization.getDictionaryFromObject(object: response), options: []), let jsonString = String(data: data, encoding: .utf8) {
            ChatBridge.sendNotification(withName: "getMembershipDataFromHome", body: ["membershipResponse": jsonString,"fd_flow_sms_status_data": smsFDData])
        }
    }
}

extension TabVC: UploadMultipleImageVCDelegate {
    func clickedBtnSubmit() {
        //refreshTableView()
        self.popViewController()
        refreshMemberships()
    }
    
    func clickedBtnBack(refreshNeeded: Bool) {
        self.popViewController()
        if refreshNeeded {
            // refreshTableView()
            refreshMemberships()
        }
    }
}

extension TabVC: PermissionScreenVCDelegate {
    func routeToIMEIVerifyScreen() {
        routeToVerifyIMEIScreen()
    }
}

// MARK: - Router
extension TabVC {
    
    // MARK: FD Routing
    func routeToFraudDetectionVC(for activationCode: String?, fromScreen: FromScreen = .blank) {
        let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
        membershipActivationFDVC.activationCode = activationCode
        membershipActivationFDVC.fromScreen = fromScreen
        if let navVC = navigationController {
            navVC.pushViewController(membershipActivationFDVC, animated: true)
        }
    }
    
    func routeToDownloadingStatusVC(for activationCode: String?, with activityRefId: String?) {
        let vc = DownloadingStatusVC()
        TFLiteModelsDownloadManager.sharedInstance.activationCode = activationCode
        TFLiteModelsDownloadManager.sharedInstance.activityRefId = activityRefId
        TFLiteModelsDownloadManager.sharedInstance.delegate = vc
        downloadManager.startModelDownloading()
        presentOverFullScreen(vc, animated: true, completion: nil)
    }
    
    func showStartActivationBottomSheet(for activationCode: String?, with coverAmount: NSNumber?, with activityRefId: String?, brand: String?) {
        let activationBottomSheet = StartActivationBottomSheet()
        activationBottomSheet.delegate = self
        activationBottomSheet.activationCode = activationCode
        activationBottomSheet.activityRefId = activityRefId
        activationBottomSheet.setViewModel(coverAmount: coverAmount?.stringValue ?? "", brand: brand ?? "")
        let basePickerVC = BasePickerVC(withView:activationBottomSheet, height:activationBottomSheet.height())
       Utilities.topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    func startFDFlow(for activationCode: String?,with coverAmount: NSNumber?, with activityRefId: String?, brand: String?, testType: String, fromScreen: FromScreen = .blank) {
        FDCacheManager.shared.fdTestType = testType == "MT" ? .MT : .SDT
        if testType == FDTestType.MT.getAPIValue() { // if MT flow is there
            rootTabVC?.hideDownloadingStatusStrip()
            if downloadManager.isModelsDownloaded() { // models already downloaded
                if downloadManager.checkIfModelCouldLoad() {  // check if models are eligible to load with TFLite
                    self.routeToFraudDetectionVC(for: activationCode, fromScreen: fromScreen)
                } else { // else change flow to SDT
                    Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                    self.interactor?.changeFDFlowType(testType: .SDT, activationCode: activationCode, activityReferenceId: activityRefId, reasonToChange: downloadManager.mirrorTestErrorMessages?.modelLoadingRetryExceeded, from: fromScreen)
                }
            } else if let activationCode = activationCode { // need to download models for mirror test
                if downloadManager.checkIfRetryCountExceededFor() { // retry count exceeded change flow
                    Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                    self.interactor?.changeFDFlowType(testType: .SDT, activationCode: activationCode, activityReferenceId: activityRefId, reasonToChange: downloadManager.mirrorTestErrorMessages?.downloadingRetryExceeded, from: fromScreen)
                } else { // start downloading
                    TFLiteModelsDownloadManager.sharedInstance.fromScreen = fromScreen
                    if fromScreen == .PEPaymentSuccessScreen { // show directly downloading status screen
                        self.routeToDownloadingStatusVC(for: activationCode, with: activityRefId)
                    } else { // bottomsheet to start activation
                        self.showStartActivationBottomSheet(for: activationCode, with: coverAmount, with: activityRefId, brand: brand)
                    }
                }
            }
        } else {
            self.routeToFraudDetectionVC(for: activationCode, fromScreen: fromScreen)
        }
    }
    
    func routeToMembershipActivation(activationCode: String?, activityRefId: String?, onDownloadCompletion: (()->())? = nil) {
        if FDCacheManager.shared.fdTestType == .MT {
            rootTabVC?.hideDownloadingStatusStrip() // hide if visible
            if downloadManager.isModelsDownloaded() { // models are already downloaded
                if downloadManager.checkIfModelCouldLoad() { // check if models are eligible to load with TFLite
                    if let _ = onDownloadCompletion {
                        onDownloadCompletion?()
                        return
                    }
                    let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
                    self.routeTo(viewController: membershipActivationFDVC)
                } else { // else change flow to SDT
                    Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                    self.interactor?.changeFDFlowType(testType: .SDT, activationCode: activationCode, activityReferenceId: activityRefId, reasonToChange: downloadManager.mirrorTestErrorMessages?.modelLoadingRetryExceeded, from: .blank)
                }
            } else if let activationCode = activationCode {
                if downloadManager.checkIfRetryCountExceededFor() { // if retry exceeded change flow to SDT
                    Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
                    self.interactor?.changeFDFlowType(testType: .SDT, activationCode: activationCode, activityReferenceId: activityRefId, reasonToChange: downloadManager.mirrorTestErrorMessages?.downloadingRetryExceeded, from: .blank)
                    return
                } else { // start downloading models flow
                    if !downloadManager.oaDownloadManager.isDownloading() { // if models already not downloading
                        downloadManager.startModelDownloading()
                    }
                    downloadManager.fromScreen = .fromMembership
                    let vc = DownloadingStatusVC()
                    vc.onDownloadCompletion = onDownloadCompletion
                    downloadManager.activationCode = activationCode
                    downloadManager.activityRefId = activityRefId
                    downloadManager.delegate = vc
                    presentOverFullScreen(vc, animated: true, completion: nil)
                }
            }
        } else { // for other than MT test type
            let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
            self.routeTo(viewController: membershipActivationFDVC)
        }
    }
    
    func routeToSetBasicDetailScreen() {
        routeTo(viewController: MembershipDetailsFraudDetectionVC())
    }
    
    func routeToVerifyIMEIScreen() {
        routeTo(viewController: IMEIVerificationVC())
    }
    
    func routeToSendSMSScreen(imeinumber: String?, imeinumber2: String? = nil, mobileNo: String? = nil) {
        let vc = SendSMSScreenVC()
        vc.imeiNumber = imeinumber
        vc.imeiNumber2 = imeinumber2
        vc.mobileNo = mobileNo
        routeTo(viewController: vc)
    }
    
    func routeToMirrorTestVideoGuide() {
        routeTo(viewController: VideoGuideViewController())
    }
    
    func routeToSendSMSSuccessScreen(mobileNumber: String?) {
        let vc = SMSSuccessSceenVC()
        vc.mobileNumber = mobileNumber
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToSuccessScreen() {
        let vc = ReuploadSuccessScreen()
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToIMEIScreen() {
        // TODO: KAG, this needs testing and through review!
        let vc = IMEIScreenVC()
        let navVC = BaseNavigationController(rootViewController: vc)
        self.presentOverFullScreen(navVC, animated: true, completion: nil)
    }
    
    func routeToMirrorTestRatingScreen() {
        let vc = ServiceRatingVC()
        vc.flowType = .MirrorFlow
        routeTo(viewController: vc)
    }
    
    func routeToRefundSuccessScreen(_ orderId: String, purchaseAmount: String) {
        var model = ServiceRatingModel()
        model.headerData = RatingHeaderViewModel(imageRightText: Strings.RefundText.refundHeaderText, headingText: "", descriptionText: Strings.RefundText.refundHeaderDetailtext, image: #imageLiteral(resourceName: "result_success"), backgroundColor: .seaGreen)
        var section = ServiceRatingSectionModel()
        section.cells = [RefundDetailModel(purchaseAmount: purchaseAmount, orderId: orderId)]
        model.sections = [section]
        
        let vc = ServiceRatingVC()
        vc.flowType = .RefundFlow
        vc.serviceRatingModel = model
        routeTo(viewController: vc)
    }
    
    // MARK: Home MHC
    func routeToCompleteMHC() {
        routeTo(viewController: MHCCategoryVC(nibName: "MHCCategoryVC", bundle: nil))
    }
    
    func routeToMHCTest(mhcCategory: NSDictionary) {
        var categorizedtests = [[MhcTestName]]()
        
        var selectionCells = MhcTestCategory.values.map({MHCCategorySelectionCellModel(withCategory: $0)})
        MHCResult.shared.clearData()
        selectionCells = selectionCells.map{
            var cellModel = $0
            if let categoryName = mhcCategory["name"] as? String {
                if categoryName == cellModel.category.categoryName() {
                    cellModel.isSelected = true
                }else {
                    cellModel.isSelected = false
                }
            }else {
                cellModel.isSelected = false
            }
            return cellModel
        }
        
        for category in selectionCells.compactMap({!$0.isSelected ? $0.category : nil}) {
            for test in category.tests() {
                test.setTestStatus(.result_not_available)
            }
        }
        
        categorizedtests = selectionCells.compactMap({$0.isSelected ? $0.category : nil}).map({$0.tests()})
        navigator = MHCMainNavigator(categorizedtests: categorizedtests, isFromBuyback:false)
        navigator?.delegate = self
        navigator?.presentMainContainerVC(viewController: self)
        MHCConstants.mhcCompletionStatus = .pending
        MHCResult.save()
    }
    
    // MARK: DEEPLINK ROUTING
    func routeToActivateVoucher() {
        EventTracking.shared.eventTracking(name: .activateVouchers, [.location: "Account Tab screen"])
        navigationController?.pushViewController(ActivateVoucherVC(), animated: true)
    }
    
    func startBuyPlanJourneyForDeeplink(_ deeplinkURL: String) {
        let vc = BuyPlanJourneyVC()
        vc.addToInitialProperties(["deeplinkURL": deeplinkURL])
        present(vc, animated: true, completion: nil)
    }
    
    func routeToIdFencePlans() {
        let vc = BuyPlanJourneyVC()
        vc.addToInitialProperties(["routeName":"ServiceDetailScreen","data":["buyPlanJourney":["category": Destination.idfance.category,"service": Constants.PlanServices.idFence,"screenTitle":"IDFence","serviceName": "Dark Web Monitering"]]])
        present(vc, animated: true, completion: nil)
    }
    
    // MARK: TAKEN FROM HOME
    func goToSpotLightActions(url: String?) {
        if let urlString = url {
            if urlString.contains("home/completepurchase") {
                var serviceType = "wallet"
                let url = customerMembershipResponse?.data?.leadData?.paymentLink
                if let serviceName = customerMembershipResponse?.data?.leadData?.plan?.services?.first?.serviceName{
                    switch serviceName {
                    case Constants.PlanServices.wallet: serviceType = "wallet"
                    case Constants.PlanServices.idFence: serviceType = "IDFence"
                    case Constants.PlanServices.adld: serviceType = "PE"
                    case Constants.PlanServices.whcInspection,Constants.PlanServices.extendedWarranty :serviceType = "HS"
                    default:
                        break
                    }
                }
                let paymentVC = OAPaymentVC()
                paymentVC.addToInitialProperties(["data": url ?? "", "service_Type": serviceType, "hasEW": false, "isRenewal": true])
                self.present(paymentVC, animated: true, completion: nil)
            }else if urlString.contains("home/walletenhanment"){
                self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.paycreditcardbill.message, subTitle:nil, forScreen: "Pay Bill") {(status) in
                    if status {
                        let vc = CardManagementVC()
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            } else{
                DeepLinkManager.handleLink(URL(string: urlString))
            }
        }
    }
}

extension TabVC: MHCTestDelegate {
    func testFinished() {
        debugPrint("testFinished")
        navigator = nil
        EventTracking.shared.eventTracking(name: .MHCComplete, [.location:  Event.EventParams.mhcHome, .result: MHCConstants.mhcScoreValue], withAppFlyer: true)
        let vc = MHCResultMainVC()
        vc.delegate = self
        presentInFullScreen(vc, animated: false)
    }
}

extension TabVC: MHCTestResultDelegate {
    
    func result() {
        let topMostView = Utilities.topMostPresenterViewController
        if let navVC = topMostView as? BaseNavigationController {
            EventTracking.shared.eventTracking(name: .MHCSummary, [.location: Event.EventParams.mhcHome])
            let mhcVC : MHCResultVC = MHCResultVC(nibName: "MHCResultVC", bundle: nil)
            navVC.pushViewController(mhcVC, animated: true)
        }
    }
    
    func completePendingTest() {
        
    }
    
    func retryTests() {
        
    }
}

extension TabVC: MembershipRefreshDelegate {
    func refreshMemberships() {
        ChatBridge.refreshMembershipOnReactNative()
    }
}

extension TabVC: ScheduleVisitDelegate {
    
    func clickedScheduleVisit(date: Date, slot: ScheduleVisitTimeSlotViewModel, serviceReqId: String?, crmTrackingNumber: String?) {
        
        let topMostController = Utilities.topMostPresenterViewController as? BaseNavigationController
        guard let scheduleVisitVC = topMostController?.viewControllers.last as? ScheduleVisitVC else {
            return
        }
        Utilities.addLoader(onView: scheduleVisitVC.view, message: "Please wait", count: &scheduleVisitVC.loaderCount, isInteractionEnabled: false)
        
        let requestDto = ClaimUpdateScheduleVisitRequestDTO()
        requestDto.scheduleSlotEndDateTime = date.string(with: .slotDateFormat) + " " + slot.endTime! + ":00"
        requestDto.scheduleSlotStartDateTime = date.string(with: .slotDateFormat) + " " + slot.startTime! + ":00"
        requestDto.serviceID = serviceReqId ?? ""
        ClaimUpdateScheduleVisitRequestUseCase.service(requestDTO: requestDto) { (request, response, error) in
            Utilities.removeLoader(fromView: scheduleVisitVC.view, count: &scheduleVisitVC.loaderCount)
            do {
                try Utilities.checkError(response, error: error)
                let vc = ClaimSuccessVC()
                let model = ClaimSuccessViewModel()
                model.scheduleSlotStartDateTime = requestDto.scheduleSlotStartDateTime
                model.scheduleSlotEndDateTime = requestDto.scheduleSlotEndDateTime
                model.successType = .visitScheduled
                model.srId = serviceReqId ?? ""
                model.crmTrackingNumber = crmTrackingNumber
                vc.successModel = model
                vc.delegate = self
                self.presentInFullScreen(vc, animated: true, completion: {
                    topMostController?.viewControllers.last?.navigationController?.popViewController(animated: false)
                })
            } catch {
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
}



// MARK:- Configuration Logic
extension TabVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = TabInteractor()
        let presenter = TabPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension TabVC { // routing logic
    
    private func getTempCustomerModel(from cellModel: PlanDetailAndSRModel) -> TemporaryCustomerInfoModel {
        let tempCustomerInfoModel = TemporaryCustomerInfoModel()
        tempCustomerInfoModel.setupPlanDetailAndSRModel(planDetailAndSRModel: cellModel)
        return tempCustomerInfoModel
    }
    
    func routeToFinalIMEIScreen() {
        let vc = IMEIScreenVC()
        let navVC = BaseNavigationController(rootViewController: vc)
        self.presentOverFullScreen(navVC, animated: true, completion: nil)
    }
    
    func routeToAllowPermission() {
        let vc = PermissionScreenVC()
        vc.delegate = self
        self.presentInFullScreen(vc, animated: true, completion: nil)
    }
    
    func routeToInvoiceReuploadFDVC(_ tempCustInfo: TemporaryCustomerInfoModel?, _ docsToUploadInfo: GetDocsToUploadResponseDTO?) {
        FDCacheManager.clearCache()
        FDCacheManager.shared.tempCustInfoModel = tempCustInfo
        FDCacheManager.shared.getDocsToUploadModel = docsToUploadInfo
        let reuploadVC = InvoiceReuploadFDVC()
        routeTo(viewController: reuploadVC)
    }
    
    func routeToUploadDocument(srId: String, memId: String, claimType: Constants.Services, serviceDocuments: [ServiceDocument], productName: String, crmTrackingNumber: String?, insurancePartnerCode: String?, productCode: String?, brand: String, assetId: String?, additionalDetail: AdditionalDetails?) {
        var isGifterProof = false
        if let additionalData = additionalDetail {
            isGifterProof = additionalData.isGifterProfRequired()
        }
        
        let viewModel = UploadMultipleImageViewModel()
        viewModel.srId = srId
        viewModel.crmTrackingNumber = crmTrackingNumber
        viewModel.serviceDocuments = serviceDocuments
        
        switch claimType {
        case .extendedWarranty:
            viewModel.title = "Service Details"
            viewModel.headerText = "Please upload following documents within 7 days to continue with your service request"
            viewModel.primaryActionTitle = "Submit Document"
            
        case .accidentalDamage:
            viewModel.title = "Raise a Service Request"
            viewModel.headerText = ""
            viewModel.primaryActionTitle = "Submit Request"
            EventTracking.shared.addScreenTracking(with: .SR_Upload_Image)
            
            
        case .buglary:
            viewModel.title = "Raise a Service Request"
            viewModel.headerText = ""
            viewModel.primaryActionTitle = "Submit Request"
            
        case .peTheft, .peADLD, .peExtendedWarranty :
            
            viewModel.title = "Raise Service Request"
            viewModel.headerText = "Please upload following documents within 7 days to continue with your service request"
            viewModel.primaryActionTitle = "Submit Request"
            
            let uploadVC = UploadDocumentsVC()
            uploadVC.serviceRequestType = claimType ?? .extendedWarranty
            uploadVC.viewModel = viewModel
            uploadVC.productCode = productCode
            uploadVC.brand = brand
            uploadVC.assetId = assetId
            uploadVC.insurancePartnerCode = insurancePartnerCode
            uploadVC.memId = memId
            //            uploadVC.isMandatory = "Y"
            uploadVC.delegate = self
            routeTo(viewController: uploadVC)
            return
            
        case .fire:
            EventTracking.shared.addScreenTracking(with: .SR_Upload_FIR)
            
            
        default:
            break
        }
        
        let uploadVC = UploadMultipleImageVC()
        uploadVC.serviceRequestType = claimType
        uploadVC.viewModel = viewModel
        uploadVC.category = claimType
        uploadVC.productCode = productCode
        uploadVC.insurancePartnerCode = insurancePartnerCode
        uploadVC.isGifterPrrofRequired = isGifterProof
        uploadVC.delegate = self
        routeTo(viewController: uploadVC)
    }
    
    /* SR related routing logic */
    func routeToTimeline(memId: String? = nil, srId: String, claimType: Constants.Services, crmTrackingNumber: String?,responseCache:SearchServiceRequestPlanResponseDTO?=nil) {
        
        EventTracking.shared.eventTracking(name: .SRDetail ,[.srNumber: srId])
        var timeline: TimelineBaseVC?
        timeline = TimelineBaseVC()
        
        if let timeline = timeline {
            timeline.cacheResponseDTO = responseCache
            timeline.srId = srId
            
            switch claimType {
            case .accidentalDamage, .breakdown, .fire, .buglary :
                if let memId = memId {
                    timeline.membershipId = memId
                }
            default:
                break
            }
            
            timeline.crmTrackingNumber = crmTrackingNumber
            timeline.eventSubCat = CacheManager.shared.eventProductName ?? ""
            routeTo(viewController: timeline)
        }
    }
    
    private func routeToSMSScreen(model: PlanDetailAndSRModel?){
        if let plan = model {
            EventTracking.shared.eventTracking(name: .resendSMS ,[.location: "Membership Tab", .mode: FDCacheManager.shared.fdTestType.rawValue])
            // Here you need to redirect user to Send SMS screen. Here control will come only in case of FD
            let orderId = plan.tempCustomerDetail?.first?.orderId?.stringValue
            FDCacheManager.clearCache()
            FDCacheManager.shared.setupGlanceValue(plan)
            FDCacheManager.shared.tempCustInfoModel = getTempCustomerModel(from: plan)
            routeToSendSMSScreen(imeinumber: nil, mobileNo: SaveFDDataMode.getSMSNumber(orderId))
        }
    }
    
    private func routeToInspectionDetails(plan: PlanDetailAndSRModel) {
        let inspectionDetail = InspectionDestailsVC()
        inspectionDetail.delegate = self
        let task = Utilities.getWhcTask(tasks: plan.task)
        inspectionDetail.serviceRequestId = Int(task?.srNo ?? "") ?? 0
        EventTracking.shared.eventTracking(name: .ViewInspectionDetails ,[.location:"WHC Inspection"])
        routeTo(viewController: inspectionDetail)
    }
    
    func routeToScheduleVisitForReschedule(plan: PlanDetailAndSRModel) {
        EventTracking.shared.eventTracking(name: .rescheduleInspectionTapped,[.category:"HomeTab"])
        let visitVC = ScheduleVisitVC()
        let task = Utilities.getWhcTask(tasks: plan.task)
        visitVC.serviceReqId = task?.srNo ?? ""
        visitVC.getAPiCalledFirstTime = true
        visitVC.serviceRequestSourceType = Strings.SODConstants.nonSOD
        visitVC.buCode = Constants.SchemeVariables.partnerBuCode
        visitVC.bpCode = Constants.SchemeVariables.partnerCode
        if let partnercode = plan.partnerCode {
            visitVC.bpCode = partnercode.stringValue
        }
        if let partnerbucode = plan.partnerBUCode {
            visitVC.buCode = partnerbucode.stringValue
        }
        routeTo(viewController: visitVC)
    }
    
    func routeToScheduleInspection(srId: String, claimType: Constants.Services, crmTrackingNumber: String? , pincode : String? , serviceRequestType:String?, memId: String?, assestId: String?, productCode: String?) {
        let visitVC = ScheduleVisitVC()
        visitVC.getAPiCalledFirstTime = true
        visitVC.serviceRequestSourceType = Strings.SODConstants.nonSOD
        visitVC.memId = memId
        visitVC.assetId = assestId
        visitVC.productCode = productCode
        visitVC.crmTrackingNumber = crmTrackingNumber
        visitVC.serviceReqId = srId
        visitVC.scheduleVisitDelegate = self
        visitVC.addressDetail = AddressDetail(pincode: pincode)
        visitVC.serviceType = serviceRequestType
        self.routeTo(viewController: visitVC)
    }
    
    private func showRatingView(for memID: String?, and srId: String) {
        rView = OARatingView()
        if let rView = rView {
            rView.ratingViewController = self
            rView.membershipId = memID
            rView.srId = srId
            rView.initialSetup()
            if let navVC = Utilities.topMostPresenterViewController as? BaseNavigationController, let lastVC = navVC.viewControllers.last {
                lastVC.addOABottomView(rView, isForRating: true)
            }
        }
    }
    

    func removeRatingView(_ containerView: UIView) {
        if let rView = rView {
            if let storeRv = rView.storeRv {
                Utilities.topMostPresenterViewController.removeOABottomView(storeRv, containerView)
            }
            
            if let renewalView = rView.rv {
                Utilities.topMostPresenterViewController.removeOABottomView(renewalView, containerView)
            }
            
            Utilities.topMostPresenterViewController.removeOABottomView(rView, containerView)
        }
    }
    
    
    private func routeToMembershipDetail(_ plan: PlanDetailAndSRModel?, buybackStatusList: [BuyBackStatus]) {
        if let plan = plan, let customerMembershipResponse = customerMembershipResponse {
            let membershipDetailsVC = MembershipDetailsVC()
            membershipDetailsVC.plan = plan
            membershipDetailsVC.buybackStatusList = buybackStatusList
            membershipDetailsVC.customerMembershipResponse = customerMembershipResponse
            membershipDetailsVC.refreshDelegate = self
            membershipDetailsVC.selectedSegmentIndex = 0
            routeTo(viewController: membershipDetailsVC)
        }
    }
    
    private func routeToMyCards(_ plan: PlanDetailAndSRModel?){
        Utilities.cardOpenCount()
        EventTracking.shared.eventTracking(name: .myCards, [.location: Event.EventParams.membershipTab])
        present(CardManagementVC(), animated: true, completion: nil)
    }
    
    func routeToGetStarted(for model: PlanDetailAndSRModel, with assetsAvailability: [String : AssetEligibility], draftSRId: String?) {
        
        let vc = GetStartedVC()
        vc.memResponse = model.membershipResponse
        if model.products?.first?.category != Constants.Category.finance {
            ClaimHomeServe.shared.membershipId = model.membershipId
            ClaimHomeServe.shared.draftSRId = draftSRId
            ClaimHomeServe.shared.productCode = model.products?.first?.productCode  //for PE
            ClaimHomeServe.shared.assetIds = [model.products?.first?.assetId?.stringValue ?? ""] //for PE
            ClaimHomeServe.shared.assetAvailability = assetsAvailability
        }
        ClaimHomeServe.shared.setUpAdditionalValue(invDocMismatchL: model.getInvDocNameMismatch(), assetAtribute: model.getAssetAttributes())
        
        var productName : String?
        if let brand = model.products?.first?.brand,let name =  model.products?.first?.productName{//for PE
            productName = brand + " " + name
        }
        if productName == nil {//for PE
            if let text = model.products?.first?.brand ?? model.products?.first?.productName {
                productName = text
            }
        }
        
        var serialNo: String?
        
        if model.products?.first?.category == Constants.Category.personalElectronics {
            if model.products?.first?.productCode == Constants.VoucherConstants.mobileMP {
                serialNo = "IMEI \(model.products?.first?.serialNo ?? "")"
            } else {
                serialNo = "Sr. No. \(model.products?.first?.serialNo ?? "")"
            }
        }
        
        if let date = Double(model.planPurchaseDate ?? "") {
            ClaimHomeServe.shared.purchaseDate = NSNumber(value: date)
        }
        
        vc.updateInfo(plan: model.planName ?? "", product: productName, applianceCount: (model.products?.count ?? 0 > 1 ? model.products?.count.description : nil), serialNo: serialNo)
        
        routeTo(viewController: vc)
    }
    
    /* Refer HomeAppliancesTabVC, in order to decide which methods need to be defined here. */
    func routeToInspection(purchaseDate: Date, activationCode: String, response: TemporaryCustomerInfoResponseDTO) {
        
        let details = response.customerDetails?.first
        
        if let pincode = details?.pinCode {
            
            var isValidCityStateCode = false
            
            if let cityCode = CustomerDetailsCoreDataStore.currentCustomerDetails?.cityCode, cityCode.count > 0, let stateCode = CustomerDetailsCoreDataStore.currentCustomerDetails?.stateCode, stateCode.count > 0 {
                isValidCityStateCode = true
            }
            
            if details?.pinCode == CustomerDetailsCoreDataStore.currentCustomerDetails?.pincode && !UserDefaults.standard.bool(forKey: UserDefaultsKeys.tempCustomer.rawValue) && isValidCityStateCode {
                
                let selectAddress = WHCSelectAddressVC()
                let userDtls = CustomerDetailsCoreDataStore.currentCustomerDetails
                let addressDetail = AddressDetail(pincode: userDtls?.pincode ?? "")
                addressDetail.address = userDtls?.addressLine1 ?? ""
                addressDetail.state = userDtls?.stateName ?? ""
                addressDetail.city = userDtls?.cityName ?? ""
                addressDetail.cityCode = userDtls?.cityCode ?? ""
                addressDetail.stateCode = userDtls?.stateCode ?? ""
                
                selectAddress.modelArray = [addressDetail]
                selectAddress.activationCode = activationCode
                selectAddress.customerDetails = details
                selectAddress.pinCode = userDtls?.pincode ?? ""
                routeTo(viewController: selectAddress)
                return
            }
            
            let addressRegistrationVC = AddressRegistrationVC()
            addressRegistrationVC.activationCode = activationCode
            addressRegistrationVC.pincode = pincode
            addressRegistrationVC.customerDetails = details
            addressRegistrationVC.purchasedDate = purchaseDate
            addressRegistrationVC.isAdding = true
            routeTo(viewController: addressRegistrationVC)
        } else {
            showAlert(message: Strings.Global.somethingWrong)
        }
    }
    
    private func routeTo(viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func popViewController() {
        navigationController?.popViewController(animated: true)
    }
    
    func refreshAndRouteToMembershipTab() {
        changeTabIndex(itemTag: .membership)
        customerMembershipResponse = nil
        handleRefresh()
    }
}

extension TabVC: InspectionDestailsVCDelegate {
    func positiveConfirmationTapped() {
        self.popViewController()
        refreshMemberships()
    }
    
    func negativeConfirmationTapped() {
        self.popViewController()
        refreshMemberships()
    }
}

extension TabVC: ClaimSuccessVCDelegate {
    
    func clickCrossButton() {
        refreshMemberships()
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
    
    func clickViewServiceRequest() {
        refreshMemberships()
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
}

// MARK:- Display Logic Conformance
extension TabVC: TabDisplayLogic {
    
    func showFDTestTypeChangeSuccess(for activationCode: String?, from location: FromScreen) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        hideDownloadingStatusStrip() // hide if visible on tabbar
        NotificationCenter.default.post(name: Notification.Name.membershipTab, object: nil)
        FDCacheManager.shared.changeTestType(to: .SDT)
        let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
        membershipActivationFDVC.activationCode = activationCode
        membershipActivationFDVC.fromScreen = fromScreen
        self.routeTo(viewController: membershipActivationFDVC)
        self.downloadManager.resetEverything()
    }
    
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?, for activationCode: String?, from fromScren: FromScreen) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.showAlert(title: "", message: message, primaryButtonTitle: Strings.Global.retry, secondaryButtonTitle: Strings.Global.cancel, isCrossEnable: false, primaryAction: {
            Utilities.addLoader(onView: self.view, count: &self.loaderCount, isInteractionEnabled: false)
            self.interactor?.changeFDFlowType(testType: .SDT, activationCode: activationCode, activityReferenceId: TFLiteModelsDownloadManager.sharedInstance.activityRefId, reasonToChange: reasonToChange, from: fromScren)
        }) {
            self.downloadManager.resetEverything()
        }
    }
    
    func handleReuploadCase(orderId: String?) {
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            if let frontPanelImage = saveObject.mtFrontPanelName, (FDCacheManager.shared.imageCaptureType == .ReuploadBoth || FDCacheManager.shared.imageCaptureType == .ReuploadFrontPanel) {
                // panel preview for front panel
                self.openPanelPreview(with: DataDiskStogare.getImageFromDocumentDirectory(filename: frontPanelImage), for: .FrontPanel)
            } else if let backPanelImage = saveObject.mtBackPanelName, FDCacheManager.shared.imageCaptureType == .ReuploadBackPanel {
                // panel preview for back panel
                self.openPanelPreview(with: DataDiskStogare.getImageFromDocumentDirectory(filename: backPanelImage), for: .BackPanel)
            } else {
                // images are not captured yet
                self.routeToMirrorTestVideoGuide()
            }
        } else {
            if (Permission.checkGalleryPermission() == PHAuthorizationStatus.authorized) {
                // Access has been granted.
                self.routeToIMEIScreen()
            } else {
                self.routeToAllowPermission()
            }
        }
    }
    
    func setGetDocsToUploadResponse(with response: GetDocsToUploadResponseDTO?, and plan: PlanDetailAndSRModel) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        FDCacheManager.clearCache()
        let orderId = plan.tempCustomerDetail?.first?.orderId?.stringValue
        let activationCode = plan.activationCode
        let tempCustomerInfoModel = getTempCustomerModel(from: plan)
        let activityRefId = tempCustomerInfoModel.customerDetails?.first?.activityRefId
        FDCacheManager.shared.setupGlanceValue(plan)
        FDCacheManager.shared.tempCustInfoModel = tempCustomerInfoModel
        FDCacheManager.shared.getDocsToUploadModel = response
        let verificationDocs = response?.deviceDetails?.first?.verificationDocuments
        if isReuploadInvoiceAdditionalImage(with: response) {
            FDCacheManager.shared.deleteImagesIntoDirectory()
            // Here we need to handle reupload case of Invoice and Additional invoice
            routeToInvoiceReuploadFDVC(tempCustomerInfoModel, response)
        } else if isReuploadForFrontOrBackPanelOnly(with: verificationDocs) {
            if FDCacheManager.shared.fdTestType == .MT {
                if Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs) {
                    // check for model downloaded
                    routeToMembershipActivation(activationCode: activationCode, activityRefId: activityRefId) { [weak self] in
                        self?.handleReuploadCase(orderId: orderId)
                    }
                } else {
                    self.handleReuploadCase(orderId: orderId)
                }
            } else {
                if SaveFDDataMode.getSaveData(orderId) != nil || !Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: response?.deviceDetails?.first?.verificationDocuments) {
                    self.routeToSendSMSScreen(imeinumber: nil, mobileNo: SaveFDDataMode.getSMSNumber(orderId))
                } else {
                    if (Permission.checkGalleryPermission() == PHAuthorizationStatus.authorized) {
                        // Access has been granted.
                        self.routeToIMEIScreen()
                        
                    } else {
                        routeToAllowPermission()
                    }
                }
                return
            }
        } else {
            if FDCacheManager.shared.fdTestType == .MT {
                if validateMTSaveImage == true {
                    let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
                    if let saveObject = SaveFDDataMode.getSaveData(orderId) {
                        if let frontPanelImage = saveObject.mtFrontPanelName {
                            self.openPanelPreview(with: DataDiskStogare.getImageFromDocumentDirectory(filename: frontPanelImage), for: .FrontPanel)
                        }
                    }
                } else {
                    routeToMembershipActivation(activationCode: activationCode, activityRefId: activityRefId)
                }
            }
        }
        
        validateMTSaveImage = false
    }
    
    /* Refer HomeAppliancesTabVC, in order to decide which methods need to be defined here. */
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
        validateMTSaveImage = false
    }
    
    func displayTempCustomer(response: TemporaryCustomerInfoResponseDTO, purchaseDate: Date, activationCode: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        routeToInspection(purchaseDate: purchaseDate, activationCode: activationCode, response: response)
    }
    
    func displayTempCustomerError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayEligibilityForPendingSubmission(model : SRListCompletedViewModel , assetsAvailability: [String : AssetEligibility]){
        
        //TODO: Handle for only HomeServe , Need to handle for other type
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        //        if model.claimType is HomeServeType {
        let claimObject = ClaimHomeServe.shared
        claimObject.membershipId = model.memId
        claimObject.serviceRequestID = model.srId
        claimObject.productCode = model.assetsDetail?.first?.productCode
        let dict = assetsAvailability
        if let assest = model.assetsDetail{
            claimObject.assetIds = assest.map{return $0.assetReferenceNo ?? ""}
            if let assestID =  claimObject.assetIds {
                for key in assestID{
                    dict[key]?.eligible = true
                }
            }
        }
        if let issueId = model.workflowData?.visit?.issueReportedByCustomer?.first?.issueId , let intIssueId = Int(issueId) {
            claimObject.issueId = NSNumber(value: intIssueId)
        }
        claimObject.incidentDescription = model.requestDescription
        claimObject.pinCode = model.serviceAddress?.pincode
        claimObject.address = model.serviceAddress?.addressLine1
        claimObject.assetAvailability = dict
        claimObject.boolPedingSubmission = true
        claimObject.assetServiceType = model.claimType
        claimObject.placeOfIncident = model.workflowData?.documentUpload?.placeOfIncident
        if let startTime = model.scheduleSlotStartDateTime , let endTime = model.scheduleSlotEndDateTime {
            let arrStartTime = startTime.components(separatedBy: " ")
            let arrStartTimeColon = arrStartTime.last?.components(separatedBy: ":")
            let startTime_ = arrStartTimeColon?.joined(separator: ":")
            let arrEndTime = endTime.components(separatedBy: " ")
            let arrEndTimeColon = arrEndTime.last?.components(separatedBy: ":")
            let endTime_ = arrEndTimeColon?.joined(separator: ":")
            let model =  ScheduleVisitTimeSlotViewModel()
            model.startTime = startTime_
            model.endTime = endTime_
            claimObject.scheduledTimeslot = model
            claimObject.scheduledDate =  DateFormatter.scheduleFormat.date(from: startTime)
        }
        if let whenHappenDate = model.dateOfIncident {
            claimObject.dateWhenHappen = whenHappenDate
        }
        if let createdOn = model.createdOn{
            claimObject.createdOn = createdOn
        }
        //        }
        
        claimObject.damageType = model.documentUploadDetails?.deviceBreakdownDetail?.breakDownType ?? model.documentUploadDetails?.mobileDamageDetails?.damageType
        
        claimObject.damagePartName = model.documentUploadDetails?.deviceBreakdownDetail?.breakDownPart ?? model.documentUploadDetails?.mobileDamageDetails?.damagePart
        
        claimObject.damageConditions = {
            var conditions = [PartDamageViewModel]()
            
            if model.documentUploadDetails?.deviceBreakdownDetail != nil {
                
                let isTouchWorking = (model.documentUploadDetails?.deviceBreakdownDetail?.breakDownIsTouchWorks == "Y")
                let isSwitchedOn = (model.documentUploadDetails?.deviceBreakdownDetail?.breakDownIsSwitchedOn == "Y")
                
                let touchWorking = PartDamageViewModel(title: "", switchedOn: isTouchWorking)
                touchWorking.damageKey = "touchScreenStatus"
                let switchedON = PartDamageViewModel(title: "", switchedOn: isSwitchedOn)
                switchedON.damageKey = "damageIsSwitchedOn"
                
                conditions = [touchWorking, switchedON]
                
            } else if model.documentUploadDetails?.mobileDamageDetails != nil {
                
                let isTouchWorking = (model.documentUploadDetails?.mobileDamageDetails?.damageIsTouchWorks == "Y")
                let isSwitchedOn = (model.documentUploadDetails?.mobileDamageDetails?.damageIsSwitchedOn == "Y")
                
                let touchWorking = PartDamageViewModel(title: "", switchedOn: isTouchWorking)
                touchWorking.damageKey = "touchScreenStatus"
                let switchedON = PartDamageViewModel(title: "", switchedOn: isSwitchedOn)
                switchedON.damageKey = "damageIsSwitchedOn"
                
                conditions = [touchWorking, switchedON]
            }
            return conditions
        }()
        
        claimObject.blockReferenceNo = model.documentUploadDetails?.mobileLossDetails?.simBlockRefNo
        claimObject.blockReferenceDate = model.documentUploadDetails?.mobileLossDetails?.simBlockReqDate
        claimObject.insurancePartnerCode = model.insurancePartnerCode
        
        //TODO: Remove HardCoding
        let vc = GetStartedVC()
        vc.memResponse = CacheManager.shared.memResponse
        vc.raisedFromCrm  =  ClaimHomeServe.shared.boolPedingSubmission
        vc.crmServiceType = model.claimType//(Constants.AssetServices(rawValue: model.claimType.getServiceCode()))!
        vc.memId = ClaimHomeServe.shared.membershipId
        
        var serialNo: String?
        
        if model.claimType.getCategoryType() == Constants.Category.personalElectronics {
            if model.assetsDetail?.first?.productCode == Constants.VoucherConstants.mobileMP {
                serialNo = "IMEI \(model.assetsDetail?.first?.serialNo ?? "")"
            } else {
                serialNo = "SERIAL NO. \(model.assetsDetail?.first?.serialNo ?? "")"
            }
        }
        
        vc.updateInfo(plan: "" , product: model.assets.first ?? "" , applianceCount: (model.assetsDetail?.count ?? 0 > 1 ? model.assetsDetail?.count.description : nil), serialNo: serialNo)        
        routeTo(viewController: vc)
    }
}

extension TabVC: UserUpdateViewDelegate {
    func gotitButtonAction() {
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
}

// MARK:- New Methods
extension TabVC {
    func handledMembershipResponse(membershipResponse: CustomerMembershipDetailsResponseDTO?) {
        do {
            try Utilities.checkError(membershipResponse, error: nil)
            MembershipListUseCase.handleSuccessResponse(response: membershipResponse)
            if let membershipResponse = membershipResponse {
                self.customerMembershipResponse = membershipResponse
                getMembershipRefreshedResponse(response: membershipResponse)
            }
        } catch { }
        
        ChatBridge.updateMyAccount()
        ChatBridge.refreshHomeScreen()
    }
    
    func handledInitialMembershipResponse(membershipResponse: CustomerMembershipDetailsResponseDTO?) {
        if let membershipResponse = membershipResponse {
            customerMembershipResponse = membershipResponse
        }
        
        setInitialTabSelected()
        OfferSyncUseCase.shared.syncOffers()
        getFeedbacks()
        initializeTabBars()
    }
    
    //the function takes the tabBar.tag as an Int
    func changeTabIndex(itemTag: TabIndex) {
        self.selectedIndex = itemTag
        ChatBridge.changeTabIndex(itemTag.rawValue)
        tabBarIndexChanged(itemTag.rawValue)
    }
    
    func tabBarIndexChanged(_ index: Int) {
        guard let newIndex = TabIndex(rawValue: index) else { return }
        selectedIndex = newIndex
        switch selectedIndex {
        case .newHome:
            EventTracking.shared.addScreenTracking(with: .Home)
            ChatBridge.homeScreenAppeared()
            removeRatingView(self.view)
        case .buy:
            EventTracking.shared.addScreenTracking(with: .Buy)
            removeRatingView(self.view)
        case .membership:
            EventTracking.shared.addScreenTracking(with: .Membership)
            
            ClaimHomeServe.clean()
            CacheManager.shared.eventProductName = nil
            CacheManager.shared.srType = nil
            CacheManager.shared.purchaseFlowFrom = .none
            CacheManager.shared.purchaseFlowFor = .none
            
            EventTracking.shared.eventTracking(name: .memberships)
            
            CacheManager.shared.memResponse = nil
            Utilities.checkForRatingPopUp(viewController: self)
        case .account:
            EventTracking.shared.addScreenTracking(with: .accounts)
            removeRatingView(self.view)
        default: break
        }
        
        ChatBridge.selectedActionType(type: .TABSWITHCHED)
    }
}

extension TabVC: StartActivationBottomSheetDelegate {
    func didTappedStartActivation(activationCode: String?, activityRefId: String?) {
        Utilities.topMostPresenterViewController.dismiss(animated: false) { [unowned self] in
            routeToDownloadingStatusVC(for: activationCode, with: activityRefId)
        }
    }
}


// MARK: - TFLiteModelsDownloadManagerDelegate
extension TabVC: TFLiteModelsDownloadManagerDelegate {
    
    func showDownloadingStatusStrip() {
        downloadManager.delegate = self
        ChatBridge.sendNotification(withName: "DownloadingStripVisibility", body: ["isVisible": true, "activationCode": downloadManager.activationCode ?? "", "testType": FDCacheManager.shared.fdTestType.rawValue])
    }
    
    func hideDownloadingStatusStrip() {
        if downloadManager.delegate is TabVC {
            downloadManager.delegate = nil
        }
        ChatBridge.sendNotification(withName: "DownloadingStripVisibility", body: ["isVisible": false])
    }
    
    func onChangeRemainingTime(time: String) {
        ChatBridge.sendNotification(withName: "OnDownloadRemainingTimeChange", body: ["remainingTime": time, "progress" : downloadManager.oaDownloadManager.overallProgress()])
    }
    
    func showActivationErrorScreen(for error: Error) {
        let errorVC = MirrorTestErrorScreenVC()
        errorVC.errorScreenFor = .NetworkIssue
        errorVC.currentError = error
        errorVC.delegate = self
        self.presentOverFullScreen(errorVC, animated: true, completion: nil)
    }
    
    func handleOnError(_ error: TFLiteModelsDownloadError) {
        if error == .ModelsLoadingError || error == .RetryExceeded { // SDT Flow Directly
            let errorReason = error == .ModelsLoadingError ? downloadManager.mirrorTestErrorMessages?.modelLoadingRetryExceeded : downloadManager.mirrorTestErrorMessages?.downloadingRetryExceeded
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
            self.interactor?.changeFDFlowType(testType: .SDT, activationCode: downloadManager.activationCode, activityReferenceId: downloadManager.activityRefId, reasonToChange: errorReason, from: downloadManager.fromScreen)
        } else {
            showActivationErrorScreen(for: error)
        }
    }
    
    func onError(error: TFLiteModelsDownloadError) {
        ChatBridge.sendNotification(withName: "OnDownloadError", body: ["error": error.rawValue])
    }
    
    func hideNoInternetErrorView() {
        downloadManager.updateRemainingTime()
        if let _ = Utilities.topMostPresenterViewController as? MirrorTestErrorScreenVC {
            Utilities.topMostPresenterViewController.dismiss(animated: false, completion: nil)
        }
    }
    
    func handlePostDownloadAction(activationCode: String?) {
        if let _ = onDownloadCompletion { // basically usefull for reupload case and models not downloaded
            Utilities.dismissAllPresentedVC { [weak self] in
                self?.onDownloadCompletion?()
                self?.onDownloadCompletion = nil
            }
            return
        }
        if let activationCode = activationCode {
            let membershipActivationFDVC = MembershipActivationFraudDetectionVC()
            membershipActivationFDVC.activationCode = activationCode
            membershipActivationFDVC.fromScreen = downloadManager.fromScreen
            rootNavVC?.pushViewController(membershipActivationFDVC, animated: true)
        }
    }
    
    func startActivation(for activationCode: String?, with activityRefId: String?, fromScreen:FromScreen, openFromNotification: Bool = false) {
        if openFromNotification {
            hideDownloadingStatusStrip()
            self.handlePostDownloadAction(activationCode: activationCode)
            return
        }
        ChatBridge.sendNotification(withName: "OnDownloadRemainingTimeChange", body: ["remainingTime": nil, "isCompleted": true, "progress" : 1])
    }
}

// MARK: - MirrorTestErrorScreenDelegate
extension TabVC: MirrorTestErrorScreenDelegate {
    func onRetry(with error: Error?) {
        downloadManager.updateRemainingTime()
        downloadManager.handleRetry(with: error)
    }
}

