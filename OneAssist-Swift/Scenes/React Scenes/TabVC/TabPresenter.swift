//
//  TabPresenter.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/04/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import UIKit

protocol TabDisplayLogic: class {
    func displayError(_ error: String)
    func displayTempCustomer(response: TemporaryCustomerInfoResponseDTO, purchaseDate: Date, activationCode: String)
    func displayTempCustomerError(_ error: String)
    // For FD
    func setGetDocsToUploadResponse(with response: GetDocsToUploadResponseDTO?, and plan: PlanDetailAndSRModel)
    func showChangeFDTestTypeError(_ message: String, reasonToChange: String?, for activationCode: String?, from location: FromScreen)
    func showFDTestTypeChangeSuccess(for activationCode: String?, from location: FromScreen)
}

class TabPresenter: BasePresenter, TabPresentationLogic {
    
    weak var viewController: TabDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func getDocsToUploadResponseRecieved(with plan: PlanDetailAndSRModel, response: GetDocsToUploadResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            self.viewController?.setGetDocsToUploadResponse(with: response, and: plan)
        } catch {
            self.viewController?.displayError(error.localizedDescription)
        }
    }
    
    func tempCustomerResponseRecieved(response: TemporaryCustomerInfoResponseDTO?, error: Error?, purchaseDate: Date, activationCode: String) {
        do {
            try checkError(response, error: error)
            viewController?.displayTempCustomer(response: response!, purchaseDate: purchaseDate, activationCode: activationCode)
        } catch {
            viewController?.displayTempCustomerError(error.localizedDescription)
        }
    }
    
    func presentChangedFDType(_ response: BaseResponseDTO?, _ error: Error?, reasonToChange: String?, activationCode: String?, from location: FromScreen = .blank) {
        do {
            try checkError(response, error: error)
            
            // Success case
            EventTracking.shared.eventTracking(name: .moveToSDT, [.mode: FDCacheManager.shared.fdTestType.rawValue, .reason: reasonToChange ?? ""])
            viewController?.showFDTestTypeChangeSuccess(for: activationCode, from: location)
        } catch let error as LogicalError {
            viewController?.showChangeFDTestTypeError(error.errorDescription!, reasonToChange: reasonToChange, for: activationCode, from: location)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
}
