//
//  MyProfileVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 29/02/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class MyProfileVC: ReactMainNavigationHideVC {

    override func moduleName() -> String {
        return "MyProfileFlow"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventTracking.shared.addScreenTracking(with: .MyProfile)
    }
}
