//
//  MyServiceRequestsVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 11/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation


class MyServiceRequestsVC: ReactMainNavigationHideVC {
    override func moduleName() -> String {
        return "MyServiceRequests"
    }
}
