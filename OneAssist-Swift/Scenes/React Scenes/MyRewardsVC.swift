//
//  MyRewardsVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 28/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class MyRewardsVC: ReactMainNavigationHideVC {
    var membershipResponse: CustomerMembershipDetailsResponseDTO?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventTracking.shared.addScreenTracking(with: .MyRewards)
    }
    
    func isIDFenceAvailableInMem() -> CustomerMembershipDetails?{
        for membership in membershipResponse?.data?.memberships ?? [] {
            if membership.checkIsMembershipIDFence(){
                return membership
            }
        }
        return nil
    }
    
    override func moduleName() -> String {
        return "MyRewardsScreen"
    }
    
    func prepareInitialProps(isAnsweredAllQuestions:Bool, showScratchCard:Bool){
        var rewardState = UserDefaultsKeys.rewardState.get() ?? "LOCKED"
        let memberShip = membershipResponse?.getIDFenceMembership()
        let pendingMemberShip = membershipResponse?.getIDFencePendingMembership()
        
        if (memberShip != nil || pendingMemberShip != nil) {
            if rewardState != "EXPIRED" || rewardState != "USED" || rewardState != "CLAIMED"
            {
                UserDefaultsKeys.rewardState.set(value: RewardStates.CLAIMED.rawValue)
                rewardState = RewardStates.CLAIMED.rawValue
            }
            addToInitialProperties(["isIDFenceInMem": true,
                                    "idFenceMemberShipDate": (memberShip != nil) ? memberShip?.purchaseDate ?? "" : ((pendingMemberShip != nil) ? pendingMemberShip?.purchaseDate ?? "" : ""),
                                    "membershipEndDate":  (memberShip != nil) ? memberShip?.endDate ?? "0" : "0",
                                    "idFenceMemRes": Serialization.getDictionaryFromObject(object: (memberShip != nil) ? memberShip : ((pendingMemberShip != nil) ? pendingMemberShip : "" ))])
        }
        addToInitialProperties(["isAnsweredAllQuestions":isAnsweredAllQuestions,
                                "rewardState":rewardState,
                                "rewardScratchDate":UserDefaults.standard.string(forKey: "rewardScratchDate"),"isNonSubscriber": UserDefaults.standard.bool(forKey: UserDefaultsKeys.isNonSubscriber.rawValue),"showScratchCard":showScratchCard])
    }
}
