//
//  OAIDFenceVC.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 13/02/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class OAIDFenceVC:  ReactMainNavigationHideVC {
    override func moduleName() -> String {
        return "OAIDFenceFlow"
    }
}
