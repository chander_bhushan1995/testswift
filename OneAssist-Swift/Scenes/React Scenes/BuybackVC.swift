
//
//  BuybackVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 13/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BuybackVC: ReactMainNavigationHideVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
    }
    
    @objc func appDidBecomeActive() {
        Utilities.checkCopyIMEIValue()
    }
    
    override func moduleName() -> String {
        return "OneAssistBuyback"
    }
}
