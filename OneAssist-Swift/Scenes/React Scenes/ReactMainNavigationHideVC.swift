//
//  ReactMainNavigationHideVC.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 09/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//


import UIKit

class ReactMainNavigationHideVC: ReactMainVC, HideNavigationProtocol {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // This change needs to be verified.
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
