//
//  CardManagementVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 27/01/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class CardManagementVC: ReactMainNavigationHideVC {
    override func moduleName() -> String {
        return "CardManagement"
    }
}
