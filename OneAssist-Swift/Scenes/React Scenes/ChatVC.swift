//
//  ChatVC.swift
//  XMPP
//
//  Created by Ashish Mittal on 09/07/18.
//  Copyright © 2018 karan. All rights reserved.
//

import UIKit
import React

class ChatVC: ReactMainNavigationHideVC {
    override func moduleName() -> String {
        return "OneAssistChat"
    }
}
