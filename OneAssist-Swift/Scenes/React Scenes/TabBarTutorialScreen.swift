//
//  TabBarTutorialScreen.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 25/02/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class TabBarTutorialScreen: ReactMainNavigationHideVC {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        reactView.backgroundColor = .clear
    }
    override func moduleName() -> String {
        return "TabBarTutorialScreen"
    }
}
