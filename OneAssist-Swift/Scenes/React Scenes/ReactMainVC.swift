//
//  ReactMainVC.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 13/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import React
import CodePush

var defaultRNInitialProps: [String : Any] {
    return [
        "apiHeader": ["Accept":"application/json",
                      "Content-Type": "application/json",
                      "User-Agent": UAString(),
                      "App-Id": Constants.headers.value.appIdValue,
                      "token": AppUserDefaults.sessionToken,
                      Constants.headers.key.appVersion : UIApplication.appVersion],
        "api_gateway_base_url": Constants.SchemeVariables.baseDomain + "/",
        "api_base_url": Constants.SchemeVariables.baseDomainOld + "/",
        "cdn_base_url": Constants.SchemeVariables.baseCDNUrl,
        "lhc_base_url": Constants.SchemeVariables.baseChatURL,
        "mobile_number": userMobileNumber,
        "customer_id": UserCoreDataStore.currentUser?.cusId ?? "",
        "user_Name":UserCoreDataStore.currentUser?.userName ?? "",
        "user_Email": CustomerDetailsCoreDataStore.currentCustomerDetails?.email ?? "",
        "firstName": CustomerDetailsCoreDataStore.currentCustomerDetails?.firstName ?? "",
        "lastName": CustomerDetailsCoreDataStore.currentCustomerDetails?.lastName ?? "",
        "customer_Address": ["addressLine1": CustomerDetailsCoreDataStore.currentCustomerDetails?.addressLine1 ?? "",
                             "cityName": CustomerDetailsCoreDataStore.currentCustomerDetails?.cityName ?? "",
                             "stateName": CustomerDetailsCoreDataStore.currentCustomerDetails?.stateName ?? ""],
        "BP_NAME": Constants.SchemeVariables.bussPartnerName,
        "BU_NAME": Constants.SchemeVariables.busUnitName,
        "initiatingSystem": Constants.SchemeVariables.initiatingSystem,
        "partnerBUCode": Constants.SchemeVariables.partnerBuCode,
        "partnerCode": Constants.SchemeVariables.partnerCode,
        "verifyEmailDismissed": AppUserDefaults.isVerifyEmailDismissed,
        "RAM": "\(UInt64.ramSize)",
        "ROM": "\(UInt64.diskSize)",
        "modelName": UIDevice.currentDeviceModel,
        "brandName": Strings.Global.apple,
        "deviceIdentifier": UIDevice.uuid,
        "logChatEvents": RemoteConfigManager.shared.isLogChatEvent,
        "custType": UserDefaultsKeys.custType.get() ?? "",
        "isProd" : Constants.isProdRelease || EnvironmentSetting.currentEnviornment == .prod,
        "UNLIMITED_APPLIANCES_COUNT": RemoteConfigManager.shared.unlimitedApplianceCount,
        "CUSTOMER_CARE_NUM": RemoteConfigManager.shared.contactNumberCustomerCare,
        
    ]
}

class ReactMainVC: BaseVC {
    
    var reactView: RCTRootView!
    var initialProperties: [String : Any] = defaultRNInitialProps
    var isLocationSet: Bool = false
    var onReactViewAppear: (()->Void)?
    final var isReactViewLoaded: Bool = false
    private var reactNativeBridge: ReactNativeBridge?
    
    deinit {
        print("ReactMainVC deinit called")
        reactNativeBridge = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add React View
        reactNativeBridge = ReactNativeBridge()
        reactView = RCTRootView(bridge: (reactNativeBridge?.bridge)!, moduleName: moduleName(), initialProperties: initialProperties)
        view.addSubview(reactView)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onReactViewAppearNotification), name: NSNotification.Name(rawValue: "RCTContentDidAppearNotification"), object: nil)
        hideNavigationShadow()
    }
    
    func addToInitialProperties(_ data: [String: Any]) {
        initialProperties.merge(data) { (_, new) in new }
    }
    
    func updateProps(_ data: [String: Any] = [:]) {
        let updateDataToBeMerged = defaultRNInitialProps.merging(data) { (_, new) in new }
        if  let reactView = reactView {
            reactView.appProperties?.merge(updateDataToBeMerged) { (_, new) in new }
        }
    }
    
    func moduleName() -> String {
        return "OneAssist"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // Layout React View
        if reactView != nil {
            reactView.frame = view.bounds
        }
    }
    
    @objc func onReactViewAppearNotification(){
        if let onReactViewAppear = onReactViewAppear, !isReactViewLoaded {
            onReactViewAppear()
        }
        isReactViewLoaded = true
    }
    
}

extension UIViewController {
    @objc func present(_ viewController: ReactMainVC, animated: Bool, completion: (() -> Void)?) {
        // TODO: KAG, test out this code.
        rootNavVC?.pushViewController(viewController, animated: animated)
        completion?()
    }
}
