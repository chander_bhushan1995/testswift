//
//  GoogleMapView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 29/10/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapView: UIView {
    
    let marker = GMSMarker()
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @objc var location: NSDictionary? = nil {
        didSet {
            updateMap()
        }
    }
    @objc var zoomLevel: Float = 15
    @objc var onTappedMarker: RCTBubblingEventBlock?
    
    class func loadView() -> GoogleMapView{
        return Bundle.main.loadNibNamed("GoogleMapView", owner: nil, options: nil)?.first as! GoogleMapView
    }
    
    override func awakeFromNib() {
        marker.icon = #imageLiteral(resourceName: "pinCode")
        marker.map = mapView
        mapView.delegate = self
    }
    
    func updateMap(){
        if let location = self.location,let latitude = location.value(forKey: "latitude") as? Double,let longitude = location.value(forKey: "longitude") as? Double {
            marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            marker.icon = #imageLiteral(resourceName: "marker")
            mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), zoom: zoomLevel, bearing: 0, viewingAngle: 0)
            mapView.animate(toZoom: zoomLevel)
        }
    }
}

extension GoogleMapView: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let onTappedMarker = self.onTappedMarker {
            onTappedMarker([:])
        }
        return true
    }
}

@objc(RCTGoogleMapViewManager)
class RCTGoogleMapViewManager: RCTViewManager {
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
  
  override func view() -> UIView! {
    return GoogleMapView.loadView()
  }
}

