//
//  BookmarkedOffersPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/10/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol BookmarkedOffersDisplayLogic: class {
    func displayError(_ error: String)
    func displayOffers(_ offers: BookmarkedCardOffersResponseDTO)
}

class BookmarkedOffersPresenter: BasePresenter, BookmarkedOffersPresentationLogic {
    weak var viewController: BookmarkedOffersDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func bookmarkedOffersRecieved(_ offers: BookmarkedCardOffersResponseDTO?, error: Error?) {
        do {
            try checkError(offers, error: error)
            if offers != nil {
                viewController?.displayOffers(offers!)
            }
        } catch {
            viewController?.displayError(error.localizedDescription)
        }
    }
}
