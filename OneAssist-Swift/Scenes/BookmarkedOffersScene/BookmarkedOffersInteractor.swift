//
//  BookmarkedOffersInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/10/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol BookmarkedOffersPresentationLogic {
    func bookmarkedOffersRecieved(_ offers: BookmarkedCardOffersResponseDTO?, error: Error?)
}

class BookmarkedOffersInteractor: BaseInteractor, BookmarkedOffersBusinessLogic {
    var presenter: BookmarkedOffersPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getBookmarkedOffers() {
        let request = BookmarkedCardOffersRequestDTO()
        let cusId = Int(UserCoreDataStore.currentUser?.cusId ?? "0")
        request.customerId = NSNumber(integerLiteral: cusId ?? 0)
        request.offerType = ""
        request.pageNo = 1
        request.pageSize = 400
        request.bookmarked = true
        
        let _ = BookmarkedCardOffersRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.bookmarkedOffersRecieved(response, error: error)
        }

    }
}
