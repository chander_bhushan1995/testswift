//
//  BookmarkedOffersVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/10/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol BookmarkedOffersBusinessLogic {
    func getBookmarkedOffers()
}

class BookmarkedOffersVC: BaseVC {
    var interactor: BookmarkedOffersBusinessLogic?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var NoBookmarkLabel: BodyTextRegularBlackLabel!
    @IBOutlet weak var addCardButton: OAPrimaryButton!
    @IBOutlet weak var noBookmarkView: UIView! {
        didSet {
            noBookmarkView.isHidden = true
        }
    }
    var list: [BankCardOffers] = []
    var webengageEventTrackingDict:[Event.EventKeys:String] = [:]
    var category:String?
    var cards: [Card] = []
    var selectedIndex:Int  = 0;
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        if self.webengageEventTrackingDict.count > 0 {
            EventTracking.shared.eventTracking(name: .bookMarkedOffers, [.location: fromScreen.rawValue, .utmCampaign: self.webengageEventTrackingDict[.utmCampaign] ?? "",.utmMedium: self.webengageEventTrackingDict[.utmMedium] ?? "",.utmSource: self.webengageEventTrackingDict[.utmSource] ?? "",])
        } else {
            EventTracking.shared.eventTracking(name: .bookMarkedOffers, [.location: fromScreen.rawValue])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBookmarks()
    }
    
    private func getBookmarks() {
        list = []
        self.tableView.reloadData()
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
        interactor?.getBookmarkedOffers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        OfferSyncUseCase.shared.syncOffers()
    }
    
    // MARK:- private methods
    private func initializeView() {
        title = Strings.BookmarkedOffersScene.title
        tableView.register(nib: Constants.CellIdentifiers.offerListCell, forCellReuseIdentifier: Constants.CellIdentifiers.offerListCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
    }
}

// MARK:- Table Datasource Conformance
extension BookmarkedOffersVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.offerListCell, for: indexPath) as! OfferListCell
        cell.delegate = self
        cell.configureCell(model: list[indexPath.row], for: indexPath)
        return cell
    }
}

// MARK:- Table Delegate Conformance
extension BookmarkedOffersVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let offer = list[indexPath.row]
        routeToOfferDetail(offer: offer)
        selectedIndex = indexPath.row
    }
}

// MARK:- Offer List Cell Delegate Conformance
extension BookmarkedOffersVC: OfferListCellDelegate {
    func clickedBookmark(for indexPath: IndexPath?) {
        if !list.isEmpty, let indexPaths = indexPath {
            let offer = list[indexPaths.row]
            let bookmarkDataDict : [String: String] = [Constants.KeyValues.offerId : offer.offerId ?? "", Constants.KeyValues.offerCode : offer.offerCode ?? "", Constants.KeyValues.outletCode : offer.outletCode ?? "", Constants.KeyValues.status : "0"]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookmarkStatusChanged"), object: nil, userInfo: bookmarkDataDict)
            
            offer.isBookmarked = !offer.isBookmarked
            if offer.isBookmarked == true{
                EventTracking.shared.eventTracking(name: .markFavourite ,
                                                   [.merchantName: offer.merchantName ?? "",
                                                    .type: Event.EventParams.offline,
                                                    .category: category ?? "",
                                                    .expiryDate: (DateFormatter.serverDateFormat.date(from: offer.endDate ?? "") ?? ""),
                                                    .location: fromScreen.rawValue,
                                                    .position: NSNumber.init(value:indexPaths.row),
                                                    .offerID: offer.offerId ?? ""])
            }
            OfferSyncUseCase.shared.storeOfferInDb(offer: offer)
            list.remove(at: indexPaths.row)
            self.tableView.reloadData()
           checkListEmpty()
        }
    }
    
    func checkListEmpty() {
        if list.isEmpty {
//           if cards.isEmpty {
//               showAddCardView()
//           } else {
               showAllOffersView()
//           }
       } else {
           hideNoBookmarkView()
       }
    }
}

// MARK:- Offer Detail Delegate Conformance
extension BookmarkedOffersVC: OfferDetailBookmarkDelegate {
    func clickedDetailBookmark() {
    }
}

// MARK:- Display Logic Conformance
extension BookmarkedOffersVC: BookmarkedOffersDisplayLogic {
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        checkListEmpty()
    }
    
    func displayOffers(_ offers: BookmarkedCardOffersResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.list = offers.data ?? []
        checkListEmpty()
        self.tableView.reloadData()
    }
}

// MARK:- Configuration Logic
extension BookmarkedOffersVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = BookmarkedOffersInteractor()
        let presenter = BookmarkedOffersPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension BookmarkedOffersVC {
    func routeToOfferDetail(offer: BankCardOffers) {
        if offer.offerId != nil {
            let vc = OnlineOfferDetailVC()
            vc.delegate = self
            vc.selectedOffer = offer
            vc.fromScreen = .fromBookMark
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            let vc = OfflineOfferDetailVC()
            vc.delegate = self
            vc.selectedOffer = offer
            vc.fromScreen = .fromBookMark
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension BookmarkedOffersVC {
    fileprivate func hideNoBookmarkView() {
        noBookmarkView.isHidden = true
        self.view.sendSubviewToBack(noBookmarkView)
    }
    
    fileprivate func showNoWalletMembership() {
        noBookmarkView.isHidden = false
        addCardButton.isHidden = true
        NoBookmarkLabel.text = Strings.BookmarkOffer.noMembership
        self.view.bringSubviewToFront(noBookmarkView)
    }
    
    fileprivate func showAllOffersView() {
        noBookmarkView.isHidden = false
        NoBookmarkLabel.text = Strings.BookmarkOffer.noBookmark
        addCardButton.setTitle(Strings.WalletTabScene.viewAllOffers, for: .normal)
        addCardButton.addTarget(self, action: #selector(BookmarkedOffersVC.routeToAllOffers), for: .touchUpInside)
        self.view.bringSubviewToFront(noBookmarkView)
    }
    
    fileprivate func showAddCardView() {
        noBookmarkView.isHidden = false
        NoBookmarkLabel.text = Strings.BookmarkOffer.noCard
        addCardButton.setTitle(Strings.WalletTabScene.offersOnCardAction, for: .normal)
        addCardButton.addTarget(self, action: #selector(routeToAddCards), for: .touchUpInside)
        self.view.bringSubviewToFront(noBookmarkView)
    }

    @objc func routeToAddCards() {
        Utilities.cardOpenCount()
        EventTracking.shared.eventTracking(name: .myCards, [.location: Event.EventParams.bookMarkedOffers])
        let vc = CardManagementVC()
        vc.addToInitialProperties(["deeplink_url_string" : "addcard/DBC", "navigatingFrom": "Bookmarked Offers Screen"])
        present(vc, animated: true, completion: nil)
    }
    
    @objc func routeToAllOffers() {
        EventTracking.shared.eventTracking(name: .seeAllOffers, [.location: Event.EventParams.explore])
        let vc = AllOffersVC()
        vc.fromScreen = .fromBookMark
        vc.userCards = cards
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
