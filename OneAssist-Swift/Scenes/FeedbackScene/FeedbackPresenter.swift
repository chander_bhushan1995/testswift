//
//  FeedbackPresenter.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol FeedbackDisplayLogic: class {
    func fedbackSubmitResponse(response : SubmitFeedbackResponseDTO)
    func errorRecieved(_ error: String)
}

class FeedbackPresenter: BasePresenter, FeedbackPresentationLogic {
    weak var viewController: FeedbackDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    func responseRecieved(response: SubmitFeedbackResponseDTO?, error: Error?) {

        if let error = error {
            viewController?.errorRecieved(error.localizedDescription)
        } else if response?.status?.lowercased() == Constants.ResponseConstants.success.lowercased() {
            viewController?.fedbackSubmitResponse(response: response!)
        } else {
            viewController?.errorRecieved(Strings.Global.somethingWrong)
        }
    }
}
