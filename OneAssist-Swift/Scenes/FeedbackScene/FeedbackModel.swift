//
//  FeedbackModel.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
class FeedbackModel
{
    var feedbackCode = ""
    var feedbackValue = ""
    
    init(_ model:WHCFeedbackModel)
    {
        self.feedbackCode = model.valueId?.stringValue ?? ""
        self.feedbackValue = model.value!
    }
    
    init(){
        
    }
}
