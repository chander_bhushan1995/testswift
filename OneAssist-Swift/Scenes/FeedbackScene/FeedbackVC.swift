//
//  FeedbackVC.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit
protocol FeedbackSubmited:class {
  
}

protocol FeedbackBusinessLogic {
    func submitFeeback(serviceRequestId : String, feedbackRating : String?, feebdbackCode : String?, feedbackComment : String?)
}

class FeedbackVC: BaseVC {
    var interactor: FeedbackBusinessLogic?

    var request: FeedbackRequest?
    var serviceRequestId : String!
    var ratingSelected : String!
    var feedbacks = [FeedbackModel]()
    
    @IBOutlet var tableView: UITableView!
    var previousSelectedRow : IndexPath?
    var fieldOtherText = ""
    @IBOutlet var sendFeedbackButton: PrimaryButton!

    @IBOutlet weak var topHeight: NSLayoutConstraint!
    var feedbackSubmitedCompletion : (()->Void)?
    
    weak var delegate:FeedbackSubmited?
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        registerCells()
        sendFeedbackButton.isEnabled = false
        addTap()
        topHeight.constant = UIApplication.shared.statusBarFrame.height + 23
    }

    func registerCells(){
        tableView.register(nib:"FeedbackCell", forCellReuseIdentifier: "FeedbackCell")
    }
    
    func checkAndActivateButton() {
        if let row = previousSelectedRow?.row, row < feedbacks.count {
            if feedbacks[row].feedbackValue.lowercased() == "other" && fieldOtherText == "" {
                sendFeedbackButton.isEnabled = false
            } else {
                sendFeedbackButton.isEnabled = true
            }
        }
    }
    
    // MARK:- Action Methods
    
    @IBAction func clickButtonClosed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickButtonSend(_ sender: Any) {

        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        EventTracking.shared.eventTracking(name: .sendFeedBackTapped)

        if previousSelectedRow != nil {

            var feedbackCode = ""
            var feedbackComment = ""

            let feedback = feedbacks[previousSelectedRow!.row]
            feedbackCode = feedback.feedbackCode

            if feedback.feedbackValue.lowercased() == "other" {
                
                feedbackComment = fieldOtherText
            }
            interactor?.submitFeeback(serviceRequestId : serviceRequestId, feedbackRating: ratingSelected, feebdbackCode: feedbackCode, feedbackComment: feedbackComment)
        }
    }
}

extension FeedbackVC : TextFieldViewDelegate {
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = (textFieldView.textFieldObj.text ?? "") as NSString
        let strNew = str.replacingCharacters(in: range, with: string)
        fieldOtherText = strNew
        checkAndActivateButton()
        sendFeedbackButton.isEnabled = fieldOtherText.count > 0
        return true
    }
}

extension FeedbackVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedbacks.count
    }
}

extension FeedbackVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackCell", for: indexPath) as! FeedbackCell
        cell.label.text = feedbacks[indexPath.row].feedbackValue
        cell.selectionStyle = .none
        cell.isSelected = false
        cell.radioButton.selectedState = false
        cell.textView.delegate = self
        cell.textFieldHeightConstraint.constant = 0.0
        cell.textView.descriptionText = nil
        cell.textView.isHidden = true
        cell.textView.placeholderFieldText = nil
        cell.textView.fieldText = nil
        
        if indexPath.row == previousSelectedRow?.row {
            cell.radioButton.selectedState = true
            if feedbacks[indexPath.row].feedbackValue.lowercased() == "other" {
                
                cell.textFieldHeightConstraint.constant = 100.0
                cell.textView.descriptionText = "LEAVE A COMMENT"
                cell.textView.placeholderFieldText = "COMMENT"
                cell.textView.fieldText = fieldOtherText
                cell.textView.isHidden = false
            }
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        previousSelectedRow = indexPath
        tableView.reloadData()
        checkAndActivateButton()
    }
}

// MARK:- Display Logic Conformance
extension FeedbackVC: FeedbackDisplayLogic {

    func fedbackSubmitResponse(response : SubmitFeedbackResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.dismiss(animated: true, completion: feedbackSubmitedCompletion)
    }

    func errorRecieved(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
}

// MARK:- Configuration Logic
extension FeedbackVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = FeedbackInteractor()
        let presenter = FeedbackPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension FeedbackVC {
    
}
