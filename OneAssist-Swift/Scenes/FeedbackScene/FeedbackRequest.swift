//
//  FeedbackRequest.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class FeedbackRequest
{
    var customerId = ""
    var rating = ""
    var feedbackCode = ""
    var feedbackComment = ""
}
