//
//  FeedbackInteractor.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol FeedbackPresentationLogic {
    func responseRecieved(response: SubmitFeedbackResponseDTO?, error: Error?)
}

class FeedbackInteractor: BaseInteractor, FeedbackBusinessLogic {
    var presenter: FeedbackPresentationLogic?
    
    // MARK: Business Logic Conformance
    func submitFeeback(serviceRequestId : String, feedbackRating : String?, feebdbackCode : String?, feedbackComment : String?) {
        let request = SubmitFeedbackRequestDTO()
        request.serviceReqId = serviceRequestId
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? UserCoreDataStore.currentUser?.custIds?.first ?? ""
        let serviceRequestFeedback = ServiceRequestFeedback(dictionary: [:])
        serviceRequestFeedback.feedbackCode = feebdbackCode
        serviceRequestFeedback.feedbackRating = feedbackRating
        serviceRequestFeedback.feedbackComments = feedbackComment
        request.serviceRequestFeedback = serviceRequestFeedback
        
        let _ = WHCSubmitFeedbackUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.responseRecieved(response: response, error: error)
        }
    }
}
