//
//  OnlineOffersVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OnlineOffersBusinessLogic {
    func getOffers(filterName: String?, pageNo: Int, cards: [Card], filterbankCards: [Card], onBoardingCards: [Card])
    
}

class OnlineOffersVC: BaseVC, ChildOffers {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var emptyLabel: H3BoldLabel!
    
    weak var childDelegate: ChildViewDelegate?
    
    var interactor: OnlineOffersBusinessLogic?
    
    var userCards: [Card] = []
    var filterBankCards: [Card] = []
    var onBoardingCards: [Card] = []
    var sectionList: [[BankCardOffers]]?
    
    var filterName: String?
    var changedFilterName: String?
    
    var pageNo: Int = 1
    var loadMoreRow = 0
    var hasFirstResponse: Bool = false
    var apiInProgress: Bool = false
    var bankFilterApply: Bool = false
    var selectedBookMarkId: String? = nil
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()

        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(bookmarkStatusChanged(_:)), name: Notification.Name("BookmarkStatusChanged"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        if checkForRefreshOffers() {
            invalidateData()
            getOffers()
        }
    }
    
    func checkForRefreshOffers() -> Bool {
        
        if !hasFirstResponse || filterName != changedFilterName || self.bankFilterApply == true  {
            filterName = changedFilterName
            return true
        }
        return false
    }
    
    func invalidateData() {
        self.emptyLabel.text = nil
        self.emptyLabel.isHidden = true
        loadMoreRow = 0
        sectionList = nil
        pageNo = 1
        tableView.reloadData()
    }
    
    func getOffers() {
        Utilities.addLoader(count: &loaderCount, isInteractionEnabled: false)
        interactor?.getOffers(filterName: changedFilterName, pageNo: pageNo, cards: userCards, filterbankCards: filterBankCards.count>0 ? filterBankCards : [], onBoardingCards: onBoardingCards)
    }
    
//    func getUserCards() {
//        if let pratentVC = self.parent as? AllOffersVC {
//            pratentVC.getUserCards()
//        }else {
//            invalidateData()
//            getOffers()
//        }
//    }
    
    //MARK: - helper methods
    func fetchFilteredOffers(filterName: String?) {
        changedFilterName = filterName
    }
    
    func updateWallet(cards: [Card]) {
        self.userCards = cards
        invalidateData()
        getOffers()
    }
    
    func fetchBankFilteredOffers(filterBankCards: [Card])-> Bool {
        if self.filterBankCards.containsSameElements(as: filterBankCards) {
            self.bankFilterApply = false
            
        }else {
            self.filterBankCards = filterBankCards
            self.bankFilterApply = true
        }
        return self.bankFilterApply
    }
    
    @objc func bookmarkStatusChanged(_ notification: NSNotification) {

        if let offerId = notification.userInfo?[Constants.KeyValues.offerId] as? String, let status = notification.userInfo?[Constants.KeyValues.status] as? String, offerId != "" {
            if status == "1" {
               selectedBookMarkId = offerId
            }
            
            guard let sectionList = self.sectionList else {
                return
            }
            for onlineOffers in sectionList {
                if let offer = onlineOffers.first(where: {$0.offerId == offerId}) {
                    offer.isBookmarked = status == "1" ? true : false
                    self.tableView.reloadData()
                    break
                }
            }
        }
    }
    
    // MARK:- private methods
    private func initializeView() {
       // filterBankCards = userCards
        
        tableView.registerHeader(withIdentifier: Constants.HeaderViewIdentifiers.OfferHeaderView)
        tableView.register(nib: Constants.CellIdentifiers.offerLoadMoreCell, forCellReuseIdentifier: Constants.CellIdentifiers.offerLoadMoreCell)
        tableView.register(nib: Constants.CellIdentifiers.offerListCell, forCellReuseIdentifier: Constants.CellIdentifiers.offerListCell)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 440
    }
    
}

// MARK:- Table Datasource Conformance
extension OnlineOffersVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sectionList = self.sectionList else {
            return 0
        }
        return sectionList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionList = self.sectionList else {
            return 0
        }
        if section == sectionList.count-1 {
            return sectionList[section].count + loadMoreRow
        }
        return sectionList[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let sectionList = self.sectionList else {
            return UITableViewCell()
        }
        if indexPath.section == sectionList.count-1 {
            if indexPath.row == sectionList[indexPath.section].count {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.offerLoadMoreCell, for: indexPath) as! OfferLoadMoreCell
                cell.actIndicator.startAnimating()
                cell.actIndicator.isHidden = false
                return cell
            }
        }
    
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.offerListCell, for: indexPath) as! OfferListCell
        cell.delegate = self
        
        return cell
    }
    
}

// MARK:- Table Delegate Conformance
extension OnlineOffersVC: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        childDelegate?.childViewScrolled(childScrollView: scrollView)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let sectionList = self.sectionList else {
            return
        }
        
        if (cell is OfferListCell) {
          let onlineOffers = sectionList[indexPath.section]
          let cellObj = (cell as! OfferListCell)
          let offer = onlineOffers[indexPath.row]
          if selectedBookMarkId != nil && offer.offerId == selectedBookMarkId {
              offer.isBookmarked = true
              selectedBookMarkId = nil
          }
          cellObj.configureCell(model: offer, for: indexPath)
        }
        
        if indexPath.section == sectionList.count-1 {
            if indexPath.row == sectionList[indexPath.section].count {
                if !apiInProgress {
                    apiInProgress = true
                    interactor?.getOffers(filterName: changedFilterName, pageNo: pageNo, cards: userCards, filterbankCards: filterBankCards.count>0 ? filterBankCards : [], onBoardingCards: onBoardingCards)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let sectionList = self.sectionList else {
            return
        }
        if indexPath.section == sectionList.count-1 {
            if indexPath.row == sectionList[indexPath.section].count {
                return
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
        let onlineOffers = sectionList[indexPath.section]
        routeToOfferDetail(offer: onlineOffers[indexPath.row], index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let sectionList = self.sectionList, sectionList.count == 2 else {
            return 0.01
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let sectionList = self.sectionList, sectionList.count == 2 else {
            return UIView()
        }
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.HeaderViewIdentifiers.OfferHeaderView) as! OfferHeaderView
        if section == 0 {
            headerView.titleLabel.text = "OFFER ON YOUR CARD" + (userCards.count>1 ? "S" : "")
            
        }else {
            headerView.titleLabel.text = "OTHER OFFERS"
            
        }
        return headerView
    }
    
}

// MARK:- Offer List Cell Delegate Conformance
extension OnlineOffersVC: OfferListCellDelegate {
    func clickedBookmark(for indexPath: IndexPath?) {
        self.tableView.reloadData()
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.bookmarkOffer.message, subTitle:nil, forScreen: "Bookmark Offer") {[unowned self] (status) in
            if status {
                guard let sectionList = self.sectionList, let selectionIndexPath = indexPath else {
                    return
                }
              let onlineOffers = sectionList[selectionIndexPath.section]
              let offer = onlineOffers[selectionIndexPath.row]
              if offer.isBookmarked == false {
                  EventTracking.shared.eventTracking(name: .markFavourite ,
                                                     [.merchantName: offer.merchantName ?? "",
                                                      .type: Event.EventParams.online,
                                                      .position: NSNumber.init(value:selectionIndexPath.row),
                                                      .expiryDate: (DateFormatter.serverDateFormat.date(from: offer.endDate ?? "") ?? ""),
                                                      .location: FromScreen.fromAllOffers.rawValue,
                                                      .offerID:offer.offerId ?? ""])
                  if Utilities.offersFavoritedCount() > 1 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[weak self] in
                        guard let self = self else { return }
                         UserDefaultsKeys.Location.set(value: Event.EventParams.favouriteOffer)
                          Utilities.checkForRatingPopUp(viewController: self)
                      }
                  }
              }
              offer.isBookmarked = !offer.isBookmarked
              OfferSyncUseCase.shared.storeOfferInDb(offer: offer)
            }
        }
    }
}

// MARK:- Offer Detail Delegate Conformance
extension OnlineOffersVC: OfferDetailBookmarkDelegate {
    
    func clickedDetailBookmark() {
       // self.tableView.reloadData()
    }
}


// MARK:- Display Logic Conformance
extension OnlineOffersVC: OnlineOffersDisplayLogic {
    func displayOnlineOffers(sectionList: [[BankCardOffers]] , noOfRows: Int) {
        self.sectionList = sectionList
        self.loadMoreRow = noOfRows
        Utilities.removeLoader(count: &loaderCount)
        hasFirstResponse = true
        bankFilterApply = false
        pageNo = pageNo + 1
        self.tableView.reloadData()
        apiInProgress = false
        
        if let list = self.sectionList, list.count > 0 {
            self.emptyLabel.text = nil
            self.emptyLabel.isHidden = true
        }
    }
    
    func displayOnlineOffersError(_ error: String, pageNo: Int) {
        if pageNo == 1 {
            Utilities.removeLoader(count: &loaderCount)
            hasFirstResponse = true
            bankFilterApply = false
            showAlert(message: error)
            if let list = self.sectionList, list.count > 0 { }else {
                self.emptyLabel.text = error
                self.emptyLabel.isHidden = false
            }
        } else {
            self.loadMoreRow = 0
            self.tableView.reloadData()
        }
        
        apiInProgress = false
    }
}

// MARK:- Configuration Logic
extension OnlineOffersVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = OnlineOffersInteractor()
        let presenter = OnlineOffersPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension OnlineOffersVC {
    
    func routeToOfferDetail(offer: BankCardOffers, index: Int) {
        let vc = OnlineOfferDetailVC()
        vc.delegate = self
        vc.fromScreen = .fromAllOffers
        vc.selectedOffer = offer
        vc.index = index
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
