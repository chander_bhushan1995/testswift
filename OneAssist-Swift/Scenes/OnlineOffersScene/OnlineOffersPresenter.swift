//
//  OnlineOffersPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OnlineOffersDisplayLogic: class {
    func displayOnlineOffers(sectionList: [[BankCardOffers]] , noOfRows: Int)
    func displayOnlineOffersError(_ error: String, pageNo: Int)
}

class OnlineOffersPresenter: BasePresenter, OnlineOffersPresentationLogic {
    weak var viewController: OnlineOffersDisplayLogic?
    var userCardOffer: [BankCardOffers] = []
    var otherCardOffer: [BankCardOffers] = []
    
    // MARK: Presentation Logic Conformance
    func recievedOffersResponse(card:[Card],  response: OnlineCardOffersResponseDTO?, error: Error?, pageNo: Int) {
        
        if pageNo == 1 {
            userCardOffer = []
            otherCardOffer = []
        }
        
        do {
            try checkError(response, error: error)
            
            guard let newList = response?.data, newList.count > 0 else {
                viewController?.displayOnlineOffersError(response?.message ?? "", pageNo: pageNo)
                return
            }
            
//            let cardIssuerCodes = Array(Set(card.map {$0.cardIssuerCode}))
//            let cardTypeIds = Array(Set(card.map {$0.cardTypeId?.stringValue}))

            if let newList = response?.data {
//                if otherCardOffer.count>0 {
//                    otherCardOffer += newList
//                }else {
                
                var existingcardArray: [BankCardOffers] = []
                
                newList.forEach { (offers) in
                    if let cardDetailList = offers.cardDetailsList {
                        var isAdded = false
                        for cardDetail in cardDetailList {
                            for addedCard in card {
                                if addedCard.cardIssuerCode == cardDetail.cardIssuerCode && addedCard.cardTypeId?.stringValue == cardDetail.cardTypeId {
                                    isAdded = true
                                    existingcardArray += [offers]
                                    break;
                                }
                            }
                            if isAdded {
                                break;
                            }
                        }
                    }
                }
            
//                let existingcardArray = newList.filter { (offer) -> Bool in
//                    return (offer.cardDetailsList?.contains(where: {cardIssuerCodes.contains($0.cardIssuerCode) && cardTypeIds.contains($0.cardTypeId)}) ?? false)
//                }
//                let existingcardArray = newList.filter{cardIssuerCodes.contains($0.cardDetails?.cardIssuerCode) && cardTypeIds.contains($0.cardDetails?.cardTypeId)}
                userCardOffer += existingcardArray
                otherCardOffer += newList.filter{!existingcardArray.contains($0)}
//                }

                var sectionList: [[BankCardOffers]] = []
                if userCardOffer.count > 0 {
                    sectionList.append(userCardOffer)
                }
                if otherCardOffer.count > 0 {
                    sectionList.append(otherCardOffer)
                }
                
                var loadMoreRow = 0
                if ((userCardOffer.count+otherCardOffer.count) % 10 == 0) {
                    loadMoreRow += 1
                }
                
                viewController?.displayOnlineOffers(sectionList: sectionList, noOfRows: loadMoreRow)
            }
        } catch {
            viewController?.displayOnlineOffersError(error.localizedDescription, pageNo: pageNo)
        }
    }
}
