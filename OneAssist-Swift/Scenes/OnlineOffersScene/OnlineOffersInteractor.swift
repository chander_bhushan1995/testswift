//
//  OnlineOffersInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/09/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OnlineOffersPresentationLogic {
    func recievedOffersResponse(card:[Card],  response: OnlineCardOffersResponseDTO?, error: Error?, pageNo: Int)
}

class OnlineOffersInteractor: BaseInteractor, OnlineOffersBusinessLogic {
    var presenter: OnlineOffersPresentationLogic?
    
    // MARK: Business Logic Conformance
    func getOffers(filterName: String?, pageNo: Int, cards: [Card], filterbankCards: [Card], onBoardingCards: [Card]) {
        
//        let otherCars = filterbankCards.filter({ filtercard in
//            !cards.contains(where: { usercard in
//                (filtercard.cardIssuerCode == usercard.cardIssuerCode && filtercard.cardTypeId == usercard.cardTypeId)
//            })
//        }).map{Card(card: $0)}
//
//        let addedCars = filterbankCards.filter({ filtercard in
//            cards.contains(where: { usercard in
//                (filtercard.cardIssuerCode == usercard.cardIssuerCode && filtercard.cardTypeId == usercard.cardTypeId)
//            })
//        }).map{Card(card: $0)}
        
        let otherCars = filterbankCards.map{ Card(card: $0) }
        
        var addedCars: [Card] = []
        if otherCars.count <= 0 {
            addedCars = onBoardingCards.filter{element in
                return !cards.contains(where: {element.cardTypeId == $0.cardTypeId && element.cardIssuerCode == $0.cardIssuerCode})
            } + cards
        }
                
        let request = OnlineCardOffersRequestDTO()
        let cusId = Int(UserCoreDataStore.currentUser?.cusId ?? "0")
        request.customerId = NSNumber(integerLiteral: cusId ?? 0)
        request.longitude = NSNumber()
        request.latitude = NSNumber()
        request.pageNo = NSNumber(integerLiteral: pageNo)
        request.pageSize = NSNumber(integerLiteral: kPageSizeForOffer)
        request.category = filterName
       // request.cards = filterbankCards.map{ Card(card: $0) }
        request.addedCards = addedCars
        request.otherCards = otherCars
        
        let _ = OnlineCardOffersRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            self?.presenter?.recievedOffersResponse(card:cards, response: response, error: error, pageNo: pageNo)
        }
    }
}
