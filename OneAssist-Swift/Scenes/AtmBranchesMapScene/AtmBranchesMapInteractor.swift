//
//  AtmBranchesMapInteractor.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 19/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

enum AtmRadius : Float {
    case initialRadiusValue = 10.0
}

protocol AtmBranchesPresentationLogic {
    func responseRecievedWallet(response: WalletCardsResponseDTO?, error: Error?)
    func responseRecievedNearestATM(response: NearestATMResponseDTO?, error: Error?)
    func userLocationDidUpdate(location : CLLocationCoordinate2D)
}

class AtmBranchesMapInteractor: BaseInteractor, AtmBranchesBusinessLogic {
    var presenter: AtmBranchesPresentationLogic?

    var radius : Float = AtmRadius.initialRadiusValue.rawValue
    lazy var locationManager = Permission.shared.locationManager

    // MARK: Business Logic Conformance
    func getCustomerCards() {
        let _ = WalletCardsUseCase.service(requestDTO: WalletCardsRequestDTO()) { (usecase, response, error) in
            self.presenter?.responseRecievedWallet(response: response, error: error)
        }
    }

    func getNearestAtmBranches(bankName : String?, latitude : NSNumber, longitude : NSNumber) {
        let request = NearestATMRequestDTO(name: bankName ?? "", latitude: latitude, longitude: longitude, radius: NSNumber.init(value: self.radius))
        let _ = NearestATMRequestUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.responseRecievedNearestATM(response: response, error: error)
        }
    }

       func fetchUserLocation() {
           self.locationManager.delegate = self
           self.locationManager.distanceFilter = kCLDistanceFilterNone
           self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
           self.locationManager.startUpdatingLocation()
       }
}

extension AtmBranchesMapInteractor : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           manager.stopUpdatingLocation()
           let currentLocation = locations[0]
           presenter?.userLocationDidUpdate(location: currentLocation.coordinate)
       }
       
       func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
           manager.stopUpdatingLocation()
           let errors = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey: Strings.Global.somethingWrong])
           presenter?.responseRecievedNearestATM(response: nil, error: errors)
       }
}

