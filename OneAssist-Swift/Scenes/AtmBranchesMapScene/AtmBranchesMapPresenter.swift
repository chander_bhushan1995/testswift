//
//  AtmBranchesMapPresenter.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 19/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

protocol AtmBranchesDisplayLogic: class {
    func cardsListRecieved(cardList: WalletCardsResponseDTO)
    func atmBranchListRecieved(atmList: NearestATMResponseDTO)
    func errorRecieved(_ error: String)
    func userLocationDidUpdate(location : CLLocationCoordinate2D)
}

class AtmBranchesMapPresenter: BasePresenter, AtmBranchesPresentationLogic {
    weak var viewController: AtmBranchesDisplayLogic?

    // MARK: Presentation Logic Conformance
    
    func responseRecievedWallet(response: WalletCardsResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.cardsListRecieved(cardList: response!)
        } catch {
            viewController?.errorRecieved(error.localizedDescription)
        }
    }
    func responseRecievedNearestATM(response: NearestATMResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            viewController?.atmBranchListRecieved(atmList: response!)
        } catch {
            viewController?.errorRecieved(error.localizedDescription)
        }
    }

    func userLocationDidUpdate(location : CLLocationCoordinate2D) {
        viewController?.userLocationDidUpdate(location : location)
    }
}
