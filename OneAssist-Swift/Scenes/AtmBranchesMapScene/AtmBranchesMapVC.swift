//
//  AtmBranchesMapVC.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 19/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

protocol AtmBranchesBusinessLogic {
    func getCustomerCards()
    func getNearestAtmBranches(bankName : String?, latitude : NSNumber, longitude : NSNumber)
    func fetchUserLocation()
}

class AtmClusterMarkerModel: NSObject, GMUClusterItem {
    var bankName : String?
    var bankAddress : String?
    var bankLogoUrl : String?
    var position: CLLocationCoordinate2D
    var inactiveIcon: UIImage?
    var activeIcon : UIImage?
    
    init(position: CLLocationCoordinate2D) {
        self.position = position
    }
}

class AtmBranchesMapVC : BaseVC, AddCardButtonDelegate {
    
    var interactor: AtmBranchesBusinessLogic?
    
    var issuerList : [WalletCard]?
    var selectedIssuer : WalletCard?
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var atmDirectionsView : BankAtmInformationView!
    @IBOutlet weak var constraintAtmDirectionsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cardsListView : CardsListMapView!
    @IBOutlet weak var constraintCardsListViewHeight: NSLayoutConstraint!
    
    var userLocation : CLLocationCoordinate2D!
    fileprivate var currentSelectedMarker : GMSMarker?
    fileprivate var currentSelectedLocation : CLLocationCoordinate2D?
    
    fileprivate var clusterManager: GMUClusterManager!
    fileprivate var markers : [AtmClusterMarkerModel] = []
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScreen), name: .refreshViewController, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterForegroundOnViewController), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func didEnterForegroundOnViewController(){
        UserDefaultsKeys.Location.set(value: Event.Events.atmFinder.rawValue)
        Utilities.checkForRatingPopUp(viewController:self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureView()
    }
    
    deinit {
         Permission.shared.resetData()
        NotificationCenter.default.removeObserver(self, name: .refreshViewController, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func refreshScreen() {
        mapView.camera = GMSCameraPosition.camera(withTarget: userLocation, zoom: 5.0)
        interactor?.getCustomerCards()
    }
    
    // MARK:- private methods
    private func initializeView() {
        
        let navigationSelectionView = NavigationBarSelectionView.instanceFromNib()
        navigationSelectionView.delegate = self
        navigationItem.titleView = navigationSelectionView
        
        cardsListView.delegate = self
        cardsListView.carddelegate = self
        atmDirectionsView.delegate = self
        atmDirectionsView.isHidden = true
        
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        // To shift the user location button on map a padding of 50 px is added so that directions button does not overlap
        let mapInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 40.0, right: 0.0)
        mapView.padding = mapInsets
        
        // Set up the cluster manager with default icon generator and renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 50, 100, 200, 1000], backgroundImages: [#imageLiteral(resourceName: "atmBranchMarkerInactive"), #imageLiteral(resourceName: "atmBranchMarkerInactive"), #imageLiteral(resourceName: "atmBranchMarkerInactive"), #imageLiteral(resourceName: "atmBranchMarkerInactive"), #imageLiteral(resourceName: "atmBranchMarkerInactive")])
        
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
    }
    
    private func configureView() {
        
        constraintAtmDirectionsViewHeight.constant = 0
        atmDirectionsView.updateConstraints()
        
        constraintCardsListViewHeight.constant = 0
        cardsListView.updateConstraints()
        cardsListView.isHidden = true
        
        Permission.shared.checkLocationPermission(eventLocation: Event.EventParams.ATM) {[weak self] (status) in
            guard let self = self else {return}
            if status ==  .notDetermined{
                
            }else if status == .authorizedAlways || status == .authorizedWhenInUse {
                if self.mapView.isMyLocationEnabled {
                    self.interactor?.fetchUserLocation()
                }
                
            }else {
                self.showOpenSettingsAlert(title: MhcStrings.AlertMessage.gpsTitle, message: MhcStrings.AlertMessage.gpsSetting) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func loadMarkersOnMap(forBank bank : String?) {
        mapView.clear()
        Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: false)
        interactor?.getNearestAtmBranches(bankName: bank, latitude: NSNumber.init(value: userLocation.latitude), longitude: NSNumber.init(value: userLocation.longitude))
    }
    
    func showCardsListView() {
        EventTracking.shared.eventTracking(name: .editAtm)
        
        cardsListView.isHidden = false
        
        var count = 0
        if let issuerList = issuerList {
            count = issuerList.count + 1
        }
        var cardListViewHeight : CGFloat = 0.0
        cardListViewHeight = count < 6 ? (BankListCell.cellHeight() * CGFloat(count)) + cardsListView.constraintAddCardViewHeight.constant : (BankListCell.cellHeight() * CGFloat(cardsListView.maxRowsToShow)) + cardsListView.constraintAddCardViewHeight.constant
        let directionsViewY = atmDirectionsView.frame.origin.y
        cardListViewHeight = cardListViewHeight > (directionsViewY - cardsListView.frame.origin.y) ? (directionsViewY - (cardsListView.frame.origin.y * 2.0)) : cardListViewHeight
        
        constraintCardsListViewHeight.constant = cardListViewHeight
        UIView.animate(withDuration: 0.2, animations: {
            self.cardsListView.alpha = 1.0
            self.view.layoutIfNeeded()
        })
    }
    
    func hideCardsListView() {
        constraintCardsListViewHeight.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.cardsListView.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.cardsListView.isHidden = true
        }
    }
    
    func showAtmDetailsCardView() {
        atmDirectionsView.isHidden = false
        self.constraintAtmDirectionsViewHeight.constant = 135
        UIView.animate(withDuration: 0.2) {
            self.atmDirectionsView.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    func hideAtmDetailsCardView() {
        currentSelectedMarker?.icon = #imageLiteral(resourceName: "atmBranchMarkerInactive")
        currentSelectedMarker = nil
        currentSelectedLocation = nil
        
        self.constraintAtmDirectionsViewHeight.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.atmDirectionsView.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.atmDirectionsView.isHidden = true
        }
    }
}

extension AtmBranchesMapVC : NavigationBarActionDelegate {
    func navigationBarActionButtonTapped() {
        // toggle visiblity of the card list card view
        if cardsListView.isHidden {
            showCardsListView()
        } else {
            hideCardsListView()
        }
    }
}

extension AtmBranchesMapVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        hideCardsListView()
        
        if let item = marker.userData as? AtmClusterMarkerModel {
            
            if currentSelectedMarker == nil || (currentSelectedMarker != nil && marker != currentSelectedMarker!) {
                atmDirectionsView.bankCoordinates = item.position
                
                atmDirectionsView.bankNameLabel.text = item.bankName
                atmDirectionsView.bankAddressLabel.text = item.bankAddress
                let placeHolderImage = #imageLiteral(resourceName: "bankBranchMarkerInactive")
                var imageUrl : String?
                if selectedIssuer != nil, let bankData : Issuer = CoreDataStack.sharedStack.fetchObject(predicateString: "cardIssuerCode == %@", inManagedObjectContext: CoreDataStack.sharedStack.mainContext,argumentList:[selectedIssuer!.issuerCode!])!.first {
                    imageUrl = bankData.imagePath
                }
                atmDirectionsView.bankIconImageView.setImageWithUrlString(imageUrl, placeholderImage: placeHolderImage)
                
                currentSelectedMarker?.icon = #imageLiteral(resourceName: "atmBranchMarkerInactive")
                currentSelectedMarker = marker
                marker.icon = #imageLiteral(resourceName: "atmBranchMarkerActive")
                currentSelectedLocation = marker.position
                
                clusterManager.cluster()
                
                showAtmDetailsCardView()
            }
        } else {
            print("Did tap a cluster marker")
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        hideCardsListView()
        hideAtmDetailsCardView()
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        hideCardsListView()
        hideAtmDetailsCardView()
        return false
    }
    
    func focusMapToShowAllMarkers() {
        let firstLocation = markers.first!.position
        var bounds = GMSCoordinateBounds.init(coordinate: firstLocation, coordinate: firstLocation)
        
        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(15))
        self.mapView.animate(with: update)
    }
}

extension AtmBranchesMapVC : GMUClusterManagerDelegate {
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: mapView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
        return false
    }
}

extension AtmBranchesMapVC : GMUClusterRendererDelegate {
    
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        
        let marker = BankMarkerView()
        if let model = object as? AtmClusterMarkerModel {
            
            var image = model.inactiveIcon
            marker.bankName = model.bankName
            marker.bankAddress = model.bankAddress
            if currentSelectedLocation != nil && model.position.latitude == currentSelectedLocation!.latitude && model.position.longitude == currentSelectedLocation!.longitude {
                image = model.activeIcon
                currentSelectedMarker = marker
            }
            marker.map = mapView
            marker.icon = image
        }
        
        return marker
    }
}

extension AtmBranchesMapVC : BankAtmDirectionDelegate {
    
    func directionsButtonTapped(coordinates: CLLocationCoordinate2D) {
        EventTracking.shared.eventTracking(name: .getDirection,[.location : Event.Events.findATM.rawValue , .atm : selectedIssuer?.issuerName ?? ""])
        Utilities.openGoogleMapsDirectionsUrl(from: userLocation, to: coordinates)
    }
}

extension AtmBranchesMapVC : CardRowTappedDelegate {
    
    func issuerSelected(issuer : WalletCard?) {
        if let issuer = issuer {
            loadMarkersOnMap(forBank: issuer.issuerName)
            selectedIssuer = issuer
            (navigationItem.titleView as! NavigationBarSelectionView).navigationBarActionButton.setTitle(issuer.issuerName ?? "", for: .normal)
            EventTracking.shared.addScreenTracking(with: .bankATM)
        } else {
            EventTracking.shared.eventTracking(name: .viewAllATM)
            loadMarkersOnMap(forBank: nil)
            (navigationItem.titleView as! NavigationBarSelectionView).navigationBarActionButton.setTitle("All ATMs nearby", for: .normal)
            selectedIssuer = nil
        }
        
        hideCardsListView()
    }
}

// MARK:- Display Logic Conformance
extension AtmBranchesMapVC: AtmBranchesDisplayLogic {
    
    func cardsListRecieved(cardList: WalletCardsResponseDTO) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if let cardListData = cardList.data?.cards {
            issuerList = cardListData.unique{$0.issuerCode!}.filter({ $0.isBankCard })
            cardsListView.issuerList = issuerList
            cardsListView.cardsTableView.reloadData()
            
            if let issuer = selectedIssuer {
                var index = 0
                var issuerName : String?
                if let banks = issuerList {
                    for i in 0..<banks.count {
                        let bank = banks[i]
                        if bank.issuerName == issuer.issuerName {
                            index = i + 1
                            issuerName = bank.issuerName
                            break
                        }
                    }
                    cardsListView.selectTableRow(row : index)
                    loadMarkersOnMap(forBank : issuerName)
                } else {
                    cardsListView.selectTableRow(row : 0)
                    loadMarkersOnMap(forBank : nil)
                }
            } else {
                cardsListView.selectTableRow(row : 0)
                loadMarkersOnMap(forBank : nil)
            }
        }
    }
    
    func atmBranchListRecieved(atmList: NearestATMResponseDTO) {
        
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        
        markers.removeAll()
        clusterManager.clearItems()
        
        if let atmData = atmList.data {
            for atmBranchData in atmData {
                let position = CLLocationCoordinate2D(latitude: NSString(string: atmBranchData.latitude!).doubleValue, longitude: NSString(string: atmBranchData.longitude!).doubleValue)
                
                let item = AtmClusterMarkerModel(position: position)
                item.bankName = atmBranchData.name
                item.bankAddress = atmBranchData.address
                //item.bankLogoUrl = bankBranchData.
                item.inactiveIcon = #imageLiteral(resourceName: "atmBranchMarkerInactive")
                item.activeIcon = #imageLiteral(resourceName: "atmBranchMarkerActive")
                clusterManager.add(item)
                markers.append(item)
            }
            
            // Call cluster() after items have been added to perform the clustering and rendering on map.
            clusterManager.cluster()
            
            // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
            clusterManager.setDelegate(self, mapDelegate: self)
            focusMapToShowAllMarkers()
        }
    }
    
    func errorRecieved(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func userLocationDidUpdate(location : CLLocationCoordinate2D) {
        if userLocation == nil {
            userLocation = location
            mapView.camera = GMSCameraPosition.camera(withTarget: userLocation, zoom: 5.0)
            interactor?.getCustomerCards()
        }
    }
}

// MARK:- Configuration Logic
extension AtmBranchesMapVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = AtmBranchesMapInteractor()
        let presenter = AtmBranchesMapPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

extension AtmBranchesMapVC {
    func addCardButtonTapped() {
        self.showPopupForVerifyNumber(title: Strings.NumberVerifyAlerts.addCard.message, subTitle: nil, forScreen: "Add Card") { (status) in
            if status {
                EventTracking.shared.eventTracking(name: .addCard, [.location: Event.EventParams.atm])
                let vc = CardManagementVC()
                vc.addToInitialProperties(["deeplink_url_string" : "addcard/DBC", "navigatingFrom": "Atm finder Screen"])
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
}
