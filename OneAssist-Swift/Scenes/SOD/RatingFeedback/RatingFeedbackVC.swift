//
//  RatingFeedbackVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 16/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol RatingFeedbackBusinessLogic {
    func getFeedbacks(for rating: Int, ratingType: Constants.RatingType?)
    func submitFeedback(serviceRequestID:String,feedbackCodes: String, rating: Int,comments: String)
}

class RatingFeedbackVC: BaseVC,HideNavigationProtocol {
    
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var headingLabel: H3BoldLabel!
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var submitFeedbackButton: OAPrimaryButton!
    
    var onSubmitFeedback: (()->())?
    var rating: Int?
    var ratingType:  Constants.RatingType?
    var serviceRequestID: String?
    var comments:String?
    
    fileprivate var dataSource: TableViewDataSource<Row, FeedbackTableViewSection>?
    fileprivate var interactor: RatingFeedbackBusinessLogic?
    
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseViews()
    }
    
    
    func initialiseViews(){
        checkButtonEnabled()
        headingLabel.text = Strings.RatingFeedback.whatsWrong
        submitFeedbackButton.setTitle(Strings.RatingFeedback.submitFeedback, for: .normal)
        tableViewObj.dataSource = self
        tableViewObj.delegate = self
        tableViewObj.sectionHeaderHeight = UITableView.automaticDimension
        tableViewObj.estimatedSectionHeaderHeight = CGFloat(200)
        if let rating = self.rating, let ratingType = self.ratingType {
            Utilities.addLoader(onView: view, message: Strings.RatingFeedback.pleaseWait, count: &loaderCount, isInteractionEnabled: true)
            interactor?.getFeedbacks(for: rating, ratingType: ratingType)
        }
    }
    
    fileprivate func checkButtonEnabled() {
        
        if let sections = dataSource?.tableSections {
            for section in sections {
                if section.isSelected, !section.isCommentTextView {
                    submitFeedbackButton.isEnabled = true
                    return
                }
            }
        }
        
        submitFeedbackButton.isEnabled = false
    }
    
    
    @IBAction func tappedOnCrossButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTappedOnSubmitButton(_ sender: Any) {
        EventTracking.shared.eventTracking(name: .ratingfeedback,[.location: Strings.SODConstants.sodClaim,.feedback: Strings.SODConstants.feedback])
        var reasons = [String]()
        var reasonText = [String]()
        if let sections = dataSource?.tableSections {
            for section in sections {
                if section.isSelected,!section.isCommentTextView {
                    reasons.append(section.feedbackId ?? "")
                    reasonText.append(section.title ?? "")
                }            }
        }
        let allReasons = reasons.joined(separator: ",")
        if let serviceRequestID = self.serviceRequestID {
            Utilities.addLoader(message: Strings.RatingFeedback.pleaseWait, count: &loaderCount, isInteractionEnabled: false)
            self.interactor?.submitFeedback(serviceRequestID: serviceRequestID, feedbackCodes: allReasons, rating: self.rating ?? 0, comments: self.comments ?? "")
        }
    }
}

extension RatingFeedbackVC: RatingFeedbackDisplayLogic {
    func displayFeedbacks(datasource: TableViewDataSource<Row, FeedbackTableViewSection>) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        dataSource = datasource
        tableViewObj.reloadData()
    }
    
    func displayError(error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(message: error)
    }
    
    func displayFeedbackSubmitted(){
        Utilities.removeLoader(count: &loaderCount)
        ChatBridge.refreshHomeScreen()
        if let onSubmitFeedback = self.onSubmitFeedback {
            self.dismiss(animated: false)
            onSubmitFeedback()
        }
    }
}

extension RatingFeedbackVC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.numberOfRows(in: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let model = dataSource?.sectionModel(for: section), model.isCommentTextView {
            let view = Bundle.main.loadNibNamed(Constants.CellIdentifiers.WHCService.commentView, owner: nil, options: nil)?.first as? CommentView
            view?.setViewModel(placeholder: model.subtitle ?? "",title: model.title ?? "",delegate: self)
            return view
        }else{
            let view = Bundle.main.loadNibNamed(Constants.CellIdentifiers.WHCService.feedbackTableHeaderView, owner: nil, options: nil)?.first as? FeedbackTableHeaderView
            let model = dataSource?.sectionModel(for: section)
            view?.tag = section
            view?.delegate = self
            view?.seperatorView.isHidden = false
            view?.setFeedback(model?.title ?? "", isSelected: model?.isSelected ?? false)
            return view
        }
    }
}

extension RatingFeedbackVC: FeedbackTableHeaderViewDelegate {
    func feedbackTableHeaderView(_ cell: FeedbackTableHeaderView, isSelected: Bool) {
        let section = cell.tag
        dataSource?.sectionModel(for: section).isSelected = isSelected
        checkButtonEnabled()
    }
}

extension RatingFeedbackVC: CommentViewDelegate {
    func didFinishEditingComment(comment: String) {
        self.comments = comment
    }
}

extension RatingFeedbackVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = RatingFeedbackInteractor()
        let presenter = RatingFeedbackPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}


