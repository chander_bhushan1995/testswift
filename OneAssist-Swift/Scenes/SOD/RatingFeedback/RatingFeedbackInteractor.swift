//
//  RatingFeedbackInteractor.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 16/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol RatingFeedbackPresentationLogic {
    func feedbackListReceived(response: WHCFeedbackResponseDTO?, error: Error?)
    func feedbackSubmitted(error: Error?)
}


class RatingFeedbackInteractor: RatingFeedbackBusinessLogic {
    
    var presenter: RatingFeedbackPresentationLogic?
    
    func getFeedbacks(for rating: Int, ratingType: Constants.RatingType?) {
        let request = WHCFeedbackRequestDTO(rating.description, ratingType: ratingType)
        
        let _ = WHCFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.feedbackListReceived(response: response, error: error)
        }
    }
    
    func submitFeedback(serviceRequestID:String,feedbackCodes: String, rating: Int,comments: String) {
        let feedbackRequest = ServiceRequestFeedback(dictionary: [:])
        feedbackRequest.feedbackCode = feedbackCodes
        feedbackRequest.feedbackRating = rating.description
        feedbackRequest.feedbackComments = comments
        let request = SubmitFeedbackRequestDTO()
        request.serviceReqId = serviceRequestID
        // FIXME: remove default value
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? ""
        request.serviceRequestFeedback = feedbackRequest
        
        let _ = WHCSubmitFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            self.presenter?.feedbackSubmitted(error: error)
        }

    }
}
