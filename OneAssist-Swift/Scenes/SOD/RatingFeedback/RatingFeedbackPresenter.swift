//
//  RatingFeedbackPresenter.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 16/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol RatingFeedbackDisplayLogic: class {
    func displayFeedbacks(datasource: TableViewDataSource<Row, FeedbackTableViewSection>)
    func displayError(error: String)
    func displayFeedbackSubmitted()
}

class RatingFeedbackPresenter: BasePresenter, RatingFeedbackPresentationLogic {
    weak var viewController: RatingFeedbackDisplayLogic?
    
    func feedbackListReceived(response: WHCFeedbackResponseDTO?, error: Error?) {
        if let feedbackList = response?.data {
            var sections: [FeedbackTableViewSection] = [FeedbackTableViewSection]()
            for feedback in feedbackList {
                let section = FeedbackTableViewSection()
                section.feedbackId = feedback.valueId?.stringValue
                section.title = feedback.value
                
                sections.append(section)
            }
            
            let section = FeedbackTableViewSection()
            section.isSelected = true
            section.isCommentTextView = true
            section.title = Strings.RatingFeedback.leaveComment
            section.subtitle = Strings.RatingFeedback.tellFeedback
            sections.append(section)
            
            let dataSource = TableViewDataSource<Row, FeedbackTableViewSection>()
            dataSource.tableSections = sections
            
            self.viewController?.displayFeedbacks(datasource: dataSource)
        } else {
            self.viewController?.displayError(error: error?.localizedDescription ?? "")
        }
    }
    
    func feedbackSubmitted(error: Error?){
        if let error = error {
            self.viewController?.displayError(error: error.localizedDescription ?? "")
        }else{
            self.viewController?.displayFeedbackSubmitted()
        }
    }
}
