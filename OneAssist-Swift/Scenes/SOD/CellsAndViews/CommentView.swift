//
//  CommentView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 16/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

protocol CommentViewDelegate {
    func didFinishEditingComment(comment:String)
}

class CommentView: UIView {
    
    
    @IBOutlet weak var titleLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var textView: KMPlaceholderTextView!
    var delegate: CommentViewDelegate?
    
    override func awakeFromNib() {
        textView.delegate = self
        textView.layer.borderColor = UIColor.dlsBorder.cgColor
        textView.layer.cornerRadius = 2
        textView.layer.borderWidth = 1
        textView.clipsToBounds = true
    }
    
    func setViewModel(placeholder: String,title: String,delegate: CommentViewDelegate?){
        self.titleLabel.text = title
        self.textView.placeholder = placeholder
        self.delegate = delegate
    }
    
}

extension CommentView: UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        self.delegate?.didFinishEditingComment(comment: textView.text ?? "")
    }
}
