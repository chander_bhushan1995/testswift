//
//  RefundDetailTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 31/12/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct RefundDetailModel {
    var purchaseAmount: String?
    var orderId: String?
}

class RefundDetailTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var trasactionIdLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupModel(_ model: RefundDetailModel){
        self.amountLabel.text = model.purchaseAmount
        self.trasactionIdLabel.text = model.orderId
    }
    
}
