//
//  TimelineHeaderView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

protocol RatingHeaderViewDelegate: NSObjectProtocol {
    func backButtonTapped()
    func chatButtonTapped()
    func didUpdateAlpha(_ alpha: CGFloat)
}

struct RatingHeaderViewModel {
    var imageRightText:String?
    var headingText:String?
    var descriptionText:String?
    var image: UIImage?
    var backgroundColor: UIColor?
}


class RatingHeaderView: GSKStretchyHeaderView {
    
    @IBOutlet weak var backIcon: UIImageView!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var imageTextContainer: UIStackView!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var imageRightTextLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var headingTextLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var descriptionTextLabel: BodyTextRegularBlackLabel!
    @IBOutlet var headingTrailing: NSLayoutConstraint!
    @IBOutlet var headingLeading: NSLayoutConstraint!
    @IBOutlet var headingTop: NSLayoutConstraint!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: BodyTextBoldBlackLabel!
    
    
    
    var delegate: RatingHeaderViewDelegate?
    var model: RatingHeaderViewModel?
    
    class func loadView() -> RatingHeaderView {
        return Bundle.main.loadNibNamed("RatingHeaderView", owner: self, options: nil)?[0] as! RatingHeaderView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backButtonTapped)))
        backIcon.isUserInteractionEnabled = true
        
        expansionMode = .topOnly
        contentExpands = false
        contentAnchor = .bottom
        minimumContentHeight = 64
        maximumContentHeight = 120
        
        imageRightTextLabel.textColor = .white
        headingTextLabel.textColor = .white
        titleLabel.alpha = 0
        descriptionTextLabel.textColor = .white
        chatButton.titleLabel?.font = DLSFont.supportingText.regular
        let chatImage = UIImage(named: "chatSmall")
        let tintedImage = chatImage?.withRenderingMode(.alwaysTemplate)
        chatButton.setImage(tintedImage, for: .normal)
        chatButton.tintColor = .buttonBlue
    }
    
    func setViewModel(_ model: RatingHeaderViewModel?, delegate: RatingHeaderViewDelegate) {
        self.model = model
        self.delegate = delegate
        headingTextLabel.text = model?.headingText
        titleLabel.text = model?.headingText
        descriptionTextLabel.text = model?.descriptionText
        imageRightTextLabel.text = model?.imageRightText
        if let image = model?.image {
            leftImageView.isHidden = false
            leftImageView.image = image
        }else{
            leftImageView.isHidden = true
        }
        setMaximumContentHeight(RatingHeaderView.height(forData: model), resetAnimated: true)
    }
    
    class func height(forData aData: RatingHeaderViewModel?) -> CGFloat {
        var height: CGFloat = 64
        if let model = aData {
            if model.image != nil || model.imageRightText != nil{
                var width:CGFloat = 32.0
                var heightStackView: CGFloat = 0
                if model.image != nil {
                    width += 24
                    heightStackView += 20
                }
                height += max(heightStackView,(model.imageRightText?.height(withWidth: screensize.width - width, font: DLSFont.bodyText.bold) ?? 0)) + 12.0 // 12 is top height
            }
            height += (model.headingText?.height(withWidth: screensize.width - 32, font: DLSFont.bodyText.bold) ?? 0)
            height += (model.descriptionText?.height(withWidth: screensize.width - 32, font: DLSFont.supportingText.regular) ?? 0) + 10.0 // 12 is top&bottom space
        }
        return height
    }
    
    override func didChangeStretchFactor(_ stretchFactor: CGFloat) {
        var alpha = max(0, min(1, CGFloatTranslateRange(stretchFactor, 0.2, 0.8, 0, 1)))
        var beta = 1 - alpha
        if stretchFactor.isNaN {
            beta = 1
            alpha = 0
        }
        imageTextContainer.alpha = alpha
        descriptionTextLabel.alpha = alpha
        headingTextLabel.alpha = alpha
        titleLabel.alpha = beta
        backgroundView.backgroundColor = model?.backgroundColor?.transformToColor(.white, by: beta)
        backIcon.changePngColorTo(color: UIColor.white.transformToColor(.black, by: beta))
        chatButton.tintColor = UIColor.white.transformToColor(.buttonBlue, by: beta)
        chatButton.setTitleColor(UIColor.white.transformToColor(.buttonBlue, by: beta), for: .normal)
        delegate?.didUpdateAlpha(alpha)
    }
    
    @objc func backButtonTapped() {
        delegate?.backButtonTapped()
    }
    
    @IBAction func tappedOnChat(_ sender: Any) {
        delegate?.chatButtonTapped()
    }
}
