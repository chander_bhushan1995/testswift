//
//  LeftImgRightDescCollectionViewCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 31/05/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit


class LeftImgRightDescCollectionViewCell: UICollectionViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardsDescriptiontraillingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardsDescription: BodyTextRegularGreyLabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardsDescription.textColor = UIColor.charcoalGrey.withAlphaComponent(0.75)
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 4
        containerView.layer.borderColor = (UIColor.activeBorderColor).cgColor
        containerView.layer.borderWidth = 1
    }
    
    private func setupViewForVideoGuide() {
        cardsDescriptiontraillingConstraint.constant = 8.0
        cardImageViewWidthConstraint.constant = 40.0
        cardImageViewHeightConstraint.constant = 44.0
        cardsDescription.textColor = UIColor.charcoalGrey
        cardsDescription.font = DLSFont.supportingText.regular
        containerView.backgroundColor = UIColor.buttonTitleBlue.withAlphaComponent(0.06)
        containerView.layer.cornerRadius = 0
        containerView.layer.borderWidth = 0
    }
    
    func setSafetyViewModel(model: SafetyData){
        self.cardImageView.setImageWithUrlString(model.iconUrl)
        self.cardsDescription.text = model.title
    }
    
    func setupVideoGuideData(model: ImageWithValue){
        setupViewForVideoGuide()
        self.cardImageView.setImageWithUrlString(model.image)
        self.cardsDescription.text = model.value
    }
}
