//
//  StatisticsCellTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 07/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit




class FactsCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.layer.cornerRadius = 4
        self.backgroundColor = .statsBGBlue
    }
    
    
    
}
