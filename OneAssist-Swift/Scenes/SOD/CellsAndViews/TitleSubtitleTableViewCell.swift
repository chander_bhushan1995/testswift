//
//  TitleSubtitleTableViewCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct TitleSubtitleCellVM {
    var heading:String?
    var description:String?
}

class TitleSubtitleTableViewCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var headingLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func setViewModel(_ model: TitleSubtitleCellVM){
        self.headingLabel.text = model.heading
        self.descriptionLabel.text = model.description
        self.headingLabel.setLineSpacing(lineSpacing: 3, lineHeightMultiple: 1)
        self.descriptionLabel.setLineSpacing(lineSpacing: 3, lineHeightMultiple: 1)
    }
    
    func setViewModel(_ model: ActivationHeader?){
        self.descriptionLabel.textColor = UIColor.charcoalGrey
        self.headingLabel.font = DLSFont.h2.bold
        self.descriptionLabel.font = DLSFont.h2.bold
        self.headingLabel.text = model?.title
        self.descriptionLabel.text = model?.subTitle
        if !(model?.subTitleBold?.boolValue ?? true) {
            self.descriptionLabel.font = DLSFont.h3.regular
        }
        headingLabel.setLineSpacing(lineSpacing: 4, lineHeightMultiple: 1)
    }
    
    func setViewModelForBottomSheet(_ model: ActivationHeader?){
        self.descriptionLabel.textColor = UIColor.charcoalGrey
        self.headingLabel.font = DLSFont.h3.bold
        self.descriptionLabel.font = DLSFont.h3.regular
        self.headingLabel.text = model?.title
        self.descriptionLabel.text = model?.subTitle
    }
    
    class func heightForBottomSheetModel(_ model: ActivationHeader?, totalWidth: CGFloat) -> CGFloat {
        let widthConstraints: CGFloat = 32
        let collectiveHeigthConstains: CGFloat = 29
        let titleHeight = model?.title?.height(withWidth: totalWidth - widthConstraints, font: DLSFont.h3.bold) ?? 0.0
        let descHeight = model?.subTitle?.height(withWidth: totalWidth - widthConstraints, font: DLSFont.h3.regular) ?? 0.0
        return collectiveHeigthConstains + titleHeight + descHeight
    }
}
