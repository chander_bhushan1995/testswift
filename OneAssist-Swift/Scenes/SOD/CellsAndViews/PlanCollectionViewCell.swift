//
//  PlanCollectionViewCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import Cosmos

struct PlanCollectionViewCellVM{
    var heading:String?
    var description: String?
    var note: String?
    var image:UIImage?
    var primaryButtonText: String?
    var secondaryButtonText: String?
    var listItems:[String] = []
    var listItemsImageSize = CGSize(width: 6, height: 6)
    var listItemsImage: UIImage?
    var imageUrl: String?
    var badgeText: String?
    var actionUrl: String?
    var planCD: PlanCD?
    
    func sizeForCell(isMirrorFlow: Bool = false) -> CGSize{
        var height:CGFloat = 34 + 16 // root view top & bottom
        
        let headingHeight = (self.heading?.height(withWidth: screensize.width-(16+16+64+16), font: DLSFont.bodyText.bold)) ?? 0 // first 64 is ( 32 for plan cell lead trail and next 32 for collection view cell lead and trail)
        if let _ = description{
            height += max((self.description?.height(withWidth: screensize.width-(16+16+64+16), font: DLSFont.bodyText.regular) ?? 0 + 8 + headingHeight),56) //56 is image height
        }else{
            height += max(headingHeight,56) //56 is image height
        }
        
        height += 24 //bottom of list view
        if !self.listItems.isEmpty {
            var listModel = ListViewModel()
            listModel.list = Array(self.listItems.prefix(3))
            listModel.headerToListSpacing = 0
            if let image = self.listItemsImage {
                listModel.image = image
                listModel.imageSize = self.listItemsImageSize
                height += ListView().setViewModel(model: listModel) ?? 0 + 20
                //                    height += 16+16 //leading trailing of list view
            }
        }
        
        if let _ = note {
            height += self.note?.height(withWidth: screensize.width - 32, font: DLSFont.supportingText.regular) ?? 0 + 24 // note text height + top
        }
        
        if let _ = secondaryButtonText {
            height += 36
        }
        
        height += (24 + 16 + 48+20) // primary button top, bottom, height
        return CGSize(width: screensize.width - 32,height: height)
    }
    
    init(heading: String?, image: UIImage?, primaryButtonText: String?, listItems: [String]?, listItemsImageSize: CGSize, listItemsImage: UIImage?, badgeText: String?, planCD: PlanCD?){
        self.heading = heading
        self.image = image
        self.primaryButtonText = primaryButtonText
        self.listItems = listItems ?? []
        self.listItemsImageSize = listItemsImageSize
        self.listItemsImage = listItemsImage
        self.badgeText = badgeText
        self.planCD = planCD
        self.secondaryButtonText = (listItems?.count ?? 0) > 3 ? "See all benefits" : nil
    }
    
    init(_ plan: PitchPlan?) {
        heading = plan?.title
        description = plan?.desc
        primaryButtonText = plan?.butttonText
        note = plan?.note
        imageUrl = plan?.image
        actionUrl = plan?.actionUrl
    }
}

protocol PlanCollectionViewCellDelegate {
    func tappedOnPrimaryButton(_ planCD: PlanCD?, actionUrl: String?)
    func tappedOnSecondaryButton(_ benefits: [String])
}

class PlanCollectionViewCell: UICollectionViewCell, ReuseIdentifier, NibLoadableView {
    
    
    @IBOutlet weak var shadowView: DLSShadowView!
    @IBOutlet weak var planNameLabel: BodyTextBoldBlackLabel!
    @IBOutlet weak var ratingViewContainer: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewsLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var planImage: UIImageView!
    @IBOutlet weak var listView: ListView!
    @IBOutlet weak var viewDetailsButton: OAPrimaryButton!
    @IBOutlet weak var badgeLabel: TagsBoldGreyLabel!
    @IBOutlet weak var descriptionLabel: BodyTextRegularGreyLabel!
    
    @IBOutlet weak var noteLabel: SupportingTextRegularBlackLabel!
    
    @IBOutlet weak var secondaryButton: OASecondaryButton!
    @IBOutlet weak var buttonsContainerViewTop: NSLayoutConstraint!
    
    var delegate: PlanCollectionViewCellDelegate?
    var model: PlanCollectionViewCellVM?
    var planCD: PlanCD?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        listView.isHidden = true
        ratingViewContainer.isHidden = true
        badgeLabel.backgroundColor = .sunglow
        badgeLabel.textColor = .white
        badgeLabel.isHidden = true
        self.shadowView.clipsToBounds = true
        descriptionLabel.isHidden = true
        //planNameLabel.textColor = .white
    }
    
    func setViewModel(_ model: PlanCollectionViewCellVM, delegate: PlanCollectionViewCellDelegate){
        self.model = model
        self.planCD = model.planCD
        
        self.delegate = delegate
        planNameLabel.text = model.heading
        planImage.image = model.image
        if let imgUrl = model.imageUrl {
            planImage.setImageWithUrlString(imgUrl)
        }
        viewDetailsButton.setTitle(model.primaryButtonText, for: .normal)
        descriptionLabel.text = model.description
        descriptionLabel.isHidden = model.description?.isEmpty ?? true
        noteLabel.text = model.note
        noteLabel.isHidden = model.note?.isEmpty ?? true
        secondaryButton.setTitle(model.secondaryButtonText, for: .normal)
        secondaryButton.isHidden = model.secondaryButtonText?.isEmpty ?? true
        if let _ = model.note {
            buttonsContainerViewTop.constant = 24
        }
        
        if let badgeText = model.badgeText{
            badgeLabel.isHidden = false
            badgeLabel.text = badgeText
        }else{
            badgeLabel.isHidden = true
        }
        
        
        if !model.listItems.isEmpty {
            listView.isHidden = false
            var listModel = ListViewModel()
            listModel.headerToListSpacing = 0
            listModel.list = Array(model.listItems.prefix(3))
            if let image = model.listItemsImage {
                listModel.image = image
                listModel.imageSize = model.listItemsImageSize
                listView.setViewModel(model: listModel)
            }
        }else{
            listView.isHidden = true
        }
    }
    
    @IBAction func tappedOnPrimaryButton(_ sender: Any) {
        delegate?.tappedOnPrimaryButton(self.planCD, actionUrl: model?.actionUrl)
    }
    
    @IBAction func tappedOnSecondaryButton(_ sender: Any) {
        if let list = self.model?.listItems {
            delegate?.tappedOnSecondaryButton(list)
        }
    }
}
