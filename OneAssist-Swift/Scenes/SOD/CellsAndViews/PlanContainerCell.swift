//
//  PlanContainerCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

//MARK: - VIEW MODEL FOR CURRENT CELL
struct PlanContainerCellVM{
    var attributedString:NSAttributedString?
    var cells:[Any]?
    var backgroundColors: (top: UIColor,bottom:UIColor)
}

//MARK: - DELEGATES
protocol PlanContainerCellDelegate {
    func viewDetailsOfPlan(_ planCD: PlanCD?)
    func viewPan(_ actionUrl: String?)
    func viewPlanBenefits(_ benefits: [String])
}

class PlanContainerCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    //MARK: - OUTLETS
    @IBOutlet weak var containerView: LayerContainerView!
    @IBOutlet weak var headingLabel: H3RegularBlackLabel!
    @IBOutlet weak var collectionView: WrappingCollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    //MARK: - INSTANCE VARIABLES
    var delegate: PlanContainerCellDelegate?
    var model: PlanContainerCellVM?
    var isMirrorFlow: Bool =  false
    
    //MARK: - OVERRIDED METHODS
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.backgroundColor = UIColor.orange
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cell: PlanCollectionViewCell.self)
    }
    
    //MARK: - INSTANCE METHODS
    func setViewModel(_ model: PlanContainerCellVM,delegate: PlanContainerCellDelegate, isMirrorFlow: Bool = false){
        self.isMirrorFlow = isMirrorFlow
        self.delegate = delegate
        self.model = model
        headingLabel.attributedText = model.attributedString
        collectionView.reloadData()
        self.collectionView.layoutIfNeeded()
        //self.collectionViewHeight.constant = 200.0
        self.collectionViewHeight.constant = self.collectionView.intrinsicContentSize.height
        containerView.update(start: .topCenter, end: .bottomCenter, colors: [model.backgroundColors.top,model.backgroundColors.bottom], type: .axial)
    }
}

//MARK: - COLLECTIONVIEW DATASOURCE , DELEGATE CONFIRMANCE
extension PlanContainerCell: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model?.cells?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let model = self.model?.cells?[indexPath.item] as? PlanCollectionViewCellVM{
            let cell: PlanCollectionViewCell = collectionView.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model, delegate: self)
            return cell
        }
        return UICollectionViewCell()
    }
    
    
}

//MARK: - FLOW LAYOUT CONFIRM
extension PlanContainerCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if  let model = self.model?.cells?[indexPath.item] as? PlanCollectionViewCellVM{
            return model.sizeForCell(isMirrorFlow: self.isMirrorFlow)
        }
        return CGSize(width: 0,height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

//MARK: - PLAN DETAIL CELL DELEGATE
extension PlanContainerCell :PlanCollectionViewCellDelegate{
    func tappedOnPrimaryButton(_ planCD: PlanCD?, actionUrl: String?){
        if self.isMirrorFlow {
            self.delegate?.viewPan(actionUrl)
        }else {
            self.delegate?.viewDetailsOfPlan(planCD)
        }
    }
    
    func tappedOnSecondaryButton(_ benefits: [String]){
        self.delegate?.viewPlanBenefits(benefits)
    }
}

