//
//  LeftRightImageWithLabelTooltipView.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 18/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct RoundedTooltipVM {
    var leftImage: UIImage?
    var text: String?
    var rightImage: UIImage?
}

protocol RoundedTooltipDelegate {
    func tappedOnView()
}

class RoundedTooltip: UIView {

    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var label: BodyTextBoldBlackLabel!
    @IBOutlet weak var rightImage: UIImageView!
    
    var delegate: RoundedTooltipDelegate?
    
    override func awakeFromNib() {
        containerView.layer.cornerRadius = containerView.frame.height/2
        label.textColor = .white
    }
    
    class func loadView() -> RoundedTooltip {
        return Bundle.main.loadNibNamed("RoundedTooltip", owner: self, options: nil)?[0] as! RoundedTooltip
    }
    
    func setViewModel(model: RoundedTooltipVM, delegate: RoundedTooltipDelegate) {
        self.leftImage.image = model.leftImage
        self.label.text = model.text
        self.rightImage.image = model.rightImage
        self.delegate = delegate
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = containerView.frame.height/2
    }
    
    @IBAction func tappedOnView(_ sender: Any) {
        delegate?.tappedOnView()
    }
    
}
