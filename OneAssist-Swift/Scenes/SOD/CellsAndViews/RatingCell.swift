//
//  RatingCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit
import Cosmos

struct RatingCellVM {
    var currentRating: Double?
    var heading: String?
    var ratingTitle: String?
    var description: String?
    var primaryButtonText: String?
    var ratingSumitImage: String = "star_confatti"
    var isHideRatingViewContainer: Bool? = false
    var primaryButtonTag: RatingCellPrimaryBtnTags = RatingCellPrimaryBtnTags.submitBtnTag
    var tags: [RatingTagVM] = []
    var isVisibleTagList: Bool = false
}

enum RatingCellPrimaryBtnTags: Int {
    case submitBtnTag = 10
    case rateNowBtnTag = 20
    case addFeedback = 30
}

protocol RatingCellDelegate {
    func didSubmitRating(indexPath: IndexPath?,rating: Double)
    func didTappedOnRateNow(indexPath: IndexPath?)
    func didRatingSelected(rating: Double, indexPath:IndexPath?)
    func didSelectedTag(for index: Int,indexPath:IndexPath?)
    func didSelectedFeedback(indexPath: IndexPath?)
}

class RatingCell: UITableViewCell, ReuseIdentifier, NibLoadableView {
    
    @IBOutlet weak var dlsShadowView: DLSShadowView!
    @IBOutlet weak var dlsShadowViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rateAppStoreViewContainer: UIView!
    @IBOutlet weak var ratefinalImage: UIImageView!
    @IBOutlet weak var descriptionLabel: BodyTextRegularBlackLabel!
    
    @IBOutlet weak var ratingViewContainer: UIView!
    @IBOutlet weak var headingLabel: H3BoldLabel!
    @IBOutlet weak var ratingTitleLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var primaryButton: OAPrimaryButton!
    @IBOutlet weak var primaryButtonHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var tagStyledView: TagStyledView!
    @IBOutlet weak var tagViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tagContainerView: UIView!
    @IBOutlet var tagContainerViewBottomConstraint: NSLayoutConstraint!
    
    var delegate: RatingCellDelegate?
    var model: RatingCellVM?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rateAppStoreViewContainer.isHidden = true
        ratingViewContainer.isHidden = false
        ratingView.settings.emptyImage = UIImage(named: "starGrey")
        ratingView.settings.filledImage = UIImage(named: "starGreen")
        ratingView.settings.fillMode = .full
        ratingView.settings.starMargin = 10
        //        ratingView.settings.emptyBorderColor = UIColor.clearColor
        primaryButton.isEnabled = ratingView.rating > 0
        primaryButton.isEnabled = false
        
        ratingView.didTouchCosmos = { rating in
            self.primaryButton.isEnabled = rating > 0
            self.delegate?.didRatingSelected(rating: rating, indexPath: self.indexPath)
        }
    }
    
    func setViewModel(_ model: RatingCellVM,delegate: RatingCellDelegate){
        setupTagView()
        self.model = model
        self.delegate = delegate
        self.headingLabel.text = model.heading
        self.ratingTitleLabel.text = model.ratingTitle
        self.descriptionLabel.text = model.description
        self.ratefinalImage.image = UIImage(named: model.ratingSumitImage)
        self.ratingView.rating = model.currentRating ?? 0
        self.primaryButton.tag = model.primaryButtonTag.rawValue
        self.primaryButton.setTitle(model.primaryButtonText, for: .normal)
        self.primaryButton.isEnabled = (model.currentRating ?? 0) > 0
        ratingViewContainer.isHidden = model.isHideRatingViewContainer ?? false
        rateAppStoreViewContainer.isHidden = !(model.isHideRatingViewContainer ?? false)
        
        if model.primaryButtonText == nil {
            self.primaryButtonHeightConstraints.constant = 0.0
            self.primaryButton.isHidden = true
            
        }else {
            self.primaryButton.isHidden = false
            self.primaryButtonHeightConstraints.constant = 48.0
        }
        
        if model.tags.count>0, model.isVisibleTagList == true {
            dlsShadowViewTopConstraint.constant = 0.0
            self.tagStyledView.tags = model.tags as [AnyObject]
            tagContainerView.isHidden = false
            tagContainerViewBottomConstraint.isActive = true
            
        }else {
           tagContainerView.isHidden = true
            if tagContainerViewBottomConstraint != nil {tagContainerViewBottomConstraint.isActive = false}
        }
    }
    
    private func setupTagView() {
        tagStyledView.cellForItem = { item in
            if let cell = item.cell as? RatingTagsCell {
                cell.setViewModel(model: self.model?.tags[item.indexPath.item])
            }
        }
        tagStyledView.didSelectItemAt = { indexPath in
        }
        
        tagStyledView.containerSize = {[unowned self] size in
            if size.height>100 {
                self.tagViewHeight.constant = size.height
            }else {
                self.tagViewHeight.constant = 100.0
            }
        }
        
        tagStyledView.register(UINib(nibName: RatingTagsCell.Identifier.identifier, bundle: nil), forCellWithReuseIdentifier: RatingTagsCell.Identifier.identifier)
        
        tagStyledView.options = TagStyledView.Options(sectionInset: UIEdgeInsets(top: 6.0, left: 6.0, bottom: 6.0, right: 6.0),
                                                      lineSpacing: 10.0,
                                                      interitemSpacing: 10.0,
                                                      align: TagStyledView.Options.Alignment.center)
        
        tagStyledView.didSelectItemAt = { [weak self] indexPath in
            self?.delegate?.didSelectedTag(for: indexPath.item, indexPath: self?.indexPath)
        }
    }
    
    
    @IBAction func tappedOnButton(_ sender: UIButton) {
        switch sender.tag {
        case RatingCellPrimaryBtnTags.submitBtnTag.rawValue:
            delegate?.didSubmitRating(indexPath: self.indexPath, rating: ratingView.rating)
        case RatingCellPrimaryBtnTags.rateNowBtnTag.rawValue:
            delegate?.didTappedOnRateNow(indexPath: self.indexPath)
       case RatingCellPrimaryBtnTags.addFeedback.rawValue:
            delegate?.didSelectedFeedback(indexPath: self.indexPath)
        default:
            break
        }
    }
}
