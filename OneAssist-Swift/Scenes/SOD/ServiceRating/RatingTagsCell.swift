//
//  RatingTagsCell.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 29/08/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

struct RatingTagVM {
    var tagText:String?
    var isSelected: Bool = false
}

extension RatingTagsCell {
    struct Identifier {
        static let identifier = String(describing: RatingTagsCell.self)
    }
}

class RatingTagsCell: UICollectionViewCell, TagStyling {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tagLabel.font = DLSFont.tags.bold
        tagLabel.textColor = .buttonTitleBlue
        containerView.layer.cornerRadius = 2
        containerView.layer.borderColor = UIColor.buttonTitleBlue.cgColor
        containerView.layer.borderWidth = 1
    }
    
    func setViewModel(model: RatingTagVM?){
        tagLabel.text = model?.tagText
        if model?.isSelected ?? false {
            self.containerView.backgroundColor = .buttonTitleBlue
            self.tagLabel.textColor = .white
        } else {
            self.containerView.backgroundColor = .white
            tagLabel.textColor = .buttonTitleBlue
        }
    }
    
    func updateStyle(_ isViewMore: Bool) { }

}
