//
//  ServiceRatingInteractor.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol ServiceRatingPresentationLogic {
    func presentPlanData(serviceRequestType:String,serviceReqID:String,productCode:String,dataCD: [PlanCD]?,couponCodeResponse: CouponsResponseDTO?,homeApplianceCategoryResponseDTO:HomeApplianceCategoryResponseDTO?,subscriptionBenefits: SubscriptionBenefitsResponseDTO?, error: Error?)
    func ratingSubmitted(response: SubmitFeedbackResponseDTO?,indexPath: IndexPath)
    func mirrorFlowRatingData(response: MirrorFlowRatingRespModel?)
    func displayError(_ error:Error)
}

class ServiceRatingInteractor: ServiceRatingBusinessLogic {
    var presenter: ServiceRatingPresentationLogic?
    
    var couponCodeResponse: CouponsResponseDTO?
    var homeApplianceCategoryResponseDTO: HomeApplianceCategoryResponseDTO?
    var subscriptionBenefits: SubscriptionBenefitsResponseDTO?
    var plansResponseArray: [PlanCD]?
    var error:Error?
    
    func loadRatingAndPlanData(serviceRequestType:String,serviceReqID:String,productCode:String){
        let group = DispatchGroup()
        
        group.enter()
        let _ = SODAppliancesUseCase.service(requestDTO: nil) {[weak self] (usecase, response, error) in
            guard self != nil else { return }
            self?.homeApplianceCategoryResponseDTO = response
            group.leave()
        }
        
        group.enter()
        let _ = OASubscriptionBenefitsUseCase.service(requestDTO: nil) {[weak self] (usecase, response, error) in
            guard self != nil else { return }
            self?.subscriptionBenefits = response
            group.leave()
        }
        
        group.enter()
        let _ = CouponCodeUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.couponCodeResponse = response
            if error == nil {
                let obj = SuggestPlanRequestDTO.requestForWHC()
                let _  = SuggestPlansUseCase.service(requestDTO:obj) { [weak self] (service,response,error) in
                    self?.plansResponseArray = response?.dataCD
                    self?.error = error
                    group.leave()
                }
            }else{
                self.error = error
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            self.presenter?.presentPlanData(serviceRequestType:serviceRequestType,serviceReqID:serviceReqID,productCode:productCode,dataCD: self.plansResponseArray,couponCodeResponse: self.couponCodeResponse, homeApplianceCategoryResponseDTO: self.homeApplianceCategoryResponseDTO, subscriptionBenefits: self.subscriptionBenefits, error: self.error)
        }
    }
    
    func loadMirrorFlowRatingData(){
        OAMirrorFlowRatingUseCase.service(requestDTO: nil) { [weak self] (_, response, error) in
            guard let self = self else { return }
            self.presenter?.mirrorFlowRatingData(response: response)
        }
    }
    
    func submitRating(serviceReqID:String,rating: Double,indexPath: IndexPath){
        let request = SubmitFeedbackRequestDTO()
        request.serviceReqId = serviceReqID
        // FIXME: remove default value
        request.modifiedBy = UserCoreDataStore.currentUser?.cusId ?? "suren"
        let requestFeedback = ServiceRequestFeedback(dictionary: [:])
        requestFeedback.feedbackRating = Int(rating).description
        requestFeedback.feedbackCode = ""
        requestFeedback.feedbackComments = ""
        request.serviceRequestFeedback = requestFeedback
        let _ = WHCSubmitFeedbackUseCase.service(requestDTO: request) { (usecase, response, error) in
            if let error = error {
                self.presenter?.displayError(error)
            }
            self.presenter?.ratingSubmitted(response: response,indexPath: indexPath)
        }
    }
}
