//
//  ServiceRatingModel.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

struct ServiceRatingModel{
    var headerData: RatingHeaderViewModel?
    var planBenefitsBottomSheet: PlanBenefitsBottomSheetVM?
    var sections: [ServiceRatingSectionModel] = []
    
}

struct ServiceRatingSectionModel {
    var cells: [Any] = []
}
