//
//  FeedbackInputViewController.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 03/09/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

protocol FeedbackInputDelegate: class {
    func submitInputFeedback(_ feddback: String?, for indexPath: IndexPath?)
}

class FeedbackInputViewController: BaseVC {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var inputContainer: UIView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var inputErrorLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    weak var delegate: FeedbackInputDelegate?
    var indexPath: IndexPath?

    // MARK: - OVERRIDED METHODS
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }

    
    private func initializeView() {
        backgroundImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(headerTapped))
        backgroundImageView.addGestureRecognizer(tap)
        
        inputTextView.styleTextView(with: .white, borderColor: UIColor.black, shadowColor: UIColor.white)
         
        inputTextView.text = nil
        inputErrorLabel.text = nil
        inputErrorLabel.textColor = .salmonRed
    }
    
    @objc func headerTapped() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmitAction(_ sender: UIButton?){
        if inputTextView.text.isEmpty {
            inputErrorLabel.text = Strings.EmptyTextFieldError.inputFeedback
        }else {
            self.delegate?.submitInputFeedback(inputTextView.text, for: self.indexPath)
            self.dismiss(animated: true, completion: nil)
        }
    }

}

extension FeedbackInputViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
         inputErrorLabel.text = nil
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
    
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        do {
            try Validation.validateDescription(text: text)
        } catch {
            return false
        }
        
        let str = textView.text as NSString
        let strNew = str.replacingCharacters(in: range, with: text)
        
        guard strNew.count <= 400 else {
            inputErrorLabel.text = Strings.EmptyTextFieldError.feedbackMax
            return false
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        inputErrorLabel.text = nil
    }
}
