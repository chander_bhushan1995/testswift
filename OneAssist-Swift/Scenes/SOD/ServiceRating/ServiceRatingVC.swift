//
//  ServiceRatingVC.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

// MARK: - BUISNESS LOGIC
protocol ServiceRatingBusinessLogic {
    func loadRatingAndPlanData(serviceRequestType:String,serviceReqID:String,productCode:String)
    func loadMirrorFlowRatingData()
    func submitRating(serviceReqID:String,rating: Double,indexPath: IndexPath)
}


enum FlowType {
    case MirrorFlow
    case RefundFlow
    case ServiceRatingFlow
}

class ServiceRatingVC: BaseVC, HideNavigationProtocol {
    
    // MARK: - OUTELETS
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var buttonView: UIView!
    
    // MARK: - INSTANCE VARIABLES
    var interactor: ServiceRatingBusinessLogic?
    var ratingHeaderView: RatingHeaderView!
    var serviceRatingModel: ServiceRatingModel = ServiceRatingModel()
    var serviceRequestID: String?
    var serviceRequestNumber: String? // basically it is refPrimaryTrackingNo in response
    var productName: String?
    var productCode: String?    // in case SOD it would be AC,TV etc
    var serviceRequestType: String?
    var serviceAddress: ServiceAddress?
    var onRatingCompletion: (()->())?
    var onViewPlan: ((_ actionUrl: String)->())?
    var flowType: FlowType = .ServiceRatingFlow
    fileprivate var alpha: CGFloat = 1
    
    // MARK: - OVERRIDED METHODS
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        self.loadRatingAndPlanData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var barViewColor: UIColor = .violet
        if self.flowType == .MirrorFlow {
            barViewColor = .seaGreen
        }else if self.flowType == .RefundFlow {
            barViewColor = .seaGreen
        }
        
        UIApplication.shared.makeStatusBarView(with: barViewColor)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.makeStatusBarView(with: .white)
        UIApplication.shared.statusBarStyle = .default
    }
    
    // MARK: - INSTANCE METHODS
    func initializeView(){
        addTap()
        view.backgroundColor = .backgroundWhiteBlue
        
        ratingHeaderView = RatingHeaderView.loadView()
        updateHeaderView()
        tableViewObj.addSubview(ratingHeaderView)
        if self.flowType != .RefundFlow {
            tableViewObj.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: .leastNormalMagnitude, right: 0);
            tableViewObj.contentInsetAdjustmentBehavior = .never
        }
        tableViewObj.register(cell: RatingCell.self)
        tableViewObj.register(cell: TitleSubtitleTableViewCell.self)
        tableViewObj.register(cell: PlanContainerCell.self)
        tableViewObj.register(cell: RefundDetailTableViewCell.self)
        tableViewObj.estimatedRowHeight = 100
        tableViewObj.rowHeight = UITableView.automaticDimension
        
        tableViewObj.dataSource = self
        tableViewObj.delegate = self
        tableViewObj.reloadData()
        tableViewObj.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        
        if self.flowType == .MirrorFlow {
            EventTracking.shared.eventTracking(name: .ratingPopUp, [.location: "Mobile FD Activation", .mode: FDCacheManager.shared.fdTestType.rawValue])
        }else if self.flowType == .RefundFlow {
            self.buttonView.isHidden = false
        }
    }
    
    func updateHeaderView() {
        var barViewColor: UIColor = .violet
        if self.flowType == .MirrorFlow {
            barViewColor = .seaGreen
        }else if self.flowType == .RefundFlow {
            barViewColor = .seaGreen
        }
        
        UIApplication.shared.statusBarView?.backgroundColor = barViewColor
        UIApplication.shared.statusBarStyle = .lightContent
        ratingHeaderView?.setViewModel(serviceRatingModel.headerData, delegate: self)
        if self.flowType == .MirrorFlow || self.flowType == .RefundFlow {
            ratingHeaderView?.chatButton.isHidden = true
        }
    }
    
    func loadRatingAndPlanData(){
        if self.flowType == .RefundFlow {
            //  self.tableViewObj.reloadData()
            //  updateHeaderView()
            
        }else {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
            if self.flowType == .MirrorFlow{
                self.interactor?.loadMirrorFlowRatingData()
                return
            }
            self.interactor?.loadRatingAndPlanData(serviceRequestType: self.serviceRequestType ?? "", serviceReqID: self.serviceRequestID ?? "", productCode: self.productCode ?? "")
        }
    }
    
    func submitRating(_ rating: Double, indexPath: IndexPath){
        if let requestID = self.serviceRequestID {
            Utilities.addLoader(onView: view, count: &loaderCount, isInteractionEnabled: true)
            interactor?.submitRating(serviceReqID: requestID, rating: rating, indexPath: indexPath)
        }
    }
    
    @IBAction func backToHomeAction(_ sender: UIButton?){
        rootTabVC?.changeTabIndex(itemTag: .newHome)
        navigationController?.popToRootViewController(animated: true)
    }
    
    deinit {
        UIApplication.shared.statusBarView?.backgroundColor = .white
        UIApplication.shared.statusBarStyle = .default
    }
}

//MARK: - TABLE VIEW DATASOURCE
extension ServiceRatingVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return serviceRatingModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceRatingModel.sections[section].cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData = serviceRatingModel.sections[indexPath.section].cells[indexPath.row]
        if let model = cellData as?  RatingCellVM {
            let cell: RatingCell = tableViewObj.dequeueReusableCell(indexPath: indexPath)
            cell.indexPath = indexPath
            cell.setViewModel(model, delegate: self)
            return cell
        }else if let model = cellData as?  TitleSubtitleCellVM {
            let cell: TitleSubtitleTableViewCell = tableViewObj.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model)
            return cell
        }else if let model = cellData as?  PlanContainerCellVM {
            let cell: PlanContainerCell = tableViewObj.dequeueReusableCell(indexPath: indexPath)
            cell.setViewModel(model, delegate: self, isMirrorFlow: (self.flowType == .MirrorFlow) ? true : false)
            return cell
        }else if let model = cellData as?  RefundDetailModel {
            let cell: RefundDetailTableViewCell = tableViewObj.dequeueReusableCell(indexPath: indexPath)
            cell.setupModel(model)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- Display Logic
extension ServiceRatingVC: ServiceRatingDisplayLogic {
    
    func displayError(_ error: String) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        showAlert(title: Strings.Global.error, message: error)
    }
    
    func displayPlanAndRating(model: ServiceRatingModel) {
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        self.serviceRatingModel = model
        updateHeaderView()
        tableViewObj.reloadData()
    }
    
    func displayRatingSubmitted(indexPath: IndexPath){
        Utilities.removeLoader(fromView: view, count: &loaderCount)
        if var model = self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] as? RatingCellVM{
            model.isHideRatingViewContainer = true
            model.isVisibleTagList = false
            model.primaryButtonText = Strings.ServiceRating.rateNow
            model.primaryButtonTag = .rateNowBtnTag
            self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] = model
            if let completionBlock = self.onRatingCompletion {
                completionBlock()
            }
            self.tableViewObj.reloadData()
            ChatBridge.refreshHomeScreen()
        }
    }
}

// MARK:- RatingHeaderViewDelegate
extension ServiceRatingVC: RatingHeaderViewDelegate{
    func didUpdateAlpha(_ alpha: CGFloat) {
        self.alpha = alpha
    }
    
    func backButtonTapped(){
            self.navigationController?.popViewController(animated: true)
    }
    func chatButtonTapped(){
        self.navigationController?.pushViewController(ChatVC(), animated: true)
    }
}

//MARK: - RatingCellDelegate
extension ServiceRatingVC: RatingCellDelegate{
    func didSelectedTag(for index: Int, indexPath: IndexPath?) {
        if let indexPath = indexPath {
            if var model = self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] as? RatingCellVM{
                var vm = model.tags[index]
                vm.isSelected = !vm.isSelected
                model.tags[index] = vm
                self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] = model
                UIView.performWithoutAnimation {
                    self.tableViewObj.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
    }
    
    
    func didSubmitRating(indexPath: IndexPath?,rating: Double){
        if let indexPath = indexPath {
            if self.flowType == .MirrorFlow {
                if var model = self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] as? RatingCellVM {
                    var tags: String? = nil
                    for tag in model.tags {
                        if tag.isSelected {
                            if tags == nil {tags = tag.tagText}
                            else {tags = tags! + "," + (tag.tagText ?? "")}
                        }
                    }
                    EventTracking.shared.eventTracking(name: .ratingSubmitted, [.location: "Mobile FD Activation", .starRating: model.currentRating ?? 0.0 , .tags: tags ?? "" , .mode: FDCacheManager.shared.fdTestType.rawValue])
                    
                    if (model.currentRating ?? 0.0) > 3.0 {
                        model.isHideRatingViewContainer = true
                        model.isVisibleTagList = false
                        model.description = Strings.ServiceRating.ratingOnAppStore
                        model.primaryButtonText = Strings.ServiceRating.rateNow
                        model.primaryButtonTag = .rateNowBtnTag
                        self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] = model
                        self.tableViewObj.reloadData()
                    } else {
                        model.isHideRatingViewContainer = true
                        model.isVisibleTagList = false
                        model.description = Strings.MirrorFlowRating.lowFeedback
                        model.ratingSumitImage = "star_confatti_sad"
                        model.primaryButtonText = Strings.MirrorFlowRating.addFeedback
                        model.primaryButtonTag = .addFeedback
                        self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] = model
                        self.tableViewObj.reloadData()
                    }
                }
            }else {
                if var model = self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] as? RatingCellVM,rating > 3.0{
                    model.currentRating = rating
                    model.isVisibleTagList = false
                    self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] = model
                    self.tableViewObj.reloadData()
                    self.submitRating(rating, indexPath: indexPath)
                    EventTracking.shared.eventTracking(name: .ratingSubmitted, [.location: Strings.SODConstants.sodClaim, .starRating: rating])
                }else{
                    let vc = RatingFeedbackVC()
                    vc.rating = Int(rating)
                    vc.ratingType = .haSOD
                    vc.serviceRequestID = self.serviceRequestID
                    vc.onSubmitFeedback = {
                        if let completionBlock = self.onRatingCompletion {
                            completionBlock()
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                    self.presentInFullScreen(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func didTappedOnRateNow(indexPath: IndexPath?){
        if self.flowType == .MirrorFlow {
            UserDefaultsKeys.Location.set(value: "Mobile FD Activation")
            
        }else {
            UserDefaultsKeys.Location.set(value: Strings.SODConstants.sodClaim)
        }
        
        Utilities.checkForRatingPopUp(viewController: self, isShowForceFully: true){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func didRatingSelected(rating: Double, indexPath: IndexPath?) {
        if let indexPath = indexPath{
            if var model = self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] as? RatingCellVM{
                model.currentRating = rating
                self.serviceRatingModel.sections[indexPath.section].cells[indexPath.row] = model
                UIView.performWithoutAnimation {
                    self.tableViewObj.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
    }
    
    func didSelectedFeedback(indexPath: IndexPath?) {
        if let indexPath = indexPath{
            let vc = FeedbackInputViewController()
            vc.delegate = self
            vc.indexPath = indexPath
            self.presentOverFullScreen(vc, animated: true)
        }
    }
    
}

extension ServiceRatingVC: FeedbackInputDelegate{
    func submitInputFeedback(_ feddback: String?, for indexPath: IndexPath?) {
        if let index = indexPath {
            if var model = self.serviceRatingModel.sections[index.section].cells[index.row] as? RatingCellVM {
                if self.flowType == .MirrorFlow {
                    EventTracking.shared.eventTracking(name: .ratingSubmitted, [.location: "Mobile FD Activation", .starRating: model.currentRating ?? 0.0, .feedback: feddback ?? "", .mode: FDCacheManager.shared.fdTestType.rawValue])
                }
                
                model.isHideRatingViewContainer = true
                model.isVisibleTagList = false
                model.description = Strings.MirrorFlowRating.submitFeedback
                model.ratingSumitImage = "star_confatti_normal"
                model.primaryButtonText = nil
                model.primaryButtonTag = .addFeedback
                self.serviceRatingModel.sections[index.section].cells[index.row] = model
                self.tableViewObj.reloadData()
            }
        }
    }
}

// MARK: - PlanContainerCellDelegate
extension ServiceRatingVC: PlanContainerCellDelegate{
    
    func viewPlanBenefits(_ benefits: [String]) {
        let bottomSheet = PlanBenefitsBottomSheetView()
        let bottomSheetVM = PlanBenefitsBottomSheetVM(title: self.serviceRatingModel.planBenefitsBottomSheet?.title ?? "", planBenefits: benefits)
        bottomSheet.setViewModel(bottomSheetVM)
        Utilities.presentPopover(view: bottomSheet, height: PlanBenefitsBottomSheetView.height(forModel: bottomSheetVM))
    }
    
    func viewPan(_ actionUrl: String?) {
        if actionUrl != nil {
            EventTracking.shared.eventTracking(name: .recoClick, [.location: "Mobile FD Activation", .type: "Sales", .product: "Laptop", .service: "ADLD", .mode: FDCacheManager.shared.fdTestType.rawValue])
            DeepLinkManager.handleLink(URL(string: "https://www.oneassist.in/common/purchasemobileprotectionplan?category=PE&product=LAP"))
        }
    }
    func viewDetailsOfPlan(_ planCD: PlanCD?){
        if let planCD = planCD, let address = self.serviceAddress {
            self.routeToRegistration(planDetail: planCD, pinCode: address.pincode)
        }
    }
}


// MARK:- Configuration Logic
extension ServiceRatingVC {
    func setupDependencyConfigurator() {
        let interactor = ServiceRatingInteractor()
        let presenter = ServiceRatingPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
    
    func routeToRegistration(planDetail: PlanCD?, pinCode: String?) {
        if let plan = planDetail {
            EventTracking.shared.eventTracking(name: .afContentView, [.price: planDetail?.price ?? 0,.contentId: planDetail?.planCode ?? 0, .currency: Constants.SchemeVariables.currency,.contentType: Utilities.getProductAndServicesString(plans: [plan])])
        }
        EventTracking.shared.eventTracking(name: .selectPlan, eventDictForPlan(planDetail))
        let customer = getPlanCustomerInfo(planDetail: planDetail, pincode: pinCode)
        displayPlanCustomerInfo(customer: customer, plan: planDetail)
    }
    
    func eventDictForPlan(_ plan: PlanCD?) -> [Event.EventKeys: String] {
        guard let plan = plan else { return [:] }
        return [.location : Destination.whc.title, .premiumAmount: "\(plan.price)", .service: plan.serviceList(), .coverAmount : "\(plan.coverAmount)", .numberOFappliance : "\(plan.allowedMaxQuantity)", .screenName : Event.EventParams.exploreHA]
    }
    
    func getPlanCustomerInfo(planDetail: PlanCD?, pincode:String?) -> CreateCustomerRequestDTO {
        let customerInfo = CustomerInfo()
        customerInfo.mobileOs = Constants.AppConstants.osType
        customerInfo.firstName = CustomerDetailsCoreDataStore.currentCustomerDetails?.firstName
        customerInfo.lastName = CustomerDetailsCoreDataStore.currentCustomerDetails?.lastName
        customerInfo.emailId = CustomerDetailsCoreDataStore.currentCustomerDetails?.email
        customerInfo.mobileNumber = userMobileNumber
        customerInfo.relationship = "self"
        customerInfo.assetInfo = nil
        
        let orderInfo = CustomerOrderInfo()
        orderInfo.partnerBUCode = Constants.SchemeVariables.partnerBuCode
        orderInfo.partnerCode = Constants.SchemeVariables.partnerCode
        orderInfo.planCode = planDetail?.planCode.description
        orderInfo.paymentMode = Constants.AppConstants.online
        
        let addressInfo = CustomerAddressInfo()
        addressInfo.addrType = "INSPECTION"
        addressInfo.pinCode = pincode
        
        let customerPaymentInfo = CustomerPaymentInfo()
        customerPaymentInfo.paymentMode = PaymentConstants.PaymentMode.online.rawValue
        
        let newCustomer = CreateCustomerRequestDTO()
        newCustomer.customerInfo = [customerInfo]
        newCustomer.orderInfo = orderInfo
        newCustomer.addressInfo = [addressInfo]
        newCustomer.initiatingSystem = 21
        newCustomer.paymentInfo = customerPaymentInfo
        
        return newCustomer
    }
    
    func displayPlanCustomerInfo(customer: CreateCustomerRequestDTO, plan: PlanCD?) {
        
        let eventDict = ["TYPE": Destination.whc.title,
                     "SERVICE": plan?.serviceList() ?? "",
                     "COVER_AMOUNT": plan?.coverAmount ?? "",
                     "PREMIUM_AMOUNT": plan?.price ?? "",
                     "ADDON_SERVICE": "",
                     "PLAN": plan?.planName ?? "",
                     "ProductService": Utilities.getProductAndServicesString(plans: [plan])] as [String : Any]
        guard let requestDict = customer.createRequestParameters()?.compactMapValues({ $0 }) else { return }
        
        guard let eventData = try? JSONSerialization.data(withJSONObject: eventDict, options: []) else { return }
        guard let requestData = try? JSONSerialization.data(withJSONObject: requestDict, options: []) else { return }
        
        guard let eventText = String(data: eventData, encoding: .utf8) else { return }
        guard let reuqestText = String(data: requestData, encoding: .utf8) else {return}
        
        
        let paymentVC = OAPaymentVC()
        paymentVC.addToInitialProperties(["data": reuqestText, "service_Type": "HS", "hasEW": plan?.hasEW() ?? false, "hasCreditScore": plan?.hasCreditScore() ?? false, "eventDict": eventText, "isTrialPlan": plan?.trial ?? false, "isFreeReward": false])
        navigationController?.pushViewController(paymentVC, animated: true)
    }
}
