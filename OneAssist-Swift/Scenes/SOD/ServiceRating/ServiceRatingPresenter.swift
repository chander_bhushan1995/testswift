//
//  ServiceRatingPresenter.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

protocol ServiceRatingDisplayLogic: class {
    func displayError(_ error:String)
    func displayPlanAndRating(model: ServiceRatingModel)
    func displayRatingSubmitted(indexPath: IndexPath)
}

class ServiceRatingPresenter: ServiceRatingPresentationLogic {
    
    weak var viewController: ServiceRatingDisplayLogic?
    
    func presentPlanData(serviceRequestType:String,serviceReqID: String, productCode: String, dataCD: [PlanCD]?, couponCodeResponse: CouponsResponseDTO?, homeApplianceCategoryResponseDTO: HomeApplianceCategoryResponseDTO?,subscriptionBenefits: SubscriptionBenefitsResponseDTO?, error: Error?) {
        var model = ServiceRatingModel()
        var sections: [ServiceRatingSectionModel] = []
        var cells:[Any] = []
        model.planBenefitsBottomSheet = PlanBenefitsBottomSheetVM(title: "Benefits of Unlimited service for ", planBenefits: [])
        homeApplianceCategoryResponseDTO?.subCategories?.append(contentsOf: homeApplianceCategoryResponseDTO?.otherSubCategories?.subCategories ?? [])
        
        let headerVMHeading = homeApplianceCategoryResponseDTO?.subCategories?.filter({$0.subCategoryCode == productCode}).first?.subCategoryName ?? "" + (serviceRequestType == Constants.Services.breakdown.rawValue ? Strings.SODConstants.repairText : Strings.SODConstants.repairText)
        
        let headerVM = RatingHeaderViewModel(imageRightText: Strings.ServiceRating.serviceCompleted, headingText: headerVMHeading, descriptionText: "", image: #imageLiteral(resourceName: "result_success"), backgroundColor: .violet)
        model.headerData = headerVM
        
        cells.append(RatingCellVM(currentRating: 5.0, heading: Strings.ServiceRating.overAllExp, ratingTitle: Strings.ServiceRating.rateScale, description: Strings.ServiceRating.ratingOnAppStore, primaryButtonText: Strings.MembershipDetailsFraudDetection.submit, isHideRatingViewContainer: false, primaryButtonTag: RatingCellPrimaryBtnTags.submitBtnTag, isVisibleTagList: false))
        
        let customerName = UserCoreDataStore.currentUser?.userName ?? "" + ", "
        cells.append(TitleSubtitleCellVM(heading: "\(customerName) We know frequent appliance breakdowns are a reality.",description: "For that we bring you Unlimited Service Plans."))
        
        if let couponCodes = couponCodeResponse,error == nil, (dataCD?.count ?? 0 > 0) {
            //coupon code for WHC means HA
            var heading: NSMutableAttributedString? = nil
            if let data = couponCodes.coupons?.first(where: {$0.category == Constants.Category.homeAppliances}), let _ = data.coupondCode, let _ = data.percentDiscount {
                let text = NSMutableAttributedString(string: "Get \(data.percentDiscount ?? 0)% OFF. Use code: \(data.coupondCode ?? "") on payment", attributes: [.font : DLSFont.supportingText.regular, .foregroundColor: UIColor.white])
                text.setAttributes([.font : DLSFont.supportingText.bold,NSAttributedString.Key.foregroundColor: UIColor.white], range: (text.string as NSString).range(of: data.coupondCode ?? ""))
                text.setAttributes([.font : DLSFont.supportingText.bold,NSAttributedString.Key.foregroundColor: UIColor.white], range: (text.string as NSString).range(of: "\(data.percentDiscount ?? 0)% OFF"))
                heading = text
            }
            
            // minimum cost plan
            let plan = dataCD?.min(by: { (prevPlan, currentPlan) -> Bool in
                return currentPlan.price > prevPlan.price
            })
            
            if let plan = plan {
                let productNames =  plan.productServices?.compactMap({ (data) -> String? in
                    if let service = data as? ProductServices {
                        return homeApplianceCategoryResponseDTO?.subCategories?.filter({$0.subCategoryCode == service.productCode}).first?.subCategoryName
                    }
                    return nil
                })
                
                var headingForPlanCollection = "Unlimited service for "
                if productNames?.count == 1 {
                    headingForPlanCollection += String(describing: productNames?[0] ?? "")+" "
                }else if productNames?.count == 2 {
                    headingForPlanCollection += (", "+String(describing: productNames?[0] ?? ""))+" "
                }else{
                    headingForPlanCollection += "& more "
                }
                headingForPlanCollection += "at ₹\(plan.price)/year"
                let primaryButtonText = "Buy @ \(plan.price)/year"
                var benefits = subscriptionBenefits?.planBenefits?.filter({$0.category == "HA"}).filter({($0.isShow == true)}).first?.data ?? []
                if let benefitsData = (plan.benefits?.allObjects as? [PlanBenefitsCD])?.sorted(by: { $0.rank < $1.rank }).compactMap({$0.benefit}), !benefits.isEmpty {
                    benefits.append(contentsOf: benefitsData)
                }
                
                let planCellVM = PlanCollectionViewCellVM(heading: headingForPlanCollection, image: #imageLiteral(resourceName: "homeServ"), primaryButtonText: primaryButtonText, listItems: benefits, listItemsImageSize: CGSize(width: 12,height: 12), listItemsImage: #imageLiteral(resourceName: "ic_resultPass"), badgeText: "Best Value", planCD: plan)
                
                cells.append(PlanContainerCellVM(attributedString: heading, cells: [planCellVM], backgroundColors: (top: .gradientOrangeDark,bottom: .orangeHighContrast)))
            }
        }
        
        var section = ServiceRatingSectionModel()
        section.cells = cells
        sections.append(section)
        model.sections = sections
        self.viewController?.displayPlanAndRating(model: model)
    }
    
    func mirrorFlowRatingData(response: MirrorFlowRatingRespModel?){
        var model = ServiceRatingModel()
        var sections: [ServiceRatingSectionModel] = []
        var cells:[Any] = []
        
        let header = response?.data?.header
        model.headerData = RatingHeaderViewModel(imageRightText: header?.title, headingText: "", descriptionText: header?.subTitle, image: #imageLiteral(resourceName: "result_success"), backgroundColor: .seaGreen)
        let tags = response?.data?.feedback_tags?.compactMap({RatingTagVM(tagText: $0, isSelected: false)}) ?? []
        cells.append(RatingCellVM(currentRating: 5.0, heading: Strings.ServiceRating.overAllExp, ratingTitle: Strings.ServiceRating.rateScale, description: Strings.ServiceRating.ratingOnAppStore, primaryButtonText: Strings.MembershipDetailsFraudDetection.submit, isHideRatingViewContainer: false, primaryButtonTag: RatingCellPrimaryBtnTags.submitBtnTag, tags: tags, isVisibleTagList: true))
        
        if let offerText = response?.data?.offer_data, let personalInfo = offerText.offer_Text {
            let customerName = UserCoreDataStore.currentUser?.userName ?? ""
            cells.append(TitleSubtitleCellVM(heading: String.init(format: personalInfo, customerName),description: offerText.offer_desc))
        }
        
        
        let planCellVM = PlanCollectionViewCellVM(response?.data?.plans?.first)
        
        var planCollectionHeader: NSMutableAttributedString? = nil
        if let code = response?.data?.offer_data?.offer_code, let discount = response?.data?.offer_data?.offer_dis {
            let offerText = String(format: Strings.MirrorFlowRating.discount, discount,code)
            let text = NSMutableAttributedString(string: offerText, attributes: [.font : DLSFont.h3.regular, .foregroundColor: UIColor.white])
            text.setAttributes([.font : DLSFont.h2.bold,NSAttributedString.Key.foregroundColor: UIColor.white], range: (offerText as NSString).range(of: discount))
            text.setAttributes([.font : DLSFont.h2.bold,NSAttributedString.Key.foregroundColor: UIColor.white], range: (offerText as NSString).range(of: code))
            text.setAttributes([.font : DLSFont.h2.bold,NSAttributedString.Key.foregroundColor: UIColor.white], range: (offerText as NSString).range(of: "% OFF."))
            planCollectionHeader = text
        }
        cells.append(PlanContainerCellVM(attributedString: planCollectionHeader, cells: [planCellVM], backgroundColors: (top: .gradientBlueStart,bottom: .gradientBlueEnd)))
        
        var section = ServiceRatingSectionModel()
        section.cells = cells
        sections.append(section)
        model.sections = sections
        self.viewController?.displayPlanAndRating(model: model)
    }
    
    
    func ratingSubmitted(response: SubmitFeedbackResponseDTO?, indexPath: IndexPath) {
        self.viewController?.displayRatingSubmitted(indexPath: indexPath)
    }
    
    func displayError(_ error: Error) {
        self.viewController?.displayError(error.localizedDescription)
    }
}
