//
//  InspectionSuccessPresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionSuccessDisplayLogic: class {
    
}

class InspectionSuccessPresenter: BasePresenter, InspectionSuccessPresentationLogic {
    weak var viewController: InspectionSuccessDisplayLogic?
    
    // MARK: Presentation Logic Conformance
    
}
