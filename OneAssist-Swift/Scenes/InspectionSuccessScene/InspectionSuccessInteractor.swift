//
//  InspectionSuccessInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionSuccessPresentationLogic {
    
}

class InspectionSuccessInteractor: BaseInteractor, InspectionSuccessBusinessLogic {
    var presenter: InspectionSuccessPresentationLogic?
    
    // MARK: Business Logic Conformance
    
}
