//
//  InspectionSuccessVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/12/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol InspectionSuccessBusinessLogic {
    
}

class InspectionSuccessVC: BaseVC, HideNavigationProtocol {
    var interactor: InspectionSuccessBusinessLogic?
    
    @IBOutlet weak var labelHeading: H2BoldLabel!
    @IBOutlet weak var labelSubHeading: H3RegularGreyLabel!

    @IBOutlet weak var buttonGotIt: OAPrimaryButton!
    // MARK:- Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupDependencyConfigurator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDependencyConfigurator()
    }
    
    // MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
    }
    
    // MARK:- private methods
    private func initializeView() {
        labelHeading.text = Strings.InspectionSuccessScene.heading

//        labelSubHeading.font = DLSFont.h3.bold
        labelSubHeading.text = Strings.InspectionSuccessScene.subHeading

        buttonGotIt.setTitle(Strings.InspectionSuccessScene.secondaryButtonAction, for: .normal)
    }
    
    // MARK:- Action Methods
    @IBAction func clickedBtnCross(_ sender: Any) {
        routeToHome()
    }
    
    @IBAction func clickedButtonGotIt(_ sender: Any) {
        routeToHome()
    }
}

// MARK:- Display Logic Conformance
extension InspectionSuccessVC: InspectionSuccessDisplayLogic {
    
}

// MARK:- Configuration Logic
extension InspectionSuccessVC {
    
    fileprivate func setupDependencyConfigurator() {
        let interactor = InspectionSuccessInteractor()
        let presenter = InspectionSuccessPresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension InspectionSuccessVC {
    func routeToHome() {
        setTabAsRootNew()
    }
}
