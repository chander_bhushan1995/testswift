//
//  LoginMobilePresenter.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol LoginMobileDisplayLogic: class
{
    func displayError(_ error: String)
	func routeToVerifyOTP(response: LoginMobileResponseDTO)
}

class LoginMobilePresenter: BasePresenter, LoginMobilePresentationLogic
{
	weak var viewController: LoginMobileDisplayLogic?
	
	// MARK: Presentation Logic Conformance
	
    func responseRecieved(response: LoginMobileResponseDTO?, error: Error?) {
        do {
            try checkError(response, error: error)
            
            // Success case
            viewController?.routeToVerifyOTP(response: response!)
        } catch let error as LogicalError {
            viewController?.displayError(error.errorDescription!)
        }
        catch {
            fatalError("Please use Logical Error Struct to throw any Error in OneAssist App")
        }
    }
}
