//
//  LoginMobileVC.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit
import StoreKit

protocol LoginMobileBusinessLogic {
	func doVerifyMobile(loginRequest: String)
}

class LoginMobileVC: BaseVC, LoginMobileDisplayLogic {
	var interactor: LoginMobileBusinessLogic?
	var editNumber: Bool = false
    var loginFor: String?
	@IBOutlet weak var buttonVerify: OAPrimaryButton!
	@IBOutlet weak var textFieldViewMobile: TextFieldView!
    
    var handler: TextFieldViewDelegateHandler!
    var isRootToWindow: Bool = false
    var showBackAction: Bool = false
    
    var loginActionHandler: ((Bool) -> Void)? = nil //use this when login from somewhere else
    var mobileNumber = ""
	
	// MARK: Object lifecycle
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
	{
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		setupDependencyConfigurator()
	}
	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
		setupDependencyConfigurator()
	}
	
	// MARK: View lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		initializeView()  //for setting the variables
        if let logoutSuccess = AppUserDefaults.logoutSuccess,
           logoutSuccess == true {
            Utilities.topMostPresenterViewController.view.makeToast(Strings.ToastMessage.logoutSuccess)
            AppUserDefaults.logoutSuccess = false
        }
       // Utilities.clearDataOnly()
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventTracking.shared.addScreenTracking(with: .EnterMobileNumber)
    }
    
	override func viewDidAppear(_ animated: Bool) {
            if editNumber {
			textFieldViewMobile.textFieldObj.becomeFirstResponder()
			editNumber = false
		}
        EventTracking.shared.eventTracking(name: .enterNumberScreenview, [:])
    }
	
	// MARK: Helper
    
    func setUpLoginAction(block: @escaping (Bool) -> Void) {
        self.loginActionHandler = block
    }
    
    private func addNavigationbarActions() {
        if(!isRootToWindow || showBackAction) {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navigateBack"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(dismissVC))
        }
        
        if(loginActionHandler == nil){
            if(!showBackAction){
                let barButtonItem = UIBarButtonItem()
                barButtonItem.setTitleTextAttributes([
                    NSAttributedString.Key.font : DLSFont.supportingText.medium,
                    NSAttributedString.Key.foregroundColor : UIColor.bodyTextGray,
                    ], for: .normal)
                barButtonItem.setTitleTextAttributes([
                    NSAttributedString.Key.font : DLSFont.supportingText.medium,
                    NSAttributedString.Key.foregroundColor : UIColor.bodyTextGray,
                    ], for: .selected)
                barButtonItem.title = Strings.LoginMobileScene.skip
                barButtonItem.target = self
                barButtonItem.action = #selector(onClickSkipVC)
                self.navigationItem.rightBarButtonItem = barButtonItem
            }
        }
    }
	
	private func initializeView() {
        title = Strings.LoginMobileScene.title
        addNavigationbarActions()
		addTap()
        buttonVerify.setTitle(Strings.LoginMobileScene.mobileAction, for: .normal)
		initializeTextFieldView()
	}
	
	private func initializeTextFieldView() {
		
        buttonVerify.isEnabled = false
		textFieldViewMobile.fieldType = MobileFieldTypeNoMinChar.self
        handler = TextFieldViewDelegateHandler(requiredFields: [textFieldViewMobile]) {[weak self] (isValid,_) in
            self?.buttonVerify.isEnabled = isValid
        }
		textFieldViewMobile.descriptionText = Strings.LoginMobileScene.textFieldMobileDescription
        textFieldViewMobile.placeholderFieldText = Strings.LoginMobileScene.textFieldMobilePlaceholder
		textFieldViewMobile.prefixText = Strings.LoginMobileScene.textFieldMobilePrefix
        
        textFieldViewMobile.fieldText = mobileNumber
        if mobileNumber.count == MobileFieldType.maxCharacterLength {
            buttonVerify.isEnabled = true
        }
	}
    
    private func validateForm() throws {
        try Validation.validateMobileNumber(text: textFieldViewMobile.fieldText)
    }
    
	// MARK: Action Methods
	
	@IBAction func clickedBtnVerify(_ sender: Any) {
        do {
            try validateForm()
            buttonVerify.addLoader()
            EventTracking.shared.eventTracking(name: .VerifyMobileNumber, [.location: loginFor ?? "Login Screen"], withGI: true)
            interactor?.doVerifyMobile(loginRequest:textFieldViewMobile.fieldText ?? "")

        } catch let exception as ValidationError {
            switch exception {
            case .invalidMobile(let error):
                textFieldViewMobile.setError(error)
            default:
                break
            }
        } catch {}
	}
	
    @objc func dismissVC() {
        if showBackAction {
            UserDefaultsKeys.custType.set(value: CustType.notVerified)
            if DeepLinkManager.isLoginRequired() {
                currentDeeplink = nil
            }
            self.routeToTabbar()
        } else {
            if self.isPresentVC {
                self.dismiss(animated: true, completion: nil);
            } else {
                navigationController?.popViewController(animated: true)
            }
            if DeepLinkManager.isLoginRequired() {
                currentDeeplink = nil
            }
        }
    }
    
    @objc func onClickSkipVC() {
        self.showAlertWithCrossAction(title: Strings.LoginMobileScene.skipAlertTitle, message: Strings.LoginMobileScene.skipAlertMessage, primaryButtonTitle: Strings.LoginMobileScene.skipButtonText, primaryAction: { [unowned self] in
            UserDefaultsKeys.custType.set(value: CustType.notVerified)
            if DeepLinkManager.isLoginRequired() {
                currentDeeplink = nil
            }
            self.routeToTabbar()
            EventTracking.shared.eventTracking(name: .loginSkip, [:])
        }, { })
    }

	// MARK: Display Logic Conformance
	
    func displayError(_ error: String) {
        buttonVerify.removeLoader()
        showAlert(title: Strings.Global.error, message: error)
    }
	
    func routeToVerifyOTP(response: LoginMobileResponseDTO) {
        buttonVerify.removeLoader()
        self.openVerifyOTP(response: response)
    }
}

// MARK:- Configuration Logic
extension LoginMobileVC {
    fileprivate func setupDependencyConfigurator() {
        let interactor = LoginMobileInteractor()
        let presenter = LoginMobilePresenter()
        self.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = self
    }
}

// MARK:- Routing Logic
extension LoginMobileVC {
    
    func openVerifyOTP(response: LoginMobileResponseDTO) {
        let vc = VerifyOTPVC()
        vc.loginFor = self.loginFor ?? "Login Screen"
        vc.loginActionHandler = self.loginActionHandler
        vc.dataSource = response
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func routeToTabbar() {
        setTabAsRoot()
    }
}

