//
//  LoginMobileInteractor.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/07/17.
//  Copyright (c) 2017 Mukesh. All rights reserved.
//

import UIKit

protocol LoginMobilePresentationLogic {
    func responseRecieved(response: LoginMobileResponseDTO?, error: Error?)
}

class LoginMobileInteractor: BaseInteractor, LoginMobileBusinessLogic
{
    var presenter: LoginMobilePresentationLogic?
    
    // MARK: Business Logic Conformance
    
    func doVerifyMobile(loginRequest: String) {
        let request = LoginMobileRequestDTO(mobileNo: loginRequest)
        let _ = LoginRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
            response?.mobileNo = loginRequest
            self?.presenter?.responseRecieved(response: response, error: error)
        }
    }
}
