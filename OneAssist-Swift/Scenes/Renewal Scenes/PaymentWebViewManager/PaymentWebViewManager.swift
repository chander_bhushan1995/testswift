//
//  PaymentWebViewManager.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 23/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class PaymentWebViewManager {
    
    func initializeView(membershipDetail: PlanDetailAndSRModel?, addressInfo: WHCPincodeResponseDTO?, completionHandler: @escaping ((_ url: String?, _ error: String?) -> ())) {
        let obj: RenewalPaymentRequestDTO = RenewalPaymentRequestDTO()
        
        if let addressInfo = addressInfo {
            let addrInfo = CustomerAddressInfo()
            addrInfo.addrType = Strings.Global.inspection
            
            if let custAddr = addressInfo.data?.customerAddress?.first {
                addrInfo.cityCode = custAddr.cityCode
                addrInfo.pinCode = custAddr.pincode
                addrInfo.stateCode = custAddr.stateCode
            } else if let custAddr = addressInfo.data {
                addrInfo.cityCode = custAddr.cityCode
                addrInfo.pinCode = custAddr.pincode
                addrInfo.stateCode = custAddr.stateCode
            }
            
            obj.addressInfo = [addrInfo]
        }
        
        let customerInfo = CustomerInfo()
        customerInfo.emailId = membershipDetail?.customer?[0].emailId
        customerInfo.firstName = membershipDetail?.customer?[0].firstName
        customerInfo.mobileNumber = membershipDetail?.customer?[0].mobileNumber?.stringValue
        customerInfo.previousCustId = membershipDetail?.customer?[0].custId?.stringValue
        customerInfo.relationship = membershipDetail?.customer?[0].relationship
        
        obj.customerInfo = [customerInfo]
        obj.initiatingSystem = "21"
        obj.membershipId = membershipDetail?.membershipId
        
        let orderInfo = CustomerOrderInfo()
        orderInfo.partnerBUCode = Constants.SchemeVariables.partnerBuCode
        orderInfo.partnerCode = Constants.SchemeVariables.partnerCode
        orderInfo.planCode = membershipDetail?.planCode?.stringValue
        
        obj.orderInfo = orderInfo
        
        let _ = RenewalPaymentRequestUseCase.service(requestDTO:obj) { (service, response, error) in
            do {
                try BasePresenter().checkError(response, error: error)
                completionHandler(response?.data?.payNowLink, nil)
            } catch {
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
}
