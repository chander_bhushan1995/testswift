//
//  ServiceRequestCard.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 14/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol ServiceRequestCardDelegate: class {
    /* For History timeline. */
    func clickedButtonViewDetails(_ cell: ServiceRequestCard, clickedBtnViewDetails sender: Any)
    
    /* For Ongoing and History*/
    func clickedTextArrowButtonView(_ cell: ServiceRequestCard, clickedTextArrowBtnView sender: Any)
    /* For Ongoing */
    func clickedButtonSRView(_ cell: ServiceRequestCard, clickedBtnSRView sender: Any)
    
    /* For History */
    func clickedButtonCall(_ cell: ServiceRequestCard, clickedBtnCall sender: Any)
    
    func clickedDownloadInvoice(_ cell: ServiceRequestCard, clickedBtnDownloadInvoice sender: Any)
}

extension ServiceRequestCardDelegate {
    /* For History timeline. */
    func clickedButtonViewDetails(_ cell: ServiceRequestCard, clickedBtnViewDetails sender: Any){}
    
    /* For Ongoing and History*/
    func clickedTextArrowButtonView(_ cell: ServiceRequestCard, clickedTextArrowBtnView sender: Any){}
    /* For Ongoing */
    func clickedButtonSRView(_ cell: ServiceRequestCard, clickedBtnSRView sender: Any){}
    
    /* For History */
    func clickedButtonCall(_ cell: ServiceRequestCard, clickedBtnCall sender: Any){}
    
    func clickedDownloadInvoice(_ cell: ServiceRequestCard, clickedBtnDownloadInvoice sender: Any){}
}

class ServiceRequestCard: UITableViewCell,ReuseIdentifier,NibLoadableView {
    
    @IBOutlet weak var appliancesNameLabel: H3BoldLabel!
    @IBOutlet weak var moreAppliancesButton: OASecondaryButton!
    
    // It is expected service closure in case of ongoing and service id in history.
    @IBOutlet weak var serviceReqInfoLabel: SupportingTextRegularGreyLabel!
    
    @IBOutlet weak var membershipInfoView: MembershipInfoView!
    @IBOutlet weak var membershipActionView: MembershipActionView!
    
    @IBOutlet weak var viewDetailsButton: OASecondaryButton!
    @IBOutlet weak var downloadInvoiceButton: OASecondaryButton!
    
    @IBOutlet weak var applianceNameLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet var memInfoViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var viewDetailsButtonTopConstraint: NSLayoutConstraint!
    
    weak var delegate: ServiceRequestCardDelegate?
    var assetNames: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        membershipInfoView.delegate = self
        membershipActionView.delegate = self
        downloadInvoiceButton.titleLabel?.font = DLSFont.supportingText.bold
        
        /* Here you can access UI elements and set their properties. */
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        showServiceRequestInfo()
        showViewDetailsButton()
        moreAppliancesButton.isHidden = false
        assetNames = []
    }
    
    private func hideServiceRequestInfo() {
        serviceReqInfoLabel.isHidden = true
        memInfoViewTopConstraint.isActive = false
    }
    
    private func showServiceRequestInfo() {
        serviceReqInfoLabel.isHidden = false
        memInfoViewTopConstraint.isActive = true
    }
    
    private func hideViewDetailsButton() {
        viewDetailsButton.isHidden = true
        viewDetailsButtonTopConstraint.isActive = false
    }
    
    private func showViewDetailsButton() {
        viewDetailsButton.isHidden = false
        viewDetailsButtonTopConstraint.isActive = true
    }
    
    @IBAction func moreButtonTapped(_ sender: Any) {
        showBenefitsList(items: assetNames)
    }
    
    @IBAction func viewDetailsClicked(_ sender: Any) {
        delegate?.clickedButtonViewDetails(self, clickedBtnViewDetails: sender)
    }
    
    @IBAction func onClickDownloadInvoice(_ sender: Any){
        delegate?.clickedDownloadInvoice(self, clickedBtnDownloadInvoice: sender)
    }
    
    func setDraftSRViewModel() {
        assetNames = []
        applianceNameLabelHeightConstraint.constant = 0
        appliancesNameLabel.isHidden = true
        moreAppliancesButton.isHidden = true
        hideServiceRequestInfo()
        hideViewDetailsButton()
        
        membershipInfoView.setupUI(with: .textArrowButtonCard)
        membershipInfoView.setupTextArrowWithMemStatusCard(with: Strings.Global.resumeRaiseClaimText, and: nil, and: UIColor.backgroundInProgress, and: Strings.Global.resumeClaim)
        
        membershipActionView.setupUI(with: .noCard)
        
        downloadInvoiceButton.isHidden = true
    }
    
    func setOngoingViewModel(with serviceRequest: SRListCompletedViewModel) {
        
        assetNames = serviceRequest.assets
        
        applianceNameLabelHeightConstraint.constant = 21
        if assetNames.count == 1 {
            appliancesNameLabel.text = assetNames.first ?? ""
            moreAppliancesButton.isHidden = true
        } else if assetNames.count > 1 {
            appliancesNameLabel.text = "\(assetNames.first ?? ""), ..."
            moreAppliancesButton.setTitle("+\(assetNames.count - 1) more", for: .normal)
        }
        
        let startDate = Date(string: serviceRequest.thirdPartyProperties?.eddFromDate, formatter: .scheduleFormat)?.string(with: .dayMonth)
        
        let endDate = Date(string: serviceRequest.thirdPartyProperties?.eddToDate, formatter: .scheduleFormat)?.string(with: .dayMonth)
        
        if startDate != nil && endDate != nil {
            let dateString = String(format: Strings.ServiceRequest.expectedClouser, startDate!, endDate!)//"Expected service closure between \(startDate!) - \(endDate!)"
            serviceReqInfoLabel.text = dateString
        } else {
            hideServiceRequestInfo()
        }
        
        hideViewDetailsButton()
        
        membershipInfoView.setupOnGoingAndCompletedSRCard(with: serviceRequest)
        membershipActionView.setupUI(with: .noCard)
        
        downloadInvoiceButton.isHidden = !serviceRequest.hasDownloadInvoiceButton
    }
    
    func setHistoryViewModel(with serviceRequest: HistoryModel) {
        
        assetNames = serviceRequest.assets
        
        applianceNameLabelHeightConstraint.constant = 21
        if assetNames.count == 1 {
            appliancesNameLabel.text = assetNames.first ?? ""
            moreAppliancesButton.isHidden = true
        } else if assetNames.count > 1 {
            appliancesNameLabel.text = "\(assetNames.first ?? ""), ..."
            moreAppliancesButton.setTitle("+\(assetNames.count - 1) more", for: .normal)
        }
        
        serviceReqInfoLabel.text = serviceRequest.subtitle
        
        downloadInvoiceButton.isHidden = !serviceRequest.hasDownloadInvoiceButton
        
        membershipInfoView.setupUI(with: .textArrowButtonCard)
        
        if serviceRequest.status == .rejected || serviceRequest.status == .cancelled {
            
            membershipInfoView.setupTextArrowWithMemStatusCard(with: serviceRequest.detailStatus, and: nil, and: UIColor.backgroundError, and: Strings.Global.viewDetails, and: nil, and: nil)
            
            membershipActionView.setupUI(with: .callCard)
            
            hideViewDetailsButton()
        } else if serviceRequest.status == .completed {
            membershipInfoView.setupTextArrowWithMemStatusCard(with: serviceRequest.detailStatus, and: nil, and: UIColor.backgroundSuccess, and: Strings.Global.viewDetails, and: nil, and: nil)
            
            membershipActionView.setupUI(with: .noCard)
            
            hideViewDetailsButton()
        } else if serviceRequest.status == .completedRatingRequired {
            membershipInfoView.setupTextArrowWithMemStatusCard(with: serviceRequest.detailStatus, and: nil, and: UIColor.backgroundSuccess, and: Strings.Global.rateUs, and: nil, and: nil)
            
            membershipActionView.setupUI(with: .noCard)
        }
    }
    
}

extension ServiceRequestCard: MembershipInfoDelegate {
    func clickedTextArrowButtonView(_ view: MembershipInfoView, clickedTextArrowBtnView sender: Any) {
        delegate?.clickedTextArrowButtonView(self, clickedTextArrowBtnView: sender)
    }
    
    func clickedButtonSRView(_ view: MembershipInfoView, clickedBtnSRView sender: Any) {
        delegate?.clickedButtonSRView(self, clickedBtnSRView: sender)
    }
}

extension ServiceRequestCard: MembershipActionDelegate {
    func clickedButtonCall(_ view: MembershipActionView, clickedBtnCall sender: Any) {
        delegate?.clickedButtonCall(self, clickedBtnCall: sender)
    }
}

extension ServiceRequestCard{
    
    fileprivate func showBenefitsList(items:[String]){
        guard items.count > 0 else { return }
        let mv = ScrollableMenuView()
        mv.titleLabel.text = Strings.Global.yourAppliances
        // Heading is required
        mv.items = items
        mv.image = #imageLiteral(resourceName: "small_dot")
        let contentH = min(Utilities.topMostPresenterViewController.view.bounds.height * 0.75, CGFloat(items.count) * 80.0)
        Utilities.presentPopover(view: mv, height: contentH)
    }
}
