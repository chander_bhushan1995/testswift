
import UIKit

class OADownloadTask: NSObject {
    
    internal var totalBytesWritten: Int64 = 0
    internal var totalBytesExpectedToWrite: Int64 = 0
    internal var isDownloading = false
    
    var url: NSURL?
    var urlString: String?
    var destination: String?
    var fileName: String?
    var downloadError: NSError?
    var lastErrorMessage: String?
    var completed: Bool = false
    var cachedProgress: Float = 0
    var identifier: String?
    var position: Int = -1
    
    // MARK: - INIT
    init(urlString: String, destination:String, totalBytesExpectedToWrite:Int64) {
        super.init()
        self.url = NSURL(string: urlString)
        self.commonInit(urlString: urlString, destination: destination, totalBytesExpectedToWriteInput: totalBytesExpectedToWrite)
    }
    
    init(url: NSURL, destination:String, totalBytesExpectedToWrite:Int64) {
        super.init()
        self.url = url
        self.commonInit(urlString: url.absoluteString ?? "", destination: destination, totalBytesExpectedToWriteInput: totalBytesExpectedToWrite)
    }
    
    // MARK: - INSTANCE METHODS
    func commonInit(urlString: String, destination:String, totalBytesExpectedToWriteInput:Int64) {
        self.urlString = urlString
        self.completed = false
        self.totalBytesWritten = 0
        self.totalBytesExpectedToWrite = totalBytesExpectedToWriteInput
        
        self.destination = destination
        self.fileName = (destination as NSString).lastPathComponent
        self.prepareFolderForDestination()
    }
    
    func prepareFolderForDestination() {
        let absoluteDestinationPath = self.absoluteDestinationPath()
        let containerFolderPath = (absoluteDestinationPath as NSString).deletingLastPathComponent
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: containerFolderPath) == false {
            do {
                try fileManager.createDirectory(atPath: containerFolderPath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Create Directory Error: \(error.localizedDescription)")
            } catch {
                print("Create Directory Error - Something went wrong")
            }
        }
        
        if fileManager.fileExists(atPath: absoluteDestinationPath) {
            if self.verifyDownload() {
                self.cachedProgress = 1
                // retain file - this task has been completed
            } else {
                self.cleanUp()
            }
        } else {
            self.cleanUp()
        }
    }
    
    func absoluteDestinationPath() -> String {
        let documentDictionary = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        return "\(documentDictionary!)/\(self.destination!)"
    }
    
    func verifyDownload() -> Bool {
        let fileManager = FileManager.default
        let absoluteDestinationPath = self.absoluteDestinationPath()
        if fileManager.fileExists(atPath: absoluteDestinationPath) == false {
            return false
        }
        
        var isVerified = false
        
        do {
            let fileAttributes = try fileManager.attributesOfItem(atPath: absoluteDestinationPath)
            let fileSize = (fileAttributes[FileAttributeKey.size] as! NSNumber).int64Value
            isVerified = fileSize == self.totalBytesExpectedToWrite
        } catch let error as NSError {
            print("Error Received \(error.localizedDescription)")
        } catch {
            print("Error Received")
        }
        
        if isVerified {
            self.completed = true
            self.cachedProgress = 1
        } else {
            self.totalBytesWritten = self.totalBytesExpectedToWrite
        }//end else
        
        return isVerified
    }
    
    func cleanUp() {
        self.completed = false
        self.downloadError = nil
        self.totalBytesWritten = 0
        self.cachedProgress = 0
        self.deleteDestinationFile()
    }
    
    func cleanUpWithResumableData(data: NSData) {
        self.completed = false
        self.totalBytesWritten = Int64(data.length)
        self.deleteDestinationFile()
        self.downloadError = nil
    }
    
    func deleteDestinationFile() {
        let fileManager = FileManager.default
        let absoluteDestinationPath = self.absoluteDestinationPath()
        if fileManager.fileExists(atPath: absoluteDestinationPath) {
            do {
                try fileManager.removeItem(atPath: absoluteDestinationPath)
            } catch let error as NSError {
                print("Removing Existing File Error: \(error.localizedDescription)")
            }
        }
    }
    
    func isHittingErrorBecauseOffline() -> Bool {
        if let _ = self.downloadError, let unwrappedLastErrorMessage = self.lastErrorMessage {
            return unwrappedLastErrorMessage.contains("(Code \(URLError.notConnectedToInternet))") || unwrappedLastErrorMessage.contains("(Code \(URLError.networkConnectionLost))")
        } else {
            return false
        }
    }
    
    func isHittingErrorConnectingToServer() -> Bool {
        if let unwrappedLastErrorMessage = self.lastErrorMessage {
            return unwrappedLastErrorMessage.contains("(Code \(URLError.redirectToNonExistentLocation))") || unwrappedLastErrorMessage.contains("(Code \(URLError.badServerResponse))") ||
                unwrappedLastErrorMessage.contains("(Code \(URLError.zeroByteResource))") || unwrappedLastErrorMessage.contains("(Code \(URLError.timedOut))")
        } else {
            return false
        }
    }
    
    func captureReceivedError(error:NSError) {
        self.downloadError = error
    }
    
    func fullErrorDescription() -> String {
        if let unwrappedError = self.downloadError {
            let errorCode = unwrappedError.code
            return "Downloading URL %@ failed because of error: \(self.urlString) (Code \(errorCode))"
        } else {
            return "No Error"
        }
    }
    
    func getURL() -> NSURL {
        return self.url!
    }
    
    func downloadingProgress() -> Float {
        if self.completed {
            return 1
        }//end if
        
        if self.totalBytesExpectedToWrite > 0 {
            return Float(self.totalBytesWritten) / Float(self.totalBytesExpectedToWrite)
        } else {
            return 0
        }
    }
}
