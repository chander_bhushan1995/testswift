
import UIKit
import SSZipArchive

enum OADownloadTaskStatus: Int {
    case BinaryUnits = 1
    case OSNativeUnits = 2
    case LocalizedFormat = 4
}

protocol OADownloadBatchDelegate: NSObject {
    func receiveError(task: OADownloadTask)
}

class OADownloadBatch: NSObject {
    
    private var downloadInputs: [OADownloadTask] = [OADownloadTask]()
    private var urls: [String] = [String]()
    private var session: URLSession?
    private var numberOfBytesDownloadedSinceStart: Int = 0
    private var startTime: NSDate?
    
    internal var completed: Bool = false
    
    var requestTimeoutSeconds: Double = 90 // timeout for particular download task request
    weak var delegate: OADownloadBatchDelegate?
    
    // MARK: - REQUIRED INIT
    required init(delegate: OADownloadBatchDelegate) {
        super.init()
        self.delegate = delegate
    }
    
    @discardableResult
    func addTask(taskInfo: [String: Any]) -> OADownloadTask? {
        var urlString:String?
        
        assert(taskInfo["url"] != nil, "Task Info's URL must not be present")
        assert(taskInfo["url"] is NSURL || taskInfo["url"] is String, "Task Info's URL must be NSURL or String")
        assert(taskInfo["destination"] != nil, "Task Info's Destination must be present")
        assert(taskInfo["destination"] is String, "Task Info's Destination must be a String")
        
        if let url = taskInfo["url"] as? NSURL {
            urlString = url.absoluteString
        } else if let url = taskInfo["url"] as? String {
            urlString = url
        }//end else
        
        let destination = taskInfo["destination"] as! String
        var totalExpectedToWrite:Int64 = 0
        if let fileSize = taskInfo["fileSize"] as? Int {
            totalExpectedToWrite = Int64(fileSize)
        }//end if
        if let unwrappedURLString = urlString {
            if self.isTaskExistWithURL(urlString: unwrappedURLString) == false {
                let downloadTask = OADownloadTask(urlString: urlString!, destination: destination, totalBytesExpectedToWrite: totalExpectedToWrite)
                
                if let identifier = taskInfo["identifier"] as? String {
                    downloadTask.identifier = identifier
                }//end if
                downloadTask.position = self.downloadInputs.count
                
                urls.append(unwrappedURLString)
                downloadInputs.append(downloadTask)
                
                return downloadTask
            }
        }
        return nil
    }
    
    func isTaskExistWithURL(urlString: String) -> Bool {
        return self.urls.firstIndex(of: urlString) != nil
    }
    
    func handleDownloadFileAt(downloadFileLocation: URL, forDownloadURL: String) -> Bool {
        if let downloadTask = self.downloadInfoOfTaskUrl(url: forDownloadURL) {
            let absoluteDestinationPath = downloadTask.absoluteDestinationPath()
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: absoluteDestinationPath) {
                do {
                    try fileManager.removeItem(atPath: absoluteDestinationPath)
                } catch {
                    print("Something went wrong when removing the file")
                    return false
                }
            }
            do {
                try fileManager.moveItem(atPath: downloadFileLocation.path, toPath: absoluteDestinationPath)
            } catch let error as NSError {
                puts(error.description)
                return false
            }
            
            downloadTask.isDownloading = false
            let isVerified = downloadTask.verifyDownload()
            if isVerified {
                self.updateCompleteStatus()
                if let url = URL(string: absoluteDestinationPath), url.pathExtension == "zip" {
                    handleZipFileExtraction(toFile: absoluteDestinationPath)
                }
            }//end if
            return isVerified
        }
        
        return false
    }
    
    func updateCompleteStatus() {
        for task in downloadInputs {
            if task.completed == false {
                self.completed = false
                return
            }
        }
        
        self.completed = true
    }
    
    func handleZipFileExtraction(toFile: String) {
        let fileManager = FileManager.default
        let containerFolderPath = (toFile as NSString).deletingLastPathComponent
        let success: Bool = SSZipArchive.unzipFile(atPath: toFile,
                                                   toDestination: containerFolderPath,
                                                   preserveAttributes: true,
                                                   overwrite: true,
                                                   nestedZipLevel: 0,
                                                   password: nil,
                                                   error: nil,
                                                   delegate: nil,
                                                   progressHandler: nil,
                                                   completionHandler: nil)
        do {
            try fileManager.removeItem(atPath: toFile)
        } catch {
            print("Something went wrong when removing the file")
        }
        
        if success != false {
            print("Success unzip")
        } else {
            print("No success unzip")
            return
        }
    }
    
    func startDownloadTask(downloadTask: OADownloadTask) {
        let request = NSMutableURLRequest(url: downloadTask.getURL() as URL)
        if downloadTask.totalBytesExpectedToWrite == 0 {
            self.requestForTotalBytesForURL(url: downloadTask.getURL(), callback: { (totalBytes) -> () in
                downloadTask.totalBytesExpectedToWrite = totalBytes
                self.downloadRequest(request: request, task: downloadTask)
            })
        } else {
            self.downloadRequest(request: request, task: downloadTask)
        }
    }
    
    func requestForTotalBytesForURL(url: NSURL, callback: @escaping (_ totalBytes:Int64) -> ()) {
        let headRequest = NSMutableURLRequest(url: url as URL)
        headRequest.setValue("", forHTTPHeaderField: "Accept-Encoding")
        headRequest.httpMethod = "HEAD"
        
        let sharedSession = URLSession.shared
        let headTask = sharedSession.dataTask(with: headRequest as URLRequest) { (data, response, error) -> Void in
            let responseStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            if (responseStatusCode >= 200 && responseStatusCode < 300) {
                if let expectedContentLength = response?.expectedContentLength {
                    callback(expectedContentLength)
                } else {
                    callback(-1)
                }
            }else{
                let task = self.downloadInputs.filter({$0.urlString == url.absoluteString}).first
                if let task = task {
                    task.downloadError = NSError(domain: NSPOSIXErrorDomain, code: URLError.badURL.rawValue, userInfo: ["message":"url is not reachable"])
                    self.delegate?.receiveError(task: task)
                }
            }
        }
        headTask.resume()
    }
    
    func downloadRequest(request: NSMutableURLRequest, task: OADownloadTask) {
        request.timeoutInterval = requestTimeoutSeconds
        if let unwrappedSession = self.session {
            if let error = task.downloadError {
                let downloadTask = unwrappedSession.downloadTask(withResumeData: (error.userInfo[NSURLSessionDownloadTaskResumeData] as! NSData) as Data)
                task.isDownloading = true
                downloadTask.resume()
                task.downloadError = nil
            } else {
                let downloadTask = unwrappedSession.downloadTask(with: request as URLRequest)
                task.isDownloading = true
                downloadTask.resume()
            }
        }
    }
    
    func redownloadRequestOfTask(task: OADownloadTask) {
        if let error = task.downloadError {
            if let resumableData = error.userInfo[NSURLSessionDownloadTaskResumeData] as? NSData {
                if let unwrappedSession = self.session {
                    let downloadTask = unwrappedSession.downloadTask(withResumeData: resumableData as Data)
                    task.cleanUpWithResumableData(data: resumableData)
                    downloadTask.resume()
                }
            }
        } else {
            let request = NSMutableURLRequest(url: task.getURL() as URL)
            request.timeoutInterval = requestTimeoutSeconds
            if let unwrappedSession = self.session {
                let downloadTask = unwrappedSession.downloadTask(with: request as URLRequest)
                task.cleanUp()
                downloadTask.cancel()
                self.startDownloadTask(downloadTask: task)
            }
        }
    }
    
    func totalBytesWrittenAndReceived() -> [String: Int64] {
        var totalDownloadedBytes:Int64 = 0
        var totalBytesExpectedToReceived:Int64 = 0
        for task in downloadInputs {
            totalDownloadedBytes += task.totalBytesWritten
            totalBytesExpectedToReceived += task.totalBytesExpectedToWrite
        }//end for
        
        return ["totalDownloadedBytes": totalDownloadedBytes, "totalToBeReceivedBytes": totalBytesExpectedToReceived]
    }
    
    func setDownloadingSession(inputSession: URLSession?) {
        self.startTime = NSDate()
        self.session = inputSession
    }
    
    func continuteAllInCompleteDownloadTask() {
        for task in downloadInputs {
            if task.completed == false {
                self.startDownloadTask(downloadTask: task)
            }
        }
    }
    
    func resumeAllSuspendedTasks() {
        if let unwrappedSession = session {
            unwrappedSession.getTasksWithCompletionHandler({ (dataTasks, uploadTasks, downloadTasks) -> Void in
                for downloadTask in downloadTasks {
                    if let urlString = downloadTask.originalRequest?.url?.absoluteString {
                        if let _ = self.downloadInfoOfTaskUrl(url: urlString) {
                            if downloadTask.state == URLSessionTask.State.suspended {
                                downloadTask.resume()
                            }
                        }
                    }
                }
            })
        }
    }
    
    func suspendAllOngoingDownloadTasks() {
        if let unwrappedSession = session {
            unwrappedSession.getTasksWithCompletionHandler({ (dataTasks, uploadTasks, downloadTasks) -> Void in
                for downloadTask in downloadTasks {
                    if let urlString = downloadTask.originalRequest?.url?.absoluteString {
                        if let _ = self.downloadInfoOfTaskUrl(url: urlString) {
                            if downloadTask.state == URLSessionTask.State.running {
                                downloadTask.suspend()
                            }
                        }
                    }
                }
            })
        }
    }
    
    func downloadInfoOfTaskUrl(url: String) -> OADownloadTask? {
        if let indexOfObject = urls.firstIndex(of: url) {
            return downloadInputs[Int(indexOfObject)]
        } else {
            return nil
        }
    }
    
    func downloadRate() -> Int64 {
        let rate = Int64(Double(numberOfBytesDownloadedSinceStart) / self.elapsedSeconds())
        return rate
    }
    
    func elapsedSeconds() -> Double {
        let now = NSDate()
        if let unwrappedStartTime = startTime {
            let distanceBetweenDates = now.timeIntervalSince(unwrappedStartTime as Date)
            return distanceBetweenDates
        } else {
            return 0
        }
    }
    
    func isDownloading() -> Bool {
        return session != nil
    }
    
    func downloadObjects() -> [OADownloadTask] {
        return self.downloadInputs
    }
    
    func captureDownloadingInfoOfDownloadTask(downloadTask: URLSessionDownloadTask) -> OADownloadTask? {
        if let url = downloadTask.originalRequest?.url {
            if let downloadTaskInfo = self.downloadInfoOfTaskUrl(url: url.absoluteString) {
                downloadTaskInfo.totalBytesWritten = downloadTask.countOfBytesReceived
                if downloadTaskInfo.totalBytesExpectedToWrite == 0 {
                    downloadTaskInfo.totalBytesExpectedToWrite = downloadTask.countOfBytesExpectedToReceive
                }
                return downloadTaskInfo
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func updateProgressOfDownloadURL(url: String, progressPercentage: Float, totalBytesWritten: Int64) -> OADownloadTask? {
        if let downloadTask = self.downloadInfoOfTaskUrl(url: url) {
            numberOfBytesDownloadedSinceStart += Int(totalBytesWritten - downloadTask.totalBytesWritten)
            downloadTask.totalBytesWritten = totalBytesWritten
            return downloadTask
        }//end if
        
        return nil
    }
}
