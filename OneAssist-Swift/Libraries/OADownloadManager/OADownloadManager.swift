
import UIKit
import Reachability
import ReachabilityManager
import UserNotifications

// MARK: - OADownloadManagerDataDelegate
protocol OADownloadManagerDataDelegate: AnyObject {
    // required protocol functions
    func didFinishAllForDataDelegate()
    
    // optional protocol functions
    func didFinishDownloadTask(downloadTask:OADownloadTask)
}


// MARK: - OADownloadManagerUIDelegate
protocol OADownloadManagerUIDelegate: AnyObject {
    // required protocol functions
    func didFinishAll()
    
    // optional protocol functions
    func didReachProgress(progress:Float)
    func didHitDownloadErrorOnTask(task: OADownloadTask)
    func didFinishOnDownloadTaskUI(task: OADownloadTask)
    func didReachIndividualProgress(progress: Float, onDownloadTask:OADownloadTask)
    func didFinishBackgroundDownloading()
    func didStartDownloading()
}

// MARK: - OADownloadManagerDataDelegate default Implementation
extension OADownloadManagerDataDelegate {
    func didFinishDownloadTask(downloadTask:OADownloadTask) {}
}

// MARK: - OADownloadManagerUIDelegate default Implementation
extension OADownloadManagerUIDelegate {
    func didReachProgress(progress:Float) {}
    func didHitDownloadErrorOnTask(task: OADownloadTask) {}
    func didFinishOnDownloadTaskUI(task: OADownloadTask) {}
    func didReachIndividualProgress(progress: Float, onDownloadTask:OADownloadTask) {}
    func didFinishBackgroundDownloading() {}
    func didStartDownloading() {}
}

// MARK: - TFLiteModelsDownloadManager
class OADownloadManager: NSObject {
    
    private var currentBatch:OADownloadBatch?
    private var initialDownloadedBytes:Int64 = 0
    private var totalBytes:Int64 = 0
    private var internetReachability:Reachability?
    
    var requestTimeoutSeconds: Double = 90
    var session: URLSession!
    var backgroundCompletionHandler: (() -> Void)?
    var onConnectionLost: (()->())? = nil
    var onConnectionAppear: (()->())? = nil
    
    weak var dataDelegate:OADownloadManagerDataDelegate?
    weak var uiDelegate:OADownloadManagerUIDelegate?
    
    static let sharedInstance = OADownloadManager()
    
    private override init() {
        super.init()
        
        let configuration = URLSessionConfiguration.background(withIdentifier: Constants.SessionIDs.modelDownloadSessionID)
        configuration.sessionSendsLaunchEvents = true
        session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        self.listenToInternetConnectionChange()
    }
    
    func setInitialDownloadBytes(initialDownloadedBytes:Int64) {
        self.initialDownloadedBytes = initialDownloadedBytes
    }
    
    func setTotalBytes(totalBytes:Int64) {
        self.totalBytes = totalBytes
    }
    
    func isDownloading() -> Bool {
        if let unwrappedCurrentBatch = currentBatch {
            return unwrappedCurrentBatch.completed == false
        } else {
            return false
        }
    }
    
    @discardableResult
    func addBatch(arrayOfDownloadInformation:[[String: Any]]) -> [OADownloadTask] {
        let batch = OADownloadBatch(delegate: self)
        batch.requestTimeoutSeconds = requestTimeoutSeconds
        for downloadTask in arrayOfDownloadInformation {
            batch.addTask(taskInfo: downloadTask)
        }
        self.currentBatch = batch
        return batch.downloadObjects()
    }
    
    func downloadingTasks() -> [OADownloadTask] {
        if let unwrappedCurrentBatch = self.currentBatch {
            return unwrappedCurrentBatch.downloadObjects()
        } else {
            return [OADownloadTask]()
        }
    }
    
    func downloadRateAndRemainingTime() -> (downloadRate: String, remainingTime: String, remainingTimeInSeconds: Int64)? {
        if let unwrappedCurrentBatch = currentBatch {
            let rate = unwrappedCurrentBatch.downloadRate()
            let downloadRate = String(format: "%@/s", ByteCountFormatter.string(fromByteCount: rate, countStyle: ByteCountFormatter.CountStyle.file))
            let remainingTimeInSeconds = self.remainingTimeGivenDownloadingRate(downloadRate: rate)
            let remainingTime = self.formatTimeFromSeconds(numberOfSeconds: remainingTimeInSeconds)
            return (downloadRate: downloadRate, remainingTime: remainingTime, remainingTimeInSeconds: remainingTimeInSeconds)
        } else {
            return nil
        }
    }
    
    func remainingTimeGivenDownloadingRate(downloadRate:Int64) -> Int64 {
        if downloadRate == 0 {
            return 0
        }
        
        var actualTotalBytes:Int64 = 0
        if let currentBatchUnwrapped = currentBatch {
            let bytesInfo = currentBatchUnwrapped.totalBytesWrittenAndReceived()
            if totalBytes == 0 {
                actualTotalBytes = bytesInfo["totalToBeReceivedBytes"]!
            } else {
                actualTotalBytes = totalBytes
            }
            let actualDownloadedBytes = bytesInfo["totalDownloadedBytes"]! + initialDownloadedBytes
            let timeRemaining:Float = Float(actualTotalBytes - actualDownloadedBytes) / Float(downloadRate)
            return Int64(timeRemaining)
        }
        
        return 0
    }
    
    func formatTimeFromSeconds(numberOfSeconds:Int64) -> String {
        if numberOfSeconds == 0 {
            return ""
        }
        let seconds = numberOfSeconds % 60
        let minutes = (numberOfSeconds / 60) % 60
        let hours = (numberOfSeconds / 3600)
        var remainingTimeString = " "
        
        if hours > 0 {
            remainingTimeString.append("\(String(format: "%02lld", hours)) hours ")
        }
        
        if minutes > 0 {
            remainingTimeString.append("\(String(format: "%02lld", minutes)) mins ")
        }
        
        // to show 0 seconds in case hours & mins left 0
        if seconds > 0 || (hours >= 0 && minutes >= 0) {
            remainingTimeString.append("\(String(format: "%02lld", seconds)) secs ")
        }
        
        return remainingTimeString
    }
    
    
    func downloadBatch(downloadInformation:[[String: Any]]) {
        self.addBatch(arrayOfDownloadInformation: downloadInformation)
        self.startDownloadingCurrentBatch()
    }
    
    func startDownloadingCurrentBatch() {
        if let currentBatchUnwrapped = currentBatch {
            self.startADownloadBatch(batch: currentBatchUnwrapped)
            uiDelegate?.didStartDownloading()
        }
    }
    
    func addDownloadTask(task:[String: Any]) -> OADownloadTask? {
        if self.currentBatch == nil {
            currentBatch = OADownloadBatch.init(delegate: self)
            currentBatch?.requestTimeoutSeconds = requestTimeoutSeconds
        }//end if
        
        if let downloadTaskInfo = self.currentBatch!.addTask(taskInfo: task) {
            if downloadTaskInfo.completed {
                self.processCompletedDownload(task: downloadTaskInfo)
                self.postToUIDelegateOnIndividualDownload(task: downloadTaskInfo)
            } else if(currentBatch!.isDownloading()) {
                currentBatch!.startDownloadTask(downloadTask: downloadTaskInfo)
            }
            
            currentBatch!.updateCompleteStatus()
            if let unwrappedUIDelegate = self.uiDelegate {
                DispatchQueue.main.async {
                    unwrappedUIDelegate.didReachProgress(progress: self.overallProgress())
                }
            }
            if currentBatch!.completed {
                self.postCompleteAll()
            }
            
            return downloadTaskInfo
        }
        
        return nil
    }
    
    func overallProgress() -> Float {
        if let unwrappedCurrentBatch = currentBatch {
            var actualTotalBytes:Int64 = 0
            let bytesInfo = unwrappedCurrentBatch.totalBytesWrittenAndReceived()
            if totalBytes == 0 {
                actualTotalBytes = bytesInfo["totalToBeReceivedBytes"]!
            } else {
                actualTotalBytes = totalBytes
            }//end else
            
            let actualDownloadedBytes = bytesInfo["totalDownloadedBytes"]! + initialDownloadedBytes
            if actualTotalBytes == 0 {
                return 0
            }//end if
            
            let progress = Float(actualDownloadedBytes) / Float(actualTotalBytes)
            return progress
        } else {
            return 0
        }//end else
    }
    
    func continueIncompletedDownloads() {
        if let unwrappedCurrentBatch = currentBatch {
            unwrappedCurrentBatch.resumeAllSuspendedTasks()
        }
    }
    
    func suspendAllOngoingDownloads() {
        if let unwrappedCurrentBatch = currentBatch {
            unwrappedCurrentBatch.suspendAllOngoingDownloadTasks()
        }
    }
    
    func processCompletedDownload(task:OADownloadTask) {
        if let dataDelegateUnwrapped = self.dataDelegate {
            dataDelegateUnwrapped.didFinishDownloadTask(downloadTask: task)
        }
        
        if let uiDelegateUnwrapped = self.uiDelegate {
            DispatchQueue.main.async {
                uiDelegateUnwrapped.didFinishOnDownloadTaskUI(task: task)
            }
        }
        
        if let currentBatchUnwrapped = currentBatch, currentBatchUnwrapped.completed {
            self.postCompleteAll()
        }
    }
    
    func postCompleteAll() {
        if let dataDelegateUnwrapped = self.dataDelegate {
            dataDelegateUnwrapped.didFinishAllForDataDelegate()
        }
        
        if let uiDelegateUnwrapped = self.uiDelegate {
            DispatchQueue.main.async {
                uiDelegateUnwrapped.didFinishAll()
            }
        }
    }
    
    func startADownloadBatch(batch:OADownloadBatch) {
        let session = OADownloadManager.sharedInstance.session
        batch.setDownloadingSession(inputSession: session)
        session?.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) -> Void in
            for task in batch.downloadObjects() {
                var isDownloading = false
                let url = task.getURL()
                for downloadTask in downloadTasks {
                    if url.absoluteString == downloadTask.originalRequest?.url?.absoluteString {
                        if let downloadTaskInfo = batch.captureDownloadingInfoOfDownloadTask(downloadTask: downloadTask) {
                            self.postToUIDelegateOnIndividualDownload(task: downloadTaskInfo)
                            isDownloading = true
                            if downloadTask.state == URLSessionTask.State.suspended {
                                downloadTask.resume()
                            }//end if
                        }
                    }
                }//end for
                if task.completed == true {
                    self.processCompletedDownload(task: task)
                    self.postToUIDelegateOnIndividualDownload(task: task)
                } else if isDownloading == false {
                    batch.startDownloadTask(downloadTask: task)
                }
            }//end for
            
            batch.updateCompleteStatus()
            if let uiDelegateUnwrapped = self.uiDelegate {
                DispatchQueue.main.async {
                    uiDelegateUnwrapped.didReachProgress(progress: self.overallProgress())
                }
            }
            if batch.completed {
                self.postCompleteAll()
            }
        }
    }
    
    func postProgressToUIDelegate() {
        if let uiDelegateUnwrapped = self.uiDelegate {
            DispatchQueue.main.async {
                let overallProgress = self.overallProgress()
                uiDelegateUnwrapped.didReachProgress(progress: overallProgress)
            }
        }
    }
    
    func postToUIDelegateOnIndividualDownload(task:OADownloadTask) {
        if let uiDelegateUnwrapped = self.uiDelegate {
            DispatchQueue.main.async {
                task.cachedProgress = task.downloadingProgress()
                uiDelegateUnwrapped.didReachIndividualProgress(progress: task.cachedProgress, onDownloadTask: task)
            }
        }
    }
    
    func postDownloadErrorToUIDelegate(task:OADownloadTask) {
        if let uiDelegateUnwrapped = self.uiDelegate {
            DispatchQueue.main.async {
                uiDelegateUnwrapped.didHitDownloadErrorOnTask(task: task)
            }
        }
    }
    
    func cancelAllOutStandingTasks() {
        OADownloadManager.sharedInstance.session.getAllTasks { (tasks) in
            tasks.forEach({$0.cancel()})
        }
    }
    
    func updateBatchCompleteStatus() { // update batch completed forcefully
        currentBatch?.completed = true
    }
}

// MARK: - LISTENER FOR INTERNET CONNECTION
extension OADownloadManager {
    
    func listenToInternetConnectionChange() {
        do {
            self.internetReachability = try Reachability.init()
        } catch {
            print("Unable to create Reachability")
            return
        }
        
        self.internetReachability?.whenReachable = { reachability in
            DispatchQueue.main.async {
                self.continueIncompletedDownloads()
                if let closure = self.onConnectionAppear {
                    closure()
                }
            }
        }
        
        self.internetReachability?.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                if let closure = self.onConnectionLost {
                    closure()
                }
            }
        }
        
        do {
            try self.internetReachability?.startNotifier()
        } catch {
            print("Unable to satrt notifier")
        }
    }
}


// MARK: - URLSessionDownloadDelegate
extension OADownloadManager: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let unwrappedError = error {
            if let downloadURL = task.originalRequest?.url?.absoluteString, let unwrappedCurrentBatch = currentBatch {
                if let downloadTaskInfo = unwrappedCurrentBatch.downloadInfoOfTaskUrl(url: downloadURL) {
                    downloadTaskInfo.captureReceivedError(error: unwrappedError as NSError)
                    currentBatch?.redownloadRequestOfTask(task: downloadTaskInfo)
                    self.postDownloadErrorToUIDelegate(task: downloadTaskInfo)
                }
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("bytes written \(bytesWritten) progress = \(Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))")
        if let downloadURL = downloadTask.originalRequest?.url?.absoluteString, let currentBatchUnwrapped = currentBatch {
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            if let downloadTaskInfo = currentBatchUnwrapped.updateProgressOfDownloadURL(url: downloadURL, progressPercentage: progress, totalBytesWritten: totalBytesWritten) {
                self.postProgressToUIDelegate()
                self.postToUIDelegateOnIndividualDownload(task: downloadTaskInfo)
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("didfinish called with url \(location)")
        if let downloadURL = downloadTask.originalRequest?.url?.absoluteString, let currentBatchUnwrapped = self.currentBatch {
            if let downloadTask = currentBatchUnwrapped.downloadInfoOfTaskUrl(url: downloadURL) {
                let finalResult = currentBatchUnwrapped.handleDownloadFileAt(downloadFileLocation: location, forDownloadURL: downloadURL)
                if finalResult {
                    self.processCompletedDownload(task: downloadTask)
                } else {
                    downloadTask.cleanUp()
                    currentBatchUnwrapped.startDownloadTask(downloadTask: downloadTask)
                    self.postProgressToUIDelegate()
                }
            }
        }
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            self.backgroundCompletionHandler?()
            self.backgroundCompletionHandler = nil
            self.uiDelegate?.didFinishBackgroundDownloading()
        }
    }
}


// MARK: - OADownloadBatchDelegate
extension OADownloadManager: OADownloadBatchDelegate {
    
    func receiveError(task:OADownloadTask) {
        if let uiDelegateUnwrapped = self.uiDelegate {
            DispatchQueue.main.async {
                uiDelegateUnwrapped.didHitDownloadErrorOnTask(task: task)
            }
        }
    }
}
