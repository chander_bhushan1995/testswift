//
//  File.swift
//  OneAssist-Swift
//
//  Created by Raj on 17/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation
class JSSerialization {
    
    static func getDictionaryFromArray(array: [Any]) -> [Any] {
        var resultingArray: [Any] = []
        
        for element in array {
            let elementValue = getValue(value: element)
            //            if !(elementValue is NSNull) {
            resultingArray.append(elementValue)
            //            }
        }
        
        return resultingArray
    }
    
    static func getDictionaryFromObject(object: Any) -> [String: Any] {
        
        var dictionary: [String: Any] = [:]
        
        
        
        var mirror: Mirror? = Mirror(reflecting: object)
        
        repeat {
            
            for property in mirror!.children {
                
                if let propertyName = property.label {
                    
                    if propertyName == "some" {
                        
                        var mirror: Mirror? = Mirror(reflecting: property.value)
                        
                        repeat {
                            
                            for childProperty in mirror!.children {
                                
                                if let propertyName = childProperty.label {
                                    
                                    if let value = property.value as? JSONKeyCoder {
                                        
                                        if let userDefinedKeyName = value.key(for: propertyName) {
                                            
                                            let elementValue = getValue(value: childProperty.value)
                                            
                                            //                                            if !(elementValue is NSNull) {
                                            
                                            dictionary[userDefinedKeyName] = elementValue
                                            
                                            //                                            }
                                            
                                        } else {
                                            
                                            let elementValue = getValue(value: childProperty.value)
                                            
                                            //                                            if !(elementValue is NSNull) {
                                            
                                            dictionary[propertyName] = elementValue
                                            
                                            //                                            }
                                            
                                        }
                                        
                                    } else {
                                        
                                        
                                        let elementValue = getValue(value: childProperty.value)
                                        
                                        //                                        if !(elementValue is NSNull) {
                                        
                                        dictionary[propertyName] = elementValue
                                        
                                        //                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            mirror = mirror?.superclassMirror
                            
                        } while mirror != nil
                        
                    } else {
                        
                        
                        
                        if let value = object as? JSONKeyCoder {
                            
                            if let userDefinedKeyName = value.key(for: propertyName) {
                                
                                let elementValue = getValue(value: property.value)
                                
                                //                                if !(elementValue is NSNull) {
                                
                                dictionary[userDefinedKeyName] = elementValue
                                
                                //                                }
                                
                            } else {
                                
                                let elementValue = getValue(value: property.value)
                                
                                //                                if !(elementValue is NSNull) {
                                
                                dictionary[propertyName] = elementValue
                                
                                //                                }
                                
                            }
                            
                        } else {
                            
                            
                            let elementValue = getValue(value: property.value)
                            
                            //                            if !(elementValue is NSNull) {
                            
                            dictionary[propertyName] = elementValue
                            
                            //                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
            mirror = mirror?.superclassMirror
            
        } while mirror != nil
        
        
        
        return dictionary
        
    }
    
    private static func getValue(value: Any) -> Any {
        if let numericValue = value as? NSNumber {
            return numericValue
        } else if let boolValue = value as? Bool {
            return boolValue
        } else if let stringValue = value as? String {
            return stringValue
        } else if let arrayValue = value as? Array<Any> {
            return getDictionaryFromArray(array: arrayValue)
        } else {
            let dictionary = getDictionaryFromObject(object: value)
            if dictionary.count == 0 {
                return NSNull()
            } else {
                return dictionary
            }
        }
    }
    
}
