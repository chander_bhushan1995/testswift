//
//  IDFenceSubscriptionStatusUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 10/12/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class IDFenceSubscriptionStatusRequestDTO: BaseRequestDTO {
    var memUUID = ""
    
    init(_ memUUID: String) {
        self.memUUID = memUUID
    }
}

class IDFenceSubscriptionStatusResponseDTO: BaseResponseDTO {
    var data: String?
}

class IDFenceSubscriptionStatusUseCase: BaseRequestUseCase<IDFenceSubscriptionStatusRequestDTO, IDFenceSubscriptionStatusResponseDTO> {
    
    let dataProvider = dataProviderFactory.walletDataProvider()
    
    override func getRequest(requestDto: IDFenceSubscriptionStatusRequestDTO?, completionHandler: @escaping (IDFenceSubscriptionStatusResponseDTO?, Error?) -> Void) {
        dataProvider.getSubscriptionStatusOfIDFence(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
    
}
