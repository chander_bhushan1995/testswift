//
//  PremiumMembershipSIStatusUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 05/11/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class MembershipSIStatusRequestDTO: BaseRequestDTO {
    var memUUID = ""
}

class MembershipSIStatusResponseDTO: BaseResponseDTO {
    var data: Task?
}


class PremiumMembershipSIStatusUseCase: BaseRequestUseCase<MembershipSIStatusRequestDTO, MembershipSIStatusResponseDTO> {
    
    let dataProvider = dataProviderFactory.walletDataProvider()
    
    override func getRequest(requestDto: MembershipSIStatusRequestDTO?, completionHandler: @escaping (MembershipSIStatusResponseDTO?, Error?) -> Void) {
        dataProvider.getSIStatusOfIDFencePremium(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
    
}
