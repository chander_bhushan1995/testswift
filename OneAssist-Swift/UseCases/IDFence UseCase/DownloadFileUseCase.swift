//
//  DownloadFileUseCase.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 20/02/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class DownloadRequestDTO: BaseRequestDTO {
    var downloadUrl = ""
    var destinationUrl = ""
    var fileName = ""
    var isSaveFile = false
}

class DownloadResponseDTO: BaseResponseDTO {
    var destinationUrl: String?
}


class DownloadFileUseCase : BaseRequestUseCase<DownloadRequestDTO, DownloadResponseDTO> {
    
    let dataProvider = dataProviderFactory.walletDataProvider()
    
    override func getRequest(requestDto: DownloadRequestDTO?, completionHandler: @escaping (DownloadResponseDTO?, Error?) -> Void) {
        if requestDto?.isSaveFile ?? false  { //using when save file after dowmload
            dataProvider.getRaiseClaimDocumentRequest(requestDto: requestDto, taskCallBack: { (request) in
                self.request = request
            }, completionHandlerBlock: completionHandler)
            
        }else {
            dataProvider.downloadFile(requestDto: requestDto, taskCallBack: { (request) in
                self.request = request
            }, completionHandlerBlock: completionHandler)
            
        }
    }
}
