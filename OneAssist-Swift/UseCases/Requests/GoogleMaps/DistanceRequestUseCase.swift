//
//  DistanceRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Raj on 08/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class GoogleDistanceResponseDTO: BaseResponseDTO {
    
    var destinationAddresses: [String]?
    var originAddresses: [String]?
    var rows: [DataRow]?
}

class DataRow: ParsableModel {
    
    var elements: [Element]?
}

class Element: ParsableModel {
    
    var distance: Distance?
    var duration: Distance?
    var status: String?
}
class Distance: ParsableModel {
    
    var text: String?
    var value: NSNumber?
}

class GoogleDistanceRequestDTO:BaseRequestDTO{
    var origins:String?
    var destinations:String?
    var key = "AIzaSyB0FdcgEJQG5tyraTbdNg_Zx1GxwTTcbZo"//"AIzaSyBLTBZiFCcqiYcS3v3wg0n4mzCo_erZIvw"
    init(origins:String?,destinations:String?){
        self.destinations = destinations
        self.origins = origins
    }
}

class GoogleDistanceRequestUseCase : BaseRequestUseCase<GoogleDistanceRequestDTO, GoogleDistanceResponseDTO> {
    
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GoogleDistanceRequestDTO?, completionHandler: @escaping (GoogleDistanceResponseDTO?, Error?) -> Void) {
    
        dataProvider.getServiceCenterDistance(requestDto: requestDto,taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler )
    }
    
    
}
