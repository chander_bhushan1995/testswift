//
//  WalletCardsUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 05/02/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class WalletCardsRequestDTO: BaseRequestDTO {
    
    let customerId: String
    
    init(customerId: String? = nil) {
        self.customerId = customerId ?? UserCoreDataStore.currentUser?.cusId ?? "0"
    }
}

class WalletCardsResponseDTO: BaseResponseDTO {
    var data: WalletCardsData?
}

class WalletCardsData: ParsableModel {
    var custId: NSNumber?
    var cards: [WalletCard]?
}

class WalletCard: ParsableModel {
    var walletId: NSNumber?
    var custId: NSNumber?
    var prodCode: String?
    var accountNo: NSNumber?
    var contentType: String?
    var nameOnCard: String?
    var issueDate: NSNumber?
    var issueMonth: NSNumber?
    var issueYear: NSNumber?
    var issuerCode: String?
    var issuerId: String?
    var association: String?
    var statusFlag: String?
    var createdOn: String?
    var lossReportId: NSNumber?
    var contentNo: String?
    var issuerName: String?
    var expiryDate: NSNumber?
    var expiryMonth: NSNumber?
    var expiryYear: NSNumber?
    
    var isBankCard: Bool {
        return ["CRC", "DBC"].contains(contentType?.uppercased())
    }
    
    var cardTypeId: Int {
        if "CRC" == contentType?.uppercased() {
            return 1
        } else if "DBC" == contentType?.uppercased() {
            return 2
        }
        
        return 0
    }
    
    var networkId: Int {
        var category = 0
        if contentNo?.count == 16 {
            let firstNumber = contentNo?.first
            if firstNumber == "4" {
                category = 1
            } else if firstNumber == "5" || firstNumber == "6" {
                category = 2
            }
        }
        
        return category
    }
}

class WalletCardsUseCase : BaseRequestUseCase<WalletCardsRequestDTO, WalletCardsResponseDTO> {
    
    let dataProvider = dataProviderFactory.walletDataProvider()
    
    override func getRequest(requestDto: WalletCardsRequestDTO?, completionHandler: @escaping (WalletCardsResponseDTO?, Error?) -> Void) {
        
        dataProvider.getWalletCards(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
