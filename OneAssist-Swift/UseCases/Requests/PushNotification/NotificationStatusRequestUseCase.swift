//
//  NotificationStatusRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 12/27/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class NotificationStatusRequestDTO: BaseRequestDTO {
    var commId: String
    
    init(commId: String) {
        self.commId = commId
    }
}

class NotificationStatusRequestUseCase: BaseRequestUseCase<NotificationStatusRequestDTO, BaseResponseDTO> {
    
    let dataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: NotificationStatusRequestDTO?, completionHandler: @escaping (BaseResponseDTO?, Error?) -> Void) {
        dataProvider.saveNotificationStatus(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
