//
//  DeviceRegistrationDetailsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 12/27/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class DeviceRegistrationDetailsRequestDTO: BaseRequestDTO {
    var appRegistrationDtl: AppRegistrationDetail
    
    init(custId: String?, registrationId: String?) {
        appRegistrationDtl = AppRegistrationDetail(custId: custId, registrationId: registrationId)
    }
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class AppRegistrationDetail {
    var custId: String?
    var apkVersion = UIApplication.appVersion
    var devModel = UIDevice.deviceModel
    var deviceId = UIDevice.uuid
    //var imeiNo = UIDevice.uuid
    var make = "Apple"
    var osVersion = UIDevice.deviceVersion
    var platform = "IOS"
    var registrationId: String?
    
    init(custId: String?, registrationId: String?) {
        self.custId = custId
        self.registrationId = registrationId
    }
}

class DeviceRegistrationDetailsRequestUseCase: BaseRequestUseCase<DeviceRegistrationDetailsRequestDTO, BaseResponseDTO> {
    
    let dataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: DeviceRegistrationDetailsRequestDTO?, completionHandler: @escaping (BaseResponseDTO?, Error?) -> Void) {
        dataProvider.saveDeviceRegistrationDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
