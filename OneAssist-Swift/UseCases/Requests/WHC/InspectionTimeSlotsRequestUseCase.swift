//
//  InspectionTimeSlotsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 28/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class InspectionTimeSlotsRequestDTO: BaseRequestDTO {
    
    var serviceRequestDate: String
    var pincode : String? 
    var serviceRequestType = Constants.PlanServices.whcInspection
    var serviceRequestSourceType: String?
    var memId: String?
    var assetId: String?
    var bpCode: String?
    var buCode: String?
    var productCode: String?
    var serviceRequestId: String?
    init(selectedDate: Date) {
        let dateStr = DateFormatter.slotDateFormat.string(from: selectedDate)
        self.serviceRequestDate = dateStr
    }
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class InspectionTimeSlotsResponseDTO: BaseResponseDTO {
    var data: SlotsData?
}

class SlotsData: ParsableModel {
    var serviceRequestDate: String?
    var serviceRequestType: String?
    var serviceSlots: [Slots]?
}

class Slots: ParsableModel {
    var startTime: String?
    var endTime: String?
}

class InspectionTimeSlotsRequestUseCase : BaseRequestUseCase<InspectionTimeSlotsRequestDTO, InspectionTimeSlotsResponseDTO> {
    
    let dataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: InspectionTimeSlotsRequestDTO?, completionHandler: @escaping (InspectionTimeSlotsResponseDTO?, Error?) -> Void) {
        dataProvider.getTimeSlots(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
