//
//  WHCInspectionUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 27/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class WHCInspectionRequestDTO:BaseRequestDTO {
    var requestId: String = ""

}

class WHCInspectionResponseDTO:BaseResponseDTO{
    var data:[WHCInspectionResponseModel]?
   }

class WHCInspectionResponseModel:ParsableModel
{
    var status:String?
    var createdOn: String?
    var createdBy: String?
    var modifiedOn: String?
    var modifiedBy: Bool?
    var serviceRequestId: String?
    var serviceRequestType: String?
    var serviceRequestTypeName:String?
    var referenceNo: String?
    var refPrimaryTrackingNo: String?
    var refSecondaryTrackingNo: String?
    var assignee: String?
    var scheduleSlotStartDateTime: String?
    var scheduleSlotEndDateTime: String?
    var dueDateTime: String?
    var actualStartDateTime: String?
    var actualEndDateTime: String?
    var servicePartnerCode: Int?
    var servicePartnerBuyCode:Int?
    var workflowProcessId: Int?
    var workflowData:WorkFlowModel?
    
}
class WorkFlowModel:ParsableModel
{
    var visit:[String:Any]?
}

class WHCInspectionUseCase: BaseRequestUseCase<WHCInspectionRequestDTO,WHCInspectionResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: WHCInspectionRequestDTO?, completionHandler: @escaping (WHCInspectionResponseDTO?, Error?) -> Void) {
        responseDataProvider.getWHCInspectionDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
