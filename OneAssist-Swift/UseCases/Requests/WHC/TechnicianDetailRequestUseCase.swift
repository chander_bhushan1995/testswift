//
//  TechnicianDetailRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 27/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class TechnicianDetailRequestDTO: BaseRequestDTO {

    var technicianId: String

    init(technicianId: String) {
        self.technicianId = technicianId
    }
}

class TechnicianDetailResponseDTO: BaseResponseDTO {

    init() {
        super.init(dictionary: [:])
    }
    required init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }

    var data : TechnicialDetail?
}

class TechnicialDetail : ParsableModel {

    var technicianId : String?
    var firstName : String?
    var middleName : String?
    var lastName : String?
    var mobileNumber : String?
    var emailId : String?
    var profileMongoId : String?
    var imageByteArray: String?
    var experience : String?
    var startOTP : String?
    var endOTP : String?
}

class TechnicianDetailRequestUseCase: BaseRequestUseCase<TechnicianDetailRequestDTO, TechnicianDetailResponseDTO> {

    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: TechnicianDetailRequestDTO?, completionHandler: @escaping (TechnicianDetailResponseDTO?, Error?) -> Void) {
        responseDataProvider.getTechnicianDetail(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
