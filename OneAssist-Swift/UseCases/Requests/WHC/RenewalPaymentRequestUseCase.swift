//
//  RenewalPaymentRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 31/01/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import Foundation

class PaymentInfo {
    
}

class RenewalPaymentRequestDTO: BaseRequestDTO {
    var addressInfo: [CustomerAddressInfo]?
    var customerInfo: [CustomerInfo]?
    var initiatingSystem: String?
    var membershipId: String?
    var orderInfo: CustomerOrderInfo?
    var paymentInfo: PaymentInfo?
}

class RenewalPaymentResponseDTO: BaseResponseDTO {
    var data: LinkInfo?
}

class LinkInfo: ParsableModel {
    var payNowLink: String?
}

class RenewalPaymentRequestUseCase: BaseRequestUseCase<RenewalPaymentRequestDTO, RenewalPaymentResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: RenewalPaymentRequestDTO?, completionHandler: @escaping (RenewalPaymentResponseDTO?, Error?) -> Void) {
        responseDataProvider.getRenewalPaymentResult(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
