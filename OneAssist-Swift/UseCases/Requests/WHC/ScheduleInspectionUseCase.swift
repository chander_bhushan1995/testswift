//
//  ScheduleInspectionUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 28/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class ScheduleInspectionRequestDTO: BaseRequestDTO {
    
    var postPaymentDetails: PostPaymentDetails?
    
    override func createRequestParameters() -> [String : Any]? {
        self.postPaymentDetails?.activationCode =  (self.postPaymentDetails?.activationCode ?? "").removingPercentEncoding
        return JSONParserSwift.getDictionary(object: self)
    }
}

class PostPaymentDetails {
    var customerDetails: [CustomerDetails]?
    var task: Task?
    var activationCode: String?
    var initiatingSystem: NSNumber = 21

}

class CustomerDetails {
    var pinCode: String?
    var relationshipCode: String?
    var stateCode: String?
    var city: String?
    var addrLine1: String?
    var gender: String?
    var assetInfo: String?
}

class ScheduleInspectionResponseDTO: BaseResponseDTO {
    
}

class ScheduleInspectionRequestUseCase: BaseRequestUseCase<ScheduleInspectionRequestDTO, ScheduleInspectionResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: ScheduleInspectionRequestDTO?, completionHandler: @escaping (ScheduleInspectionResponseDTO?, Error?) -> Void) {
        responseDataProvider.scheduleInspection(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
