//
//  WHCFeedbackUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation


class WHCFeedbackRequestDTO: BaseRequestDTO {
    var rating: String?
    var ratingType: String?
    
    init(_ rating:String, ratingType: Constants.RatingType?) {
        self.rating = rating
        self.ratingType = ratingType?.rawValue
    }
}

class WHCFeedbackResponseDTO: BaseResponseDTO {
    var data : [WHCFeedbackModel] = []
}

class WHCFeedbackModel : ParsableModel {
    var status:String?
    var createdOn:String?
    var createdBy:String?
    var modifiedOn:String?
    var modifiedBy:String?
    var valueId: NSNumber?
    var keySetId: NSNumber?
    var keySetName: String?
    var keySetDescription:String?
    var key:String?
    var value:String?
}

class WHCFeedbackUseCase:BaseRequestUseCase<WHCFeedbackRequestDTO, WHCFeedbackResponseDTO> {
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: WHCFeedbackRequestDTO?, completionHandler: @escaping (WHCFeedbackResponseDTO?, Error?) -> Void) {
        responseDataProvider.getWHCFeedbacks(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

class serviceRequestFeedback:ParsableModel
{
    var feedbackRating:String = ""
    var feedbackCode:String = ""
    var feedbackComments:String = ""
}

class SubmitFeedbackRequestDTO: BaseRequestDTO {
    var serviceReqId: String?
    var modifiedBy : String?
    var serviceRequestFeedback : ServiceRequestFeedback?
    
    override func createRequestParameters() -> [String : Any]? {
        var request = JSONParserSwift.getDictionary(object: self)
        
        request.removeValue(forKey: "serviceReqId")
        
        return request
    }
   
}

class SubmitFeedbackResponseDTO: BaseResponseDTO {

}

class ServiceRequestFeedback:ParsableModel {
    var feedbackRating : String?
    var feedbackCode : String?
    var feedbackComments : String?
}


class WHCSubmitFeedbackUseCase: BaseRequestUseCase <SubmitFeedbackRequestDTO, SubmitFeedbackResponseDTO> {

    let responseDataProvider = dataProviderFactory.whcDataProvider()

    override func getRequest(requestDto: SubmitFeedbackRequestDTO?, completionHandler: @escaping (SubmitFeedbackResponseDTO?, Error?) -> Void) {
        responseDataProvider.submitFeedback(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
