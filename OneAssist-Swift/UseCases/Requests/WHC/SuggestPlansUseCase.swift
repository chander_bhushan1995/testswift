//
//  SelectAppliancesUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation


class SuggestPlanRequestDTO: BaseRequestDTO {
    
    var isWhc = false
    
    var recommendationParam: [RecommendationParam]?
    var disjunctive: String?
    var priceType: String?
    var businessUnitType: [String]?
    var activity: String?
    var noOfCustomer: Int?
    var numberOfPlan: String?
    var planCode: String?
    var requireAllPlans: String?
    var brandName: String?
    var isSubscription: String?
    var trialPlanRequired: String?
    
    override func createRequestParameters() -> [String : Any]? {
        var dict = JSONParserSwift.getDictionary(object: self)
        dict.removeValue(forKey: "brandName")
        dict.removeValue(forKey: "isWhc")
        return dict
    }
}

class RecommendationParam {
    var invoiceDate: String?
    var invoiceValue: String?
    var productSpecifications: [NameValuePair]?
    var serviceList: [NameValuePair]?
    var productCode: String?
    var categoryCode: String?
}

class NameValuePair {
    
    var name: String?
    var value: String?
    
    init(name: String, value: String) {
        self.name = name
        self.value = value
    }
}

class SuggestPlanResponseDTO: BaseResponseDTO {
    
    init() {
        super.init(dictionary: [:])
    }
    
    required init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }
    
    var dataCD: [PlanCD]?
    var data:[SuggestPlans]?
}

class SuggestPlans: ParsableModel {
    var activationDefferedDays: NSNumber?
    var allowedMaxQuantity: NSNumber?
    var allowedUnits: NSNumber? // added in case of SOD
    var annuity: NSNumber?
    var coverAmount: NSNumber?
    var discountAllowed: NSNumber?
    var downgradeAllowed: NSNumber?
    var frequency: String?
    var planDuration: String?
    var graceDays: NSNumber?
    var isPremium: NSNumber?
    var maxInsuranceValue: NSNumber?
    var minInsuranceValue: NSNumber?
    var noOfCustomers: NSNumber?
    var planCode: NSNumber?
    var planDescription: String?
    var planName: String?
    var planStatus: String?
    var price: NSNumber?
    var anchorPrice: NSNumber?
    var priceType: NSNumber?
    var refundAllowed: NSNumber?
    var renewalWindowDays: NSNumber?
    var taxcode: String?
    var trial: NSNumber?
    var upgradeAllowed: NSNumber?
    var benefits: [PlanBenefits]?
    var excludedBenefits: [PlanBenefits]?
    var productServices: [PlanProductServices]?
    var planServiceAttributes: [PlanServiceAttribute]?
    var planStatistics: PlanStatistics?
    var paymentOps: [String]?
}

class PlanBenefits: ParsableModel {
    var benefitId: NSNumber?
    var benefit: String?
    var benefitValue: String?
    var shortName: String?
    var planCode: NSNumber?
    var rank: NSNumber?
}

class PlanProductServices: ParsableModel {
    var productCode: String?
    var serviceList: [String]?
}

class PlanServiceAttribute: ParsableModel {
    var attributeName: String?
    var attributeValue: String?
    var serviceName: String?
}

class PlanStatistics: ParsableModel {
    var plan_code: String?
    var rank: NSNumber?
    var metadata: StatisticsMetaData?
}

class StatisticsMetaData: ParsableModel {
    var membershipCount: NSNumber?
}

class SuggestPlansUseCase: BaseRequestUseCase<SuggestPlanRequestDTO, SuggestPlanResponseDTO> {
    
    private let dataProvider = dataProviderFactory.homeAppliancesDataProvider()
    
    override func getRequest(requestDto: SuggestPlanRequestDTO?, completionHandler: @escaping (SuggestPlanResponseDTO?, Error?) -> Void) {
        let apiCacheChecker = SuggestPlanCacheChecker()        
        apiCacheChecker.getCacheTimeDuration = (requestDto?.isWhc ?? false) ? CacheTimeDuration / 2 : 0
        dataProvider.suggestPlans(request: requestDto, apiCacheChecker: apiCacheChecker, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

// Requests
extension SuggestPlanRequestDTO {
    class func requestForWHC() -> SuggestPlanRequestDTO {
        let request = SuggestPlanRequestDTO()
        let param = RecommendationParam()
        request.businessUnitType = ["App"]
        request.priceType = "S"
        request.noOfCustomer = 1
        request.isWhc = true
        request.numberOfPlan = "200"
        request.disjunctive = "false"
        
        request.activity = "S"
        
        param.categoryCode = Constants.Category.homeAppliances
        param.serviceList = [NameValuePair(name: Constants.PlanServices.whcInspection, value:"")]
        request.recommendationParam = [param]
        return request
    }
}
