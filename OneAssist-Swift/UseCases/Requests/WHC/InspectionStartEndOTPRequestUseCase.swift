//
//  InspectionStartEndOTPRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 28/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class InspectionStartEndOTPRequestDTO: BaseRequestDTO {

    var serviceRequestId: String

    init(serviceRequestId: String) {
        self.serviceRequestId = serviceRequestId
    }
}

class InspectionStartEndOTPResponseDTO: BaseResponseDTO {

    init() {
        super.init(dictionary: [:])
    }
    required init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }

    var data : InspectionStartEndOTP?
}

class InspectionStartEndOTP : ParsableModel {

    var serviceStartCode : String?
    var serviceEndCode : String?
}

class InspectionStartEndOTPRequestUseCase : BaseRequestUseCase<InspectionStartEndOTPRequestDTO, InspectionStartEndOTPResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: InspectionStartEndOTPRequestDTO?, completionHandler: @escaping (InspectionStartEndOTPResponseDTO?, Error?) -> Void) {
        responseDataProvider.getStartEndOTP(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
