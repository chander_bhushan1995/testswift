//
//  WHCPinVerificationUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class WHCPincodeRequestDTO:BaseRequestDTO {
    var pincode:String?
    var type:String?
    var addrType:String?
    var serviceRequestSourceType: String? //for SOD
    
    init(pincode: String? = nil, type: String? = nil, addrType: String? = nil) {
        self.pincode = pincode
        self.type = type
        self.addrType = addrType
    }
    
    init(pincode: String,serviceRequestSourceType: String? = nil){
        self.pincode = pincode
        self.serviceRequestSourceType = serviceRequestSourceType
    }
}

class WHCPincodeResponseDTO:BaseResponseDTO{
    var data:WHCPincResponseModel?
}

class WHCPincResponseModel:ParsableModel
{
    var pincode:String?
    var isPincodeServicable:String?
    var supportedRequestTypes: [String]?
    var hubId:Int?
    var isCourtesyApplicable:String?
    var stateName :String?
    var cityName:String?
    var stateCode: String?
    var cityCode: String?
    var pincodeServicable:String?
    var customerAddress:[CustomerAddressModel]?
}

class CustomerAddressModel:ParsableModel{
    var addressLine1:String?
    var addressLine2: String?
    var pincode: String?
    var stateName: String?
    var stateCode: String?
    var cityName: String?
    var cityCode: String?
}

class WHCPinVerificationUseCase:BaseRequestUseCase<WHCPincodeRequestDTO,WHCPincodeResponseDTO> {
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: WHCPincodeRequestDTO?, completionHandler: @escaping (WHCPincodeResponseDTO?, Error?) -> Void) {
        responseDataProvider.getWHCPinCodeResponse(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

class WHCPinVerificationRequestDTO: BaseRequestDTO {
    var name: String
    var email: String?
    var pincode: String
    var mobileNumber: String
    var campaignId: String? = RemoteConfigManager.shared.oaHACampaignId
    
    init(pincode: String, mobileNumber: String, name: String, email: String) {
        self.pincode = pincode
        self.mobileNumber = mobileNumber
        self.name = name
        self.email = email
    }
    
    init(pincode: String, mobileNumber: String, name: String){
        self.pincode = pincode
        self.mobileNumber = mobileNumber
        self.name = name
    }
}

class WHCNotifyUseCase: BaseRequestUseCase<WHCPinVerificationRequestDTO, BaseResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: WHCPinVerificationRequestDTO?, completionHandler: @escaping (BaseResponseDTO?, Error?) -> Void) {
        responseDataProvider.getWHCNotify(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

class ClaimPincodeRequestUseCase: BaseRequestUseCase<WHCPincodeRequestDTO, WHCPincodeResponseDTO> {
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    override func getRequest(requestDto: WHCPincodeRequestDTO?, completionHandler: @escaping (WHCPincodeResponseDTO?, Error?) -> Void) {
        responseDataProvider.checkPincodeServiciability(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
