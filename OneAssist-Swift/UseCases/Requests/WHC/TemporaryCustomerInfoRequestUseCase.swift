//
//  TemporaryCustomerInfoRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 05/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class TemporaryCustomerInfoRequestDTO: BaseRequestDTO {
    var activationCode = ""
}

class TemporaryCustomerInfoResponseDTO: BaseResponseDTO {
    var customerDetails: [TempCustomerDetail]?
    var enableFDFlow: NSNumber?
    var paymentDone: Bool?
    var preBoardingTaskStage: String?
    var task: [Task]?
}

class TempCustomerDetail: ParsableModel {
    var activityRefId: String?
    var addrLine1: String?
    var addrLine2: String?
    var amount: NSNumber?
    var city: String?
    var custId: String?
    var deviceId: String?
    var deviceMake: String?
    var deviceModel: String?
    var dob: String?
    var documentStage: String?
    var emailId: String?
    var firstName: String?
    var gender: String?
    var landmark: String?
    var lastName: String?
    var mobileNo: NSNumber?
    var orderId: NSNumber?
    var paymentDate: String?
    var pinCode: String?
    var planName: String?
    var planServices: [String]?
    var productCode: [String]?
    var relationshipCode: String?
    var stateCode: String?
    var taskList: AnyObject?
    var uploadDocsLastDate: String?
    var coverAmount: NSNumber?
    var planValidity: NSNumber?
    var devicePurchaseAmount: String?
    var devicePurchaseDate: String?
    var maxInsuranceValue: NSNumber?
    var minInsuranceValue: NSNumber?
    var minDeviceDate: String?
    var partnerBuCode: String?
    var partnerCode: String?
    
}

class TemporaryCustomerInfoRequestUseCase: BaseRequestUseCase<TemporaryCustomerInfoRequestDTO, TemporaryCustomerInfoResponseDTO> {
    
    let dataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: TemporaryCustomerInfoRequestDTO?, completionHandler: @escaping (TemporaryCustomerInfoResponseDTO?, Error?) -> Void) {
        dataProvider.getTempCustomer(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
