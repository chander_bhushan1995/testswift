//
//  ReRescheduleInspectionUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 15/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class RescheduleInspectionRequestDTO: BaseRequestDTO {
    
    var serviceReqId: String?
    var modifiedBy: String?
    var scheduleSlotStartDateTime: String?
    var scheduleSlotEndDateTime: String?
    
    override func createRequestParameters() -> [String : Any]? {
        return ["modifiedBy" : modifiedBy ?? "Test", "scheduleSlotStartDateTime": scheduleSlotStartDateTime ?? "", "scheduleSlotEndDateTime": scheduleSlotEndDateTime ?? ""]
    }
}

class RescheduleInspectionResponseDTO: BaseResponseDTO {
    
}

class RescheduleInspectionRequestUseCase : BaseRequestUseCase<RescheduleInspectionRequestDTO, RescheduleInspectionResponseDTO> {
    
    let dataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: RescheduleInspectionRequestDTO?, completionHandler: @escaping (RescheduleInspectionResponseDTO?, Error?) -> Void) {
        dataProvider.rescheduleInspection(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
