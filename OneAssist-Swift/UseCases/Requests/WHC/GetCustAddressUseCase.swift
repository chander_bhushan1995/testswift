//
//  GetCustAddressUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 25/01/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import Foundation

class GetCustAddressUseCase: BaseRequestUseCase<WHCPincodeRequestDTO, WHCPincodeResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.whcDataProvider()
    
    override func getRequest(requestDto: WHCPincodeRequestDTO?, completionHandler: @escaping (WHCPincodeResponseDTO?, Error?) -> Void) {
        responseDataProvider.getCustomerAddress(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
