//
//  BuyNowUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 30/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class BuyNowWhcRequestDTO: BaseRequestDTO {
    var planCode = ""
}

class BuyNowWhcResponseDTO: BaseResponseDTO {
    var uniqueId: String?
}

class BuyNowWhcRequestUseCase : BaseRequestUseCase<BuyNowWhcRequestDTO, BuyNowWhcResponseDTO> {
    
    let dataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: BuyNowWhcRequestDTO?, completionHandler: @escaping (BuyNowWhcResponseDTO?, Error?) -> Void) {
        dataProvider.buyNow(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
