//
//  MHCDataAddUpdateUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 12/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class MHCDataAddUpdateRequestDTO: BaseRequestDTO {
    
    let data: [String: Any]
    
    init(data: [String: Any]) {
        self.data = data
    }
}

class MHCDataAddUpdateResponseDTO: BaseResponseDTO {
}

class MHCDataAddUpdateUseCase : BaseRequestUseCase<MHCDataAddUpdateRequestDTO, MHCDataAddUpdateResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: MHCDataAddUpdateRequestDTO?, completionHandler: @escaping (MHCDataAddUpdateResponseDTO?, Error?) -> Void) {
        responseDataProvider.postMHCData(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
