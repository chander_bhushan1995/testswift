//
//  SaveCustomerDetailsUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 13/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData

class SaveCustomerDetailsUseCase {
    func saveCustomerDetails(_ response: GetCustomerDetailsResponseDTO? = nil, completion: @escaping ((Error?) -> Void)) {
        if let response = response {
            SaveCustomerDetailsUseCase.saveCustomerDetails(from: response)
            completion(nil)
        } else if SaveCustomerDetailsUseCase.getCustomerDetails() == nil || CacheManager.shared.forceFetchCustomerDetail {
            GetCustomerDetailsRequestUseCase().getCustomerDetail(completionHandler: { (response, error) in
                if let response = response, error == nil {
                    SaveCustomerDetailsUseCase.saveCustomerDetails(from: response)
                    completion(nil)
                } else {
                    completion(error)
                }
            })
        } else {
            completion(nil)
        }
    }
    
    static func saveCustomerDetails(from response: GetCustomerDetailsResponseDTO) {
        let details = CustomerDetailsCoreDataStore.currentCustomerDetails ?? CustomerDetailsCoreDataStore(context: CoreDataStack.sharedStack.mainContext)
        let dtl = response.userInfos?.first(where: { $0.addressType == "PER" }) ?? response.userInfos?.first
        details.addressLine1 = dtl?.addressLine1
        details.addressLine2 = dtl?.addressLine2
        details.cityCode = dtl?.cityCode
        details.cityName = dtl?.cityName
        details.firstName = dtl?.firstName ?? details.firstName
        details.lastName = dtl?.lastName ?? details.lastName
        details.stateName = dtl?.stateName
        details.stateCode = dtl?.stateCode
        details.pincode = dtl?.pinCode
        details.gender = dtl?.gender
        details.mobileNumber = dtl?.mobileNumber ?? details.mobileNumber
        details.email = dtl?.email ?? details.email
        if let custType = dtl?.custType {
            UserDefaultsKeys.custType.set(value: custType)
        }
        CoreDataStack.sharedStack.saveMainContext()
        CoreDataStack.sharedStack.insertApiCacheData(apiCacheChecker: CustomerDetailsCacheChecker())
        CacheManager.shared.forceFetchCustomerDetail = false
    }
    
    private static func getCustomerDetails() -> CustomerDetailsCoreDataStore? {
        let list: [CustomerDetailsCoreDataStore]? = CoreDataStack.sharedStack.fetchApiCacheData(apiCacheChecker: CustomerDetailsCacheChecker())
        return list?.first
    }
}
