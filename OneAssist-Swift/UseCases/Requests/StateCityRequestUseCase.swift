//
//  StateAndCityRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 23/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class StateCityRequestDTO: BaseRequestDTO {
    
    var pinCode: String
    
    init(pinCode: String) {
        self.pinCode = pinCode
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        var parameters = super.createRequestParameters()
        parameters?[Constants.RequestKeys.addressTmp] = [Constants.RequestKeys.pinCode: pinCode]
        
        return parameters
    }
}

class StateCityResponseDTO: BaseResponseDTO {
    var addressTmp: AddressTmp?
}

class AddressTmp: ParsableModel {
    
    var addressList: [AnyObject]?
    var city: String?
    var cityName: String?
    var partnerBuCode: NSNumber?
    var partnerCode: NSNumber?
    var pinCode: String?
    var stateCode: String?
    var stateName: String?
}

class StateCityRequestUseCase: BaseRequestUseCase<StateCityRequestDTO, StateCityResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: StateCityRequestDTO?, completionHandler: @escaping (StateCityResponseDTO?, Error?) -> Void) {
        responseDataProvider.getStateCity(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
