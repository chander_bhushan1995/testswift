//
//  OneOAUseCase.swift
//  OneAssist-Swift
//
//  Created by Raj on 07/02/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import Foundation

class GetLongURLResponseDTO: BaseResponseDTO {
    var longUrl:String?
}


class GetLongURLRequestDTO: BaseRequestDTO {
    var url: String
    
    init(url: String) {
        self.url = url
    }
}


class GetLongURLUseCase : BaseRequestUseCase<GetLongURLRequestDTO, GetLongURLResponseDTO> {
    let dataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: GetLongURLRequestDTO?, completionHandler: @escaping (GetLongURLResponseDTO?, Error?) -> Void) {
        dataProvider.get1oaLongUrl(url: requestDto?.url ?? "", taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
