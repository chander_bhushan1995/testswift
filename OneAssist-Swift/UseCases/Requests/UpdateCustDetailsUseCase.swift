//
//  UpdateCustDetailsUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 09/10/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class UpdateCustDetailsRequestDTO: BaseRequestDTO {
    
    var customers: [CustDetails]?
    var memId: String?
    
    override func createRequestParameters() -> [String : Any]? {
        let dict = super.createRequestParameters()
        return dict
    }
    
    init(customars: [CustDetails]?, memId: String?) {
        self.customers = customars
        self.memId = memId
    }
}

/// Use case to get bank details
class UpdateCustDetailsRequestUseCase: BaseRequestUseCase<UpdateCustDetailsRequestDTO, BaseResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: UpdateCustDetailsRequestDTO?, completionHandler: @escaping (BaseResponseDTO?, Error?) -> Void) {
        responseDataProvider.updateCustDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

