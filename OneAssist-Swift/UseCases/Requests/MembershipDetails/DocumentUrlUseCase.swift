//
//  DocumentUrlUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/09/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class DocumentUrlRequestDTO: BaseRequestDTO {
    var documentId: String
    
    init(documentId: String) {
        self.documentId = documentId
    }
}

class DocumentUrlResponseDTO: BaseResponseDTO {
    var data: String?
}

class DocumentUrlUseCase : BaseRequestUseCase<DocumentUrlRequestDTO, DocumentUrlResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: DocumentUrlRequestDTO?, completionHandler: @escaping (DocumentUrlResponseDTO?, Error?) -> Void) {
        responseDataProvider.getDocumentUrl(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
