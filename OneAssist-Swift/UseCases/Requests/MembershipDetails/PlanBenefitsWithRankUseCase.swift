//
//  PlanBenefitsWithRankUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 03/02/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import Foundation

class PlanBenefitsWithRankRequestDTO: BaseRequestDTO {
    var planCode: [String]
    
    init(planCode: String) {
        self.planCode = [planCode]
    }
}

class PlanBenefitsWithRankResponseDTO: BaseResponseDTO {
    var data: [PlanBenefitObject]?
}

class PlanBenefitObject: ParsableModel {
    
    var planCode: NSNumber?
    var frequency: String?
    var benefitList: [BenefitObject]?
}

class BenefitObject: ParsableModel {
    var planCode: NSNumber?
    var benefitId: NSNumber?
    var benefit: String?
    var rank: NSNumber?
}

class PlanBenefitsWithRankUseCase : BaseRequestUseCase<PlanBenefitsWithRankRequestDTO, PlanBenefitsWithRankResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: PlanBenefitsWithRankRequestDTO?, completionHandler: @escaping (PlanBenefitsWithRankResponseDTO?, Error?) -> Void) {
        responseDataProvider.getPlanBenefitsWithRank(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
