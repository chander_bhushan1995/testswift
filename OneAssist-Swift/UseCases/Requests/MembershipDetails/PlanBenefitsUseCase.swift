//
//  PlanBenefitsUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class PlanBenefitsRequestDTO: BaseRequestDTO {
    var planList: String
    
    init(planList: String) {
        self.planList = planList
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        var parameters = super.createRequestParameters()
        
        parameters?[Constants.RequestKeys.planCode] = planList
        
        return parameters
    }
}

class PlanBenefitsResponseDTO: BaseResponseDTO {
    var planBenefitMappingDTO: PlanBenefitMappingDTO?
}

class PlanBenefitMappingDTO: ParsableModel {
    
    var benefitLst: [BenefitList]?
}

class BenefitList: ParsableModel {
    
    var benefit: String?
    var benefitId: NSNumber?
    var value: String?
}

class PlanBenefitsUseCase : BaseRequestUseCase<PlanBenefitsRequestDTO, PlanBenefitsResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: PlanBenefitsRequestDTO?, completionHandler: @escaping (PlanBenefitsResponseDTO?, Error?) -> Void) {
        responseDataProvider.getPlanBenefits(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
