//
//  DownloadInvoiceUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 16/09/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class DownloadInvoiceRequestDTO: BaseRequestDTO {
    var orderId: String
    var membershipType: String
    var taxType: String = "GST"
    
    init(orderId: String, membershipType: String) {
        self.orderId = orderId
        self.membershipType = membershipType
    }
}

class DownloadInvoiceResponseDTO: BaseResponseDTO {
    var data: [Invoice]?
}

class Invoice: ParsableModel {
    var memPurchaseDate: String?
    var docType: String?
    var taxType: String?
    var orderId: String?
    var invoiceNo: String?
    var planPrice: String?
    var memId: String?
}


class DownloadInvoiceUseCase : BaseRequestUseCase<DownloadInvoiceRequestDTO, DownloadInvoiceResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: DownloadInvoiceRequestDTO?, completionHandler: @escaping (DownloadInvoiceResponseDTO?, Error?) -> Void) {
        responseDataProvider.getInvoices(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

