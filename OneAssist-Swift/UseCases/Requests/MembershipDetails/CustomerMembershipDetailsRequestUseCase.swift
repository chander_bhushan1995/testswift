//
//  CustomerMembershipDetailsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 05/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

private let docUpload = "DOC_UPLOAD"
private let extWarranty = "Extended Warran"

protocol MembershipChecks {
    func check(in tasks: [Task], for name: String) -> Bool?
    func isPEMembership(for categories: [String]?, and tasks: [Task]?,and categoryCode: [String]?) -> Bool
    func isWalletMembership(for categories: [String]?, and categoryCode: [String]?) -> Bool
    func isHAMembership(for categories: [String]?, and tasks: [Task]?, and categoryCode: [String]?) -> Bool
    func isWhcMembership(for assets: [Asset]?, and tasks: [Task]?) -> Bool
    func isEWHAMembership(for assets: [Asset]?, and tasks: [Task]?)->Bool
    func checkIsMembershipIDFence(for services: [Service]?) -> Bool?
}

extension MembershipChecks {
    
    internal func check(in tasks: [Task], for name: String) -> Bool? {
        for task in tasks {
            if task.name == name {
                return true
            }
        }
        return nil
    }
    
    internal func checkIsMembershipIDFence(for services: [Service]?) -> Bool? {
        return !(services?.filter { $0.serviceName == Constants.PlanServices.idFence }.isEmpty ?? true)
    }
    
    /* These below checks are for individual plan check */
    func isPEMembership(for categories: [String]?, and tasks: [Task]?,and categoryCode: [String]?) -> Bool {
        if let categories = categories {
//            if categories.count > 1 {
//                return false
//            }
            
            /* Combo plan is also included in this. Need to check in presenter of memberships tab. */
            if categories.contains(Constants.Category.personalElectronics) {
                return true
            }
        }
        
        /* This condition is for FD flow, will be neglected later because it is not there in iOS. */
        if let tasks = tasks {
            if let _ = check(in: tasks, for: docUpload) {
                return true
            }
        }
        
        if let categoryCode = categoryCode {
            return categoryCode.contains(Constants.Category.personalElectronics)
        }
        
        return false
    }
    
    func isWalletMembership(for categories: [String]?, and categoryCode: [String]?) -> Bool {
        if let categories = categories {
            if categories.count > 1 {
                return false
            }
            
            if categories.contains(Constants.Category.finance) {
                return true
            }
        }
        
        if let categoryCode = categoryCode {
            return categoryCode.contains(Constants.Category.finance)
        }
        
        return false
    }
    
    func isHAMembership(for categories: [String]?, and tasks: [Task]?, and categoryCode: [String]?) -> Bool {
        if let categories = categories {
            if categories.count > 1 {
                return false
            }
            
            if categories.contains(Constants.Category.homeAppliances) {
                return true
            }
        }
        
        if let tasks = tasks {
            if let _ = check(in: tasks, for: Strings.Global.inspection) {
                return true
            }
        }
        
        if let categoryCode = categoryCode {
            return categoryCode.contains(Constants.Category.homeAppliances)
        }
        
        return false
    }
    
    func isWhcMembership(for assets: [Asset]?, and tasks: [Task]?) -> Bool {
        /* This method would be called only on HA memberships. */
        if let tasks = tasks {
            if let _ = check(in: tasks, for: Strings.Global.inspection) {
                return true
            }
        }
        
        guard let assets = assets else {
            return true
        }
        
        for asset in assets {
            guard let services = asset.services else {
                return true
            }
            
            if services.map({ $0.serviceName }).contains(extWarranty) == false {
                return true
            }
        }
        
        return false
    }
    
    /* This method would be called only on HA memberships. */
    func isEWHAMembership(for assets: [Asset]?, and tasks: [Task]?)->Bool{
        if let tasks = tasks {
            if let _ = check(in: tasks, for: Strings.Global.inspection) {
                return false
            }
        }
        
        guard let assets = assets else {
            return false
        }
        
        for asset in assets {
            guard let services = asset.services else {
                return false
            }
            return services.map({ $0.serviceName }).contains(extWarranty)
        }
        return false
    }
    
    /* This method would be called only on PE memberships. */
    func isABBMembership(for assets: [Asset]?) -> Bool{
        if let assets = assets, !assets.isEmpty {
            for asset in assets {
                if let services = asset.services, !services.isEmpty  {
                    for service in services {
                        if service.serviceName?.lowercased() == Constants.PlanServices.assuredBuyback.lowercased() {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
    
    /* This method would be called only on PE memberships. */
    func isABBOnlyMembership(for assets: [Asset]?) -> Bool {
        if let assets = assets, !assets.isEmpty {
            for asset in assets {
                if let services = asset.services, !services.isEmpty  {
                    for service in services {
                        if service.serviceName?.lowercased() != Constants.PlanServices.assuredBuyback.lowercased() {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }
        
    /* This method would be called only on PE memberships. */
    func isABBTenureActive(for assets: [Asset]?) -> Bool {
        if let assets = assets, !assets.isEmpty {
            for asset in assets {
                if let service = asset.services?.first(where: {$0.serviceName?.lowercased() != Constants.PlanServices.assuredBuyback.lowercased()}) {
                    return service.isServiceTenureActive
                }
            }
        }
        return false
    }
    
    /* Below is the check for combo plan. As of now we are considering that there is combo of Mobile and Wallet only.
        In future combo could be generic, in this case change would be there in backend. */
    /*
    func isComboPlan(for categories: [String]?) -> Bool {
        if let categories = categories {
            if categories.count > 1 {
                /* Here in future conditions regarding other combos could be added. */
                if categories.contains(Constants.Category.finance) && categories.contains(Constants.Category.personalElectronics) {
                    return true
                }
            }
        }
        
        return false
    } */
    
    
}


class CustomerMembershipDetailsRequestDTO: BaseRequestDTO {
    var custId: String?
    var assetRequired = "true"
    var claimRequired = "true"
    var mobileNo: String?
    var memId: String?
    var membershipType: String?
    var assetDocRequired:String?
    var assetAttributesRequired: String?
    var serviceRequestTypes = "HA_EW,HA_BD,HA_AD,HA_FR,HA_BR,PE_ADLD,PE_THEFT,PE_EW,PMS"
    
    var renewalEligibilityRequired: String?
    var customerInfoRequired: String?
    var startIndex: String?
    var totalSize: String?
    var checkClaimEligibility: String?
    var assetId: String?
    var assetScopeRequired: String?
    var assetServicesRequired: String?
    var category: String?
    var assetRenewalEligibilityRequired: String?
    var leadDataRequired: String?
    var customerAddressRequired: String?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class CustomerMembershipDetailsResponseDTO: BaseResponseDTO {
    var data: CustomerMembershipData?
    
    func checkCustomerHaveAnyIDFencePlan() -> Bool {
        var isIDFenceInMem = false
       let idFenceMemberships = data?.memberships?.filter({$0.checkIsMembershipIDFence()}) ?? []
       let idFencePendingMemberships = data?.pendingMemberships?.filter({$0.checkIsMembershipIDFence()}) ?? []
       if !idFenceMemberships.isEmpty || !idFencePendingMemberships.isEmpty {
           isIDFenceInMem = true
       }
       return isIDFenceInMem
    }
    
    func getIDFenceMembership() -> CustomerMembershipDetails? {
       var iDFenceInMem: CustomerMembershipDetails? = nil
       let idFenceMemberships = data?.memberships?.filter({$0.checkIsMembershipIDFence()}) ?? []
        
       if !idFenceMemberships.isEmpty {
          iDFenceInMem = idFenceMemberships.last
       }
       return iDFenceInMem
    }
    
    func getIDFencePendingMembership() -> CustomerPendingMembershipDetails? {
        var iDFenceInMem: CustomerPendingMembershipDetails? = nil
       let idFencePendingMemberships = data?.pendingMemberships?.filter({$0.checkIsMembershipIDFence()}) ?? []
        if !idFencePendingMemberships.isEmpty {
            iDFenceInMem = idFencePendingMemberships.last
        }
       
       return iDFenceInMem
    }
}

class CustomerMembershipData: ParsableModel {
    var customers: [CustomerR]?
    var memberships: [CustomerMembershipDetails]?
    var pendingMemberships: [CustomerPendingMembershipDetails]?
    var leadData: LeadData?
}

// Active, Rejected and Cancelled memberships would appear in this.
class CustomerMembershipDetails: ParsableModel, MembershipChecks {
    var plan: Plan?
    var assets: [Asset]?
    var endDate: String?
    var memId: NSNumber?
    var memUUID: String?
    var membershipStatus: String?
    var orderId: NSNumber?
    var graceDays: NSNumber?
    var enableFileClaim = false
    var salesPrice: NSNumber?
    var startDate: String?
    var claims: [Claims]?
    var purchaseDate:String?
    
    // This information is at 'Plan' level in pending memberships
    var products: [String]?
    var coverAmount: NSNumber?
    var trial: String?
    
    var enableClaimTracking: Bool?
    var showSIOpt: Bool?
    var showInsuranceCertificate: NSNumber?
    var showPaymentReceipt: NSNumber?
    var ekitStatus: String?
    var ekitDocId: String?
    var partnerCode: NSNumber?
    
    var isSIEnabled: NSNumber?
    var renewalStatus: String?
    var renewalActivity: String?
    var customers: [CustomerR]?
    var categories: [String]?
    var partnerBUCode: NSNumber?
    
    // SI Status for IDFence Premium Membership
    var siStatus: String?
    
    // For Dranf Membership
    var draftMemberships: [Intimation]?
    
    func isExpiring() -> Bool {
        if renewalActivity == Constants.RenewalActivityFlags.changePlan || renewalActivity == Constants.RenewalActivityFlags.renewalWindow || renewalActivity == Constants.RenewalActivityFlags.buyNewPlan {
            return true
        }
        return false
    }
    
    func typeOfClaim() -> Constants.Services? {
        var type: Constants.Services? = nil
        if isPEMembership() {
            type = .peADLD

        }  else if isHAMembership() {
            if isWhcMembership() {
                type = .accidentalDamage
            } else {
                type = .selfRepair
            }
        }
        return type
    }
    
    func checkIsMembershipIDFence() -> Bool {
        return checkIsMembershipIDFence(for: self.plan?.services) ?? false
    }
    
    func isPEMembership() -> Bool {
        return isPEMembership(for: categories, and: nil, and:  products?.map({
            Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
        }))
    }
    
    func isWalletMembership() -> Bool {
        return isWalletMembership(for: categories, and: products?.map({
            Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
        }))
    }
    
    func isHAMembership() -> Bool {
        return isHAMembership(for: categories, and: nil, and: products?.map({
            Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
        }))
    }
    
    func isWhcMembership() -> Bool {
        return isWhcMembership(for: assets, and: nil)
    }
    
    func isHAEWMembership() -> Bool {
        return isEWHAMembership(for: assets, and: nil)
    }
    
    func isABBMembership() -> Bool {
        return isABBMembership(for: assets)
    }
    
    func isABBOnlyMembership() -> Bool {
        return isABBOnlyMembership(for: assets)
    }
    
    func isABBTenureActive() -> Bool {
        return isABBTenureActive(for: assets)
    }
    
    func getInvDocNameMismatch() -> String? {
        var invDocNameMismatch: String? = nil
        if let assets = assets, assets.count>0,
           let firstassets = assets.first {
            invDocNameMismatch = firstassets.invDocNameMismatch
        }
        
        return invDocNameMismatch
    }
    
    func getAssetAttributes() -> [AssetAttributes]? {
        var assetAttributes: [AssetAttributes]? = nil
        if let assets = assets, assets.count>0,
           let firstassets = assets.first {
            assetAttributes = firstassets.assetAttributes
        }
        
        return assetAttributes
    }
}

class Task: ParsableModel {
    var fdTestType: String? 
    var name: String?
    var srNo: String?
    var serviceScheduleSlot: String?
    var serviceScheduleStartDate: String?
    var status: String?
}

// Only Pending memberships would appear in this.
class CustomerPendingMembershipDetails: ParsableModel, MembershipChecks {
    // There could be memId in the case of renewal pending membership.
    var memId: NSNumber?
    var activationCode: String?
    var assets: [Asset]?
    var orderId: NSNumber?
    var graceDays: NSNumber?
    var pincode: String?
    var plan: Plan?
    var purchaseDate: String?
    var salesPrice: NSNumber?
    var startDate: String?
    var task: Task?
    var uploadDocsLastDate: NSNumber?
    var customers: [CustomerR]?
    var isSIEnabled: NSNumber?
    var partnerCode: NSNumber?
    var partnerBUCode: NSNumber?
    var claims: [Claims]?
    var enableFileClaim = false
    var glance: NSNumber?
    var maxInsuranceValue: NSNumber?
    var minInsuranceValue: NSNumber?
    var bpCode: NSNumber?
    var buCode: NSNumber?
    var transactionDate: String?
    var invoiceLimitInDays: NSNumber?
    var minPurchaseDateForGlance: String?
    var activityRefId: String?
    // This would be available only in case of PE Pending memberships i.e fraud detection scenarios
    var tempCustomerInfo: TemporaryCustomerInfoResponseDTO?
    
    // This is used to identify whether pending membership is for renewal or for boarding new customer.
    var activity: String?
    

    func typeOfClaim() -> Constants.Services? {
        var type: Constants.Services? = nil
        if isPEMembership() {
            type = .peADLD
        } else if isHAMembership() {
            if isWhcMembership() {
                type = .accidentalDamage
            } else {
                type = .selfRepair
            }
        }
        return type
    }
    
    func checkIsMembershipIDFence() -> Bool {
        return checkIsMembershipIDFence(for: self.plan?.services) ?? false
    }
    
    func isPendingMembership(with subCategoryCode: String) -> Bool {
        if let products = plan?.products, products.contains(subCategoryCode) {
            return true
        } else if let productCodes = tempCustomerInfo?.customerDetails?.first?.productCode, productCodes.contains(subCategoryCode) {
            return true
        } else {
            return false
        }
    }
    func isPEMembership() -> Bool {
        if let task = task {
            return isPEMembership(for: nil, and: [task], and: plan?.products?.map({
                Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
            }))
        }
        
        return isPEMembership(for: nil, and: nil, and: plan?.products?.map({
            Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
        }))
    }
    
    func isHAMembership() -> Bool {
        if let task = task {
            return isHAMembership(for: nil, and: [task], and: plan?.products?.map({
                Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
            }))
        }
        return isHAMembership(for: nil, and: nil, and: plan?.products?.map({
            Utilities.getProductCategory(for: $0)?.categoryCode ?? ""
        }))
    }
    
    func isWhcMembership() -> Bool {
        if let task = task {
            return isWhcMembership(for: assets, and: [task])
        }
        
        return isWhcMembership(for: assets, and: nil)
    }
}

class Asset: ParsableModel {
    var assetId: NSNumber?
    var brand: String?
    var model: String?
    var name: String?
    var category: String?
    var prodCode: String?
    var serialNo: String?
    var invoiceDate:String?
    var warrantyPeriod: String?
    var custId: String?
    var size : String?
    var sizeUnit : String?
    var technology : String?
    var services: [Service]?
    var assetDocuments :[AssetDocuments]?
    var canRenew: String?
    var currentAge: NSNumber?
    var invoiceValue: NSNumber?
    var status: String?
    var message: String?
    var productVariantId: NSNumber?
    var invDocNameMismatch: String?
    var assetAttributes: [AssetAttributes]?
    
    func getProduct(from asset: Asset, with applianceCategories: [HomeApplianceSubCategories]) -> Product {
        var product : Product {
            let product = Product(dictionary: [:])
            product.assetId = asset.assetId
            product.brand = asset.brand
            product.model = asset.model
            product.productCode = asset.prodCode
            product.productName = asset.name
            product.technology = asset.technology
            product.size = asset.size
            product.sizeUnit = asset.sizeUnit
            product.serialNo = asset.serialNo
            product.canRenew = asset.canRenew
            product.message = asset.message
            product.category = asset.category
            product.serviceList = asset.services?.map({ $0.serviceName ?? "" })
            product.productTypeImageUrlStr = Utilities.getImageStrForProductName(asset.prodCode ?? "", applianceCategories: applianceCategories)
            
            return product
        }
        
        return product
    }
}

class Plan: ParsableModel {
    var handsetinsurancebuffer: NSNumber?
    var handsetinsurancevalue: NSNumber?
    var handsetmininsurancevalue: NSNumber?
    var maxInsuranceValue: NSNumber?
    var minInsuranceValue: NSNumber?
    var price: NSNumber?
    var productCode: String?
    
    // These are the only 3 attributes used by the 'Plan' object inside 'non-pending' memberships,
    // but all the below attributes would be used by the 'Plan' object inside 'pending' memberships.
    var planName: String?
    var planCode: NSNumber?
    var allowedMaxQuantity: NSNumber?
    
    var products: [String]?
    var categories: [String]?
    var services: [Service]?
    var trial: String?
    var coverAmount: NSNumber?
}

class Claims: ParsableModel {
    var createdBy: String?
    var editClaim: Bool?
    var modifiedBy: String?
    var refPrimaryTrackingNo: String?
    var refSecondaryTrackingNo: String?
    var referenceNo: String?
    var serviceRequestId: NSNumber?
    var serviceRequestType: String?
    var workflowStage: String?
    var workflowStageStatus: String?
    var workflowData: WorkflowData?
    var status: String?
    var initiatingSystem: NSNumber?
    var createdOn: String?
}


class Claim: ParsableModel {
    var initiatedDate: String?
    var srNo: NSNumber?
    var status: String?
}

class AssetAttributes: ParsableModel {
    var key: String?
    var value: String?
}

class AssetDocuments:ParsableModel{
    var docKey: String?
    var fileName: String?
    var docReferenceId: String?
    var status: String?
    var categoryCode: String?
}

class Service: ParsableModel, JSONKeyCoder {
    var serviceDescription: [Description]?
    var partnerCode: NSNumber?
    var serviceId: NSNumber?
    var serviceName: String?
    
    // Attributes for non-insurance backed
    var attributes: [Attribute]?
    var insuranceBacked: Bool = true
    var noOfVisitsLeft: NSNumber?
    var products: [Products]?
    
    var isServiceTenureActive: Bool = true
    
    func key(for key: String) -> String? {
        if key == "serviceDescription" {
            return "description"
        }
        return nil
    }
}

class Products: ParsableModel {
    var code: String?
    var displayValue: String?
}

class Attribute: ParsableModel {
    var attributeName: String?
    var attributeValue: String?
}

class CustomerR: ParsableModel {
    var custId: NSNumber?
    var firstName: String?
    var lastName: String?
    var relationship: String?
    var mobileNumber: NSNumber?
    var emailId: String?
    var addressLine1: String?
    var addressLine2: String?
    var cityName: String?
    var stateName: String?
    var email: String?
    var gender: String?
    var pinCode: String?
    var cityCode: String?
    var stateCode: String?
    var relationshipCode: String?
    var dob: String?
    var custType: String?
    var addresses: [Addres]?
}

class LeadData: ParsableModel {
    var startDate: String?
    var salesPrice: NSNumber?
    var purchaseDate: String?
    var orderId: NSNumber?
    var customers: [CustomerR]?
    var plan: Plan?
    var task: Task?
    var assest: [Asset]?
    var paymentLink: String?
}

class CustomerMembershipDetailsRequestUseCase: BaseRequestUseCase<CustomerMembershipDetailsRequestDTO, CustomerMembershipDetailsResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: CustomerMembershipDetailsRequestDTO?, completionHandler: @escaping (CustomerMembershipDetailsResponseDTO?, Error?) -> Void) {
        responseDataProvider.getCustomerMembershipDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
