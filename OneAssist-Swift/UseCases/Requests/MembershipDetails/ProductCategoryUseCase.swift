//
//  ProductCategoryUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class ProductCategoryRequestDTO: BaseRequestDTO {
    var category: String
    
    init(category: String) {
        self.category = category
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        var parameters = super.createRequestParameters()
        
        parameters?[Constants.RequestKeys.categoryCode] = category
        
        return parameters
    }
}

class ProductCategoryResponseDTO: BaseResponseDTO {
    var subCategoryList: [SubCategoryList]?
}

class SubCategoryList: ParsableModel {
    
    var categoryCode: String?
    var categoryName: String?
    var subCategoryCode: String?
    var subCategoryName: String?
}

class ProductCategoryUseCase : BaseRequestUseCase<ProductCategoryRequestDTO, ProductCategoryResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: ProductCategoryRequestDTO?, completionHandler: @escaping (ProductCategoryResponseDTO?, Error?) -> Void) {
        responseDataProvider.getProductCategory(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
