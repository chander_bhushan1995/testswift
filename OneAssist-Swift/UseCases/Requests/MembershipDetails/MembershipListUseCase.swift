//
//  MembershipListUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 08/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class MembershipListUseCase: BasePresenter {
    
    private func checkForTrialMemberships(response: CustomerMembershipDetailsResponseDTO) -> ([CustomerMembershipDetails]?,[CustomerPendingMembershipDetails]?)  {
        
        //TODO: Need to see whether we can remove this or not.
        
        let memberships = response.data?.memberships?.filter({
            // assuming trial could be nil
            return $0.trial != nil
        })
        
        let pendingMemberships = response.data?.pendingMemberships?.filter({
            // assuming trial could be nil
            $0.plan?.trial != nil
        })
        
        if ((memberships?.count ?? 0) > 0 || (pendingMemberships?.count ?? 0) > 0) {
            return (memberships,pendingMemberships)
        } else {
            return (response.data?.memberships,response.data?.pendingMemberships)
        }
    }
    
    func getMembershipArrayForLoggedInUserWith(completionHandler: @escaping (CustomerMembershipDetailsResponseDTO?, Error?) -> Void) {
        let req = CustomerMembershipDetailsRequestDTO()
        req.mobileNo = userMobileNumber
        
        /* "A,P,E" filter is there to avoid the cancelled memberships i.e X.
         Now X is also applied for IDFence Memberships only. */
        req.membershipType = "A,P,E,X"
        req.assetDocRequired = "true"
        req.assetScopeRequired = "true"
        req.checkClaimEligibility = "true"
        req.assetServicesRequired = "true"
        req.renewalEligibilityRequired = "true"
        req.customerInfoRequired = "true"
        req.assetRenewalEligibilityRequired = "true"
        req.leadDataRequired = "true"
        req.customerAddressRequired = "true"
        req.assetAttributesRequired = "true"
        
        let _ = CustomerMembershipDetailsRequestUseCase.service(requestDTO: req, completionBlock: {[weak self] (usecase, detailsResponse, error) in
            do {
                try self?.checkError(detailsResponse, error: error)
                MembershipListUseCase.handleSuccessResponse(response: detailsResponse)
                completionHandler(detailsResponse, nil)
            } catch {
                completionHandler(nil, error)
            }
        })
    }
    
    func getMembershipForCustomerDetail(completionHandler: @escaping (CustomerMembershipDetailsResponseDTO?, Error?) -> Void) {
        let req = CustomerMembershipDetailsRequestDTO()
        req.mobileNo = userMobileNumber
        req.membershipType = "A,P,E,X"
        req.assetDocRequired = "true"
        req.renewalEligibilityRequired = "true"
        req.assetRenewalEligibilityRequired = "true"
        req.leadDataRequired = "true"
        req.customerInfoRequired = "true"
        req.customerAddressRequired = "true"
        req.assetAttributesRequired = "true"
        
        let _ = CustomerMembershipDetailsRequestUseCase.service(requestDTO: req, completionBlock: {[weak self] (usecase, detailsResponse, error) in
            if error != nil || detailsResponse?.error != nil {
                completionHandler(nil,error)
            } else if detailsResponse?.status == Constants.ResponseConstants.success {
                MembershipListUseCase.handleSuccessResponse(response: detailsResponse)
                completionHandler(detailsResponse,nil)
            }
        })
    }
    
    func getMembershipDataForMembershipId(_ membershipId: String?, completionHandler: @escaping (CustomerMembershipDetailsResponseDTO?, Error?) -> Void) {
        let req = CustomerMembershipDetailsRequestDTO()
        req.mobileNo = userMobileNumber
        req.membershipType = "A"
        req.assetServicesRequired = "true"
        req.checkClaimEligibility = "true"
        req.memId = membershipId
        
        let _ = CustomerMembershipDetailsRequestUseCase.service(requestDTO: req, completionBlock: {[weak self] (usecase, detailsResponse, error) in
            if error != nil {
                completionHandler(nil,error)
            } else if let status = detailsResponse?.status, status == Constants.ResponseConstants.success {
                completionHandler(detailsResponse,nil)
            } else{
                do {
                    try self?.checkError(detailsResponse, error: error)
                    completionHandler(detailsResponse, nil)
                } catch {
                    completionHandler(nil, error)
                }
            }
        })
    }
    
    static func handleSuccessResponse(response: CustomerMembershipDetailsResponseDTO?) {
        guard let response = response, let data = response.data else { return }
        setCustomerData(data: data)
        
        //TODO: Need to see whether we can remove this or not.
        // assuming trial could be nil
        let memberships = response.data?.memberships?.filter({ $0.trial != nil })
        let pendingMemberships = response.data?.pendingMemberships?.filter({ $0.plan?.trial != nil })
        
        if !((memberships ?? []).isEmpty && (pendingMemberships ?? []).isEmpty) {
            // This happens when there is a trial membership
            data.memberships = memberships
            data.pendingMemberships = pendingMemberships
        }
        
        // Remove duplicate plans based on activationCode
        response.data?.pendingMemberships = response.data?.pendingMemberships?.unique { $0.activationCode }
    }
}


extension MembershipListUseCase {
    private static func setCustomerData(data: CustomerMembershipData) {
        let customers = data.customers ?? []
        let memberships = data.memberships ?? []
        let pendingMemberships = data.pendingMemberships ?? []
        
        if let customerObject = customers.first {
            saveuserDetail(customerObject)
        }
        
        if let customerObject = pendingMemberships.last?.customers?.first, memberships.isEmpty {
            saveuserDetail(customerObject)
        }
    }
    
    private static func saveuserDetail(_ customerObject: CustomerR) {
        if let user = UserCoreDataStore.currentUser {
            user.mobileNo = customerObject.mobileNumber?.description
            user.cusId = customerObject.custId?.description
            let firstName = customerObject.firstName ?? ""
            let lastName = customerObject.lastName ?? ""
            user.userName = "\(firstName) \(lastName)".trimmingCharacters(in: [" "])
            user.userCustIds = nil
        }
        CoreDataStack.sharedStack.saveMainContext()
        SaveCustomerDetailsUseCase().saveCustomerDetails(getCustomerObject(customerObject)) {(_) in}
    }
    
    private static func getCustomerObject(_ customerObject: CustomerR) ->GetCustomerDetailsResponseDTO {
        //save customer detail
        let obj = GetCustomerDetailsResponseDTO(dictionary: [:])
        let userInfo = UserInfo()
        userInfo.email = customerObject.emailId
        userInfo.mobileNumber = customerObject.mobileNumber?.stringValue
        userInfo.firstName = customerObject.firstName
        userInfo.lastName = customerObject.lastName
        userInfo.gender = customerObject.gender
        userInfo.addressLine1 = customerObject.addressLine1
        userInfo.addressLine2 = customerObject.addressLine2
        userInfo.pinCode = customerObject.pinCode
        userInfo.cityCode = customerObject.cityCode
        userInfo.stateCode = customerObject.stateCode
        userInfo.relationshipCode = customerObject.relationshipCode
        userInfo.custType = customerObject.custType
        
        if let mailingAddress = customerObject.addresses?.filter({($0.mailingAddress == "true")}).first{
            userInfo.addressLine1 = mailingAddress.addressLine1
            userInfo.addressLine2 = mailingAddress.addressLine2
            userInfo.pinCode = mailingAddress.pincode
            userInfo.cityCode = mailingAddress.cityCode
            userInfo.stateCode = mailingAddress.stateCode
        }
        
        obj.userInfos = [userInfo]
        
        return obj
    }
}
