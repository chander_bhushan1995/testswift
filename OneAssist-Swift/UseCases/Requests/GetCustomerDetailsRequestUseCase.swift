//
//  GetCustomerDetailsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 05/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class GetCustomerDetailsResponseDTO: BaseResponseDTO {
    var userInfos: [UserInfo]?
}

class UserInfo {
    var addressLine1: String?
    var addressLine2: String?
    var cityName: String?
    var stateName: String?
    var customerId: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var mobileNumber: String?
    var gender: String?
    var pinCode: String?
    var cityCode: String?
    var stateCode: String?
    var relationshipCode: String?
    var dob: String?
    var custType: String?
    var addressType: String?
}

class GetCustomerDetailsRequestUseCase: BasePresenter {
    
    func getCustomerDetail(completionHandler:@escaping(GetCustomerDetailsResponseDTO?,Error?) -> Void) {
        
        if UserDefaults.standard.bool(forKey: UserDefaultsKeys.tempCustomer.rawValue) {
            let req = TemporaryCustomerInfoRequestDTO()
            req.activationCode = UserCoreDataStore.currentUser?.activationCode ?? ""
            let _ = TemporaryCustomerInfoRequestUseCase.service(requestDTO: req, completionBlock: {[weak self] (usecase, response, error) in
                do {
                    try self?.checkError(response, error: error)
                    
                    let mainResponse = GetCustomerDetailsResponseDTO(dictionary: [:])
                    mainResponse.userInfos = []
                    mainResponse.status = Constants.ResponseConstants.success
                    
                    if let details = response?.customerDetails {
                        for detail in details {
                            
                            let newResponse = UserInfo()
                            newResponse.customerId = detail.custId
                            newResponse.firstName = detail.firstName
                            newResponse.lastName = detail.lastName
                            newResponse.email = detail.emailId
                            newResponse.mobileNumber = detail.mobileNo?.stringValue
                            newResponse.gender = detail.gender
                            newResponse.addressLine1 = detail.addrLine1
                            newResponse.pinCode = detail.pinCode
                            newResponse.cityCode = detail.city
                            newResponse.stateCode = detail.stateCode
                            newResponse.relationshipCode = detail.relationshipCode
                            
                            mainResponse.userInfos?.append(newResponse)
                        }
                    }
                    SaveCustomerDetailsUseCase.saveCustomerDetails(from: mainResponse)
                    completionHandler(mainResponse,nil)
                } catch {
                    completionHandler(nil,error)
                }
            })
        } else {
            if let customerId = UserCoreDataStore.currentUser?.cusId , let intCustomerId = Int(customerId) {
                let number = NSNumber(value: intCustomerId)
                let req = GetPermanentCustomerDetailsRequestDTO(custId: number)
                let _ = GetPermanentCustomerDetailsRequestUseCase.service(requestDTO: req, completionBlock: {[weak self] (usecase, response, error) in
                    do {
                        try self?.checkError(response, error: error)
                        let mainResponse = GetCustomerDetailsResponseDTO(dictionary: [:])
                        mainResponse.userInfos = []
                        mainResponse.status = Constants.ResponseConstants.success
                        let newResponse = UserInfo()
                        newResponse.addressLine1 = response?.mailAddressDtl?.addressLine1
                        newResponse.addressLine2 = response?.mailAddressDtl?.addressLine2
                        newResponse.cityName = response?.mailAddressDtl?.cityName
                        newResponse.stateName = response?.mailAddressDtl?.stateName
                        newResponse.firstName = response?.customer?.firstName
                        newResponse.lastName = response?.customer?.lastName
                        newResponse.email = response?.customer?.email
                        newResponse.mobileNumber = response?.customer?.contactNo?.description
                        newResponse.gender = response?.customer?.gender
                        newResponse.pinCode = response?.mailAddressDtl?.pincode
                        newResponse.cityCode = response?.mailAddressDtl?.city
                        newResponse.stateCode = response?.mailAddressDtl?.state
                        newResponse.dob = response?.customer?.dob
                        mainResponse.userInfos?.append(newResponse)
                        EventTracking.shared.analyticWork(newResponse)
                        SaveCustomerDetailsUseCase.saveCustomerDetails(from: mainResponse)
                        completionHandler(mainResponse,nil)
                    } catch {
                        completionHandler(nil,error)
                    }
                })
            } else {
                // No customer information Found
                let mainResponse = GetCustomerDetailsResponseDTO(dictionary: [:])
                mainResponse.userInfos = []
                mainResponse.status = Constants.ResponseConstants.success
                completionHandler(mainResponse,nil)
            }
        }
    }
}
