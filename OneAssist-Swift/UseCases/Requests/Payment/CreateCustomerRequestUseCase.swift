//
//  CreateCustomerRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 24/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class CreateCustomerRequestDTO: BaseRequestDTO {
    
    var applicationDate: String?
    var addressInfo: [CustomerAddressInfo]?
    var customerInfo: [CustomerInfo]?
    var customerOrderInfo: CustomerOrderInfo?
    var memStartDate: String?
    var initiatingSystem:Int?
    var orderInfo: CustomerOrderInfo?
    var paymentInfo:CustomerPaymentInfo?
    
    // SOD required extract params
    var thirdPartyParams: ThirdPartyParams?
    
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class CustomerPaymentInfo {
    var paymentMode:String?
    var paymentOptions: [String]?
    var viaLink:Bool = true
}

class CustomerOrderInfo {
    
    var applicationNo: String?
    var partnerBUCode: String?
    var partnerCode: String?
    var paymentMode: String?
    var planCode: String?
    var promoCode: String?
    var salesPrice: String?
    var activity: String?
    var planInfo: [ProductInfo] = [] // added in case of SOD
    var siPlanCode: String?
}

class CustomerInfo {
    var custId: NSNumber?
    var aadharId: String?
    var customerdob: String?
    var dlNo: String?
    var emailId: String?
    var firstName: String?
    var gender: String?
    var imeiNo1: String?
    var imeiNo2: String?
    var lastName: String?
    var middleName: String?
    var mobileModel: String?
    var mobileInvoiceDate: String?
    var mobileInvoiceValue: NSNumber?
    var mobileMake: String?
    var mobileNumber: String?
    var mobileOs: String?
    var pancard: String?
    var passport: String?
    var relationship: String?
    var previousCustId: String?
    var salutation: String?
    var warrantyPeriod: String?
    var assetInfo: [ProductInfo]?
}

class ProductInfo {
    var productCode:String?
    var invoiceDate:String? //": "10-Sep-2019",
    var invoiceValue:NSNumber?//": 12000,
    var assetMake:String?//": "motorola",
    var assetModel:String?//": "Moto G (5S) Plus"
    var planCode: NSNumber? // added in case of SOD
    var quantity: NSNumber? // added in case of SOD
    var assetTechnology: String? // added in case of SOD
    var planPrice: NSNumber? // added in case of SOD
    var serviceType: String? // added in case of SOD
    var issueReportedByCustomer: [IssueReportedByCustomer]? // added in case of SOD
}

class CustomerAddressInfo {
    var addrLine1: String?
    var addrLine2: String?
    var addrType: String?
    var cityCode: String?
    var landmark: String?
    var pinCode: String?
    var stateCode: String?
}


//For SOD
class ThirdPartyParams {
    var serviceRequestType: String?
    var insuranceBacked: String?
    var serviceRequestSourceType: String?
    var initiatingSystem: NSNumber?
    var assets: [ProductInfo]?
}

//For SOD
class IssuesReportedByCustomer {
    var issueId: NSNumber?
    var issueDescription: String?
}




class CreateCustomerResponseDTO: BaseResponseDTO {
    
    var membershipNumber: NSNumber?
    var orderId: NSNumber?
    var primaryCustomerId: NSNumber?
    var secondaryCustomerId: NSNumber?
    var emailId: String?
    var productName: String?
    var planName: String?
    var price: NSNumber?
    var taxAmt: NSNumber?
    var discount: NSNumber?
    var paynowlink: String?
    var payNowLink: String?
    var startDate: String?
    var endDate: String?
    var isMotoPayment: String?
    var paymentModes: PaymentMode?
    
    var token: String {
        if let arr = paynowlink?.components(separatedBy: "=") {
            return arr.count >= 2 ? arr[1] : ""
        }
        return ""
    }
    
    var promoToken: String {
        if let arr = paynowlink?.components(separatedBy: "?token=") {
            return arr.count >= 2 ? arr[1] : ""
        }
        return ""
    }
    
    var amountPayable: String {
        let pr = price?.doubleValue ?? 0
        let tx = taxAmt?.doubleValue ?? 0
        let dis = discount?.doubleValue ?? 0
        
        let total = (pr - dis) + tx
        return "\(total)"
    }
}

class PaymentMode: ParsableModel {
    
    var NetBankingPG: String?
    var CreditDebitPG: String?
    var OperatorBilling: String?
}
