//
//  MasterDataUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class MasterDataRequestDTO: BaseRequestDTO {
    
    let masterName: String
    let subParam: String?
    
    init(masterName: String, subParam: String? = nil) {
        self.masterName = masterName
        self.subParam = subParam
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        var dict = JSONParserSwift.getDictionary(object: self)
        
        if subParam == nil {
            dict.removeValue(forKey: Constants.RequestKeys.subParam)
        }
        return dict
    }
}

class MasterDataResponseDTO: BaseResponseDTO {
    var wSmasterInfoDTOlist: [MasterInfoDTOlist]?
}

class MasterInfoDTOlist: ParsableModel {
    var paramCode: String?
    var paramName: String?
}

class MasterDataRequestUseCase : BaseRequestUseCase<MasterDataRequestDTO, MasterDataResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: MasterDataRequestDTO?, completionHandler: @escaping (MasterDataResponseDTO?, Error?) -> Void) {
        responseDataProvider.getMasterData(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
