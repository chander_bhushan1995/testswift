//
//  GetCustDetailsUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 09/10/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class GetCustDetailsRequestDTO: BaseRequestDTO {
    var token: String?
    
    init(token: String?) {
        self.token = token
    }
}

class GetCustDetailsResponseDTO: BaseResponseDTO {
    var data: CustDetailsResponse?
    
}

class CustDetailsResponse: ParsableModel {
    
    var customers: [CustDetails]?
    var memId: String?
    var membershipEndDate: String?
    var membershipStartDate: String?
    var planAttributes: PlanAttribute?
    var planCode: String?
    var planName: String?
    var products: [ProductDetail]?
    var sumAssured: NSNumber?
    
}

class PlanAttribute: ParsableModel {
    
    var MOST_IMPORTANT_TNC_LINK: String?
    var TNC_LINK: String?
    
    
}

class CustDetails: ParsableModel {

    var addresses: [Addres]?
    var custId: NSNumber?
    var emailId: String?
    var firstName: String?
    var lastName: String?
    var mobileNumber: String?
    var gender: String?
    var custType: String?
}


class ProductDetail: ParsableModel {
    
    var brand: String?
    var category: String?
    var custId: NSNumber?
    var model: String?
    var name: String?
    var prodCode: String?
    var serialNo: String?
    var size: String?
    var sizeUnit: String?
    var technology: String?
    var warrantyPeriod: String?

}


class Addres: ParsableModel {
    
    var addressId: NSNumber?
    var addressLine1: String?
    var addressLine2: String?
    var addressType: String?
    var city: String?
    var landmark: String?
    var pincode: String?
    var state: String?
    var country: String?
    var mailingAddress: String?
    var stateName: String?
    var stateCode: String?
    var cityName: String?
    var cityCode: String?
    var stdcode: String?
    var mobileNo: NSNumber?
}

/// Use case to get bank details
class GetCustDetailsRequestUseCase: BaseRequestUseCase<GetCustDetailsRequestDTO, GetCustDetailsResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: GetCustDetailsRequestDTO?, completionHandler: @escaping (GetCustDetailsResponseDTO?, Error?) -> Void) {
        responseDataProvider.getCustDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
