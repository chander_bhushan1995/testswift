//
//  GetSRsReportForPincodeUseCase.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 06/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

class GetSRsReportForPincodeReqDTO: BaseRequestDTO {
    var pinCode:String?
    var serviceRequestSourceType: String?
    var serviceCategory: String?
    
    init(pinCode: String?, serviceRequestSourceType: String, serviceCategory: String?){
        self.pinCode = pinCode
        self.serviceRequestSourceType = serviceRequestSourceType
        self.serviceCategory = serviceCategory
    }
}

class GetSRsReportForPincodeResDTO: BaseResponseDTO{
    var data : Report?
}

class Report: ParsableModel, JSONKeyCoder {
    public var HA : HAReport?
}

class HAReport: ParsableModel, JSONKeyCoder {
    public var pending : NSNumber?
    public var completed : NSNumber?
}


class GetSRsReportForPincodeUseCase: BaseRequestUseCase<GetSRsReportForPincodeReqDTO, GetSRsReportForPincodeResDTO> {
    let dataProvider = dataProviderFactory.commonDataProvider()
    override func getRequest(requestDto: GetSRsReportForPincodeReqDTO?, completionHandler: @escaping (GetSRsReportForPincodeResDTO?, Error?) -> Void) {
        dataProvider.getSRsReportForPincode(requestDto: requestDto!, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
