//
//  GetServicesList.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 02/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class GetServicesListRequestDTO: BaseRequestDTO {
    var productCodes:[String]?
    var serviceCodes: [String]?
    
    init(productCode:String,serviceRequestTypes:[String]?){
        self.productCodes = [productCode]
        self.serviceCodes = serviceRequestTypes
    }
}

class GetServicesListResponseDTO: BaseResponseDTO{
     var data : [ServiceData]?
}

public class ServiceData:ParsableModel, JSONKeyCoder {
     var id : NSNumber?
     var name : String?
     var desc : String?
     var productId : String?
     var productName: String?
     var productCode : String?
     var serviceId : String?
     var serviceCode : String?
     var serviceName : String?
    public func key(for key: String) -> String? {
        if key == "desc" {
            return "description"
        }
        return nil
    }
}

class GetServicesListUseCase: BaseRequestUseCase<GetServicesListRequestDTO, GetServicesListResponseDTO> {
    let dataProvider = dataProviderFactory.commonDataProvider()
    override func getRequest(requestDto: GetServicesListRequestDTO?, completionHandler: @escaping (GetServicesListResponseDTO?, Error?) -> Void) {
        dataProvider.getServicesList(requestDto: requestDto!, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
