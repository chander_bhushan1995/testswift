//
//  GetProductSpecsUseCase.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 05/06/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class GetProductSpecsRequestDTO: BaseRequestDTO {
    var productCodes:[String]?
    var isSubscription: Bool = false
    
    init(productCodes:[String]){
        self.productCodes = productCodes
    }
    
    init(productCodes:[String],isSubscription: Bool){
        self.productCodes = productCodes
        self.isSubscription = isSubscription
    }
}

class GetProductSpecsResponseDTO: BaseResponseDTO{
    var data : [ProductSpecs]?
}

class ProductSpecs:ParsableModel, JSONKeyCoder {
     var id : NSNumber?
     var productCode : String?
     var itemName : String?
     var itemDescription : String?
     var status : String?
     var productTechnology : String?
     var productUnit : String?
     var productSize : String?
     var isSubscription : String?
}


class GetProductSpecsUseCase: BaseRequestUseCase<GetProductSpecsRequestDTO, GetProductSpecsResponseDTO> {
    let dataProvider = dataProviderFactory.commonDataProvider()
    override func getRequest(requestDto: GetProductSpecsRequestDTO?, completionHandler: @escaping (GetProductSpecsResponseDTO?, Error?) -> Void) {
        dataProvider.getProductsSpecs(requestDto: requestDto!, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
