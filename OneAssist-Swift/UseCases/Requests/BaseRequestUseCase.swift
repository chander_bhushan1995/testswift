//
//  BaseService.swift
//  WynkObjectivec
//
//  Created by Sudhir.Kumar on 01/08/17.
//  Copyright © 2017 sudhir kumar. All rights reserved.
//

import Foundation
import Alamofire

enum  ConnectionState {
    case eConnectionSuccess
    case eConnectionFailed
    case eConnectionFailedNoInternetConnection
    case eConnectionStateCancelled
}

protocol BaseRequestUseCaseProtocol{
    var key : String? {get}
    func cancel()
}

extension BaseRequestUseCaseProtocol {
    static var dataProviderFactory: DataProviderFactory {
        return ResponseDataProviderFactory.shared
    }
}

class BaseRequestUseCase<RequestDTOType: BaseRequestDTO, ResponseDTOType: Any>: BaseRequestUseCaseProtocol {
    
    var request: Request?
    private var tag: NSInteger?
    var key: String?
    
    init() {
    }
    
    func cancel() {
        self.request?.cancel()
    }
    
    static func service(with completionBlock : @escaping(_ service : BaseRequestUseCase? , _ responseDTO : ResponseDTOType? , _ error : Error?) -> Void) -> BaseRequestUseCase {
        
        let baseService = self.init(with: 0, requestDTO: nil, completion: completionBlock)
        
        return baseService
    }
    
    static func service(with tag:NSInteger , completionBlock: @escaping (_ service:BaseRequestUseCase? , _ responseDTO: Any? , _ error : Error?) -> Void) -> BaseRequestUseCase{
        let baseService = self.init(with: tag, requestDTO: nil, completion: completionBlock)
        
        return baseService
    }
    
    @discardableResult
    static func service(with tag:NSInteger = 5 ,  requestDTO  : RequestDTOType?, completionBlock: @escaping (_ service:BaseRequestUseCase? , _ responseDTO: ResponseDTOType? , _ error : Error?) -> Void) -> BaseRequestUseCase{
        let baseService = self.init(with: tag, requestDTO: requestDTO, completion: completionBlock)
        
        return baseService
    }
    
    required  init(with tag : NSInteger , requestDTO:RequestDTOType? , completion : @escaping (_ service : BaseRequestUseCase? , _ responseDTO : ResponseDTOType? , _ error : Error?) -> Void) {
        
        self.key = self.key(from: requestDTO)
        
        self.getRequest(requestDto: requestDTO) { (response, error) in
            completion(self, response, error)
        }
    }
    
    /// Used to prepare and send request and return response received from backend
    ///
    /// - Parameters:
    ///   - requestDto: Request model
    ///   - completionHandler: Completeion handler to give response or error
    ///   - response: Response model
    ///   - error: Error in api
    func getRequest(requestDto: RequestDTOType?, completionHandler: @escaping(_ response: ResponseDTOType?, _ error: Error?) -> Void) {
        
    }
    
    func key(from requestDto: RequestDTOType?) -> String {
           return ""
    }
}
