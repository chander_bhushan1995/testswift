//
//  AssetMakeRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 26/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class AssetMakeRequestDTO: BaseRequestDTO {
    var serviceType: String?
    var subCategoryCodeList: [String]?
}

class AssetMakeResponseDTO: BaseResponseDTO {
    var assetMakeDtl: AssetMakeDtl?
}

class AssetMakeDtl: ParsableModel {
    var subCategoryMap: [String: [SubCategory]?]?
}

class SubCategory: ParsableModel {
    var authBy: String?
    var authOn: String?
    var createdBy: String?
    var createdOn: String?
    var emioptionlst: [AnyObject]?
    var event: String?
    var modifiedBy: String?
    var modifiedOn: String?
    var oaSystemConfigPK: OaSystemConfigPK?
    var paramType: String?
    var paramValue: String?
    var paramAttributedValue: NSMutableAttributedString? = nil
    var rejectReason: String?
    var status: String?
    var additionalAttributes: AdditionalAttributes?
}

class OaSystemConfigPK: ParsableModel {
    var paramCode: String?
    var paramName: String?
    var paramType: String?
}
class AdditionalAttributes: ParsableModel {
    var priority: String?
    var image: String?
}
class AssetMakeRequestUseCase : BaseRequestUseCase<AssetMakeRequestDTO, AssetMakeResponseDTO> {
    
    let dataProvider = dataProviderFactory.homeAppliancesDataProvider()
    
    override func getRequest(requestDto: AssetMakeRequestDTO?, completionHandler: @escaping (AssetMakeResponseDTO?, Error?) -> Void) {
        dataProvider.getAssetMake(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
