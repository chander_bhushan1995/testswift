//
//  DuplicateIMEIRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/5/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class DuplicateIMEIRequestDTO: BaseRequestDTO {
    
    private let imeiNumber: String
    
    init(imeiNumber: String) {
        self.imeiNumber = imeiNumber
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        var dict = super.createRequestParameters()
        
        dict?[Constants.RequestKeys.imeiNo] = imeiNumber
        
        return dict
    }
}

class DuplicateIMEIResponseDTO: BaseResponseDTO {
    var imeiDedupe: String?
}

class DuplicateIMEIRequestUseCase : BaseRequestUseCase<DuplicateIMEIRequestDTO, DuplicateIMEIResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    
    override func getRequest(requestDto: DuplicateIMEIRequestDTO?, completionHandler: @escaping (DuplicateIMEIResponseDTO?, Error?) -> Void) {
        dataProvider.duplicateIMEICheck(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
