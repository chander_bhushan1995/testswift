//
//  UserQuestionAnswerUseCase.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 02/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class GetQuestionAnswerRequestDTO: BaseRequestDTO {
    var categoryCode: String = ""
    var questionCode: String = ""
    var custId: String = ""
    
    override init() {
        super.init()
        self.categoryCode = "BOARDING"
        self.questionCode = "ASSET_HA,ASSET_PE,ASSET_F"
        self.custId = UserCoreDataStore.currentUser?.cusId ?? ""
    }
}

class GetQuestionAnswerResponseDTO: BaseResponseDTO {
    var data: [QuestionAnswerData]?
    
}

class QuestionAnswerData: ParsableModel {
    var questionId: NSNumber?
    var questionText: String?
    var subText: String?
    var questionCode: String = "DEFAULT"
    var questionType: String?
    var questionRank: NSNumber?
    var optionList:[OptionList]?
    var otherAnswerList: [OtherAnswerList]?
    var answered: NSNumber?
}

class OptionList: ParsableModel {
    var optionId: NSNumber?
    var optionCode: String?
    var optionText: String?
    var optionRank: NSNumber?
    var displayType: String?
    var selected: NSNumber?
}

class OtherAnswerList: ParsableModel {
    var brand: String?
    var model: String?
}


class UserQuestionAnswerUseCase : BaseRequestUseCase<GetQuestionAnswerRequestDTO, GetQuestionAnswerResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    
    override func getRequest(requestDto: GetQuestionAnswerRequestDTO?, completionHandler: @escaping (GetQuestionAnswerResponseDTO?, Error?) -> Void) {
        dataProvider.getQuestionAnswerList(request: requestDto ?? GetQuestionAnswerRequestDTO(), taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
