//
//  VerifyOTPRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 22/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class MobileAppInfo: ParsableModel {
    var version: String = UIApplication.appVersion
    var os: String = "IOS"
}

class VerifyOTPRequestDTO: BaseRequestDTO {
    var username: String
    var password: String
    var clientType: String  = "WEBCUSTOMER"
    var loginType: String = "OTP"
    var mobileApp: MobileAppInfo = MobileAppInfo(dictionary: [:])
    
    init(username: String, password:String) {
        self.username = username
        self.password = password
    }
}

class VerifyOTPResponseDTO: BaseResponseDTO {
    var data: LoginResponse?
}

class LoginResponse: ParsableModel {
    var userId: NSNumber?
    var firstName: String?
    var clientId: NSNumber?
    var mobileNum: String?
    var wrongPasswordCount: NSNumber?
    var userName: String?
    var custId: NSNumber?
    var emailId: String?
    var custUUID: String?
    var userType: String?
    var lastLoginDate: NSNumber?
}

class VerifyOTPRequestUseCase : BaseRequestUseCase<VerifyOTPRequestDTO, VerifyOTPResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    
    override func getRequest(requestDto: VerifyOTPRequestDTO?, completionHandler: @escaping (VerifyOTPResponseDTO?, Error?) -> Void) {
        dataProvider.verifyOTP(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
