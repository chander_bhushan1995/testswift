//
//  LogoutRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/5/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class LogoutRequestDTO: BaseRequestDTO {
}

class LogoutResponseDTO: BaseResponseDTO {
}

class LogoutRequestUseCase : BaseRequestUseCase<LogoutRequestDTO, LogoutResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    
    override func getRequest(requestDto: LogoutRequestDTO?, completionHandler: @escaping (LogoutResponseDTO?, Error?) -> Void) {
        dataProvider.logoutUser(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
