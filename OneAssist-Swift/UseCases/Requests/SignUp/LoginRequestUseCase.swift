//
//  LoginRequest.swift
//  OneAssistAlamoFire
//
//  Created by Sudhir.Kumar on 09/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import UIKit

class LoginMobileRequestDTO: BaseRequestDTO {
    
    var contactId: String
    var clientType: String = "WEBCUSTOMER"
    
    init(mobileNo: String) {
        self.contactId = mobileNo
    }
    
    override func createRequestParameters() -> [String : Any]? {
        var parameters = super.createRequestParameters()
        
        parameters?[Constants.RequestKeys.contactId] = contactId
        
        return parameters
    }
}

class LoginMobileResponseDTO: BaseResponseDTO {
    
    var otp: NSNumber?
    var mobileNo: String?
}

class LoginRequestUseCase : BaseRequestUseCase<LoginMobileRequestDTO, LoginMobileResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    
    override func getRequest(requestDto: LoginMobileRequestDTO?, completionHandler: @escaping (LoginMobileResponseDTO?, Error?) -> Void) {
        dataProvider.loginUser(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
