//
//  GetMembershipAssetDetailsUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
class GetMembershipAssetListRequestDTO: BaseRequestDTO {
    
    var membershipId:String?
    var assestID:String?
    var assetDoc:Bool?
    override func createRequestParameters() -> [String : Any]? {
        
        let parameters = super.createRequestParameters()
        
        return parameters
    }
}

class GetMembershipAssetListResponseDTO: BaseResponseDTO {
    var data:GetMembershipAssetDetailsData?
}
class GetMembershipAssetDetailsData:ParsableModel{
   var orderId:NSNumber?
   var accountNo:NSNumber?
   var startDate:String?
   var endDate:String?
   var memNo:String?
   var memStatus:String?
var assetDetails:[AssestDetails]?
}
class AssestDetails:ParsableModel{
    var assetId:NSNumber?
    var custId:NSNumber?
    var productCode:String?
   var categoryCode:String?
   var createdBy:String?
   var createdOn:String?
   var modifiedOn:String?
   var modifiedBy:String?
   var status:String?
   var make:String?
   var model:String?
   var deviceId:NSNumber?
   var invoiceDate:String?
   var invoiceValue:String?
   var warrantyPeriod:NSNumber?
   var age:String?
   var technology:String?
   var size:String?
   var sizeUnit:String?
   var assetDocuments:AnyObject?
    
}

class GetMembershipAssetDetailsRequestUseCase: BaseRequestUseCase<GetMembershipAssetListRequestDTO, GetMembershipAssetListResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetMembershipAssetListRequestDTO?, completionHandler: @escaping (GetMembershipAssetListResponseDTO?, Error?) -> Void) {
        responseDataProvider.getMembershipAssetDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
