//
//  GetBankDetailsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/14/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

/// Request model to get bank details
class GetBankDetailsRequestDTO: BaseRequestDTO {
    var custId: String?
    var srnNumber: String?
    var custBankId: String?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
    
    init(custId: String?, srnNumber: String?, custBankId: String?) {
        self.custId = custId
        self.srnNumber = srnNumber
        self.custBankId = custBankId
    }
}

/// Bank details response model
class GetBankDetailsResponseDTO: BaseResponseDTO {
    var data: BankDetails?
}

/// This class contains all Bank details
class BankDetails: ParsableModel {

    var custName: String?
    var bankName: String?
    var bankCode: String?
    var accountNo: String?
    var documentRefNo: String?
    var srNumber: String?
    
}

/// Use case to get bank details
class GetBankDetailsRequestUseCase: BaseRequestUseCase<GetBankDetailsRequestDTO, GetBankDetailsResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetBankDetailsRequestDTO?, completionHandler: @escaping (GetBankDetailsResponseDTO?, Error?) -> Void) {
        responseDataProvider.getBankDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
