//
//  MembershipServicesRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 16/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class MembershipServicesRequestDTO: BaseRequestDTO {
    
//    https://<context>/myaccount/api/membership/{memId}/services
    
    let memId: String
    
    init(memId: String) {
        self.memId = memId
    }
}

class MembershipServicesResponseDTO: BaseResponseDTO {
    var data: [MembershipServices]?
}

class MembershipServices: ParsableModel {
    var assetId: NSNumber?
    var productCode: String?
    var productName: String?
    var services: [Service]?
}

class Description: ParsableModel {
    var claimType: String?
    var damageType: String?
    var displayValue: String?
    var subDescription: [String]?
    var priority: NSNumber?
}

class MembershipServicesRequestUseCase : BaseRequestUseCase<MembershipServicesRequestDTO, MembershipServicesResponseDTO> {
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: MembershipServicesRequestDTO?, completionHandler: @escaping (MembershipServicesResponseDTO?, Error?) -> Void) {
        dataProvider.getMembershipServices(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
