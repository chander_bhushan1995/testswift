//
//  DeviceBackRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Raj on 28/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class DeviceBackResponseDTO:BaseResponseDTO{
    
}
class DeviceBackRequestDTO: BaseRequestDTO {
    
    var modifiedBy: String?
    var serviceRequestId: NSNumber?
    var workflowData: WorkflowData2?
    init(modifiedBy:String?,serviceRequestId:NSNumber?,workflowData:WorkflowData2?){
        self.workflowData = workflowData
        self.modifiedBy = modifiedBy
        self.serviceRequestId = serviceRequestId
    }
    
}
class WorkflowData2 {
    
    var delivery: Delivery2?
    init(delivery:Delivery2?){
        self.delivery = delivery
    }
    
}
class Delivery2 {
    
    var berOptOutAcknowledgment: String?
    init(berOptOutAcknowledgment:String?){
        self.berOptOutAcknowledgment = berOptOutAcknowledgment
    }
    
}
class DeviceBackRequestUseCase : BaseRequestUseCase<DeviceBackRequestDTO, DeviceBackResponseDTO> {
    
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: DeviceBackRequestDTO?, completionHandler: @escaping (DeviceBackResponseDTO?, Error?) -> Void) {
        dataProvider.wantDeviceBack(requestDto: requestDto,taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler )
    }
    
}
