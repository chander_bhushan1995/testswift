//
//  CheckAvailabilityRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 3/19/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class CheckAvailabilityRequestDTO: BaseRequestDTO {
    
    let memId: String
    
    init(memId: String) {
        self.memId = memId
    }
    
    override func createRequestParameters() -> [String : Any]? {
        var params: [String: Any] = [:]
        params["memId"] = memId
        
        return params
    }
}

class CheckAvailabilityResponseDTO: BaseResponseDTO {
    var data: [AssetAvailability]?
}

class AssetAvailability: ParsableModel {
    var assetId: String?
    var eligibleForRaiseClaim: String?
    var message: String?
}

class CheckAvailabilityRequestUseCase : BaseRequestUseCase<CheckAvailabilityRequestDTO, CheckAvailabilityResponseDTO> {
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: CheckAvailabilityRequestDTO?, completionHandler: @escaping (CheckAvailabilityResponseDTO?, Error?) -> Void) {
        dataProvider.getCheckAvailability(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
