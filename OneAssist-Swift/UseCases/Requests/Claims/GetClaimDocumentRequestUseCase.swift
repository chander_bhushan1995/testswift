//
//  GetClaimDocumentRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

enum ClaimDocType: String {
    case image = "ImageFile"
    case thumbnail = "ThumbnailImageFile"
    //    case byteArray = "ByteArrayFile"
    //    case zip = "ZipFile"
    case boardingThumbnail = "thumbnail"
}

class GetClaimDocumentRequestDTO: BaseRequestDTO {
    
    var fileTypeRequired: String?
    var documentIds: String?
    var storageRefIds: String?
    var lengthRequired: String?
    var breadthRequired: String?
    
    init(documentId: String?, storageRefIds: String? = nil, claimDocType: ClaimDocType) {
        self.documentIds = documentId
        self.storageRefIds = storageRefIds
        self.fileTypeRequired = claimDocType.rawValue
        self.lengthRequired = "500"
        self.breadthRequired = "500"
    }
    
    override func createRequestParameters() -> [String : Any]? {
        let parameters = JSONParserSwift.getDictionary(object: self)
//        parameters.removeValue(forKey: "documentId")
        return parameters
    }
}

class GetClaimDocumentResponseDTO: BaseResponseDTO {
    
    required init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }
    
    var image: UIImage?
}

class GetClaimDocumentRequestUseCase: BaseRequestUseCase<GetClaimDocumentRequestDTO, GetClaimDocumentResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetClaimDocumentRequestDTO?, completionHandler: @escaping (GetClaimDocumentResponseDTO?, Error?) -> Void) {
        responseDataProvider.getClaimDocumentRequest(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
