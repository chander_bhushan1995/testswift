//
//  GetServiceRequestUpdateEventRequest.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 27/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class ClaimNotifyAllDocumentUploadedRequestDTO: BaseRequestDTO {
    var requestDescription: String?
    var dateOfIncident: String?
    var modifiedBy: String? = UserCoreDataStore.currentUser?.cusId ?? "test"
    var serviceID: String = ""
    var srType: String? = ""
    var serviceDocuments: [ServiceDocument]?
    
    override func createRequestParameters() -> [String : Any]? {
        var parameters = JSONParserSwift.getDictionary(object: self)
        parameters.removeValue(forKey: "serviceID")
        parameters.removeValue(forKey: "srType")
        return parameters
        
    }
}

class ClaimNotifyAllDocumentUploadedResponseDTO: BaseResponseDTO {
}

class ClaimNotifyAllDocumentUploadedRequestUseCase: BaseRequestUseCase<ClaimNotifyAllDocumentUploadedRequestDTO, ClaimNotifyAllDocumentUploadedResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: ClaimNotifyAllDocumentUploadedRequestDTO?, completionHandler: @escaping (ClaimNotifyAllDocumentUploadedResponseDTO?, Error?) -> Void) {
        responseDataProvider.notifyAllDocumentUploaded(requestDto: requestDto!, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
