//
//  UpdateServiceRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 03/04/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class UpdateServiceRequestDTO: BaseRequestDTO {

    var serviceRequestId : String?
    var modifiedBy: String?
    var incidentDescription: String?
    var dateOfIncident: String?
    var initiatingSystem: NSNumber = 21
    var issueReportedByCustomer: [IssueReportedByCustomer]?
    var placeOfIncident: String?
    var refPrimaryTrackingNo: String?
    var refSecondaryTrackingNo: String?
    var referenceNo: String?
    var requestDescription: String?
    var scheduleSlotEndDateTime: String?
    var scheduleSlotStartDateTime: String?
    var serviceRequestAddressDetails: ServiceRequestAddressDetail?
    var serviceRequestType: String?
    var workFlowValue: String?
    var insurancePartnerCode: NSNumber?
    var additionalDetails: [String: Any]?
    override func createRequestParameters() -> [String : Any]? {
        let dict = JSONParserSwift.getDictionary(object: self)
        return dict
    }
}

//MARK: PE Theft

class UpdatePETheftServiceRequestPlanRequestDTO: UpdateServiceRequestDTO {
    
    var incidentDetail: PETheftIncidentDetail?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

//MARK: PE ADLD

class UpdatePEADLDServiceRequestPlanRequestDTO: UpdateServiceRequestDTO {
    var incidentDetail: [String: Any]?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

//MARK: PE EW

class UpdatePEEWServiceRequestPlanRequestDTO: UpdateServiceRequestDTO {
    var incidentDetail: [String: Any]?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class UpdateServiceRequestResponseDTO: BaseResponseDTO {

}

class UpdateServiceRequestUseCase: BaseRequestUseCase<UpdateServiceRequestDTO, UpdateServiceRequestResponseDTO> {

    let responseDataProvider = dataProviderFactory.claimDataProvider()

    override func getRequest(requestDto: UpdateServiceRequestDTO?, completionHandler: @escaping (UpdateServiceRequestResponseDTO?, Error?) -> Void) {
        responseDataProvider.updateServiceRequestPlan(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
