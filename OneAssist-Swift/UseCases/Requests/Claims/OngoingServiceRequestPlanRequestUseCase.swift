//
//  OngoingServiceRequestPlanRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 06/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class OngoingServiceRequestPlanRequestDTO: SearchServiceRequestPlanRequestDTO {
    
    convenience init(memIds: [String]) {
        self.init()
        status = "\(Constants.TimelineStatus.onHold),\(Constants.TimelineStatus.pending),\(Constants.TimelineStatus.inProgress)"
        referenceNumbers =  memIds.joined(separator: ",")
        sortOrder = "desc" // asc in case of ascending
    }
}

class OngoingServiceRequestPlanRequestUseCase: SearchServiceRequestPlanRequestUseCase {
    
}

