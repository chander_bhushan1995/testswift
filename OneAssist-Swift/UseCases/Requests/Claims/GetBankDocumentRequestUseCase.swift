//
//  GetBankDocumentRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 08/06/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

/// Use case to get cancelled cheque
class GetBankDocumentRequestUseCase:  BaseRequestUseCase<GetClaimDocumentRequestDTO, GetClaimDocumentResponseDTO>  {

    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetClaimDocumentRequestDTO?, completionHandler: @escaping (GetClaimDocumentResponseDTO?, Error?) -> Void) {
        responseDataProvider.getBankDocumentRequest(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
