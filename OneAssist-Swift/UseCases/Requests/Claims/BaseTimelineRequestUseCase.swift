
//
//  BaseTimelineRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Raj on 29/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation


class JavaScriptTimeineResponceDTO: BaseResponseDTO {
    
    var timeline: [Timeline]?
    
}

class Timeline: ParsableModel {
    
    var cards: [TimeLineCard]?
    var misc: Misc?
    var stage: Stage?
    
    
}
class TimeLineCard: ParsableModel {
    
    var cardDocDescription: CardDocDescription?
    var cardReasonList: CardReasonList?
    var cardSingleAction: CardSingleAction?
    var cardDualAction: CardDualAction?
    var cardDCategory: CardDCategory?
    var cardCancelled: CardCancelled?
    var cardVisitScheduled: CardVisitScheduled?
    var cardTechVisit: CardTechVisit?
    var cardFeedback: CardFeedback?
    var cardSectionListAction: CardSectionListAction?
    var cardWalkin: CardWalkin?
    var cardLocation: CardLocation?
    var template: String?
    
    
}
class CardLocation: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var action2: Action?
    var actionParam1: String?
    var actionParam2: NSNumber?
    var stageDescription: String?
    var displayList: [DisplayList]?
    var highlight: String?
    var subDescription: String?
    var bottomMessage:String?
    
}
class DisplayList: ParsableModel {
    
    var key1: String?
    var key2: String?
}
class CardDualAction: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var action2: Action?
    var actionParam1: String?
    var actionParam2: String?
    var stageDescription: String?
    var subDescription: String?
    
}
class CardWalkin: ParsableModel ,JSONKeyCoder{
    
    var action1: Action?
    var stageDescription: String?
    var pincode: String?
    var subDescription: String?
    var highlight: String?
}
class CardSectionListAction: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var action2: Action?
    var actionParam1: String?
    var stageDescription: String?
    var highlight: String?
    var parts: [Part]?
    var subText1: String?
    var subText2: String?
    
    
}
class CostBreakup: ParsableModel {
    var subtotal: String?
    var total: String?
    var discount: String?
    var parts: [Part]?
}

class Part: ParsableModel {
    
    var header: String?
    var items: [Item]?
}
class Item: ParsableModel,JSONKeyCoder {
    
    var stageDescription: String?
    var value: String?
    var discount: String?
}
class CardFeedback: ParsableModel {
    
    var heading: String?
    var subHeading: String?
}
class CardTechVisit: ParsableModel ,JSONKeyCoder{
    
    var action1: Action?
    var contentHeader: String?
    var stageDescription: String?
    var heading: String?
    var imageUrl: String?
    var phoneNo: String?
    var subHeading: String?
    }
class CardVisitScheduled: ParsableModel {
    
    var action1: Action?
    var action2: Action?
    var action3: Action?
    var contentHeader: String?
    var dateAndDay: String?
    var endDateTime: String?
    var phoneNo: String?
    var startDateTime: String?
    var timeSlot: String?
    
    
}
class CardCancelled: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var stageDescription: String?
    var highlight: String?
    var phoneNo: String?
    
}
class CardDCategory: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var address: String?
    var stageDescription: String?
    var phoneNo: String?
    
}
class CardSingleAction: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var actionParam1: String?
    var stageDescription: String?
    var highlight: String?
    var subDescription: String?
    var costBreakup: CostBreakup?
    
}
class CardReasonList: ParsableModel {
    
    var action1: Action?
    var contentHeader: String?
    var highlights: String?
    var pendency: DocPendency?
    
    
}

class DocPendency: ParsableModel {
    
    var doucmentPendency: [DoucmentPendency]?
    var otherPendencies: [OtherPendency]? // raj to do
    
}

class OtherPendency: PendencyCustomClass {
  
}
class PendencyCustomClass:ParsableModel{
    var name: String?
    var reason: String?
    var docId: String?
    var docTypeId: String?
    var documentKey: String?
    var status: String?
    var storageRefId: String?
    var value: String?
}
class DoucmentPendency: PendencyCustomClass {
}
class CardDocDescription: ParsableModel,JSONKeyCoder {
    
    var action1: Action?
    var contentHeader: String?
    var stageDescription: String?
    var files: [File]?
    
    
}
class File: ParsableModel {
    
    var docName: String?
    var storageRefId: String?
    var url: String?
    
}
class Action: ParsableModel {
    
    var enable: Bool = false
    var id: String?
    var name: String?
    var navigationAction: String?
}
class Stage: ParsableModel ,JSONKeyCoder{
    
    var action1: Action?
    var stageDescription: String?
    var id: String?
    var isExpanded: Bool = false
    var name: String?
    var state: NSNumber?
    var status: String?
    var miscData: MiscData?
    
    
}
class MiscData: ParsableModel {
    
    var berData: [BerData]?
    
    
}
class BerData: ParsableModel {
    
    var breakUp: BreakUp?
    var descriptionField: String?
    var header: String?
    
    
}
class BreakUp: ParsableModel {
    
    var key1: Key1?
    var key2: Key1?
    var key3: Key1?
    var key4: Key1?
    var key5: Key1?
    var subHeader: String?
    var subItems: [SubItem]?
    
    
}
class SubItem: ParsableModel {
    
    var descriptionField: String?
    var header: String?
    var name: String?
    var value: NSNumber?
    
    
}
class Key1: ParsableModel {
    
    var name: String?
    var value: NSNumber?
    
    
}
class Misc: ParsableModel {
    
    var createdOn: String?
    var icPartnerCode: NSNumber?
    var memId: String?
    var pincode: String?
    var productCode: String?
    var srId: String?
    var srType: String?
    var thirdPartyProps: ThirdPartyProp?
    var updateContact: UpdateContact?
    var srNumber: String?
    
    
}
class UpdateContact: ParsableModel {
    
    var items: [String]? // "email", "alternateMobileNo"
    var message: String?
    
    
}
class ThirdPartyProp: ParsableModel {
    
    var claimPartnerAttributesdetails: AnyObject?
    var deviceWarranty: AnyObject?
    var eddFromDate: String?
    var eddToDate: String?
    var hubId: AnyObject?
    var icMarketValue: AnyObject?
    var mu: AnyObject?
    var sigma: AnyObject?
    var sumAssured: NSNumber?
    
    var alternateMobileNo: String?
    var assetInvoiceNo: AnyObject?
    var assignee: String?
    var businessPartnerName: String?
    var componentDetailResponse: [ComponentDetailResponse]?
    var courtesyRequired: AnyObject?
    var coverAmount: NSNumber?
    var customerId: NSNumber?
    var defaultHub: NSNumber?
    var dob: String?
    var endDate: String?
    var firstName: String?
    var lastName: String?
    var middleName: AnyObject?
    var mobPurchageDate: String?
    var partnerName: String?
    var planCode: NSNumber?
    var planName: String?
    var productName: String?
    var purchaseDate: String?
    var srAge: NSNumber?
    var stageAge: String?
    var stageIntDate: NSNumber?
    var startDate: String?
    
}
class ComponentDetailResponse: ParsableModel {
    var scopeCode: NSNumber?
    var scopeName: String?
    var status: String?
    }
class JavaScriptTimeineRequestDTO:BaseRequestDTO{
    var timeLineData:Data!
    init(timeLineData:Data){
        self.timeLineData = timeLineData
    }
}

class JavaScriptTimelineDataRequestUsecase : BaseRequestUseCase<JavaScriptTimeineRequestDTO, JavaScriptTimeineResponceDTO> {

    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: JavaScriptTimeineRequestDTO?, completionHandler: @escaping (JavaScriptTimeineResponceDTO?, Error?) -> Void) {
        dataProvider.getJSTimelineCard(data: requestDto, completionHandler: completionHandler )
    }
    

}
