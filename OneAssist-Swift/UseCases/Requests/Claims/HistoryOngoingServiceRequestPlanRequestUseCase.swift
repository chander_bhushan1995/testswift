//
//  HistoryOngoingServiceRequestPlanRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 10/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class HistoryOngoingServiceRequestPlanRequestDTO: SearchServiceRequestPlanRequestDTO {
    convenience init(memIds: [String]) {
        self.init()
        
        /* For Ongoing: onHold, pending, inProgress[for ongoing SRs]  AND CO, CRES for rating. */
        /* For History: X, F, CUNR, CREJ  AND CO, CRES for rating. */
        status = "\(Constants.TimelineStatus.onHold),\(Constants.TimelineStatus.pending),\(Constants.TimelineStatus.inProgress),X,CO,F,CUNR,CRES,CREJ"
        referenceNumbers =  memIds.joined(separator: ",")
        sortOrder = "desc" // asc in case of ascending
    }
}

class HistoryOngoingServiceRequestPlanRequestUseCase: SearchServiceRequestPlanRequestUseCase {
    
}
