//
//  UpdateBankDetailsRequestUse.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 5/17/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

/// Request model to update bank details
class UpdateBankDetailsRequestDTO: BaseRequestDTO {

    var custID: String?
    let name: String?
    let accountNo: String?
    let bankCode: String?
    let cancelledCheque: Data?
    var srNumber: String?
    
    init(custId: String?, bankDetails: BankDetails?, file: Data?) {
        self.custID = custId
        self.name = bankDetails?.custName
        self.accountNo = bankDetails?.accountNo
        self.bankCode = bankDetails?.bankCode
        self.cancelledCheque = file
        self.srNumber = bankDetails?.srNumber
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        var parameters = super.createRequestParameters()
        
        //Remove extra parameters which is not part of request
        parameters?.removeValue(forKey: "custID")
        parameters?.removeValue(forKey: "srNumber")
        
        parameters?["cancelledCheque"] = cancelledCheque
        return parameters
    }
}

/// Use case to update bank details
class UpdateBankDetailsRequestUseCase : BaseRequestUseCase<UpdateBankDetailsRequestDTO, BaseResponseDTO> {
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: UpdateBankDetailsRequestDTO?, completionHandler: @escaping (BaseResponseDTO?, Error?) -> Void) {
        dataProvider.updateBankDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
