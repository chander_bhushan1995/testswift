//
//  ExcessPaymentRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 3/27/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class ExcessPaymentRequestDTO: BaseRequestDTO {
    
    var subscriptionType: String = "NS"
    var srId : String?
    var adviceId : String?
    
}

class ExcessPaymentResponseDTO: BaseResponseDTO {
    var uniqueId : String?
    var redirectURL: String?
}

class ExcessPaymentRequestUseCase : BaseRequestUseCase<ExcessPaymentRequestDTO, ExcessPaymentResponseDTO> {
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: ExcessPaymentRequestDTO?, completionHandler: @escaping (ExcessPaymentResponseDTO?, Error?) -> Void) {
        dataProvider.getExcessPayment(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}

