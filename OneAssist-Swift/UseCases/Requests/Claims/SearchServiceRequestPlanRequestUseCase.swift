//
//  SearchServiceRequestPlanRequestUseCase.swift
//  OneAssist-Swift
//
//  Searchd by Varun on 06/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class SearchServiceRequestPlanRequestDTO: BaseRequestDTO {
    var assignee: String?
    var servicePartnerCode: String?
    var servicePartnerBUCode: String?
    var status: String?
    var todate: String?
    var fromdate: String?
    var serviceRequestId: String?
    var workFlowStage: String?
    var workFlowStageStatus:String?
    var referenceNumbers: String?
    var page: String?
    var size: String?
    var sortBy:String?
    var sortOrder:String?
    var feedbackStatus:String?
    var requiresAdditionalDetails:String? = "Y"
    var technicianProfileRequired: String? = "Y"
    var refPrimaryTrackingNo: String?
    var options = "showAssets,showScopes,costToCustomerDetails,walkInAscDetails,incidentDetail"
    var isWalkInSRsRequired:String? = "Y"
    
    //TODO: only for HA
//    var serviceRequestTypes: String? = "HA_EW,HA_BR,HA_BD,HA_AD,HA_FR"
    var serviceRequestTypes: String? = "PE_EW,PE_THEFT,PE_ADLD,HA_EW,HA_BR,HA_BD,HA_AD,HA_FR,PMS"
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}
class PayLaterResponseDTO:BaseResponseDTO{
    
}

class PayLaterRequestDTO: BaseRequestDTO {
    var serviceRequestId: NSNumber?
    var workflowData: ADLDWorkflowData?
    init(workflowData:ADLDWorkflowData?,serviceRequestId:NSNumber?){
        self.workflowData = workflowData
        self.serviceRequestId = serviceRequestId
        
    }
}

class ADLDWorkflowData {
    
    var repair: ADLDRepair?
    init(repair:ADLDRepair?){
        self.repair = repair
    }
    
}
class ADLDRepair {
    
    var payment: ADLDPayment?
    init(payment:ADLDPayment?){
        self.payment = payment
    }
    
}
class ADLDPayment {
    
    var status:String?
    init(status:String?){
        self.status = status
    }
    
}
extension JSONKeyCoder {
    public func key(for key: String) -> String? {
        if key == "stageDescription" {
            return "description"
        }
        return nil
    }
}

class SearchServiceRequestPlanResponseDTO: BaseResponseDTO {
    var data: [SearchService]?
    var totalElements: NSNumber?
    var totalPages: NSNumber?
    var sendJSData: String?
}

class SearchService: ParsableModel {
    
    var activitiHistoryTasks: AnyObject?
    var actualEndDateTime: String?
    var actualStartDateTime: String?
    var additionalDetails: AdditionalDetails?
    var assets: [SRAsset]?
    var assignee: String?
    var createdBy: String?
    var createdOn: String?
    var dueDateTime: String?
    var initiatingSystem: NSNumber?
    var modifiedBy: String?
    var modifiedOn: String?
    var pendency: Pendency?
    var incidentDetail: IncidentDetail?
    var refPrimaryTrackingNo: String? // This is CRM Service Request ID, which we will show to the user
    var refSecondaryTrackingNo: String?
    var referenceNo: String? // This is membership ID
    var requestDescription: String?
    var scheduleSlotEndDateTime: String?
    var scheduleSlotStartDateTime: String?
    var serviceAddress: ServiceAddress?
    var serviceDocuments: [ServiceDocument]?
    var servicePartnerBuCode: NSNumber?
    var servicePartnerCode: NSNumber?
    var dateOfIncident: String?


    var serviceRequestFeedback: ServiceFeedbackModel?
    var technicianProfileDetails: TechnicialDetail?
    var thirdPartyProperties: ThirdPartyProperty?
    
    var serviceRequestId: NSNumber?
    var showDownloadJobsheet: NSNumber = NSNumber(booleanLiteral: false)
    var serviceRequestType: String?
    var serviceRequestSourceType: String?
    var customerId: NSNumber?
    var shipmentDetails: AnyObject?
    var status: String?
    var workflowAlert: AnyObject?
    var workflowData: WorkflowData?
    var workflowProcDefKey: String?
    var workflowProcessId: String?
    var workflowStage: String?
    var workflowStageStatus: String?
    var isWalkInAsc:String?
    var insurancePartnerCode: NSNumber?
}

class AdditionalDetails: ParsableModel {
    var gifterName: String?
    var customerGifterRelation: String?
    var invoiceNameVariationReason: String?
    var mismatchCustomerName: String?
    var customerNameInvoiceMismatch: String?
    
    
    func isGifterProfRequired() -> Bool {
        var isGifterPrrofRequired = false
        if let invoiceMismatch = self.customerNameInvoiceMismatch,
           let variationReason = self.invoiceNameVariationReason,
           invoiceMismatch == "Y", variationReason == Strings.GifterScenario.GifterTypeValue.gifterSomeoneType {
            isGifterPrrofRequired = true
        }
        return isGifterPrrofRequired
    }
}

class ThirdPartyProperty: ParsableModel {
    var claimPartnerAttributesdetails: String?
    var deviceWarranty: String?
    var hubId: NSNumber?
    var icMarketValue: NSNumber?
    var sumAssured: NSNumber?
    var mu: NSNumber?
    var sigma: NSNumber?
    var eddFromDate: String?
    var eddToDate: String?
}

class SRAsset:ParsableModel {
    var assetAge: AnyObject?
    var assetId: String?
    var assetInspectionStatus: AnyObject?
    var assetReferenceNo: String?
    var assetSize: AnyObject?
    var assetTechnology: AnyObject?
    var assetUnit: AnyObject?
    var createdBy: AnyObject?
    var createdOn: String?
    var isAccidentalDamage: AnyObject?
    var isFunctional: AnyObject?
    var isInformationCorrect: AnyObject?
    var make: String?
    var modelNo: AnyObject?
    var modifiedBy: AnyObject?
    var modifiedOn: String?
    var productCode: String?
    var serialNo: String?
    var serviceRequestAssetDocuments: [AnyObject]?
    var serviceRequestId: NSNumber?
    var status: AnyObject?
}

class ServiceFeedbackModel:ParsableModel{
    var feedbackCode:[String]?
    var feedbackRating:String?
    var feedbackComments:String?
}

class WorkflowData: ParsableModel {
    
    var additionalAttributes: AdditionalAttribute?
    var claimSettlement: ClaimSettlement?
    var completed: Completed?
    var delivery: Delivery?
    var documentUpload: DocumentUpload?
    var icDoc: IcDoc?
    var insuranceDecision: InsuranceDecision?
    var partnerStageStatus: InsuranceDecision?
    var pickup: Pickup?
    var repair: Repair?
    var repairAssessment: RepairAssessment?
    var softApproval: SoftApproval?
    var verification: VerificationStatus?
    var visit: Visit?
}

class IncidentDetail: ParsableModel{
    var incidentDescription: [IncidentDescriptions]?
}

class IncidentDescriptions: ParsableModel {
    var createdBy: NSNumber?
    var createdOn: String?
    var displayName: String?
    var id: String?
    var key: String?
    var modifiedBy: String?
    var modifiedOn: String?
    var serviceRequestId: NSNumber?
    var value: String?
    var verificationStatus: String?
}


class VerificationStatus:BaseWorkFlowStage, JSONKeyCoder {
    var damageLossDateTimeVerRemarks: AnyObject?
    var damageLossDateTimeVerStat: AnyObject?
    var documentId: AnyObject?
    var remarks: AnyObject?
    var verificationNewCases: AnyObject?
    var payment : Payment?
}

class Payment : ParsableModel {
    
    var chargeId : String?
    var adviceAmount : String?
    var adviceId : String?
    var transactionAmount: NSNumber?
}

class Visit: BaseWorkFlowStage, JSONKeyCoder {
    
    var dateOfIncident: String?
    var estimatedInvoiceUploadStaus: AnyObject?
    var isSelfService: String?
    var issueReportedByCustomer: [IssueReportedByCustomer]?
    var placeOfIncident: String?
    var serviceAddress: String?
    var serviceCancelReason: AnyObject?
    var serviceStartCode: String?
   
}

class SoftApproval: BaseWorkFlowStage, JSONKeyCoder {
   
}

class BaseWorkFlowStage: ParsableModel {
    var status: String?
    var statusCode: String?
    var stageDescription: String?

}

class RepairAssessment: BaseWorkFlowStage, JSONKeyCoder {
    
    var accidentalDamage: AnyObject?
    var costToCompany: AnyObject?
    var costToCustomer: String?
    var customerDecision: AnyObject?
    var diagonosisReportedbyAssignee: AnyObject?
    var isCustomerAgree: AnyObject?
    var isSpareRequestRaisedWithIC: String?
    var labourCost: AnyObject?
    var sparePartsRequired: [SparePartsRequired]?
    var transport: AnyObject?
    var estimateAmt: NSNumber?
    var overrunCoverAmount: String?
}

class SparePartsRequired: ParsableModel {
    
    var cost: String?
    var createdBy: String?
    var documentId: String?
    var isInsuranceCovered: String?
    var modifiedBy: String?
    var remarks: AnyObject?
    var sparePartAvailable: String?
    var sparePartDescription: String?
    var sparePartId: String?
    var status: String?
}

class Repair: BaseWorkFlowStage, JSONKeyCoder {
    var cameraStatus: AnyObject?
    var resolutionCode: AnyObject?
    var serviceEndCode: String?
    var touchScreenStatus: AnyObject?
    var payment: Payment?
    var advices: [Advices]?
    var scopeDetails: [ScopeDetails]?
}

class Advices: ParsableModel {
    var accountNumber: AnyObject?
    var adviceAmount: NSNumber?
    var adviceDate: AnyObject?
    var adviceId: NSNumber?
    var chargeId: NSNumber?
    var chargeType: AnyObject?
    var membershipId: AnyObject?
    var orderId: AnyObject?
    var partnerType: NSNumber?
    var status: AnyObject?
    var transactionAmount: AnyObject?
    var transactionDate: AnyObject?
}

class ScopeDetails: ParsableModel {
    var cost: NSNumber?
    var createdBy: NSNumber?
    var createdOn: String?
    var isCovered: String?
    var modifiedBy: AnyObject?
    var modifiedOn: String?
    var remarks: AnyObject?
    var scopeEstimationId: NSNumber?
    var scopeId: NSNumber?
    var scopeName: String?
    var serviceRequestId: NSNumber?
    var status: String?
    var workFlowStage: String?
}

class InsuranceDecision: BaseWorkFlowStage, JSONKeyCoder {
    
    var deliveryICApproved: AnyObject?
    var deliveryICBerApproved: AnyObject?
    var deliveryICRejected: AnyObject?
    var deliveryToAscStatus: AnyObject?
    var excessAmtApproved: AnyObject?
    var excessAmtReceived: AnyObject?
    var icDecision: NSNumber?
    var icDepriciation: NSNumber?
    var icEstimateAmt: NSNumber?
    var icExcessAmt: NSNumber?
    var icUnderInsur: NSNumber?
    var payment: Payment?
    var paymentAmt: NSNumber?
    var paymentDate: AnyObject?
    var reinstallPrem: NSNumber?
    var salvageAmt: NSNumber?

}

class Pickup: BaseWorkFlowStage, JSONKeyCoder  {
    var pickupQc: AnyObject?
    var verifier: AnyObject?
    var pincodeCategory: String?

//    var ascHub: AnyObject?
//    var fulfilmentHubId: AnyObject?
//    var pickupDate: AnyObject?
//    var status: String?
}

class Delivery: BaseWorkFlowStage, JSONKeyCoder {
    
    var actDelvDate: String?
    var bankAccountVerificationStatus: String?
    var bankUtrn: String?
//    var bankAccountPaymentRemarks: AnyObject?
//    var bankAccountRefNo: AnyObject?
//    var bankAccountVerificationSubStatus: AnyObject?
    
//    var courtesyRaised: AnyObject?
//    var delvDueDate: AnyObject?
//    var status: String?
}

class Completed: BaseWorkFlowStage, JSONKeyCoder {
    
    var refundAmount: AnyObject?
  
}

class IcDoc: BaseWorkFlowStage, JSONKeyCoder {
    
    var estimatedInvoiceVerificationStatus: AnyObject?
    
}

class ClaimSettlement: BaseWorkFlowStage, JSONKeyCoder {
    
    var claimAmount: AnyObject?
    var settlementDate: AnyObject?
}

class ServiceAddress: ParsableModel {
    
    var addressLine1: String?
    var addressLine2: String?
    var addresseeFullName: String?
    var countryCode: String?
    var createdBy: AnyObject?
    var createdOn: String?
    var district: String?
    var email: String?
    var landmark: String?
    var mobileNo: NSNumber?
    var modifiedBy: AnyObject?
    var modifiedOn: AnyObject?
    var pincode: String?
    var serviceAddressId: NSNumber?
    var status: AnyObject?
}

class AdditionalAttribute: ParsableModel {
    var claimCount: NSNumber?
    var courtesyRequired: Bool?
    var deliveryICApproved: AnyObject?
    var deliveryICBerApproved: AnyObject?
    var statusCode: String?
}

class Pendency: ParsableModel {
    var documentUpload: DocumentUpload?
}

class DocumentUpload: BaseWorkFlowStage, JSONKeyCoder {
    var documents: [Document]?
    var incidenceDate: DocumentStatu?
    var incidenceDescription: DocumentStatu?
    var incidenceDescriptionRejectionDetail: DocumentStatu?
    var storageRefId: AnyObject?
    var placeOfIncident: String?
    // PE Cases
    var deviceBreakdownDetail: DeviceBreakdownDetail?
    var mobileDamageDetails: MobileDamageDetail?
    var mobileLossDetails: MobileLossDetail?
}

class DeviceBreakdownDetail: ParsableModel {
    
    var breakDownIsSwitchedOn: String?
    var breakDownIsTouchWorks: String?
    var breakDownPart: String?
    var breakDownPlace: String?
    var breakDownType: String?
    var otherbreakDownPart: String?
    var purchaseInvoice: String?
}

class MobileDamageDetail: ParsableModel {
    
    var damageIsSwitchedOn: String?
    var damageIsTouchWorks: String?
    var damagePart: String?
    var damagePlace: String?
    var damageType: String?
    var otherDamagePart: String?
    var purchaseInvoice: String?
}

class MobileLossDetail: ParsableModel {
    var simBlockRefNo: String?
    var simBlockReqDate: String?
    var lossFirCompDate: String?
    var lossfir: String?
}

class Document: ParsableModel {
    var documentId: String?
    var documentStatus: DocumentStatu?
    var documentKey: String?
    var documentTypeId: String?
    var documentName: String?
    var isMandatory: String?
}

class DocumentStatu: ParsableModel {
    var remarks: String?
    var status: String?
    var key: String?
    var displayName: String?
    var value: String?
}

class ServiceDocument: ParsableModel {
    var createdBy: String?
    var createdOn: String?
    var documentId: String?
    var documentName: String?
    var documentTypeId: NSNumber?
    var modifiedBy: String?
    var modifiedOn: String?
    var serviceRequestId: NSNumber?
    var status: String?
    var statusCode: String?
    var storageRefId: String?
    
    var docCategory: String?
    var documentKey: String?
    var documentNo: String?
}

//MARK: - Drafts against a membership

class DraftsMembershipRequestDTO: BaseRequestDTO {
    var referenceNumbers : String = ""
    convenience init(memIds: [String]) {
        self.init()
        referenceNumbers =  memIds.joined(separator: ",")
    }
}

class DraftsMembershipResponseDTO:BaseResponseDTO{
    var data: [IntimationData]?
}

class IntimationData: ParsableModel {
    var referenceNo: String?
    var intimation: [Intimation]?
}

class Intimation: ParsableModel {
    var status: String?
    var createdOn: String?
    var modifiedOn: String?
    var createdBy: String?
    var modifiedBy: String?
    var id: String?
    var referenceNo: String?
    var serviceCategory: String?
}

class SearchServiceRequestPlanRequestUseCase: BaseRequestUseCase<SearchServiceRequestPlanRequestDTO, SearchServiceRequestPlanResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: SearchServiceRequestPlanRequestDTO?, completionHandler: @escaping (SearchServiceRequestPlanResponseDTO?, Error?) -> Void) {
        responseDataProvider.searchServiceRequestPlan(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}

class PayLaterRequestUseCase: BaseRequestUseCase<PayLaterRequestDTO, PayLaterResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: PayLaterRequestDTO?, completionHandler: @escaping (PayLaterResponseDTO?, Error?) -> Void) {
        responseDataProvider.payLater(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}


class DraftsMembershipUseCase: BaseRequestUseCase<DraftsMembershipRequestDTO, DraftsMembershipResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: DraftsMembershipRequestDTO?, completionHandler: @escaping (DraftsMembershipResponseDTO?, Error?) -> Void) {
        responseDataProvider.draftMembership(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}


