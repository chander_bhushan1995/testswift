//
//  WhatHappendRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation


class WhatHappendRequestDTO: BaseRequestDTO {
    var productCode: String?
    var taskType: String?
    var status: String = "A"
    var productVariantId: NSNumber?
    var productVariantIds: [String]? // for sod recomended by sanjeev gupta
    var options: String?
}

class WhatHappendResponseDTO: BaseResponseDTO {
    var data:[WhatHappendData]?
}
class WhatHappendData:ParsableModel{
    var taskName:String?
    var serviceTaskId : NSNumber?
    var taskDescription:String?
    var cost: NSNumber?
    var productVariantId: NSNumber?
}

class WhatHappendRequestUseCase: BaseRequestUseCase<WhatHappendRequestDTO, WhatHappendResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: WhatHappendRequestDTO?, completionHandler: @escaping (WhatHappendResponseDTO?, Error?) -> Void) {
        responseDataProvider.whatHappend(requestDto: requestDto!, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
