//
//  DeviceConditionRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 07/05/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

import Foundation

class DeviceConditionRequestDTO: BaseRequestDTO {
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class DeviceConditionResponseDTO: BaseResponseDTO {
    var data: [DeviceCondition]?
}

class DeviceCondition: ParsableModel {
    var createdBy: String?
    var createdOn: String?
    var key: String?
    var keySetDescription: String?
    var keySetId: NSNumber?
    var keySetName: String?
    var modifiedBy: String?
    var modifiedOn: String?
    var status: String?
    var value: String?
    var valueId: NSNumber?
}

class DeviceConditionRequestUseCase: BaseRequestUseCase<DeviceConditionRequestDTO, DeviceConditionResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: DeviceConditionRequestDTO?, completionHandler: @escaping (DeviceConditionResponseDTO?, Error?) -> Void) {
        responseDataProvider.getDeviceCondition(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
