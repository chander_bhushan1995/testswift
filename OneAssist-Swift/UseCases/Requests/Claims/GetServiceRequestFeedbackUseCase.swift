//
//  GetServiceRequestFeedbackUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class GetServiceRequestFeedbackRequestDTO: SearchServiceRequestPlanRequestDTO {
    convenience init(memberships:[String]) {
        self.init()
        workFlowStageStatus = "SU,IP,CO"
        workFlowStage = "CO,DS,CS"
        feedbackStatus = "NA"
        page = "1"
        size = "1"
        sortBy = "actualEndDateTime"
        sortOrder = "desc"
        referenceNumbers = memberships.joined(separator: ",")
        serviceRequestTypes = nil
        requiresAdditionalDetails = nil
        technicianProfileRequired = nil
    }
}
class GetServiceRequestFeedbackUseCase:SearchServiceRequestPlanRequestUseCase {
    
    override func getRequest(requestDto: SearchServiceRequestPlanRequestDTO?, completionHandler: @escaping (SearchServiceRequestPlanResponseDTO?, Error?) -> Void) {
        responseDataProvider.getFeedbackRequest(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
class ServiceRequestGetFeedback:BasePresenter{
    func getFeedbackWithCompletion(membershipsIDs:[String], completionHandler: @escaping(SearchServiceRequestPlanResponseDTO?, Error?) -> Void) {

    let req = GetServiceRequestFeedbackRequestDTO(memberships:membershipsIDs)
        let _ = GetServiceRequestFeedbackUseCase.service(requestDTO: req, completionBlock: {[weak self] (usecase, detailsResponse, error) in
                    do {
                        try self?.checkError(detailsResponse, error: error)
                        completionHandler(detailsResponse,nil)
                    } catch {
                        completionHandler(nil,error)
                    }
                })
            }
}
