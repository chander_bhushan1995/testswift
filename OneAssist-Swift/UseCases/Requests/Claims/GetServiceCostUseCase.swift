//
//  GetServiceCostUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 23/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class GetServiceCostRequestDTO: BaseRequestDTO {
    
    var serviceRequestType: String?
    var productCode: String?
    
    init(serviceType: String?, prodCode: String?) {
        serviceRequestType = serviceType
        productCode = prodCode
    }
}

class GetServiceCostResponseDTO: BaseResponseDTO {
    var data: [ServiceCostDetails]?
}

class ServiceCostDetails: ParsableModel {
    var createdBy: String?
    var createdOn: String?
    var id: NSNumber?
    var productCode: String?
    var productTechnology: String?
    var serviceCategory: String?
    var serviceCost: NSNumber?
    var serviceRequestType: String?
    var status: String?
}

class GetServiceCostRequestUseCase: BaseRequestUseCase<GetServiceCostRequestDTO, GetServiceCostResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetServiceCostRequestDTO?, completionHandler: @escaping (GetServiceCostResponseDTO?, Error?) -> Void) {
        responseDataProvider.getServiceCost(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
