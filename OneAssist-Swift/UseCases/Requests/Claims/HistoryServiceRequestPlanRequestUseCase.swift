//
//  HistoryServiceRequestPlanRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 09/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
class HistoryServiceRequestPlanRequestDTO: SearchServiceRequestPlanRequestDTO {
    
    convenience init(memberships:[String], isABB: Bool = false) {
        self.init()
//        status = Constants.TimelineStatus.completed + "," + Constants.TimelineStatus.completeUnresolve
        if isABB {
            serviceRequestTypes = "PE_EW,PE_THEFT,PE_ADLD,HA_EW,HA_BR,HA_BD,HA_AD,HA_FR,PMS,BUY_BACK"
            status = "X,CO,F,CUNR,CRES,CREJ,P,IP"
        } else {
            serviceRequestTypes = "PE_EW,PE_THEFT,PE_ADLD,HA_EW,HA_BR,HA_BD,HA_AD,HA_FR,PMS"
            status = "X,CO,F,CUNR,CRES,CREJ"
        }
        referenceNumbers = memberships.joined(separator: ",")
    }
}
class HistoryServiceRequestPlanRequestUseCase:SearchServiceRequestPlanRequestUseCase{

}
