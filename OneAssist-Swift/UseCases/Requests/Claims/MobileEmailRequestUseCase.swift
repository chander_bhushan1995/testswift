//
//  MobileEmailRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Raj on 22/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation


class MobileEmailResponseDTO:BaseResponseDTO{
    
}

class MobileEmailRequestDTO: BaseRequestDTO {

    var serviceRequestAddressDetails: ServiceRequestAddressDetail2?
    var serviceRequestId: NSNumber?

    init(serviceRequestAddressDetails:ServiceRequestAddressDetail2?,serviceRequestId:NSNumber?){
        self.serviceRequestAddressDetails = serviceRequestAddressDetails
        self.serviceRequestId = serviceRequestId
    }
}


class ServiceRequestAddressDetail2:BaseRequestDTO {
    var alternateMobileNo: NSNumber?
    var email: String?
    init(alternateMobileNo:NSNumber?,email:String?){
        self.email = email
        self.alternateMobileNo = alternateMobileNo
    }
}
class MobileEmailRequestUseCase : BaseRequestUseCase<MobileEmailRequestDTO, MobileEmailResponseDTO> {
    
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: MobileEmailRequestDTO?, completionHandler: @escaping (MobileEmailResponseDTO?, Error?) -> Void) {
        dataProvider.updateAlternateMobileEmail(requestDto: requestDto,taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler )
    }
    
}
