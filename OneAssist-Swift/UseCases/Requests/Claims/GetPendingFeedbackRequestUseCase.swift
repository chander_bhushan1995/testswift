//
//  GetPendingFeedbackRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 06/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
class GetPendingFeedbackRequestDTO: BaseRequestDTO {
    var productCode:String?
    var taskType:String?
    var status:String?
    override func createRequestParameters() -> [String : Any]? {
        
        let parameters = super.createRequestParameters()
        
        return parameters
    }
}

class GetPendingFeedbackResponseDTO: BaseResponseDTO {
    var data:[GetPendingFeedbackData]?
}
class GetPendingFeedbackData:ParsableModel{
    var taskName:String?
}

class GetPendingFeedbackRequestUseCase: BaseRequestUseCase<GetPendingFeedbackRequestDTO, GetPendingFeedbackResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetPendingFeedbackRequestDTO?, completionHandler: @escaping (GetPendingFeedbackResponseDTO?, Error?) -> Void) {
        responseDataProvider.getPendingFeedback(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}

