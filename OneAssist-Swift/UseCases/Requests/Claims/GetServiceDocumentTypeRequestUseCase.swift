//
//  GetServiceDocumentTypeRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 29/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class GetServiceDocumentTypeRequestDTO: BaseRequestDTO {
    
    var serviceRequestTypeId: String?
    var referenceCode: String?
    var insurancePartnerCode: String?
    var isMandatory: String?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
    
    init(srType: Constants.Services, productCode: String?, insurancePartnerCode: String?, isMandatory: String?) {
        self.serviceRequestTypeId = srType.rawValue
        self.referenceCode = productCode
        self.insurancePartnerCode = insurancePartnerCode
        self.isMandatory = isMandatory
    }
}

class GetServiceDocumentTypeResponseDTO: BaseResponseDTO {
    var data: [ServiceDocumentType]?
}

class ServiceDocumentType: ParsableModel, JSONKeyCoder {
    var typeDescription: String?
    var docKey: String?
    var docName: String?
    var docTypeId: NSNumber?
    var insurancePartnerCode: String?
    var isMandatory: String?
    var referenceCode: String?
    var docCategory: String?
    var displaytext: String?
    var displayName: String?
    
    func key(for key: String) -> String? {
        if key == "typeDescription" {
            return "description"
        }
        return nil
    }
}

class GetServiceDocumentTypeRequestUseCase: BaseRequestUseCase<GetServiceDocumentTypeRequestDTO, GetServiceDocumentTypeResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetServiceDocumentTypeRequestDTO?, completionHandler: @escaping (GetServiceDocumentTypeResponseDTO?, Error?) -> Void) {
        let checker = DocumentTypeCacheChecker()
        responseDataProvider.getServiceDocumentType(requestDto: requestDto, apiCacheChecker: checker, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
