//
//  GetDocumentDetailsUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 19/11/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class GetDocumentDetailsRequestDTO: BaseRequestDTO {
    
    var custId: String?
    var docCategory: String?
    var assetId: String?
    var category: String?
    var memId: String?
    
    init(custId: String?, docCategory: String?, assetId: String?, assetCategory: String?, memId: String?) {
        self.custId = custId
        self.assetId = assetId
        self.docCategory = docCategory
        self.category = assetCategory
        self.memId = memId
    }
}

class GetDocumentDetailsResponseDTO: BaseResponseDTO {
    var data: [DocumentDetails]?
}

class DocumentDetails: ParsableModel {
    var docCategory: String?
    var documentKey: String?
    var documentName: String?
    var documentNo: String?
    var storageRefId: String?
    var assetId: NSNumber?

}

class GetDocumentDetailsRequestUseCase: BaseRequestUseCase<GetDocumentDetailsRequestDTO, GetDocumentDetailsResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetDocumentDetailsRequestDTO?, completionHandler: @escaping (GetDocumentDetailsResponseDTO?, Error?) -> Void) {
        responseDataProvider.getDocumentDetails(requestDto: requestDto!, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
