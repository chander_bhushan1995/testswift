//
//  CreateServiceRequestPlanRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 05/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class CreateServiceRequestPlanRequestDTO: BaseRequestDTO {
    
    var draftId: String?
    var createdBy: String?
    var incidentDescription: String?
    var dateOfIncident: String?
    var initiatingSystem: NSNumber = 21
    var issueReportedByCustomer: [IssueReportedByCustomer]?
    var placeOfIncident: String?
    var refPrimaryTrackingNo: String?
    var refSecondaryTrackingNo: String? // Includes assest id in , separated manner
    var referenceNo: String?
    var requestDescription: String?
    var scheduleSlotEndDateTime: String?
    var scheduleSlotStartDateTime: String?
    var serviceRequestAddressDetails: ServiceRequestAddressDetail?
    var serviceRequestType: String?
    var workFlowValue: String = "1"
    var insurancePartnerCode: NSNumber?
    var customerId: String?
    var assets: [Product]?
    var insuranceBacked: String?
    var additionalDetails: [String: Any]?
    override func createRequestParameters() -> [String : Any]? {
        let dict = JSONParserSwift.getDictionary(object: self)
        return dict
    }
}

//MARK: PE Theft

class CreatePETheftServiceRequestPlanRequestDTO: CreateServiceRequestPlanRequestDTO {

    var incidentDetail: PETheftIncidentDetail?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class PETheftIncidentDetail {
    var simBlockRefNo: String?
    var simBlockReqDate: String?
    var lossFirCompDate: String?
    var lossfir: String?
}

//MARK: PE ADLD

class CreatePEADLDServiceRequestPlanRequestDTO: CreateServiceRequestPlanRequestDTO {
    var incidentDetail: [String: Any]?
    
    override func createRequestParameters() -> [String : Any]? {
        var dict = JSONParserSwift.getDictionary(object: self)
        dict["incidentDetail"] = incidentDetail
        return dict
    }
}

class PEADLDDamageDetail: BaseRequestDTO {
    var damageType: String?
    var damagePlace: String?
    var damagePart: String?
    var purchaseInvoice: String?
    var otherDamagePart: String?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

//MARK: PE EW

class CreatePEEWServiceRequestPlanRequestDTO: CreateServiceRequestPlanRequestDTO {
    var incidentDetail: [String: Any]?
    
    override func createRequestParameters() -> [String : Any]? {
        var dict = JSONParserSwift.getDictionary(object: self)
        dict["incidentDetail"] = incidentDetail
        return dict
    }
}

class PEEWBreakDownDetail: BaseRequestDTO {
    var breakDownType: String?
    var breakDownPlace: String?
    var breakDownPart: String?
    var purchaseInvoice: String?
    var otherbreakDownPart: String?
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class ServiceRequestAddressDetail: ParsableModel {
    var addressLine1: String?
    var addressLine2: AnyObject?
    var addresseeFullName: String?
    var countryCode: String?
    var district: String?
    var landmark: String?
    var email: String?
    var mobileNo: String?
    var pincode: String?
}

class IssueReportedByCustomer: ParsableModel {
    var issueDescription: String?
    var issueId: String?
}

class CreateServiceRequestPlanResponseDTO: BaseResponseDTO {
    var data: CreateService?
}

class CreateService: ParsableModel {
    var actualEndDateTime: AnyObject?
    var actualStartDateTime: AnyObject?
    var assignee: AnyObject?
    var createdBy: String?
    var createdOn: AnyObject?
    var dateOfIncident: String?
    var dueDateTime: AnyObject?
    var incidentDescription: AnyObject?
    var initiatingSystem: NSNumber?
    var issueReportedByCustomer: [IssueReportedByCustomer]?
    var message: AnyObject?
    var modifiedBy: AnyObject?
    var modifiedOn: AnyObject?
    var otp: AnyObject?
    var placeOfIncident: String?
    var refPrimaryTrackingNo: String?
    var refSecondaryTrackingNo: String?
    var referenceNo: String?
    var remarks: AnyObject?
    var requestDescription: String?
    var scheduleSlotEndDateTime: String?
    var scheduleSlotStartDateTime: String?
    var serviceCancelReason: AnyObject?
    var servicePartnerBuCode: AnyObject?
    var servicePartnerCode: AnyObject?
    var serviceRequestAddressDetails: ServiceRequestAddressDetail?
    var serviceRequestFeedback: AnyObject?
    var serviceRequestId: NSNumber?
    var serviceRequestType: String?
    var serviceRequestTypeId: AnyObject?
    var status: String?
    var thirdPartyProperties: AnyObject?
    var workFlowValue: NSNumber?
    var workflowAlert: AnyObject?
    var workflowData: AnyObject?
    var workflowJsonString: AnyObject?
    var workflowProcDefKey: AnyObject?
    var workflowProcessId: AnyObject?
    var workflowStage: AnyObject?
    var workflowStageStatus: AnyObject?
}

class CreateServiceRequestPlanRequestUseCase: BaseRequestUseCase<CreateServiceRequestPlanRequestDTO, CreateServiceRequestPlanResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: CreateServiceRequestPlanRequestDTO?, completionHandler: @escaping (CreateServiceRequestPlanResponseDTO?, Error?) -> Void) {
        responseDataProvider.createServiceRequestPlan(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
