//
//  ServiceCenterListRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Raj on 02/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class CityCodeResponseDTO: BaseResponseDTO {
    
    var data: CityData?
   
}

class CityData: ParsableModel {
    
    var cityCode: String?
    var cityName: String?
    var stateCode: String?
    var stateName: String?
    
}


class ServiceCenterListResponceDTO: BaseResponseDTO {

    var data: [PartnerBusinessUnitList]?
}

class PartnerBusinessUnitList: ParsableModel {

    var authorisedBy: String?
    var authorisedOn: String?
    var businessUnitCode: String?
    var businessUnitName: String?
    var businessUnitStatus: String?
    var city: String?
    var pincode:String?
    var corporateFaxNumber: NSNumber?
    var corporateLandlineNumber: NSNumber?
    var corporateMobileNumber: NSNumber?
    var createdBy: String?
    var createdOn: String?
    var fax: NSNumber?
    var gstAddress: String?
    var isWalkin: String?
    var landline: NSNumber?
    var latitude: String?
    var longitude: String?
    var mobile: NSNumber?
    var modifiedBy: String?
    var modifiedOn: String?
    var partnerBUOperationHours: [PartnerBUOperationHour]?
    var partnerBusinessUnitCode: NSNumber?
    var partnerCode: NSNumber?
    var roleNames: [String]?
    var businessUnitType: String?
    var corporateAltEmailId: String?
    var emailId: String?
    var industry: AnyObject?
    var landmark: String?
    var line1: AnyObject?
    var line2: AnyObject?
    var state: String?
    var status: String?
}

class PartnerBUOperationHour: ParsableModel {

    var createdBy: String?
    var createdDate: String?
    var modifiedBy: String?
    var modifiedDate: String?
    var operationTimeFrom: String?
    var operationTimeTo: String?
    var operational: String?
    var partnerBuCode: NSNumber?
    var sortNumber: NSNumber?
    var weekDay: String?
    


}

class TempPartnerBUOperationHour {
    var operationTimeFrom: String?
    var operationTimeTo: String?
    var operational: String?
    var weekDay: String?
    init(operationTimeFrom:String?,operationTimeTo:String?,weekDay:String?){
        self.operationTimeFrom = operationTimeFrom
        self.operationTimeTo = operationTimeTo
        self.weekDay = weekDay
    }
}
class ServiceCenterListRequestDTO: BaseRequestDTO {
    var pincode:String
    init(pincode:String){
        self.pincode = pincode
    }
}

class ServiceCenterClusterMarkerModel: NSObject, GMUClusterItem {
    var centerName : String?
    var centerAddress : String?
    var position: CLLocationCoordinate2D
    var inactiveIcon: UIImage?
    var activeIcon : UIImage?
    var mobile: String?
    var openToday:String?
    var clossesAt:String?
    var distance:Double!
    var kmDistance:String!
    var timingDetails:[PartnerBUOperationHour]?
    init(position: CLLocationCoordinate2D,centerName:String?,centerAddress:String?,activeIcon:UIImage?,mobile:String?,openToday:String?,clossesAt:String?,timingDetails:[PartnerBUOperationHour]?,distance:Double,kmDistance:String!) {
        self.position = position
        self.centerName = centerName
        self.centerAddress = centerAddress
        self.activeIcon = activeIcon
        self.mobile =  mobile
        self.timingDetails = timingDetails
        self.openToday  = openToday
        self.clossesAt = clossesAt
        self.distance = distance
        self.kmDistance = kmDistance
    }
}
class ServiceCenterListRequestUseCase : BaseRequestUseCase<ServiceCenterListRequestDTO, ServiceCenterListResponceDTO> {
    
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: ServiceCenterListRequestDTO?, completionHandler: @escaping (ServiceCenterListResponceDTO?, Error?) -> Void) {
        
        dataProvider.getServiceCenterList(requestDto: requestDto,taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler )
    }
    
    
}
