//
//  AddressRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
class AddressRequestDTO: BaseRequestDTO {
    var custID:String?
    var pincode:String?
    
    override func createRequestParameters() -> [String : Any]? {
        
        let parameters = super.createRequestParameters()
        return JSONParserSwift.getDictionary(object:self)
       // return parameters
    }
}



