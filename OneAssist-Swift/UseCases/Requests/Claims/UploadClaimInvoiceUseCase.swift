//
//  UploadClaimInvoiceUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 09/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class UploadClaimInvoiceRequestDTO: BaseRequestDTO {
    
//    serviceplatform/api/servicerequests/{serviceRequestId}/documents?documentId={{documentId}}&documentTypeId={{documentTypeId}}&entityName=ASSET&entityId={{assetId}}
    
    let serviceRequestId: String
    let documentTypeId: String?
    let documentId: String?
    var entityName: String?
    var entityId: String?
    let files: Data?
    
    init(serviceRequestId: String, documentTypeId: String?, documentId: String? = nil, file: Data?) {
        self.serviceRequestId = serviceRequestId
        self.documentTypeId = documentTypeId
        self.documentId = documentId
        self.files = file
    }
    
    override func createRequestParameters() -> [String : Any]? {
        var request = [String: Any]()
        request["files"] = files ?? Data()
        return request
    }
}

class UploadClaimInvoiceResponseDTO: BaseResponseDTO {
    var data: UploadClaimInvoice?
}

class UploadClaimInvoice: ParsableModel {
    
    var createdBy: String?
    var createdOn: String?
    var documentId: String?
    var documentName: String?
    var documentTypeId: NSNumber?
    var modifiedBy: String?
    var modifiedOn: String?
    var serviceRequestId: NSNumber?
    var status: String?
    var storageRefId: String?
}

class UploadClaimInvoiceRequestUseCase : BaseRequestUseCase<UploadClaimInvoiceRequestDTO, UploadClaimInvoiceResponseDTO> {
    
    let dataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: UploadClaimInvoiceRequestDTO?, completionHandler: @escaping (UploadClaimInvoiceResponseDTO?, Error?) -> Void) {
        dataProvider.uploadClaimInvoice(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
