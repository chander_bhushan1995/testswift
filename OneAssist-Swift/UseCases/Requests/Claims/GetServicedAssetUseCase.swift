//
//  GetServicedAssetUseCase.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 24/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class GetServicedAssetRequestDTO: BaseRequestDTO {
    
    var referenceNo: String?
    var productCode: String?
    var serviceRequestStatus: String? = "CO"

    init(memId: String?, prodCode: String?) {
        referenceNo = memId
        productCode = prodCode
    }
}

class GetServicedAssetResponseDTO: BaseResponseDTO {
    var data: [ServiceAssetDetails]?
}

class ServiceAssetDetails: ParsableModel {
    var assetAge: AnyObject?
    var assetId: NSNumber?
    var assetInspectionStatus: AnyObject?
    var assetReferenceNo: AnyObject?
    var assetServiceStatus: AnyObject?
    var assetSize: String?
    var assetTechnology: String?
    var assetUnit: String?
    var category: String?
    var createdBy: String?
    var createdOn: String?
    var isAccidentalDamage: AnyObject?
    var isFunctional: AnyObject?
    var isInformationCorrect: AnyObject?
    var make: String?
    var markedForTransport: AnyObject?
    var modelNo: String?
    var modifiedBy: AnyObject?
    var modifiedOn: AnyObject?
    var productCode: String?
    var serialNo: String?
    var serviceRequestAssetDocuments: [AnyObject]?
    var serviceRequestId: NSNumber?
    var serviceType: AnyObject?
    var status: String?

    func getProduct(from asset: ServiceAssetDetails) -> Product {
        var product : Product {
            let product = Product(dictionary: [:])
            product.assetId = asset.assetId
            product.brand = asset.make
            product.model = asset.modelNo
            product.productCode = asset.productCode
            product.productName = Utilities.getProduct(productCode: asset.productCode ?? "")?.subCategoryName
            product.technology = asset.assetTechnology
            product.size = asset.assetSize
            product.sizeUnit = asset.assetUnit
            product.serialNo = asset.serialNo
            product.category = asset.category
            
            return product
        }
        
        return product
    }
    
}


class GetServicedAssetRequestUseCase: BaseRequestUseCase<GetServicedAssetRequestDTO, GetServicedAssetResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.claimDataProvider()
    
    override func getRequest(requestDto: GetServicedAssetRequestDTO?, completionHandler: @escaping (GetServicedAssetResponseDTO?, Error?) -> Void) {
        responseDataProvider.getServicedAsset(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandler: completionHandler)
    }
}
