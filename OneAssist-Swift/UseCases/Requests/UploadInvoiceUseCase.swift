//
//  UploadInvoiceUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 22/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class UploadInvoiceRequestDTO: BaseRequestDTO {
    
    let docKey: String?
    let memId: String?
    let productCode: String?
    let filename_file: Data?
    
    init(docKey: String?, mem_id: NSNumber?, productCode: String?, filename_file: Data?) {
        self.docKey = docKey
        self.memId = mem_id?.description
        self.productCode = productCode
        self.filename_file = filename_file
    }
    
    override func createRequestParameters() -> [String : Any]? {
        var request = JSONParserSwift.getDictionary(object: self)
        request["filename_file"] = filename_file
        return request
    }
}

class UploadInvoiceResponseDTO: BaseResponseDTO {
    var refId: String?
}

class UploadInvoiceRequestUseCase : BaseRequestUseCase<UploadInvoiceRequestDTO, UploadInvoiceResponseDTO> {
    
    let dataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: UploadInvoiceRequestDTO?, completionHandler: @escaping (UploadInvoiceResponseDTO?, Error?) -> Void) {
        dataProvider.uploadInvoice(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
