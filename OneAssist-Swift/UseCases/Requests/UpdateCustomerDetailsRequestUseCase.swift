//
//  UpdateCustomerDetailsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 05/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class UpdateCustomerDetailsRequestDTO: BaseRequestDTO {
    
    var customer: Customer?
    var mailAddressDtl: MailAddressDtl?
    var sessionId: String?
    var successfull: String?
    
    override func createRequestParameters() -> [String : Any]? {
        
        return JSONParserSwift.getDictionary(object: self)
    }
}


class UpdateCustomerDetailsResponseDTO: BaseResponseDTO {

}

class UpdateCustomerDetailsRequestUseCase : BaseRequestUseCase<UpdateCustomerDetailsRequestDTO, UpdateCustomerDetailsResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    override func getRequest(requestDto: UpdateCustomerDetailsRequestDTO?, completionHandler: @escaping (UpdateCustomerDetailsResponseDTO?, Error?) -> Void) {
        dataProvider.updateCustomerDetails(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
