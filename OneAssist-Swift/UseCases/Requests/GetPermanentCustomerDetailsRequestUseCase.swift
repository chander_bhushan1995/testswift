//
//  GetPermanentCustomerDetailsRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 12/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class GetPermanentCustomerDetailsRequestDTO: BaseRequestDTO {
    
    let custId: NSNumber
    
    init(custId: NSNumber) {
        self.custId = custId
    }
}


class GetPermanentCustomerDetailsResponseDTO: BaseResponseDTO {
    
    var customer: Customer?
    var mailAddressDtl: MailAddressDtl?
    //    var successfull: String?
    var hasAddress: String?
    
    var isAddressAvailable: Bool {
        return (hasAddress == "Y" ? true : false)
    }
}

class MailAddressDtl: ParsableModel {
    
    var addressLine1: String?
    var addressLine2: String?
    var city: String?
    var cityName: String?
    var pincode: String?
    var state: String?
    var stateName: String?
}

class Customer: ParsableModel {
    var custId: NSNumber?
    var contactNo: NSNumber?
    var dob: String?
    var email: String?
    var firstName: String?
    var gender: String?
    var lastName: String?
}

class GetPermanentCustomerDetailsRequestUseCase : BaseRequestUseCase<GetPermanentCustomerDetailsRequestDTO, GetPermanentCustomerDetailsResponseDTO> {
    
    let dataProvider = dataProviderFactory.authenticationDataProvider()
    
    override func getRequest(requestDto: GetPermanentCustomerDetailsRequestDTO?, completionHandler: @escaping (GetPermanentCustomerDetailsResponseDTO?, Error?) -> Void) {
        dataProvider.getCustomerDetails(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
