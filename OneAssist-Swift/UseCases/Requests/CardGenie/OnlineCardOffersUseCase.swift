//
//  OnlineCardOffersUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class OnlineCardOffersRequestDTO: BaseRequestDTO {
    
    var bookmarked: Bool?
    var cards: [Card]?
    var addedCards: [Card]?
    var otherCards: [Card]?
    var category: String?
    var customerId: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var maxDistance: NSNumber?
    let offerType: String? = "Online"
    var pageNo: NSNumber?
    var pageSize: NSNumber?
    
    override init() {}
    
    override func createRequestParameters() -> [String : Any]? {
        let request = JSONParserSwift.getDictionary(object: self)
        var mainRequest: [String : Any] = [String : Any]()
        mainRequest["query"] = request
        return mainRequest
    }
}

class OnlineCardOffersResponseDTO: BaseResponseDTO {
    var data: [BankCardOffers]?
    var offerCount: NSNumber?
}

class OnlineCardOffersRequestUseCase : BaseRequestUseCase<OnlineCardOffersRequestDTO, OnlineCardOffersResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: OnlineCardOffersRequestDTO?, completionHandler: @escaping (OnlineCardOffersResponseDTO?, Error?) -> Void) {
        responseDataProvider.getOnlineCardOffers(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
