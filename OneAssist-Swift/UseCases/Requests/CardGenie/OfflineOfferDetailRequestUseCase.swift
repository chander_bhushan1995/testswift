//
//  OfflineOfferDetailRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/4/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class OfflineOfferDetailRequestDTO: BaseRequestDTO {
    
    let latitude: String
    let longitude: String
    let outletCode: String
    let offerCode: String
    let customerId: String
    
    init(latitude: String, longitude: String, outletCode: String, offerCode: String, customerId: String) {
        self.latitude = latitude
        self.longitude = longitude
        self.outletCode = outletCode
        self.offerCode = offerCode
        self.customerId = customerId
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        return JSONParserSwift.getDictionary(object: self)
    }
}

class OfflineOfferDetailResponseDTO: BaseResponseDTO {
    var data: OfferDetail?
}

class OfflineOfferDetailRequestUseCase: BaseRequestUseCase<OfflineOfferDetailRequestDTO, OfflineOfferDetailResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: OfflineOfferDetailRequestDTO?, completionHandler: @escaping (OfflineOfferDetailResponseDTO?, Error?) -> Void) {
        responseDataProvider.getOfflineOfferDetail(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
