//
//  DeleteBookmarkedOffersRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 29/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class DeleteBookmarkedOffersRequestDTO: BaseRequestDTO {
    
    var expiredBookmarks: [ExpiredBookmark]?
    
    override init() {}
    
    override func createRequestParameters() -> [String : Any]? {
        
        return JSONParserSwift.getDictionary(object: self)
    }
}

class ExpiredBookmark: ParsableModel {
    
    var bookmarkId: String?
}

class DeleteBookmarkedOffersResponseDTO: BaseResponseDTO {
    var data: [DeletedBookmark]?
}

class DeletedBookmark: ParsableModel {
    var bookmarkId: String?
}

class DeleteBookmarkedOffersRequestUseCase : BaseRequestUseCase<DeleteBookmarkedOffersRequestDTO, DeleteBookmarkedOffersResponseDTO> {
    
    let dataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: DeleteBookmarkedOffersRequestDTO?, completionHandler: @escaping (DeleteBookmarkedOffersResponseDTO?, Error?) -> Void) {
        dataProvider.deleteBookmarkedOffers(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
