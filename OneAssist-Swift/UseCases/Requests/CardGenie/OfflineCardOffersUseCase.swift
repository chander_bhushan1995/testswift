//
//  OfflineCardOffersUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation



class OfflineCardOffersRequestDTO: BaseRequestDTO {
    
    var bookmarked: Bool?
    var cards: [Card]?
    var addedCards: [Card]?
    var otherCards: [Card]?
    var category: String?
    var customerId: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var maxDistance: NSNumber?
    var offerType: String? = "Offline"
    var pageNo: NSNumber?
    var pageSize: NSNumber?
    
    override init() {}
    
    override func createRequestParameters() -> [String : Any]? {
        let request = JSONParserSwift.getDictionary(object: self)
        var mainRequest: [String : Any] = [String : Any]()
        mainRequest["query"] = request
        return mainRequest
    }
}

class Card:Equatable {
    var cardIssuerCode: String?
    var cardTypeId: NSNumber?
    var networkId: NSNumber?
    var cardIssuerId: String?
    
    init(cardIssuerCode: String?, cardTypeId: NSNumber?, networkId: NSNumber?) {
        self.cardIssuerCode = cardIssuerCode
        self.cardTypeId = cardTypeId
        self.networkId = networkId
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(walletCard: WalletCard) {
        self.cardTypeId = NSNumber(integerLiteral: walletCard.cardTypeId)
        self.networkId = NSNumber(integerLiteral: walletCard.networkId)
        self.cardIssuerCode = walletCard.issuerCode
        self.cardIssuerId = walletCard.issuerId
    }
    
    init(cardDetail: CardDetail) {
        self.cardTypeId = NSNumber(value: cardDetail.cardTypeId)
        self.cardIssuerCode = cardDetail.cardIssuerCode
        self.cardIssuerId = cardDetail.cardIssuerId
        self.networkId = 0
    }
    
    init(card: Card) {
        self.cardTypeId = card.cardTypeId
        self.cardIssuerCode = card.cardIssuerCode
        self.networkId = card.networkId
    }
    
    convenience init(creditCard: OptionList) {
        self.init(cardIssuerCode: creditCard.optionCode, cardTypeId: 1, networkId: 0)
    }
    
    convenience init(debitCard: OptionList) {
        self.init(cardIssuerCode: debitCard.optionCode, cardTypeId: 2, networkId: 0)
    }
    
}

extension Card: Comparable {
    static func < (lhs: Card, rhs: Card) -> Bool {
        return (lhs.cardIssuerCode == rhs.cardIssuerCode && lhs.cardIssuerId == rhs.cardIssuerId)
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.cardIssuerCode == rhs.cardIssuerCode &&
            lhs.cardTypeId == rhs.cardTypeId &&
            lhs.cardIssuerId == rhs.cardIssuerId
    }
}

class OfflineCardOffersResponseDTO: BaseResponseDTO {
    var data: [BankCardOffers]?
    var offerCount: NSNumber?
}

class BankCardOffers: ParsableModel {
    
    var bookmark: NSNumber?
    var bookmarkId: String?
    var distance: String?
    var endDate: String?
    var image: String?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var merchantLocation: String?
    var merchantName: String?
    var offerCode: String?
    var offerExpiringInDays: NSNumber?
    var offerId: String?
    var offerTitle: String?
    var offerType: String?
    var outletCode: String?
    var cardDetailsList: [CardDetail]?
    
    var isBookmarked: Bool {
        set(newValue) {
            bookmark = newValue ? 1 : 0
        }
        
        get {
            return bookmark?.intValue == 1
        }
    }
}

class CardDetail: ParsableModel {
    var cardTypeId: String?
    var cardIssuerId: String?
    var cardIssuerName: String?
    var cardIssuerCode: String?
    var imagePath: String?
    var helpline1: String?
    var helpline2: String?
    var landline1: String?
    var landline2: String?
    var ranking: NSNumber?
    var tagging: String?
    var issuerShortName: String?
    
    var isSelected: Bool = false // use for filter in offer screen
}

class UniqueCards: ParsableModel { //use this for filter
    var cardIssuerName: String?
    var cardIssuerCode: String?
    var issuerShortName: String?
    var cards: [CardDetail]?
    
    var isSelected: Bool = false // use for filter in offer screen
}

class OfflineCardOffersRequestUseCase : BaseRequestUseCase<OfflineCardOffersRequestDTO, OfflineCardOffersResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: OfflineCardOffersRequestDTO?, completionHandler: @escaping (OfflineCardOffersResponseDTO?, Error?) -> Void) {
        
        responseDataProvider.getOfflineCardOffers(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
