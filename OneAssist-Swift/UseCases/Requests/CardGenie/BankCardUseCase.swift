//
//  BankCardUseCase.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 13/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation


class BankCardRequestDTO: BaseRequestDTO {

}

class BankCardResponseDTO: BaseResponseDTO {
    var data: [CardDetail]?
}


class BankCardUseCase : BaseRequestUseCase<BankCardRequestDTO, BankCardResponseDTO> {
    
    let dataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: BankCardRequestDTO?, completionHandler: @escaping (BankCardResponseDTO?, Error?) -> Void) {
        dataProvider.getBankCards(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
