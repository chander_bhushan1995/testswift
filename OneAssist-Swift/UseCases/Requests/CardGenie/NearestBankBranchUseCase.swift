//
//  NearestBankBranchUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class NearestBankBranchRequestDTO: BaseRequestDTO {
    
    var bankName: String
    var latitude: NSNumber
    var longitude: NSNumber
    var radius: NSNumber
    
    init(bankName: String, latitude: NSNumber, longitude: NSNumber, radius: NSNumber) {
        self.bankName = bankName
        self.latitude = latitude
        self.longitude = longitude
        self.radius = radius
    }
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class NearestBankBranchResponseDTO: BaseResponseDTO {
    var data: [BankBranchResponse]?
}

class BankBranchResponse: ParsableModel {
    var address: String?
    var distance: String?
    var latitude: String?
    var longitude: String?
    var bankName: String?
    var phone1: String?
    var phone2: String?
    var placeId: String?
    var type: String?
}


class NearestBankBranchRequestUseCase : BaseRequestUseCase<NearestBankBranchRequestDTO, NearestBankBranchResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: NearestBankBranchRequestDTO?, completionHandler: @escaping (NearestBankBranchResponseDTO?, Error?) -> Void) {
        
        responseDataProvider.getNearestBankBranch(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
