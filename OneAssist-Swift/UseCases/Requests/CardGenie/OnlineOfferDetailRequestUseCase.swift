//
//  OnlineOfferDetailRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/4/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class OnlineOfferDetailRequestDTO: BaseRequestDTO {
    
    private let customerId: String?
    private let offerId: String
    
    init(customerId: String?, offerId: String) {
        self.offerId = offerId
        self.customerId = customerId ?? "0"
    }
    
    override func createRequestParameters() -> [String : Any]? {
        
        return JSONParserSwift.getDictionary(object: self)
    }
}

class OnlineOfferDetailResponseDTO: BaseResponseDTO {
    var data: OfferDetail?
}

class OfferDetail: ParsableModel {
    
    var isBookmarked: Bool {
        return bookmark?.intValue == 1
    }
    
    var availableDiscount: String?
    var bookmark: NSNumber?
    var bookmarkId: String?
    var category: String?
    var distance: String?
    var endDate: String?
    var image: String?
    var isProcessed: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var merchantLocation: String?
    var merchantName: String?
    var merchantPhoneNo: String?
    var offerId: NSNumber?
    var offerTitle: String?
    var offerType: String?
    var pincode: String?
    var startDate: String?
    var termsConditions: String?
    var cardDetailsList: [CardDetail]?
}

class OnlineOfferDetailRequestUseCase: BaseRequestUseCase<OnlineOfferDetailRequestDTO, OnlineOfferDetailResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: OnlineOfferDetailRequestDTO?, completionHandler: @escaping (OnlineOfferDetailResponseDTO?, Error?) -> Void) {
        responseDataProvider.getOnlineOfferDetail(request: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
