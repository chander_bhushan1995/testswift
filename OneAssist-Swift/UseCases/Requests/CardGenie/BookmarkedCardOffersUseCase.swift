//
//  BookmarkedCardOffersUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class BookmarkedCardOffersRequestDTO: BaseRequestDTO {
    
    var bookmarked: Bool? = true
    var cards: [Card]?
    var category: String?
    var customerId: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var maxDistance: NSNumber?
    var offerType: String? = ""
    var pageNo: NSNumber? = 1
    var pageSize: NSNumber? = 400
    
    override init() {}
    
    override func createRequestParameters() -> [String : Any]? {
        let request = JSONParserSwift.getDictionary(object: self)
        var mainRequest: [String : Any] = [String : Any]()
        mainRequest["query"] = request
        return mainRequest
    }
}

class BookmarkedCardOffersResponseDTO: BaseResponseDTO {
    var data: [BankCardOffers]?
    var dataCD: [Offer]?
    var offerCount: NSNumber?
    
    init() {
        super.init(dictionary: [:])
    }
    
    required init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }

    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        if key == "data", let arrayValue = value as? [BankCardOffers] {
            data = arrayValue
        }
    }
}

class BookmarkedCardOffersRequestUseCase : BaseRequestUseCase<BookmarkedCardOffersRequestDTO, BookmarkedCardOffersResponseDTO> {
    
    let dataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: BookmarkedCardOffersRequestDTO?, completionHandler: @escaping (BookmarkedCardOffersResponseDTO?, Error?) -> Void) {
        let apiCacheChecker = BookmarkedOffersCacheChecker()
        
        dataProvider.getBookmarkedCardOffers(requestDto: requestDto, apiCacheChecker: apiCacheChecker, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
