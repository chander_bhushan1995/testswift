//
//  CategoryListRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/4/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class CategoryListRequestDTO: BaseRequestDTO {}

class CategoryListResponseDTO: BaseResponseDTO {
    
    var data: [CategoryList]?
    var categories: [OfferCategoryCD]?
    
    init() {
        super.init(dictionary: [:])
    }
    
    required init(dictionary: [String : Any]) {
        super.init(dictionary: dictionary)
    }
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        if key == "data", let arrayValue = value as? [CategoryList] {
            data = arrayValue
        }
    }

}

class CategoryList: ParsableModel {
    var category: String?
}

class CategoryListRequestUseCase: BaseRequestUseCase<CategoryListRequestDTO, CategoryListResponseDTO> {
    
    let dataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: CategoryListRequestDTO?, completionHandler: @escaping (CategoryListResponseDTO?, Error?) -> Void) {
        let apiCacheChecker = OfferCategoriesCacheChecker()
        
        dataProvider.getCategoryList(requestDto: requestDto, apiCacheChecker: apiCacheChecker, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
