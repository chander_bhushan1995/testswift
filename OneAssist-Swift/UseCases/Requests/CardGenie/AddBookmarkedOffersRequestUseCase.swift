//
//  AddBookmarkedOffersRequestUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 29/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

//
//  AddBookmarkedOffersUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class AddBookmarkedOffersRequestDTO: BaseRequestDTO {
    
    var bookmarkedOffers: [BookmarkedOffer]?
    
    override init() {}
    
    override func createRequestParameters() -> [String : Any]? {
        
        return JSONParserSwift.getDictionary(object: self)
    }
}

class BookmarkedOffer {
    
    var customerId: String?
    var endDate: String?
    var offerProvider: String?
    var offerReference: String?
    var outletCode: String?
    var merchantName : String?
    var image : String?
    var offerTitle : String?
}

class AddBookmarkedOffersResponseDTO: BaseResponseDTO {
    var data: [ValidData]?
}

class ValidData: ParsableModel {
    
    var bookmarkId: NSNumber?
    var customerId: NSNumber?
    var endDate: String?
    var image: AnyObject?
    var merchantName: AnyObject?
    var offerProvider: String?
    var offerReference: String?
    var offerReferenceDescription: AnyObject?
    var offerTitle: AnyObject?
    var outletCode: String?
}

class AddBookmarkedOffersRequestUseCase : BaseRequestUseCase<AddBookmarkedOffersRequestDTO, AddBookmarkedOffersResponseDTO> {

    let dataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: AddBookmarkedOffersRequestDTO?, completionHandler: @escaping (AddBookmarkedOffersResponseDTO?, Error?) -> Void) {
        dataProvider.addBookmarkedOffers(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}

