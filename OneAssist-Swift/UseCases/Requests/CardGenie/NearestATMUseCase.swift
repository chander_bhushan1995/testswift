//
//  NearestATMUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 04/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
 

class NearestATMRequestDTO: BaseRequestDTO {

    var name : String?
    var latitude: NSNumber
    var longitude: NSNumber
    var radius: NSNumber
    let type: String = "ATM"
    
    init(name : String?, latitude: NSNumber, longitude: NSNumber, radius: NSNumber) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.radius = radius
    }
    
    override func createRequestParameters() -> [String : Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}

class NearestATMResponseDTO: BaseResponseDTO {
    var data: [ATMResponse]?
}

class ATMResponse: ParsableModel {
    var address: String?
    var distance: String?
    var latitude: String?
    var longitude: String?
    var name: String?
    var phone1: String?
    var phone2: String?
    var placeId: String?
    var type: String?
}


class NearestATMRequestUseCase : BaseRequestUseCase<NearestATMRequestDTO, NearestATMResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.cardGenieDataProvider()
    
    override func getRequest(requestDto: NearestATMRequestDTO?, completionHandler: @escaping (NearestATMResponseDTO?, Error?) -> Void) {
        responseDataProvider.getNearestATM(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
