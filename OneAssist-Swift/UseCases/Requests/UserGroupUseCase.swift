//
//  UserGroupUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 09/02/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

class UserGroupRequestDTO: BaseRequestDTO {
    var custId: String = UserCoreDataStore.currentUser?.cusId ?? ""
}

class UserGroupResponseDTO: BaseResponseDTO {
    var data: UserGroupData?
}

class UserGroupData: ParsableModel {
    var userLabels: [String]?
}

class UserGroupUseCase : BaseRequestUseCase<UserGroupRequestDTO, UserGroupResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: UserGroupRequestDTO?, completionHandler: @escaping (UserGroupResponseDTO?, Error?) -> Void) {
        responseDataProvider.getUserGroup(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
