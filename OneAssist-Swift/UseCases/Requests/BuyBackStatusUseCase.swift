//
//  CustomerBuyBackStatusUseCase.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/06/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class BuyBackStatusRequestDTO: BaseRequestDTO {
    
    let modelName: String
    let customerId: String
    let ram: String
    let storage: String
    let deviceIdentifier: String = UIDevice.uuid
    init(modelName: String, customerId: String, ram: String, storage: String) {
        self.modelName = modelName
        self.customerId = customerId
        self.ram = ram
        self.storage = storage
    }
}

class BuyBackStatusResponseDTO: BaseResponseDTO {
    var data: BuyBackStatusData?
}

class BuyBackStatusData: ParsableModel {
    var buybackStatus: [BuyBackStatus]?
}

class BuyBackStatus: ParsableModel {
    var buyBackStatus: String?
    var price: String?
    var deviceInfo: DeviceInfo?
    var quoteId: String?
    var orderId: String?
    var quoteCreationTime: String?
    var pincode: String?
    var cityName: String?
    var mhcScore: String?
    var membershipId: String?
    var couponCodeInfo : CouponCodeInfo?
    var subType: String?
    var flow: String?
}

class DeviceInfo: ParsableModel {
    var brand: String?
    var modelName: String?
    var serialNumber: String?
    var primaryMemory: String?
    var secondaryMemory: String?
    var deviceType: String?
    var image: String?
    var deviceName: String?
    var deviceId: String?
    var deviceIdentifier: String?
}

class CouponCodeInfo: ParsableModel {
    var couponCode: String?
    var couponPrice: String?
    var isApplied: NSNumber?
}

class BuyBackStatusUseCase : BaseRequestUseCase<BuyBackStatusRequestDTO, BuyBackStatusResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: BuyBackStatusRequestDTO?, completionHandler: @escaping (BuyBackStatusResponseDTO?, Error?) -> Void) {
        responseDataProvider.getBuyBackStatusData(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
