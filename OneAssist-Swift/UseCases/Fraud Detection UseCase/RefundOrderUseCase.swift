//
//  RefundOrderUseCase.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 04/01/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

class RefundOrderRequestDTO: BaseRequestDTO {
    var orderId: String?
    
    init(orderId: String?) {
        self.orderId = orderId
    }
    
    override func createRequestParameters() -> [String : Any]? {
        let dict = JSONParserSwift.getDictionary(object: self)
        return dict
    }
}

class RefundOrderResponseDTO: BaseResponseDTO {
    var data: RefundOrderData?
}

class RefundOrderData: ParsableModel {
    var txnId: String?
}

class RefundOrderUseCase: BaseRequestUseCase<RefundOrderRequestDTO,RefundOrderResponseDTO> {
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: RefundOrderRequestDTO?, completionHandler: @escaping (RefundOrderResponseDTO?, Error?) -> Void) {
        responseDataProvider.refundOrder(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
