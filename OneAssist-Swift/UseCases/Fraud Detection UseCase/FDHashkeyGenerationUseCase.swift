//
//  FDHashkeyGenerationUseCase.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 25/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation


class FDHashKeyGenerationReqDTO: BaseRequestDTO {
    var activityReferenceId: String?
}

class FDHashKeyGenerationResDTO: BaseResponseDTO {
    var data: FDHashKeyData?
}

class FDHashKeyData: ParsableModel {
    var hashKey: String?
}

class FDHashKeyGenerationUseCase: BaseRequestUseCase<FDHashKeyGenerationReqDTO, FDHashKeyGenerationResDTO> {
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    override func getRequest(requestDto: FDHashKeyGenerationReqDTO?, completionHandler: @escaping (FDHashKeyGenerationResDTO?, Error?) -> Void) {
        responseDataProvider.getFDHashKey(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
