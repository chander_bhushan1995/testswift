//
//  UploadInvoiceFDUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 12/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class UploadInvoiceFDRequestDTO: BaseRequestDTO {
    
    let activityReferenceId: String?
    let name: String?
    let fileName: String?
    let docFileContentType: String?
    let displayName: String?
    let docMandatory: String?
    let uploadedFile: Data?
    
    init(activityReferenceId: String?, name: String?, fileName: String?, docFileContentType: String?, displayName: String?, docMandatory: String?, uploadedFile: Data?) {
        
        self.activityReferenceId = activityReferenceId
        self.name = name
        self.fileName = fileName
        self.docFileContentType = docFileContentType
        self.displayName = displayName
        self.docMandatory = docMandatory
        self.uploadedFile = uploadedFile
    }
    
    
    override func createRequestParameters() -> [String : Any]? {
        var request = JSONParserSwift.getDictionary(object: self)
        request["uploadedFile"] = uploadedFile
        return request
    }
}

class UploadInvoiceFDResponseDTO: BaseResponseDTO {
    var data: String?
}

class UploadInvoiceFDUseCase: BaseRequestUseCase<UploadInvoiceFDRequestDTO, UploadInvoiceFDResponseDTO> {
    
    let dataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: UploadInvoiceFDRequestDTO?, completionHandler: @escaping (UploadInvoiceFDResponseDTO?, Error?) -> Void) {
        dataProvider.uploadInvoiceFD(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
