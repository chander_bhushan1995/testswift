//
//  UpdatePostPaymentDetailUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 13/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class UpdatePostPaymentDetailsRequestDTO: BaseRequestDTO {
    let addressInfo: AddressInfo?
    let deviceModel: String?
    let orderId: String?
    let imei: String?
    let imei2: String?
    let purchaseAmount: String?
    let purchaseDate:  NSNumber?
    
    init(addressInfo: AddressInfo, deviceModel: String, orderId: String,imei: String? = nil, imei2: String? = nil, purchaseAmount: String? = nil, purchaseDate: NSNumber? = nil) {
        self.addressInfo = addressInfo
        self.deviceModel = deviceModel
        self.orderId = orderId
        self.imei = imei
        self.imei2 = imei2
        self.purchaseAmount = purchaseAmount
        self.purchaseDate = purchaseDate
    }
    
    override func createRequestParameters() -> [String : Any]? {
        var dict = JSONParserSwift.getDictionary(object: self)
        if imei == nil {
            dict.removeValue(forKey: Constants.RequestKeys.imei)
        }
        if imei2 == nil {
            dict.removeValue(forKey: Constants.RequestKeys.imei2)
        }
        if purchaseAmount == nil || purchaseAmount == "" {
            dict.removeValue(forKey: Constants.RequestKeys.purchaseAmount)
        }
        if purchaseDate == nil || purchaseDate?.intValue == 0 {
            dict.removeValue(forKey: Constants.RequestKeys.purchaseDate)
        }
        
        return dict
    }
}

class AddressInfo {
    let addressLine1: String?
    let pincode: String?
    
    init(addressLine1: String, pincode: String) {
        self.addressLine1 = addressLine1
        self.pincode = pincode
    }
}

class UpdatePostPaymentDetailsResponseDTO: BaseResponseDTO {
    var data: String?
}

class UpdatePostPaymentDetailUseCase: BaseRequestUseCase<UpdatePostPaymentDetailsRequestDTO, UpdatePostPaymentDetailsResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: UpdatePostPaymentDetailsRequestDTO?, completionHandler: @escaping (UpdatePostPaymentDetailsResponseDTO?, Error?) -> Void) {
        responseDataProvider.updatePostPaymentDetails(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
