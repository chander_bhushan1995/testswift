//
//  FDTestTypeChangeUseCase.swift
//  OneAssist-Swift
//
//  Created by Chandra Bhushan on 12/05/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation

class FDTestTypeChangeRequestDTO: BaseRequestDTO {
    var modelType: String?
    var reasonCode: String?
    var reasonValue: String?
    var activityReferenceId: String?
   
    init(modelType: String?, reasonCode: String = "OTHERS", reasonValue: String?, activityReferenceId: String?) {
        self.modelType = modelType
        self.reasonCode = reasonCode
        self.reasonValue = reasonValue
        self.activityReferenceId = activityReferenceId
    }
    
    override func createRequestParameters() -> [String : Any]? {
        let dict = JSONParserSwift.getDictionary(object: self)
        return dict
    }
}

class FDTestTypeChangeUseCase: BaseRequestUseCase<FDTestTypeChangeRequestDTO, BaseResponseDTO> {
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: FDTestTypeChangeRequestDTO?, completionHandler: @escaping (BaseResponseDTO?, Error?) -> Void) {
        responseDataProvider.changeFDTestType(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
