//
//  SubmitDocUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 26/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class SubmitDocRequestDTO: BaseRequestDTO {
    var activityRefId: String?
    
    init(activityRefId: String?) {
        self.activityRefId = activityRefId
    }
    
    override func createRequestParameters() -> [String : Any]? {
        let dict = JSONParserSwift.getDictionary(object: self)
        return dict
    }
}

class SubmitDocResponseDTO: BaseResponseDTO {
}

class SubmitDocUseCase: BaseRequestUseCase<SubmitDocRequestDTO,SubmitDocResponseDTO> {
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: SubmitDocRequestDTO?, completionHandler: @escaping (SubmitDocResponseDTO?, Error?) -> Void) {
        
        responseDataProvider.submitDocs(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}
