//
//  GetDocsToUploadUseCase.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 04/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class GetDocsToUploadRequestDTO: BaseRequestDTO {
    var activationCode = ""
}

class GetDocsToUploadResponseDTO: BaseResponseDTO {
    
    var activityReferenceId: String?
    var deviceDetails: [DeviceDetail]?
}


class DeviceDetail : ParsableModel {
    var deviceId : String?
    var deviceId2 : String?
    var make : String?
    var model : String?
    var productCode : String?
    var verificationDocuments : [VerificationDocument]?
}

class VerificationDocument: ParsableModel {
    var displayName : String?
    var docMandatory : String?
    var duplicateRequired : NSNumber?
    var guideMessage : [String]?
    var name : String?
    var panelMessage : String?
    var rank : NSNumber?
    var sampleImageName : String?
    var status : String?
    var value : String?
}

class GetDocsToUploadUseCase: BaseRequestUseCase<GetDocsToUploadRequestDTO, GetDocsToUploadResponseDTO> {
    
    let responseDataProvider = dataProviderFactory.commonDataProvider()
    
    override func getRequest(requestDto: GetDocsToUploadRequestDTO?, completionHandler: @escaping (GetDocsToUploadResponseDTO?, Error?) -> Void) {
        
        responseDataProvider.getDocsToUpload(requestDto: requestDto, taskCallBack: { (request) in
            self.request = request
        }, completionHandlerBlock: completionHandler)
    }
}



