//
//  WHCDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol WHCDataProvider {
    func getWHCInspectionDetails(requestDto: WHCInspectionRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WHCInspectionResponseDTO?, Error?)->Void)
    
    func getWHCFeedbacks(requestDto: WHCFeedbackRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WHCFeedbackResponseDTO?, Error?)->Void)
    
    func getWHCPinCodeResponse(requestDto: WHCPincodeRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WHCPincodeResponseDTO?, Error?)->Void)
    
    func getWHCNotify(requestDto: WHCPinVerificationRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void)
    
    func getCustomerAddress(requestDto: WHCPincodeRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (WHCPincodeResponseDTO?, Error?) -> Void)
    
    func getTechnicianDetail(requestDto: TechnicianDetailRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(TechnicianDetailResponseDTO?, Error?)->Void)
    
    func getStartEndOTP(requestDto: InspectionStartEndOTPRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(InspectionStartEndOTPResponseDTO?, Error?)->Void)
    
    func getTimeSlots(requestDto: InspectionTimeSlotsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(InspectionTimeSlotsResponseDTO?, Error?)->Void)
    
    func scheduleInspection(requestDto: ScheduleInspectionRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(ScheduleInspectionResponseDTO?, Error?)->Void)
    
    func rescheduleInspection(requestDto: RescheduleInspectionRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(RescheduleInspectionResponseDTO?, Error?)->Void)
    
    func getTempCustomer(requestDto: TemporaryCustomerInfoRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(TemporaryCustomerInfoResponseDTO?, Error?)->Void)
    
    func submitFeedback(requestDto: SubmitFeedbackRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SubmitFeedbackResponseDTO?, Error?)->Void)
}
