//
//  ClaimDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/21/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol ClaimDataProvider {
    func getServiceCenterList(requestDto: ServiceCenterListRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (ServiceCenterListResponceDTO?, Error?) -> Void)
    
    func updateAlternateMobileEmail(requestDto: MobileEmailRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (MobileEmailResponseDTO?, Error?) -> Void)
    
    func wantDeviceBack(requestDto: DeviceBackRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (DeviceBackResponseDTO?, Error?) -> Void)
     func payLater(requestDto: PayLaterRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (PayLaterResponseDTO?, Error?) -> Void)
     func getJSTimelineCard(data: JavaScriptTimeineRequestDTO?, completionHandler: @escaping (JavaScriptTimeineResponceDTO?, Error?) -> Void)
    
    func getcityCode(requestDto: ServiceCenterListRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (CityCodeResponseDTO?, Error?) -> Void)
    
     func getServiceCenterDistance(requestDto: GoogleDistanceRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (GoogleDistanceResponseDTO?, Error?) -> Void)

    func createServiceRequestPlan(requestDto: CreateServiceRequestPlanRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (CreateServiceRequestPlanResponseDTO?, Error?) -> Void)

    func updateServiceRequestPlan(requestDto: UpdateServiceRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (UpdateServiceRequestResponseDTO?, Error?) -> Void)
    
    func getMembershipAssetDetails(requestDto: GetMembershipAssetListRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetMembershipAssetListResponseDTO?, Error?) -> Void)

    
    func whatHappend(requestDto: WhatHappendRequestDTO, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (WhatHappendResponseDTO?, Error?) -> Void)
    
    func searchServiceRequestPlan(requestDto: SearchServiceRequestPlanRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (SearchServiceRequestPlanResponseDTO?, Error?) -> Void)

    func getPendingFeedback(requestDto: GetPendingFeedbackRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetPendingFeedbackResponseDTO?, Error?) -> Void)
    
    func uploadClaimInvoice(requestDto: UploadClaimInvoiceRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (UploadClaimInvoiceResponseDTO?, Error?) -> Void)
    
    func getMembershipServices(requestDto: MembershipServicesRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (MembershipServicesResponseDTO?, Error?) -> Void)
    
    func getCheckAvailability(requestDto: CheckAvailabilityRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (CheckAvailabilityResponseDTO?, Error?) -> Void)
    
    func checkPincodeServiciability(requestDto: WHCPincodeRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (WHCPincodeResponseDTO?, Error?) -> Void)
    
    func getFeedbackRequest(requestDto: SearchServiceRequestPlanRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (SearchServiceRequestPlanResponseDTO?, Error?) -> Void)
    
    func getClaimDocumentRequest(requestDto: GetClaimDocumentRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetClaimDocumentResponseDTO?, Error?) -> Void)
    
    /// Used to prepare and send request to get bank documents (Cancelled cheque)
    ///
    /// - Parameters:
    ///   - requestDto: Request model to get bank document
    ///   - taskCallBack: Request object
    ///   - completionHandler: Get bank documents response model or error
    func getBankDocumentRequest(requestDto: GetClaimDocumentRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetClaimDocumentResponseDTO?, Error?) -> Void)
    
    func getExcessPayment(requestDto: ExcessPaymentRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ExcessPaymentResponseDTO?, Error?) -> Void)

     func updateSR(requestDto: ClaimUpdateScheduleVisitRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ClaimUpdateScheduleVisitResponseDTO?, Error?) -> Void)
    
    func notifyAllDocumentUploaded(requestDto: ClaimNotifyAllDocumentUploadedRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ClaimNotifyAllDocumentUploadedResponseDTO?, Error?) -> Void)
    
    func getServiceDocumentType(requestDto: GetServiceDocumentTypeRequestDTO?, apiCacheChecker: ApiCacheChecker, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetServiceDocumentTypeResponseDTO?, Error?) -> Void)
    
    func getDeviceCondition(requestDto: DeviceConditionRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (DeviceConditionResponseDTO?, Error?) -> Void)

    /// Used to prepare and send request to get bank details
    ///
    /// - Parameters:
    ///   - requestDto: Request model to get bank details.
    ///   - taskCallBack: Requset object
    ///   - completionHandler: Get bank detail response model or error
    func getBankDetails(requestDto: GetBankDetailsRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetBankDetailsResponseDTO?, Error?) -> Void)
    
    
    /// Used to prepare and send request to update bank details
    ///
    /// - Parameters:
    ///   - requestDto: Request model to update bank details.
    ///   - taskCallBack: Request model
    ///   - completionHandler: Update bank detail response model or error
    func updateBankDetails(requestDto: UpdateBankDetailsRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (UploadClaimInvoiceResponseDTO?, Error?) -> Void)
    
    
    func getDocumentDetails(requestDto: GetDocumentDetailsRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetDocumentDetailsResponseDTO?, Error?) -> Void)
    
    func getServiceCost(requestDto: GetServiceCostRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetServiceCostResponseDTO?, Error?) -> Void)
    
    func getServicedAsset(requestDto: GetServicedAssetRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetServicedAssetResponseDTO?, Error?) -> Void)
    
    func draftMembership(requestDto: DraftsMembershipRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (DraftsMembershipResponseDTO?, Error?) -> Void)
}
