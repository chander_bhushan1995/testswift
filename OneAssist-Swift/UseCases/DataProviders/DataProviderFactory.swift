//
//  DataProviderFactory.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol DataProviderFactory {
    var authenticationDataProvider: () -> AuthenticationDataProvider { get }
    
    var commonDataProvider: () -> CommonDataProvider { get }

    var cardGenieDataProvider: () -> CardGenieDataProvider { get }

    var walletDataProvider: () -> WalletDataProvider { get }

    var whcDataProvider: () -> WHCDataProvider { get }

    var remoteConfigDataProvider: () -> RemoteConfigDataProvider { get }

    var homeAppliancesDataProvider: () -> HomeAppliancesDataProvider { get }
    
    var claimDataProvider: () -> ClaimDataProvider { get }
}
