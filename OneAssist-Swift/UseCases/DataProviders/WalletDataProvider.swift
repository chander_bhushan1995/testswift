//
//  WalletDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol WalletDataProvider {
    func getWalletCards(requestDto: WalletCardsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WalletCardsResponseDTO?, Error?)->Void)
    
    func getSIStatusOfIDFencePremium(requestDto: MembershipSIStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(MembershipSIStatusResponseDTO?, Error?)->Void)
    
    func getSubscriptionStatusOfIDFence(requestDto: IDFenceSubscriptionStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(IDFenceSubscriptionStatusResponseDTO?, Error?)->Void)
    
    func downloadFile(requestDto: DownloadRequestDTO?, taskCallBack:@escaping (_ task : Request?)-> Void, completionHandlerBlock:@escaping(DownloadResponseDTO?, Error?)->Void)
    
    func getRaiseClaimDocumentRequest(requestDto: DownloadRequestDTO?, taskCallBack:@escaping (_ task : Request?)-> Void, completionHandlerBlock:@escaping(DownloadResponseDTO?, Error?)->Void)

}
