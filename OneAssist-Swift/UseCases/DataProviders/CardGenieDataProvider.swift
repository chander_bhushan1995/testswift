//
//  CardGenieDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol CardGenieDataProvider {
    func addBookmarkedOffers(requestDto: AddBookmarkedOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(AddBookmarkedOffersResponseDTO?, Error?) -> Void)
    
    func getCategoryList(requestDto: CategoryListRequestDTO? , apiCacheChecker: ApiCacheChecker, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(CategoryListResponseDTO?, Error?) -> Void)
    
    func getBookmarkedCardOffers(requestDto: BookmarkedCardOffersRequestDTO? , apiCacheChecker: ApiCacheChecker, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BookmarkedCardOffersResponseDTO?, Error?) -> Void)
    
    func getNearestBankBranch(requestDto: NearestBankBranchRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(NearestBankBranchResponseDTO?, Error?) -> Void)
    
    func getNearestATM(requestDto: NearestATMRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(NearestATMResponseDTO?, Error?) -> Void)
    
    func getOnlineCardOffers(request: OnlineCardOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OnlineCardOffersResponseDTO?, Error?) -> Void)
    
    func getOfflineCardOffers(request: OfflineCardOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OfflineCardOffersResponseDTO?, Error?) -> Void)
    
    func getOnlineOfferDetail(request: OnlineOfferDetailRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OnlineOfferDetailResponseDTO?, Error?) -> Void)
    
    func getOfflineOfferDetail(request: OfflineOfferDetailRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OfflineOfferDetailResponseDTO?, Error?) -> Void)
    
    func deleteBookmarkedOffers(request: DeleteBookmarkedOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DeleteBookmarkedOffersResponseDTO?, Error?) -> Void)
    
    func getBankCards(request: BankCardRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BankCardResponseDTO?, Error?) -> Void)
}
