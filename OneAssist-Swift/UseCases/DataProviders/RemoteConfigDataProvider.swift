//
//  RemoteConfigDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol RemoteConfigDataProvider {
    
    func getSODAppliances(completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void)
    
    func getSafetyGuidLines(completionHandler: @escaping (SafetyGuidesResponseDTO?, Error?) -> Void)
    
    func getOAPromises(completionHandler: @escaping (OAPromisesResponseDTO?, Error?) -> Void)
    
    func getBuyPlanCategory(completionHandler: @escaping (BuyPlanResponseDTO?, Error?) -> Void)
    
    func getHomeApplianceCategory(completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void)
    
    func getHomeApplianceOtherCategory(completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void)
    
    func getWHCApplianceList(completionHandler: @escaping (WHCApplianceListResponseDTO?, Error?) -> Void)
    
    func getQuickFactsList(completionHandler: @escaping (RenewalFactsResponseDTO?, Error?) -> Void)
    
    func getFDBarcodeSupportList(completionHandler: @escaping (FDbarcodeScannerResponseDTO?, Error?) -> Void)

    func getCustomerTestimonials(completionHandler: @escaping (CustomerTestimonialsResponseDTO?, Error?) -> Void)
    
    func getCategoryCouponCodes(completionHandler: @escaping (CouponsResponseDTO?, Error?) -> Void)
    
    func getCategoryExclusiveBenefits(completionHandler: @escaping (ExclusiveBenfitsResponseDTO?, Error?) -> Void)
    
    func getPlanTagline(completionHandler: @escaping (PlanTaglineResponseDTO?, Error?) -> Void)
    
    func getIDFenceDetailPageData(completionHandler: @escaping (PlanDetailPageData?, Error?) -> Void)
    
    func getOAAssuredServices(completionHandler: @escaping (OAAssuredServicesResponse?, Error?) -> Void)
    
    func getWhyChooseOA(completionHandler: @escaping (WhyChooseOAResponse?, Error?) -> Void)
    
    func getPlatformRatings(completionHandler: @escaping (PlatformRatingsResponse?, Error?) -> Void)
    
    func getSubscriptionBenefits(completionHandler: @escaping (SubscriptionBenefitsResponseDTO?, Error?) -> Void)
    
    func getSODProductsOffers(completionHandler: @escaping (SODOffersResponseDTO?, Error?) -> Void)
    
    func getMirrorTestVideoGuide(completionHandler: @escaping (MirrorTestVideoGuideResponseDTO?, Error?) -> Void)
    
    func getMirrorTestSamplePhotos(completionHandler: @escaping (MirrorTestSamplePhotoDTO?, Error?) -> Void)
    
    func getMirrorTestLanguage(completionHandler: @escaping (MirrorTestLanguageModelDTO?, Error?) -> Void)
    
    func getPhotoHelpPoints(completionHandler: @escaping (PhotoClickHelpPointsDTO?, Error?) -> Void)

    func getMirrorFlowRatingData(completionHandler: @escaping (MirrorFlowRatingRespModel?, Error?) -> Void)    

    func getNotificationPrompt(completionHandler: @escaping (NotificationAlertResponseDTO?, Error?) -> Void)
    
    func getSODProductServiceScreen(completionHandler: @escaping (SODProductServiceScreenResDTO?, Error?) -> Void)
    
    func getSODProductScreen(completionHandler: @escaping (SODProductServiceScreenResDTO?, Error?) -> Void)
    
    func getCDNDataVersioning(completionHandler: @escaping (CDNDataVersioningDTO?, Error?) -> Void)
    
    func getModelDownloadingStatusScreenData(completionHandler: @escaping (ModelDownloadingStatusScreenData?, Error?) -> Void)
    
    func getSmartMirrorFlowModelsInfo(completionHandler: @escaping (MirrorFlowModelsInfoResponseDTO?, Error?) -> Void)
    
    func getMirrorTestThresolds(completionHandler: @escaping (MirrorTestThresoldsResponseDTO?, Error?) -> Void)
    
    func getMirrorTestErrorMessages(completionHandler: @escaping (MirrorTestErrorMessagesResDTO?, Error?) -> Void)
}
