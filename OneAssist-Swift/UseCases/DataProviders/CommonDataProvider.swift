//
//  CommonDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol CommonDataProvider {
    func getPlanBenefits(requestDto: PlanBenefitsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(PlanBenefitsResponseDTO?, Error?)->Void)
    
    func getPlanBenefitsWithRank(requestDto: PlanBenefitsWithRankRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(PlanBenefitsWithRankResponseDTO?, Error?)->Void)
    
    func getRenewalPaymentResult(requestDto: RenewalPaymentRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(RenewalPaymentResponseDTO?, Error?)->Void)
    
    func saveDeviceRegistrationDetails(requestDto: DeviceRegistrationDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void)
    
    func saveNotificationStatus(requestDto: NotificationStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void)
    
    func getMasterData(requestDto: MasterDataRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(MasterDataResponseDTO?, Error?)->Void)
    
    func updatePostPaymentDetails(requestDto: UpdatePostPaymentDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UpdatePostPaymentDetailsResponseDTO?, Error?)->Void)
    
    func uploadInvoice(requestDto: UploadInvoiceRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UploadInvoiceResponseDTO?, Error?)->Void)
    
    func uploadInvoiceFD(requestDto: UploadInvoiceFDRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UploadInvoiceFDResponseDTO?, Error?)->Void)
    
    func buyNow(requestDto: BuyNowWhcRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BuyNowWhcResponseDTO?, Error?)->Void)
    
    func getServicesList(requestDto: GetServicesListRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void,completionHandlerBlock: @escaping(GetServicesListResponseDTO?, Error?)->Void)
    
    func getProductsSpecs(requestDto: GetProductSpecsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void,completionHandlerBlock: @escaping(GetProductSpecsResponseDTO?, Error?)->Void)
    
    func getSRsReportForPincode(requestDto: GetSRsReportForPincodeReqDTO?, taskCallBack: @escaping (_ task: Request?) -> Void,completionHandlerBlock: @escaping(GetSRsReportForPincodeResDTO?, Error?)->Void)
    
    func getProductCategory(requestDto: ProductCategoryRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(ProductCategoryResponseDTO?, Error?)->Void)
    
    func getStateCity(requestDto: StateCityRequestDTO?, taskCallBack:@escaping (_ task : Request?)-> Void, completionHandlerBlock:@escaping(StateCityResponseDTO?, Error?)->Void)
    
    func getCustomerMembershipDetails(requestDto: CustomerMembershipDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(CustomerMembershipDetailsResponseDTO?, Error?)->Void)
        
    func getCustDetails(requestDto: GetCustDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetCustDetailsResponseDTO?, Error?)->Void)
    
    func getDocsToUpload(requestDto: GetDocsToUploadRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetDocsToUploadResponseDTO?, Error?)->Void)
    
    func submitDocs(requestDto: SubmitDocRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SubmitDocResponseDTO?, Error?)->Void)
    
    func updateCustDetails(requestDto: UpdateCustDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void)
    
    func getBuyBackStatusData(requestDto: BuyBackStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BuyBackStatusResponseDTO?, Error?)->Void)
    
    func postMHCData(requestDto: MHCDataAddUpdateRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(MHCDataAddUpdateResponseDTO?, Error?)->Void)
    
    func getUserGroup(requestDto: UserGroupRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UserGroupResponseDTO?, Error?)->Void)
    
    func sendImageUploadLink(requestDto: SendSMSRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SendSMSResponseDTO?, Error?)->Void)
    
    func getIMEIKnowMoreResponse(requestDto: KnowMoreRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(IMEIFoundKnowMoreModel?, Error?)->Void)
    
    func getInvoices(requestDto: DownloadInvoiceRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DownloadInvoiceResponseDTO?, Error?)->Void)
    
    func getDocumentUrl(requestDto: DocumentUrlRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DocumentUrlResponseDTO?, Error?)->Void)
    
    func refundOrder(requestDto: RefundOrderRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(RefundOrderResponseDTO?, Error?)->Void)
    
    func get1oaLongUrl(url: String, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetLongURLResponseDTO?, Error?)->Void)
    
    func changeFDTestType(requestDto: FDTestTypeChangeRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void)
    
    func getFDHashKey(requestDto: FDHashKeyGenerationReqDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(FDHashKeyGenerationResDTO?, Error?)->Void)
}
