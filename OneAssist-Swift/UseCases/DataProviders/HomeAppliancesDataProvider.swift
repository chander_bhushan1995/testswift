//
//  HomeAppliancesDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol HomeAppliancesDataProvider {
    func getAssetMake(requestDto: AssetMakeRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (AssetMakeResponseDTO?, Error?) -> Void)
    
    func suggestPlans(request: SuggestPlanRequestDTO?, apiCacheChecker: ApiCacheChecker, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SuggestPlanResponseDTO?, Error?) -> Void)
}
