//
//  AuthenticationDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthenticationDataProvider {
    
    func getCustomerDetails(request: GetPermanentCustomerDetailsRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetPermanentCustomerDetailsResponseDTO?, Error?) -> Void)
    
    func loginUser(request: LoginMobileRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(LoginMobileResponseDTO?, Error?) -> Void)
    
    func logoutUser(request: LogoutRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(LogoutResponseDTO?, Error?) -> Void)
    
    func verifyOTP(request: VerifyOTPRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(VerifyOTPResponseDTO?, Error?) -> Void)
    
    func updateCustomerDetails(request: UpdateCustomerDetailsRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UpdateCustomerDetailsResponseDTO?, Error?) -> Void)
    
    func duplicateIMEICheck(request: DuplicateIMEIRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DuplicateIMEIResponseDTO?, Error?) -> Void)
    
    func getQuestionAnswerList(request: GetQuestionAnswerRequestDTO , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetQuestionAnswerResponseDTO?, Error?) -> Void) 
}
