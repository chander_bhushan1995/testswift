//
//  RemoteConfigUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 17/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import UIKit

let DEFAULT_MORE_ORDER_ID_VALUE = "100"

//MARK: - CDN DATA VERSIONING
class CDNDataVersioningDTO: BaseResponseDTO {
    var homeScreenData: String?
    var spotlightData: String?
    var buybackQuestionsData: String?
}

//MARK: - SOD Product Service Screen
class SODProductServiceScreenResDTO: BaseResponseDTO {
    var serviceSelectionCTAText: String?
    var cardsAndSequencing : [CardsAndSequencing] = []
    var notesData : NotesData?
    var factsData: FactsData?
    var headerConfig: HeaderConfig?
    var buttonActionData: ButtonActionData?
}

class ButtonActionData: ParsableModel {
    var title : String?
    var paragraph : String?
}

class HeaderConfig: ParsableModel {
    var backgroundColor: [String]?
}

class CardsAndSequencing: ParsableModel {
    var cardKey: String?
    var isVisible: Bool = true
}

class MirrorTestErrorMessagesResDTO: BaseResponseDTO {
    var downloadingRetryExceeded: String?
    var modelLoadingRetryExceeded: String?
    var objectDetectionRetryExceeded: String?
    var mirrorTestRetryExceeded: String?
    var frontPanelErrorMessages: FrontPanelUserErrorMessages?
}

class FrontPanelUserErrorMessages: ParsableModel {
    var objectDetectionFaild: String?
    var okWithLowConfidence: String?
    var obstractedImageDetected: String?
    var objectIsFar: String?
    var objectIsNear: String?
    var OCRFailed: String?
    var darkImageDetected: String?
    var OCRLeftRightFailed: String?
}

class MirrorTestThresoldsResponseDTO: BaseResponseDTO {
    var download: Thresolds?
    var preview_obj_detection: Thresolds?
    var front_panel_mirror_test: Thresolds?
    var loggingFor: Thresolds?
}

class Thresolds: ParsableModel {
    
    var model_download_threshold_time_in_ms : NSNumber?
    var min_time_to_check_download_speed : NSNumber?
    var no_of_retries_for_slow_internet : NSNumber?
    var no_of_retry_for_model_loading : NSNumber?
   
    var delay_for_process_next_frame : NSNumber?
    var max_ms_to_show_retry_bottom_sheet : NSNumber?
    var non_mandatory_classes_rety_count : NSNumber?
    var bottom_sheet_retry_count : NSNumber?
    var bounding_box_per_unit_percentage : NSNumber? //double
    var bounding_box_per_unit_percentage_near : NSNumber? // double
    var LRMinBrightness : NSNumber?
    var TBMinBrightness : NSNumber?
    var stripSize : NSNumber? // double
    var object_detection_min_confidence : NSNumber? // double
    var image_classification_ok_confidence : NSNumber? // double
    
    var delay_for_preview_process_next_frame : NSNumber?
    var max_ms_for_object_detect : NSNumber?
    var hashCodeAPICallThresold: NSNumber?
    var changeHashCodeThresold: NSNumber?
    var imeiDigitsComparisonForOCR: NSNumber?
    
    var far: Bool = false
    var dark: Bool = false
    var ocr: Bool = false
    var imageClassification: Bool = false
}

class MirrorFlowModelsInfoResponseDTO: BaseResponseDTO {
    var modelURL : String?
    var fileNameWithExt : String?
    var classificationModelInfo : ModelInfo?
    var objectDetectionModelInfo : ModelInfo?
}

class ModelInfo : ParsableModel {
    var labelFileName : String?
    var modelFileName : String?
}

class ModelDownloadingStatusScreenData: BaseResponseDTO {
    var title: String?
    var cards: [NotesData]?
}

class NotesData: ParsableModel {
    var title : String?
    var backgroundColor : String? = UIColor.notesYellow.toHexString()
    var listPoints : [ListPoint]?
}

class FactsData: ParsableModel {
    var imageURL: String?
    var backgroundColor: String? = UIColor.factsBGBlue.toHexString()
    var title: String?
}


//MARK: - Safety Guidlines
class SafetyGuidesResponseDTO: BaseResponseDTO {
    var title: String?
    var alertMessage: AlertMessage?
    var data : [SafetyData]?
}

class AlertMessage: ParsableModel {
    var heading : String?
    var subHeading : String?
    var bgColor: String?
}

class SafetyData: ParsableModel {
    var iconUrl : String?
    var imageUrl: String?
    var title : String?
    var descriptions : String?
    var backgroundColor: String?
}

//MARK: - OA Promises
class OAPromisesResponseDTO: BaseResponseDTO {
    var backgroundColor: String? = UIColor.violet.toHexString()
    var title: String?
    var promises:[String]?
    var imageURL: String?
}

// MARK: - Buy Plans
class BuyPlanResponseDTO: BaseResponseDTO {
    var buyPlans: [BuyPlanCategories]?
}

class BuyPlanCategories: ParsableModel {
    var category: String?
    var name: String?
    var isNew: NSNumber?
    var productCodes: [ProductCodes]?
}

class ProductCodes: ParsableModel {
    var heading: String?
    var title: String?
    var subtitle: String?
    var note: String?
    var image: String?
    var invoiceDaysLimit: NSNumber?
    var productCode: String?
    var newProduct: NSNumber?
    var freeTrail: String?
}

// MARK: - Mirror flow rating
class MirrorFlowRatingRespModel: BaseResponseDTO {
    var data : MirrorFlowRatingData?
}

class MirrorFlowRatingData: ParsableModel{
    var header: ActivationHeader?
    var offer_data : OfferData?
    var feedback_tags : [String]?
    var plans : [PitchPlan]?
}

class OfferData: ParsableModel {
    var offer_Text: String?
    var offer_desc: String?
    var offer_dis : String?
    var offer_code : String?
}

class PitchPlan: ParsableModel, JSONKeyCoder  {
    var title : String?
    var image: String?
    var desc : String?
    var note : String?
    var actionUrl: String?
    var butttonText : String?
    
    public func key(for key: String) -> String? {
        if key == "desc" {
            return "description"
        }
        return nil
    }

}

// MARK: - HA Categories
class HomeApplianceCategoryResponseDTO: BaseResponseDTO {
    var subCategories: [HomeApplianceSubCategories]?
    
    var numberOfColumns: NSNumber? //for SOD
    var otherSubCategories: OtherCategories? //for SOD
    
    func addSubCategory(sub: HomeApplianceSubCategories) {
        self.subCategories?.append(sub)
    }
}

class OtherCategories: ParsableModel {
    var imageUrl:String?
    var title: String?
    var subCategories: [HomeApplianceSubCategories]?
}

class HomeApplianceSubCategories: ParsableModel {
    var subCategoryName: String?
    var subCategoryCode: String?
    var subCatImageUrl: String?
    var subCatIconUrl: String?
    var order: String?
}

class WHCApplianceListResponseDTO: BaseResponseDTO {
    var appliancesList: [WHCAppliance]?
}

class WHCAppliance: ParsableModel {
    var applianceName: String?
    var applianceImageUrl: String?
}

class RenewalFactsResponseDTO: BaseResponseDTO {
    var title: String?
    var facts: [String]?
}

class FDbarcodeScannerResponseDTO: BaseResponseDTO {
    var enableScannerOS: [String]?
    var barCodeValidationOS: [String]?
    var savetoFirestore: Bool?
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        if key == "savetoFirestore", let value = value as? NSNumber {
            savetoFirestore = value.boolValue
        }
    }
}


// MARK: - CustomerTestimonials
class CustomerTestimonialsResponseDTO: BaseResponseDTO {
    var customerTestimonials: [CustomerTestimonial]?
}

class CustomerTestimonial: ParsableModel {
    var category: String?
    var testimonials: [Testimonial]?
}

class Testimonial: ParsableModel {
    var name: String?
    var testimonialDescription: String?
    var rating: NSNumber?
}

// MARK: - Exclusive Benfits
class ExclusiveBenfitsResponseDTO: BaseResponseDTO {
    var exclusiveBenfits: [ExclusiveBenfit]?
}

class ExclusiveBenfit: ParsableModel {
    var productCodes: [String]?
    var name, image, title, benefitDescription: String?
    var listPoints: [ListPoint]?
    var condition: String?
}

class ListPoint: ParsableModel {
    var type, title, buttonText, image, text: String?
}

// MARK: - Coupon
class CouponsResponseDTO: BaseResponseDTO {
    var coupons: [Coupon]?
}

class Coupon: ParsableModel {
    var category, coupondCode: String?
    var percentDiscount: NSNumber?
}

//MARK:- Why Choose OA
class WhyChooseOAResponse: BaseResponseDTO {
    var heading: String?
    var resions: [String]?
}

class SubscriptionBenefitsResponseDTO: BaseResponseDTO {
    public var planBenefits : [OAPlanBenefits]?
}

public class OAPlanBenefits: ParsableModel {
    public var category : String?
    public var data : [String]?
    public var isShow : NSNumber?
}

//MARK: - SOD Products & services Offers

class SODOffersResponseDTO: BaseResponseDTO  {
    var data : [ProductsData]?
}

class ProductsData: ParsableModel {
    var productCode : String?
    var habdHeaderText: String?
    var pmsHeaderText: String?
    var offers : [OffersData]?
}

class OffersData: ParsableModel {
    var service : String?
    var offerText : String?
    var couponCode : String?
    var detailedOfferText: String?
    var backgroundColors: [String] = ["#17D18D","#1DC5C7"]
    var imageURL: String?
}

//MARK: - PlatformRatingsDesc
class PlatformRatingsResponse: BaseResponseDTO {
    var heading: String?
    var platforms: [PlatformsRatingData]?
}
class PlatformsRatingData: ParsableModel, JSONKeyCoder {
    var rating: String?
    var desc: String?
    public func key(for key: String) -> String? {
        if key == "desc" {
            return "description"
        }
        return nil
    }
}

//MARK: - OAAssuredServices
class OAAssuredServicesResponse: BaseResponseDTO {
    var heading : String?
    var services : [AssuredServices]?
}

class AssuredServices: ParsableModel, JSONKeyCoder {
    var image : String?
    var title : String?
    var desc : String?
    
    public func key(for key: String) -> String? {
        if key == "desc" {
            return "description"
        }
        return nil
    }
}

// MARK: - Plan Tagline
class PlanTaglineResponseDTO: BaseResponseDTO {
    var defaultTagline: String?
    var taglines: [Tagline]?
}

class Tagline: ParsableModel {
    var mandatory_servicelist: [String]?
    var tagline: String?
    var productCodes: [String]?
}

// MARK: - ID Fence Detail Page
class PlanDetailPageData: BaseResponseDTO {
    var headerSubtitle: String?
    var riskCalculatorDetail: RiskCalculatorDetailResponseModel?
    var notes: [PlanDetailPageTypeModel]?
    var issues: PlanDetailPageDataModel?
    var carousel: PlanDetailPageDataModel?
    var timeline: [PlanDetailPageTypeModel]?
    var popupMoreAboutIssues: PlanDetailPageDataModel?
    var popupMoreAboutPremium: PlanDetailPageDataModel?
    var blogs: PlanDetailPageTypeModel?
    var creditScoreFeatures: PlanDetailPageTypeModel?
    var whatIdentityTheft: PlanDetailPageListModel?
}

class RiskCalculatorDetailResponseModel: ParsableModel {
    var title: String?
    var questions: [RiskCalculatorQuestionsResponseModel]?
}

class RiskCalculatorQuestionsResponseModel: ParsableModel {
    var id: NSNumber?
    var cardType: String?
    
    var question: String?
    var correctAnswer: String?
    var correctResponse: String?
    var badResponse: String?
    var correctHeader: String?
    var wrongHeader: String?
    
    var cardDescription: String?
    var cardCTA: String?
}

class PlanDetailPageTypeModel: ParsableModel {
    var isFreeTrial: NSNumber?
    var data: PlanDetailPageDataModel?
    
    var trial: Bool? { return isFreeTrial?.boolValue }
}

class PlanDetailPageDataModel: ParsableModel {
    var title: String?
    var subtitle: String?
    var list: [PlanDetailPageListModel]?
    var note: String?
}

class PlanDetailPageListModel: ParsableModel {
    var title: String?
    var subtitle: String?
    var imageUrl: String?
    var webPageURL: String?
    var webViewTitle: String?
}

class MirrorTestVideoGuideResponseDTO: BaseResponseDTO {
    var activationHeader: ActivationHeader?
    var videoGuide: VideoGuide?
    var guidePoints: GuidePoints?
    var quickTips: GuidePoints?
}

class ActivationHeader: ParsableModel {
    var title: String?
    var subTitle: String?
    var subTitleBold: NSNumber?
    var reuploadboth: ActivationHeader?
    var reuploadfront: ActivationHeader?
    var reuploadback: ActivationHeader?
}

class VideoGuide: ParsableModel {
    var title: String?
    var buttonText: String?
    var user_guide_video_url: String?
    var showSamplePhoto: NSNumber?
}

class GuidePoints: ParsableModel {
    var title: String?
    var subTitle: String?
    var points: [ImageWithValue]?
    var buttonActionText: String?
}

class MirrorTestSamplePhotoDTO: BaseResponseDTO {
    var facts: [Facts]?
}

class Facts: ParsableModel {
    var title: String?
    var subTitle: String?
    var points: [String]?
    var imageUrl: String?
    var lineSpacing: CGFloat = 10.0
}

class ImageWithValue: ParsableModel {
    var image: String?
    var value: String?
}

class MirrorTestLanguageModelDTO: BaseResponseDTO {
    var lang_data: [LanguageData]?
}

class LanguageData:ParsableModel {
    var order: NSNumber = NSNumber(value: 0)
    var language: String?
    var locate: String?
    var languageCode: String?
    var data: Language?
}

class Language: ParsableModel {
    var CASE_1: LanguageCase?
    var CASE_2: LanguageCase?
    var CASE_3: LanguageCase?
    var CASE_4: LanguageCase?
    var CASE_5: LanguageCase?
    var CASE_6: LanguageCase?
    var CASE_7: LanguageCase?
    var CASE_8: LanguageCase?
}

class LanguageCase: ParsableModel {
    var text: String?
    var delay_iOS: NSNumber = NSNumber(floatLiteral: 0.0)
}

class PhotoClickHelpPointsDTO: BaseResponseDTO {
    var photo_click_help_points: PhotoClickHelpPoints?
    var agree_submit_photos: UserDeclarations?
}

class PhotoClickHelpPoints: ParsableModel {
    var front_panel: Facts?
    var pre_front_panel: Facts?
    var back_panel: GuidePoints?
    var back_panel_animated_hints: [String]?
}

class ChatCellModel: ParsableModel {
    var title: String?
}

class UserDeclarations: ParsableModel {
    var ischecked: Bool = false
    var title: String?
    var facts: [String]?
    var points: [String]?
}
    
class NotificationAlertResponseDTO: BaseResponseDTO {
    var firstTimePrompt : NotificationPromptModel?
    var settingPrompt : NotificationPromptModel?
    var enablePrompt : NotificationPromptModel?
}

class NotificationPromptModel: ParsableModel {
    var promptType: String? // firstTime, setting, enable
    var header: String?
    var paragraph: String?
    var firstButton: String?
    var secondButton: String?
}

class ModuleFlowSequencing: ParsableModel {
    var sodFlowSequencing: SODFlowSequencing?
}

class SODFlowSequencing: ParsableModel {
    var screen_1: String = ""
    var screen_2: String = ""
    lazy var screens = [screen_1, screen_2]
}

class BuyPlanRevampRequestUseCase: BaseRequestUseCase<BaseRequestDTO, BuyPlanResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (BuyPlanResponseDTO?, Error?) -> Void) {
        dataProvider.getBuyPlanCategory(completionHandler: completionHandler)
    }
}

class OAPromisesRequestUseCase: BaseRequestUseCase<BaseRequestDTO, OAPromisesResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (OAPromisesResponseDTO?, Error?) -> Void) {
        dataProvider.getOAPromises(completionHandler: completionHandler)
    }
}

class SafetyGuidlinesUseCase: BaseRequestUseCase<BaseRequestDTO, SafetyGuidesResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (SafetyGuidesResponseDTO?, Error?) -> Void) {
        dataProvider.getSafetyGuidLines(completionHandler: completionHandler)
    }
}

class SODAppliancesUseCase: BaseRequestUseCase<BaseRequestDTO, HomeApplianceCategoryResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void) {
        dataProvider.getSODAppliances(completionHandler: completionHandler)
    }
}

class HomeApplianceCategoryRequestUseCase: BaseRequestUseCase<BaseRequestDTO, HomeApplianceCategoryResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void) {
        dataProvider.getHomeApplianceCategory(completionHandler: completionHandler)
    }
}

// ha other categories

class HomeApplianceOtherCategoryRequestUseCase: BaseRequestUseCase<BaseRequestDTO, HomeApplianceCategoryResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void) {
        dataProvider.getHomeApplianceOtherCategory(completionHandler: completionHandler)
    }
}

class WHCApplianceListUseCase: BaseRequestUseCase<BaseRequestDTO, WHCApplianceListResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (WHCApplianceListResponseDTO?, Error?) -> Void) {
        dataProvider.getWHCApplianceList(completionHandler: completionHandler)
    }
}

class QuickFactsListUseCase: BaseRequestUseCase<BaseRequestDTO, RenewalFactsResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (RenewalFactsResponseDTO?, Error?) -> Void) {
        dataProvider.getQuickFactsList(completionHandler: completionHandler)
    }
    
}


class FDbarcodeScannersupporUseCase: BaseRequestUseCase<BaseRequestDTO, FDbarcodeScannerResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (FDbarcodeScannerResponseDTO?, Error?) -> Void) {
        dataProvider.getFDBarcodeSupportList(completionHandler: completionHandler)
    }
}
// Plan purchase master data

class CustomerTestimonialsUseCase: BaseRequestUseCase<BaseRequestDTO, CustomerTestimonialsResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (CustomerTestimonialsResponseDTO?, Error?) -> Void) {
        dataProvider.getCustomerTestimonials(completionHandler: completionHandler)
    }
}

class CouponCodeUseCase: BaseRequestUseCase<BaseRequestDTO, CouponsResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (CouponsResponseDTO?, Error?) -> Void) {
        dataProvider.getCategoryCouponCodes(completionHandler: completionHandler)
    }
}

class ExclusiveBenfitsUseCase: BaseRequestUseCase<BaseRequestDTO, ExclusiveBenfitsResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (ExclusiveBenfitsResponseDTO?, Error?) -> Void) {
        dataProvider.getCategoryExclusiveBenefits(completionHandler: completionHandler)
    }
}

class PlanTaglineUseCase: BaseRequestUseCase<BaseRequestDTO, PlanTaglineResponseDTO> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (PlanTaglineResponseDTO?, Error?) -> Void) {
        dataProvider.getPlanTagline(completionHandler: completionHandler)
    }
}

class IDFenceDetailPageUseCase: BaseRequestUseCase<BaseRequestDTO, PlanDetailPageData> {
    
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (PlanDetailPageData?, Error?) -> Void) {
        dataProvider.getIDFenceDetailPageData(completionHandler: completionHandler)
    }
}

class OAAssuredServicesUseCase: BaseRequestUseCase<BaseRequestDTO, OAAssuredServicesResponse> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (OAAssuredServicesResponse?, Error?) -> Void) {
        dataProvider.getOAAssuredServices(completionHandler: completionHandler)
    }
}

class WhyChooseOAUseCase:BaseRequestUseCase<BaseRequestDTO, WhyChooseOAResponse> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (WhyChooseOAResponse?, Error?) -> Void) {
        dataProvider.getWhyChooseOA(completionHandler: completionHandler)
    }
}

class OAPlatformRatingsUseCase:BaseRequestUseCase<BaseRequestDTO, PlatformRatingsResponse> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (PlatformRatingsResponse?, Error?) -> Void) {
        dataProvider.getPlatformRatings(completionHandler: completionHandler)
    }
}

class OASODAppliancesData:BaseRequestUseCase<BaseRequestDTO, PlatformRatingsResponse> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (PlatformRatingsResponse?, Error?) -> Void) {
        dataProvider.getPlatformRatings(completionHandler: completionHandler)
    }
}

class OASubscriptionBenefitsUseCase:BaseRequestUseCase<BaseRequestDTO, SubscriptionBenefitsResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (SubscriptionBenefitsResponseDTO?, Error?) -> Void) {
        dataProvider.getSubscriptionBenefits(completionHandler: completionHandler)
    }
}

class OASODProductsOffersUseCase: BaseRequestUseCase<BaseRequestDTO, SODOffersResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (SODOffersResponseDTO?, Error?) -> Void) {
        dataProvider.getSODProductsOffers(completionHandler: completionHandler)
    }
}

class GetMirrorTestVideoGuideUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorTestVideoGuideResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorTestVideoGuideResponseDTO?, Error?) -> Void) {
        dataProvider.getMirrorTestVideoGuide(completionHandler: completionHandler)
    }
}

class GetMirrorTestSamplePhotsUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorTestSamplePhotoDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorTestSamplePhotoDTO?, Error?) -> Void) {
        dataProvider.getMirrorTestSamplePhotos(completionHandler: completionHandler)
    }
}

class GetLanguageDataUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorTestLanguageModelDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorTestLanguageModelDTO?, Error?) -> Void) {
        dataProvider.getMirrorTestLanguage(completionHandler: completionHandler)
    }
}

class GetPhotoHelpPointsUseCase: BaseRequestUseCase<BaseRequestDTO, PhotoClickHelpPointsDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (PhotoClickHelpPointsDTO?, Error?) -> Void) {
        dataProvider.getPhotoHelpPoints(completionHandler: completionHandler)
    }
}

class OAMirrorFlowRatingUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorFlowRatingRespModel> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorFlowRatingRespModel?, Error?) -> Void) {
        dataProvider.getMirrorFlowRatingData(completionHandler: completionHandler)
    }
}

class ModelDownloadingStatusScreenUseCase: BaseRequestUseCase<BaseRequestDTO, ModelDownloadingStatusScreenData> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (ModelDownloadingStatusScreenData?, Error?) -> Void) {
        dataProvider.getModelDownloadingStatusScreenData(completionHandler: completionHandler)
    }
}

class OANotificationPromptUseCase: BaseRequestUseCase<BaseRequestDTO, NotificationAlertResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (NotificationAlertResponseDTO?, Error?) -> Void) {
        dataProvider.getNotificationPrompt(completionHandler: completionHandler)
   }
}

class SODProductServiceLayoutUseCase: BaseRequestUseCase<BaseRequestDTO, SODProductServiceScreenResDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (SODProductServiceScreenResDTO?, Error?) -> Void) {
        dataProvider.getSODProductServiceScreen(completionHandler: completionHandler)
    }
}

class SODProductScreenLayoutUseCase: BaseRequestUseCase<BaseRequestDTO, SODProductServiceScreenResDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (SODProductServiceScreenResDTO?, Error?) -> Void) {
        dataProvider.getSODProductScreen(completionHandler: completionHandler)
    }
}

class CDNVersioningUseCase: BaseRequestUseCase<BaseRequestDTO, CDNDataVersioningDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (CDNDataVersioningDTO?, Error?) -> Void) {
        dataProvider.getCDNDataVersioning(completionHandler: completionHandler)
    }
}

class TFLiteModelsInfoUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorFlowModelsInfoResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorFlowModelsInfoResponseDTO?, Error?) -> Void) {
        dataProvider.getSmartMirrorFlowModelsInfo(completionHandler: completionHandler)
    }
}

class MirrorTestThresoldsUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorTestThresoldsResponseDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorTestThresoldsResponseDTO?, Error?) -> Void) {
        dataProvider.getMirrorTestThresolds(completionHandler: completionHandler)
    }
}

class MirrorTestErrorMessagesUseCase: BaseRequestUseCase<BaseRequestDTO, MirrorTestErrorMessagesResDTO> {
    let dataProvider = dataProviderFactory.remoteConfigDataProvider()
    
    override func getRequest(requestDto: BaseRequestDTO?, completionHandler: @escaping (MirrorTestErrorMessagesResDTO?, Error?) -> Void) {
        dataProvider.getMirrorTestErrorMessages(completionHandler: completionHandler)
    }
}

