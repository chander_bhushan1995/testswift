//
//  OfferSyncUseCase.swift
//  OneAssist-Swift
//
//  Created by Varun on 03/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class OfferSyncUseCase {
    
    static let shared = OfferSyncUseCase()
    private init() {}
    
    func getBookmarkedOffers() -> [Offer] {
        let context = CoreDataStack.sharedStack.secondaryContext
        if let list: [Offer] = CoreDataStack.sharedStack.fetchObject(predicateString: "bookmarked == '1'", inManagedObjectContext: context) {
            
            let bookmarkedList = list.filter({ (offer) -> Bool in
                let expiringDate = Date(string: offer.endDate ?? "", formatter: DateFormatter.serverDateFormat) ?? Date.currentDate()
                let order = Date.compareDate(fromdate: expiringDate, todate: Date())
                return order != -1
            })
            return bookmarkedList
        }
        return []
    }
    
    private func getPrimaryId(offer: BankCardOffers) -> String {
        var primaryId = ""
        
        if let id = offer.offerId {
            primaryId = id
        } else {
            let offerCode = offer.offerCode ?? ""
            let outletCode = offer.outletCode ?? ""
            primaryId = offerCode + outletCode
        }
        return primaryId
    }
    
    func isOfferBookmarked(offer: BankCardOffers) -> Bool {
        let context = CoreDataStack.sharedStack.secondaryContext
        let primaryId = getPrimaryId(offer: offer)
        if let list: [Offer] = CoreDataStack.sharedStack.fetchObject(predicateString: "primaryId == '\(primaryId)'", inManagedObjectContext: context), list.count > 0 {
            return list[0].bookmarked == 1
        }
        return false
    }
    
    func storeOfferInDb(offer: BankCardOffers) {
       
        let primaryId = getPrimaryId(offer: offer)
        let context = CoreDataStack.sharedStack.secondaryContext
        let list: [Offer]? = CoreDataStack.sharedStack.fetchObject(predicateString: "primaryId == '\(primaryId)'", inManagedObjectContext: context)
        
        var offerDB: Offer!
        if let list = list, list.count > 0 {
            offerDB = list[0]
            
        } else {
            offerDB =  Offer(context: context)
            offerDB.bookmarkId = offer.bookmarkId
        }
        
        offerDB.primaryId = primaryId
        offerDB.bookmarked = offer.bookmark?.int32Value ?? 0
        offerDB.syncStatus = 1
        offerDB.endDate = offer.endDate
        offerDB.offerProvider = offer.offerId != nil ? "CG" : "MT"
        offerDB.offerReference = offer.offerId ?? offer.offerCode
        offerDB.outletCode = offer.outletCode
        offerDB.brandImage = offer.image
        offerDB.merchantName = offer.merchantName
        offerDB.discount = offer.offerTitle
        offerDB.offerId = offer.offerId
        
        if let cardDetailList = offer.cardDetailsList {
            for cardDetail in cardDetailList {
                let card = Issuer(context: context)
                card.cardTypeId = cardDetail.cardTypeId
                card.cardIssuerId = cardDetail.cardIssuerId
                card.cardIssuerName = cardDetail.cardIssuerName
                card.cardIssuerCode = cardDetail.cardIssuerCode
                card.imagePath = cardDetail.imagePath
                card.helpline1 = cardDetail.helpline1
                card.helpline2 = cardDetail.helpline2
                card.landline1 = cardDetail.landline1
                card.landline2 = cardDetail.landline2
                card.ranking = cardDetail.ranking?.int64Value ?? 0
                card.tagging = cardDetail.tagging
                card.bookmarkCard = 1
                offerDB.addToCardDetailList(card)
            }
        }

        CoreDataStack.sharedStack.saveContext(context: context) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BookmarkChanged"), object: nil)
            print("Doneeeee")
        }
    }
    
    func syncOffers() {
        guard Utilities.isCustomerVerified() else { return }
        let context = CoreDataStack.sharedStack.mainContext
        let list: [Offer]? = CoreDataStack.sharedStack.fetchObject(predicateString: "syncStatus == '1'", inManagedObjectContext: context)
        
        if let listOffers = list, listOffers.count > 0 {
            
            let deleteOffers = listOffers.filter({ (offer) -> Bool in
                return offer.bookmarkId != nil && offer.bookmarked == 0
            })
            
            let locallyDeleteOffers = listOffers.filter({ (offer) -> Bool in
                return offer.bookmarkId == nil && offer.bookmarked == 0
            })
            
            let deleteReqOffers = deleteOffers.map({ (offer) -> ExpiredBookmark in
                let bookmark = ExpiredBookmark(dictionary: [:])
                bookmark.bookmarkId = offer.bookmarkId
                return bookmark
            })
            
            let addOffers = listOffers.filter({ (offer) -> Bool in
                return offer.bookmarkId == nil && offer.bookmarked == 1
            })
            
            let locallyAddOffers = listOffers.filter({ (offer) -> Bool in
                return offer.bookmarkId != nil && offer.bookmarked == 1
            })
            
            let addReqOffers = addOffers.map({ (offer) -> BookmarkedOffer in
                let bookmark = BookmarkedOffer()
                bookmark.customerId = UserCoreDataStore.currentUser?.cusId
                bookmark.endDate = offer.endDate
                bookmark.offerProvider = offer.offerProvider
                bookmark.offerReference = offer.offerReference
                bookmark.outletCode = offer.outletCode
                bookmark.merchantName = offer.merchantName
                bookmark.image = offer.image
                bookmark.offerTitle = offer.offerTitle
                return bookmark
            })
            
            if deleteReqOffers.count > 0 {
                let request = DeleteBookmarkedOffersRequestDTO()
                request.expiredBookmarks = deleteReqOffers
                let _ = DeleteBookmarkedOffersRequestUseCase.service(requestDTO: request, completionBlock: { (usecase, response, error) in
                    
                    if error == nil {
                        let context = CoreDataStack.sharedStack.secondaryContext
                        if let list: [Offer] = CoreDataStack.sharedStack.fetchObject(predicateString: "bookmarked == '0'", inManagedObjectContext: context) {
                            for entity in list {
                                context.delete(entity)
                            }
                        }
                        CoreDataStack.sharedStack.saveContext(context: context) {}
                    }
                    
                })
                
            }
            
            if addReqOffers.count > 0 {
                
                let requestAdd = AddBookmarkedOffersRequestDTO()
                requestAdd.bookmarkedOffers = addReqOffers
                let _ = AddBookmarkedOffersRequestUseCase.service(requestDTO: requestAdd, completionBlock: { (usecase, response, error) in
                    
                    let context = CoreDataStack.sharedStack.secondaryContext
                    if let list: [Offer] = CoreDataStack.sharedStack.fetchObject(predicateString: "bookmarked == '1'", inManagedObjectContext: context) {
                        
                        for entity in list {
                            
                            if let data = response?.data {
                                for validData in data {
                                    if validData.offerReference == entity.offerReference {
                                        
                                        entity.bookmarked = 1
                                        entity.syncStatus = 0
                                        entity.bookmarkId = validData.bookmarkId?.description
                                        
                                        break
                                    }
                                }
                            }
                        }
                    }
                    CoreDataStack.sharedStack.saveContext(context: context) {}
                    
                })
            }
            
            if locallyDeleteOffers.count > 0 {
                for offer in locallyDeleteOffers {
                    context.delete(offer)
                }
                
            }
            
            if locallyAddOffers.count > 0 {
                for offer in locallyAddOffers {
                    offer.syncStatus = 0
                }
                
            }
            
            CoreDataStack.sharedStack.saveContext(context: context) {}
        }
        
        
    }

    func storeBookMarkOfferInDb(offers: [BankCardOffers], apiCacheChecker: ApiCacheChecker)->[Offer]? {
        let context = CoreDataStack.sharedStack.secondaryContext
        
        // remove old records if any
        if let list: [Offer] = CoreDataStack.sharedStack.fetchObject(predicateString: nil, inManagedObjectContext: context) {
            for entity in list {
                context.delete(entity)
            }
        }
        
        let dbList = offers.map({ (offer) -> Offer in
            let primaryId = getPrimaryId(offer: offer)
            
            let offerDB = Offer(context: context)
            offerDB.bookmarkId = offer.bookmarkId
            offerDB.primaryId = primaryId
            offerDB.bookmarked = offer.bookmark?.int32Value ?? 0
            offerDB.syncStatus = 0
            offerDB.endDate = offer.endDate
            offerDB.offerProvider = offer.offerId != nil ? "CG" : "MT"
            offerDB.offerReference = offer.offerId ?? offer.offerCode
            offerDB.outletCode = offer.outletCode
            offerDB.brandImage = offer.image
            offerDB.merchantName = offer.merchantName
            offerDB.discount = offer.offerTitle
            offerDB.offerId = offer.offerId
            
            if let cardDetailList = offer.cardDetailsList {
                for cardDetail in cardDetailList {
                    let card = Issuer(context: context)
                    card.cardTypeId = cardDetail.cardTypeId
                    card.cardIssuerId = cardDetail.cardIssuerId
                    card.cardIssuerName = cardDetail.cardIssuerName
                    card.cardIssuerCode = cardDetail.cardIssuerCode
                    card.imagePath = cardDetail.imagePath
                    card.helpline1 = cardDetail.helpline1
                    card.helpline2 = cardDetail.helpline2
                    card.landline1 = cardDetail.landline1
                    card.landline2 = cardDetail.landline2
                    card.ranking = cardDetail.ranking?.int64Value ?? 0
                    card.tagging = cardDetail.tagging
                    card.bookmarkCard = 1
                    offerDB.addToCardDetailList(card)
                }
            }
            return offerDB
        })
        
        CoreDataStack.sharedStack.insertApiCacheData(apiCacheChecker: apiCacheChecker)
        CoreDataStack.sharedStack.saveContext(context: context) {}
        return dbList
    }
    
    
    func syncAllBookMarksFromServer() {
        let request = BookmarkedCardOffersRequestDTO()
        let cusId = Int(UserCoreDataStore.currentUser?.cusId ?? "0")
        request.customerId = NSNumber(integerLiteral: cusId ?? 0)
        request.offerType = ""
        request.pageNo = 1
        request.pageSize = 400
        request.bookmarked = true
        
        let _ = BookmarkedCardOffersRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
        }
    }
}
