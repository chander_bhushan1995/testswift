//
//  DLSTabBar.swift
//  TabbarProgrametically
//
//  Created by Ankur Batham on 26/03/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import UIKit

let pi = CGFloat.pi
let pi2 = CGFloat.pi / 2
let barHeight : CGFloat = 55
let barBottomRadius : CGFloat = 10
let marginLeft : CGFloat = 10
let marginRight : CGFloat = 10
let marginBottom : CGFloat = 10
var barTopRadius : CGFloat = 2

class DLSTabBar: UITabBar {
    public var barBackColor : UIColor = UIColor.white

    private lazy var background: CAShapeLayer = {
        let result = CAShapeLayer();
        result.fillColor = self.barBackColor.cgColor
        result.shadowColor = UIColor.dlsShadow.cgColor
        result.shadowOpacity = 1
        result.shadowOffset = CGSize(width: 2, height: 2)
        result.shadowRadius = 4
        return result
    }()
    
    private var barRect : CGRect{
        get{
            let h = barHeight
            let w = bounds.width - (marginLeft + marginRight)
            let x = bounds.minX + marginLeft
            
            let rect = CGRect(x: x, y: 0, width: w, height: h)
            return rect
        }
    }
    
    
    private func createBackgroundPath() -> CGPath{
        let rect = barRect
        let topLeftRadius = barTopRadius
        let topRightRadius = barTopRadius
        let bottomRigtRadius = barBottomRadius
        let bottomLeftRadius = barBottomRadius
        
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: rect.minX + topLeftRadius, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX - topLeftRadius, y:rect.minY))
        
        path.addArc(withCenter: CGPoint(x: rect.maxX - topRightRadius, y: rect.minY + topRightRadius), radius: topRightRadius, startAngle:3 * pi2, endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - bottomRigtRadius))
        
        path.addArc(withCenter: CGPoint(x: rect.maxX - bottomRigtRadius, y: rect.maxY - bottomRigtRadius), radius: bottomRigtRadius, startAngle: 0, endAngle: pi2, clockwise: true)
        path.addLine(to: CGPoint(x: rect.minX + bottomRigtRadius, y: rect.maxY))
        
        path.addArc(withCenter: CGPoint(x: rect.minX + bottomLeftRadius, y: rect.maxY - bottomLeftRadius), radius: bottomLeftRadius, startAngle: pi2, endAngle: pi, clockwise: true)
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY + topLeftRadius))
        
        path.addArc(withCenter: CGPoint(x: rect.minX + topLeftRadius, y: rect.minY + topLeftRadius), radius: topLeftRadius, startAngle: pi, endAngle: 3 * pi2, clockwise: true)
        path.close()
        
        return path.cgPath
    }
    
    private func getTabBarItemViews() -> [(item : UITabBarItem, view : UIView)]{
        guard let items = self.items else{
            return[]
        }
        var result : [(item : UITabBarItem, view : UIView)] = []
        for item in items {
            if let v = getViewForItem(item: item) {
                result.append((item: item, view: v))
            }
        }
        return result
    }
    
    private func getViewForItem(item : UITabBarItem?) -> UIView?{
        if let item = item {
            let v = item.value(forKey: "view") as? UIView
            return v
        }
        return nil
    }
    
    private func positionItem(barRect : CGRect, totalCount : Int, idx : Int, item : UITabBarItem, view : UIView){
        let margin : CGFloat = 6
        let x = view.frame.origin.x
        let y = barRect.origin.y + margin
        let h = barHeight - (margin * 2)
        let w = view.frame.width
        view.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    private func layoutElements(selectedChanged : Bool){
        self.background.path = self.createBackgroundPath()
        let items = getTabBarItemViews()
        if items.count <= 0 {
            return
        }
        
        let barR = barRect
        let total = items.count
        for (idx, item) in items.enumerated() {
            self.positionItem(barRect: barR, totalCount: total, idx: idx, item: item.item, view: item.view)
        }
    }
    
    private func setup(){
        self.isTranslucent = true
        self.backgroundColor = UIColor.clear
        self.layer.insertSublayer(background, at: 1)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let frostView = UIView()
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        frostView.frame = CGRect(x: 0.0, y: 0.0, width: bounds.size.width, height: bounds.size.height + bottomSafeArea + 20.0)
        frostView.backgroundColor = UIColor.white
        frostView.autoresizingMask = .flexibleWidth
        insertSubview(frostView, at: 0)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = barHeight + marginBottom
        return sizeThatFits
    }
    
//    override var selectedItem: UITabBarItem? {
//        get{
//            return super.selectedItem
//        }
//        set{
//            let changed = (super.selectedItem != newValue)
//            super.selectedItem = newValue
//            if changed {
//                layoutElements(selectedChanged: changed)
//            }
//        }
//    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        background.fillColor = self.barBackColor.cgColor
        self.layoutElements(selectedChanged: false)
    }
    
    override func prepareForInterfaceBuilder() {
        self.isTranslucent = true
        self.backgroundColor = UIColor.clear
        
        background.fillColor = self.barBackColor.cgColor
    }
    
}

