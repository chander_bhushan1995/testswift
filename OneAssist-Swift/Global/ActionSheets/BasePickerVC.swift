//
//  BasePickerVC.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 08/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BasePickerVC: BaseVC {
    private let contentBottomInset:CGFloat = 0.0
    private let contentTopInset:CGFloat = 0.0
    private var height:CGFloat!
    fileprivate var detailView:UIView!
    fileprivate var isPresenting = false
    var opacityComponent:CGFloat = 0.7
    var backgroundColor:UIColor = .black
    var dismissCallback: (()->())?
    var isGestureEnable: Bool = true
    private var pickerView:(width:CGFloat, height:CGFloat) = (UIScreen.main.bounds.size.width,UIScreen.main.bounds.size.height)
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    convenience init(withView detailView:UIView, height:CGFloat=100){
        self.init()
        self.detailView = detailView
        self.height = height
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isGestureEnable {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BasePickerVC.handleTap(_:)))
            tapGesture.delegate = self
            view.addGestureRecognizer(tapGesture)
        }
        view.backgroundColor = backgroundColor.withAlphaComponent(opacityComponent)
        let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 20
        let frame = CGRect(x: 0, y: pickerView.height-(self.height+topPadding-20), width: pickerView.width, height: self.height+(topPadding-20))
        detailView.frame = frame
        view.frame = UIScreen.main.bounds
        self.view.addSubview(detailView)
    }
}

extension BasePickerVC: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            detailView.frame.origin.y += detailView.bounds.height
            view.alpha = 0
            
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.detailView.frame.origin.y -= self.detailView.bounds.height
                self.view.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.detailView.frame.origin.y += self.detailView.bounds.height
                self.view.alpha = 0
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}

extension BasePickerVC : UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.detailView) == true {
            return false
        }
        return true
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true){
            if let callback = self.dismissCallback {
                callback()
            }
        }
    }
}
