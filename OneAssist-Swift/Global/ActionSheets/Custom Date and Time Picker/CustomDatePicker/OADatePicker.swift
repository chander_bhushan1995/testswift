//
//  OADatePicker.swift
//  vertical-date-picker
//
//  Created by Pankaj Verma on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//


import UIKit

protocol OADatePickerDelegate:class {
    func dateSelected(_ date:Date)
}
enum DateType:Int{
    case Day
    case Month
    case Year
}
class OADatePicker: UIView {
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
        registerCell()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
        registerCell()
    }
    
    //MARK:- outlets
    @IBOutlet weak var dateRow: UICollectionView!
    @IBOutlet weak var monthRow: UICollectionView!
    @IBOutlet weak var yearRow: UICollectionView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var selectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noteLabel: BodyTextRegularBlackLabel!
    
    //MARK:- public properties
    
    /// Minimum (oldest) allowed date
    public var minimumDate:Date?
    
    /// Maximum allowed date
    public var maximumDate:Date?
    
    /// Initial date
    public var date:Date = Date() {
        didSet{
            configureDataSource()
        }
    }
    
    public var note:String? {
        didSet {
            noteLabel?.text = note
        }
    }
    
    weak var delegate:OADatePickerDelegate?
    
    // selectd date background
    public var selectedBgColor: UIColor = UIColor.veryLightGray2
    public var selectionType: SelectionType = .square
    
    //    public var cellSize:CGFloat = 44
    public var numberOfCellOnScreen:Double = 5
    
    //MARK:- private properties
    fileprivate var years = ModelDate.getYears()
    fileprivate let Months = ModelDate.getMonths()
    fileprivate var days:[ModelDate] = []
    fileprivate var infiniteScrollingBehaviourForYears: InfiniteScrollingBehaviour!
    fileprivate var infiniteScrollingBehaviourForDays: InfiniteScrollingBehaviour!
    fileprivate var infiniteScrollingBehaviourForMonths: InfiniteScrollingBehaviour!
    @IBAction func getDate(_ sender: UIButton) {
        self.checkForBoundaryDate()
        let date = CalendarHelper.getThatDate(days, Months, years)
        self.isHidden = true
        delegate?.dateSelected(date)
    }
}

//MARK:- initial configuration
extension OADatePicker{
    
    /// Load nib OADatePicker
    fileprivate func loadNib(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("OADatePicker", owner: self, options: nil)
        addSubview(calendarView)
        selectionViewHeight.constant = Utilities.datePickerHeight * 0.16
        calendarView.frame = self.bounds
//        configureDataSource()
        delay(0.1){
            self.selectionView.backgroundColor = self.selectedBgColor
            self.selectionView.selectSelectionType(selectionType: self.selectionType)
            self.selectDate(date: self.date)
            self.calendarView.backgroundColor = UIColor.white
        }
    }
    
    /// Register cell for collectionViews: dateRow, monthRow and yearRow
    fileprivate func registerCell(){
        let bundle = Bundle(for: self.classForCoder)
        let nibName = UINib(nibName: "OADatePickerCell", bundle:bundle)
        dateRow.register(nibName, forCellWithReuseIdentifier: "cell")
        monthRow.register(nibName, forCellWithReuseIdentifier: "cell")
        yearRow.register(nibName, forCellWithReuseIdentifier: "cell")
    }
    
    /// Enable vertical infinite scrolling behaviour
    fileprivate func configureDataSource(){
        let currentMonth = self.date.monthStr
        for month in Months {
            if month.value == currentMonth {
                month.isSelected = true
            }
        }
        days = ModelDate.getDays(years, Months)
        if let _ = infiniteScrollingBehaviourForDays {}
        else {
            //            let configuration = CollectionViewConfiguration(layoutType: .fixedSize(sizeValue: cellSize, lineSpacing: 0), scrollingDirection: .vertical)
            let configuration = CollectionViewConfiguration(layoutType: .numberOfCellOnScreen(numberOfCellOnScreen), scrollingDirection: .vertical)
            infiniteScrollingBehaviourForMonths = InfiniteScrollingBehaviour(withCollectionView: monthRow, andData: Months, delegate: self, configuration: configuration)
            infiniteScrollingBehaviourForDays = InfiniteScrollingBehaviour(withCollectionView: dateRow, andData: days, delegate: self, configuration: configuration)
            infiniteScrollingBehaviourForYears = InfiniteScrollingBehaviour(withCollectionView: yearRow, andData: years, delegate: self, configuration: configuration)
        }
    }
}

//MARK:- UICollectionViewDelegate
extension OADatePicker: UICollectionViewDelegate {
    
    public func didEndScrolling(inInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) {
        selectMiddleRow(collectionView: behaviour.collectionView, data: behaviour.dataSetWithBoundary as! [ModelDate])
        guard  let tag = DateType(rawValue: behaviour.collectionView.tag) else{return}
        switch tag {
        case .Day:
            behaviour.reload(withData: days)
        case .Month:
            behaviour.reload(withData: Months)
            break
        case .Year:
            behaviour.reload(withData: years)
        }
        checkForBoundaryDate()
    }
}

//MARK:- InfiniteScrollingBehaviourDelegate
extension OADatePicker : InfiniteScrollingBehaviourDelegate {
    public func configuredCell(collectionView: UICollectionView, forItemAtIndexPath indexPath: IndexPath, originalIndex: Int, andData data: InfiniteScollingData, forInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) -> UICollectionViewCell {
        let cell = behaviour.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OADatePickerCell
        //        cell.dateLbl.font = fontForDeselected
        guard  let tag = DateType(rawValue: behaviour.collectionView.tag) else{return cell}
        switch tag {
        case .Day:
            if let day = data as? ModelDate {
                if day.isSelected {
                    cell.setSelected()
                }else {
                    if day.isValid {
                        cell.setSemiDeSelected()
                    }else{
                        cell.setDeSelected()
                    }
                }
                cell.dateLbl.text = day.value
            }
        case .Month:
            if let month = data as? ModelDate {
                cell.dateLbl.text = month.value
                if month.isSelected {
                    cell.setSelected()
                }else {
                    if month.isValid {
                        cell.setSemiDeSelected()
                    }else{
                        cell.setDeSelected()
                    }
                }
            }
            
        case .Year:
            if let year = data as? ModelDate {
                cell.dateLbl.text = year.value
                if year.isSelected {
                    cell.setSelected()
                }else {
                    if year.isValid {
                        cell.setSemiDeSelected()
                    }else{
                        cell.setDeSelected()
                    }
                }
            }
        }
        
        return cell
    }
    
    
    public func didSelectItem(collectionView: UICollectionView,atIndexPath indexPath: IndexPath, originalIndex: Int, andData data: InfiniteScollingData, inInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) {
        if let cell = behaviour.collectionView.cellForItem(at: indexPath) as? OADatePickerCell {
            cell.dateLbl.font = DLSFont.h3.regular
            guard  let tag = DateType(rawValue: behaviour.collectionView.tag) else{return}
            switch tag {
            case .Day:
                for i in 0..<days.count {
                    if i != originalIndex {
                        days[i].isSelected = false
                    }
                }
                days[originalIndex].isSelected = true
                compareDays()
                cell.setSelected()
            case .Month:
                for i in 0..<Months.count {
                    if i != originalIndex {
                        Months[i].isSelected = false
                    }
                }
                Months[originalIndex].isSelected = true
                compareDays()
                cell.setSelected()
                infiniteScrollingBehaviourForMonths.reload(withData: Months)
                
            case .Year:
                for i in 0..<years.count {
                    if i != originalIndex {
                        years[i].isSelected = false
                    }
                }
                years[originalIndex].isSelected = true
                cell.setSelected()
                compareDays()
                infiniteScrollingBehaviourForYears.reload(withData: years)
            }
            behaviour.scroll(toElementAtIndex: originalIndex)
            delay(0.5) { [weak self] in
                self?.checkForBoundaryDate()
            }
        }
    }
}

//MARK:- public methods
extension OADatePicker {
    static func getDOBPicker() -> OADatePicker {
        
        let datePicker = OADatePicker()
        
        var component = DateComponents()
        
        component.year = -100
        datePicker.minimumDate = gregorianCalendar.date(byAdding: component, to: Date.currentDate())
        
        component.year = -18
        datePicker.maximumDate = gregorianCalendar.date(byAdding: component, to: Date.currentDate())
        
        datePicker.date = datePicker.maximumDate ?? Date.currentDate()
        
        return datePicker
    }
    public func yearRange(inBetween start: Int, end: Int) {
        let years = ModelDate.getYears(startYear: start, endYear: end)
        self.years = years
        infiniteScrollingBehaviourForYears.reload(withData: years)
    }
}

//MARK:- Utils
extension OADatePicker {
    
    /// Will reset to boundary date (min or max which one is close) if chosen date is out of boundary
    fileprivate func checkForBoundaryDate(){
        let date = CalendarHelper.getThatDate(days, Months, years)
         let maxDate = maximumDate ?? Date()
        if date > maxDate {
            selectDate(date: maxDate)
        }
        if let minDate = minimumDate,  date < minDate {
            selectDate(date: minDate)
        }
    }
    
    /// Selected date initially
    fileprivate func selectDate(date: Date){
        let (month,day,year) = date.dateTupleStr //ymd
        let yearIndexForSelection = years.firstIndex { (dateComponent) -> Bool in
            return Int(dateComponent.value) == Int(year)
        }
        
        let dateIndexForSelection = days.firstIndex { (dateComponent) -> Bool in
            return Int(dateComponent.value) == Int(day)
        }
        
        let m = Int(month)! - 1 //monthIndexForSelection
        
        for year in years {year.isSelected = false}
        if let y = yearIndexForSelection {
            years[y].isSelected = true
            collectionState(infiniteScrollingBehaviourForYears, y)
            
        }
        
        for month in Months {month.isSelected = false}
        Months[m].isSelected = true
        collectionState(infiniteScrollingBehaviourForMonths, m)
        
        for day in days {day.isSelected = false}
        if let d = dateIndexForSelection {
            days[d].isSelected = true
            collectionState(infiniteScrollingBehaviourForDays, d)
        }
    }
    
    private func collectionState(_ collectionView: InfiniteScrollingBehaviour, _ index: Int) {
        let indexPathForFirstRow = IndexPath(row: index, section: 0)
        guard  let tag = DateType(rawValue: collectionView.collectionView.tag) else{return}
        switch tag {
        case .Day:
            self.days[indexPathForFirstRow.row].isSelected = true
            infiniteScrollingBehaviourForDays.reload(withData: days)
            break
        case .Month:
            self.Months[indexPathForFirstRow.row].isSelected = true
            infiniteScrollingBehaviourForMonths.reload(withData: Months)
            break
        case .Year:
            self.years[indexPathForFirstRow.row].isSelected = true
            infiniteScrollingBehaviourForYears.reload(withData: years)
            break
            
        }
        collectionView.scroll(toElementAtIndex: index)
    }
    
    /// Will get the middle visible row and make it highlighted.
    fileprivate func selectMiddleRow(collectionView: UICollectionView, data: [ModelDate]){
        let Row = calculateMedian(array: collectionView.indexPathsForVisibleItems) + (collectionView.didScrollUp ? 1 : 0)
       // let Row = calculateMedian(array: collectionView.indexPathsForVisibleItems)
        let selectedIndexPath = IndexPath(row: Int(Row), section: 0)
        for i in 0..<data.count {
            if i != selectedIndexPath.row {
                data[i].isSelected = false
            }
        }
        if let cell = collectionView.cellForItem(at: selectedIndexPath) as? OADatePickerCell {
            cell.setSelected()
            data[Int(Row)].isSelected = true
            if collectionView.tag != DateType.Day.rawValue{ //0
                compareDays()
            }
        }
        collectionView.scrollToItem(at: selectedIndexPath, at: .centeredVertically, animated: true)
    }
    
    private func calculateMedian(array: [IndexPath]) -> Float {
        let sorted = array.sorted()
        guard sorted.count > 0 else {return 0}
        if sorted.count % 2 == 0 {
            return Float((sorted[(sorted.count / 2)].row + sorted[(sorted.count / 2) - 1].row)) / 2
        } else {
            return Float(sorted[(sorted.count - 1) / 2].row)
        }
    }
    
    /// Will handle the cases when change in datasource of one collection will update other's collection view datasource. For example: Changing the month from Jan to Feb will change number of datys.
    fileprivate func compareDays(){
        guard let selectedDay = days.filter({ (modelObject) -> Bool in
            return modelObject.isSelected
        }).first else{ return}
        
            days = ModelDate.getDays(years, Months)
            days.selectDay(selectedDay: selectedDay)
        infiniteScrollingBehaviourForDays.reload(withData: days)

        // if previously selected days is not there in datasource (e.g. 31st Jan to Feb), select the max available day
        if let selectedDayInt = Int(selectedDay.value), selectedDayInt > days.count {
            if let lastDay = days.last{
                lastDay.isSelected = selectedDay.isSelected
                    infiniteScrollingBehaviourForDays.scroll(toElementAtIndex: days.count-1)
            }
        }
    }
    
    fileprivate func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}
