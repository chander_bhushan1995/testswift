//
//  Date.swift
//  Animation_Demo
//
//  Created by Pankaj Verma on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//


import Foundation

class OADate {

    var month: String
    var day: String
    var year:String
    
    init(month: String, day: String, year:String) {
        self.month = month
        self.day = day
        self.year = year
    }
    
    class func getMonths() -> [String]{
        return ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
    class func getYears(startYear: Int, endYear: Int) -> [String] {
        return CalendarHelper.fetchYears(startYear: startYear, endYear: endYear)
    }
    
}

public enum SelectionType {
    case square
    case roundedsquare
    case circle
}


