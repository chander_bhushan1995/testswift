//
//  ModelDate.swift
//  Animation_Demo
//
//  Created by Pankaj Verma on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation

class ModelDate {

    
    var value:String
    var isSelected:Bool
    var isValid:Bool = false
    init(value:String, isSelected:Bool = false) {
        self.value = value
        self.isSelected = isSelected
    }
}

extension ModelDate: InfiniteScollingData {}

extension ModelDate {
    
    class func getMonths() -> [ModelDate]{
        let months = OADate.getMonths()
        
        var arrMonths:[ModelDate] = []
        for index in 0..<months.count {
            let x = ModelDate(value: months[index], isSelected: false)
            arrMonths.append(x)
        }
        return arrMonths
    }
    
//    class func getYears(startYear: Int = Date().dateTupleInt.2 - 99, endYear: Int = Date().dateTupleInt.2 + 99) -> [ModelDate] {
    class func getYears(startYear: Int = 1_900, endYear: Int = 2_500) -> [ModelDate] {
       let years = OADate.getYears(startYear: startYear, endYear: endYear)
        
        var arrYears:[ModelDate] = []
        for index in 0..<years.count {
            arrYears.append(ModelDate(value: years[index], isSelected: false))
        }
        return arrYears
    }
    
    class func getDays(_ years:[ModelDate] , _ months: [ModelDate]) -> [ModelDate] {
        let days = CalendarHelper.fetchDays(years, months)
        return ModelDate.dayUpTo(lastDayInt: days)
    }
}

extension ModelDate {
    class func dayUpTo(lastDayInt: Int) -> [ModelDate] {
        return (1...lastDayInt).map { (dayInt) -> ModelDate in
            return ModelDate(value: "\(dayInt)")
        }
    }
}

extension ModelDate : Equatable {
    static func ==(lhs: ModelDate, rhs: ModelDate) -> Bool {
        return lhs.value == rhs.value
    }
}

extension Date{
    func toSecond() -> Int64! {
        return Int64(self.timeIntervalSince1970)
    }
    var dateTupleStr : (String,String,String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.calendar = gregorianCalendar
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let ArrDate = dateFormatter.string(from: self).components(separatedBy: "/")
        return (ArrDate[0],ArrDate[1],ArrDate[2])
    }
    
    var dateTupleInt : (Int,Int,Int){ //(dd, mm, yyyy)
        return (gregorianCalendar.component(.day, from:self), gregorianCalendar.component(.month, from:self), gregorianCalendar.component(.year, from:self))
    }
    var timeTuple : (Int,Int){
        return (gregorianCalendar.component(.hour, from:self), gregorianCalendar.component(.minute, from:self))
    }
    var monthStr: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        dateFormatter.calendar = gregorianCalendar
        return dateFormatter.string(from: self)
    }
}

extension ModelDate {
    class func getHours() -> [ModelDate]{
        let hours = OATime.getHours()
        
        var arrHours:[ModelDate] = []
        for index in 0..<hours.count {
            let x = ModelDate(value: hours[index], isSelected: false)
            arrHours.append(x)
        }
        return arrHours
    }
    
    class func getMinutes() -> [ModelDate]{
        let minutes = OATime.getMinutes()
        
        var arrMinutes:[ModelDate] = []
        for index in 0..<minutes.count {
            let x = ModelDate(value: minutes[index], isSelected: false)
            arrMinutes.append(x)
        }
        return arrMinutes
    }
    
    class func getAMPM() -> [ModelDate]{
        let ampms = OATime.getAmPm()
        
        var arrAMPM:[ModelDate] = []
        for index in 0..<ampms.count {
            let x = ModelDate(value: ampms[index], isSelected: false)
            arrAMPM.append(x)
        }
        return arrAMPM
    }
}

extension ModelDate {
    /// Date in range limit will be darker than date out of range
    class func setState(_ days:inout [ModelDate], _ years:inout [ModelDate] , _ months: inout [ModelDate], maxDate:OADate, minDate:OADate){
        
    }
}
