//
//  OATime.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 25/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation
class OATime {
    
    var hour: String
    var minut: String
    var ampm:String
    
    init(hour: String, minut: String, ampm:String) {
        self.hour = hour
        self.minut = minut
        self.ampm = ampm
    }
    
    class func getHours() -> [String]{
        let arrayH = Array(1...12)
        let stringArray = arrayH.map {
            (number: Int) -> String in
            return String(format: "%02d",number)
        }
        return stringArray
    }
    class func getAmPm() -> [String]{
        return ["AM","PM"]
    }
    class func getMinutes() -> [String] {
        let arrayM = Array(0...59)
        let stringArray = arrayM.map {
            (number: Int) -> String in
            return String(format: "%02d",number)
        }
        return stringArray
    }
    
}
