//
//  OATimePicker.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 25/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol OATimePickerDelegate:class {
    func timeSelected(_ time:String)
}
enum TimeType:Int{
    case Hour
    case Minute
    case ampm
}
class OATimePicker: UIView {
    
    @IBOutlet weak var hourRow: UICollectionView!
    @IBOutlet weak var minuteRow: UICollectionView!
    @IBOutlet weak var ampmRow: UICollectionView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var selectionViewHeight: NSLayoutConstraint!
    public var minimumDate:Date?
    public var maximumDate:Date?
    public var date:Date = Date()
    
    public var baseDate:String?

    var hours = ModelDate.getHours()
    let minutes = ModelDate.getMinutes()
    let ampm = ModelDate.getAMPM()
    var infiniteScrollingBehaviourForHours: InfiniteScrollingBehaviour!
    var infiniteScrollingBehaviourForMinutes: InfiniteScrollingBehaviour!
    var infiniteScrollingBehaviourForAMPMs: InfiniteScrollingBehaviour!
    weak var delegate:OATimePickerDelegate?
    public var selectedBgColor: UIColor = UIColor.veryLightGray2
    public var cellSize:CGFloat = 44
    public var selectionType: SelectionType = .square
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
        registerCell()
        
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
        registerCell()
    }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
    }
    
    private func initConfig(){
        
        //let configuration = CollectionViewConfiguration(layoutType: .fixedSize(sizeValue: cellSize, lineSpacing: 0), scrollingDirection: .vertical)
        let configuration = CollectionViewConfiguration(layoutType: .numberOfCellOnScreen(5), scrollingDirection: .vertical)
        infiniteScrollingBehaviourForHours = InfiniteScrollingBehaviour(withCollectionView: hourRow, andData: hours, delegate: self, configuration: configuration)
        infiniteScrollingBehaviourForMinutes = InfiniteScrollingBehaviour(withCollectionView: minuteRow, andData: minutes, delegate: self, configuration: configuration)
        infiniteScrollingBehaviourForAMPMs = InfiniteScrollingBehaviour(withCollectionView: ampmRow, andData: ampm, delegate: self, configuration: CollectionViewConfiguration(layoutType: .numberOfCellOnScreen(3), scrollingDirection: .vertical))
    }
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("OATimePicker", owner: self, options: nil)
        addSubview(calendarView)
        selectionViewHeight.constant = Utilities.datePickerHeight * 0.16
        calendarView.frame = self.bounds
        initConfig()
        delay(0.1){
            self.selectionView.backgroundColor = self.selectedBgColor
            self.selectionView.selectSelectionType(selectionType: self.selectionType)
            self.initialDate(date: self.date)
            self.calendarView.backgroundColor = UIColor.white
        }
    }
    
    func initialDate(date: Date){
        let (hour,minute) = date.timeTuple
        //        let hourIndexForSelection = hours.index { (dateComponent) -> Bool in
        //            return dateComponent.value == "\(hour)"
        //        }
        //
        //        let minuteIndexForSelection = minutes.index { (dateComponent) -> Bool in
        //            return dateComponent.value == "\(minute)"
        //        }
        //
        let h = ((hour%12 - 1)+12)%12
        let m = minute
        let a = hour/12
        
        for hour in hours {hour.isSelected = false}
        hours[h].isSelected = true
        collectionState(infiniteScrollingBehaviourForHours, h)
        
        for minute in minutes {minute.isSelected = false}
        minutes[m].isSelected = true
        collectionState(infiniteScrollingBehaviourForMinutes, m)
        
        for a in ampm {a.isSelected = false}
        ampm[a].isSelected = true
        collectionState(infiniteScrollingBehaviourForAMPMs, a)
        
    }
    
    fileprivate func checkForDateLimit(){
        let time24 = CalendarHelper.getThatTimeString12(hours, minutes, ampm)
        let date = CalendarHelper.getSelectedTimeWithBaseDate(date: baseDate, time: time24)
        let maxDate = maximumDate ?? Date()
        if date > maxDate {
            initialDate(date: maxDate) ////maxDate Date()
        }
        if let minDate = minimumDate, date < minDate {
            initialDate(date: minDate) //minDate
        }
    }
    
    private func collectionState(_ collectionView: InfiniteScrollingBehaviour, _ index: Int) {
        let indexPathForFirstRow = IndexPath(row: index, section: 0)
        guard  let tag = TimeType(rawValue: collectionView.collectionView.tag) else{return}
        switch tag {
        case .Hour:
            self.hours[indexPathForFirstRow.row].isSelected = true
            infiniteScrollingBehaviourForHours.reload(withData: hours)
            break
        case .Minute:
            self.minutes[indexPathForFirstRow.row].isSelected = true
            infiniteScrollingBehaviourForMinutes.reload(withData: minutes)
            break
        case .ampm:
            self.ampm[indexPathForFirstRow.row].isSelected = true
            infiniteScrollingBehaviourForAMPMs.reload(withData: ampm)
            break
            
        }
        collectionView.scroll(toElementAtIndex: index)
    }
    
    private func registerCell(){
        let bundle = Bundle(for: self.classForCoder)
        let nibName = UINib(nibName: "OADatePickerCell", bundle:bundle)
        hourRow.register(nibName, forCellWithReuseIdentifier: "cell")
        minuteRow.register(nibName, forCellWithReuseIdentifier: "cell")
        ampmRow.register(nibName, forCellWithReuseIdentifier: "cell")
    }
}

extension OATimePicker: UICollectionViewDelegate {
    
    
    public func didEndScrolling(inInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) {
        selectMiddleRow(collectionView: behaviour.collectionView, data: behaviour.dataSetWithBoundary as! [ModelDate])
        guard  let tag = TimeType(rawValue: behaviour.collectionView.tag) else{return}
        switch tag {
        case .Hour:
            behaviour.reload(withData: hours)
        case .Minute:
            behaviour.reload(withData: minutes)
            break
        case .ampm:
            behaviour.reload(withData: ampm)
            
        }
        checkForDateLimit()
    }
    
    func selectMiddleRow(collectionView: UICollectionView, data: [ModelDate]){
        
        let Row = calculateMedian(array: collectionView.indexPathsForVisibleItems) + (collectionView.didScrollUp ? 1 : 0)
        let selectedIndexPath = IndexPath(row: Int(Row), section: 0)
        for i in 0..<data.count {
            if i != selectedIndexPath.row {
                data[i].isSelected = false
            }
        }
        if let cell = collectionView.cellForItem(at: selectedIndexPath) as? OADatePickerCell {
            cell.setSelected()
            data[Int(Row)].isSelected = true
//            if collectionView.tag != 0{
//                compareDays()
//            }
        }
        collectionView.scrollToItem(at: selectedIndexPath, at: .centeredVertically, animated: true)
    }
    @IBAction func getTime(_ sender: UIButton) {
        self.checkForDateLimit()
        let time = CalendarHelper.getThatTimeString12(hours, minutes, ampm)
        self.isHidden = true
        delegate?.timeSelected(time)
    }
}

//extension OATimePicker {
//
//    func compareDays(){
//        let newDays = ModelDate.getDays(years, Months)
//        if let selectedDay = days.filter({ (modelObject) -> Bool in
//            return modelObject.isSelected
//        }).first {
//            newDays.selectDay(selectedDay: selectedDay)
//        }
//        days = newDays
//        // infiniteScrollingBehaviourForDays.collectionView.reloadSections(IndexSet(integer: 0))
//        infiniteScrollingBehaviourForDays.reload(withData: days)
//        if Int(currentDay)! > days.count{
//            let index = days.count - 1
//            days[index].isSelected = true
//            currentDay = "\(days.count)"
//        }
//    }
//
//}

extension OATimePicker : InfiniteScrollingBehaviourDelegate {
    public func configuredCell(collectionView: UICollectionView, forItemAtIndexPath indexPath: IndexPath, originalIndex: Int, andData data: InfiniteScollingData, forInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) -> UICollectionViewCell {
        let cell = behaviour.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OADatePickerCell
        //        cell.dateLbl.font = fontForDeselected
        guard  let tag = TimeType(rawValue: behaviour.collectionView.tag) else{return cell}
        switch tag {
        case .Hour:
            if let hour = data as? ModelDate {
                if hour.isSelected {
                    cell.setSelected()
                }else {
                    cell.setDeSelected()
                }
                cell.dateLbl.text = hour.value
            }
        case .Minute:
            if let minute = data as? ModelDate {
                cell.dateLbl.text = minute.value
                if minute.isSelected {
                    cell.setSelected()
                }else {
                    cell.setDeSelected()
                }
            }
            
        case .ampm:
            if let ampm = data as? ModelDate {
                cell.dateLbl.text = ampm.value
                if ampm.isSelected {
                    cell.setSelected()
                }else {
                    cell.setDeSelected()
                }
            }
        }
        
        return cell
    }
    
    
    public func didSelectItem(collectionView: UICollectionView,atIndexPath indexPath: IndexPath, originalIndex: Int, andData data: InfiniteScollingData, inInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) {
        if let cell = behaviour.collectionView.cellForItem(at: indexPath) as? OADatePickerCell {
            cell.dateLbl.font = DLSFont.h3.regular
            guard  let tag = TimeType(rawValue: behaviour.collectionView.tag) else{return}
            switch tag {
            case .Hour:
                for i in 0..<hours.count {
                    if i != originalIndex {
//                        currentHour = hours[i].value
                        hours[i].isSelected = false
                    }
                }
                hours[originalIndex].isSelected = true
        //        compareDays()
                cell.setSelected()
                infiniteScrollingBehaviourForHours.reload(withData: hours)
            case .Minute:
                for i in 0..<minutes.count {
                    if i != originalIndex {
                        minutes[i].isSelected = false
                    }
                }
                minutes[originalIndex].isSelected = true
//                compareDays()
                cell.setSelected()
                infiniteScrollingBehaviourForMinutes.reload(withData: minutes)
                
            case .ampm:
                for i in 0..<ampm.count {
                    if i != originalIndex {
                        ampm[i].isSelected = false
                    }
                }
                ampm[originalIndex].isSelected = true
                cell.setSelected()
//                compareDays()
                infiniteScrollingBehaviourForAMPMs.reload(withData: ampm)
            }
            behaviour.scroll(toElementAtIndex: originalIndex)
            delay(0.5) { [weak self] in
                self?.checkForDateLimit()
            }
        }
    }
}

extension OATimePicker {
    
    func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    func calculateMedian(array: [IndexPath]) -> Float {
        let sorted = array.sorted()
        guard sorted.count > 0 else {return 0}
        if sorted.count % 2 == 0 {
            return Float((sorted[(sorted.count / 2)].row + sorted[(sorted.count / 2) - 1].row)) / 2
        } else {
            return Float(sorted[(sorted.count - 1) / 2].row)
        }
    }
    
}
