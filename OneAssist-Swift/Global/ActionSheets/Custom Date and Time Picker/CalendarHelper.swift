//
//  CalendarHelper.swift
//  Animation_Demo
//
//  Created by Pankaj Verma on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//


import UIKit

class CalendarHelper{
    
    static func fetchYears(startYear: Int, endYear: Int) -> [String] {
        var years:[String] = []
        let StartDateComponents = DateComponents(year: startYear , month: 1, day: 1)
        
        guard let startDate = gregorianCalendar.date(from: StartDateComponents) else { return [] }
        
        if endYear > startYear {
            let range = endYear - startYear
            for i in 0 ... range {
                guard let yearsBetween = gregorianCalendar.date(byAdding: .year, value: i, to: startDate) else { return [] }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                dateFormatter.calendar = gregorianCalendar
                let year = dateFormatter.string(from: yearsBetween)
                years.append(year)
            }
            return years
        }else {
            fatalError("startYear cannot be greater than EndYear")
        }

    }
    
    static func fetchDays(_ years: [ModelDate], _ months: [ModelDate]) -> Int {
        var numDays = 0
        let components = gregorianCalendar.dateComponents([.month], from: Date())
        var currentMonth = String(components.month ?? 12)
        for (index, month) in months.enumerated() {
            if month.isSelected == true {
                currentMonth = "\(index + 1)"
                break
            }
        }
        if let yearInt = Int(years.currentYear.value), let monthInt = Int(currentMonth){
            let dateComponents = DateComponents(year: yearInt, month: monthInt)
            let date = gregorianCalendar.date(from: dateComponents)!
            
            let range = gregorianCalendar.range(of: .day, in: .month, for: date)!
            numDays = range.count
        }
        return numDays
    }
    
    
    static func getThatDate(_ days: [ModelDate], _ months: [ModelDate], _ years: [ModelDate]) -> Date{
        var currentDay: String = ""
        var currentMonth: String = ""
        var currentYear: String = ""
        for day in days{
            if day.isSelected == true {
                currentDay = day.value
                break
            }
        }
        for month in months {
            if month.isSelected == true {
                currentMonth = month.value
                break
            }
        }
        for year in years {
            if year.isSelected == true {
                currentYear = year.value
                break
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.calendar = gregorianCalendar
        let date = dateFormatter.date(from: "\(currentMonth)/\(currentDay)/\(currentYear)")
        
        return date ?? Date()
    }
    static func getThatTimeString12(_ hours: [ModelDate], _ minutes: [ModelDate], _ ampm: [ModelDate]) -> String{
        var currentHour: String = ""
        var currentMinute: String = ""
        var currentAMPM: String = ""
        for hour in hours{
            if hour.isSelected == true {
                currentHour = hour.value
                break
            }
        }
        for minute in minutes {
            if minute.isSelected == true {
                currentMinute = minute.value
                break
            }
        }
        for a in ampm {
            if a.isSelected == true {
                currentAMPM = a.value
                break
            }
        }
        
        return "\(currentHour):\(currentMinute) \(currentAMPM)"
    }
    static func getThatTimeString24(_ hours: [ModelDate], _ minutes: [ModelDate], _ ampm: [ModelDate]) -> String{
        var currentHour: Int = 0
        var currentMinute:Int = 0
        
        for hour in hours{
            if hour.isSelected == true {
                currentHour = Int(hour.value) ?? 0
                break
            }
        }
        
        for minute in minutes {
            if minute.isSelected == true {
                currentMinute = Int(minute.value) ?? 0
                break
            }
        }
        
       let a =  ampm.filter{
            return $0.isSelected
        }.first
        if let val =  a?.value{
            if val == "AM" && currentHour == 12  {
                currentHour = 0
            }
            if val == "PM" &&  currentHour != 12{
                 currentHour += 12
            }
        }
//        for a in ampm {
//            if a.isSelected == true && a.value == "PM"{
//                if currentHour % 12 == 0 {
//                    currentHour += 11
//                }else{
//                    currentHour += 12
//                }
//            }
//        }

        return "\(currentHour):\(currentMinute):00"
    }
    static func getSelectedTimeWithBaseDate(date:String?, time:String)->Date{
        
        let dateFormatter = DateFormatter.incidentTimeFormat
        dateFormatter.setLocal()
        dateFormatter.calendar = gregorianCalendar
        let dateStr = (date ?? "")+", "+time
        let date = dateFormatter.date(from: dateStr)
        return date ?? Date()
    }
}

extension Array where Element: ModelDate {
    
    var currentMonth: ModelDate {
        if let selectedMonth = self.filter({ (modelObject) -> Bool in
            return modelObject.isSelected
        }).first {
            return selectedMonth
        }
        return ModelDate(value: "12", isSelected: false)
    }
    
    var currentYear: ModelDate {
        if let selectedYear = self.filter({ (modelObj) -> Bool in
            return modelObj.isSelected
        }).first {
            return selectedYear
        }
        return ModelDate(value: "2019", isSelected: false)
    }

    func selectDay(selectedDay: ModelDate){
        for day in self  {
            if day == selectedDay {
                day.isSelected = selectedDay.isSelected
            }
        }
    }
}


