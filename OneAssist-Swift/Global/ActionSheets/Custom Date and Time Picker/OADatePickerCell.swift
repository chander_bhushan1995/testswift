//
//  OADatePickerCell.swift
//  Vertical-date-picker
//
//  Created by Pankaj Verma on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//


import UIKit

class OADatePickerCell: UICollectionViewCell {
    public var fontForDeselected: UIFont = DLSFont.supportingText.regular
    public var fontForSelected: UIFont = DLSFont.h3.regular
    public var fontForSemiDeselected:UIFont = DLSFont.supportingText.regular
    public var deselectedBgColor: UIColor = .clear
    public var selectedTextColor: UIColor = .buttonBlue
    public var deselectedTextColor: UIColor = .disabledAndLines
    public var semiDeselectedTextColor:UIColor = .gray//UIColor.bodyTextGray

    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}

extension OADatePickerCell {

    func setSelected(){
        self.backgroundColor = .clear
        self.dateLbl.textColor = selectedTextColor
        self.dateLbl.font = fontForSelected
        self.alpha = 1
    }
//    func selectSelectionType(selectionType: SelectionType){
//        switch selectionType {
//        case .square:
//            self.layer.cornerRadius = 0.0
//            break
//        case .roundedsquare:
//            self.layer.cornerRadius = 5.0
//            break
//        case .circle:
//            self.layer.cornerRadius = self.frame.size.width / 2
//            break
//        }
//    }
    
    func setDeSelected(){
        self.backgroundColor = deselectedBgColor
        self.dateLbl.textColor = deselectedTextColor
        self.dateLbl.font = fontForDeselected
        self.alpha = 1
    }
    func setSemiDeSelected(){
        self.backgroundColor = deselectedBgColor
        self.dateLbl.textColor = semiDeselectedTextColor
        self.dateLbl.font = fontForSemiDeselected
        self.alpha = 1
    }

}
