//
//  ScrollableMenuView.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 08/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class ScrollableMenuView: UIView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var menuView: UIView!
    @IBOutlet weak var allBenifitView: AllBenefitsView!
    @IBOutlet weak var scrollView: UIScrollView!
    var items:[String]?
    var image:UIImage?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
        
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        allBenifitView.setContent(items: items ?? [], image: image)
        scrollView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0);

    }
    
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("ScrollableMenuView", owner: self, options: nil)
        addSubview(menuView)
        menuView.frame = self.bounds
    }
    
}
