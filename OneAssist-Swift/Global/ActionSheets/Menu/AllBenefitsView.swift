//
//  AllBenefitsView.swift
//  OneAssist-Swift
//
//  Created by Varun on 02/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class AllBenefitsView: UIView {
    var label: UILabel?
    var bodyColor: UIColor = UIColor.charcoalGrey
    
    @discardableResult
    func setContent(items: [String], image: UIImage?, textAlignment: NSTextAlignment = .left)->CGFloat? {
        
        label?.removeFromSuperview()
        label = UILabel()
        label!.numberOfLines = 0
        
        let fullString = NSMutableAttributedString()
        
        let font: UIFont = DLSFont.bodyText.regular
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlignment
        paragraphStyle.paragraphSpacing = 0.25 * font.lineHeight
        paragraphStyle.headIndent = (image?.size.width ?? -8) + 8
        
        for item in items.enumerated() {
            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = image
            
            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)
            
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)
            fullString.append(NSAttributedString(string: "  "))
            fullString.append(NSAttributedString(string: item.element))
            
            if item.offset != items.count - 1 {
                fullString.append(NSAttributedString(string: "\n\n"))
            }
        }
        
        // draw the result in a label
        fullString.addAttributes([.font: font, .paragraphStyle: paragraphStyle, .baselineOffset: NSNumber(value: 0), .foregroundColor: bodyColor], range: NSRange(location: 0, length: fullString.length))
        label!.attributedText = fullString
        label!.textAlignment = textAlignment
        
        if textAlignment == .left {
            label!.sizeToFit()
        }
        
        addSubview(label!)
        label!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label!.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            label!.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 8),
            label!.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            label!.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8),
            ])
        
        
        var count = 0
        let charCount:Int = Int(UIScreen.main.bounds.width/10)
        for item in items {
           var c = item.count/(charCount) //asuming 40 character in 1 line
            if c == 0 { c = 1 }
             count +=  c
        }
        for constraint in constraints {
            if constraint.firstAttribute == .height {
                constraint.constant = count.cgFloat * 36  //label!.frame.height
            }
            if constraint.firstAttribute == .width {
                constraint.constant = label!.frame.width
            }
        }
        return label?.frame.height
    }
}
