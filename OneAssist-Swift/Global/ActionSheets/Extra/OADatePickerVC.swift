//
//  OADatePickerVC.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 08/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol DatePickerDelegate: class {
     func dateSelected(_ date:Date)
}
class OADatePickerVC: UIViewController {
    
    
    @IBOutlet weak var backdropView:UIView!
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    @IBOutlet weak var topPadding: NSLayoutConstraint!
    @IBOutlet weak var datePicker: OADatePicker!
    weak var delegate: DatePickerDelegate?
    
    var amounts: [MasterInfoDTOlist] = []
    var contentHeight:CGFloat = 0
    var isPresenting = false
    let contentBottomInset:CGFloat = 20.0
    let contentTopInset:CGFloat = 20.0
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //        view.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        datePicker.delegate = self
        
        datePicker.yearRange(inBetween: 1990, end: 2022)

        //  scrollView.contentInset = UIEdgeInsetsMake(contentTopInset, 0, contentBottomInset, 0.0);
        updateTopPadding(contentHeight: contentHeight)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PopoverMenuVC.handleTap(_:)))
        backdropView.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
   @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
}

extension OADatePickerVC: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            
            datePicker.frame.origin.y += menuHeight.constant
            backdropView.alpha = 0
            
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.datePicker.frame.origin.y -= self.menuHeight.constant
                self.backdropView.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.datePicker.frame.origin.y += self.menuHeight.constant
                self.backdropView.alpha = 0
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}
extension OADatePickerVC{
    func updateTopPadding(contentHeight:CGFloat?){
        let maxContentHeight = self.view.bounds.height * 0.75
        let extraVerticalPadding = contentBottomInset+contentTopInset+32.0
        if let contentHeight = contentHeight, (contentHeight+extraVerticalPadding) < maxContentHeight {
            topPadding.constant = self.view.bounds.height - (contentHeight+extraVerticalPadding)
            return
        }
        topPadding.constant = self.view.bounds.height - maxContentHeight
    }
}

extension OADatePickerVC: OADatePickerDelegate{
    func dateSelected(_ date:Date){
        dismiss(animated: true, completion: nil)
        self.delegate?.dateSelected(date)
        print("selected date: \(date)")
    }
}
