//
//  SelectMenuVC.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 08/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
protocol ItemSelectable :class{
    func didSelectRow(row:Int)
}
class SelectMenuVC: BaseVC {

    @IBOutlet weak var backdropView:UIView!
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    @IBOutlet weak var topPadding: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    weak var delegate: ItemSelectable?

    var amounts: [MasterInfoDTOlist] = []
    var height:CGFloat = 70
    var isPresenting = false
    let contentBottomInset:CGFloat = 20.0
    let contentTopInset:CGFloat = 20.0
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //        view.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "PickerCell", bundle: nil), forCellReuseIdentifier: "PickerCell")

      //  scrollView.contentInset = UIEdgeInsetsMake(contentTopInset, 0, contentBottomInset, 0.0);
            let contentH:CGFloat = CGFloat(amounts.count * 72)
            updateTopPadding(contentHeight: contentH)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PopoverMenuVC.handleTap(_:)))
        backdropView.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
}

extension SelectMenuVC: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            
            tableView.frame.origin.y += menuHeight.constant
            backdropView.alpha = 0
            
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.tableView.frame.origin.y -= self.menuHeight.constant
                self.backdropView.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.tableView.frame.origin.y += self.menuHeight.constant
                self.backdropView.alpha = 0
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}
extension SelectMenuVC{
    func updateTopPadding(contentHeight:CGFloat?){
        let maxContentHeight = self.view.bounds.height * 0.75
        let extraVerticalPadding = contentBottomInset+contentTopInset+32.0
        if let contentHeight = contentHeight, (contentHeight+extraVerticalPadding) < maxContentHeight {
            topPadding.constant = self.view.bounds.height - (contentHeight+extraVerticalPadding)
            return
        }
        topPadding.constant = self.view.bounds.height - maxContentHeight
    }
}

extension SelectMenuVC:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath) as! PickerCell
        cell.textLabel?.text = amounts[indexPath.row].paramName
        cell.textLabel?.font = DLSFont.h3.regular
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate?.didSelectRow(row:indexPath.row )
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

