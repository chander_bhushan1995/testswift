//
//  SelectMenuView.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 09/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
//protocol ContentHeightDataSource: class {
//    func heightForContent(height:CGFloat)
//}
protocol ContentDelegate: class {
    func didSelectRow(row:Int)
}
class SelectMenuView: UIView , UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var menuView: UIView!
    
   // var amounts: [MasterInfoDTOlist] = []
    var list: [String] = []
//    weak var dataSource:ContentHeightDataSource?
    weak var delegate:ContentDelegate?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "PickerCell", bundle: nil), forCellReuseIdentifier: "PickerCell")
    }
    
    private func loadinit(){
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("SelectMenuView", owner: self, options: nil)
        addSubview(menuView)
        menuView.frame = self.bounds
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath) as! PickerCell
        cell.textLabel?.text = list[indexPath.row]
        cell.textLabel?.font = DLSFont.h3.regular
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectRow(row:indexPath.row )
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
