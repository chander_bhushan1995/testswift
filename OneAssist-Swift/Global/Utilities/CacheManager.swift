//
//  CacheManager.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

enum PurchaseCategory: String {
    case none = ""
    case home = "Home"
    case wallet = "Wallet"
    case mobile = "Mobile"
    case more = "More"
    case claim = "Claim"
}

enum PurchaseFlowFrom {
    case none
    case suggestAPlan
    case activateVoucher
    case upgrade
    case renew
}

enum ImageCaptureType {
    case RetakeFrontPanel
    case RetakeBackPanel
    case ReuploadFrontPanel
    case ReuploadBackPanel
    case ReuploadBoth
    case CaptureBoth
}

class CacheManager {
    
    private(set) static var shared = CacheManager()
    private init() {}
    
    var serviceCenterListMarkers =  [ServiceCenterClusterMarkerModel]()
    var currentLocation:CLLocationCoordinate2D?
    var pincodeSelectedForServiceCenter:String?
    var citySelectedForServiceCenter:String?
    var stateSelectedForServiceCenter:String?
    var productCategories: [SubCategoryList]?
    var purchaseFlowFor = PurchaseCategory.none
    var purchaseFlowFrom = PurchaseFlowFrom.none
    var eventProductName: String?
    var srType: Constants.Services?
    var userName: String?
    var PurchaseType:String?
    var mobileNo: String?
    var hasMobileNo: Bool { return mobileNo != nil }
    var selectedTabIndex: TabIndex? = nil
    var memResponse: CustomerMembershipDetails?
    var forceFetchCustomerDetail = true
    var isCameFromDeeplinkForSOD = false
    var eventLocationForSOD: String = ""
    var idFenceTrailSIPlanCode: String?
    var trailPlanPaymentOptions: [String] = []
    
    static func clearCache() {
        /// product categories are initialize at app launch, do not clear on logout
        let categories = self.shared.productCategories
        shared = CacheManager()
        shared.productCategories = categories
    }
}

class FDCacheManager {
    private(set) static var shared = FDCacheManager()
    
    private init() {}
    
    var fdTestType: FDTestType = FDTestType.SDT
    var selectLanguageForMirrorTest: LanguageData?
    var imageCaptureType: ImageCaptureType = .CaptureBoth
    
    var isShowFullForm: Bool = false   //Full farm include Amount/date or both field in post detail complte page
    var maxInsuranceValue: NSNumber?
    var minInsuranceValue: NSNumber?
    var transactionDate: String?
    var minDeviceDate: String?
    var purchaseAmount: String?
    var purchaseDate: String?
    var activityRefId: String?

    var tempCustInfoModel: TemporaryCustomerInfoModel?  = nil {
        didSet {
            if let infoModel = tempCustInfoModel {
               fdTestType = infoModel.getFDTestType()
                
                if let customerDetails = infoModel.customerDetails, let customer = customerDetails.first {
                    if (customer.devicePurchaseDate == nil) || (customer.devicePurchaseAmount == nil) {
                        FDCacheManager.shared.isShowFullForm = true
                        
                    }
                    
                    if let activityRefId = customer.activityRefId {
                        self.activityRefId = activityRefId
                    }
                    
                    if let purchaseAmount = customer.devicePurchaseAmount {
                        FDCacheManager.shared.purchaseAmount = purchaseAmount
                    }
                    
                    if let purchaseDate = customer.devicePurchaseDate,
                       let date = DateFormatter.dayMonthAndYear.date(from: purchaseDate) {
                        FDCacheManager.shared.purchaseDate = String(date.millisecondsSince1970)
                    }
                    
                    if let paymentDate = customer.paymentDate,
                       let date = DateFormatter.dayMonthAndYear.date(from: paymentDate) {
                        FDCacheManager.shared.transactionDate = String(date.millisecondsSince1970)
                    }
                    
                    if let minInsuranceValue = customer.minInsuranceValue {
                        FDCacheManager.shared.minInsuranceValue = minInsuranceValue
                    }
                    
                    if let maxInsuranceValue = customer.maxInsuranceValue {
                        FDCacheManager.shared.maxInsuranceValue = maxInsuranceValue
                    }
                    
                    if let minDeviceDate = customer.minDeviceDate,
                       let date = DateFormatter.dayMonthAndYear.date(from: minDeviceDate) {
                        FDCacheManager.shared.minDeviceDate = String(date.millisecondsSince1970)
                    }
                }
            }
        }
    }
    var getDocsToUploadModel: GetDocsToUploadResponseDTO? = nil {
        didSet {
            let verificationDocs = FDCacheManager.shared.getDocsToUploadModel?.deviceDetails?.first?.verificationDocuments
            
            if Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs),
                Utilities.isReuploadCase(of: Constants.RequestKeys.backPanel, with: verificationDocs){
                self.imageCaptureType = .ReuploadBoth
                
            }else if Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs),
                !Utilities.isReuploadCase(of: Constants.RequestKeys.backPanel, with: verificationDocs){
                self.imageCaptureType = .ReuploadFrontPanel
                
            }else if !Utilities.isReuploadCase(of: Constants.RequestKeys.frontPanel, with: verificationDocs),
                Utilities.isReuploadCase(of: Constants.RequestKeys.backPanel, with: verificationDocs){
                self.imageCaptureType = .ReuploadBackPanel
                
            }
        }
    }
   
    func setupGlanceValue(_ plan: PlanDetailAndSRModel) {
        self.isShowFullForm = plan.glance
        self.maxInsuranceValue = plan.maxInsuranceValue
        self.minInsuranceValue  = plan.minInsuranceValue
        self.transactionDate = plan.transactionDate
        self.minDeviceDate = plan.minPurchaseDateForGlance
    }
    
    func deleteFrontPanelImageInDir() {
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
        let fpname = orderId! + "_fp.png"
        DataDiskStogare.deletefile(fileName: fpname)
        
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            SaveFDDataMode.savedata(saveObject.imei, saveObject.imei2, orderId, saveObject.smsNumber, nil, saveObject.mtBackPanelName, saveObject.mtFrontPanelRetryCount, nil)
        }
    }
    
    func deleteBackPanelImageInDir() {
        let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
        let bpname = orderId! + "_bp.png"
        DataDiskStogare.deletefile(fileName: bpname)
        
        if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            SaveFDDataMode.savedata(saveObject.imei, saveObject.imei2, orderId, saveObject.smsNumber, saveObject.mtFrontPanelName, nil, saveObject.mtFrontPanelRetryCount)
        }
    }
    
     func deleteImagesIntoDirectory() {
           
           let orderId = FDCacheManager.shared.tempCustInfoModel?.customerDetails?.first?.orderId?.stringValue
           let fpname = orderId! + "_fp.png"
           let bpname = orderId! + "_bp.png"
           DataDiskStogare.deletefile(fileName: fpname)
           DataDiskStogare.deletefile(fileName: bpname)
           
           if let saveObject = SaveFDDataMode.getSaveData(orderId) {
            SaveFDDataMode.savedata(saveObject.imei, saveObject.imei2, orderId, saveObject.smsNumber, nil, nil, saveObject.mtFrontPanelRetryCount, nil)
           }
       }
    
    static func clearCache() {
        shared = FDCacheManager()
    }
    
    func changeTestType(to test: FDTestType) {
        self.fdTestType = test
        self.tempCustInfoModel?.setFDTestType(test)
    }
}
