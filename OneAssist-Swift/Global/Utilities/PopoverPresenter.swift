//
//  PopoverPresenter.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol PopoverPresented {
    var containerSize: CGSize {get}
}

class PopoverTransitionHandler: NSObject, UIPopoverPresentationControllerDelegate {
	
	private override init() {}
	static let shared = PopoverTransitionHandler()
    
    var isAlertType: Bool = false
    
	func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
		return .none
	}
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return !isAlertType
    }
}

extension UIViewController {
    func presentOverFullScreen(_ viewController: UIViewController,
                               animated: Bool,
                               completion: (() -> Void)? = nil) {
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: animated, completion: completion)
    }
    
    func presentInFullScreen(_ viewController: UIViewController,
                             animated: Bool,
                             completion: (() -> Void)? = nil) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: animated, completion: completion)
    }
    
    func presentOverCurrentContext(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)? = nil){
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: animated, completion: completion)
    }
    
    func present<T: UIViewController> (_ viewController: T, from view: UIView? = nil, isAlertType: Bool = false) where T: PopoverPresented {
        
        viewController.modalPresentationStyle = .popover
        
        if let view = view {
            viewController.popoverPresentationController?.sourceRect = view.bounds
            viewController.popoverPresentationController?.sourceView = view
            viewController.popoverPresentationController?.permittedArrowDirections = [] //[.up, .down]
        } else {
            
            viewController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            viewController.popoverPresentationController?.sourceView = self.view
            viewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        
        viewController.popoverPresentationController?.backgroundColor = .white
        viewController.popoverPresentationController?.delegate = PopoverTransitionHandler.shared
        PopoverTransitionHandler.shared.isAlertType = isAlertType
        viewController.preferredContentSize = viewController.containerSize
        
        
        
        self.present(viewController, animated: true) {
            viewController.view.superview?.superview?.superview?.backgroundColor = UIColor(white: 0, alpha: 0.3)
        }
    }
    
    func addOABottomView(_ view: UIView, isForRating: Bool = false) {
        view.removeFromSuperview()
        if let superView = self.view.viewWithTag(1980) {
            superView.removeFromSuperview()
        }
        var superView = self.view ?? UIView()
        if isForRating {
            superView = UIView()
            superView.tag = 1980
            superView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(superView)
            let leading = NSLayoutConstraint(item: superView, attribute: .leading, relatedBy: .equal, toItem: superView.superview, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: superView, attribute: .trailing, relatedBy: .equal, toItem: superView.superview, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: superView, attribute: .bottom, relatedBy: .equal, toItem: superView.superview, attribute: .bottom, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: superView, attribute: .top, relatedBy: .equal, toItem: superView.superview, attribute: .top, multiplier: 1, constant: 64)
            superView.backgroundColor = .clear
            self.view.addConstraints([leading,trailing,bottom,top])
        }
        
        superView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        let viewLeading = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: view.superview, attribute: .leading, multiplier: 1, constant: 0)
        let viewTrailing = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: view.superview, attribute: .trailing, multiplier: 1, constant: 0)
        let viewBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: view.superview, attribute: .bottom, multiplier: 1, constant: -200)
        view.sizeToFit()
        superView.addConstraints([viewLeading,viewTrailing,viewBottom])
        UIView.animate(withDuration: 0.3) {
            viewBottom.isActive = false
            superView.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: view.superview, attribute: .bottom, multiplier: 1, constant: 19))
            view.layoutIfNeeded()
        }
    }
    
    func removeOABottomView(_ subView: UIView,_ superView: UIView) {
        if let superView = superView.viewWithTag(1980) {
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                let frame = CGRect(x: superView.frame.origin.x, y: UIScreen.main.bounds.size.height, width: superView.frame.size.width, height: superView.frame.size.height)
                superView.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
                if finished {
                    superView.removeFromSuperview()
                }
            })
            return
        }
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            let frame = CGRect(x: subView.frame.origin.x, y: UIScreen.main.bounds.size.height, width: subView.frame.size.width, height: subView.frame.size.height)
            subView.frame = frame
        }, completion: {(_ finished: Bool) -> Void in
            if finished {
                subView.removeFromSuperview()
            }
        })
    }
}
