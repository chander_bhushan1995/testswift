//
//  ExcelCollectionViewLayout.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 19/08/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

public class ExcelCollectionViewLayout: UICollectionViewLayout {
    
    public weak var delegate: UICollectionViewDelegateFlowLayout?
    private var itemAttributes = [[UICollectionViewLayoutAttributes]]()
    private var itemsSize = [[CGSize]]()
    private var contentSize: CGSize = .zero
    
    private var numberOfColumns: Int {
        guard collectionView != nil else { fatalError("collectionView must not be nil. See the README for more details.") }
        return collectionView!.numberOfItems(inSection: 0)
    }
    private var numberOfRows: Int {
        guard collectionView != nil else { fatalError("collectionView must not be nil. See the README for more details.") }
        return collectionView!.numberOfSections
    }
    
    override public func prepare() {
        guard let collectionView = collectionView else { fatalError("collectionView must not be nil. See the README for more details.") }
        guard collectionView.numberOfSections != 0 else { return }
        
        if itemAttributes.count != collectionView.numberOfSections {
            generateItemAttributes(collectionView: collectionView)
            return
        }
        
        for section in 0..<collectionView.numberOfSections {
            for item in 0..<collectionView.numberOfItems(inSection: section) {
                if section != 0 && item != 0 {
                    continue
                }
                
                let attributes = layoutAttributesForItem(at: IndexPath(item: item, section: section))!
                if section == 0 {
                    var frame = attributes.frame
                    frame.origin.y = collectionView.contentOffset.y
                    attributes.frame = frame
                }
                
                if item == 0 {
                    var frame = attributes.frame
                    frame.origin.x = collectionView.contentOffset.x
                    attributes.frame = frame
                }
            }
        }
    }
    
    override public var collectionViewContentSize: CGSize {
        return contentSize
    }
    
    override public func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return itemAttributes[indexPath.section][indexPath.row]
    }
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributes = [UICollectionViewLayoutAttributes]()
        for section in itemAttributes {
            let filteredArray = section.filter { obj -> Bool in
                return rect.intersects(obj.frame)
            }
            
            attributes.append(contentsOf: filteredArray)
        }
        
        return attributes
    }
    
    override public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    // MARK: - Helpers
    
    private func generateItemAttributes(collectionView: UICollectionView) {
        if itemsSize.count != numberOfColumns || itemsSize.first?.count != numberOfRows {
            calculateItemSizes()
        }
        
        var column = 0
        var xOffset: CGFloat = 0
        var yOffset: CGFloat = 0
        var contentWidth: CGFloat = 0
        
        itemAttributes = []
        
        for section in 0..<numberOfRows {
            var sectionAttributes: [UICollectionViewLayoutAttributes] = []
            for index in 0..<numberOfColumns {
                let itemSize = itemsSize[section][index]
                let indexPath = IndexPath(item: index, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
                
                // All the items of first row except first item of first row has the highest zIndex means that they will be displayed on the top
                // The first element of the first row has lower zIndex means that all the other element of first row can come on top and rest will come on bottom.
                // All the items of first colum except first item of first column has the lower zIndex means that they will be displayed on the top and will come only below first row items.
                if section == 0 && index == 0 {
                    attributes.zIndex = 1023
                } else if section == 0 {
                    attributes.zIndex = 1024
                } else if index == 0 {
                    attributes.zIndex = 1022
                }
                
                if section == 0 {
                    var frame = attributes.frame
                    frame.origin.y = collectionView.contentOffset.y
                    attributes.frame = frame
                }
                if index == 0 {
                    var frame = attributes.frame
                    frame.origin.x = collectionView.contentOffset.x
                    attributes.frame = frame
                }
                
                sectionAttributes.append(attributes)
                
                xOffset += itemSize.width
                column += 1
                
                if column == numberOfColumns {
                    if xOffset > contentWidth {
                        contentWidth = xOffset
                    }
                    
                    column = 0
                    xOffset = 0
                    yOffset += itemSize.height
                }
            }
            
            itemAttributes.append(sectionAttributes)
        }
        
        if let attributes = itemAttributes.last?.last {
            contentSize = CGSize(width: contentWidth, height: attributes.frame.maxY)
        }
    }
    
    private func calculateItemSizes() {
        // This method is used to calculate and store the size of all the cell.
        // All the cells in a row must have same height
        // All the cells in a column must have same width
        guard let collectionView = collectionView else { fatalError("collectionView must not be nil. See the README for more details.") }
        guard let delegate = delegate else { fatalError("delegate must be set in order to calculate items' size. See the README for more details.") }
        var cellSizes = [[CGSize]]()
        for section in 0..<numberOfRows {
            var rowItemsSize: [CGSize] = []
            var maxWidth: CGFloat = 0.0
            var maxHeight: CGFloat = 0.0
            for index in 0..<numberOfColumns {
                let size = delegate.collectionView!(collectionView, layout: self, sizeForItemAt: IndexPath(item: index, section: section))
                maxWidth = CGFloat.maximum(maxWidth, size.width)
                maxHeight = CGFloat.maximum(maxHeight, size.height)
            }
            let maxSize = CGSize(width: maxWidth, height: maxHeight)
            rowItemsSize = [CGSize](repeating: maxSize, count: numberOfColumns)
            cellSizes.append(rowItemsSize)
        }
        itemsSize = cellSizes
    }
}
