//
//  DataDiskStogare.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 18/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

let questionFilename = "questionData"
let offerbankFilename = "offerbank"

class DataDiskStogare: DataProvider {
    class func saveOfferbanksData(data: Data?){
        if let dataValue = data {
            do {
                let dictionary = try JSONSerialization.jsonObject(with: dataValue, options: .allowFragments)
                let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    let filename = getDocumentsDirectory().appendingPathComponent(offerbankFilename)
                    print(filename)
                    try? jsonString.write(to: filename, atomically: true, encoding: .utf8)
                }
            }catch {
                
            }
        }
    }
    
    class func getOfferbanksData()->BankCardResponseDTO? {
        let filename = getDocumentsDirectory().appendingPathComponent(offerbankFilename)
        
        if let encodedObjectJsonString = try? String(contentsOf: filename, encoding: .utf8), let jsonData = encodedObjectJsonString.data(using: .utf8) {
            do {
                let response: BankCardResponseDTO? = try JSONParserSwift.parse(data: jsonData)
                return response
                
            }catch {
                return nil
            }
        }
        return nil
    }
    
    
    class func deleteOfferbanksData() {
        do {
            let fileManager = FileManager.default
            var filePath = try  fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            filePath.appendPathComponent(offerbankFilename)
            
            if fileManager.fileExists(atPath: filePath.path) {
                try fileManager.removeItem(at: filePath)
            }
            
        }catch {
            print("file not deleted")
        }
    }
    
    class func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    class func saveDatawithFile(data: Data?, fileName: String) {
        
        if let dataValue = data {
            DataDiskStogare.deletefile(fileName: fileName)
            do {
                let dictionary = try JSONSerialization.jsonObject(with: dataValue, options: .allowFragments)
                let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    let filename = getDocumentsDirectory().appendingPathComponent(fileName)
                    print(filename)
                    try? jsonString.write(to: filename, atomically: true, encoding: .utf8)
                }
            }catch {
                
            }
        }
    }
    
    class func saveImageintoDirectory(image: UIImage?, fileName: String, completionblock: (Bool, String?)-> Void) {
        if let image = image {
            let fileURL = getDocumentsDirectory().appendingPathComponent(fileName)
            // if image already saved no need to save same image again
            if image.jpegData(compressionQuality: 1.0) != getImageFromDocumentDirectory(filename: fileName)?.jpegData(compressionQuality: 1.0) {
                DataDiskStogare.deletefile(fileName: fileName)
                do {
                    if let data = image.jpegData(compressionQuality:  1.0),
                       !FileManager.default.fileExists(atPath: fileURL.path) {
                        try data.write(to: fileURL)
                        completionblock(true, fileURL.absoluteString)
                    } else {
                        completionblock(false, nil)
                    }
                }catch {
                    print("error saving file:", error)
                    completionblock(false, nil)
                }
            } else {
                completionblock(true, fileURL.absoluteString)
            }
        }
    }

    class func getImageFromDocumentDirectory(filename: String)-> UIImage? {
       let imageURL = getDocumentsDirectory().appendingPathComponent(filename)
       let image    = UIImage(contentsOfFile: imageURL.path)
       return image
    }
    
    class func getDataFromDocumentDirectory(filename: String)-> Data? {
        let imageURL = getDocumentsDirectory().appendingPathComponent(filename)
        
        do {
            let imgData = try Data(contentsOf:imageURL, options:[])
            return imgData
        }
        catch {
            return nil
        }
    }
    
    class func checkFileexist(fileName: String) -> Bool {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    class func deletefile(fileName: String) {
        do {
            let fileManager = FileManager.default
            var filePath = try  fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            filePath.appendPathComponent(fileName)
            
            if fileManager.fileExists(atPath: filePath.path) {
                try fileManager.removeItem(at: filePath)
            }
            
        }catch {
            print("file not deleted")
        }
    }
    
    class func getCustomerQuestionData()->GetQuestionAnswerResponseDTO? {
        let filename = getDocumentsDirectory().appendingPathComponent(questionFilename)
        
        if let encodedObjectJsonString = try? String(contentsOf: filename, encoding: .utf8), let jsonData = encodedObjectJsonString.data(using: .utf8) {
            do {
                let response: GetQuestionAnswerResponseDTO? = try JSONParserSwift.parse(data: jsonData)
                return response
                
            }catch {
                return nil
            }
        }
        return nil
    }
}
