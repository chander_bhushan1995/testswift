//
//  FieldType.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import UIKit

protocol FieldType {
	
	static var keyboardType: UIKeyboardType {get}
	static var isSecure: Bool {get}
	static var maxCharacterLength: Int {get}
    static var minCharacterLength: Int {get}
}

extension FieldType {
    
    static func validateTextLimit(text: String?) -> Bool {
        let text = text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (text == nil && minCharacterLength == 0) || (text != nil && text!.count >= minCharacterLength)  {
            return true
        }
        return false
    }
}

protocol NumberFieldType: FieldType {}

extension NumberFieldType {
	
	static var keyboardType: UIKeyboardType {
		return .numberPad
	}
}

extension FieldType {
	
	static var keyboardType: UIKeyboardType {
		return .default
	}
    
	static var isSecure: Bool {
		return false
	}
	
	static var maxCharacterLength: Int {
		return Int.max
    }
    
    static var minCharacterLength: Int {
        return 1
    }
}

class BaseFieldType: FieldType {}

class NameFieldType: FieldType {
    
    static var minCharacterLength: Int = 1
}

class CurrencyFieldType: FieldType {
    
    static var keyboardType: UIKeyboardType {
        return .numberPad
    }
}

class MobileFieldType: NumberFieldType {
    
    static var minCharacterLength: Int = 10
	
	static let maxCharacterLength: Int = 10
}

class MobileFieldTypeNoMinChar: NumberFieldType {
    
    static var minCharacterLength: Int = 1
    
    static let maxCharacterLength: Int = 10
}

class OTPFieldType: NumberFieldType {
    
    static var minCharacterLength: Int = 6
	
	static let maxCharacterLength: Int = 6
}

class ImeiFieldType: NumberFieldType {
    
    static var minCharacterLength: Int = 7
    
    static let maxCharacterLength: Int = 25
    
    static let isSecure: Bool = false
}

class ReEnterImeiFieldType: NumberFieldType {

    static var minCharacterLength: Int = 7

    static let maxCharacterLength: Int = 25

    static let isSecure: Bool = false
}

class CardFieldType: NumberFieldType {
    
    static var minCharacterLength: Int = 15
    
    static let maxCharacterLength: Int = 23
    
    static let isSecure: Bool = false
}

class BankCardFieldType: NumberFieldType {
    
    static var minCharacterLength: Int = 15
    
    static let maxCharacterLength: Int = 26
    
    static let isSecure: Bool = false
}

class CVVFieldType: NumberFieldType {
    
    static var minCharacterLength: Int = 3
    
    static let maxCharacterLength: Int = 3
    
    static let isSecure: Bool = true
}



class EmailFieldType: FieldType {
    
    static var minCharacterLength: Int = 1
    
	static let keyboardType: UIKeyboardType = .emailAddress
}

class PasswordFieldType: FieldType {
    
    static var minCharacterLength: Int = 1
	
	static let isSecure: Bool = true
}

class SerialNoFieldType: FieldType {
    
    static var minCharacterLength: Int = 7
    
    static let maxCharacterLength: Int = 25
}
