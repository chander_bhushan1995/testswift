//
//  Date.swift
//
//  Created by Mukesh Yadav on 02/08/17.
//  Copyright © 2017 Mukesh Yadav. All rights reserved.
//

import Foundation
import UIKit

let gregorianCalendar = Calendar(identifier: .gregorian)

extension DateFormatter {
    static let assetDetailsDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let serverDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let dayMonthAndYear: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let monthAndYear: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM, YYYY"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let offerDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let slotDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let dayMonth: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let dayMonthFull: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM, EEEE"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let day: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let time24period: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let time12NotAPperiod: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let time12period: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let scheduleFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let scheduleTimeFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let currentDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.calendar = gregorianCalendar
        return formatter
    }()
    
    static let incidentTimeFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy, hh:mm a"
        formatter.calendar = gregorianCalendar
        return formatter
    } ()
    
    static let monthTimeFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM"
        formatter.calendar = gregorianCalendar
        return formatter
    } ()
    
    static let dayMonthYear: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        formatter.calendar = gregorianCalendar
        return formatter
    } ()
    
    static let eventDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy hh:mm:ss aa"
        formatter.calendar = gregorianCalendar
        return formatter
    } ()
    
    static let logsTimeFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd_MMM_yyyy hh_mm_ss_SSSS"
        formatter.calendar = gregorianCalendar
        return formatter
    } ()
    
    static let mirrorTestLogDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        formatter.calendar = gregorianCalendar
        return formatter
    } ()
    
    ///This will habdle the case when device is set to 24 hour format
    func setLocal(){
        var identifier = self.locale.identifier
        if !identifier.hasSuffix("_POSIX") {
            identifier = identifier+"_POSIX"
            let locale = NSLocale(localeIdentifier: identifier)
            self.locale = locale as Locale
        }
    }
}

extension Date {
    var time: Time {
        return Time(self)
    }
    static var currentDate = { () -> Date in
        return Date().addingTimeInterval(TimeInterval(19800))
    }
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return gregorianCalendar.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return gregorianCalendar.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return gregorianCalendar.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return gregorianCalendar.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
    
    func getSuffix() -> String {
        let dayOfMonth = gregorianCalendar.component(.day, from: self)
        return getSuffixFromDay(dayOfMonth: dayOfMonth)
    }
    
    func getSuffixFromDay(dayOfMonth: Int) -> String {
        switch (dayOfMonth) {
        case 1,21,31:
            return "st"
        case 2,22:
            return "nd"
        case 3,23:
            return "rd"
        default:
            return "th"
        }
    }
    
    /// Prints a string representation for the date with the given formatter
    func string(with format: DateFormatter) -> String {
        return format.string(from: self)
    }
    
    func stringInEventFormat() -> String {
        return DateFormatter.eventDateFormat.string(from: self)
    }
    
    /// Creates an `Date` from the given string and formatter. Nil if the string couldn't be parsed
    init?(string: String?, formatter: DateFormatter) {
        guard let date = formatter.date(from: string ?? "") else { return nil }
        self.init(timeIntervalSince1970: date.timeIntervalSince1970)
    }
    
    func days(from date: Date) -> Int? {
        let components = gregorianCalendar.dateComponents([.day], from: date, to: self)
        return components.day
    }
    
    static func getDate(from longString: NSString) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(longString.doubleValue/1000)).addingTimeInterval(TimeInterval(19800))
    }
    
    static func getDateStr(from longString: NSString, with formatter: DateFormatter) -> String {
        return Date(timeIntervalSince1970: TimeInterval(longString.doubleValue/1000)).addingTimeInterval(TimeInterval(19800)).string(with: formatter)
    }
    
    static func compareDate(fromdate: Date, todate: Date)-> Int{
        let order = gregorianCalendar.compare(fromdate, to: todate, toGranularity: .day)
        switch order {
        case .orderedAscending:
            return -1
        case .orderedDescending:
            return 1
        default:
            return 0
        }
    }
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        dateFormatter.calendar = gregorianCalendar
        return dateFormatter.string(from: self).capitalized
    }
    
    func getYears(from date: Date) -> Int {
        // Replace the hour (time) of both dates with 00:00
        let date1 = gregorianCalendar.startOfDay(for: date)
        let date2 = gregorianCalendar.startOfDay(for: self)
        
        let components = gregorianCalendar.dateComponents([.year], from: date1, to: date2)
        return components.year ?? 0
    }
    
    static func convert(date string: String?, from: DateFormatter, to: DateFormatter) -> String? {
        let date = Date(string: string, formatter: from)
        return date?.string(with: to)
    }
    
    static func getDate(from longString: String, with formatter: DateFormatter? = nil) -> Date {
        if let longString = longString as NSString? {
            let date = Date(timeIntervalSince1970: TimeInterval(longString.doubleValue/1000)).addingTimeInterval(TimeInterval(19800))
            if let formatter = formatter, let formattedDate = formatter.date(from: date.string(with: formatter)) {
                return formattedDate
            }
            return date
        }
        return Date.currentDate()
    }
    
    func ordinalDate() -> String {
        // Day
        let anchorComponents = gregorianCalendar.dateComponents([.day, .month], from: self)
        var day  = "\(anchorComponents.day!)"
        switch (day) {
        case "1" , "21" , "31":
            day.append("st")
        case "2" , "22":
            day.append("nd")
        case "3" ,"23":
            day.append("rd")
        default:
            day.append("th")
        }
        
        // Month
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMM"
        dateFormate.calendar = gregorianCalendar
        let month = dateFormate.string(from: self)
        
        return day + " " + month
    }
    
    func timeIntervalTillNowInMilliSeconds() -> Double {
        return timeIntervalSinceNow * -1000
    }
    
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    func isSameDayAs(_ date: Date) -> Bool {
        return gregorianCalendar.component(.day, from: self) == gregorianCalendar.component(.day, from: date) && gregorianCalendar.component(.month, from: self) == gregorianCalendar.component(.month, from: date) && gregorianCalendar.component(.year, from: self) == gregorianCalendar.component(.year, from: date)
    }
    
    static func timeDifferenceString(time1: Date, time2: Date)->String{
        let difference = gregorianCalendar.dateComponents([.minute, .second], from: time1, to: time2)
        let formattedString = String(format: "%02ld:%02ld:00", difference.minute!, difference.second!)
        return formattedString
    }
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}

extension UIDatePicker {
    
    static func getDOBPicker() -> UIDatePicker {
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        
        var component = DateComponents()
        
        component.year = -100
        datePicker.minimumDate = gregorianCalendar.date(byAdding: component, to: Date.currentDate())
        
        component.year = -18
        datePicker.maximumDate = gregorianCalendar.date(byAdding: component, to: Date.currentDate())
        
        datePicker.date = datePicker.maximumDate ?? Date.currentDate()
        
        return datePicker
    }
}

class Time: Comparable, Equatable {
    init(_ date: Date) {
        //get just the minute and the hour of the day passed to it
        let dateComponents = gregorianCalendar.dateComponents([.hour, .minute], from: date)
        
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = dateComponents.hour! * 3600 + dateComponents.minute! * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        hour = dateComponents.hour!
        minute = dateComponents.minute!
    }
    
    init(_ hour: Int, _ minute: Int) {
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = hour * 3600 + minute * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        self.hour = hour
        self.minute = minute
    }
    
    var hour : Int
    var minute: Int
    
    var date: Date {
        //create a new date components.
        var dateComponents = DateComponents()
        
        dateComponents.hour = hour
        dateComponents.minute = minute
        
        return gregorianCalendar.date(byAdding: dateComponents, to: Date())!
    }
    
    /// the number or seconds since the beggining of the day, this is used for comparisions
    private let secondsSinceBeginningOfDay: Int
    
    //comparisions so you can compare times
    static func == (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay == rhs.secondsSinceBeginningOfDay
    }
    
    static func < (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay < rhs.secondsSinceBeginningOfDay
    }
    
    static func <= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay <= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func >= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay >= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func > (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay > rhs.secondsSinceBeginningOfDay
    }
}
