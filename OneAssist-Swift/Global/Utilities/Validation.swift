//
//  Validation.swift
//  OneAssist-Swift
//
//  Created by Varun on 25/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

enum ValidationError: LocalizedError {
    case invalidMobile(String)
    case invalidOTP(String)
    case invalidPincode(String)
    case invalidEmail(String)
    case invalidCardExpiration(String)
    case invalidSimilarity(String)
    case emptyError(String)
    case generalError(String)
    case nameError(String)
    case serialNumberError(String)
    case imeiNumberError(String)
    case invalidDescription(String)
}


class Validation {
    
    static private let regexEmail = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
    static private let regexAlphaNum = "^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$"

    static private let regexMobNo = "^[456789][0-9]{9}$"
    static private let regexMobNo_debug = "^[0123456789][0-9]{9}$"

    static private let regexOtpNo = "^[0-9]{6}$"
    static private let regexPincode = "^[1-9][0-9]{5}$"
    static private let regexNameType = "^[a-zA-Z ]*$"
    static private let regexAlphanumeric = "^[a-zA-Z0-9]*$"
    static private let regexDescription = "^[a-zA-Z0-9,_\\/.!\\-;:?() ]*$"
    static private let stringSelfMatch = "SELF MATCHES %@"
    
    static  let errorEmpty = "Field should not be Empty.".localized()
    static private let invalidMobile = "invalid Mobile No".localized()
    static let emptyAnotherNumber = "Enter different number to get image upload link".localized()
    static private let invalidOTP = "invalid OTP".localized()
    static private let invalidPincode = "invalid Pincode".localized()
    static private let invalidEmail = "invalid EmailId".localized()
    static private let invalidName = "invalid Name".localized()
    static private let invalidCardExpiration = "invalid Expiry Month and Year".localized()
    static private let invalidGeneral = "invalid Input".localized()
    static private let invalidSimilarity = "do not match".localized()
    static private let invalidSerialNumber = "invalid Serial No".localized()
    static private let invalidIMEI = "Please enter a valid IMEI number".localized()
    static private let invalidDescription = "invalid Description".localized()
    
    private static func checkForEmpty(_ text: String?) throws {
        guard let str = text, !str.isEmpty else {
            throw ValidationError.emptyError(Validation.errorEmpty)
        }
    }

    static func validateMobileNumber(text: String?) throws {
        
        try checkForEmpty(text)
        
        var test = NSPredicate(format: stringSelfMatch, regexMobNo)
        
        /// for testing, mobile numbers starting with 0, 1, 2, 3 are considered as valid. (to avoid sending otp on some stranger's mobile)
        #if DEBUG
            test = NSPredicate(format: stringSelfMatch, regexMobNo_debug)
        #endif
        if !(test.evaluate(with: text!)) {
            throw ValidationError.invalidMobile(invalidMobile)
        }
    }
    
    static func validateSecondaryMobileNumber(text: String?, existingNumber: String) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexMobNo)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.invalidMobile(invalidMobile)
        }else if text ==  existingNumber {
            throw ValidationError.invalidMobile(emptyAnotherNumber)
        }
    }
    
    static func validateOTP(text: String?) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexOtpNo)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.invalidOTP(invalidOTP)
        }
    }
    
    static func validatePincode(text: String?) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexPincode)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.invalidPincode(invalidPincode)
        }
    }
    
    static func validateDescription(text: String?) throws {
        
        let test = NSPredicate(format: stringSelfMatch, regexDescription)
        if !test.evaluate(with: text ?? "") {
            throw ValidationError.invalidDescription(invalidDescription)
        }
    }
    
    static func validateEmail(text: String?) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexEmail)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.invalidEmail(invalidEmail)
        }
    }
    
    static func validateName(text: String?) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexNameType)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.nameError(invalidName)
        }
    }
    
    static func validateSerialNumber(text: String?) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexAlphanumeric)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.serialNumberError(invalidSerialNumber)
        }
    }
    
    static func validateIMEINumber(text: String?) throws {
        
        try checkForEmpty(text)
        
        let test = NSPredicate(format: stringSelfMatch, regexAlphanumeric)
        if !(test.evaluate(with: text!)) {
            throw ValidationError.imeiNumberError(invalidIMEI)
        }
    }
    
    static func validateCurrentYearCardExpiry(month: String, year: String) throws {
        
        let currentMonth = gregorianCalendar.component(.month, from: Date.currentDate())
        let currentYear = gregorianCalendar.component(.year, from: Date.currentDate())
        
        if let yr = Int(year), let mth = Int(month) {
            if yr == currentYear && mth < currentMonth {
                throw ValidationError.invalidCardExpiration(invalidCardExpiration)
            }
        } else {
            throw ValidationError.generalError(invalidGeneral)
        }
    }
    
    static func validateSimilarity(text1: String, text2: String, fieldType: String) throws {
        if text1 != text2 {
            throw ValidationError.invalidSimilarity("\(fieldType) \(invalidSimilarity)")
        }
    }

}
