//
//  EventManager.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 28/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//
import EventKit
import UIKit

private let eventMappingFileName = "EventMappingFile.json"

protocol ShowingAlert: class {
    func showAlert()
    func showAlertWithAction()
}

class EventManager: NSObject {
    private var eventStore = EKEventStore()
    weak var delegate: ShowingAlert?
    
    func createEvent(on date: Date, withTitle title: String, forHours hours: Int, location: String? = nil) {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .denied:
            delegate?.showAlertWithAction()
        case .restricted:
            break
            // Handle error
        case .authorized:
            saveEvent(on: date, withTitle: title, forHours: hours, location: location)
        case .notDetermined:
            sharedAppDelegate?.isShowBlurView = false
            eventStore.requestAccess(to: .event) { (granted, error) in
                if granted {
                    self.saveEvent(on: date, withTitle: title, forHours: hours, location: location)
                } else {
                    DispatchQueue.main.async {
                    self.delegate?.showAlertWithAction()
                    }
                }
            }
        }
    }
    
    func createEventForWalletId(_ walletId: String, date: Date, withTitle title: String) {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .denied:
            delegate?.showAlertWithAction()
        case .restricted:
            break
        // Handle error
        case .authorized:
            saveEventForWalletId(walletId, date: date, withTitle: title)
        case .notDetermined:
            sharedAppDelegate?.isShowBlurView = false
            eventStore.requestAccess(to: .event) { (granted, error) in
                if granted {
                    self.saveEventForWalletId(walletId, date: date, withTitle: title)
                } else {
                    DispatchQueue.main.async {
                     self.delegate?.showAlertWithAction()
                    }
                }
            }
        }
    }
    
    func removeEventForWalletId(_ walletId: String, deleteAllEvent: Bool = false) {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .denied:
            delegate?.showAlertWithAction()
        case .restricted:
            break
        // Handle error
        case .authorized:
            deleteEventForWalletId(walletId, deleteAllEvent: deleteAllEvent)
        case .notDetermined:
            sharedAppDelegate?.isShowBlurView = false
            eventStore.requestAccess(to: .event) { (granted, error) in
                if granted {
                    self.deleteEventForWalletId(walletId, deleteAllEvent: deleteAllEvent)
                } else {
                    DispatchQueue.main.async {
                     self.delegate?.showAlertWithAction()
                    }
                }
            }
        }
    }

    
    private func saveEvent(on date: Date, withTitle title: String, forHours hours: Int, location:String? = nil) {
        let event = EKEvent(eventStore: eventStore)
        event.title = title
        event.location = location
        event.startDate = date
        event.endDate = date.addingTimeInterval(TimeInterval(hours * 60 * 60))
        event.alarms = [EKAlarm(absoluteDate: date)]
        event.calendar = eventStore.defaultCalendarForNewEvents
        
        do {
            try eventStore.save(event, span: .thisEvent, commit: true)
            DispatchQueue.main.async {
                self.delegate?.showAlert()
            }
        } catch {
        }
    }
    
    private func saveEventForWalletId(_ walletId: String, date: Date, withTitle title: String) {
        let gregorian = NSCalendar(calendarIdentifier: .gregorian)!
        var components = gregorian.components([.year, .month, .day, .hour, .minute, .second], from: date)
        components.hour = 12
        components.minute = 0
        components.second = 0
        let startDate = gregorian.date(from: components)!
        components.hour = 24
        let endDate = gregorian.date(from: components)!

        if let eventIdentifier = EventMappingData.shared.eventIdForWalletId(walletId), let event = eventStore.event(withIdentifier: eventIdentifier) {
            // Updating the already exisiting event.
            event.title = title
            event.startDate = startDate
            event.endDate = endDate
            event.url = URL(string: "www.oneassist.in/wallet/cards/detail/\(walletId)")
            event.alarms = [EKAlarm(absoluteDate: startDate)]
            event.calendar = eventStore.defaultCalendarForNewEvents
            event.recurrenceRules = [EKRecurrenceRule(recurrenceWith: .monthly, interval: 1, end: EKRecurrenceEnd(end: date.addingTimeInterval(TimeInterval(6 * 30 * 24 * 60 * 60))))] // recurring event for 6 months
            do {
                try eventStore.save(event, span: .futureEvents, commit: true)
                if let eventId = event.eventIdentifier {
                    EventMappingData.shared.setEventId(eventId, walletId: walletId)
                }
                DispatchQueue.main.async {
                    self.delegate?.showAlert()
                }
            } catch {
            }
        } else {
            // Creating a new event.
            let event = EKEvent(eventStore: eventStore)
            event.title = title
            event.startDate = startDate
            event.endDate = endDate
            event.url = URL(string: "www.oneassist.in/wallet/cards/detail/\(walletId)")
            event.alarms = [EKAlarm(absoluteDate: startDate)]
            event.calendar = eventStore.defaultCalendarForNewEvents
            event.recurrenceRules = [EKRecurrenceRule(recurrenceWith: .monthly, interval: 1, end: EKRecurrenceEnd(end: date.addingTimeInterval(TimeInterval(6 * 30 * 24 * 60 * 60))))] // recurring event for 6 months
            do {
                try eventStore.save(event, span: .futureEvents, commit: true)
                if let eventId = event.eventIdentifier {
                    EventMappingData.shared.setEventId(eventId, walletId: walletId)
                }
                DispatchQueue.main.async {
                    self.delegate?.showAlert()
                }
            } catch {
            }
        }
    }
    
    private func deleteEventForWalletId(_ walletId: String, deleteAllEvent: Bool = false) {
        if let eventIdentifier = EventMappingData.shared.eventIdForWalletId(walletId), let calendarEvent = eventStore.event(withIdentifier: eventIdentifier) {
            do {
                try self.eventStore.remove(calendarEvent, span: deleteAllEvent ? .futureEvents : .thisEvent)
                if deleteAllEvent {
                    EventMappingData.shared.removeEventIdForWalletId(walletId)
                }
            } catch {
            }
        } else {
            print("Doesn't have mapping for walletId: \(walletId) or a calender event")
        }
    }
}

class EventMappingData: NSObject, Codable {
    private var mappingData: [String:String] = [:]
    
    static let shared = EventMappingData()
    
    private override init() {
        super.init()
        self.mappingData = eventMappingData()?.mappingData ?? [:]
    }
    
    func eventIdForWalletId(_ walletId: String) -> String? {
        return mappingData[walletId]
    }
    
    func setEventId(_ eventId: String, walletId: String) {
        mappingData[walletId] = eventId
        saveEventMappingData()
    }
    
    func removeEventIdForWalletId(_ walletId: String) {
        mappingData.removeValue(forKey: walletId)
        saveEventMappingData()
    }
    
    private func saveEventMappingData() {
        let testResult = EventMappingData.shared
        let encodedObject = try? JSONEncoder().encode(testResult)
        if let encodedObjectJsonString = String(data: encodedObject!, encoding: .utf8) {
            try? encodedObjectJsonString.write(to: eventMappingFileUrl(), atomically: true, encoding: .utf8)
        }
    }
    
    private func eventMappingData() -> EventMappingData? {
        if let encodedObjectJsonString = try? String(contentsOf: eventMappingFileUrl(), encoding: .utf8), let jsonData = encodedObjectJsonString.data(using: .utf8), let testResultObject = try? JSONDecoder().decode(EventMappingData.self, from: jsonData) {
            return testResultObject
        }
        
        return nil
    }
    
    private func eventMappingFileUrl() -> URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(eventMappingFileName)
    }
}
