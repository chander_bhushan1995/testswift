//
//  JavaScriptJsonHandler.swift
//  OneAssist-Swift
//
//  Created by Raj on 29/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation
import Alamofire
import JavaScriptCore

enum JSError:Error {
    case techErr
    case parseError
    case jsException(String)
    var localizedDescription: String?{
        switch self {
        case .techErr: return "Technical error accured."
        case .parseError: return "Unable to parse JSON."
        case .jsException (let exc): return "JS Exception:"+exc
        }
    }
}

@objc class JavaScriptJsonHandler :NSObject{
    static var jsContext: JSContext!
    
    static func downloadJSFile(from url:String) {
        if  let url = URL(string: url) {
            let destination: DownloadRequest.Destination = { _, _ in
                return (fileInDocumentsDirectory(filename: Strings.ServiceRequest.JavaScript.fileName) ?? URL(fileURLWithPath: ""), [.removePreviousFile, .createIntermediateDirectories])
            }
            AF.download(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil, to: destination)
                .response(completionHandler: { response in
                    print(response)
                })
        }
    }
    

    static  func fileInDocumentsDirectory(filename: String) -> URL? {
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
        
    }
    

    static  func getDocumentsURL() -> URL {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        return documentsURL
    }
    
    static  func getTimeLineData(response:SearchServiceRequestPlanResponseDTO, completionHandler: @escaping (JavaScriptTimeineResponceDTO?, Error?) -> Void)  {
        jsContext = JSContext()
        jsContext.exceptionHandler = { context, exception in
            if let exc = exception {
                print("JS Exception:", exc.toString())
                completionHandler(nil, JSError.jsException(exc.toString()))
            }
        }
        if let jsFilePath = fileInDocumentsDirectory(filename: Strings.ServiceRequest.JavaScript.fileName)?.path {
            do {
                let fileContents = try String(contentsOfFile: jsFilePath)
                jsContext.evaluateScript(fileContents)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        if let function = jsContext.objectForKeyedSubscript(Strings.ServiceRequest.JavaScript.functionName),let timeLineData = response.sendJSData  {

                if let functionData = function.call(withArguments: [timeLineData,true]) {
                    if let dict = functionData.toString().toDictionary(){
                        do {
                        if dict["error"] == nil {
                            
                            
                            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
                            let decoded = String(data: jsonData, encoding: .utf8)!
                            debugPrint("Timeline JS Parsed Data ------- \n",decoded)
                            let req = JavaScriptTimeineRequestDTO(timeLineData: jsonData)
                            let _ = JavaScriptTimelineDataRequestUsecase.service(requestDTO: req) { (usecase, response, error) in
                               completionHandler(response,error)
                            }
                            }else{
                                completionHandler(nil, JSError.techErr)
                            }
                        }catch{
                            completionHandler(nil, JSError.parseError)
                        }
                    }
            }
        }
    }
}

