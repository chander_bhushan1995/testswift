//
//  DeepLinkManager.swift
//  OneAssist-Swift
//
//  Created by OneAssist on 12/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import FirebaseDynamicLinks
import React

class DeepLinkManager {
    static var webengageEventTrackingDict = [Event.EventKeys: String]()
    static var suggestAMobilePlacDict = [String:String]()
    static var webEngageEventTrackingURL:String?
    static var oneOauUrl:String?
    
    /*
     if deeplink is coming from push notification or dynamic links isInAppDeeplink value will be false other wise true
     */
    static func handleLink(_ url: URL?, usedIn internalApp: Bool = true, appOpenedWithLink: String? = nil) {
        sharedAppDelegate?.forInternalDeeplink = internalApp
        if let urlValue = url,
           let encodeUrl = urlValue.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let decodeUrl = URL(string: encodeUrl) {
            if !internalApp {
                sharedAppDelegate?.appOpenedWithLink = appOpenedWithLink ?? decodeUrl.absoluteString
                sharedAppDelegate?.appopenEvent()
            }
            
            DeepLinkManager.webEngageEventTrackingURL = decodeUrl.absoluteString
            webengageEventTrackingDict[.utmSource] = decodeUrl.queryItems["utm_source"] ?? ""
            webengageEventTrackingDict[.utmMedium] = decodeUrl.queryItems["utm_medium"] ?? ""
            webengageEventTrackingDict[.utmCampaign] = decodeUrl.queryItems["utm_campaign"] ?? ""
            
            if (decodeUrl.queryItems.count) > 0 {
                if decodeUrl.pathComponents.contains("bankdetail") {
                    handleBankDetailQueryUrl(url: decodeUrl)
                }
                else if decodeUrl.pathComponents.contains("wallet/offer") || decodeUrl.path.contains("wallet/offlineoffers"){
                    handleLinkPath(decodeUrl.path, completeUrl: decodeUrl)
                } else {
                    handleQueryUrl(url: decodeUrl)
                }
            } else {
                handleLinkPath(decodeUrl.path, completeUrl: decodeUrl)
            }
        }
    }
    
    static func handleQueryUrl(url:URL) {
        
        let pathString = url.path.dropFirst()
        let deepLink = DeepLink(url: String(pathString))
        currentDeeplink = deepLink
        currentDeeplinkUrl = url.absoluteString
        
        if deepLink == nil { // deeplink is not handled on app then it will redirect to device webview
            Utilities.openCustomURL(url: url)
        }
        
        checkAndHandleAlreadyLoaded()
    }
    
    static func handleBankDetailQueryUrl(url:URL) {
        guard let urlTrim = URL(string: url.deletingLastPathComponent().relativePath) else {
            return
        }
        
        let pathString = urlTrim.path.dropFirst()
        
        let deepLink = DeepLink(url: String(pathString))
        currentDeeplink = deepLink
        currentDeeplinkUrl = url.absoluteString
        
        checkAndHandleAlreadyLoaded()
    }
    
    static func handleLinkPath(_ path: String, completeUrl: URL? = nil) {
        let componentsArray = path.components(separatedBy: "/").filter({ !$0.isEmpty })
        if componentsArray.count == 0 { return }
        
        var linkKey = componentsArray[0]
        var deepLink = DeepLink(url: linkKey)
        if (componentsArray.count >= 2) {
            let paramArray = Array(componentsArray[2..<componentsArray.count])// save extra params if any
            linkKey = "\(componentsArray[0])/\(componentsArray[1])"
            deepLink = DeepLink(url: linkKey)
            currentDeeplinkParams = paramArray
        }
        
        currentDeeplink = deepLink
        
        if let completeUrl = completeUrl {
            currentDeeplinkUrl = completeUrl.absoluteString
        }
        
        if deepLink == nil { // deeplink is not handled on app then it will redirect to device webview
            if let url = URL(string: path) {
                Utilities.openCustomURL(url: url)
            }
        }
        
        checkAndHandleAlreadyLoaded()
    }
    
    static func handlePushNotification(_ userInfo: [AnyHashable : Any], isCameFromNotification: Bool = false) {
        if !isCameFromNotification {
            EventTracking.shared.eventTracking(name: .afOpenedWithPN)
        }
        let userInfo = userInfo
        if let customData = userInfo["customData"] as? [[String : String]], let pushData = customData.filter({ (dict) -> Bool in
            return dict["key"] == "ll_external_link_url"  // for external link
        }).first {
            print(pushData)
            if let link = pushData["value"], let url = URL(string: link) {
                if let isWebEngageNotification = userInfo["source"] as? String, isWebEngageNotification == "webengage" {
                    // open in the external app.
                    Utilities.openCustomURL(url: url)
                    sharedAppDelegate?.appOpenedWithLink = url.absoluteString
                    sharedAppDelegate?.appopenEvent()
                }
            }
        }
        else if let customData = userInfo["customData"] as? [[String : String]], let pushData = customData.filter({ (dict) -> Bool in
            return dict["key"] == "ll_deep_link_url"
        }).first {
            print(pushData)
            if let link = pushData["value"], let url = URL(string: link) {
                if let isWebEngageNotification = userInfo["source"] as? String, isWebEngageNotification == "webengage" {
                    let handsetModel = customData.filter({$0["key"] == Constants.RequestKeys.handsetModel}).first?["value"] ?? ""
                    let handsetAmount = customData.filter({$0["key"] == Constants.RequestKeys.handsetAmount}).first?["value"] ?? ""
                    let date = customData.filter({$0["key"] == Constants.RequestKeys.date}).first?["value"] ?? ""
                    
                    DeepLinkManager.suggestAMobilePlacDict = [Constants.RequestKeys.handsetModel:handsetModel,
                                                              Constants.RequestKeys.handsetAmount:handsetAmount,
                                                              Constants.RequestKeys.date:date]
                    DeepLinkManager.handleLink(url, usedIn: false)
                }else {
                    let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
                    if let dynamicLinkUrl = dynamicLink?.url {
                        DeepLinkManager.handleLink(dynamicLinkUrl, usedIn: false)
                    } else {
                        DynamicLinks.dynamicLinks().handleUniversalLink(url, completion: { (link, error) in
                            DeepLinkManager.handleLink(link?.url, usedIn: false)
                        })
                    }
                }
            }
        }
        else if let urlString = userInfo["ll_deep_link_url"] as? String, let url = URL(string: urlString) {
            
            let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
            if let dynamicLinkUrl = dynamicLink?.url {
                DeepLinkManager.handleLink(dynamicLinkUrl, usedIn: false)
            } else {
                DynamicLinks.dynamicLinks().handleUniversalLink(url, completion: { (link, error) in
                    DeepLinkManager.handleLink(link?.url, usedIn: false)
                })
            }
        } else if let notificationType = userInfo["type"] as? String {
            var link: DeepLink?
            var linkParams: [String]?
            switch notificationType {
            case "AW", "TX", "EOM":
                link = .mobileTab
            case "EOS":
                var params = [String]()
                if let offerId = userInfo["offer_id"] as? String {
                    params.append(offerId)
                }
                
                if let outletCode = userInfo["outlet_code"] as? String {
                    params.append(outletCode)
                }
                
                link = .offerDetails
                linkParams = params
            case "RW", "UG", "MB":
                break
            case "PB": // Goto plan benefits
                break
            case "BV", "MSMB":
                break
            default:
                break
            }
            
            if let deeplink = link {
                currentDeeplink = deeplink
                currentDeeplinkParams = linkParams
            }
            sharedAppDelegate?.appopenEvent()
            checkAndHandleAlreadyLoaded()
        }
    }
    
    static func checkAndHandleAlreadyLoaded() {
        guard let tabVC = rootTabVC, !Utilities.isCustomerLoggedoutAndNotSkipped() else { return }
        
        if let internalLink = sharedAppDelegate?.forInternalDeeplink, !internalLink {
            rootNavVC?.dismiss(animated: true, completion: nil)
            rootNavVC?.popToRootViewController(animated: true)
            // TODO: Any screen presented in tab bar heirarchy to be dismissed. This is to be done when other react modules are to be merged in with tab bar react module
        } else {
            sharedAppDelegate?.finalTabForInternalDeeplink = tabVC.selectedIndex
        }
        checkDeeplink(for: tabVC)
    }
    
    // MARK: TAB BAR DEEPLINK HANDLING
    
    static func checkDeeplink(for viewController: TabVC) {
        if currentDeeplink != nil {
            performDeeplinkActionWithLoginHandling(vc: viewController) {
                if let deeplink = currentDeeplinkUrl, let deeplinkUrl = URL(string: deeplink), let deeplinkNo = getParamDict(from: deeplinkUrl)["mobileNo"], deeplinkNo != UserCoreDataStore.currentUser?.mobileNo {
                    viewController.showPopupForCurrectMobileNumber(forScreen: nil) { status in
                        if status {
                            let pathString = deeplinkUrl.path.dropFirst()
                            currentDeeplink = DeepLink(url: String(pathString))
                            currentDeeplinkUrl = deeplinkUrl.absoluteString
                        } else {
                            currentDeeplink = nil
                        }
                    }
                    return
                }
                checkDeeplinkVerified(for: viewController)
            }
        }
    }
    
    static func checkDeeplinkVerified(for viewController: TabVC) {
        // Change Tab index
        if let _ = getDeeplinkForHomeTab() {
            viewController.changeTabIndex(itemTag: .newHome)
        } else if let _ = getDeeplinkForBuyTab() {
            viewController.changeTabIndex(itemTag: .buy)
        } else if let _ = getDeeplinkForMemberShipTab() {
            viewController.changeTabIndex(itemTag: .membership)
        } else if let _ = getDeeplinkForAccountTab() {
            viewController.changeTabIndex(itemTag: .account)
        }
        
        var nextViewController: UIViewController?
        
        switch currentDeeplink {
        case .addCard, .myCards, .cards:
            let vc = CardManagementVC()
            if currentDeeplink == .addCard {
                vc.addToInitialProperties(["deeplink_url_string" : "addcard/DBC"])
            } else if let params = currentDeeplinkParams, currentDeeplink == .cards {
                vc.addToInitialProperties(["deeplink_url_string" : params.joined(separator: "/")])
            }
            viewController.present(vc, animated: true, completion: nil)
        case .bankBranch:
            nextViewController = BankBranchesListVC()
        case .mhc:
            nextViewController = MHCCategoryVC(nibName: "MHCCategoryVC", bundle: nil)
        case .mhcResult:
            nextViewController = MHCResultVC(nibName: "MHCResultVC", bundle: nil)
        case .ATM:
            nextViewController = AtmBranchesMapVC()
        case .offerDetails:
            if let params = currentDeeplinkParams {
                if params.count == 2 {
                    // Offline Offer
                    let cardoffer = BankCardOffers(dictionary: [:])
                    cardoffer.offerCode = params.first
                    cardoffer.outletCode = params.last
                    
                    let vc = OfflineOfferDetailVC()
                    vc.selectedOffer = cardoffer
                    vc.fromScreen = .deepLink
                    vc.webengageEventTrackingDict = self.webengageEventTrackingDict
                    nextViewController = vc
                } else if params.count == 1 {
                    // Online Offer
                    let cardoffer = BankCardOffers(dictionary: [:])
                    cardoffer.offerId = params.first
                    
                    let vc = OnlineOfferDetailVC()
                    vc.webengageEventTrackingDict = self.webengageEventTrackingDict
                    vc.selectedOffer = cardoffer
                    vc.fromScreen = .deepLink
                    nextViewController = vc
                }
            }
        case .buyback, .myOffers, .offlineOffer, .onlineOffer, .myRewards, .onboardingQuestion, .profileQuestion, .myProfile, .rewardAppShare, .whcSelectAddress, .serviceDetail, .idfence:
            ChatBridge.sendNotification(withName: "DeeplinksData", body: currentDeeplinkUrl ?? currentDeeplink?.rawValue ?? "")
        case .covidKnowMore, .riskCalculator, .homeSuggestAPlan, .sopSuggestAPlan, .suggestAplanHome, .walletSuggestAPlan, .suggestAplanWallet, .mobileSuggestAPlan, .suggestAplanMobile, .submitDetails, .homeAssistSeggestPlan, .buy, .salesRecommendationIDFence, .salesRecommendation, .howclaimworks:
            if currentDeeplink == .riskCalculator {
                if (!viewController.isReactViewLoaded) {
                    viewController.onReactViewAppear = {
                        viewController.buySubTabSelectedCategory = "F"
                    }
                } else {
                    viewController.changeBuyPlanCategory("F")
                }
            }
            
            if let urlPath = currentDeeplinkUrl, let url = URL(string: urlPath)?.appending("isCameFromDeeplink", value: (sharedAppDelegate?.forInternalDeeplink ?? false) ? "false" : "true") {
                let dict = getParamDict(from: url)
                if let category = dict["category"] {
                    if (!viewController.isReactViewLoaded) {
                        viewController.buySubTabSelectedCategory = category
                    } else {
                        viewController.changeBuyPlanCategory(category)
                    }
                }
                if currentDeeplink == .buy || currentDeeplink == .salesRecommendation {
                    if (dict.keys.contains("service") || dict.keys.contains("product")) {
                        viewController.startBuyPlanJourneyForDeeplink(url.absoluteString)
                    }
                } else {
                    viewController.startBuyPlanJourneyForDeeplink(url.absoluteString)
                }
            }
        case .upgradeNowMobile:
            if let url = currentDeeplinkUrl {
                viewController.startBuyPlanJourneyForDeeplink(url)
            }
        case .whcInspectionDetails:
            if let serviceRequestNumber = currentDeeplinkParams?.first {
                let inspectionDetail = InspectionDestailsVC()
                inspectionDetail.serviceRequestId = Int(serviceRequestNumber) ?? 0
                nextViewController = inspectionDetail
            }
        case .srTimeline:
            if let urlStr = currentDeeplinkUrl, let url = URL(string: urlStr) {
                let dict = getParamDict(from: url)
                if let timeline = TimelineRouter.getAssociatedTimeline(serviceType: dict["serviceType"] ?? "") {
                    timeline.deeplinkModel = (dict["srNumber"] ?? "",dict["serviceType"] ?? "")
                    timeline.crmTrackingNumber = dict["srNumber"]
                    nextViewController = timeline
                }
            }
        case .srReschedule:
            if let urlStr = currentDeeplinkUrl, let url = URL(string: urlStr) {
                let dict = getParamDict(from: url)
                viewController.routeToScheduleInspection(srId: dict["srId"] ?? "", claimType: .extendedWarranty, crmTrackingNumber: dict["srNumber"], pincode: dict["pincode"], serviceRequestType: dict["serviceType"], memId: nil, assestId: nil, productCode: nil)
            }
        case .excessPayment:
            let paymentVC = ExcessPaymentDeeplinkVC()
            paymentVC.urlString = currentDeeplinkUrl ?? ""
            nextViewController = paymentVC
        case .bankDetails:
            if let urlStr = currentDeeplinkUrl, let url = URL(string: urlStr) {
                let dict = getParamDict(from: url)
                let bankDetails = BankDetailsVC()
                bankDetails.srNumber = dict["srNumber"]
                nextViewController = bankDetails
            }
        
        case .activateVoucher:
            viewController.routeToActivateVoucher()
        case .favoriteOffer:
            let bookmarkedVC = BookmarkedOffersVC()
            bookmarkedVC.webengageEventTrackingDict = self.webengageEventTrackingDict
            bookmarkedVC.fromScreen = .deepLink
            nextViewController = bookmarkedVC
        case .openChat:
            if  isValidWebEngageURL(for: "/common/chat") && webengageEventTrackingDict.count > 0 {
                EventTracking.shared.eventTracking(name: .chat, [.utmCampaign: self.webengageEventTrackingDict[.utmCampaign] ?? "", .utmMedium: self.webengageEventTrackingDict[.utmMedium] ?? "", .utmSource: self.webengageEventTrackingDict[.utmSource] ?? ""])
                webengageEventTrackingDict = [:]
            }
            viewController.present(ChatVC(), animated: true, completion: nil)
        case .sodTimeline:
            if let urlStr = currentDeeplinkUrl, let url = URL(string: urlStr) {
                let dict = getParamDict(from: url)
                if let srID = dict["srId"], let srNumber = dict["srNumber"], let srType = dict["serviceType"] {
                    let timeline = TimelineBaseVC()
                    timeline.srId = srID
                    timeline.boolHistoryTimeLine = false
                    timeline.crmTrackingNumber = srNumber
                    timeline.serviceRequestSourceType = Strings.SODConstants.categoryName
                    nextViewController = timeline
                }
            }
        case .pinelab:
            let editDetailVC = EditDetailsVC()
            if let urlStr = currentDeeplinkUrl, let url = URL(string: urlStr) {
                let dict = getParamDict(from: url)
                if let token = dict["token"]?.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "/+=\n").inverted), !token.isEmpty {
                    editDetailVC.addToInitialProperties(["pinlab_token": token, "isNameEditable": dict["isNameEditable"]?.uppercased() == "Y", "isPinelabLink": dict["isPinelabLink"]?.uppercased() == "Y"])
                }
            }
            viewController.present(editDetailVC, animated: true, completion: nil)
        case .rateApp:
            UserDefaultsKeys.Location.set(value: Event.Events.deeplink.rawValue)
            Utilities.checkForRatingPopUp(viewController: viewController)
        case .component:
            CacheManager.shared.selectedTabIndex = .buy
            if let urlStr = currentDeeplinkUrl {
                guard let url = URL(string: urlStr) else { return }
                let dict = getParamDict(from: url)
                guard let theJSONData = try? JSONSerialization.data(withJSONObject: dict, options: []) else { return }
                guard let theJSONText = String(data: theJSONData, encoding: .utf8) else {return}
                let catalystVC =  CatalystVC()
                catalystVC.addToInitialProperties(["catalystQueryParams":theJSONText,"catalystDeeplinkURL":urlStr])
                viewController.present(catalystVC, animated: true, completion: nil)
            }
        default: break
        }
        
        currentDeeplink = nil
        if let vc = nextViewController {
            viewController.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static func getParamDict(from url: URL) -> [String: String] {
        var paramDict: [String: String] = [:]
        if let query = url.query {
            let params = query.components(separatedBy: "&")
            for param in params {
                let kv = param.components(separatedBy: "=")
                if kv.count == 2 {
                    paramDict[kv[0]] = kv[1].removingPercentEncoding
                }
            }
        }
        return paramDict
    }
    
    static func isValidWebEngageURL(for path:String) -> Bool {
        if let webEngageEventTrackingURL = webEngageEventTrackingURL, let urlPath = URL(string:webEngageEventTrackingURL)?.path, urlPath.contains(path) {
            return true
        }
        
        return false
    }
    
    static func getDeeplinkForHomeTab() -> DeepLink? {
        return DeepLink.listForHomeTab().first(where: { $0 == currentDeeplink})
    }
    
    static func getDeeplinkForBuyTab() -> DeepLink? {
        return DeepLink.listForBuyTab().first(where: { $0 == currentDeeplink})
    }
    
    
    static func getDeeplinkForMemberShipTab() -> DeepLink? {
        return DeepLink.listForMemberShipTab().first(where: { $0 == currentDeeplink})
    }
    
    static func getDeeplinkForAccountTab() -> DeepLink? {
        return DeepLink.listForAccountTab().first(where: { $0 == currentDeeplink})
    }
    
    static func anyDeeplinkAvailable() -> Bool {
        return currentDeeplink != nil
    }
    
    static func isLoginRequired() -> Bool {
        return DeepLink.listForWhichLoginRequired().contains(where: { $0 == currentDeeplink})
    }
    
    static func performDeeplinkActionWithLoginHandling(vc: BaseVC, _ completionblock: @escaping () -> Void) {
        if Utilities.isCustomerVerified() || !isLoginRequired() {
            completionblock()
        } else {
            vc.showLoginView(forScreen: nil) { (status) in
                if status {
                    completionblock()
                }
            }
        }
    }
}
