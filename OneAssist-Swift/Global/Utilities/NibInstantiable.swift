//
//  NibFileOwnerLoadable.swift
//  ViewTemplate
//
//  Created by Pankaj Verma on 05/03/19.
//  Copyright © 2019 Pankaj Verma. All rights reserved.
//

import UIKit
public protocol NibInstantiable: class {
     static var nib: UINib { get }
}

public extension NibInstantiable where Self: UIView {
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
   private func instantiateFromNib() -> UIView? {
        let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
    }
    
    func loadNibContent() {
        guard let view = instantiateFromNib() else {
            fatalError("Failed to instantiate nib \(Self.nib)")
        }
        self.insertSubview(subview: view, padding: Padding(top: 0,leading:0, bottom:20, trailing:0))

    }
}

