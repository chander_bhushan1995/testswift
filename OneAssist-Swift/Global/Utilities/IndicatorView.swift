//
//  IndicatorView.swift
//  OneAssist-Swift
//
//  Created by Sudhir.Kumar on 06/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner

class IndicatorView: UIView {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var spinnerView: UIView!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let sv: MMMaterialDesignSpinner = MMMaterialDesignSpinner(frame: CGRect(x: 0, y: 0, width: 49, height: 49))
        sv.lineWidth = 2.0
        sv.tintColor = UIColor.buttonTitleBlue
//        sv.center = spinnerView.center
        sv.startAnimating()
        spinnerView.addSubview(sv)
    }
 
    
    func setMessage(_ message: String){
        lblMessage.text = message
    }

}
