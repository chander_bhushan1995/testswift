//
//  FileDownloader.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 18/02/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import Foundation

class FileDownloader {

    static func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }

    static func loadFileAsync(url: URL, fileN: String?, completion: @escaping (String?, Error?) -> Void) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var fileName = url.lastPathComponent
        if let urlConponents = URLComponents(string: url.absoluteString) {
            if let alertId = urlConponents.queryItems?.first(where: { $0.name == "alertId" })?.value {
               fileName = alertId + ".pdf"
            }else if let file = fileN {
                fileName = file
            }else {
                fileName =  url.lastPathComponent
            }
        }
        
        let destinationUrl = documentsUrl.appendingPathComponent(fileName)
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.absoluteString, nil)
        }
        else
        {
            let downloadDTO = DownloadRequestDTO()
            downloadDTO.downloadUrl = url.absoluteString
            downloadDTO.destinationUrl = destinationUrl.absoluteString
            
            let _ = DownloadFileUseCase.service(requestDTO: downloadDTO) {(usecase, response, error)  in
                if response?.statusCode == "200"{
                    completion(response?.destinationUrl, nil)
                    
                }else {
                    completion(response?.destinationUrl, error)
                }
            }
        }
    }
    
    static func loadDocumentsAsync(url: URL, name: String , completion: @escaping (String?, Error?) -> Void)
    {
        let downloadDTO = DownloadRequestDTO()
        downloadDTO.downloadUrl = url.absoluteString
        downloadDTO.fileName = name
        downloadDTO.isSaveFile = true
        
        let _ = DownloadFileUseCase.service(requestDTO: downloadDTO) {(usecase, response, error)  in
            if response?.statusCode == "200"{
                completion(response?.destinationUrl, nil)
                
            }else {
                completion(response?.destinationUrl, error)
            }
        }
    }
    
    static func fileOpeninWebView(url: URL, fileName: String?, headerTitle: String = "", message: String){
        DispatchQueue.main.async {
            let vc = Utilities.topMostPresenterViewController
            var loaderCount = 0
            Utilities.addLoader(message: message, count: &loaderCount, isInteractionEnabled: false)
        }
        FileDownloader.loadFileAsync(url: url, fileN: fileName) { (path, error) in
            DispatchQueue.main.async {
                let vc = Utilities.topMostPresenterViewController
                var loaderCount = 0
                Utilities.removeLoader(count: &loaderCount)
                print("PDF File downloaded to : \(path!)")
                if error == nil {
                    let webViewVC = WebViewVC()
                    webViewVC.isLinkWebView = true
                    webViewVC.isShowFromDocumentDic = true
                    webViewVC.screenTitle = headerTitle
                    webViewVC.urlString = path ?? ""
                    Utilities.topMostPresenterViewController.presentInFullScreen(BaseNavigationController(rootViewController: webViewVC), animated: true, completion: nil)
                }
            }
        }
    }
    
    static func loadFileAsync(url: URL, fileName: String, completion: @escaping (String?, Error?) -> Void) {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destinationUrl = documentsUrl.appendingPathComponent(fileName)
        if let attributes = try? FileManager.default.attributesOfItem(atPath: destinationUrl.path), let size = attributes[.size] as? Int, let modificationDate = attributes[.modificationDate] as? Date, size > 0, let daysDiff = modificationDate.days(from: Date()), daysDiff < 30  {
            print("Non-Empty, Recent File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.absoluteString, nil)
        } else {
            let downloadDTO = DownloadRequestDTO()
            downloadDTO.downloadUrl = url.absoluteString
            downloadDTO.destinationUrl = destinationUrl.absoluteString
            
            let _ = DownloadFileUseCase.service(requestDTO: downloadDTO) {(usecase, response, error)  in
                completion(response?.destinationUrl, response?.statusCode == "200" ? nil : (error ?? LogicalError("File Doesn't exist", code: response?.statusCode)))
            }
        }
    }
}
