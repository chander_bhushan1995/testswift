//
//  Utilities.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/25/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
import WebEngage
import StoreKit
import MMMaterialDesignSpinner
import FirebaseCrashlytics
import Firebase
import FirebaseAnalytics
import AVFoundation
import AppsFlyerLib

enum Destination: String {
    case mobile = "MP01"
    case tablet = "TBL"
    case laptop = "LAP"
    case smartwatch = "WR"
    case whc = "HA"
    case ew = "EW"
    case gadgetserve = "HAGS"
    case wallet = "WP01"
    case idfance = "IDF"
    case sod = "SOD"
    case sodQTY = "SODQTY"
    
    var category: String {
        switch self {
        case .mobile, .tablet, .laptop, .smartwatch: return Constants.Category.personalElectronics
        case .ew, .whc, .gadgetserve: return Constants.Category.homeAppliances
        case .wallet, .idfance: return Constants.Category.finance
        default: return ""
        }
    }
    
    var title: String {
        switch self {
        case .mobile: return "Mobile"
        case .tablet: return "Tablet"
        case .laptop: return "Laptop"
        case .smartwatch: return "Smartwatch"
        case .whc: return "HomeAssist"
        case .ew: return "Home EW"
        case .gadgetserve: return "GadgetServ"
        case .wallet: return "Wallet"
        case .idfance: return "ID Fence"
        case .sod: return "SOD"
        default: return ""
        }
    }
    var iconName: String? {
        switch self {
        case .mobile: return "mobile_buy"
        case .tablet: return "tablet_buy"
        case .laptop: return "laptop_buy"
        case .smartwatch: return "smartwatch_buy"
        case .whc: return "whc_buy"
        case .ew: return "ew_buy"
        case .gadgetserve: return nil
        case .wallet: return "wallet_buy"
        case .idfance: return "idfence_buy"
        default: return ""
        }
    }
}


@objc class Utilities:NSObject {
    
    static var decimalNumberFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        
        return numberFormatter
    }
    
    static func tipTool(with margin: CGFloat) -> PopTip {
        let tip = PopTip()
        tip.bubbleColor = .black
        tip.maskColor = .clear
        tip.shouldShowMask = true
        tip.arrowSize = CGSize(width: 15, height: 10)
        tip.font = DLSFont.supportingText.regular
        tip.edgeMargin = margin
        tip.edgeInsets = UIEdgeInsets(top: 11, left: 15, bottom: 11, right: 15)
        
        return tip
    }
    
    static func addLoader(onView: UIView? = UIApplication.shared.keyWindow, count: inout Int, isInteractionEnabled: Bool) {
        print("addLoader: count is \(count)")
        guard let onView = onView else {
            return
        }
        
        if count == 0 {
            if let view = onView as? UIWindow {
                let mask = UIView()
                mask.tag = 998
                mask.frame = view.bounds
                mask.backgroundColor = UIColor.backgroundWhiteBlue.withAlphaComponent(0.7)
                addActivityIndicator(onView: mask)
                view.addSubview(mask)
            } else {
                addActivityIndicator(onView: onView)
            }
        }
        
        guard let view = onView.viewWithTag(999) else {
            return
        }
        
        onView.isUserInteractionEnabled = isInteractionEnabled
        
        onView.bringSubviewToFront(view)
        count = count + 1
    }
    
    static func isReuploadCase(of docName: String, with verificationDocs: [VerificationDocument]?) -> Bool {
        if let docs = verificationDocs {
            if !(docs.filter({ $0.name == docName && ($0.status == Strings.DocsUploadStatus.reupload && $0.status != nil) }).isEmpty) {
                return true
            }
        }
        
        return false
    }
    
    static func isReuploadCaseOfInvoice(with verificationDocs: [VerificationDocument]?) -> Bool {
        if let docs = verificationDocs {
            if !(docs.filter({ $0.name == Constants.RequestKeys.invoice_image && ($0.status == Strings.DocsUploadStatus.reupload && $0.status != nil) }).isEmpty) {
                return true
            }
        }
        return false
    }
    
    static func reuploadCase(of docName: String, with verificationDocs: [VerificationDocument]?) -> String? {
        if let docs = verificationDocs {
            if let doc = docs.filter({ $0.name == docName }).first {
                return doc.status
            }
        }
        return nil
    }
    
    static func addLoader(onView: UIView? = UIApplication.shared.keyWindow, message : String?, count: inout Int, isInteractionEnabled: Bool) {
        print("addLoader: count is \(count)")
        guard let onView = onView else {
            return
        }
        
        if count == 0 {
            if let view = onView as? UIWindow {
                let mask = UIView()
                mask.tag = 998
                mask.frame = view.bounds
                mask.backgroundColor = UIColor.white.withAlphaComponent(0.8)
                view.addSubview(mask)
                let _  = addProgressView(onView: mask, message: message)
            } else {
                let _ = addProgressView(onView: onView, message: message)
            }
        }
        
        guard let view = onView.viewWithTag(999) else {
            return
        }
        if let indicatorView = view as? IndicatorView {
            indicatorView.lblMessage.text = message
        }
        onView.isUserInteractionEnabled = isInteractionEnabled
        
        onView.bringSubviewToFront(view)
        count = count + 1
    }
    
    @discardableResult
    static func removeLoader(fromView: UIView? = UIApplication.shared.keyWindow, count: inout Int) -> Bool {
        print("removeLoader: count is \(count)")

        guard let fromView = fromView else {
            return false
        }
        
        count = count - 1
        if count < 0 {
            //FIXME: This logic is wrong we should not have to set count  , Instead we have to proper call loader.
            count = 0
        }
        
        if count == 0 {
            fromView.isUserInteractionEnabled = true
            if let view = fromView as? UIWindow {
                view.viewWithTag(998)?.removeFromSuperview()
            }
            fromView.viewWithTag(999)?.removeFromSuperview()
        }
        
        return count == 0
    }
    
    static let loaderView: UIView = {
        let container = UIView()
        container.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        addActivityIndicator(onView: container)
        
        return container
    }()
    
    static  func addProgressView(onView : UIView , message:String?) -> IndicatorView {
        let indicator = Bundle.main.loadNibNamed(Constants.NIBNames.indicatorView, owner: nil, options: nil)?.first as! IndicatorView
//        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.tag = 999
        indicator.backgroundColor = onView.backgroundColor?.withAlphaComponent(0.6)
        indicator.baseView.backgroundColor = onView.backgroundColor?.withAlphaComponent(0.8)
        indicator.lblMessage.text = message
//        onView.addSubview(indicator)
//        NSLayoutConstraint(item: indicator, attribute: .top, relatedBy: .equal, toItem: onView, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
//        NSLayoutConstraint(item: indicator, attribute: NSLayoutAttribute.bottom, relatedBy: .equal, toItem: onView, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
//        NSLayoutConstraint(item: indicator, attribute: .leading, relatedBy: .equal, toItem: onView, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
//        NSLayoutConstraint(item: indicator, attribute: NSLayoutAttribute.trailing, relatedBy: .equal, toItem: onView, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        onView.insertSubview(subview: indicator)
        return indicator
        
    }
    
    static func addActivityIndicator(onView: UIView) {
        
        let spinnerView1: MMMaterialDesignSpinner = spinnerView
        spinnerView1.tag = 999
        spinnerView1.startAnimating()
        onView.addSubview(spinnerView1)
    }
    
    static var spinnerView: MMMaterialDesignSpinner {
        get {
            let sv: MMMaterialDesignSpinner = MMMaterialDesignSpinner(frame: CGRect(x: (UIScreen.main.bounds.width - 50)/2, y: (UIScreen.main.bounds.height - 50)/2, width: 49, height: 49))
            sv.lineWidth = 2.0
            sv.tintColor = UIColor.buttonTitleBlue
            return sv
        }

    }
    
    static func getBadgeView1(target: UIViewController? = nil , image: UIImage, notificationSelector: Selector) -> BarButtonContainer {
        let  notificationView = BarButtonContainer()
        notificationView.iconImageView.image = image
        notificationView.actionBtn.addTarget(target, action: notificationSelector, for: .touchUpInside)
        return notificationView
    }

    static func getBadgeView(image: UIImage, notificationSelector: Selector, label: UILabel) -> UIView {
        
//        let notificationView = UIImageView(image: image)        
        let notificationView = UIImageView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        notificationView.image = image
        notificationView.contentMode = .center
        notificationView.isUserInteractionEnabled = true
        
        let bgView = UIView()
        bgView.backgroundColor = .red
        let countLabel = label
        countLabel.font = DLSFont.supportingText.regular//countLabel.font.withSize(12)
        countLabel.text = ""
        countLabel.textColor = .white
        countLabel.sizeToFit()
        bgView.addSubview(countLabel)
        notificationView.addSubview(bgView)
        
        bgView.translatesAutoresizingMaskIntoConstraints = false
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let leading = NSLayoutConstraint(item: countLabel, attribute: .leading, relatedBy: .equal, toItem: bgView, attribute: .leading, multiplier: 1, constant: 5)
        let trailing = NSLayoutConstraint(item: countLabel, attribute: .trailing, relatedBy: .equal, toItem: bgView, attribute: .trailing, multiplier: 1, constant: -5)
        let top = NSLayoutConstraint(item: countLabel, attribute: .top, relatedBy: .equal, toItem: bgView, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: countLabel, attribute: .bottom, relatedBy: .equal, toItem: bgView, attribute: .bottom, multiplier: 1, constant: 0)
        
        bgView.addConstraints([leading, trailing, top, bottom])
        
        
        let bgTrailing = NSLayoutConstraint(item: bgView, attribute: .centerX, relatedBy: .equal, toItem: notificationView, attribute: .trailing, multiplier: 1, constant: 0)
        let bgTop = NSLayoutConstraint(item: bgView, attribute: .top, relatedBy: .equal, toItem: notificationView, attribute: .top, multiplier: 1, constant: -5)
        
        notificationView.addConstraints([bgTop, bgTrailing])
        
        bgView.layer.cornerRadius = 14.33 / 2.0
        bgView.layer.masksToBounds = true
        
        let notifButton = UIButton(type: .system)
        notifButton.addTarget(nil, action: notificationSelector, for: .touchUpInside)
        
        notificationView.addSubview(notifButton)
        
        notifButton.translatesAutoresizingMaskIntoConstraints = false
        let leadingNotif = NSLayoutConstraint(item: notifButton, attribute: .leading, relatedBy: .equal, toItem: notificationView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingNotif = NSLayoutConstraint(item: notifButton, attribute: .trailing, relatedBy: .equal, toItem: notificationView, attribute: .trailing, multiplier: 1, constant: 0)
        let topNotif = NSLayoutConstraint(item: notifButton, attribute: .top, relatedBy: .equal, toItem: notificationView, attribute: .top, multiplier: 1, constant: 0)
        let bottomNotif = NSLayoutConstraint(item: notifButton, attribute: .bottom, relatedBy: .equal, toItem: notificationView, attribute: .bottom, multiplier: 1, constant: 0)
        
        notificationView.addConstraints([leadingNotif, trailingNotif, topNotif, bottomNotif])
        
        return notificationView
        
//        let barView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 20 ))
//        barView.addSubview(notificationView)
//        barView.backgroundColor = .red
//        barView.translatesAutoresizingMaskIntoConstraints = false
//        let leadingC = NSLayoutConstraint(item: barView, attribute: .leading, relatedBy: .equal, toItem: notificationView, attribute: .leading, multiplier: 1, constant: 8)
//        let trailingC = NSLayoutConstraint(item: barView, attribute: .trailing, relatedBy: .equal, toItem: notificationView, attribute: .trailing, multiplier: 1, constant: -5)
//        let topC = NSLayoutConstraint(item: barView, attribute: .top, relatedBy: .equal, toItem: notificationView, attribute: .top, multiplier: 1, constant: 0)
//        let bottomC = NSLayoutConstraint(item: barView, attribute: .bottom, relatedBy: .equal, toItem: notificationView, attribute: .bottom, multiplier: 1, constant: 0)
//        notificationView.addConstraints([leadingC, trailingC, topC, bottomC])
//        return barView
    }
    
    static func getProductCategory(for productCode: String?) -> SubCategoryList? {
        return CacheManager.shared.productCategories?.filter{ $0.subCategoryCode == productCode }.first
    }
    
    static func callCustomerCare() {
        makeCall(to: Constants.AppConstants.contactNumber)
    }
    
    static func makeCall(to number: String) {
        if let url = URL(string: "tel://\(number)") {
            sharedAppDelegate?.isShowBlurView = false
            openCustomURL(url: url)
        }
    }
    
    class func openGoogleMapsDirectionsUrl(from source : CLLocationCoordinate2D, to destination : CLLocationCoordinate2D) {
        if let url = URL(string: getGoogleMapsDirectionsUrl(from: source, to: destination)) {
            openCustomURL(url: url)
        }
    }
    
    static func openGoogleMaps(latitude: Double, longitude: Double) {
        
        if let url = URL(string: "https://www.google.com/maps/search/?api=1&query=\(latitude),\(longitude)") {
            openCustomURL(url: url)
        }
    }
    
    static func openCustomURL(url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            print("Not able to open \(url)")
        }
    }
    
    public static func jailbroken(application: UIApplication) -> Bool {
        guard let cydiaUrlScheme = URL(string: "cydia://package/com.example.package") else { return isJailbroken() }
        return application.canOpenURL(cydiaUrlScheme) || isJailbroken()
    }
    
    
    private static func isJailbroken() -> Bool {
        
        if Platform.isSimulator {
            return false
        }
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: "/Applications/Cydia.app") ||
            fileManager.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            fileManager.fileExists(atPath: "/bin/bash") ||
            fileManager.fileExists(atPath: "/usr/sbin/sshd") ||
            fileManager.fileExists(atPath: "/etc/apt") ||
            fileManager.fileExists(atPath: "/usr/bin/ssh") {
            return true
        }
        
        if canOpen("/Applications/Cydia.app") ||
            canOpen("/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            canOpen("/bin/bash") ||
            canOpen("/usr/sbin/sshd") ||
            canOpen("/etc/apt") ||
            canOpen("/usr/bin/ssh") {
            return true
        }
        
        let path = "/private/" + NSUUID().uuidString
        do {
            try "anyString".write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
            try fileManager.removeItem(atPath: path)
            return true
        } catch {
            return false
        }
    }
    
    static func canOpen(_ path: String) -> Bool {
        let file = fopen(path, "r")
        guard file != nil else { return false }
        fclose(file)
        return true
    }
    
    class func getGoogleMapsDirectionsUrl(from source : CLLocationCoordinate2D, to destination : CLLocationCoordinate2D) -> String {
        return "http://maps.google.com/maps?f=d&saddr=" + "\(source.latitude)" + "," + "\(source.longitude)" + "&daddr=" + "\(destination.latitude)" + "," + "\(destination.longitude)" + "&nav=1"
    }
    
    static func getOfferDetailShareText() -> NSAttributedString {
        
        let shareText = "\(Strings.OfferDetail.shareText) \(Strings.Global.appDownloadUrlString)"
        if let range = shareText.range(of: Strings.Global.appDownloadUrlString) {
            let atrStr = NSAttributedString(string: shareText, attributes: [NSAttributedString.Key.link: range])
            return atrStr
        }
        
        return NSAttributedString()
    }
    
    static func isOfferEndingSoon(dateString: String?, expiryInDays : NSNumber?) -> Bool {
        
        if let dayComp = expiryInDays?.intValue, dayComp <= 5 {
            return true
        }
        return false
    }
    
    static func getOfferListDateText(dateString: String?, expiryInDays : NSNumber?) -> String? {
        
        var strDate: String?
        
        let date = Date(string: dateString, formatter: DateFormatter.offerDateFormat)
        
        let currentDateString = Date.currentDate().string(with : DateFormatter.offerDateFormat)
        let currentDate = Date(string: currentDateString, formatter: DateFormatter.offerDateFormat)
        
        if let date = date {
            let diff = gregorianCalendar.dateComponents([.day], from: currentDate!, to: date)
            
            if let dayComp = (expiryInDays?.intValue ?? diff.day) {
                
                switch dayComp {
                case 0:
                    strDate = Strings.AllOffersScene.endToday
                case 1:
                    strDate = Strings.AllOffersScene.endTomorrow
                case 2...5:
                    strDate = "\(Strings.AllOffersScene.endingIn) \(dayComp) days"
                default:
                    strDate = "\(Strings.AllOffersScene.endsOn) \(date.string(with: DateFormatter.dayMonth))"
                }
            }
        }
        return strDate
    }
    
    class func showShareActionSheet(viewController : UIViewController, dataToShare : [Any]) {
        
        let shareActivityController = UIActivityViewController(activityItems: dataToShare, applicationActivities: nil)
        shareActivityController.excludedActivityTypes = [.airDrop, .print, .assignToContact, .saveToCameraRoll, .addToReadingList, .postToFlickr, .postToVimeo]
        shareActivityController.popoverPresentationController?.sourceView = viewController.view // so that iPads won't crash
        viewController.presentInFullScreen(shareActivityController, animated: true, completion: nil)
    }
    
    static func getCurrencyString(valueString: String) -> String {
        
        guard let value = Double(valueString) else {
            return ""
        }
        
        if value > 100000 {
            
            let roundedValue = round((value / 100000) * 10) / 10
            return "\(roundedValue) Lacs"
            
        } else {
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale.current
            if let formattedAmount = formatter.string(from: value as NSNumber) {
                return formattedAmount
            } else {
                return ""
            }
        }
    }
    
    static func getWhcTask(tasks: [Task]?) -> Task? {
        
        if tasks != nil {
            for task in tasks! {
                if task.name == "INSPECTION" {
                    return task
                }
            }
        }
        return nil
    }
    
    static func getProduct(productCode: String) -> HomeApplianceSubCategories? {
        
        var categories: [HomeApplianceSubCategories] = []
        
        let responseStr = [RemoteConfigManager.shared.homeAppliancesTopCategories, RemoteConfigManager.shared.homeAppliancesOtherCategories, RemoteConfigManager.shared.personalElectronicsCategories]
        
        for str in responseStr {
            let data = str.data(using: .utf8)
            do {
                if let data = data {
                    let parsedResponse: HomeApplianceCategoryResponseDTO = try JSONParserSwift.parse(data: data)
                    let list = parsedResponse.subCategories ?? []
                    categories.append(contentsOf: list)
                }
            } catch {
                print(error)
            }
        }
        return categories.filter({$0.subCategoryCode == productCode}).first
    }
    
    static func offersClickedCount() -> Int {
        var count = 0
        if let offerDetailsCount: String = UserDefaultsKeys.offerDetailsCount.get(), let offersCount = Int(offerDetailsCount) {
            count = offersCount
        }
        
        count += 1
        UserDefaultsKeys.offerDetailsCount.set(value: String(count))
        return count
    }
    
    static func offersFavoritedCount() -> Int {
        var count = 0
        if let offersFavoritedCount: String = UserDefaultsKeys.offerFavoriteCount.get(), let offersCount = Int(offersFavoritedCount) {
            count = offersCount
        }
        
        count += 1
        UserDefaultsKeys.offerFavoriteCount.set(value: String(count))
        return count
    }
    
    @discardableResult static func cardOpenCount() -> Int {
        var count = 0
        if let cardOpenCount: String = UserDefaultsKeys.cardOpenCount.get(), let openCount = Int(cardOpenCount) {
            count = openCount
        }
        
        count += 1
        UserDefaultsKeys.cardOpenCount.set(value: String(count))
        return count
    }
    
    @objc static func checkForRatingPopUp(viewController:UIViewController,isShowForceFully: Bool = false, _ rateUsCompletion: (()->())? = nil) {
        if let location: String = UserDefaultsKeys.Location.get(), !location.isEmpty {
            if let ratingDate: Date = UserDefaultsKeys.ratingSubmittedDate.get(), Date().isSameDayAs(ratingDate), !isShowForceFully {
                // Do not ask for rating if ratingSubmittedDate is set and is same day as current day
            } else {
                UserDefaultsKeys.ratingSubmittedDate.set(value: Date())
                EventTracking.shared.eventTracking(name: .ratingPopUp, [.location: location])
                viewController.showAlert(title: Strings.Alert.doBetter ,message: Strings.Alert.howWeWorking, primaryButtonTitle: Event.EventParams.awesome, Event.EventParams.notGood, primaryAction: {
                    if let rateUsCompletion = rateUsCompletion {
                        rateUsCompletion()
                    }
                    Utilities.openReview()
                    EventTracking.shared.eventTracking(name: .rateUs ,[.location: location, .feedback:Event.EventParams.awesome])
                }) {
                    EventTracking.shared.eventTracking(name: .rateUs ,[.location: location, .feedback:Event.EventParams.notGood])
                    viewController.showAlert(title: Strings.Alert.feedbackTitle, message: Strings.Alert.feedbackMessage, withFeedback: true, completion: {
                        (cancel) in
                        if cancel {
                            EventTracking.shared.eventTracking(name: .ratingfeedback ,[.location: location, .comment:"", .value:Event.EventParams.cancel])
                            UserDefaultsKeys.Value.set(value: Event.EventParams.cancel)
                        } else {
                            let comment:String? = UserDefaultsKeys.comment.get()
                            EventTracking.shared.eventTracking(name: .ratingfeedback ,[.location: location, .comment:comment ?? "", .value:Event.EventParams.submit])
                            UserDefaultsKeys.Value.set(value: Event.EventParams.submit)
                        }
                    })
                }
            }
            UserDefaultsKeys.Location.set(value: "")
        }
    }
    
    static func openReview() {
        SKStoreReviewController.requestReview()
    }
    
    static func getImageStrForProductName(_ name: String, applianceCategories: [HomeApplianceSubCategories]) -> String? {
        let filteredSubCat = applianceCategories.filter { (subCat) -> Bool in
            return subCat.subCategoryCode == name
        }
        return filteredSubCat.first?.subCatIconUrl
    }
    
    //    static func appOpened() -> Int {
    //        let number = UserDefaults.standard.integer(forKey: "appOpenedCounter")
    //        UserDefaults.standard.set(number + 1, forKey: "appOpenedCounter")
    //        UserDefaults.standard.synchronize()
    //        return number
    //    }
    
    static var isServiceTabNew: Bool {
        // TODO:- UNCOMMENT FOR SD TAB
        get {
            let key : String? = UserDefaultsKeys.User_Mobile_No.get()
            return UserDefaults.standard.bool(forKey: "isHomeNew_User\(key ?? "")") == false
        }
        set {
            let key : String? = UserDefaultsKeys.User_Mobile_No.get()
            
            UserDefaults.standard.set(newValue, forKey: "isHomeNew_User\(key ?? "")")
            UserDefaults.standard.synchronize()
        }
    }
    static var isAddressTextFieldNew: Bool {
        get {
            let key : String? = UserDefaultsKeys.User_Mobile_No.get()
            return UserDefaults.standard.bool(forKey: "isAddressTextField\(key ?? "")") == false
        }
        set {
            let key : String? = UserDefaultsKeys.User_Mobile_No.get()
            
            UserDefaults.standard.set(newValue, forKey: "isAddressTextField\(key ?? "")")
            UserDefaults.standard.synchronize()
        }
    }
    
    static func getLocationFetchingTimes() ->HomeApplianceSubCategories{
        return HomeApplianceSubCategories(dictionary: [:])
    }
    
    // MARK:- Localytics Profile methods    
    static func setWebEngageUser(mobileNumber: String){
        let weUser: WEGUser = WebEngage.sharedInstance().user
        weUser.login(mobileNumber)
        Crashlytics.crashlytics().setUserID(mobileNumber)
    }
    
    static func setFireBaseUserId(customerId: String?){
        Analytics.setUserID(customerId)
    }
    
    static func setAppsFlyerUser(customerId: String?){
        AppsFlyerLib.shared().customerUserID = customerId
    }
    
    static func set(name : String?, email : String?, phone : String?) {
        
        let weUser: WEGUser = WebEngage.sharedInstance().user
        weUser.setFirstName(name ?? "")
        weUser.setEmail(email ?? "")
        weUser.setPhone(phone ?? "")
        Crashlytics.crashlytics().setUserID(phone ?? "")
    }
    
    static func set(dateOfBirth: String) {
        let weUser: WEGUser = WebEngage.sharedInstance().user
        weUser.setBirthDateString(dateOfBirth)
    }
    
    static func presentAlertController(vc: UIViewController, completionBlock: @escaping (UIImage?, Error?) -> Void ) {
        
        // Action sheet
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: Strings.Global.takePicture, style: .default) { (action) in
            ImagePickerManager.shared.pickImageFromCamera(editing: true, handler: {(image, error) in
                completionBlock(image, error)
            })
        }
        let action2 = UIAlertAction(title: Strings.Global.selectPicture, style: .default) { (action2) in
            ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: true, handler: {(image, error) in
                completionBlock(image, error)
            })
        }
        let action3 = UIAlertAction(title: Strings.Global.cancel, style: .cancel) { (action3) in
            vc.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        alertController.addAction(action2)
        alertController.addAction(action3)
        vc.presentInFullScreen(alertController, animated: true, completion: nil)
    }
    
    static func checkError(_ response: BaseResponseDTO?, error: Error?) throws {
        if let error = error {
            throw LogicalError(error.localizedDescription)
        } else if response?.status?.lowercased() == Constants.ResponseConstants.success {
        } else if let error = response?.error, (response?.status?.lowercased() == Constants.ResponseConstants.failure || response?.status?.lowercased() == Constants.ResponseConstants.failed) {
            if let errorObj = error.first {
                if let message = executeCommonCodeForErrorCode(errorCode: errorObj.errorCode) {
                    throw LogicalError(message,code:errorObj.errorCode)
                }else if let errorMsg = errorObj.errorMessage {
                    throw LogicalError(errorMsg,code:errorObj.errorCode)
                } else {
                    throw LogicalError(errorObj.message ?? "",code:errorObj.errorCode)
                }
            } else {
                throw LogicalError(Strings.Global.somethingWrong)
            }
        } else if let invalidData = response?.invalidData, (response?.status?.lowercased() == Constants.ResponseConstants.failure || response?.status?.lowercased() == Constants.ResponseConstants.failed) {
            if let errorObj = invalidData.first {
                if let message = executeCommonCodeForErrorCode(errorCode: errorObj.errorCode) {
                    throw LogicalError(message,code:errorObj.errorCode)
                }else if let errorMsg = errorObj.errorMessage {
                    throw LogicalError(errorMsg,code:errorObj.errorCode)
                }else {
                    throw LogicalError(errorObj.message ?? "",code:errorObj.errorCode)
                }
            } else {
                throw LogicalError(Strings.Global.somethingWrong)
            }
        }else if let message = response?.responseMessage {
            throw LogicalError(message)
        } else if response?.status == Constants.ResponseConstants.failed {
            throw LogicalError(response?.message ?? "")
        } else if response?.status?.lowercased() == Constants.ResponseConstants.failure {
            throw LogicalError(response?.message ?? "")
        } else {
            throw LogicalError(Strings.Global.somethingWrong)
        }
    }
    
    static func executeCommonCodeForErrorCode(errorCode : String?) -> String? {
        
        if let errorCode = errorCode {
            if errorCode == "GATEWAY_003" || errorCode == "AUTH_005" || errorCode == "AUTH_006" {
                forceUserLogout()
                return Strings.Global.authErrorMessage
            }
        }
        return nil
    }
    
    static func logoutAndDeleteDB() {
        //silent logout from remote
        let custType: String? = UserDefaultsKeys.custType.get()
        
        if custType != nil {
           _ = LogoutRequestUseCase.service(requestDTO: LogoutRequestDTO(), completionBlock: { (_, _, _) in })
        }
        
        DataDiskStogare.deletefile(fileName: questionFilename)
        Crashlytics.crashlytics().setUserID("LogoutUser_unknow")
        
        for (key, _) in UserDefaults.standard.dictionaryRepresentation(){
            if key.hasPrefix("isHomeNew_User") || key.hasPrefix("isAddressTextField") || key.hasSuffix("Environment") || key.hasSuffix("mhcCompletionStatus") || key.hasSuffix("mhcUpdateDate") || key.hasSuffix("mhcScore") || key.hasSuffix("useNodeServer") || key.hasSuffix("nodeServerIP"){
                continue;
            }
            UserDefaults.standard.setValue(nil, forKey: key)
            UserDefaults.standard.synchronize()
            
        }
        CacheManager.clearCache()
        FDCacheManager.clearCache()
        UserDefaultsKeys.isShowTabBarTutorial.set(value: true)
        UserDefaultsKeys.isShowWelcomeMessage.set(value: true)
        UserDefaultsKeys.showTutorial.set(value: "N")
        UserDefaults.standard.synchronize()
        WebEngage.sharedInstance().user.logout()
        Utilities.setAppsFlyerUser(customerId: nil)
    }
    
    static func forceUserLogout(_ mobileNumber: String = "", showBackActionInLogin: Bool = false) {
        //silent logout from remote
        _ = LogoutRequestUseCase.service(requestDTO: LogoutRequestDTO(), completionBlock: { (_, _, _) in })
        
        Utilities.clearDataAndLogout(mobileNumber, showBackActionInLogin: showBackActionInLogin)

        let weUser: WEGUser = WebEngage.sharedInstance().user
        weUser.logout()
    }
    
    static func getResponseFilter() -> ResponseFilter? {
        return UIApplication.shared.delegate as? ResponseFilter
    }
    
    static  func showAlert(title: String? = nil, message: String , onViewController:UIViewController?) {
        // not used
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okayAction)
        onViewController?.presentInFullScreen(alert, animated: true, completion: nil)
    }
    static func clearDataAndLogout(_ mobileNumber: String = "", showBackActionInLogin: Bool = false) {
        DataDiskStogare.deletefile(fileName: questionFilename)
        ChatBridge.logout()
        let userIdentifier = "LogoutUser_" + (!userMobileNumber.isEmpty ? userMobileNumber : "unknow")
        Crashlytics.crashlytics().setUserID(userIdentifier)
        
        if let window = sharedAppDelegate?.window {
            window.rootViewController?.dismiss(animated: false, completion: nil)
            window.isUserInteractionEnabled = true
            CoreDataStack.sharedStack.deleteEntities()
            
            for (key, _) in UserDefaults.standard.dictionaryRepresentation(){
                if key.hasPrefix("isHomeNew_User") || key.hasPrefix("isAddressTextField") || key.hasSuffix("Environment") || key.hasSuffix("mhcCompletionStatus") || key.hasSuffix("mhcUpdateDate") || key.hasSuffix("mhcScore") || key.hasSuffix("useNodeServer") || key.hasSuffix("nodeServerIP"){
                    continue;
                }
                UserDefaults.standard.setValue(nil, forKey: key)
                UserDefaults.standard.synchronize()
                
            }
            CacheManager.clearCache()
            FDCacheManager.clearCache()
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue)
            UserDefaultsKeys.showTutorial.set(value: "N")
        
            UserDefaults.standard.synchronize()
            Utilities.setAppsFlyerUser(customerId: nil)
            AppUserDefaults.logoutSuccess = true
            let vc = LoginMobileVC()
            vc.isRootToWindow = true
            vc.showBackAction = showBackActionInLogin
            vc.mobileNumber = mobileNumber
            ChatBridge.setRootWithVC(vc)
        }
    }
    
    static func clearDataOnly() {
        DataDiskStogare.deletefile(fileName: questionFilename)
        ChatBridge.logout()
        let userIdentifier = "LogoutUser_" + (!userMobileNumber.isEmpty ? userMobileNumber : "unknow")
        Crashlytics.crashlytics().setUserID(userIdentifier)
        
        if let window = UIApplication.shared.delegate?.window {
            window?.rootViewController?.dismiss(animated: false, completion: nil)
            window?.isUserInteractionEnabled = true
            CoreDataStack.sharedStack.deleteEntities()
            
            for (key, _) in UserDefaults.standard.dictionaryRepresentation(){
                if key.hasPrefix("isHomeNew_User") || key.hasPrefix("isAddressTextField") || key.hasSuffix("Environment") || key.hasSuffix("mhcCompletionStatus") || key.hasSuffix("mhcUpdateDate") || key.hasSuffix("mhcScore") || key.hasSuffix("useNodeServer") || key.hasSuffix("nodeServerIP"){
                    continue;
                }
                UserDefaults.standard.setValue(nil, forKey: key)
                UserDefaults.standard.synchronize()
            }
            CacheManager.clearCache()
            FDCacheManager.clearCache()
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowTabBarTutorial.rawValue)
            UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isShowWelcomeMessage.rawValue)
            UserDefaultsKeys.showTutorial.set(value: "N")
            UserDefaults.standard.synchronize()
            Utilities.setAppsFlyerUser(customerId: nil)
            AppUserDefaults.logoutSuccess = true
        }
    }
    
    static func openAppSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            openCustomURL(url: url)
        }
    }
    
    static func showAlertForSetting(on vc: UIViewController? = nil, title: String? = nil, message: String, secondaryButtonTitle: String? = nil, secondAction: (()->Void)? = nil, primaryAction: @escaping () -> Void) {
        vc?.showAlert(title: title, message: message, primaryButtonTitle: MhcStrings.Buttons.settings, secondaryButtonTitle: secondaryButtonTitle ?? MhcStrings.Buttons.skip, isCrossEnable: false, primaryAction: {
            primaryAction()
            Utilities.openAppSettings()
        }, secondaryAction: secondAction)
    }
    
    static func showSettingsAlert(on vc: UIViewController? = nil, title: String? = nil, message: String, secondaryButtonTitle: String? = nil, secondAction: (()->Void)? = nil, primaryAction: @escaping () -> Void) {
        vc?.showAlert(title: title ?? "", message: message, primaryButtonTitle: MhcStrings.Buttons.settings, nil, isCrossEnable: false, logoImage: nil, primaryAction: {
            primaryAction()
            Utilities.openAppSettings()
        }, nil)
    }
    
    static func showTip(text: String, direction: PopTipDirection, in view: UIView, from source: CGRect, margin: CGFloat = 16) {
        tipTool(with: margin).show(text: text, direction: direction, maxWidth: UIScreen.main.bounds.width - 30, in: view, from: source)
    }
    
    static func isAddedToCalendar(srId: String, scheduleStartDate: String) -> Bool {
        return UserDefaults.standard.bool(forKey: "calendarEvent\(srId)\(scheduleStartDate)")
    }
    
    static func setCalendarEvent(srId: String, scheduleStartDate: String) {
        UserDefaults.standard.set(true, forKey: "calendarEvent\(srId)\(scheduleStartDate)")
        UserDefaults.standard.synchronize()
    }
    
    static func getStageDescription(service: SearchService?) -> String? {
        
        guard let stage = Constants.TimelineStage(rawValue: service?.workflowStage ?? "") else {
            return nil
        }
        
        //        Document upload DU
        //        Verification VR
        //        Ic decision ID
        //        Completed CO
        //        Claim settlement CS
        //        Visit VE
        //        Ic doc ICD
        //        Repair assessment RA
        
        switch stage {
        case .documentUpload:
            return service?.workflowData?.documentUpload?.stageDescription
        case .visit:
            return service?.workflowData?.visit?.stageDescription
        case .repairAssessment:
            return service?.workflowData?.repairAssessment?.stageDescription
        case .completed:
            return service?.workflowData?.completed?.stageDescription
        case .verification:
            return service?.workflowData?.verification?.stageDescription
        case .insuranceDecision:
            return service?.workflowData?.insuranceDecision?.stageDescription
        case .deductablePayment:
            break
        case .scheduled:
            break
        case .claimSettlement:
            return service?.workflowData?.claimSettlement?.stageDescription
        case .partnerStageStatus:
            break
        case .repair:
            break
        case .softApproval:
            return service?.workflowData?.softApproval?.stageDescription
        case .icDoc:
            return service?.workflowData?.icDoc?.stageDescription
        case .pickUp:
            return service?.workflowData?.pickup?.stageDescription
        case .deviceRepair:
            return service?.workflowData?.repair?.stageDescription
        case .delivery:
            return service?.workflowData?.delivery?.stageDescription
        }
        return nil
    }
    
    static func isSOPServiceAvailable(from services: [ServiceDescription]?) -> Bool {
        return services?.contains(where: { $0.service == .breakdown && $0.isInsuranceBacked == false }) ?? false
    }
    
    static func noOfServicesLeft(from services: [ServiceDescription]?, for serviceType: Constants.Services) -> NSNumber? {
        if let service = services?.filter({ $0.service == serviceType}).first {
            return service.noOfVisitsLeft
        }
        return nil
    }
    
    static func isSOPServiceAvailable(membership: CustomerMembershipDetails?) -> Bool {
        return membership?.plan?.services?.contains(where: { (service) -> Bool in
            return (service.serviceName == Constants.Services.breakdown.rawValue) && service.insuranceBacked == false}) ?? false
    }
    
    static func getSOPPlanName(_ membership: CustomerMembershipDetails) -> String? {
        if isSOPServiceAvailable(membership: membership) {
            var countStr = ""
            if let count = membership.plan?.allowedMaxQuantity {
                if Int(truncating: count) < Constants.unlimitedApplianceCount {
                    countStr = Int(truncating: count) > 1 ? "- Upto  \(count) appliances covered" : "- \(count) appliance covered"
                } else {
                    countStr = "- Unlimited appliances covered"
                }
            }
            return "\(Strings.HATabScene.Whc.gadgetServ) \(countStr)"
        }
        return nil
    }
    
    static func getClaimType(service: SearchService) -> Constants.Services {
        if service.workflowData?.visit?.isSelfService == "Y" {
            return .selfRepair
        } else {
            return Constants.Services(rawValue: service.serviceRequestType ?? "") ?? .extendedWarranty
        }
    }
    
    static func isSrTypePE(value: String?) -> Bool {
        
        if let type = Constants.Services.init(rawValue: value ?? ""), ([.peADLD, .peTheft, .peExtendedWarranty].contains(type)) {
            return true
        }
        return false
    }
    
    static func isSrTypeBuyback(value: String?) -> Bool {
        
        if let type = Constants.Services.init(rawValue: value ?? ""), ([.buyback].contains(type)) {
            return true
        }
        return false
    }
    
    static func getArrayofStages(fromModel :SearchService? , stageArray:[Constants.TimelineStage])-> [BaseWorkFlowStage]{
        var arrStage = [BaseWorkFlowStage]()
        for stage in stageArray {
            let baseWorkFlowStage = Utilities.getStageModel(fromModel: fromModel, stage: stage)
            if let baseWorkFlowStage = baseWorkFlowStage {
                arrStage.append(baseWorkFlowStage)
            }
        }
        return arrStage
    }
    static func getStageModel(fromModel :SearchService? , stage : Constants.TimelineStage) -> BaseWorkFlowStage? {
        var baseWorkFlowStage:BaseWorkFlowStage?
        if let stageData = fromModel?.workflowData {
            switch stage {
            case .documentUpload:
                baseWorkFlowStage = stageData.documentUpload
            case .verification:
                baseWorkFlowStage = stageData.verification
            case .pickUp:
                baseWorkFlowStage = stageData.pickup
            case .repairAssessment:
                baseWorkFlowStage = stageData.repairAssessment
            case .icDoc:
                baseWorkFlowStage = stageData.icDoc
            case .insuranceDecision:
                baseWorkFlowStage = stageData.insuranceDecision
            case .repair:
                baseWorkFlowStage = stageData.repair
            case .delivery:
                baseWorkFlowStage = stageData.delivery
            case .claimSettlement:
                baseWorkFlowStage = stageData.claimSettlement
            case .completed:
                baseWorkFlowStage = stageData.completed
            default:
                break
            }
        }
        return baseWorkFlowStage
    }
    
    static func isImageDataApt(_ dta: Data) -> Error? {
        if (Double(dta.count) / Double(1024 * 1024)) > 5 {
            return LogicalError.init("Please upload the valid format of the document within permissible size of 5 MB")
        }
        return nil
    }
    
    static func isImageDataAceptableForFD(_ dta: Data, _ fileName: String?) -> Error? {
        if (Double(dta.count) / Double(1024 * 1024)) > 10 {
            return LogicalError.init(Strings.iOSFraudDetection.uploadErrorMessage)
        }else {
            if let imagename = fileName {
                if imagename.isEmpty {
                    return nil
                }
                // Add here your image formats.
                let imageFormats = ["jpg", "jpeg", "png", "gif", "bmp", "tif", "zip", "rar", "docx", "pdf"]
                
                let ext = (imagename as NSString).pathExtension
                if ext.isEmpty {
                    return nil
                    
                }else {
                    
                    if !imageFormats.contains(ext.lowercased()) {
                        return LogicalError.init(Strings.iOSFraudDetection.uploadErrorMessage)
                    }
                }
            }
            return nil
        }
    }
    
    static func presentPopover(view:UIView, height:CGFloat){
        let basePickerVC = BasePickerVC(withView:view, height:height)
        topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    static func presentPopover(view:UIView, height:CGFloat, dismissCompletion: @escaping (()->())){
        let basePickerVC = BasePickerVC(withView:view, height:height)
        basePickerVC.dismissCallback = dismissCompletion
        topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    static func presentPopoverWithOutGesture(view:UIView, height:CGFloat){
        let basePickerVC = BasePickerVC(withView:view, height:height)
        basePickerVC.isGestureEnable = false
        topMostPresenterViewController.present(basePickerVC, animated: true)
    }
    
    static var topMostPresenterViewController:UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
    static func getPresentedVCInStack<T: UIViewController>() -> T? {
        var viewController: UIViewController? = Utilities.topMostPresenterViewController
        while !(viewController is T) && viewController?.presentingViewController != nil {
            viewController = viewController?.presentingViewController
        }
        guard let castedViewController = viewController as? T else {
            return nil
        }
        return castedViewController
    }
    
    static func dismissAllPresentedVC(completionHandler: @escaping (()->())) {
        var topController: UIViewController? = UIApplication.shared.keyWindow!.rootViewController!
        while (topController?.presentedViewController != nil) {
            topController = topController?.presentedViewController!
        }
        dismissAll(viewController: topController, completionHandler: completionHandler)
    }
    
    // this variable is used to check if all present vcs are dismissed call completion handler
    static func dismissAll(viewController: UIViewController?, completionHandler: @escaping (()->())) {
        if let viewController = viewController, (viewController.presentingViewController != nil) {
            let presentingVC = viewController.presentingViewController
            viewController.dismiss(animated: false) {
                dismissAll(viewController: presentingVC, completionHandler: completionHandler)
                if presentingVC?.presentingViewController == nil {
                    completionHandler()
                }
            }
        }
    }
    
    static var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
//    // Screen height.
//    static var screenHeight: CGFloat {
//        return UIScreen.main.bounds.height
//    }

    //
    static var datePickerHeight: CGFloat {
        return min(UIScreen.main.bounds.height * 0.5, 350.0)
    }
    
    static func updateMHCDataOnServer() {
        if MHCConstants.isDataUpdateOnServerPending {
            if let data = try? JSONEncoder().encode(MHCResult.shared), let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any] {
                let request = MHCDataAddUpdateRequestDTO(data: json)
                MHCDataAddUpdateUseCase.service(requestDTO: request) { (_, response, error) in
                    if error == nil {
                        MHCConstants.isDataUpdateOnServerPending = false
                        MHCResult.shared.update = true
                        MHCResult.save()
                        print(response?.message ?? "")
                        print(error?.localizedDescription ?? "")
                    }
                }
            }
        }
    }
    
    static func checkPhysicalHomeButton()-> Bool {
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return (bottomSafeArea > 0)
    }
    
    static func getAllMemberships()->(memberships: [CustomerMembershipDetails], pendingMemberships: [CustomerPendingMembershipDetails]){
        if let tabVC = rootTabVC {
            return (tabVC.customerMembershipResponse?.data?.memberships ?? [], tabVC.customerMembershipResponse?.data?.pendingMemberships ?? [])
        }
        return ([],[])
    }
    
    static func openScratchCard(state: RewardStates,_ completion: (() -> ())?) {
        let vc =  ScratchAndWinVC()
        let topNavVC = ((Utilities.topMostPresenterViewController as? BaseNavigationController))
    
        if let _ = topNavVC?.topViewController as? MyRewardsVC {
            vc.fromScreen = .myReward
        } else if let tabVC = topNavVC?.topViewController as? TabVC {
            switch tabVC.selectedIndex {
            case .account:
                vc.fromScreen = .fromAccount
            case .newHome:
                vc.fromScreen = .fromHome
            default:
                break
            }
        }

        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = .overCurrentContext;
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.88)
        vc.state = state
        if let completion = completion {
            vc.dismissTapped = completion
        }
        Utilities.topMostPresenterViewController.present(vc, animated: true, completion: nil)
    }
    
    static func isCustomerVerified()->Bool {
        let custType: String? = UserDefaultsKeys.custType.get()
    
        if custType != CustType.notVerified || UserCoreDataStore.currentUser != nil {
            return true
            
        }else {
            return false
        }
    }
    
    static func isCustomerLoggedoutAndNotSkipped() -> Bool {
        let custType: String? = UserDefaultsKeys.custType.get()
        return (custType == nil)
    }
    
    static func getModuleSequence() -> ModuleFlowSequencing? {
        let moduleValue = RemoteConfigManager.shared.moduleflowsequencing
        if let moduleData = moduleValue.data(using: .utf8) {
            do {
                let response: ModuleFlowSequencing? = try JSONParserSwift.parse(data: moduleData)
                return response
                
            }catch {
                return nil
            }
        }
        return nil
    }
    
    /*
     * this method will extract all services from plans array and store all service in set after that join all services
     * name with ,(comma) and words all services name's will be seprated with _(underscore) e.g HA_BD,PMS,DARK_WEB_MONITORING etc.
     * final generated string would look like :- HA|HA_BD,PMS
     */
    static func getProductAndServicesString(plans: [PlanCD?]? = nil) -> String {
        var services:Set<String> = Set<String>()
        let _ = plans?.compactMap({services = services.union($0?.servicesListSet() ?? Set<String>())})
        let servicesString = services.compactMap({$0.replacingOccurrences(of: " ", with: "_")}).joined(separator: ",")
        return [Destination.whc.rawValue, servicesString].joined(separator: "|")
    }
    
    static func checkCopyIMEIValue() {
        if let copyContent = UIPasteboard.general.string, copyContent.containsNumbersWithSpace() {
            let trimmedString = copyContent.components(separatedBy: .whitespacesAndNewlines).joined()
            if trimmedString.luhnCheckForValidImei() {
                UIPasteboard.general.string = trimmedString
            }
        }
    }
    
    static func downloadJavascriptFile() {
        let timestamp = "\(Date().timeIntervalSince1970)".replacingOccurrences(of: ".", with: "")
        
        let timelineJSURL = Constants.WHC.URL.getTimeLineJSURL
        let timelineJSURLWithRandomParam = timelineJSURL + timestamp
        
        if RemoteConfigManager.shared.forceUpdateJS {
            JavaScriptJsonHandler.downloadJSFile(from: timelineJSURLWithRandomParam)
        } else {
            // for download js file after 24(firbaseTime) hour no mater value is true or false in firbase
            if let savedTimeInSec = UserDefaults.standard.value(forKey: UserDefaultsKeys.jsDownloadTime.rawValue) as? Int64,let currentTime = Date().toSecond(),let  firbaseTime = Int64(RemoteConfigManager.shared.javascriptUpdateInterval) {
                if currentTime - savedTimeInSec > firbaseTime*3600 {
                    JavaScriptJsonHandler.downloadJSFile(from: timelineJSURLWithRandomParam)
                    UserDefaults.standard.set(currentTime, forKey: UserDefaultsKeys.jsDownloadTime.rawValue)
                }
            } else {
                //app install first time
                let time = Date().toSecond()
                UserDefaults.standard.set(time, forKey: UserDefaultsKeys.jsDownloadTime.rawValue)
                JavaScriptJsonHandler.downloadJSFile(from: timelineJSURLWithRandomParam)
            }
        }
    }
}


/// if an Enum confirms to following protocol, it's total number of cases will be  <Enum>.caseCount
protocol CaseCountable {
    static var caseCount: Int { get }
}

extension CaseCountable where Self: RawRepresentable, Self.RawValue == Int {
    internal static var caseCount: Int {
        var count = 0
        while let _ = Self(rawValue: count) {
            count += 1
        }
        return count
    }
}

extension Utilities {
    public static func visionImageOrientation(
        from imageOrientation: UIImage.Orientation
        ) -> VisionDetectorImageOrientation {
        switch imageOrientation {
        case .up:
            return .topLeft
        case .down:
            return .bottomRight
        case .left:
            return .leftBottom
        case .right:
            return .rightTop
        case .upMirrored:
            return .topRight
        case .downMirrored:
            return .bottomLeft
        case .leftMirrored:
            return .leftTop
        case .rightMirrored:
            return .rightBottom
        }
    }
}
