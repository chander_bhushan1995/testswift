//
//  OAKeychain.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 14/03/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import Foundation

enum OAKeychainKeys: String {
    case deviceId = "DEVICEUUID"
}

open class OAKeychain {
    private static var _sharedInstance: OAKeychain?
    public static var sharedInstance: OAKeychain {
        get {
            if _sharedInstance == nil {
                DispatchQueue.global().sync(flags: .barrier) {
                    _sharedInstance = OAKeychain()
                }
            }
            return _sharedInstance!
        }
    }
    private init() {}
    
    /*
     Only use subscript to save and retrive data from keychain
 */
    open subscript(key: String) -> String? {
        get {
            return get(withKey: key)
        } set {
            DispatchQueue.global().sync(flags: .barrier) {
                self.set(newValue, forKey: key)
            }
        }
    }
    
    /*
      This method will save the data
      Update the data
      Remove the data if you will set nil of given key
 */
    private func set(_ string: String?, forKey key: String) {
        let query = oaKeychainQuery(withKey: key)
        let objectData: Data? = string?.data(using: .utf8, allowLossyConversion: false)
        
        if SecItemCopyMatching(query, nil) == noErr { // if value is available
            if let data = objectData { // update key
                let status = SecItemUpdate(query, NSDictionary(dictionary: [kSecValueData: data]))
                print("Update status: ", status)
            } else { // if set nil in given value
                let status = SecItemDelete(query) // remove key
                print("Remove status: ", status)
            }
        } else {
            if let data = objectData { // save value
                query.setValue(data, forKey: kSecValueData as String)
                let status = SecItemAdd(query, nil)
                print("Save status: ", status)
            }
        }
    }
    
    private func get(withKey key: String) -> String? {
        let query = oaKeychainQuery(withKey: key)
        query.setValue(kCFBooleanTrue, forKey: kSecReturnData as String)
        query.setValue(kCFBooleanTrue, forKey: kSecReturnAttributes as String)
        
        var result: CFTypeRef?
        let status = SecItemCopyMatching(query, &result)
        
        guard
            let resultsDict = result as? NSDictionary,
            let resultsData = resultsDict.value(forKey: kSecValueData as String) as? Data,
            status == noErr
            else {
                print("Retrive status: ", status)
                return nil
        }
        return String(data: resultsData, encoding: .utf8)
    }
    
    private func oaKeychainQuery(withKey key: String) -> NSMutableDictionary {
        let result = NSMutableDictionary()
        result.setValue(kSecClassGenericPassword, forKey: kSecClass as String)
        result.setValue(key, forKey: kSecAttrService as String)
        result.setValue(kSecAttrAccessibleAlwaysThisDeviceOnly, forKey: kSecAttrAccessible as String)
        return result
    }
}
