//
//  InAppNotificationManager.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 17/04/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit
import WebEngage

class InAppNotificationManager: NSObject, WEGInAppNotificationProtocol {

    private func notificationPrepared(_ inAppNotificationData: [AnyHashable : Any]!, shouldStop stopRendering: UnsafeMutablePointer<ObjCBool>!) -> [AnyHashable : Any]! {
        return inAppNotificationData
    }

    private func notificationShown(_ inAppNotificationData: [AnyHashable : Any]!) {

    }

    private func notification(_ inAppNotificationData: [AnyHashable : Any]!, clickedWithAction actionId: String!) {

        if let actionData = inAppNotificationData {
            let actions = actionData["actions"] as! [[String : Any]]
            for action in actions {
                if (action["actionEId"] as? String) == actionId {
                    if let deeplinkUrl = action["actionLink"] as? String {
                        DeepLinkManager.handleLinkPath(deeplinkUrl)
                    }
                }
            }
        }
    }

    private func notificationDismissed(_ inAppNotificationData: [AnyHashable : Any]!) {

    }
}
