//
//  Analytics.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 06/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//
import Foundation
import FirebaseAnalytics
import WebEngage
let appsFlyerEventQueryParams = ["pid", "c", "af_ad", "af_channel", "af_adset", "ll_deeplink_url", "deep_link_value", "media_source", "campaign" ]

class EventTracking {
    
    static let shared = EventTracking()
    let weUser: WEGUser = WebEngage.sharedInstance().user
    private init() {
        Permission.shared.prefetchLocation()
    }
    
    func eventTracking(name: Event.Events, _ model: [Event.EventKeys: Any]? = nil, withGI: Bool = false, withAppFlyer: Bool = false) {
        var newDict: [String: Any] = [:]
        if let model = model {
            for (key, value) in model {
                newDict[key.rawValue] = value
            }
        }
        if let link = sharedAppDelegate?.appOpenedWithLink, link != Constants.AppConstants.organic{
            newDict["link"] = link
        }
        if let userType:String = UserDefaultsKeys.custType.get(), userType != CustType.notVerified{
            newDict["User Type"] = userType
        }
        newDict["User Group"] = AppUserDefaults.isCatalystUserGroup ? "Test group" : "Control group"
        if let product = sharedAppDelegate?.recoCatalystProduct {
            newDict["Catalyst_Product"] = product
        }
        if let service = sharedAppDelegate?.recoCatalystService {
            newDict["Catalyst_Service"] = service
        }
        if let subCategory = sharedAppDelegate?.recoCatalystCommSubcat {
            newDict["Catalyst_CommSubcat"] = subCategory
        }
        if let rID = sharedAppDelegate?.recoCatalystRID {
            newDict["recommendationId"] = rID
        }
        if let uiComponentID = sharedAppDelegate?.recoCatalystUIComponentID {
            newDict["uiComponentId"] = uiComponentID
        }
        if let rcID = sharedAppDelegate?.recoCatalystRCID {
            newDict["commId"] = rcID
        }
        
        if Event.appsFlyersOnlyEvents.contains(name) {
            addToAppsFlyer(name: name.rawValue, model: newDict)
            return
        }
        if withAppFlyer {
            addToAppsFlyer(name: name.rawValue, model: newDict)
        }
        
        addEventToFirebase(name.rawValue, newDict)
        addToWebengage(name:name.rawValue, model:newDict)
        if withGI {
            addToGoogleAnalytics(name.rawValue)
        }
    }
    
    func addScreenTracking(with screenName: Event.Screen) {
        addGoogleScreenTracking(with: screenName.rawValue)
        addToWebengageScreen(with: screenName.rawValue)
    }
    
    private func addToGoogleAnalytics(_ name:String){
        let tracker = GAI.sharedInstance().defaultTracker
        let eventTracker = GAIDictionaryBuilder.createEvent(
            withCategory: "UI Event",
            action: "Button Click",
            label: name,
            value: nil).build()
        tracker?.send(eventTracker as [NSObject : AnyObject]?)
    }
    
    func addEmailSystemAttribute(value:String){
        weUser.setEmail(value)
    }
    
    func setBirthDateStringSystemAttribute(value:String){
        weUser.setBirthDateString(value)
    }
    
    func phoneNumberSystemAttribute(value:String){
        weUser.setPhone(value)
    }
    func genderSystemAttribute(value:String){
        weUser.setGender(value)
    }
    
    func firstNameSystemAttribute(value:String){
        weUser.setFirstName(value)
    }
    func lastNameSystemAttribute(value:String){
        weUser.setLastName(value)
    }
    func companySystemAttribute(value:String){
        weUser.setCompany(value)
    }
    func locationSystemAttribute(_ location: CLLocationCoordinate2D){
        weUser.setUserLocationWithLatitude(location.latitude as NSNumber, andLongitude: location.longitude as NSNumber)
    }
    
    func addUserAttributes(withName:String,withValue:Any){
        let weUser: WEGUser = WebEngage.sharedInstance().user
        if let withValueDict = withValue as? [String:Any]{
            weUser.setAttribute("Address", withDictionaryValue: withValueDict)
        }
        else if let withValueArray  = withValue as? [Any]{
            weUser.setAttribute(withName, withArrayValue: withValueArray)
        }
        else if let withValueString = withValue as? String{
            weUser.setAttribute(withName, withStringValue: withValueString)
        }
        else if let withValueBoolean = withValue as? Bool {
            
            if withValueBoolean {
                weUser.setAttribute(withName, withValue: true)
            }
            else {
                weUser.setAttribute(withName, withValue: false)
            }
        }
        else if let withValueNumber = withValue as? NSNumber{
            weUser.setAttribute(withName, withValue: withValueNumber)
        }
        else if let withValueDate = withValue as? Date{
            weUser.setAttribute(withName, withDateValue: withValueDate)
        }
    }
     func addEventToFirebase(_ name: String, _ model: [String: Any]?) {
        let nameWithUnderscore = name.replacingOccurrences(of: " ", with: "_")
        var eventDict = [String:String]()
        if let model = model{
            for (key, value) in model {
                let keyString = key.replacingOccurrences(of: " ", with: "_")
                eventDict[keyString] = String(describing: value)
            }
        }
        Analytics.logEvent(nameWithUnderscore, parameters: eventDict)
    }
    
    private func addToWebengage(name:String ,model:[String:Any]?){
        let weAnalytics: WEGAnalytics = WebEngage.sharedInstance().analytics
        if let model = model{
            weAnalytics.trackEvent(withName: name, andValue:model)
        }else{
            weAnalytics.trackEvent(withName: name)
        }
    }
    
    func addToAppsFlyer(name: String, model: [String:Any]?){
        let nameWithUnderscore = name.replacingOccurrences(of: " ", with: "_")
        var eventDict = [String:String]()
        if let model = model {
            for (key, value) in model {
                if key != "link" {
                    let keyString = key.replacingOccurrences(of: " ", with: "_")
                    eventDict[keyString] = String(describing: value)
                }
            }
        }
        AppsFlyerLib.shared().logEvent(nameWithUnderscore, withValues: eventDict)
    }
    
    private func addGoogleScreenTracking(with name: String) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value: name)
        let screenView = GAIDictionaryBuilder.createScreenView().build()
        tracker?.send((screenView as! [AnyHashable : Any]))
    }
    
    private func addToWebengageScreen(with name :String){
        let weAnalytics: WEGAnalytics = WebEngage.sharedInstance().analytics
        weAnalytics.navigatingToScreen(withName: name)
    }
    
    func analyticWork(_ response: UserInfo) {
        addEmailSystemAttribute(value: response.email ?? "")
        if let mobileNumber = response.mobileNumber, !mobileNumber.isEmpty {
            phoneNumberSystemAttribute(value: "+91" + mobileNumber )
        }
        if let gender = response.gender, !gender.isEmpty {
            genderSystemAttribute(value:gender)
        }
        firstNameSystemAttribute(value:response.firstName ?? "")
        lastNameSystemAttribute(value:response.lastName ?? "")
        
        if let coordinates = Permission.shared.location?.coordinate, CLLocationCoordinate2DIsValid(coordinates) {
            locationSystemAttribute(coordinates)
        }
    }
}
