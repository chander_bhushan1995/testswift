//
//  Extensions.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/25/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner
import AVFoundation
import VideoToolbox

protocol ReuseIdentifier : class {
    
}
extension ReuseIdentifier where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

protocol NibLoadableView : class{
    
}
extension NibLoadableView where Self : UIView {
    static var nibName : String {
        return String(describing: self)
    }
    
}

extension UITableView{
    func register<T>(cell : T.Type) where T : ReuseIdentifier,T : UITableViewCell , T : NibLoadableView {
        register(UINib(nibName: T.nibName, bundle: nil), forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T>(indexPath: IndexPath) -> T where T : UITableViewCell, T : ReuseIdentifier {
        guard  let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("\(type(of: self)) Could not dequeue cell with identifier \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func removeDefaultPaddingForGrouped() {
        tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: 8.0))
        tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: 8.0))
    }
}

extension UITableView {
    func setHeaderView(_ headerView: UIView) {
        headerView.translatesAutoresizingMaskIntoConstraints = false
        tableHeaderView = headerView
        
        let centerXConstraint = NSLayoutConstraint(item: headerView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: headerView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
        let topConstraint = NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([centerXConstraint, widthConstraint, topConstraint])
        
        tableHeaderView?.layoutIfNeeded()
    }
    
    func register(nib: String, forCellReuseIdentifier identifier: String) {
        register(UINib(nibName: nib, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func register(nib: String, forHeaderFooterViewReuseIdentifier identifier: String) {
        self.register(UINib(nibName: nib, bundle: nil), forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    func registerHeader(withIdentifier identifier:String) {
        register(UINib(nibName: identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: identifier)
    }
}

extension UICollectionView {
    
    func register<T>(cell : T.Type) where T : ReuseIdentifier,T : UICollectionViewCell , T : NibLoadableView {
        register(UINib(nibName: T.nibName, bundle: nil), forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T>(indexPath: IndexPath) -> T where T : UICollectionViewCell, T : ReuseIdentifier {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("\(type(of: self)) Could not dequeue cell with identifier \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func register(nib: String) {
        register(UINib(nibName: nib, bundle: nil), forCellWithReuseIdentifier: nib)
    }
    
    func register(nib: String, forCellReuseIdentifier identifier: String) {
        register(UINib(nibName: nib, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    
    func register(nib: String, forSupplementaryViewOfKind kind: String, withReuseIdentifier identifier: String) {
        register(UINib(nibName: nib, bundle: nil), forSupplementaryViewOfKind: kind, withReuseIdentifier: identifier)
    }
}

extension String {
    
    var deletingLastPathComponent: String {
        return (self as NSString).deletingLastPathComponent
    }
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func substringsFromTag() -> [String]? {
        let components = split(separator: "<")
        let indexes = components.compactMap { (i) -> Range<String.Index>? in
            if let end = i.range(of: ">")?.lowerBound, let start = range(of: i)?.lowerBound {
                return start..<end
            }
            return nil
        }
        let substrings = indexes.map { String(self[$0]) }
        
        return substrings
    }
    //MARK:- DLS
    var firstCapitalized: String {
        let strComponents = self.components(separatedBy: " ")
        var camelCased = ""
        for str in strComponents {
            let capitalizedComponent = str.prefix(1).uppercased()+str.dropFirst().lowercased()
            camelCased = "\(camelCased) \(capitalizedComponent)"
        }
        return camelCased
    }
     func toDictionary() -> [String:AnyObject]? {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func toDate(withFormat format: String = "dd/MM/yyyy") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = gregorianCalendar
        guard let date = dateFormatter.date(from: self) else {
            return Date()
        }
        return date
    }
    
    func height(withWidth width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: .greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
        return actualSize.height
    }
    
    func width(withHeight height: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: .greatestFiniteMagnitude, height: height)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font: font], context: nil)
        return ceil(actualSize.width)
    }
    
    func leftPadding(toLength: Int, withPad character: Character) -> String {
        let stringLength = self.count
        if stringLength < toLength {
            return String(repeatElement(character, count: toLength - stringLength)) + self
        } else {
            return String(self.suffix(toLength))
        }
    }
    
    func appendingPathComponent(_ pathComponent: String) -> String {
        return (self as NSString).appendingPathComponent(pathComponent)
    }
}

extension NSAttributedString {

    func height(containerWidth: CGFloat) -> CGFloat {

        let rect = self.boundingRect(with: CGSize.init(width: containerWidth, height: CGFloat.greatestFiniteMagnitude),
                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                     context: nil)
        return ceil(rect.size.height)
    }

    func width(containerHeight: CGFloat) -> CGFloat {

        let rect = self.boundingRect(with: CGSize.init(width: CGFloat.greatestFiniteMagnitude, height: containerHeight),
                                     options: [.usesLineFragmentOrigin, .usesFontLeading],
                                     context: nil)
        return ceil(rect.size.width)
    }
}

extension UIApplication {
    
    func makeStatusBarView(with backgroundColor: UIColor) {
        if #available(iOS 13.0, *) {
            let statusBarView = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBarView.backgroundColor = backgroundColor
            UIApplication.shared.keyWindow?.addSubview(statusBarView)
        } else {
            let statusBarView = value(forKey: "statusBar") as? UIView
            statusBarView?.backgroundColor = backgroundColor
        }
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        
        let roundedValue = String(format: "%.5f", self)
        return Double(roundedValue)!
        
        //let divisor = pow(10.0, Double(places))
        //return (self * divisor).rounded() / divisor
        //return (Darwin.round(self * divisor)/divisor)
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

extension UIView {
    
    func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
        
        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)
        
        var position = layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        layer.position = position
        layer.anchorPoint = point
    }
    
    func addShadowUnderView() {
        
    }
    
    func dropShadow(color : UIColor) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = self.frame.size.height / 2.0
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
    
    func roundCornersWithBorderAndShadow(_ corners:UIRectCorner, radius: CGFloat, with borderWidth: CGFloat? = nil, and borderColor: UIColor? = nil, with shadowOffset: CGSize? = nil,and shadowColor: UIColor? = nil,and shadowOpacity: Float? = nil,and shadowRadius: CGFloat? = nil) {
        // Add rounded corners
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = maskLayer
        
        // Add border
        if let borderWidth = borderWidth,let borderColor = borderColor {
            let borderLayer = CAShapeLayer()
            borderLayer.path = maskLayer.path // Reuse the Bezier path
            borderLayer.fillColor = UIColor.white.cgColor
            borderLayer.strokeColor = borderColor.cgColor
            borderLayer.lineWidth = borderWidth
            borderLayer.frame = self.bounds
            self.layer.addSublayer(borderLayer)
        }
        
        if let shadowOffset = shadowOffset,let shadowColor = shadowColor,let shadowOpacity = shadowOpacity,let shadowRadius = shadowRadius {
            let shadowLayer = CAShapeLayer()
            shadowLayer.path = maskLayer.path
            shadowLayer.shadowOffset = shadowOffset
            shadowLayer.shadowColor = shadowColor.cgColor
            shadowLayer.shadowOpacity = shadowOpacity
            shadowLayer.shadowRadius = shadowRadius
            shadowLayer.frame = self.bounds
            self.layer.addSublayer(shadowLayer)
        }
    }
    
    @objc func initDefaultBorder() {
        layer.cornerRadius = 2
        backgroundColor = UIColor.white
        layer.borderWidth = 1
    }
   
    func getSpinnerView() -> MMMaterialDesignSpinner {
        let sv: MMMaterialDesignSpinner = MMMaterialDesignSpinner(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        sv.lineWidth = 2.0
        sv.startAnimating()
        sv.tintColor = UIColor.buttonTitleBlue
        
        backgroundColor = UIColor.clear
        tintColor = UIColor.clear
//        addSubview(sv)
        return sv
    }
    
    /// This will add the subview and set constraint programmatically, default constant for constraints is 0
    func insertSubview(subview:UIView, padding:Padding = Padding(top: 0.0,leading: 0.0,bottom: 0.0,trailing: 0.0)){
        let parentView = self
        parentView.addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: subview, attribute: .top,      relatedBy: .equal, toItem: parentView, attribute: .top,      multiplier: 1.0, constant: padding.top).isActive = true
        NSLayoutConstraint(item: subview, attribute: .leading,  relatedBy: .equal, toItem: parentView, attribute: .leading,  multiplier: 1.0, constant: padding.leading).isActive = true
        NSLayoutConstraint(item: parentView, attribute: .bottom,   relatedBy: .equal, toItem: subview, attribute: .bottom,   multiplier: 1.0, constant: padding.bottom).isActive = true
        NSLayoutConstraint(item: parentView, attribute: .trailing, relatedBy: .equal, toItem: subview, attribute: .trailing, multiplier: 1.0, constant: padding.trailing).isActive = true
        
        //or visual way
        //        let views = ["view": subview]
        //        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: .alignAllLastBaseline, metrics: nil, views: views)
        //        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: .alignAllLastBaseline, metrics: nil, views: views)
        //        addConstraints(verticalConstraints + horizontalConstraints)
    }
    struct Padding {
        var top:CGFloat, leading:CGFloat, bottom:CGFloat, trailing:CGFloat
    }
    
    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
               self.alpha = 1.0
       }, completion: completion)  }

       func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
               self.alpha = 0.0
       }, completion: completion)
      }
    
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}

extension UILabel {
    
    func setSelectedOrNot(_ isSelected: Bool) {
        self.font = isSelected ? DLSFont.h3.bold : DLSFont.h3.regular
    }
    
    func setAttributedString(_ string: String, bodyColor: UIColor, bodyFont: UIFont, attributedParts parts: [String], attributedTextColors colors: [UIColor], attributedFonts fonts: [UIFont]) {
        if parts.count == colors.count && parts.count  == fonts.count {
            let mainStr = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: bodyFont, NSAttributedString.Key.foregroundColor: bodyColor])
            let str = string as NSString
            
            for (index, part) in parts.enumerated() {
                let range = str.range(of: part)
                mainStr.addAttributes([NSAttributedString.Key.font: fonts[index], NSAttributedString.Key.foregroundColor: colors[index]], range: range)
            }
            attributedText = mainStr
        } else {
            fatalError("Provide colors and fonts for all attributed parts")
        }
    }
    
    func circledLabel(with number: String, textColor: UIColor, cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor, backgroundColor: UIColor) {
        self.text = number
        self.textColor = textColor
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.backgroundColor = backgroundColor
    }
    
    func greyCircledLabel(with number: Int) {
        
    }
    
    func addTrailing(with trailingText: String, moreText: String, moreTextColor: UIColor) {
        if let text = text, isTruncating {
            let font: UIFont = self.font
            let lengthForVisibleString = vissibleTextLength
            let trimmedString = (text as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: (text.count - lengthForVisibleString)), with: "")
            let readMoreLength = (trailingText + moreText).count
            let trimmedForReadMore = (trimmedString as NSString).replacingCharacters(in: NSRange(location: (trimmedString.count - readMoreLength), length: readMoreLength), with: "") + trailingText
            let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [.font: font])
            answerAttributed.append(NSMutableAttributedString(string: moreText, attributes: [.font: font, .foregroundColor: moreTextColor]))
            attributedText = answerAttributed
        }
    }
    
    var vissibleTextLength: Int {
        guard let text = text else { return 0 }
        
        let font: UIFont = self.font
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let actualHeight = text.height(withWidth: labelWidth, font: font)
        
        if actualHeight > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if lineBreakMode == .byCharWrapping {
                    index += 1
                } else {
                    index = (text as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: text.count - index - 1)).location
                }
            } while index != NSNotFound && index < text.count && (text as NSString).substring(to: index).height(withWidth: labelWidth, font: font) <= labelHeight
            return prev
        }
        return text.count
    }
    
    var isTruncating: Bool {
        guard let text = text else { return false }
        let font: UIFont = self.font
        return text.height(withWidth: frame.size.width, font: font) > frame.size.height
    }
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

        guard let labelText = self.text else { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = self.textAlignment

        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }

        // (Swift 4.2 and above) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))


        // (Swift 4.1 and 4.0) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        self.attributedText = attributedString
    }
}

extension String {
    func getAttributedString(bodyColor: UIColor, bodyFont: UIFont, attributedParts parts: [String], attributedTextColors colors: [UIColor], attributedFonts fonts: [UIFont]) -> NSAttributedString? {
        if parts.count == colors.count && parts.count  == fonts.count {
            let mainStr = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: bodyFont, NSAttributedString.Key.foregroundColor: bodyColor])
            let str = self as NSString
            
            for (index, part) in parts.enumerated() {
                let range = str.range(of: part)
                mainStr.addAttributes([NSAttributedString.Key.font: fonts[index], NSAttributedString.Key.foregroundColor: colors[index]], range: range)
            }
            return mainStr
        } else {
            fatalError("Provide colors and fonts for all attributed parts")
        }
    }
    
    func getAttributedDescription() -> NSAttributedString {
        
        let substrings = substringsFromTag() ?? [String]()
        let strReplaced = self.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "")
        let mainStr = NSMutableAttributedString(string: strReplaced, attributes: [NSAttributedString.Key.font: DLSFont.bodyText.regular, NSAttributedString.Key.foregroundColor: UIColor.bodyTextGray])
        let str = strReplaced as NSString
        
        for (_, part) in substrings.enumerated() {
            let range = str.range(of: part)
            mainStr.addAttributes([NSAttributedString.Key.font: DLSFont.bodyText.bold], range: range)
        }
        return mainStr
    }
    
    func nsRange(from range: Range<String.Index>) -> NSRange {
        let startPos = self.distance(from: self.startIndex, to: range.lowerBound)
        let endPos = self.distance(from: self.startIndex, to: range.upperBound)
        return NSMakeRange(startPos, endPos - startPos)
    }
}

import Kingfisher

extension UIImageView {
    func setImageWithUrlString(_ urlString: String?, placeholderImage: UIImage? = nil, completionHandler: ((_ image:UIImage?)->())? = nil) {
        let url = URL(string: urlString ?? "")
//        self.kf.setImage(with: url, placeholder: placeholderImage)
       self.kf.setImage(with: url, placeholder: placeholderImage) { result in
           switch result {
           case .success(let value):
            completionHandler?(value.image)
           case .failure(let error):
               print("Error: \(error)")
           }
       }
    }
    
    func changePngColorTo(color: UIColor) {
        guard let image =  self.image else { return }
        self.image = image.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}

extension UIApplication {
    static var appVersion: String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    
    static var build: String {
        return Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
    var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
           let statusBarView = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
           return statusBarView
       } else {
           let statusBarView = value(forKey: "statusBar") as? UIView
           return statusBarView
       }
        
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIDevice {
    
    static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
    
    static var deviceModel: String {
        return UIDevice.current.model
    }
    
    static var uuid: String {
        guard let deviceId = OAKeychain.sharedInstance[OAKeychainKeys.deviceId.rawValue] else {
            let uuid = UIDevice.current.identifierForVendor?.uuidString ?? ""
            OAKeychain.sharedInstance[OAKeychainKeys.deviceId.rawValue] = uuid
            return uuid
        }
        return deviceId
    }
    
    static var deviceVersion: String {
        return UIDevice.current.systemVersion
    }
}

extension String {
    func validateSingularity(count: Int) -> String {
        
        if count == 1 {
            return "\(count) \(self)"
        } else {
            let str = "\(self)s"
            return "\(count) \(str)"
        }
    }
}
extension String{
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}
extension URL {
    public var queryItems: [String: String] {
        var params = [String: String]()
        return URLComponents(url: self, resolvingAgainstBaseURL: false)?
            .queryItems?
            .reduce([:], { (_, item) -> [String: String] in
                params[item.name] = item.value
                return params
            }) ?? [:]
    }
    
    func appending(_ queryItem: String, value: String?) -> URL {

        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []

        // Create query item
        let queryItem = URLQueryItem(name: queryItem, value: value)

        // Append the new query item in the existing query items array
        queryItems.append(queryItem)

        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems

        // Returns the url from new url components
        return urlComponents.url!
    }
    
    func appendQueryParams(_ queryParams: [String: String?]) -> URL {
        
        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []

        for (queryItem, value) in queryParams {
            // Create query item
            let queryItem = URLQueryItem(name: queryItem, value: value)

            // Append the new query item in the existing query items array
            queryItems.append(queryItem)
        }

        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems

        // Returns the url from new url components
        return urlComponents.url!
    }
}
extension NSNumber {
    convenience init(value: String?) {
        let intValue = Int(value ?? "0") ?? 0
        self.init(value: intValue)
    }
}
extension UINavigationItem {
    
    
    func setTitle(title:String? = nil, subTitle:String? = nil, backButtonIcon:UIImage? = nil) {
        
        let textStackView = UIStackView()
        textStackView.distribution = .equalCentering
        textStackView.axis = .vertical
        
        let iconStackView = UIStackView()
        iconStackView.distribution = .equalCentering
        iconStackView.axis = .vertical
        
        let subTitleLabel = H3BoldLabel()
        subTitleLabel.text = subTitle
        subTitleLabel.textAlignment = .left
        subTitleLabel.sizeToFit()
        
        let titleLabel = SupportingTextRegularGreyLabel()
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let imageView = UIImageView(image: backButtonIcon)
        let imageView1 = UIImageView(image: backButtonIcon)
        if title != nil {
            textStackView.addArrangedSubview(titleLabel)
        }
        if subTitle != nil {
            textStackView.addArrangedSubview(subTitleLabel)
        }
        if backButtonIcon != nil {
            iconStackView.addArrangedSubview(imageView)
            if title != nil && subTitle != nil{
                imageView1.alpha = 0.0
                iconStackView.addArrangedSubview(imageView1)
            }
        }
        self.leftBarButtonItems = [UIBarButtonItem(customView: iconStackView), UIBarButtonItem(customView: textStackView) ]
        
    }
    func getBackBar(title:String? = nil, subTitle:String? = nil, backButtonIcon:UIImage? = nil){
        
        let textStackView = UIStackView()
        textStackView.distribution = .equalCentering
        textStackView.axis = .vertical
        
        let iconStackView = UIStackView()
        iconStackView.distribution = .equalCentering
        iconStackView.axis = .vertical
        
        let subTitleLabel = H3BoldLabel()
        subTitleLabel.text = subTitle
        subTitleLabel.textAlignment = .left
        subTitleLabel.sizeToFit()
        
        let titleLabel = SupportingTextRegularGreyLabel()
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let imageView = UIImageView(image: backButtonIcon)
        let imageView1 = UIImageView(image: backButtonIcon)
        if title != nil {
            textStackView.addArrangedSubview(titleLabel)
        }
        if subTitle != nil {
            textStackView.addArrangedSubview(subTitleLabel)
        }
        if backButtonIcon != nil {
            iconStackView.addArrangedSubview(imageView)
            if title != nil && subTitle != nil{
                imageView1.alpha = 0.0
                iconStackView.addArrangedSubview(imageView1)
            }
        }
        self.leftBarButtonItem = UIBarButtonItem(customView: textStackView)
       // self.leftBarButtonItems = [UIBarButtonItem(customView: iconStackView), UIBarButtonItem(customView: textStackView) ]
//        return UIBarButtonItem(customView: iconStackView)
        if backButtonIcon != nil {
            backBarButtonImage()
        }
    }
    
     func backBarButtonImage(){
        
        var backButtonBackgroundImage = #imageLiteral(resourceName: "navigateBack")
        
        // The background should be pinned to the left and not stretch.
        backButtonBackgroundImage = backButtonBackgroundImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: backButtonBackgroundImage.size.width + 100, bottom: 0, right: 0))
        //      let barAppearance = UINavigationBar.appearance(whenContainedInInstancesOf: [UIAppearanceContainer.])
        //        barAppearance.backIndicatorImage = backButtonBackgroundImage
        //        barAppearance.backIndicatorTransitionMaskImage = backButtonBackgroundImage
        
        let barAppearance = UINavigationBar.appearance()
        barAppearance.backIndicatorImage = backButtonBackgroundImage
        barAppearance.backIndicatorTransitionMaskImage = backButtonBackgroundImage
//        navigationController?.navigationBar.barTintColor = .purple
//        navigationController?.navigationBar.tintColor = .white
//        navigationController?.navigationBar.barStyle = UIBarStyle.black
//
        // Provide an empty backBarButton to hide the 'Back' text present by
        // default in the back button.
        //
        // NOTE: You do not need to provide a target or action.  These are set
        //       by the navigation bar.
        // NOTE: Setting the title of this bar button item to ' ' (space) works
        //       around a bug in iOS 7.0.x where the background image would be
        //       horizontally compressed if the back button title is empty.
//        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        self.backBarButtonItem = backButton;
        self.hidesBackButton = true
    }
}

extension UINavigationBar {
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        return CGSize(width: UIScreen.main.bounds.width, height: 200)
//    }
}

extension UIPageControl {
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
                dotView.layer.cornerRadius = dotView.frame.size.height / 2
                dotView.layer.borderColor = UIColor.clear.cgColor
                dotView.layer.borderWidth = 0.0
            }else{
                dotView.backgroundColor = .clear
                dotView.layer.cornerRadius = dotView.frame.size.height / 2
                dotView.layer.borderColor = dotBorderColor.cgColor
                dotView.layer.borderWidth = dotBorderWidth
            }
        }
    }
    
}


extension UITextField {
    
    func styleTextField(with backgroundColor: UIColor, borderColor: UIColor, shadowColor: UIColor){
        self.borderStyle = .roundedRect
       // self.layer.masksToBounds = false
        self.layer.cornerRadius = 2.0;
        self.layer.backgroundColor = backgroundColor.cgColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1.0
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 2.0
    }
}
extension UITextView {
    
    func styleTextView(with backgroundColor: UIColor, borderColor: UIColor, shadowColor: UIColor){
//        self.borderStyle = .roundedRect
//        self.layer.masksToBounds = false
        self.layer.cornerRadius = 2.0;
        self.layer.backgroundColor = backgroundColor.cgColor
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1.0
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
    }
}

extension UIButton {
    func setupCommonProperties() {
        if let buttonName = titleLabel?.text, buttonName.components(separatedBy: " ").count < 3 {
            setTitle(titleLabel?.text?.firstCapitalized, for: .normal)
        }
        self.layer.cornerRadius = 2.0;
        self.clipsToBounds = true
    }
}

extension UIRefreshControl {
    
    func addSpinnerView() {
        let sv = getSpinnerView()
        sv.frame = CGRect(x: UIScreen.main.bounds.width/2 - 15, y: 20, width: 30, height: 30)
//        let sv: MMMaterialDesignSpinner = MMMaterialDesignSpinner(frame: CGRect(x: UIScreen.main.bounds.width/2 - 15, y: 20, width: 30, height: 30))
//        sv.lineWidth = 2.0
//        sv.startAnimating()
//        sv.tintColor = UIColor.buttonTitleBlue
//
//        backgroundColor = UIColor.clear
//        tintColor = UIColor.clear
        addSubview(sv)
    }
}

extension CAGradientLayer {
    
    enum Point {
        case topLeft
        case centerLeft
        case bottomLeft
        case topCenter
        case center
        case bottomCenter
        case topRight
        case centerRight
        case bottomRight
        
        var point: CGPoint {
            switch self {
            case .topLeft:
                return CGPoint(x: 0, y: 0)
            case .centerLeft:
                return CGPoint(x: 0, y: 0.5)
            case .bottomLeft:
                return CGPoint(x: 0, y: 1.0)
            case .topCenter:
                return CGPoint(x: 0.5, y: 0)
            case .center:
                return CGPoint(x: 0.5, y: 0.5)
            case .bottomCenter:
                return CGPoint(x: 0.5, y: 1.0)
            case .topRight:
                return CGPoint(x: 1.0, y: 0.0)
            case .centerRight:
                return CGPoint(x: 1.0, y: 0.5)
            case .bottomRight:
                return CGPoint(x: 1.0, y: 1.0)
            }
        }
    }
    
    convenience init(start: Point, end: Point, colors: [UIColor], type: CAGradientLayerType) {
        self.init()
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors.map { $0.cgColor }
        self.locations = (0..<colors.count).map(NSNumber.init)
//        self.type = type
    }
    
    func update(start: Point, end: Point, colors: [UIColor], type: CAGradientLayerType) {
        self.startPoint = start.point
        self.endPoint = end.point
        self.colors = colors.map { $0.cgColor }
        self.locations = (0..<colors.count).map(NSNumber.init)
        //        self.type = type
    }
    
    func update(colors: [UIColor], type: CAGradientLayerType) {
        self.colors = colors.map { $0.cgColor }
        self.locations = (0..<colors.count).map(NSNumber.init)
        //        self.type = type
    }
}

extension CAShapeLayer {
    convenience init(start: CGPoint, end: CGPoint, color: CGColor) {
        self.init()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        self.path = linePath.cgPath
        self.strokeColor = color
        self.lineWidth = 1
        self.lineJoin = .round
        self.lineDashPattern = [1, 4]
    }
    
    func update(start: CGPoint, end: CGPoint) {
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        self.path = linePath.cgPath
        self.strokeColor = UIColor.gray2.cgColor
        self.lineWidth = 1
        self.lineJoin = .round
        self.lineDashPattern = [1, 4]
    }
}

extension UInt64 {
    
    static var oneGB: UInt64 {
        return 1024*1024*1024
    }
    
    static var ramSize: UInt64 {
        let memory = ProcessInfo().physicalMemory
        switch memory {
        case let x where x <= 1 * oneGB: return 1
        case let x where x <= 2 * oneGB: return 2
        case let x where x <= 3 * oneGB: return 3
        case let x where x <= 4 * oneGB: return 4
        case let x where x <= 5 * oneGB: return 5
        case let x where x <= 6 * oneGB: return 6
        case let x where x <= 7 * oneGB: return 7
        case let x where x <= 8 * oneGB: return 8
        case let x where x <= 9 * oneGB: return 9
        case let x where x <= 10 * oneGB: return 10
        case let x where x <= 11 * oneGB: return 11
        case let x where x <= 12 * oneGB: return 12
        case let x where x <= 13 * oneGB: return 13
        case let x where x <= 14 * oneGB: return 14
        case let x where x <= 15 * oneGB: return 15
        case let x where x <= 16 * oneGB: return 16
        default: return 0
        }
    }
    
    static var diskSize: UInt64 {
        let dictionary = try? FileManager.default.attributesOfFileSystem(forPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last ?? "")
        let memory = (dictionary?[.systemSize] as? NSNumber)?.uint64Value ?? 0
        switch memory {
        case let x where x <= 8 * oneGB: return 8
        case let x where x <= 16 * oneGB: return 16
        case let x where x <= 32 * oneGB: return 32
        case let x where x <= 64 * oneGB: return 64
        case let x where x <= 128 * oneGB: return 128
        case let x where x <= 256 * oneGB: return 256
        case let x where x <= 512 * oneGB: return 512
        case let x where x <= 1024 * oneGB: return 1024
        default: return 0
        }
    }
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}

extension String {
    func numberWithComma() -> String? {
        let largeNumber = NSNumber(value: Int(Double(self) ?? 0))
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: largeNumber)
    }
    
    func containsNumbersWithSpace() -> Bool {
        // check if there's a range for a number
        let numberRange = self.rangeOfCharacter(from: .decimalDigits)
        // check if there's a range for a whitespace
        let spaceRange = self.rangeOfCharacter(from: .whitespaces)
        return (numberRange != nil && spaceRange != nil)
    }
    
    
    func luhnCheckForValidImei() -> Bool {
        var sum = 0
        
        if self.count != 15 {
            return false
        }
        
        let reversedCharacters = self.reversed().map { String($0) }
        for (idx, element) in reversedCharacters.enumerated() {
            guard let digit = Int(element) else { return false }
            switch ((idx % 2 == 1), digit) {
            case (true, 9): sum += 9
            case (true, 0...8): sum += (digit * 2) % 9
            default: sum += digit
            }
        }
        return sum % 10 == 0
    }
    
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    subscript(r: Range<Int>) -> String? {
        get {
            let stringCount = self.count as Int
            if (stringCount < r.upperBound) || (stringCount < r.lowerBound) {
                return nil
            }
            let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: r.upperBound - r.lowerBound)
            return String(self[(startIndex ..< endIndex)])
        }
    }
    
    func containsAlphabets() -> Bool {
        //Checks if all the characters inside the string are alphabets
        let set = CharacterSet.letters
        return self.utf16.contains( where: {
            guard let unicode = UnicodeScalar($0) else { return false }
            return set.contains(unicode)
        })
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    static func encrypt(string: String) -> Data? {
        return string.data(using: .utf8)?.xor()
    }
    static func decrypt(value: Data?)->String?{
        guard let value = value else {return ""}
        return String(data: value.xor(), encoding: .utf8)
    }
}

extension Int64 {
    func numberWithComma() -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value: self))
    }
    
    func coverAmountFormat() -> String? {
        switch self {
        case let x where x >= 10000 && x < 100000: return roundedNumberWithScale(1000) + " Thousand"
        case let x where x >= 100000 && x < 10000000: return roundedNumberWithScale(100000) + " Lakh"
        case let x where x >= 10000000: return roundedNumberWithScale(10000000) + " Crore"
        default: return numberWithComma()
        }
    }
    
    func roundedNumberWithScale(_ scale: Int64) -> String {
        var format = "%.0f"
        if self % scale == 0 {
            format = "%.0f"
        } else if (self / scale < 10) && ((self * 10) % scale == 0) {
            format = "%.2f"
        } else {
            format = "%.1f"
        }
        
        return String(format: format, Double(self)/Double(scale))
    }
}

extension CGImage {
    var brightness: Double {
        get {
            var counter:Int = 0
            var r:Int = 0
            var g:Int = 0
            var b:Int = 0
            
            let width = Int(self.width)
            let height = Int(self.height)
            if let cfData = self.dataProvider?.data, let pointer = CFDataGetBytePtr(cfData) {
                for x in stride(from: 0, to: width, by: 2) {
                    for y in stride(from: 0, to: height, by: 2) {
                        let pixelAddress = x * 4 + y * width * 4
                        r += Int(pointer.advanced(by: pixelAddress).pointee)
                        g += Int(pointer.advanced(by: pixelAddress + 1).pointee)
                        b += Int(pointer.advanced(by: pixelAddress + 2).pointee)
                        counter += 1
                    }
                }
            }
            let bright = Double(Int(r+g+b) / (counter*3))
            return bright
        }
    }
}

extension UIImage {
    var brightness: Double {
        get {
            return (self.cgImage?.brightness)!
        }
    }
    convenience init(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        UIGraphicsBeginImageContextWithOptions(size, false, 1)
        let context = UIGraphicsGetCurrentContext()!
        context.setFillColor(color.cgColor)
        context.fill(CGRect(origin: .zero, size: size))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.init(data: image.pngData()!)!
    }
    
    public convenience init?(pixelBuffer: CVPixelBuffer) {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(pixelBuffer, options: nil, imageOut: &cgImage)
        if let cgImageVar = cgImage {
            self.init(cgImage: cgImageVar)
            return
        }
        return nil
    }
    
    func cropImage(rect: CGRect) -> UIImage {
        let cgImage = self.cgImage! // better to write "guard" in realm app
        let croppedCGImage = cgImage.cropping(to: rect)
        return UIImage(cgImage: croppedCGImage!)
    }
    
    func pixelBufferFromImage() -> CVPixelBuffer {
        
        
        let ciimage = CIImage(image: self)
        //let cgimage = convertCIImageToCGImage(inputImage: ciimage!)
        let tmpcontext = CIContext(options: nil)
        let cgimage =  tmpcontext.createCGImage(ciimage!, from: ciimage!.extent)
        
        let cfnumPointer = UnsafeMutablePointer<UnsafeRawPointer>.allocate(capacity: 1)
        let cfnum = CFNumberCreate(kCFAllocatorDefault, .intType, cfnumPointer)
        let keys: [CFString] = [kCVPixelBufferCGImageCompatibilityKey, kCVPixelBufferCGBitmapContextCompatibilityKey, kCVPixelBufferBytesPerRowAlignmentKey]
        let values: [CFTypeRef] = [kCFBooleanTrue, kCFBooleanTrue, cfnum!]
        let keysPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
        let valuesPointer =  UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
        keysPointer.initialize(to: keys)
        valuesPointer.initialize(to: values)
        
        let options = CFDictionaryCreate(kCFAllocatorDefault, keysPointer, valuesPointer, keys.count, nil, nil)
        
        let width = cgimage!.width
        let height = cgimage!.height
        
        var pxbuffer: CVPixelBuffer?
        // if pxbuffer = nil, you will get status = -6661
        var status = CVPixelBufferCreate(kCFAllocatorDefault, width, height,
                                         kCVPixelFormatType_32BGRA, options, &pxbuffer)
        status = CVPixelBufferLockBaseAddress(pxbuffer!, CVPixelBufferLockFlags(rawValue: 0));
        
        let bufferAddress = CVPixelBufferGetBaseAddress(pxbuffer!);
        
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB();
        let bytesperrow = CVPixelBufferGetBytesPerRow(pxbuffer!)
        let context = CGContext(data: bufferAddress,
                                width: width,
                                height: height,
                                bitsPerComponent: 8,
                                bytesPerRow: bytesperrow,
                                space: rgbColorSpace,
                                bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue);
        context?.concatenate(CGAffineTransform(rotationAngle: 0))
        context?.concatenate(__CGAffineTransformMake( 1, 0, 0, -1, 0, CGFloat(height) )) //Flip Vertical
        //        context?.concatenate(__CGAffineTransformMake( -1.0, 0.0, 0.0, 1.0, CGFloat(width), 0.0)) //Flip Horizontal
        
        
        context?.draw(cgimage!, in: CGRect(x:0, y:0, width:CGFloat(width), height:CGFloat(height)));
        status = CVPixelBufferUnlockBaseAddress(pxbuffer!, CVPixelBufferLockFlags(rawValue: 0));
        return pxbuffer!;
        
    }
    
    
    func scaledImage(_ maxDimension: CGFloat) -> UIImage? {
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        
        if size.width > size.height {
            scaledSize.height = size.height / size.width * scaledSize.width
        } else {
            scaledSize.width = size.width / size.height * scaledSize.height
        }
        UIGraphicsBeginImageContext(scaledSize)
        
        draw(in: CGRect(x: 0, y: 0, width: scaledSize.width, height: scaledSize.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
    
    func scaledImage(_ height: CGFloat, _ width: CGFloat) -> UIImage? {
        let size = self.size
        
        let widthRatio  = width  / size.width
        let heightRatio = height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContext(newSize)
        
        draw(in: rect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
    
    // This is not used now. But can use in future
    func removeStatusBarFromImage() -> UIImage? {
        // Use cropped method
        let app = UIApplication.shared
        var height = app.statusBarFrame.size.height
        if height <= 0.0 {
            height = 40.0
        }
        
        let originalCGImage = self.cgImage
        let originalCGImageWithoutStatusBar = originalCGImage?.cropping(to: CGRect(x: 0, y:height, width: self.size.width, height: self.size.height - height))
        
        return UIImage(cgImage: originalCGImageWithoutStatusBar!)
    }
    
    
    func resizeToMB(_ expectedSizeInMb:Int) -> Data? {
        guard let fileSize = self.jpegData(compressionQuality: 0.9) else {return nil}
        let Size = Float(Double(fileSize.count)/1024/1024)
        
        // I am checking 5 MB size here you check as you want
        if Size > Float(expectedSizeInMb) {
            let sizeInBytes = expectedSizeInMb * 1024 * 1024
            var needCompress:Bool = true
            var imgData:Data?
            var compressingValue = Size
            
            while (needCompress && compressingValue > Float(expectedSizeInMb)) {
                let percentgeCompressed = (1/compressingValue) * Float(expectedSizeInMb)
                
                if let data:Data = self.jpegData(compressionQuality: CGFloat(percentgeCompressed)) {
                    if data.count < sizeInBytes {
                        needCompress = false
                        imgData = data
                    } else {
                        compressingValue += Float(Double(data.count)/1024/1024)
                    }
                }
            }
            return imgData
        }
        return fileSize
    }
    
    // add image properties (exif, gps etc) to image
    func addImageProperties(imageData: Data, properties: NSDictionary?) -> Data? {
        // create an imagesourceref
        if let source = CGImageSourceCreateWithData(imageData as CFData, nil) {
            // this is of type image
            if let uti = CGImageSourceGetType(source) {
                // create a new data object and write the new image into it
                let destinationData = NSMutableData()
                if let destination = CGImageDestinationCreateWithData(destinationData, uti, 1, nil) {
                    // add the image contained in the image source to the destination, overidding the old metadata with our modified metadata
                    CGImageDestinationAddImageFromSource(destination, source, 0, (properties ?? {} as! CFDictionary) as CFDictionary)
                    if CGImageDestinationFinalize(destination) == false {
                        return nil
                    }
                    return destinationData as Data
                }
            }
        }
        return nil
    }
    
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}

extension UISegmentedControl {
    /// Tint color doesn't have any effect on iOS 13.
    func ensureiOS12Style() {
        if #available(iOS 13, *) {
        }else {
            layer.borderColor = tintColor.cgColor
            let tintColorImage = UIImage(color: .clear)
            // Must set the background image for normal to something (even clear) else the rest won't work
            setBackgroundImage(UIImage(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: [.highlighted, .selected], barMetrics: .default)
            setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
            layer.borderWidth = 1
            setTitleTextAttributes([.foregroundColor: UIColor.dodgerBlue], for:.normal)
            setTitleTextAttributes([.foregroundColor: UIColor.black], for:.selected)
        }
    }
    
    func ensureClearStyle() {
        layer.borderColor = tintColor.cgColor
        let tintColorImage = UIImage(color: .clear)
        // Must set the background image for normal to something (even clear) else the rest won't work
        setBackgroundImage(UIImage(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
        setBackgroundImage(tintColorImage, for: [.highlighted, .selected], barMetrics: .default)
        setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        layer.borderWidth = 1
        setTitleTextAttributes([.foregroundColor: UIColor.dodgerBlue], for:.normal)
        setTitleTextAttributes([.foregroundColor: UIColor.black], for:.selected)
    }
    
    func ensureCustomiOS13Style() {
        if #available(iOS 13, *) {
            backgroundColor = UIColor.groupTableViewBackground
            layer.borderColor = UIColor.dodgerBlue.cgColor
            selectedSegmentTintColor = UIColor.white
            layer.borderWidth = 1

            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.dodgerBlue]
           setTitleTextAttributes(titleTextAttributes, for:.normal)

            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.black]
           setTitleTextAttributes(titleTextAttributes1, for:.selected)
            
        }
    }
}


extension AVCaptureDevice {
    
    func configureDesiredFrameRate(_ desiredFrameRate: Int) {
        
        var isFPSSupported = false
        
        do {
            
            if let videoSupportedFrameRateRanges = activeFormat.videoSupportedFrameRateRanges as? [AVFrameRateRange] {
                for range in videoSupportedFrameRateRanges {
                    if (range.maxFrameRate >= Double(desiredFrameRate) && range.minFrameRate <= Double(desiredFrameRate)) {
                        isFPSSupported = true
                        break
                    }
                }
            }
            
            if isFPSSupported {
                try lockForConfiguration()
                activeVideoMaxFrameDuration = CMTimeMake(value: 1, timescale: Int32(desiredFrameRate))
                activeVideoMinFrameDuration = CMTimeMake(value: 1, timescale: Int32(desiredFrameRate))
                unlockForConfiguration()
            }
            
        } catch {
            print("lockForConfiguration error: \(error.localizedDescription)")
        }
    }
    
}

extension FileManager {
    
    class func getURL(for path: String) -> URL?{
        return try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(path)
    }
    
    class func documentsDir() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    class func appendingPathComponent(_ pathComponent: String)->String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        let mainPath = paths[0].appendingPathComponent(pathComponent)
        return mainPath
    }
    
    class func cachesDir() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
}


extension CVPixelBuffer {

  /**
   Returns thumbnail by cropping pixel buffer to biggest square and scaling the cropped image to
   model dimensions.
   */
  func centerThumbnail(ofSize size: CGSize ) -> CVPixelBuffer? {

    let imageWidth = CVPixelBufferGetWidth(self)
    let imageHeight = CVPixelBufferGetHeight(self)
    let pixelBufferType = CVPixelBufferGetPixelFormatType(self)

    assert(pixelBufferType == kCVPixelFormatType_32BGRA)

    let inputImageRowBytes = CVPixelBufferGetBytesPerRow(self)
    let imageChannels = 4

    let thumbnailSize = min(imageWidth, imageHeight)
    CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))

    var originX = 0
    var originY = 0

    if imageWidth > imageHeight {
      originX = (imageWidth - imageHeight) / 2
    }
    else {
      originY = (imageHeight - imageWidth) / 2
    }

    // Finds the biggest square in the pixel buffer and advances rows based on it.
    guard let inputBaseAddress = CVPixelBufferGetBaseAddress(self)?.advanced(
        by: originY * inputImageRowBytes + originX * imageChannels) else {
      return nil
    }

    // Gets vImage Buffer from input image
    var inputVImageBuffer = vImage_Buffer(
        data: inputBaseAddress, height: UInt(thumbnailSize), width: UInt(thumbnailSize),
        rowBytes: inputImageRowBytes)

    let thumbnailRowBytes = Int(size.width) * imageChannels
    guard  let thumbnailBytes = malloc(Int(size.height) * thumbnailRowBytes) else {
      return nil
    }

    // Allocates a vImage buffer for thumbnail image.
    var thumbnailVImageBuffer = vImage_Buffer(data: thumbnailBytes, height: UInt(size.height), width: UInt(size.width), rowBytes: thumbnailRowBytes)

    // Performs the scale operation on input image buffer and stores it in thumbnail image buffer.
    let scaleError = vImageScale_ARGB8888(&inputVImageBuffer, &thumbnailVImageBuffer, nil, vImage_Flags(0))

    CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))

    guard scaleError == kvImageNoError else {
      return nil
    }

    let releaseCallBack: CVPixelBufferReleaseBytesCallback = {mutablePointer, pointer in

      if let pointer = pointer {
        free(UnsafeMutableRawPointer(mutating: pointer))
      }
    }

    var thumbnailPixelBuffer: CVPixelBuffer?

    // Converts the thumbnail vImage buffer to CVPixelBuffer
    let conversionStatus = CVPixelBufferCreateWithBytes(
        nil, Int(size.width), Int(size.height), pixelBufferType, thumbnailBytes,
        thumbnailRowBytes, releaseCallBack, nil, nil, &thumbnailPixelBuffer)

    guard conversionStatus == kCVReturnSuccess else {

      free(thumbnailBytes)
      return nil
    }

    return thumbnailPixelBuffer
  }

  static func buffer(from image: UIImage) -> CVPixelBuffer? {
    let attrs = [
      kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue,
      kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue
    ] as CFDictionary

    var pixelBuffer: CVPixelBuffer?
    let status = CVPixelBufferCreate(kCFAllocatorDefault,
                                     Int(image.size.width),
                                     Int(image.size.height),
                                     kCVPixelFormatType_32BGRA,
                                     attrs,
                                     &pixelBuffer)

    guard let buffer = pixelBuffer, status == kCVReturnSuccess else {
      return nil
    }

    CVPixelBufferLockBaseAddress(buffer, [])
    defer { CVPixelBufferUnlockBaseAddress(buffer, []) }
    let pixelData = CVPixelBufferGetBaseAddress(buffer)

    let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
    guard let context = CGContext(data: pixelData,
                                  width: Int(image.size.width),
                                  height: Int(image.size.height),
                                  bitsPerComponent: 8,
                                  bytesPerRow: CVPixelBufferGetBytesPerRow(buffer),
                                  space: rgbColorSpace,
                                  bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue) else {
      return nil
    }

    context.translateBy(x: 0, y: image.size.height)
    context.scaleBy(x: 1.0, y: -1.0)

    UIGraphicsPushContext(context)
    image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
    UIGraphicsPopContext()

    return pixelBuffer
  }
    
    
    /// Returns thumbnail by cropping pixel buffer to biggest square and scaling the cropped image
    /// to model dimensions.
    func resized(to size: CGSize ) -> CVPixelBuffer? {

      let imageWidth = CVPixelBufferGetWidth(self)
      let imageHeight = CVPixelBufferGetHeight(self)

      let pixelBufferType = CVPixelBufferGetPixelFormatType(self)

      assert(pixelBufferType == kCVPixelFormatType_32BGRA ||
             pixelBufferType == kCVPixelFormatType_32ARGB)

      let inputImageRowBytes = CVPixelBufferGetBytesPerRow(self)
      let imageChannels = 4

      CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))

      // Finds the biggest square in the pixel buffer and advances rows based on it.
      guard let inputBaseAddress = CVPixelBufferGetBaseAddress(self) else {
        return nil
      }

      // Gets vImage Buffer from input image
      var inputVImageBuffer = vImage_Buffer(data: inputBaseAddress, height: UInt(imageHeight), width: UInt(imageWidth), rowBytes: inputImageRowBytes)

      let scaledImageRowBytes = Int(size.width) * imageChannels
      guard  let scaledImageBytes = malloc(Int(size.height) * scaledImageRowBytes) else {
        return nil
      }

      // Allocates a vImage buffer for scaled image.
      var scaledVImageBuffer = vImage_Buffer(data: scaledImageBytes, height: UInt(size.height), width: UInt(size.width), rowBytes: scaledImageRowBytes)

      // Performs the scale operation on input image buffer and stores it in scaled image buffer.
      let scaleError = vImageScale_ARGB8888(&inputVImageBuffer, &scaledVImageBuffer, nil, vImage_Flags(0))

      CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))

      guard scaleError == kvImageNoError else {
        return nil
      }

      let releaseCallBack: CVPixelBufferReleaseBytesCallback = {mutablePointer, pointer in

        if let pointer = pointer {
          free(UnsafeMutableRawPointer(mutating: pointer))
        }
      }

      var scaledPixelBuffer: CVPixelBuffer?

      // Converts the scaled vImage buffer to CVPixelBuffer
      let conversionStatus = CVPixelBufferCreateWithBytes(nil, Int(size.width), Int(size.height), pixelBufferType, scaledImageBytes, scaledImageRowBytes, releaseCallBack, nil, nil, &scaledPixelBuffer)

      guard conversionStatus == kCVReturnSuccess else {

        free(scaledImageBytes)
        return nil
      }

      return scaledPixelBuffer
    }

}


extension UIProgressView {
    
    @IBInspectable var barHeight : CGFloat {
        get {
            return transform.d * 2.0
        }
        set {
            // 2.0 Refers to the default height of 2
            let heightScale = newValue / 2.0
            let c = center
            transform = CGAffineTransform(scaleX: 1.0, y: heightScale)
            center = c
        }
    }
    
    func setCornerRadius(cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.layer.sublayers![1].cornerRadius = cornerRadius
        self.subviews[1].clipsToBounds = true
    }
}

/**
 Below protocol is useful when we wanted to dismiss a view controller from mid of
 a stack of presented view controllers ie. A presented B presented C, want to dismiss B from C
 
 You should add below code in viewWillAppear of B ( which you wanted to dismiss)
 
 if isDismissedInStack {
     self.dismiss(animated: false, completion: nil)
 }
 */
protocol DismissPresentedViewInStack: AnyObject {
    var isDismissedInStack: Bool {set get}
    func setDismissProperty()
}

extension DismissPresentedViewInStack where Self: UIViewController {
    func setDismissProperty() {
        isDismissedInStack = true
        view.isHidden = true
    }
    
    func setDefaultProperty() {
        isDismissedInStack = false
        view.isHidden = false
    }
}

extension UIViewController {
    func checkError(_ response: BaseResponseDTO?, error: Error? ,interruptErrorHandler : ((_ error : ApiResponseError)-> Bool)? = nil) throws {
        if let error = response?.error, response?.status?.lowercased() == Constants.ResponseConstants.failure {
            if let errorObj = error.first {
                if let errorHandler = interruptErrorHandler {
                    if errorHandler(errorObj){
                        return;
                    }
                }
            }
        }
        
        try Utilities.checkError(response, error: error)
    }
}
