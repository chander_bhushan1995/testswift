//
//  ImagePickerManager.swift
//  ImagePickerSwift
//
//  Created by Mukesh Yadav on 23/01/17.
//  Copyright © 2017 Mukesh Yadav. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import MobileCoreServices

class ImagePickerManager: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate {

    // MARK:- Private properties
    private var postHandler: ((_ image: UIImage?, _ error: Error?) -> Void)?
    
    // MARK:- Private constants
    static let sourceUnavailable = "Source Unavailable"
    static let permissionDenied = "Permission Denied"
    static let viewControllerError = "View Controller Not Found"
    static let pickCancelled = "Image Pick Cancelled"
    static let imageNotFound = "Image not found"
    
    var imageName = ""
    
    /// Mark initializer private so that it cannot be accessed from other classes to make it a singleton.
    private override init() { }
    
    // MARK:- Public properties and methods
    /// Public singleton shared object of `ImagePickerManager`
    public static let shared = ImagePickerManager()
    
    /// Change this to hide errors for permission and source unavailability
    public let showAlerts = true
    
    /// Pick an Image from Camera.
    /// ```
    ///    ImagePickerManager.shared.pickImageFromCamera(editing: true) {[weak self] (image, error) in
    ///
    ///        if let strongSelf = self {
    ///            if image != nil {
    ///                // Use image object
    ///            } else {
    ///                print(error?.localizedDescription)
    ///            }
    ///        }
    ///    }
    /// ```
    /// - Parameters:
    ///   - editing: Allow croping of picked image.
    ///   - handler: Closure to Handle picked image. error will be nil if image picked successfully.
    ///      - image: Resulting image which is picked.
    ///   - error: Error object if any error occured while picking image.
    public func pickImageFromCamera(editing: Bool, handler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        pickImageFrom(source: .camera, editing: editing, handler: handler)
    }
    
    /// Pick an Image from Photo Library.
    /// ```
    ///    ImagePickerManager.shared.pickImageFromPhotoLibrary(editing: true) {[weak self] (image, error) in
    ///
    ///        if let strongSelf = self {
    ///            if image != nil {
    ///                // Use image object
    ///            } else {
    ///                print(error?.localizedDescription)
    ///            }
    ///        }
    ///    }
    /// ```
    /// - Parameters:
    ///   - editing: Allow croping of picked image.
    ///   - handler: Closure to Handle picked image. error will be nil if image picked successfully.
    ///      - image: Resulting image which is picked.
    ///   - error: Error object if any error occured while picking image.
    public func pickImageFromPhotoLibrary(editing: Bool, handler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        pickImageFrom(source: .photoLibrary, editing: editing, handler: handler)
    }
    
    /// Pick an Image from Photo Album.
    /// ```
    ///    ImagePickerManager.shared.pickImageFromPhotoAlbum(editing: true) {[weak self] (image, error) in
    ///
    ///        if let strongSelf = self {
    ///            if image != nil {
    ///                // Use image object
    ///            } else {
    ///                print(error?.localizedDescription)
    ///            }
    ///        }
    ///    }
    /// ```
    /// - Parameters:
    ///   - editing: Allow croping of picked image.
    ///   - handler: Closure to Handle picked image. error will be nil if image picked successfully.
    ///      - image: Resulting image which is picked.
    ///   - error: Error object if any error occured while picking image.
    public func pickImageFromPhotoAlbum(editing: Bool, handler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        pickImageFrom(source: .savedPhotosAlbum, editing: editing, handler: handler)
    }
    
    // MARK:- Private methods
    private func pickImageFrom(source: UIImagePickerController.SourceType, editing: Bool, handler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        if !UIImagePickerController.isSourceTypeAvailable(source) {
            let error = ImagePickerError(description: ImagePickerManager.sourceUnavailable)
            showSourceErrorAlert()
            handler(nil, error)
        } else {
            
            do {
                try checkPermissions(source: source, editing: editing, handler: handler)
                
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = source
                imagePicker.allowsEditing = editing
                imagePicker.delegate = self
                imagePicker.mediaTypes = [kUTTypeImage as String]
                postHandler = handler
                
                if let viewController = getCurrentViewController() {
                    //imagePicker.modalPresentationStyle = .popover
                    imagePicker.popoverPresentationController?.delegate = self
                    imagePicker.popoverPresentationController?.sourceView = viewController.view
                    imagePicker.popoverPresentationController?.sourceRect = CGRect(x: viewController.view.frame.width / 2, y: ((viewController.view.frame.height) / 2), width: 0, height: 0)
                    imagePicker.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                    
                    viewController.presentInFullScreen(imagePicker, animated: true, completion: nil)
                    
                    DispatchQueue.main.async {
                        viewController.view.alpha = 0.5
                    }
                    
                } else {
                    let error = ImagePickerError(description: ImagePickerManager.viewControllerError)
                    handler(nil, error)
                }
                
            } catch _ as NotDeterminedError {
                // Do nothing
            } catch let error {
                showPermissionAlert(source: source)
                handler(nil, error)
            }
        }
    }
    
    private func checkPermissions(source: UIImagePickerController.SourceType, editing: Bool, handler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) throws {
        if source == .camera {
            
            let cameraMediaType = AVMediaType.video
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
            
            switch cameraAuthorizationStatus {
            case .denied:
                throw ImagePickerError(description: ImagePickerManager.permissionDenied)
            case .notDetermined:
                sharedAppDelegate?.isShowBlurView = false
                AVCaptureDevice.requestAccess(for: cameraMediaType, completionHandler: { (status) in
                    DispatchQueue.main.async {
                        self.pickImageFrom(source: source, editing: editing, handler: handler)
                    }
                    
                })
                
                throw NotDeterminedError.notDetermined
            default: break
            }
        } else {
            let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
            
            switch photoAuthorizationStatus {
            case .denied:
                throw ImagePickerError(description: ImagePickerManager.permissionDenied)
            case .notDetermined:
                sharedAppDelegate?.isShowBlurView = false
                PHPhotoLibrary.requestAuthorization({ (status) in
                    DispatchQueue.main.async {
                        self.pickImageFrom(source: source, editing: editing, handler: handler)
                    }
                })
                
                throw NotDeterminedError.notDetermined
            default: break
            }
        }
    }
    
    func showPermissionAlert(source : UIImagePickerController.SourceType) {
        
        if !showAlerts {
            return
        }

        var message : String
        if source == .camera {
            message = "OneAssist does not have permission to access the camera. Please change the settings to continue with image upload.".localized()
        } else {
            message = "OneAssist does not have permission to access the gallery. Please change the settings to continue with image upload.".localized()
        }
        
        if let viewController = getCurrentViewController() {
//            viewController.presentInFullScreen(alert, animated: true, completion: nil)
            viewController.showAlert(title: ImagePickerManager.permissionDenied, message: message, primaryButtonTitle: "Settings", "Cancel", primaryAction: {
                Utilities.openAppSettings()
            }, nil)
        }
        
    }
    
    private func showSourceErrorAlert() {
        if !showAlerts {
            return
        }
        
//        let alert = UIAlertController(title: "", message: ImagePickerManager.sourceUnavailable, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
//            alert.dismiss(animated: true, completion: nil)
//        }))
        
        if let viewController = getCurrentViewController() {
//            viewController.presentInFullScreen(alert, animated: true, completion: nil)
            viewController.showAlert(message: ImagePickerManager.sourceUnavailable)
        }
    }
    
    /// Get current view controller object
    private func getCurrentViewController() -> UIViewController? {
        let window = UIApplication.shared.windows.first
        let rootVC = window?.rootViewController
        var currentVC:UIViewController?
        
        if rootVC is UINavigationController {
            let navController = rootVC as! UINavigationController
            currentVC = navController.visibleViewController
        } else if rootVC != nil {
            currentVC = rootVC
            while let presentedViewController = currentVC?.presentedViewController {
                currentVC = presentedViewController
            }
        }
        return currentVC
    }
    
    // MARK:- UIImagePickerControllerDelegate implementation
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        let error = ImagePickerError(description: ImagePickerManager.pickCancelled)
        if let handler = postHandler {
            handler(nil, error)
        }
        
        picker.dismiss(animated: true) {
            if let viewController = self.getCurrentViewController() {
                viewController.view.alpha = 1.0
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
            let assetResources = PHAssetResource.assetResources(for: asset)
            imageName = assetResources.first!.originalFilename
        }
        
        if let editedImage = info[.editedImage] as? UIImage {
            if let handler = postHandler {
                handler(editedImage, nil)
            }
        } else if let originalImage = info[.originalImage] as? UIImage {
            if let handler = postHandler {
                handler(originalImage, nil)
            }
        } else {
            let error = ImagePickerError(description: ImagePickerManager.imageNotFound)
            if let handler = postHandler {
                handler(nil, error)
            }
        }
        
        picker.dismiss(animated: true) {
            if let viewController = self.getCurrentViewController() {
                viewController.view.alpha = 1.0
            }
        }
    }
    
    // MARK:- UIPopoverPresentationControllerDelegate implementation
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        let error = ImagePickerError(description: ImagePickerManager.pickCancelled)
        if let handler = postHandler {
            handler(nil, error)
        }
        
        if let viewController = getCurrentViewController() {
            viewController.view.alpha = 1.0
        }
    }
    
    
    func checkImageSize(_ image: UIImage){
       // var Size = Float()
       // if let data = UIIMagePNG
    }
}

// MARK:- Error handling for ImagePickerManager

enum NotDeterminedError: Error {
    case notDetermined
}

class ImagePickerError: NSObject, LocalizedError {
    
    private var desc = ""
    
    init(description: String) {
        desc = description
    }
    
    override var description: String {
        get {
            return "ImagePickerManager: \(desc)"
        }
    }
    
    var errorDescription: String? {
        get {
            return self.description
        }
    }
}
