//
//  Deeplink.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 07/04/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation



/**
 Enum for Deeplinks.
 */

enum DeepLink: String,CaseIterable {
    
    // Common Links
    case openChat = "common/chat"
    case rateApp = "rate/rateapp"
    case sodTimeline = "sr/servicedetail"

    // Pinelab
    case pinelab = "customer/verification"
    
    // catalyst recommendation
    case component = "component"
    
    
    //Home
    case homeHome = "home/home"
    case mhc = "mobile/mhc"
    case mhcResult = "mhc/result"
    case addCard = "wallet/addcard"
    case myCards = "wallet/mycards"
    case cards = "wallet/cards"
    case myOffers = "wallet/offers"
    case offerDetails = "wallet/offer"
    case bankBranch = "wallet/bankbranch"
    case ATM = "wallet/ATM"
    case offlineOffer = "wallet/offlineoffers"
    case onlineOffer = "wallet/onlineoffers"
    case buyback = "buyback"
    
    //Buy
    case homeAssistSeggestPlan = "common/purchasehomeprotectionplan"
    case mobileSuggestAPlan = "common/purchasemobileprotectionplan"
    case walletSuggestAPlan = "common/purchasewalletprotectionplan"
    case homeSuggestAPlan = "common/purchasehomeassistprotectionplan"
    case sopSuggestAPlan = "common/purchasehomeunlimitedserviceplan"
    case homeBuyPlan = "home/buyplan"
    case mobileTab2 = "category/mobile-tablet-insurance-damage-protection"
    case mobileTab3 = "category/laptop-damage-insurance-protection"
    case mobileTab4 = "category/wearable-insurance-damage-protection"
    case suggestAplanMobile = "mobile/suggestaplan"
    case submitDetails = "mobile/submitdetails"
    case walletTab2 = "category/wallet-card-theft-protection"
    case suggestAplanWallet = "wallet/suggestaplan"
    case homeTab2 = "category/home-appliances-service-repair-warranty"
    case homeTab3 = "category/home-appliances-service-repair-extended-warranty"
    case suggestAplanHome = "home/suggestaplan"
    case buy = "buy"
    case salesRecommendation = "sales/recommendation"
    case salesRecommendationIDFence = "category/identity-theft-monitoring-protection-idfence"
    case covidKnowMore = "covidknowmore"
    case riskCalculator = "campaign/Risk-Calculator"
    case howclaimworks = "productService/howclaimworks"
    
    //Membership
    case homeMemberships = "home/memberships"
    case uploadDocumentStatus = "upload-document/status"
    case mobileTab = "mobile/mymobile"
    case upgradeNowMobile = "mobile/upgradenow"
    case walletTab = "wallet/mywallet"
    case upgradeNowWallet = "wallet/upgradenow"
    case homeTab = "home/myhome"
    case whcSelectAddress = "home/selectaddress"
    case whcInspectionDetails = "home/inspectiondetails"
    case whcApplianceDetails = "home/appliancedetails"
    case ongoingSR = "SR/ongoing"
    case historySR = "SR/history"
    case serviceDetail = "SR/servicedetail"
    case srTimeline = "/timeline"
    case srUploadDoc = "/document-upload"
    case excessPayment = "/doPaymentViaLink"
    case bankDetails = "/customer-bank-details"
    case srReschedule = "myaccount/service/reschedule"
    case idfence = "idfence"
    
    //Account
    case accountTab = "common/myaccount"
    case homeAccount = "home/account"
    case favoriteOffer = "wallet/Myfavouriteoffers"
    case myRewards = "myRewards"
    case onboardingQuestion = "onboardingQuestion"
    case profileQuestion = "profileQuestion"
    case myProfile = "myProfile"
    case rewardAppShare = "rewardAppShare"
    case activateVoucher = "common/activatevoucher"
    
    
    /**
     init method will take 5 different cases and if there is a different url ,make it default
     */
    init?(url: String) {
        
        if url.contains("/timeline") {
            self = .srTimeline
        } else if url.contains("sr/servicedetail") {
            self = .sodTimeline
        }else if url.contains("/document-upload") {
            self = .srUploadDoc
        } else if url.contains("/doPaymentViaLink") {
            self = .excessPayment
        } else if url.contains("service/bankdetail") || url.contains("customer-bank-details"){
            self = .bankDetails
        } else if url.contains("service/reschedule") {
            self = .srReschedule
        } else if url.contains("buyback/") {
            self = .buyback
        } else if url.contains("buy/") {
            self = .buy
        }else if url.contains("component/") {
            self = .component
        }else if url.contains("sales/recommendation") {
            self = .salesRecommendation
        }else if url.contains("category/identity-theft-monitoring-protection-idfence"){
            self = .salesRecommendationIDFence
        }else if url.contains("myRewards") {
            self = .myRewards
        }else if url.contains("onboardingQuestion") {
            self = .onboardingQuestion
        }else if url.contains("profileQuestion") {
            self = .profileQuestion
        }else if url.contains("myProfile") {
            self = .myProfile
        }else if url.contains("covidknowmore") {
            self = .covidKnowMore
        }else if url.contains("rewardAppShare") {
            self = .rewardAppShare
        }else if url.contains("campaign/Risk-Calculator"){
            self = .riskCalculator
        }else if url.contains("productService/howclaimworks"){
            self = .howclaimworks
        }else if url.contains("idfence"){ //if any other deeplink contains path with idfence, needs to be checked before this
            self = .idfence
        }
        else {
            self.init(rawValue: url)
        }
    }
    
    static func listForWhichLoginRequired() -> [DeepLink] {
        return [.cards, .addCard, .myCards, .bankBranch, .buyback, .submitDetails, .homeMemberships, .uploadDocumentStatus, .walletTab, .mobileTab, .homeTab, .upgradeNowMobile, .upgradeNowWallet, .ongoingSR, .historySR, .serviceDetail, .srTimeline, .srUploadDoc, .bankDetails, .excessPayment, .srReschedule, .whcSelectAddress, .whcInspectionDetails, .homeAccount, .accountTab, .favoriteOffer, .openChat, .pinelab, .myProfile, .myRewards, .onboardingQuestion, .profileQuestion, .rewardAppShare, .sodTimeline, .idfence]
    }
    
    static func listForHomeTab() -> [DeepLink] {
        return [.homeHome, .cards, .addCard, .myCards, .myOffers, .onlineOffer, .offlineOffer, .bankBranch, .mhc, .mhcResult, .buyback, .ATM, .offerDetails]
    }
    
    static func listForAccountTab() -> [DeepLink] {
        return [.homeAccount, .activateVoucher, .accountTab, .myRewards, .onboardingQuestion, .profileQuestion, .myProfile, .favoriteOffer, .rewardAppShare]
    }
    
    static func listForBuyTab() -> [DeepLink] {
        return [.homeAssistSeggestPlan, .mobileSuggestAPlan, .walletSuggestAPlan, .homeSuggestAPlan, .homeBuyPlan, .mobileTab2, .mobileTab3, .mobileTab4, .suggestAplanMobile, .submitDetails, .walletTab2, .suggestAplanWallet, .homeTab2, .homeTab3, .suggestAplanHome, .buy, .salesRecommendation, .salesRecommendationIDFence, .covidKnowMore, .riskCalculator, .sopSuggestAPlan, .howclaimworks]
    }
    
    
    static func listForMemberShipTab() -> [DeepLink] {
        return [.homeMemberships, .uploadDocumentStatus, .mobileTab, .upgradeNowMobile, .walletTab, .upgradeNowWallet, .homeTab, .whcSelectAddress, .whcInspectionDetails, .whcApplianceDetails, .ongoingSR, .historySR, .serviceDetail, .srTimeline, .srUploadDoc, .excessPayment, .bankDetails, .srReschedule, .idfence]
    }
}

var currentDeeplink: DeepLink? {
    get {
        if let deeplinkText = UserDefaults.standard.string(forKey: UserDefaultsKeys.currentDeeplink.rawValue), let deeplink = DeepLink(rawValue: deeplinkText) {
            return deeplink
        } else {
            return nil
        }
    } set {
        if let newValue = newValue?.rawValue {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.currentDeeplink.rawValue)
        } else {
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.currentDeeplink.rawValue)
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.currentDeeplinkUrl.rawValue)
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.currentDeeplinkParams.rawValue)
            // Setting url and params to null as well
        }
        
        UserDefaults.standard.synchronize()
    }
}

var currentDeeplinkUrl: String? {
    get {
        return UserDefaults.standard.string(forKey: UserDefaultsKeys.currentDeeplinkUrl.rawValue)
    } set {
        if let newValue = newValue {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.currentDeeplinkUrl.rawValue)
        } else {
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.currentDeeplinkUrl.rawValue)
        }
        
        UserDefaults.standard.synchronize()
    }
}

var currentDeeplinkParams: [String]? {
    get {
        return UserDefaults.standard.stringArray(forKey: UserDefaultsKeys.currentDeeplinkParams.rawValue)
    } set {
        if let newValue = newValue {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.currentDeeplinkParams.rawValue)
        } else {
            UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.currentDeeplinkParams.rawValue)
        }
        
        UserDefaults.standard.synchronize()
    }
}
