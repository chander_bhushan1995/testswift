//
//  Log.swift
//
//  Created by Mukesh on 7/4/17.
//
//

public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        Swift.print(items, separator: " ", terminator: terminator)
    #endif
}

public func print<Target>(_ items: Any..., separator: String = " ", terminator: String = "\n", to output: inout Target) where Target : TextOutputStream {
    #if DEBUG
        Swift.print(items, separator: " ", terminator: terminator, to: &output)
    #endif
}
