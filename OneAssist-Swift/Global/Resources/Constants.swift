//
//  Constants.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/19/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//


import Foundation
import AppsFlyerLib
enum EnvironmentVariables {
    static func baseMainDomain() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://api.oneassist.in/apigateway" //webservice.oneassist.in
        case .aws: return "https://awswebservice.oneassist.in"
        case let env: return "https://\(env.toStringValue()).1atesting.in/apigateway"
        }
    }

    static func baseDomain() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://api.oneassist.in/apigateway" //oneassist.in
        case .aws: return "https://aws.oneassist.in"
        case let env: return "https://\(env.toStringValue()).1atesting.in/apigateway"
        }
    }

    static func baseDomainOld() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://oneassist.in"
        case .aws: return "https://aws.oneassist.in"
        case let env: return "https://\(env.toStringValue()).1atesting.in"
        }
    }

    static func servicePlatform() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://api.oneassist.in/apigateway/serviceplatform/api/"
        case .aws: return "https://awswebservice.oneassist.in/serviceplatform/api/"
        case let env: return "https://\(env.toStringValue()).1atesting.in/apigateway/serviceplatform/api/"
        }
    }

    static func oasysAdmin() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://api.oneassist.in/apigateway/OASYSADMIN/"
        case .aws: return "https://awswebservice.oneassist.in/apigateway/OASYSADMIN/"
        case let env: return "https://\(env.toStringValue()).1atesting.in/apigateway/OASYSADMIN/"
        }
    }

    static func bussPartnerName() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod, .aws: return "139"
        default: return "288"
        }
    }

    static func busUnitName() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod, .aws: return "2185"
        default: return "4390"
        }
    }

    static func newRelicToken() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod, .aws: return "AAb2c1a7142e43a8f42faf1e7392286dc99e322b5b"
        default: return "AAa81d5e7ee7daf78bbeba785cf4aa53e2fc9c77fb"
        }
    }
    
    static func baseChatURL() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://lhc.oneassist.in/"
        case .aws: return "https://awslhc.oneassist.in/"
        case .uat2: return "https://uat2.1atesting.in/"
        case let env: return "https://\(env.toStringValue())lhc.1atesting.in/"
        }
    }
    
    static func chatHostName() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "ejabberd.oneassist.in"
        case .aws: return "awsejabberd.oneassist.in"
        case .uat2: return "uat2ejabberd.1atesting.in"
        case .uat1: return "ejabberduat.1atesting.in"
        case let env: return "\(env.toStringValue())ejabberd.1atesting.in"
        }
    }
    
    static func baseCDNUrl() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://ws.oneassist.in/"
        case .aws: return "https://awsws.oneassist.in/"
        case .uat1: return "https://uat1cdn.1atesting.in/"
        case let env: return "https://\(env.toStringValue()).1atesting.in/"
        }
    }
    
    static func baseCDNUrlForTimeLineJS() -> String {
        switch EnvironmentSetting.currentEnviornment {
        case .prod: return "https://ws.oneassist.in/"
        case .aws: return "https://awsws.oneassist.in/"
        case .uat1: return "https://uat1cdn.1atesting.in"
        case let env: return "https://\(env.toStringValue()).1atesting.in"
        }
    }
}

enum PincodeState: String {
    case enter
    case fetch
    case error
}

enum Constants {
    
    static var isProdRelease: Bool {
        #if DEBUG
        return false
        #else
        return true
        #endif
    }

    static var getWeekday:String{
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "EEEE"
        dateFormatterGet.calendar = gregorianCalendar
        return dateFormatterGet.string(from: date).uppercased()
    }
    static var getCurrentDate:Date {
        let currentTime = Date() //.string(with : DateFormatter.time12NotAPperiod)
        return currentTime
    }
    
    static var appInstallDate: Date {
        if let documentsFolderPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last?.path, let installDate = (try? FileManager.default.attributesOfItem(atPath: documentsFolderPath)[.creationDate]) as? Date {
            return installDate
        }
        return Date() // Should never execute
    }
    
    enum SchemeVariables {
        
        #if DEBUG
        
        static let baseMainDomain = EnvironmentVariables.baseMainDomain()
        static let baseDomain = EnvironmentVariables.baseDomain()
        static let baseDomainOld = EnvironmentVariables.baseDomainOld()
        static let servicePlatform = EnvironmentVariables.servicePlatform()
        static let oasysAdmin = EnvironmentVariables.oasysAdmin()
        static let bussPartnerName = EnvironmentVariables.bussPartnerName()
        static let busUnitName = EnvironmentVariables.busUnitName()
        static let newRelicToken = EnvironmentVariables.newRelicToken()
        static let baseChatURL = EnvironmentVariables.baseChatURL()
        static let chatHostName = EnvironmentVariables.chatHostName()
        static let baseCDNUrl = EnvironmentVariables.baseCDNUrl()
        static let baseCDNUrlForJS = EnvironmentVariables.baseCDNUrlForTimeLineJS()
        
        #elseif TEST
        
        static let baseMainDomain = EnvironmentVariables.baseMainDomain()
        static let baseDomain = EnvironmentVariables.baseDomain()
        static let baseDomainOld = EnvironmentVariables.baseDomainOld()
        static let servicePlatform = EnvironmentVariables.servicePlatform()
        static let oasysAdmin = EnvironmentVariables.oasysAdmin()
        static let userName = EnvironmentVariables.userName()
        static let password = EnvironmentVariables.password()
        static let bussPartnerName = EnvironmentVariables.bussPartnerName()
        static let busUnitName = EnvironmentVariables.busUnitName()
        static let newRelicToken = EnvironmentVariables.newRelicToken()
        static let baseChatURL = EnvironmentVariables.baseChatURL()
        static let chatHostName = EnvironmentVariables.chatHostName()
        static let baseCDNUrl = EnvironmentVariables.baseCDNUrl()
        static let baseCDNUrlForJS = EnvironmentVariables.baseCDNUrlForTimeLineJS()
        static let adminServicePlatformAPIGateway = EnvironmentVariables.adminServicePlatformAPIGateway()

        #else
        
        static let baseMainDomain = "https://api.oneassist.in/apigateway" //webservice.oneassist.in
        static let baseDomain = "https://api.oneassist.in/apigateway" //oneassist.in
        static let baseDomainOld = "https://oneassist.in"
        static let servicePlatform = "https://api.oneassist.in/apigateway/serviceplatform/api/" //webservice.oneassist.in
        static let oasysAdmin = "https://api.oneassist.in/apigateway/OASYSADMIN/" //webservice.oneassist.in
        static let userName = "MobileApp"
        static let password = "O45YS@1#!$12"
        static let bussPartnerName = "139"
        static let busUnitName = "2185"
        static let newRelicToken = "AAb2c1a7142e43a8f42faf1e7392286dc99e322b5b"
        static let baseChatURL = "https://lhc.oneassist.in/"
        static let chatHostName = "ejabberd.oneassist.in"
        static let baseCDNUrl = "https://ws.oneassist.in/"
        static let baseCDNUrlForJS = "https://ws.oneassist.in"
        #endif
        
        static let partnerCode = "139"
        static let partnerBuCode = "2185"
        static let partnerBuName = "ASSIST IOS"
        static let leadSource = "Assist_app"
        static let currency = "INR"
        static let initiatingSystem =  21        
    }
    
    enum URL {
        static let baseMainUrlOasys = SchemeVariables.baseMainDomain + "/OASYS/"
        static let baseUrlOasys = SchemeVariables.baseDomain + "/OASYS/"
        static let baseUrlApiGateWay = SchemeVariables.baseDomain + "/"
        static let baseCustomer = baseUrlApiGateWay + "myaccount/api/customer/"
        static let baseAddCard = ""
        static let appStoreURL = "itms://itunes.apple.com/in/app/one-assist-for-iphone/id1074619069?mt=8"
        static let idFencePdf = "blob:"
    }
    
    enum headers {
        enum key {
            static let contentType = "Content-Type"
            static let appIdKey = "App-Id"
            static let token = "token"
            static let appVersion = "apkVersion" //will be used by Walkin
            
        }
        enum value {
            static let authorizationValue = "Basic dW5pdGUtd3M6dW5pdGUtMTIz"
            static let contentTypeValue = "application/json"
            static let appIdValue = "mobileApp"
        }
        
    }
    
    enum Category {
        static let homeAppliances = "HA"
        static let finance = "F"
        static let personalElectronics = "PE"
        static let quickRapairs = "QR"
    }
    
    enum AppConstants {
        static let contactNumber = "18001233330"
        static let online = "ONLINE"
        static let osType = "IOS"
        static let yes = "yes"
        static let no = "no"
        static let mainStoryboard = "Main"
        static let sodOrderActivity = "SO"
        static let organic = "organic"
        static let catalystGroup = "catalystVariationGroup"
        
        // MARK: Change this to show/hide Service Request tab.
    }
    
    enum VoucherConstants {
        static let mobileMP = "MP01"
        static let mobileTE = "TE"
        static let mobileTP = "TP"
    }
    
    enum RequestKeys {
        static let custId = "custId"
        static let mobileId = "mobileId"
        static let contactId = "contactId"
        static let planCode = "planCode"
        static let categoryCode = "categoryCode"
        static let mobileNo = "mobileNo"
        static let username = "username"
        static let password = "password"
        static let addressTmp = "addressTmp"
        static let pinCode = "pinCode"
        static let token = "token"
        static let orderId = "orderId"
        static let verificationNo = "verificationNo"
        static let promoCode = "promoCode"
        static let memId = "memId"
        static let cardPaymentMode = "CCD"
        static let netbankingPaymentMode = "NBK"
        static let operatorBillingPaymentMode = "OB"
        static let action = "action"
        static let clientkey = "clientkey"
        static let clientpass = "clientpass"
        static let apikey = "apikey"
        static let bank_code = "bank_code"
        static let network = "network"
        static let card_type = "card_type"
        static let cardcodes = "cardcodes"
        static let categories = "categories"
        static let user_latitude = "user_latitude"
        static let user_longitude = "user_longitude"
        static let brand_code = "brand_code"
        static let offer_code = "offer_code"
        static let outlet_code = "outlet_code"
        static let subParam = "subParam"
        static let sessionId = "sessionId"
        static let walletId = "walletId"
        static let imeiNo = "imeiNo"
        static let voucherCode = "voucherCode"
        static let docKey = "docKey"
        static let productCode = "productCode"
        static let filename_file = "filename_file"
        static let invoice_image = "INVOICE_IMAGE"
        static let additional_invoice_image = "ADDITIONAL_INVOICE_DOCUMENT"
        static let backPanel = "REAR_PANEL_IMAGE"
        static let frontPanel = "SCREEN_IMAGE"
        static let phone = "Phone"
        static let departmentId = "DepartamentID"
        static let question = "question"
        
        static let handsetAmount = "handset_amount"
        static let handsetModel = "handset_model"
        static let date = "date"
        static let activationCode = "activationCode"
        static let imei = "imei"
        static let imei2 = "imei2"
        static let purchaseAmount = "purchaseAmount"
        static let purchaseDate = "purchaseDate"
        
    }
    
    enum ResponseConstants {
        static let success = "success"
        static let failed = "failed"
        static let failure = "failure"
        static let redirect = "redirect"
        
    }
    
    enum IDFenceSubscriptionStatus {
        static let CUSTOMER_DOES_NOT_EXIST = "CUSTOMER_DOES_NOT_EXIST"
        static let CUSTOMER_IN_PROGRESS = "CUSTOMER_IN_PROGRESS"
        static let CUSTOMER_UNSUBSCRIBED = "CUSTOMER_UNSUBSCRIBED"
        static let CUSTOMER_ACTIVE = "CUSTOMER_ACTIVE"
        static let CUSTOMER_DEACTIVATED = "CUSTOMER_DEACTIVATED"
    }
    
    enum RenewalStatusFlags {
        static let notStarted = "NA"
        static let rejected = "R"
        static let pending = "P"
        static let initiated = "I"
    }
    
    enum RenewalActivityFlags {
        static let renewalWindow = "RW"
        static let changePlan = "UD"
        static let notApplicable = "NA"
        static let buyNewPlan = "X"
    }
    
    enum PlanServices {
        static let idFence = "DARK_WEB_MONITORING"
        static let wallet = "Card Fraud Insurance"
        static let extendedWarranty = "Extended Warran"
        static let whcInspection = "WHC_INSPECTION"
        static let creditScoreMonitoring = "CREDIT_SCORE"//"DARK_WEB_MONITORING"
        static let adld     = "ADLD"
        static let assuredBuyback = "Buyback"
    }
    
    enum HeaderViewIdentifiers {
        static let MHCResultAwesomeHeaderView = "MHCResultAwesomeHeaderView"
        static let MHCResultInfoHeaderView = "MHCResultInfoHeaderView"
        static let MHCResultScreenHeaderView = "MHCResultScreenHeaderView"
        static let MHCResultTestHeaderView = "MHCResultTestHeaderView"
        static let MHCResultTitleHeaderView = "MHCResultTitleHeaderView"
        static let MHCTestExecutionHeader = "TestExecutionHeader"
        static let MHCTestStatusHeaderView = "MHCTestStatusHeaderView"
        static let KnowMoreHeaderView = "KnowMoreHeaderView"
        static let KnowMoreFooterView = "KnowMoreFooterView"
        static let OfferHeaderView = "OfferHeaderView"
        static let GifterOptionsHeaderView = "GifterOptionsHeaderView"
    }
    
    enum CellIdentifiers {
        static let setReminderButtonBox = "SetReminderButtonBoxCell"
        static let centerList = "CenterListCell"
        static let dateTime = "DateTimeCell"
        static let setReminder = "SetReminderCell"
        static let cardOption = "CardOptionCell"
        static let offerFilter = "OfferFilterCell"
        static let adTransparent = "TransparentAdCell"
        static let imageTitleButton = "ImageTitleButtonCell"
        static let collectionOfferCell = "OfferCollectionCell"
        static let collectionAllOfferCell = "AllOffersCollectionCell"
        static let offerListCell = "OfferListCell"
        static let offerLoadMoreCell = "OfferLoadMoreCell"
        static let circularCollectionCell = "CircularCollectionCell"
        static let allATMsCell = "AllATMsCell"
        static let bankListCell = "BankListCell"
        static let collectionHomeApplianceCell = "HomeApplianceCollectionCell"
        static let notificationCell = "NotificationCell"
        static let haHeaderCollectionReusableView = "HAHeaderCollectionReusableView"
        static let scheduleVisitCollectionCell = "ScheduleVisitCollectionCell"
        static let scheduleVisitTimeSlotCell = "ScheduleVisitTimeSlotCell"
        static let whcSelectAdressCell = "WHCSelectAddressCell"
        static let whcSelectNewAdressCell = "WHCSelectNewAddressCell"
        static let addAnotherCardCell = "AddAnotherCardCell"
        static let partDamageCell = "PartDamageCell"
        static let MHCCategorySelectionCell = "MHCCategorySelectionCell"
        static let MHCCategoryPendingCell = "MHCCategoryPendingCell"
        static let MHCCategoryTestCell = "MHCCategoryTestCell"
        static let MHCBuybackFailedCell = "MHCBuybackFailedCell"
        static let MHCBuybackRemainingCell = "MHCBuybackRemainingCell"        
        static let MHCCategorySectionHeaderView = "MHCCategorySectionHeaderView"
        static let MHCCategoryPrimarySectionHeaderView = "MHCCategoryPrimarySectionHeaderView"
        
        enum WHCService{
            static let whatHappenedCell = "WhatHappenedCell"
            static let appliancesNotWorking = "AppliancesNotWorkingCell"
            static let CaughtFireCell = "CaughtFireCell"
            static let onGoingCell = "OngoingCell"
            static let finalInvoiceVerification = "FinalInvoiceVerification"
            static let uploadEstimationInvoiceCell = "UploadEstimationInVoiceCell"
            static let serviceCenterCall = "ServiceCenterCell"
            static let uploadImageCollectionViewCell = "UploadImageCollectionViewCell"
            static let imageCollectionTableViewCell = "ImageCollectionTableViewCell"
            static let documentImageCollectionViewCell = "DocumentImageCollectionViewCell"
            static let documentImageViewLabelCell = "DocumentImageViewLabelCell"
            static let serviceRequestUploadInvoiceTextCell = "ServiceRequestUploadInvoiceTextCell"
            static let ServiceRequestUploadInvoicePhotoCell = "ServiceRequestUploadInvoicePhotoCell"
            static let documentVerificationDetailsCell = "DocumentVerificationDetailsCell"
            static let repairAssessmentCell = "RepairAssessmentCell"
            static let timelineLabelCell = "TimelineLabelCell"
            static let documentVerificationTimelineCell = "DocumentVerificationTimelineCell"
            static let feedbackTableHeaderView = "FeedbackTableHeaderView"
            static let reuploadDocumentCell = "ReuploadDocumentCell"
            static let amountBreakupCell = "AmountBreakupCell"
            static let imageViewCell = "ImageViewCell"
            static let berDescriptionCell = "BERDescriptionCell"
            static let berInfoCell = "BERInfoCell"
            static let berTableHeaderView = "BERTableHeaderView"
            static let uploadDocumentsCell = "UploadDocumentsCell"
            static let findMyPhoneCell = "FindMyPhoneCell"
            static let singleSelectionAssest = "SingleSelectionAssest"
            static let multipleSelectionAssest = "MultipleSelectionAssest"
            static let rejectedClaimCell = "RejectedClaimCell"
            static let insuranceDecisionCell = "InsuranceDecisionCell"
            static let partsCell = "PartsCell"
            static let spExcessPaymentCell = "SPExcessPaymentCell"
            static let devicePickupCell = "DevicePickupCell"
            static let commentView = "CommentView"
            static let stackViewGroupedCell = "StackViewGroupedCell"
        }
    }
    
    enum CellHeight {
        static let cardOptionCell = 57
        static let myCardsCell = 110
        static let whcServiceCell = 80
    }
    
    enum NIBNames {
        static let mobileEmailAddView = "MobileEmailAddView"
        static let presentView = "PresentView"
        static let imageTitle = "ImageTitleCell"
        static let uploadDocumentsHeader = "UploadDocumentsHeader"
        static let cardTypeHeader = "CardTypeHeader"
        static let membershipActivationHeader = "MembershipActivationHeader"
        static let segmentedControlHeader = "SegmentedControlHeader"
        static let offerFilterHeader = "OfferFilterSectionHeaderView"
        static let allATMsCell = "AllATMsCell"
        static let bankListCell = "BankListCell"
        static let notificationCell = "NotificationCell"
        static let haHeaderCollectionReusableView = "HAHeaderCollectionReusableView"
        static let whcSelectAdressCell = "WHCSelectAddressCell"
        static let whcSelectNewAdressCell = "WHCSelectNewAddressCell"
        static let indicatorView = "IndicatorView"
        static let closureDateView = "ClosureDateView"
        static let invoiceReuploadHeader = "InvoiceReuploadHeader"
        static let leftRightLabelView = "LeftRightLabelView"
        static let leftImageRightTextView = "LeftImageRightTextView"
        static let startActivationBottomSheet = "StartActivationBottomSheet"
        
        enum WHCService{
            static let appliancesNotWorking = "AppliancesNotWorkingCell"
            static let whatHappenedCell = "WhatHappenedCell"
            static let CaughtFireCell = "CaughtFireCell"
            static let finalInvoiceVerification = "FinalInvoiceVerification"
            static let uploadEstimationInvoiceCell = "UploadEstimationInVoiceCell"
            static let serviceCenterCall = "ServiceCenterCell"
            static let selfRepairHeaderView = "SelfRepairHeaderView"
            static let serviceCenterView = "ServiceCenterView"
            static let ServiceRequestWhatHappenedHeaderView = "ServiceRequestWhatHappenedHeaderView"
            static let serviceRequestTellWhatWrongHeaderView = "ServiceRequestTellWhatWrongHeaderView"
            static let tableViewLabelHeader = "TableViewLabelHeader"
            static let serviceRequestUploadInvoiceTextCell = "ServiceRequestUploadInvoiceTextCell"
            static let ServiceRequestUploadInvoicePhotoCell = "ServiceRequestUploadInvoicePhotoCell"
            static let ServiceRequestUploadInvoiceHeaderView = "ServiceRequestUploadInvoiceHeaderView"
            static let singleSelectionAssest = "SingleSelectionAssest"
            static let multipleSelectionAssest = "MultipleSelectionAssest"
            
        }
    }
    
    enum ActivateVoucherURL
    {
        static let voucherCodeURL = Constants.SchemeVariables.baseDomain + "activatevoucher/"
    }
    
    enum PaymentURLs {        
        static let paymentSuccess = "paymentSuccess".lowercased()
        static let paymentFailure = "paymentFailure".lowercased()
        
        static let activateVoucherSuccess = "activationVoucherSuccess".lowercased()
        static let activateVoucherFailed = "activationVoucherFailure".lowercased()
        static let whcActivateVoucherSuccess = "schedule-inspection-details".lowercased()
        
        static let renewalPayment = Constants.URL.baseUrlOasys + "webservice/rest/api/renewals"
    }
    
    enum CardGenieCreds {
        static let username = "admin"
        static let password = "admin"
    }
    
    enum GoogleApi {
        static let mapsApi = "AIzaSyDSnsi3_Ky1fds7XFTg6-IV0abypqiSlVc"
    }
    
    enum MasterName {
        static let city = "city"
        static let state = "state"
        static let partner = "partner"
        static let partnerbu = "partnerbu"
        static let productTyp = "ProductTyp"
        static let frequency = "Frequency"
        static let relation = "Relation"
        static let paymentMod = "PaymentMod"
        static let contentTyp = "ContentTyp"
        static let mobileMake = "MobileMake"
        static let cardType = "CardType"
        static let mobileOS = "MobileOS"
        static let addressTyp = "AddressTyp"
        static let gender = "Gender"
        static let bankMst = "bankMst"
        static let salutation = "Salutation"
        static let warranty_Period = "Warranty_Period"
        static let fraudCover_Amount = "FraudCover_Amount"
    }
    
    enum OfferDistance {
        static let km2 = 2
        static let km5 = 5
        static let km10 = 10
        static let km10Plus = 20
    }
    
    enum RemoteConfig {
        static let defaultPlistName = "FirRemoteDefaultConfig"
        static let defaultExpirationTime: TimeInterval = 3600
    }
    
    enum Services: String {
        
        case fire = "HA_FR"
        case buglary = "HA_BR"
        case extendedWarranty = "HA_EW"
        
            // This includes HA_BD and SOP both. If, in the HA_BD service, isInsuranceBacked() is true then it is the case of HA_BD otherwise it is SOP.
        case breakdown = "HA_BD"
        
        case accidentalDamage = "HA_AD"
        case liquidDamage = "ADLD"
        case selfRepair = "Self_Repair"
        case peExtendedWarranty = "PE_EW"
        case peADLD = "PE_ADLD"
        case peTheft = "PE_THEFT"
        
        // In future, this might come for PE also, in that case create another key for that.
        case pms = "PMS"
        case buyback = "BUY_BACK"
        
        init?(rawValue: String) {
            switch rawValue {
                // This is HA EW case
            case "Extended Warran", "HA_EW":
                self = .extendedWarranty
            case "HA_FR":
                self = .fire
            case "HA_BR":
                self = .buglary
            case "HA_BD":
                self = .breakdown
            case "HA_AD":
                self = .accidentalDamage
            case "ADLD":
                self = .liquidDamage
            case "Self_Repair":
                self = .selfRepair
            case "PE_EW":
                self = .peExtendedWarranty
            case "PE_ADLD":
                self = .peADLD
            case "PE_THEFT":
                self = .peTheft
            case "PMS":
                self = .pms
            case "BUY_BACK":
                self = .buyback
            default:
                return nil
            }
        }
        
        // This is only for checking HA extended warranty.
        func isEWClaimType() -> Bool {
            return (self == .extendedWarranty || self == .selfRepair)
        }
        
        // If you want to check the services which are for WHC then for those below method should return true but above method should return false.
        
        
        func isHAClaimType() -> Bool {
            return self.getCategoryType() == Constants.Category.homeAppliances
        }
        
        func isPEClaimType() -> Bool {
            return self.getCategoryType() == Constants.Category.personalElectronics
        }
        
        func getCategoryType() -> String {
            
            let type: String
            switch self {
            case .fire:
                type = Constants.Category.homeAppliances
            case .buglary:
                type = Constants.Category.homeAppliances
            case .breakdown:
                type = Constants.Category.homeAppliances
            case .accidentalDamage:
                type = Constants.Category.homeAppliances
            case .extendedWarranty:
                type = Constants.Category.homeAppliances
            case .selfRepair:
                type = Constants.Category.homeAppliances
            case .peADLD:
                type = Constants.Category.personalElectronics
            case .peExtendedWarranty:
                type = Constants.Category.personalElectronics
            case .peTheft:
                type = Constants.Category.personalElectronics
            case .pms:
                type = Constants.Category.homeAppliances
            default:
                type = Constants.Category.homeAppliances
            }
            return type
        }
    }
    
    enum TimelineStage: String {
        
        case documentUpload = "DU"
        case verification = "VR"
        case insuranceDecision = "ID"
        case completed = "CO"
        case claimSettlement = "CS"
        case visit = "VE"
        case icDoc = "ICD"
        case repairAssessment = "RA"
        
        //TODO: verify below stage codes (not in master api)
        case deductablePayment = "DP"
        case scheduled = "SC"
        case partnerStageStatus = "PSS"
        case repair = "RP"
        case softApproval = "SA"
        
        // For PE:
        case pickUp = "PU"
        case deviceRepair = "RE"
        case delivery = "DE"
        
    }
    
    enum MembershipStatus {
        static let cancelled = "X"
        static let active = "A"
        static let inactive = "E"
        static let failed = "F"
    }
    
    enum PendencyStatus {
        static let complete = "Complete"
        static let incomplete = "Incomplete"
    }
    
    enum BankAccountVerifiactionStatus {
        static let upload = "U"
        static let reUpload = "RU"
        static let authorized = "A"
        static let pending = "P"
        static let reUploadPending = "RP"
    }
    
    enum TimelineStageStatus {
        static let completed = "CO"
        static let verificationPending = "VP"
        static let verificationUnsuccessful = "VU"
        static let documentUploadPending = "DUP"
        static let pending = "P"
        static let closed = "X"
        // on hold
        static let success = "SU"
        static let approved = "AP"
        static let slaExtended = "SE"
        static let spareNeeded = "SN"
        static let spareAvailable = "SPA"
        static let technicianCancelledCustomerUnavailable = "TCCA"
        static let technicianCancelledOther = "TCO"
        static let cancelledCustomerCancellation = "CACC"
        static let cancelledDueToExpiry = "CEXP"
        static let cancelledCustomerNotAvailable = "CCNA"
        static let cancelledTechnicianNotAvailable = "CTNA"
        static let inspectionFailed = "INF"
        static let rejected = "RJ"
        static let unresolved = "UNRESOLVED"
        static let ber = "BER"
        static let pickupPending = "PP"
        static let transport = "TR"
        static let accidentalDamage = "AD"
        static let successfully = "INCS"
        static let excessPaymentPending = "CPP"
        static let excessPaymentCompleted = "CPC"
        static let bera = "BERA"
        static let berr = "BERR"
        static let beras = "BERAS"
        static let berac = "BERAC"
        static let approvedStandby = "AS"
        static let appovedComplete = "AC"
        static let awaitingApproval = "AA"
        static let customerDeniedPayment = "CDP"
    }
    
    enum TimelineStatus {
        static let onHold = "OH"
        static let pending = "P"
        static let inProgress = "IP"
        static let completed = "CO"
        static let completeUnresolve = "CUNR"
        static let closeResolve = "CRES"
        static let closeReject = "CREJ"
    }
    
    enum PincodeCategory {
        static let dCategory = "D"
    }
    
    enum RatingType: String {
        case sr = "SR"
        case pe = "PE"
        case ha = "HA"
        case haSOD = "HA_SOD"
    }
    
    enum Claims{
        enum URL{
            static let membershipAssetDetail =  SchemeVariables.baseDomain + Claims.membership
            static let whatHappend = SchemeVariables.servicePlatform + Claims.serviceTasks + Claims.referenceCode
            static let address = SchemeVariables.baseDomain + Claims.address + Claims.getCustomerAddress + Claims.custId
        }
        static let membership = "/memberships/"
        static let assets = "/assets/"
        static let serviceTasks = "servicetasks?"
        static let referenceCode = "referenceCode="
        static let taskType = "&taskType="
        static let productVariantId = "&productVariantId="
        static let productVarientIds = "&productVariantIds="
        static let options = "&options="
        static let status = "&status="
        static let getCustomerAddress = "getCustomerAddress?"
        static let custId = "custId="
        static let pincode = "&pincode="
        static let address = "/myaccount/rest/customer/"
    }
    
    enum SOD {
        static let serviceRequestSourceType = "?serviceRequestSourceType=SOD"
        static let sodLabourCostTask = "LABOURCOST"
        static let sodGetIssuesTask = "ISSUE"
    }
    
    enum WHC {
        static let serViceRequestType = "?serviceRequestType=WHC_INSPECTION"
        static let webLeadCapture = "webToLeadCapture?"
        static let recoengine = "/recoengine/"
        static let serviceRequest = "servicerequests/"
        static let updateFeedback = "/updateFeedback"
        static let getFeedBack = "generickeysets/%@_FEEDBACK_CODES_"
        static let getAllPartnerBU = "/oacommons/partner/searchBusinessUnits?"
        static let updateMobEamil = "servicerequests/v2/update/submission/sr"
        static let getCityCode = "/oacommons/location/city?pincode="
        static let getJSFuncOld = "/static/my-account/generic-json-generator/claim-timeline/v1/timeline.js"
        static let getJSFunc = "/static/api-simplifier/dist/claim-timeline/v3/timeline.js?"

        enum URL {
            static let pincodeURL = SchemeVariables.servicePlatform + Pincode.pincodes
            static let pincodeAndAdressURL = SchemeVariables.baseDomain + "/myaccount/api/customer/"
            static let cityCodeURL = SchemeVariables.baseMainDomain + getCityCode
            static let serviceCenterListURL = SchemeVariables.baseMainDomain  + getAllPartnerBU
            static let updateMobileEmail = SchemeVariables.servicePlatform + updateMobEamil
            static let notifyURL = Constants.URL.baseMainUrlOasys + webLeadCapture
            static let suggestPlan = SchemeVariables.baseDomain + recoengine + SuggestPlan.suggestPlan
            static let planBenefitsWithRank = SchemeVariables.baseDomain + recoengine + PlanBenefits.planBenefits
            static let technicianDetail = SchemeVariables.oasysAdmin + InspectionDetail.technicianDetail
 
            static let technicianDetailImageURl = SchemeVariables.oasysAdmin
            static let startEndOTP = SchemeVariables.servicePlatform + InspectionDetail.startEndOTP
            static let inspectionDetailURL = SchemeVariables.servicePlatform + serviceRequest
            static let submitFeedbackURL = SchemeVariables.servicePlatform + InspectionDetail.submitFeedback
            static let getFeedbackURL = SchemeVariables.servicePlatform + getFeedBack

            static let getTimeLineJSURL =  SchemeVariables.baseCDNUrlForJS + getJSFunc
        }
        enum Pincode{
            static let pincodes = "pincodes/"
        }
        
        enum PlanBenefits {
            static let planBenefits = "plan/planbenefits"
        }
        
        enum Verify{
            static let phoneNumber = "phone_mobile="
            static let pinCode = "&whc_postalcode="
        }
        enum SuggestPlan{
            static let suggestPlan = "plan/suggestplans"
        }
        enum InspectionDetailsCell{
            static let customerCareNumber = RemoteConfigManager.shared.contactNumberCustomerCare.localized()
        }
        enum alertMessages{
            static let alertTitle  = "".localized()
            static let alertMessage = "Internet connection is weak, please try again.".localized()
            static let calendarMessage = "Event added to calendar".localized()
            static let settings = "Settings".localized()
            static let okay = "OK".localized()
            static let retry = "RETRY".localized()
        }
        
        
        enum technicianDetailsCell{
            static let shareOTP = "Please share OTP ".localized()
            static let shareOTPSecondPart =  " at the start of inspection with the technician".localized()
            static let shareOTPServiceSecondPart =  " at the start of service with the technician".localized()
            static let serviceExperience = "Service experience of ".localized()
            static let years = " years".localized()
        }
        enum InspectionDetail {
            static let technicianDetail = "webservice/security/users/"
            static let downloadTechnicianPicture = "/downloadProfilePicture"
            static let startEndOTP = "servicerequests/%@/authorizationCode/"
            static let submitFeedback = "servicerequests/%@/updateFeedback"
        }
        
    }
    
    enum PhoneConstants {
        static let height4s: CGFloat = 960
    }
    
    enum NavigationBarHeight {
        static let heightN: CGFloat = 50
    }
    
    enum MobileTabUrls {
        
    }
    
    enum WalletTabUrls {
        static let walletCards = Constants.SchemeVariables.baseDomain + "/myaccount/api/customer/"
        
        enum CardGenieUrls {
            static let offerCategories = Constants.SchemeVariables.baseDomain + "/cardgenie/offers/categories"
            static let nearestATM = Constants.SchemeVariables.baseDomain + "/cardgenie/places"
            static let nearestBankBranch = Constants.SchemeVariables.baseDomain + "/cardgenie/bankbranches"
            static let searchOffers = Constants.SchemeVariables.baseDomain + "/cardgenie/offers/v1/search"
            static let aggregatedSearchOffers = Constants.SchemeVariables.baseDomain + "/cardgenie/api/v1/aggregated-offers"
            static let offers = Constants.SchemeVariables.baseDomain + "/cardgenie/offers"
            static let bookmarkOffers = Constants.SchemeVariables.baseDomain + "/cardgenie/bookmarks"
            static let bankCards = Constants.SchemeVariables.baseDomain + "/cardgenie/cards/issuers"
        }
        
        static let siStatusURL = SchemeVariables.baseDomain + "/myaccount/api/membership/si/data?memUUID="
        static let subscriptionStatusURL = SchemeVariables.baseDomain + "/idfence-service/customer/checkSubscriptionStatus?subscriberNo="
    }
    
    enum MembershipUrls {
        static let customerDetails = Constants.URL.baseUrlOasys + "restservice/wsgetCustomerDetails"
        static let updateCustomerDetails = Constants.URL.baseMainUrlOasys + "restservice/v2/wsupdateCustomerDetails"
        static let getMembershipDetails = Constants.URL.baseUrlOasys + "webservice/rest/getCustomerMembershipdtl"
        static let getMobileDetails = Constants.URL.baseMainUrlOasys + "restservice/v2/wsgetCustomerMobileDetails"
        static let fetchBestPlans = Constants.URL.baseMainUrlOasys + "restservice/wsfetchBestPlans"
        static let getPlanBenefits = Constants.URL.baseMainUrlOasys + "restservice/wsgetPlanBenefit"
        static let getSubCategories = Constants.URL.baseMainUrlOasys + "restservice/wsgetSubcategories"
        static let getInvoices = Constants.URL.baseCustomer + "invoice"
        static let invoiceDownload = Constants.URL.baseCustomer + "invoice/download"
        static let insuranceCertificateDownload = Constants.URL.baseCustomer + "insuranceCertificate"
        static let getDocumentUrl = Constants.SchemeVariables.baseDomain + WHC.recoengine + "document/getDownloadUrl"
    }
    
    enum SODUrls {
        static let getServices =  Constants.SchemeVariables.baseDomain + WHC.recoengine + "product/v1/productservices"
        static let getProductSpecifications = Constants.SchemeVariables.baseDomain + WHC.recoengine + "product/v1/productspecifications"
        static let getSRsReportByPinCode = Constants.SchemeVariables.servicePlatform + "/pincodes"
    }
    
    enum Urls {
        static let getMasterData = "\(Constants.URL.baseMainUrlOasys)restservice/wsgetMasterData"
        static let postPaymentDetails = "\(Constants.URL.baseMainUrlOasys)webservice/rest/updatePostPaymentDetails"
        static let submitDocURL = "\(Constants.URL.baseMainUrlOasys)webservice/rest/docVerification/submitDoc"
        static let getAssets = "\(Constants.URL.baseMainUrlOasys)restservice/wsGetAssetMakes"
        static let getStateAndCity = "\(Constants.URL.baseMainUrlOasys)restservice/getStateAndCity"
        static let googleMapDistance = "https://maps.googleapis.com/maps/api/distancematrix/json"
        static let getAssetDetails = "\(Constants.URL.baseMainUrlOasys)restservice/v2/wsgetCustomerAssetDetails"
        static let activateVoucher = "\(Constants.SchemeVariables.baseDomainOld)/activatevoucher?mediumCode=app"
        static let updateCustDetails = "\(Constants.SchemeVariables.baseDomain)/myaccount/service/customer/updatecustdetail"
        static let buybackStatus = Constants.URL.baseUrlApiGateWay + "buyback/api/v1/customers/buybackStatus"
        static let mhcDataStore = Constants.URL.baseUrlApiGateWay + "buyback/mhc"
        static let userGroup = Constants.URL.baseUrlApiGateWay + "pandora-ws/api/v1/control-group?user_id="
        static let sendImageUploadLink = Constants.URL.baseUrlOasys + "webservice/rest/docVerification/sendImageUploadLink"
                
        enum SignUp {
            static let authGenerateOTP = Constants.URL.baseUrlApiGateWay + "user-service/auth/generateOTP"
            static let login = Constants.URL.baseUrlApiGateWay + "user-service/auth/login"
            static let logoutUser = Constants.URL.baseUrlApiGateWay + "auth-service/auth/token/logout" //will delete the login session from remote
            static let IMEIDedupeCheck = "\(Constants.URL.baseMainUrlOasys)restservice/wsIMEIDedupeCheck"
            static let privacyPolicy = "\(SchemeVariables.baseDomainOld)/privacypolicy"
            static let questionAnswer = Constants.SchemeVariables.baseDomain + "/myaccount/api/customer/"
        }
    }
    
    enum KeyValues {
        static let offerId = "offerId"
        static let offerCode = "offerCode"
        static let outletCode = "outletCode"
        static let status = "status"
    }
    
    enum LicenseKeys {
        
        static let gaKey = "UA-63625823-2"
    }
    
    enum WHCService{
        static let radioButtonActive = "radioButtonActive"
        static let radioButtonInactive = "radioButtonInactive"
    }
    
    enum WelcomeAlert{
        static let welcomeText = "Welcome to OneAssist. We have an amazing reward for you!"
        static let message = "You can unlock you rewards later on from your account section."
    }
    
    static let unlimitedServiceCount = 500
    static let unlimitedApplianceCount = Int(RemoteConfigManager.shared.unlimitedApplianceCount) ?? 500
    
    enum SessionIDs{
        static let modelDownloadSessionID = "background.download.session"
    }
}

enum Event {
    
    static let appsFlyersOnlyEvents: [Events] = [.afEventLogin, .afOpenedWithPN, .afUpdate, .afContentView, .afFirstPurchase, .afListView, .afReengage, .afPurchase]
    
    enum EventKeys: String {
        case name = "Name"
        case type = "Type"
        case number = "Number"
        case customerId = "CustomerId"
        case category = "Category"
        case expiryDate = "Expiring Date"
        case location = "Location"
        case appliance = "Appliance"
        case serviceType = "ServiceType"
        case questionNo = "Question Number"
        case additionalInvoice = "Additional Invoice"
        case position = "Position"
        case merchant = "Merchant"
        case merchantName = "Merchant name"
        case cardType = "Card Type"
        case result = "Result"
        case feedback = "Feedback"
        case comment = "Comment"
        case service = "Service"
        case appliancesName = "Appliance Name"
        case issueName = "Issue Name"
        case time = "Time"
        case date = "Date"
        case amount = "Amount"
        case handSetAmount = "Handset Amount"
        case handsetModel = "Handset Model"
        case paymentMode = "Payment Mode"
        case value = "Value"
        case numberOFappliance = "Number of Appliances"
        case coverAmount = "Cover Amount"
        case premiumAmount = "Premium Amount"
        case make = "Make"
        case warranty = "Warranty"
        case model = "Model"
        case IssuerName = "Issuer Name"
        case srNumber = "SR Number"
        case subcategory = "Sub-Category"
        case starRating = "Star Rating"
        case srId = "SrId"
        case status = "Status"
        case offerID = "Offer ID"
        case outletCode = "Outlet Code"
        
        case utmCampaign = "Campaign"
        case utmMedium = "Medium"
        case utmSource = "Source"
        
        case group = "Group"
        case screenName = "screen name"
        
        case action = "Action"
        case brandAndModel = "Brand and Model"
        case supported = "Supported"
        case Stage = "Stage"
        case serviceCenter = "Service Center"
        case dateTime = "Date Time"
        case consent  = "Consent"
        
        case bank = "Bank"
        case atm = "ATM"
        case plan = "Plan"
        case planDuration = "Plan Duration"
        case benefitName = "Benefit Name"
        case brand = "Brand"
        case serviceable = "Serviceable"
        case pincode = "Pincode"
        case cardIndex = "card number"
        case bankName = "Bank Name"
        case invoiceDate = "Invoice Date"
        case invoiceAmount = "Invoice Amount"
        
        case connection = "Connection"
        case connectionClosedOnError = "ConnectionClosedOnError"
        case stanzaId = "StanzaId"
        
        case count = "Count"
        case cost = "Cost"
        case screenDuration = "ScreenDuration"
        case serviceId = "serviceId"
        case email = "email"
        case mobile = "mobile"
        
        case activity = "Activity";
        case netAmount = "Net amount";
        case product = "Product";
        case price = "Price";
        case planName = "Plan Name";
        case planCode = "Plan Code";
        case buName = "BU name";
        case bpName = "BP name";
        case subCat = "Sub Category";
        case userType = "User type";
        case link = "link"
        case isEnabled = "isEnabled"
        case pushPermission = "pushPermission"
        case customLink =  "customLink"

        case emailId = "Email Id"
        case bp = "BP"
        case bu = "BU"
        case membershipStatus = "Membership Status"
        case pagePlanName = "pagePlanName"
        case pagePlanCode = "pagePlanCode"
        case hasPlan = "hasPlan"
        case blogTitle = "blogTitle"

        case contentType = "content_type"
        case contentId = "content_id"
        case currency = "currency"
        case contentList = "content_list"

        case mode = "Mode"
        case language = "Language"
        case option = "Option"
        case voice = "Voice"
        case declaration = "Declaration"
        case tags = "Tags"
        case videoProgress = "Video Progress"
        case screen = "Screen"
        case remainingTries = "Remaining Tries"
        case reason = "Reason"
        case downloadSpeed = "Download Speed"
        case downloadCompleted = "Download Completion Percentage"
    }
    
    enum Screen: String {
        case mobileHome =  "Mobile Home"
        case walletHome = "Wallet Home"
        case appliancesHome = "Appliances Home"
        case accounts = "Account"
        case safetyMeasure = "Covid Know More"
        case profile = "Profile"
        case activateVoucher = "Activate Voucher"
        case fAQ = "FAQ"
        case mobileFAQ = "Mobile FAQ"
        case walletFAQ = "Wallet FAQ"
        case claimFAQ =   "Claim FAQ"
        case generalFAQ = "General FAQ"
        case chat = "Chat"
        case privacyPolicy = "Privacy policy"
        case inbox = "Inbox"
        case menu = "Menu"
        case startMHC  = "Start MHC"
        case MHCResult = "MHC Result"
        case mobileMyProduct = "Mobile_My_Product"
        case mobileProdDetail = "Mobile_Prod_Details"
        case nearbyATM = "Nearby ATM"
        case bankATM  = "Bank ATM"
        case nearbyBanks  =  "Nearby Banks"
        case Bankbranch  = "(HDFC) Bank branches"
        case offerDetails  = "Offer Details"
        case allOffersOnline =  "All Offers - Online"
        case allOffersNear  = "All Offers - Nearby"
        case bookmarkedOffers  = "Bookmarked Offers"
        case OfferFilter = "Offer Filter"
        case MyWallet = "My Wallet"
        case AddCard  = "Add Card"
        case CardType = "Card Type"
        case IssuerName = "Issuer Name"
        case WHC_Pincode = "WHC_Pincode"
        case WHCNoofAppliances = "WHC_No_of _Appliances"
        case WHCPlan = "WHC_Plan"
        case WHCPayment = "WHC_Payment"
        case HomeEWSelectApp = "HomeEW_Select_Appliance"
        case HomeEWAppliance_ = "HomeEW_Appliance_Details"
        case HomeEWPlan = "HomeEW_Plan"
        case HomeEWApplianceSpecs = "HomeEW_Appliance_Specs"
        case HomeEWAddress = "HomeEW_Address"
        case HomeEWRegister = "HomeEW_Register"
        case HomeEWPayReview = "HomeEW_Pay_Review"
        case ApplianceDetails = "Appliance_Details"
        case SR_Ongoing = "SR_Ongoing"
        case SR_History = "SR_History"
        case SR_Raise = "SR_Raise"
        case SR_Pincode = "SR_Pincode"
        case SR_Address = "SR_Address"
        case SR_ServiceType = "SR_ServiceType"
        case SR_Appliance = "SR_Appliance"
        case SR_Issue = "SR_Issue"
        case SR_Issue_DateTime = "SR_Issue_DateTime"
        case SR_Schedule_Visit =  "SR_Schedule_Visit"
        case SR_Timeline = "SR_Timeline"
        case SR_Negative_Feedback = "SR_Negative_Feedback"
        case SR_Decription = "SR_Decription"
        case SR_Upload_Image = "SR_Upload_Image"
        case SR_Upload_FIR = "SR_Upload_FIR"
        case SR_Plan = "SR_Plan"
        case EnterMobileNumber = "Enter Mobile Number"
        case HandsetDetails = "Handset Details"
        case Mobile_Plan = "Mobile_Plan"
        case Mobile_Address = "Mobile_Address"
        case Mobile_Register = "Mobile_Register"
        case Mobile_Pay_Review = "Mobile_Pay_Review"
        case WalletProtection = "Wallet Protection"
        case Wallet_Plan = "Wallet_Plan"
        case Wallet_Address = "Wallet_Address"
        case Wallet_Register = "Wallet_Register"
        case Wallet_Pay_Review = "Wallet_Pay_Review"
        case SR_Sim_Block = "SR_Sim_Block"
        case SR_Bank_Details = "SR_Bank_Details"
        case SR_Damaged_Part = "SR_Damaged_Part"
        case Home = "Home"
        case Buy = "Buy"
        case Membership = "Membership"
        case MyRewards = "MyRewards"
        case MyProfile = "MyProfile"
    }
    
    enum EventParams {
        static let trueValue = "true"
        static let falseValue = "false"
        static let cancel = "Cancel"
        static let submit = "Submit"
        static let notGood = "Not Good"
        static let awesome = "Awesome"
        static let viewOffer = "View Offer"
        static let favouriteOffer = "Mark Favorite"
        static let visitServiceCenter = "Visit Service Center."
        static let allowToCreateEvent = "Please allow OneAssist to create event in your calendar"
        static let mhcHome = "MHC"
        static let buyback = "Buyback"
        static let resultScreen = "Result_Screen"
        static let cardRegistration = "Card Registration"        
        static let all = "All"
        static let login = "Login"
        static let walletTab = "Wallet tab"
        static let mobileTab = "Mobile tab"
        static let homeTab = "Home Tab"
        static let mobile = "Mobile"
        static let wallet = "Wallet"
        static let home = "Home"
        static let homeew = "Home EW"
        static let atm = "Atm finder Screen"
        static let bank = "Bank branch finder Screen"
        static let offline = "Offline"
        static let online = "Online"
        static let nearby = "nearby"
        static let sideMenu = "Side Menu"
        static let walletScreen = "wallet Screen"
        static let threeDotMenu = "Three dot menu"
        static let saleFlow = "Sales Flow"
        static let activateVoucher = "Activate Voucher"
        static let addcardScreen = "Add Card Screen"
        static let activation = "Activation"
        static let purchase = "Purchase"
        static let success = "Success"
        static let failure = "Failure"
        static let cardScreen = "My cards Screen"
        static let whc = "HomeAssist"
        static let exploreHA = "Explore_HA"
        static let exploreHomeEW = "Explore_Home_EW"
        static let explore = "Explore"
        static let mhcMic = "MHC_Mic"
        static let mhcCamera = "MHC_Camera"
        static let mhcGPS = "MHC_GPS"
        static let allow = "Allow"
        static let deny = "Deny"
        static let settings = "Settings"
        static let close = "Close"
        static let membershipTab = "Membership Tab"
        static let membershipDetails = "Membership details"
        static let idFenceTrial = "ID Fence Trial"
        static let idFencePremium = "ID Fence Premium"
        static let bookMarkedOffers = "Bookmarked Offers"
        static let deeplinks = "Deeplinks"
        static let planListing = "Plan Listing"
        static let planDetail = "Plan Detail"
        static let IDFenceTrialFooter = "Trial Footer"
        static let IDFenceTrialBottomsheet = "Trial Bottomsheet"
        static let allOffers = "All Offers"
        static let nearbyOffer  = "Nearby Offer"
        static let ATM  = "ATM"
        static let bankBranch  = "Bank Branch"
        static let instructionScreen = "Instruction Screen"
        static let basicDetails = "Basic Details"
        static let mobileDetectionScreen = "Mobile Detection Screen"
        static let imeiScreen = "IMEI Screen"
        static let previewScreen = "Preview Screen"
        static let moveToSDTScreen = "MoveToSDT Screen"
        static let errorScreen = "Error Screen"
        static let frontPanelPreview = "Front Panel Preview"
        static let backPanelInstruction = "Back Panel Instruction"
        static let mobileFDActivation = "Mobile FD Activation"
    }
    
    enum Events: String {
        
        case appOpen = "App Open"
        case pushEnabled = "Push Enabled"
        case enterNumberScreenview = "Enter Number Screenview"
        case verifyButtonTapped = "Verify button tapped"
        // case customerLoggedIn = "Customer LoggedIn"
        case chatOnHomeTapped = "chat on home tapped"
        case chatonMembershipDetailTapped = "char on membership detail tapped"
        case addCardFromAddCardScreenTapped = "Add card from addcardscreen tapped"
        case backToHomeFromGAP = "back_to_home_from_GAP"
        case claimEnquiryTapped = "Claim enquiry tapped"
        case skipOnSplashTapped = "skip_on_Splash_tapped"
        case logoutTapped = "Logout tapped"
        case mobileSuggestAPlanTapped = "Mobile suggest a plan  tapped"
        case letsGetStarted = "Let's get started"
        case letsGetStarted2 = "Lets_get_started"
        case atmFinderFromHome = "Atm_finder_from_home"
        case homeSuggestAPlanTapped = "Home see best plan  tapped"
        case offerTapped = "offer tapped"
        case registerSuggestPlan = "Register (Suggest Plan)"
        case walletSuggestAPlanTapped = "Wallet see best plan  tapped"
        case shareTapped = "Share tapped"
        case getProtectedTapped = "Get protected tapped"
        case callBankTapped = "Call bank tapped"
        case customerRegister = "Customer register"
        case funnelTappedWithNameOfCategory = "Funnel tapped with name of category"
        case shareOnOffer = "Share on offer"
        case callOnHomeTapped = "call_on_home_tapped"
        case verifyPhoneNumber = "Verify phone number"
        case activateVoucherFromLogin = "Activate voucher"
        case resendOTPTapped = "resend OTP tapped"
        case suggestAPlanSuccess = "See best plan"
        case suggestAPlan = "See best plan tapped"
        case chatTapped = "Chat tapped"
        case walletTapped = "Wallet tapped"
        case mobileTapped = "Mobile tapped"
        case homeTapped = "Home tapped"
        case myAccountTapped = "my account tapped"
        case planBenefitsTapped = "Plan benefits tapped"
        case myMobileDetailsTapped = "My mobile details tapped"
        case renewButtonTapped = "Renew button tapped"
        case upgradeIDFenceTapped = "Upgrade ID Fence"
        case giftNowTapped = "Gift now tapped"
        case showAllCardOffers = "Show All Card Offer tapped"
        case cardOffersTapped = "Card Offer tapped"
        case addCardTapped = "Add card tapped"
        case mhcTapped = "MHC tapped"
        case findBankAtm = "Find bank Atms"
        case findBankBranch = "Find bank branch"
        case myCardsTapped = "My cards tapped"
        case iAmInterestedTapped = "I am interested tapped"
        case goToWalletTapped = "Go To Wallet tapped"
        case offerItemTapped = "offer item tapped"
        case funnelIconTapped = "Funnel icon tapped"
        case filterApplyTapped = "Filter apply tapped"
        case firstDistanceTapped = "5Km from offer tapped"
        case secondDistanceTapped = "10Km from offer tapped"
        case thirdDistanceTapped = "15Km from offer tapped"
        case fourthDistanceTapped = "More than 15 Km from offer tapped"
        case offerUrlTapped = "Offer url tapped"
        case shareOfferTapped = "share a offer tapped"
        case getDirectionTapped = "Get direction tapped"
        case bookmarkAnOffer = "Bookmark an offer"
        case profileTapped = "Profile from side menu tapped"
        case giftPlanTapped = "Gift protection plan from side menu tapped"
        case shareAppTapped = "share from side menu tapped"
        case bookmarkedOfferTapped = "Bookmark offer tapped"
        case faqTapped = "FAQ from side menu tapped"
        case rateTapped = "Rate OneAssist tapped"
        case getDirectionBankBranch = "get direction from bank branch"
        case callBankBranch = "Call your Bank"
        case pencilTappedATM = "pencil tapped from ATM"
        case getDirectionATM = "Get direction from ATM"
        case viewAllATM = "View_All_ATM"
        case voucherActivated = "voucher activated"
        case upgradedSuccessfully = "upgrade successfully"
        case renewedSuccessfully = "renewed successfully"
        case whcPlanBenefitsTapped = "whc plan benefit tapped"
        case callYourBankTapped = "Call your bank tapped"
        case removeCardTapped = "Remove card tapped"
        //            case appOpened = "App opened"
        case verifyPincodeTapped = "Verify pincode tapped"
        case notifyMeTapped = "Notifyme from whc tapped"
        case seeBestPlanTapped = "see best plans tapped"
        // case coverAmountChanged = "cover amount changed"
        case whcSuccessfullyPurchased = "WHC succesfully purchased"
        //case scheduleInspectionTapped = "Schedule Inspection"
        case rescheduleInspectionTapped = "reschedule inspection tapped"
        case addNewAddress = "add a new address tapped"
        case submitRequestInspection = "submit inspection request tapped"
        //case showInspectionDetails = "show inspection details tapped"
        case callServiceTapped = "call service center tapped"
        case chatFromInspectionTapped = "chat from inspection assesment tapped"
        case ratingSubmitted = "Rating Submitted"
        case sendFeedBackTapped = "Send feedback tapped"
        case addTocalendarTapped = "add to calendar tapped"
        case myAppliancesDetailsTapped = "my appliance details tapped"
        case callOATapped = "call OA tapped"
        
        //////////////////////////////////
        case location = "Location"
        case bookMarkedOffers = "Bookmarked Offers"
        case markFavourite = "Mark Favourite"
        case offerUrl = "Offer URL"
        case myCards = "My Cards"
        case calculateRisk = "Calculate Risk"
        case riskCalculatorOutside = "Risk Calculator outside"
        case addCard = "Add Card"
        case cardAdded = "Card Added"
        case removeCard = "Remove Card"
        case findATM = "Find ATM"
        case editAtm = "Edit ATM"
        case findBanks = "Find Banks"
        case selectBank = "Select Bank"
        case mhcCheckNow = "MHC Check Now"
        case startMHC = "Start MHC"
        case startMHCScreenview = "Start MHC Screenview"
        case cancelMHC = "Cancel MHC"
        case MHCComplete =  "MHC Complete"
        case MHCShare = "MHC Share"
        case MHCSummary = "MHC Summary"
        case MHCCheckAgain = "MHC Check Again"
        case MHCRetry = "MHC Retry"
        case mobileBuybackIntent = "Mobile Buyback Intent"
        case VerifyMobileNumber = "Verify Mobile Number"
        case loginSkip = "Login Skip"
        case OTPVerify = "OTP Verify"
        case submitHandsetDetails = "Submit Handset Details"
        case activateVouchers = "Activate Voucher"
        case mobile = "Mobile"
        case wallet = "Wallet"
        case home = "Home"
        case chat = "Chat"
        case menu = "Menu"
        case offerDetails  = "Offer Details"
        case offerDetailsApiFailed = "Offer Details API Failed"
        case account = "Account"
        case profile = "Profile"
        case planBenefits = "Plan Benefits"
        case threeDotMenu = "Three Dot Menu"
        case customerLoggedIn = "Customer LoggedIn"
        case inBox = "Inbox"
        case giftNow = "Gift Now"
        case giftAPlan = "Gift a Plan"
        case resendOtp = "Resend OTP"
        case logout = "Logout"
        case offlineOffer = "Offline Offer"
        case FAQ = "FAQ"
        case onlineOffers = "Online Offer"
        case backFromGap = "Back from GAP"
        case gotToWallet = "Go to Wallet"
        case renew = "Renew"
        case coverAmountChanged = "Cover amount changed"
        case myApplianceDetails = "My Appliance Details"
        case applianceDetails = "Appliance Details"
        case rateApp = "Rate app"
        case shareApp = "Share app"
        case showInspectionDetails = "Show Inspection details"
        case upgrade = "Upgrade"
        case callYourBank = "Call your bank"
        case offerFilter = "Offer Filter"
        case getDirection = "Get Direction"
        case seeAllOffers = "See All Offers"
        case SR = "SR"
        case raiseSR = "Raise SR"
        case SRDetail = "SR Detail"
        case SRLetsBegin = "SR Lets Begin"
        case SRPincodeContinue = "SR Pincode"
        case SRAddressContinue = "SR Address"
        case callButtonForCustomerCareTapped = "Click on call customer care button"
        case callButtonForTechnicianTapped = "Click on call technician button"
        case chatButtonTapped = "SR Chat"
        case rateUs = "Rate Us"
        case ratingPopUp = "Rating Popup"
        case bankSelectDropdown = "Bank Select Dropdown"
        case bankSelect = "Bank Select"
        case bankSelected = "Bank Selected"
        
        // FD Cases
        case continueActivation = "Continue Activation"
        case activationWelcome = "Activation Welcome"
        case postPurchaseContinue = "Post Purchase Continue"
        case seeHow = "See How"
        case galleryPermission = "Storage Permission"
        case verifyIMEI = "Verify IMEI"
        case seeTutorial = "See IMEI Tutorial"
        case callForIMEI = "Call for IMEI"
        case imeiNotCaptured = "IMEI Not Captured"
        case sendUploadLink = "Send Upload Link"
        case viewVerificationScreen = "View Verification Screen"
        case resendSMS = "Resend SMS"
        case invoiceReupload = "Invoice ReUpload"
        case seeSamplePhoto = "See Sample Photos"
        case seeVideo = "See Video"
        case continueVoiceSupport = "Continue Voice Support"
        case enableVoiceSupport = "Enable Voice Support"
        case skipVoiceSupport = "Skip Voice Support"
        case verifyThreeDot = "Verification Three Dot"
        case frontImageCapture = "Front Image Captured"
        case frontImageNotCaptured = "Front Image Not Captured Screenview"
        case retakeFrontImage = "Retake Front Image"
        case chatFraudDetection = "Chat Fraud Detection"
        case continueToBackPanel = "Continue To Back Panel"
        case backImageCaptured = "Back Image Captured"
        case submitImages = "Submit Images"
        case ratingFeedback = "Rating Feedback"
        case recoClick = "Reco Click"
        
        case ratingfeedback = "Rating feedback"
        case SRPlanSelect = "SR Plan Select"
        case SRChat = "sr Chat"
        case SRCrossButton = "SR Cross Button"
        case SRPincodeEdit = "SR Pincode Edit"
        case SRServiceTypeSubmit = "SR ServiceType Submit"
        case SRApplianceSubmit = "SR Appliance Submit"
        case SRIssueSubmit = "SR Issue Submit"
        case SRDT_Submit = "SRDT_submit"
        case SRVisitDT_Submit = "SR Visit DT_Submit"
        case SR_calendar = "SR Calendar"
        case SRUploadDoc = "SR Upload Doc"
        case SRDocSubmit = "SR Doc Submit"
        case ScheduleVisit = "SR Schedule Visit"
        case SRCallCC = "SR Call CC"
        case SRCallTechnician = "SR Call Technician"
        case SRTechnicianReached = "SR Technician Reached"
        case SRDescription = "SR Description"
        case SRViewDetails = "SR View Details"
        case SRExcessPay = "SR Excess Pay"
        case SRContactUs = "SRContactUs"
        case SRReschedule = "SRReschedule"
        case SRDetailSubmit = "SRDetailSubmit"
        case  SREstimationInvoice = "SREstimationInvoice"
        case  SRFinalInvoice = "SRFinalInvoice"
        case  FindAPlan = "Find a Plan"
        case  Address = "Address"
        case  Register = "Register"
        case PayNow = "Pay Now"
        case ApplyPromo = "Apply Promo"
        case TnCCheck  = "TnC Check"
        case SelectNoOfAppliance  = "Select No. of Appliance"
        case GetProtected  = "Get Protected"
        case SelectAppliance  = "Select Appliance"
        case ApplianceSpecs  = "Appliance Specs"
        case bankBranchFinder = "Bank Branch Finder"
        case atmFinder = "ATM Finder"
        case mhc = "MHC"
        case deeplink = "Deeplink"
        
        //          case GetDirection  = "Get Direction"
        
        case NotifyMe  = "Notify Me"
        case PlanPurchased  = "Plan Purchased"
        case  PlanUpgraded  = "Plan Upgraded"
        case planActivated = "Plan Activated"
        case PlanRenewed  = "Plan Renewed"
        case FreeTrial  = "Free Trial"
        case  ScheduleInspection  = "Schedule Inspection"
        case ViewInspectionDetails  = "View Inspection Details"
        case RescheduleInspection  = "Reschedule Inspection"
        case ActivationSuccess  = "Activation Success"
        case alloffer = "All Offers"
        //EW
        case SRTapped = "SR Tapped"
        case SRRaised = "SR Raise"
        
        
        case SRTellUsMore = "SR Tell Us More"
        case submitRequestClicked = "Submit request clicked after selecting date"
        case uploadDocumentsClicked = "Upload documents clicked"
        case historyTappedClicked = "History tapped clicked"
        case submitDocumentsClickedAfterUploadingImage = "Submit documents clicked after uploading image"
        case SRCardTappedViewDetails = "SR card tapped to view details"
        case submitRequestClickedAfterSchedulingVisit = "Submit request clicked after scheduling visit"
        case addToCalendarClicked = "Add to calendar clicked"
        case technicianReachedResponse = "Technician reached response(Yes/Not yet)"
        
        case ratingFeedbackTapped = "Rating feedback tapped"
        case submitRequestTappedAfterReschedule = "Submit request tapped after reschedule"
        case submitClickedAfterDocReupload = "Submit clicked after doc reupload"
        case uploadEstimationInvoiceSubmit = "Upload Estimation invoice submit"
        case uploadFinalInvoiceSubmit = "Upload Final invoice submit"
        
        //HOME SERVE
        case SRSelectApplianceSubmit = "SR Select Appliance Submit"
        case SRWhatHappenedSubmit = "SR What Happened Submit"
        case SRIncidentDecriptionSubmit = "SR Incident Decription Submit"
        case SRDeductiblePaymentSubmit = "SR Deductible Payment Submit"
        
        // Service degitization
        case srDtSubmit = "SR DT_Submit"
        case srContactUs = "SR ContactUs"
        case srReschedule = "SR Reschedule"
        case srDetailSubmit = "SR Detail Submit"
        case srEstimationInvoice = "SR Estimation Invoice"
        case srFinalInvoice = "SR Final Invoice"
        case srInProgress = "SR In Progress"
        case srComplete = "SR Completed"
        case srReupload = "SR ReUpload"
        case srSIMBlockDetails = "SR SimBlock Details"
        case srBankDetails = "SR Bank Details"
        case srBankDetailsSubmit = "SR Bank Details Submit"
        case rateNow = "Rate Now"
        case notNow = "Not Now"
        case srDamagePartSubmit = "SR Damaged Part Submit"
        
        case proceedToPay = "Proceed To Pay"
        case chooseAnotherPlan = "Choose Another Plan"
        case viewAppliances = "View Appliances"
        case viewBenefits = "View Benefits"
        
        // MHC
        case micPermission = "Mic Permission"
        case micSetting = "Mic Settings Rationale"
        case cameraPermission = "Camera Permission"
        case cameraSetting = "Camera Settings Rationale"
        case locationPermission = "Location Permission"
        case locationSetting = "Location Settings Rationale"
        
        case memberships = "Memberships"
        case buyTab = "Buy Tab"
        case accountTab = "Account Tab"
        case homeTab = "Home Tab"
        
        case planDetails = "Plan Details"
        
        //Walkin
        case walkinSCList = "Walkin SC List"
        case SCMap = "SC Map"
        //            case getDirection = "Get Direction"
        case callServiceCenter = "Call Service Center"
        case setReminder = "Set Reminder"
        case BERConsent = "BER Consent"
        
        // Sales Revamp
        case buySubTab = "Buy Subtab"
        case findAPlan = "Find a plan"
        case seeBestPlans = "See Best Plans"
        case selectPlan = "Select Plan"
        case viewDetails = "View Details"
        case comparePlan = "Compare Plan"
        case comparePlanContinue = "Compare Plan Continue"
        case plansFAQ = "Plans FAQ"
        case planWhatsNotCovered = "Plan Whats Not Covered"
        case planExclusiveBenefit = "Plan Exclusive Benefit"
        case IDFence_Blog_Subscription = "IDFence_Blog_Subscription"
        case proceedPayment = "Proceed Payment"
        case pincodeServiceability = "Pincode Serviceability"
        case knowMoreIDFence = "IDFence Premium Plan Know More"
        case whyNeedThisSwipe = "IDFence Why Need This Swipe"
        case scratchCardSeen = "ID Scratch Card View"
        case scratched = "ID Scratch Card Unlock"
        case claimButtonTapped = "ID Scratch Card Claim"
        case IDScretchCardCross = "ID Scratch Card Cross"
        case InvoiceDeclaration = "Invoice_Declaration"
        case rewardUnlocked = "Reward Unlocked"
        case noPlanFound = "No Plans Found"
        case deviceAgeExceeded = "Device age Exceeded"
        case deviceBrand = "Device Brand"
        case deviceInvoiceAmount = "Device Invoice Amount"
        case idFenceBlogTapped = "IDFence_Blog_Card_Tap"
        case idFenceShareTap = "IDFence_Share_Tap"
        
        //Chat Event
        case websocketConnection = "WebSocket Connection"
        case agentMessageReceived = "Agent message received"
        
        // SOD Event
        case sodProductServiceScreen = "SOD Product Service Screen"
        case sodProductScreen = "SOD Product Screen"
        case sodProceedToBook = "SOD Proceed to Book"
        case sodSRDetail = "SOD SR Detail"
        case mobileEmailSubmission = "Mobile Email Submission"
        case sodDetailsSubmit = "SOD Details Submit"
        case payment = "Payment"
        
        case planDetailPageLoad = "plan_detal_page_load"
        case planDetailPrimaryCTATap = "Plan Detail Page Primary CTA Tap"
        case idFencePlanVideoTap = "IDFence_Plan_Detail_Page_Video_Tap"
        case pushPermission = "Notification Permission"
        case pushPermissionRationale = "Notification Permission Rationale"
        case pushSettintRationale = "Notification Setting Rationale"

        case afEventLogin = "af_login" // declared AFEventLogin in AppsFlyerLib.h file
        case afOpenedWithPN = "af_opened_from_push_notification" // declared AFEventOpenedFromPushNotification in AppsFlyerLib.h file
        case afReengage = "af_re_engage" // declared AFEventReEngage in AppsFlyerLib.h file
        case afPurchase = "af_purchase" // declared AFEventPurchase in AppsFlyerLib.h file
        case afUpdate = "af_update" // declared AFEventUpdate in AppsFlyerLib.h file
        case afContentView = "af_content_view" // declared AFEventContentView in AppsFlyerLib.h file
        case afFirstPurchase = "first_purchase"
        case afListView = "af_list_view" // declared AFEventListView in AppsFlyerLib.h file

        // Membership Documents
        case ekitDownloaded = "E-Kit Downloaded"
        case invoiceDownloaded = "Invoice Downloaded"
        
        case startPlanActivation = "Start Plan Activation"
        case activationSetupScreen = "Activation Setup Screen"
        case activationCompletionPushSent = "Activation Complete Push Sent"
        case activationPushClicked = "Activation Complete Push Click"
        case continueFromSampleVideo = "Continue from Sample Video"
        case skipFromSampleVideo = "Skip from Sample Video"
        case mobileDetection = "Mobile Detection"
        case imeiScreen = "IMEI Screen"
        case downloadComplete = "Download Complete"
        case retakeBackImage = "Retake Back Image"
        case submitImageConfirmation = "Submit Image Confirmation"
        case poorInternetScreenview = "Poor Internet Screenview"
        case poorInternetRetry = "Poor Internet Retry"
        case moveTOSDTScreenView = "MoveToSDT Screenview"
        case moveToSDT = "MoveToSDT"
        case noInternetPopup = "No Internet Popup"
    }
    
    enum UserAttribute {
        static let issuerName = "Issuer Name"
        static let appInstallDate = "App install date"
    }
}

extension Notification.Name {
    public static let refreshViewController = Notification.Name("refreshViewController")
    public static let membershipTab = Notification.Name("MembershipTab")
    public static let memTabWithoutAPI = Notification.Name("MemTabWithoutAPI")
    public static let membershipActivationFDVC = Notification.Name("MembershipActivationFDVC")
    public static let refreshMemSRStatus = Notification.Name("refreshMemSRStatus")
    public static let refreshMemBuybackStatus = Notification.Name("refreshMemBuybackStatus")
    
    public struct Claim {
        public static let loadandRefreshOngoingTab = Notification.Name("LoadandRefreshOngoingTab")
        public static let loadandRefreshOngoingTabAndHistory = Notification.Name("LoadandRefreshOngoingTabAndHistory")
        public static let loadandRefreshOngoingTabAndPushTimeLineForServiceRequest = Notification.Name("LoadandRefreshOngoingTabAndPushTimeLineForServiceRequest")
    }
    public static let appDidBecomeActive = Notification.Name("appDidBecomeActive")
}

protocol ClaimType {
    func getServiceCode() -> String
}
enum Publisher : String {
    case NewPushNotificationReceived
    
    func publish()  {
        NotificationCenter.default.post(name:Notification.Name(self.rawValue), object: nil)
    }
    func observe(observer: Any ,selector : Selector ) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: Notification.Name(self.rawValue), object: nil)
    }
    func remove(observer: Any)  {
        NotificationCenter.default.removeObserver(observer, name:  Notification.Name(self.rawValue), object: nil)
    }
}

let kDefaultTimeForOTP  = 15
let kPageSizeForOffer  = 10
enum DocumentType: String {
    case customer = "CUSTOMER"
    case claim = "CLAIM"
    case asset = "ASSET"
    
}

//enum OATabs:Int{
//    case home=0, buyNow, memberships, account
//}

enum TabIndex: Int {
    //    case mobile = 8
    //    case wallet = 7
    //    case home = 6
    //    case serviceRequest = 4
    //    case more = 5
    //
    case newHome = 0
    case buy = 1
    case membership = 2
    case account = 3
}

//Payment
enum PaymentConstants {
    enum PaymentMode:String{
        case online = "ONLINE"
        case deferred = "DEFERRED"
    }
}

//custType

enum CustType {
    static let  nonSubscriber = "NON_SUBSCRIBER"  //user has verified the number but have no membership-----(comming from api)
    static let  subscriber = "SUBSCRIBER"  //user has verified the number and have a membership----(comming from api)
    static let  notVerified = "NOT_VERIFIED"  //if user skip the number verification----(for internal use)
}

//FD Test Type

enum FDTestType: String {
    case MT = "Mirror"
    case SDT = "Secondary Device"
    
    func getAPIValue() -> String {
        switch self {
        case .MT:
            return "MT"
        case .SDT:
            return "SDT"
        default:
            return ""
        }
    }
}

//Use this to identify from which screen or VC we are move to current screen
enum FromScreen: String {
    case PEPaymentSuccessScreen = "PEPaymentSuccessScreen"
    case fromBookMark = "Bookmarked Offers"
    case fromAllOffers = "All Offers"
    case fromOfferScreen  = "Offer Screen"
    case fromThreeDotMenu = "Three Dot"
    case fromAccount = "Account Tab screen"
    case offerDetailScreen = "Offer Detail Screen"
    case wallet = "Wallet"
    case deepLink = "Deeplink"
    case mobile = "Mobile"
    case fromHome = "Explore"//Home Tab
    case myReward = "My Rewards"
    case fromhome = "Home"
    case fromMembership = "Membership"
    case blank = ""
}

//Module squancing
enum FlowSequence {
    enum SODFow:String{
        case PRODUCT_SCREEN = "PRODUCT_SCREEN"
        case PINCODE_SCREEN = "PINCODE_SCREEN"
    }
}

