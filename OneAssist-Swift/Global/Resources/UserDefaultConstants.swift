//
//  UserDefaultConstants.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/12/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

enum UserDefaultsKeys: String {
//    case sessionId
    case tempCustomer
    case deviceToken
    case deviceTokenData
    
    //TODO: This key is used at one more place so keeping this as it is.
    //        case invoiceImage
    
    case invoiceImageFirst
    case invoiceImageSecond
    case invoiceProductCode
    case membershipIdInvoice
    case showTutorial
    case User_Mobile_No
    case offerDetailsCount
    case offerFavoriteCount
    case canShowRating  // TODO: This key need to be removed.
    case sessionToken
    case Location
    case Value
    case comment
    case appInstalled
    case custIdRegistered
    case registeredAppVersion
    case currentAppVersion
    case homeLocationPermissionNotNow
    case jsDownloadTime
    case jsFileUrl
    case hideBuyback
    case buybackDismissCount
    case buybackDismissDate
    case previousBrightnessValue
    case osVersion
    case scratchCardShown
    case ratingSubmittedDate
    case custType
    case isShowTabBarTutorial
    case cardOpenCount
    case saveQuestionAnswerDataKey
    
    case isNonSubscriber
    case rewardState
    case rewardScratchDate
    case selectBankToolTipShow
    case appUpdateed
    case isShowWelcomeMessage
    case isShownBuyTab
    case pushPromptShowDate
    case isShowPushPromptOnTab
    case cookie
    case isVerifyEmailDismissed
    case isCatalystUserGroup
    case logoutSuccess

    case modelsDownloadedOn
    case modelsSetupRetryCount

    case currentDeeplink
    case currentDeeplinkUrl
    case currentDeeplinkParams

    func get() -> String? {
        return UserDefaults.standard.string(forKey: self.rawValue)
    }
    
    func set(value: String?) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func get() -> Bool {
        return UserDefaults.standard.bool(forKey: self.rawValue)
    }
    
    func set(value: Bool) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func get() -> Date? {
        return UserDefaults.standard.value(forKey: self.rawValue) as? Date
    }
    
    func set(value: Date?) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func get() -> Any? {
        return UserDefaults.standard.value(forKey: self.rawValue)
    }
    func set(value: Any?) {
        UserDefaults.standard.set(value, forKey: self.rawValue)
        UserDefaults.standard.synchronize()
    }
}

class AppUserDefaults {
    private init() {}
    
    class var scratchCardShown: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.scratchCardShown.rawValue)
        } set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.scratchCardShown.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var selectBankToolTipShow: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.selectBankToolTipShow.rawValue)
        } set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.selectBankToolTipShow.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var pushPromptShowDate: Date? {
        get {
            return UserDefaults.standard.value(forKey: UserDefaultsKeys.pushPromptShowDate.rawValue) as? Date
        } set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.pushPromptShowDate.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var isShowPushPromptOnTab: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.isShowPushPromptOnTab.rawValue)
        } set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.isShowPushPromptOnTab.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var sessionToken: String {
        get {
            let data = UserDefaults.standard.data(forKey: UserDefaultsKeys.sessionToken.rawValue)
            return String.decrypt(value: data) ?? ""
        } set {
            if let encryptedData: Data = String.encrypt(string: newValue){
                UserDefaults.standard.set(encryptedData, forKey: UserDefaultsKeys.sessionToken.rawValue)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    class var isVerifyEmailDismissed: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.isVerifyEmailDismissed.rawValue)
        } set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.isVerifyEmailDismissed.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var isCatalystUserGroup: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.isCatalystUserGroup.rawValue)
        } set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.isCatalystUserGroup.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var logoutSuccess: Bool? {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.logoutSuccess.rawValue)
            
        } set {
            if(newValue == true){
                UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.logoutSuccess.rawValue)
                
            }else {
                UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.logoutSuccess.rawValue)
            }
            
            UserDefaults.standard.synchronize()
        }
    }
}
