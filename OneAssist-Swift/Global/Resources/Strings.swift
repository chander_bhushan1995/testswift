//
//  Strings.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/19/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

enum Strings {
    
    enum Global {
        static let applyAction = "Apply".localized()
        static let accessDenied = "Access Denied".localized()
        static let cantAccessMsg = "Seems like an suspicious activity, You can't access this app.".localized()
        static let buyPlanHeader = "Easily protect your dependables against damage & theft".localized()
        static let okay = "OK".localized()
        static let save = "SAVE".localized()
        static let retry = "Retry".localized()
        static let infoMessage = "Info Message".localized()
        static let renewalNotAllowed = "Renewal is not allowed. Please contact our customer care for more details.".localized()
        static let claimNotAllowed = "You can't raise claim on this membership. Press ok to refresh membership.".localized()
        static let next = "Next".localized()
        static let mobile = "Mobile".localized()
        static let whc = "WHC".localized()
        static let ew = "EW".localized()
        static let homeAppliances = "Home Appliances".localized()
        static let wallet = "Wallet".localized()
        static let idFence = "Dark Web Monitoring for data compromises".localized()
        static let viewBenefits = "View benefits".localized()
        static let whyUpgrade = "Why upgrade to HomeAssist?".localized()
        static let planBenefits = "Plan Benefits".localized()
        static let walletAssist = "WalletAssist".localized()
        static let selfRepair = "What is  Self-Repair".localized()
        static let contactUs = "Contact Us".localized()
        static let findAPlan = "Find a Plan".localized()
        static let chatWithUs = "Chat with us".localized()
        static let `continue` = "CONTINUE".localized()
        static let membershipDetails = "Membership details".localized()
        static let membershipId = "Your Membership ID".localized()
        static let customerId = "Your relationship number is".localized()
        static let error = "Error".localized()
        static let success = "Success".localized()
        static let somethingWrong = "Something went wrong".localized()
        static let noBenefits = "No Benefits Found".localized()
        static let noPlansFound = "No Plans Found".localized()
        static let noSRFound = "No SR Found".localized()
        static let rupeeSymbol = "₹"
        static let no = "No".localized()
        static let yes = "Yes".localized()
        static let keyBenefits = "KEY BENEFITS".localized()
        static let shareText = "I loved this app - India's first protection app for home appliances, wallet and personal electronics. . Download OneAssist app here : ".localized()
        static let idFenceShareText = "Protect your digital identity using IDFence before it’s too late. Start for Free now   https://bit.ly/3mpw2os"
        static let appDownloadUrlString = "https://wz9p3.app.goo.gl/5DKh"
        static let appStoreLink = "itms://itunes.apple.com/in/app/one-assist-for-iphone/id1074619069?mt=8"
        
        static let cancel = "Cancel".localized()
        static let gotIt = "Okay, got it".localized()
        static let takePicture = "Take Picture from Camera".localized()
        static let selectPicture = "Select Picture from Gallery".localized()
        static let invoiceError = "Please upload the invoice copy of your device before proceeding further.".localized()
        static let tncError = "Please accept Terms and Conditions.".localized()
        static let noPlansError = "No plans found".localized()
        static let uploadImage = "Upload Image".localized()
        static let clickPhoto = "Click Photo".localized()
        static let pendingMembership = "We're setting up your membership. This may take a while".localized()
        static let permissionDenied = "Permission Denied".localized()
        static let GalleryPermissionDenied = "OneAssist does not have permission to access 'All Photos' in the gallery. Please change the settings to continue.".localized()
        static let CameraPermissionDenied = "OneAssist does not have permission to access camera. Please change the settings to continue.".localized()
        static let contactPermisionDenied = "Unable to access contacts".localized()
        static let contactPermisionDeniedMessage = " does not have access to contacts. Kindly enable it in privacy settings.".localized()
        static let noContact = "No Contact available".localized()
        
        static let homeTab = "Home Tab".localized()
        static let appliancesAre = "Appliances are"
        static let applianceIs = "Appliance is"
        static let covered = "covered"
        static let serviceTimeline = "Service Timeline"
        static let inspection = "INSPECTION"
        static let invalidPincode = "Invalid Pincode"
        
        static let homeRenewPlanScreen = "Home Renew Plan Screen"
        static let furtherHelp = "For further help, Call us at".localized()
        
        static let dlsPendency = "dls_pendency"
        static let dlsCancelled = "dls_cancelled"
        
        static let viewDetails = "View Details"
        static let raiseClaim = "Book a Service".localized()
        static let sellYourPhone = "Sell your Phone".localized()
        static let resumeClaim = "Resume Now".localized()
        static let activateNow = "Activate Now"
        static let tandcAgree = "Agree to Terms & Conditions"
        static let verifyNow = "Verify Now"
        static let rateUs = "Rate Us"
        
        static let claimInProgress = "Service in progress"
        static let documentUploadPending = "Document upload pending"
        static let documentReuploadPending = "Document reupload pending"
        
        static let upload = "Upload"
        
        static let onHold = "On Hold"
        static let reschedule = "Re-Schedule"
        static let rescheduleVisit = "Re-Schedule Visit"
        static let yourAppliances = "Your Appliances"
        static let walletPlans = "Select Membership"
        
        static let myDashboard = "My Dashboard"
        static let minDateString = "01/01/1960"
        
        static let noAssetsDetails = "You have not raised any repair services yet :)"
        static let membershipActivation = "Membership Activation"
        
        static let uploadImages = "Upload Images"
        
        static let welcomeAndInstructionMsg = "Hi %@! Follow below steps to activate membership by %@ to avoid cancellation."
        
        static let optional = "(optional)"
        static let apple = "Apple"
        
        static let openImageLink = "Open IMEI screen by clicking below"
        static let openImageLinkBackPanel = "Open Image upload link on another mobile and Re-upload back panel image of your %@."
        static let uploadDescText = "After opening the IMEI screen, click photos of the front and back panel from the other mobile device."
        static let uploadDescTextBackPanel = "Make sure you remove the back cover before clicking the picture."
        static let openIMEIScreen = "Open IMEI Screen"
        static let resumeRaiseClaimText = "You have 1 pending claim submission, do you want to continue?"
        
        static let walletProductCode = "WP01"
        
        static let imeiFirst = "IMEI_FIRST"
        static let imeiSecond = "IMEI_SECOND"
        
        static let distinctByName = "distinctByName"
        
        static let idFencePending = "Your upgrade request is in process. Please wait for a few minutes."
        static let idFenceProgress = "IN PROGRESS"
        static let authErrorMessage = "Please verify your phone number to see your membership details"
        static let dismiss = "Dismiss"
        static let selectBank = "Select bank"
        static let report = "Your Report"
        static let invoice = "Invoice"
        static let invoiceDownloaded = "Downloading Invoice"
        static let welcomeUser = "Welcome back"
        static let hiUser = "Hi User!"
        static let welcomeMessage = "We have got a new look. Explore our app to get the most out of your gadgets, finances & appliances 😊."
        static let sodCategory = "SOD"
        static let knowMore = " Know More "
        static let abbServiceTenureNotActive = "You can sell your phone after a few months. Check details for eligibility of your phone."
        static let requestInProgress = "Your request is in progress"
        static let discount = "Discount"
        static let subTotal = "Subtotal"
        static let total = "Total"
        static let downloadInvoice = "Download Invoice"
        static let routeName = "routeName"
        static let srProcessedSuccessfully = "Your service request has been successfully processed. Hope you have a good experience"
        static let userSalutation = "Hi %@! 👋 "
        static let activatePE_ADLD_Plan = "Please activate your accidental and liquid damage plan for your %@ mobile"
        static let unlockPlanBenefits = "It is a mandatory step to unlock plan benefits."
        static let unlockPlanWithCoverAmt = "It is a mandatory step to unlock plan benefits and your risk cover of Rs.%@"
        static let timeLeft = "Time Left: "
        static let directoryForTFLiteModals = "TFliteModels"
        static let downloadSuccessPushHeading = "Activation setup complete"
        static let downloadSuccessPushDescription = "Tap to start activation"
        static let pleaseWhileSetupDownloading = "Please wait while we are starting download..."
        static let objectDetectionLabels = "objectDetectionLabels"
        static let objectDetectionModel = "objectDetectionModel"
        static let imageClassificationLabels = "imageClassificationLabels"
        static let imageClassificationModel = "imageClassificationModel"
        static let noInternetConnection = "No internet connection"
        static let noInternetMessage = "Make sure your device is connected to Wi-Fi or Mobile data."
    }
    
    enum InitialReactRouteNames {
        static let  MembershipTabScreen = "MembershipTabScreen"
        static let BuyTab = "BuyTab"
    }
    
    enum SODProductCodes: String {
        case SafetyMeasures = "SafetyMeasures"
        case Appliances = "Appliances"
        case OneAssistPromises = "OneAssistPromises"
    }
    
    enum RiskCalculatorCardType {
        static let question = "question"
        static let entry = "entry"
    }
    
    enum IDFenceConstants {
        
        static let trialActive = "Your trial ends in %@ day(s). Please upgrade to continue monitoring your assets."
        static let trialInactive = "Your trial membership is expired. Please upgrade to continue monitoring your assets."
        static let trialCancelled = "Your trial membership is Cancelled. Please upgrade to continue monitoring your assets."
        static let trailCancelledBuyNow = "Your membership is Cancelled.  Buy now to continue monitoring your assets."
        static let trialExpired = "Your trial membership is expired. Please upgrade within %@ day(s) to continue monitoring your assets."
        static let trialMemExpired = "Your trial membership is expired. Please update your payment information from dashboard to continue monitoring your assets within %@ day(s)"
        
        static let premiumProgress = "Payment in Progress"
        static let premiumFailed = "Payment Failed. Please update your payment information from your dashboard."
        static let premiumExpired = "Your membership is expired. Please update your payment information from dashboard to continue monitoring your assets within %@ day(s)."
        static let premiumInactive = "Your membership is inactivated. Please reactivate to continue monitoring your assets."
        static let premiumCancelled = "Your Membership was canceled. Please reactivate to continue monitoring your assets."
    }
    
    enum SODConstants {
        static let done = "Done"
        static let sodServicesHeader = "I need a service expert for"
        static let bookNow = "Proceed to book"
        static let screenTitle = " Service & Repair"
        static let pcciText = "100% Safe and Secure | PCI-DSS Compliant"
        static let categoryName = "SOD"
        static let nonSOD = "NON_SOD"
        static let addTitle = "Add "
        static let habdHeaderText =  "Add %@ for Repair"
        static let pmsHeaderText = "Add %@ for Service"
        static let serviceIncludes = "SERVICING INCLUDES"
        static let repairIncludes = "REPAIR INCLUDES"
        static let repaireCostDecideV1 = "Note: Spare parts (if required) charged separately, to be decided after inspection."
        static let repaireCostDecideV2 = "Note: Labour Cost and spare parts (if required) charged separately, to be decided after inspection."
        static let availableToday = " available today"
        static let selectRepairType = "Select issue you are facing"
        static let labourCostHeading = "Labour Cost (To be Paid after inspection only)"
        static let labourCostDescription = "Note: Labour cost is charged if appliance needs repair and is to be paid after inspection only."
        static let labourCost = "Labour Cost"
        static let okay = "Okay"
        static let seeLabourCost = "See Labour Cost"
        static let repairDesc = "1 Repair - "
        static let repairText = " Repair"
        static let serviceText = " Service"
        static let reqCompleted = " request completed Rate our service"
        static let alreadyServiceAvail = "Looks like, your appliance is already covered in HomeAssist Plan"
        static let bookServiceFromMem = "You can book a service/repair for free"
        static let raiseServiceForFree = "Raise a Service Request for Free"
        static let continueBooking = "Continue with Booking"
        static let requestID = "Service Request ID "
        static let sodClaim = "SOD Claim"
        static let feedback = "feedback"
        static let serviceOnDemand = "Service On Demand"
        static func availableSlots(count:Int) -> String {return (count == 1 ? (count.description)+" slot" : (count.description)+" slots")+availableToday}
    }
    
    enum SelectedServicePosition: String {
        case top = "Top"
        case bottom = "Bottom"
    }
    
    enum ServiceRating {
        static let overAllExp = "How was your overall experience?"
        static let rateScale = "Rate us on a  scale of 1 to 5"
        static let ratingOnAppStore = "Sweet! Show us your love by giving 5 star rating on the play store."
        static let rateNow = "Rate Now"
        static let serviceCompleted = "Service Completed"
    }
    
    enum MirrorTestErrorScreen {
        static let setUpFailed = "Sorry, the setup failed. We can still help you to activate your membership."
        static let needAnotherPhone = "For this, you will need another phone to click pictures of your %@ phone protected with us."
        static let badNetwork = "Oops! We couldn't setup the process due to bad network connection."
        static let makeSureInternet = "Make sure you are in an area with good internet connectivity and your mobile has a good signal."
        static let retryButton = "Retry"
        static let showMeHow = "Show Me How?"
    }
    
    enum MirrorTestPreviewScreen {
        static let frontPhotoDescription = "Is the front photo of your phone clear/completely visible?"
        static let backPhotoDescription = "Is the back panel photo of your phone clear/completely visible?"
        static let backPanelConsent = "I agreed my fingers are not covering the back panel and image is clear"
        static let frontPanelFirstConsent = "I agreed image is clear and not cropped"
        static let frontPanelSecondConsent = "My fingers are not covering the screen"
        static let noRetakePhoto = "No, Retake Photo"
        static let yesContinue = "Yes, Continue "
    }
    
    enum MirrorFlowRating {
        static let lowFeedback = "Sorry to hear that! Please give us your feedback."
        static let addFeedback = "Add a feedback"
        static let discount = "Get %@%% OFF. Use code: %@ on payment."
        static let submitFeedback = "Thank you! Your feedback is valuable in helping us improve our services."
    }
    
    enum RefundText {
        static let hintText = "We are really sorry for the inconvenience. Your full amount will be refunded within 7 days."
        static let phoneDateOlder = "Purchase date is not eligible for plan"
        static let phoneDateOlderDetail = "The plan is valid for phones upto %@ days old purchased on or after %@. You can opt for refund if you’re not eligible."
        static let amountmatcherror = "Purchase amount does not match"
        static let amountmatchDetail = "The plan is valid for phones between ₹ %@ - ₹ %@. You can opt for refund if your phone value is higher or lesser."
        static let amountPhoneError = "Purchase amount or date does not"
        static let refundOpt = "You can opt for refund if you’re not eligible."
        
        static let refundHeaderText = "Refund request placed"
        static let refundHeaderDetailtext = "Your full amount will be refunded within 7 business days. In case of any issue, please contact your bank.\n"
    }
    
    enum RatingFeedback {
           static let whatsWrong = "Tell us what went wrong"
           static let submitFeedback = "Submit feedback"
           static let leaveComment = "Leave a comment (optional)"
           static let tellFeedback = "Tell us your feedback"
           static let pleaseWait = "Please wait ..."
       }
    
    enum FDetectionConstants {
        static let hi = "Hi".localized()
        static let activationHeaderText = "Follow below steps to activate your plan."
        static let coverAmmountText = "The sooner you activate, the better it is. It is a mandatory step to unlock plan benefits and your risk cover of Rs.%@"
        
        /**
         * Membership Task name and Product code for Fraud Detection
         * show cards with action
         */
        static let docUpload = "DOC_UPLOAD";
        static let mobileProductCode = "MP01";
        
        /**
         * Membership status
         * show cards with action
         */
        static let strPostDtlPending = "POSTDTLPENDING";
        static let strComplete = "POSTDTLCOMPLETE";
        static let strReupload = "REUPLOAD";
        static let strPending = "PENDING";
        static let strLetBegin = "Let’s Begin";
        static let strSendDocumentUploadLink = "Send document upload link";
        static let strContinue = "Continue";
        static let tryAgain = "Try Again"
        static let samplePhoto = "Sample photos"
        static let seeSamplePhoto = "See Sample Photos"
        static let submitPhotos = "Agree & submit photos"
        static let strOpenVerificationScreen = "Open verification screen";
        
        /**
         * show cards with no action
         */
        static let strCancelled = "CANCELLED";
        static let strRejected = "REJECTED";
        static let strApproved = "APPROVED";
        static let strQueued = "QUEUED";
        static let holdon = "Hold on"
        static let panelCancleMsg = "All the amazing photos you just clicked will be discarded. Submit Photos to proceed."
        static let goBackAndSubmit = "Submit My Photos"
        static let cancleRetake = "I want to retake photos"
        static let seeVideoGuide = "See Video Guide"

    }
    
    enum MembershipActivationCell {
        // There is no subtitle in case of completed
        enum StageOne {
            static let mtTitle = "Verify phone IMEI to link your membership"
            static let title = "Enter Basic Details"
            static let subTitle = "Pincode and Residential Address."
            
            static let completedTitle = "Basic details submitted"
            // No button on this.
        }
        
        enum StageTwo {
            static let mtTitle = "Enter address details"
            static let title = "Verify device IMEI"
            static let subTitle = "To link your membership with the device."
            
            static let completedTitle = "IMEI number verified"
            static let buttonText = "See how"
        }
        
        enum StageThree {
            static let mtTitle = "Take phone photos in front of the mirror. Front & back panel photos"
            static let title = "Send Image upload link"
            static let subTitle = "Share any other mobile number to get link via SMS."
            
            // This will never be shown in completed state.
            static let buttonText = "Continue"
        }
        
        enum StageFour {
            static let title = "Open image upload link on other mobile and upload required images"
            static let subTitle = "Follow instructions mentioned on the link and upload Front & Back Panel Images."
            
            // No button on this.
            // This will never be shown in completed or started state on screen.
        }
    }
    
    enum MembershipDetailsFraudDetection {
        static let memDetails = "Membership Details"
        static let firstStep = "STEP 1 of 2"
        static let heading = "Please give us some basic details"
        
        static let pincode = "Pincode".localized()
        static let address = "Residential Address".localized()
        
        static let proceedToNextStep = "Proceed to next step"
        
        static let submit = "Submit"
        static let `continue` = "Continue"
        
        static let uploadInstruction = "Please Re-upload an image of the device purchase invoice"
    }
    
    enum DocsUploadStatus {
        static let reupload = "RU"
        static let pending = "P"
        static let accepted = "AC"
    }
    
    enum EmptyTextFieldError {
        static let emptyPin = "Please enter your pincode".localized()
        static let emptySixDigitPin = "Please enter 6 digit pincode".localized()
        static let emptyDate = "Please enter date".localized()
        static let emptyDetail = "Please enter detail".localized()
        static let emptyPurchaseAmount = "Please enter purchase amount".localized()
        
        static let emptySerialNumber = "Please enter serial number".localized()
        static let emptyApplianceMake = "Please enter appliance ".localized()
        static let emptyApplianceModel = "Please enter model".localized()
        static let emptyWarranty = "Please enter warranty".localized()
        static let emptyImage = "Please upload image".localized()
        
        static let emptyAddress = "Please enter address".localized()
        static let emptyCity = "Please enter city".localized()
        static let emptyState = "Please enter state".localized()
        
        static let emptyName = "Please enter name".localized()
        static let emptyContact = "Please enter contact number".localized()
        static let emptyEmail = "Please enter email id".localized()
        static let emptyDOB = "Please enter date of birth".localized()
        static let emptyImei = "Please enter IMEI number".localized()
        static let emptyReEnterImei = "Please re-enter IMEI number".localized()
        
        static let emptyCardNumber = "Please enter card number".localized()
        static let emptyMonth = "Please enter month".localized()
        static let emptyYear = "Please enter year".localized()
        static let emptyCVV = "Please enter CVV".localized()
        static let emptyBank = "Please enter bank name".localized()
        static let emptyCoupon = "Please enter promocode".localized()
        
        static let emptyCardName = "Please enter your card name".localized()
        static let emptyIssuerName = "Please enter issuer name".localized()
        static let emptyPlaceOfIncident = "Please enter place of incident".localized()
        static let invalidDescription = "Please enter incident description".localized()
        
        static let inputFeedback = "Please enter Feedback".localized()
        static let feedbackMax = "Feedback should not more than 400 characters".localized()
        
    }
    enum LandingScene {
        
        static let mobileProtection = "Easily protect your Mobile".localized()
        static let walletProtection = "Secure your bank cards".localized()
        static let haProtection = "Simply protect your home appliances".localized()
        
        static let theftAndDamage = "Best protection plans for your mobile".localized()
        static let mhc = "Free Mobile Health Checker".localized()
        static let appLocker = "Best resale value for old mobile".localized()
        
        static let fraudProtection = "Best protection plans for your cards".localized()
        static let cardOffers = "Save money using card offers".localized()
        static let branchFinder = "Find nearest ATM & bank branch".localized()
        
        static let warrantyPlans = "Best protection plans for appliances".localized()
        static let cashlessRepair = "Unlimited and 100% cashless repair".localized()
        static let serviceGuarantee = "Quick service guarantee".localized()
        
        static let curious = "Curious?".localized()
        static let next = "Next".localized()
        static let login = "Login".localized()
        
        static let comingSoon = "Coming Soon".localized()
    }
    
    enum ToastMessage {
        static let feedbak = "Thank you for your feedback!".localized()
        static let reminder = "Reminder has been set succesfully".localized()
        static let copied = "Copied to clipboard".localized()
        static let invalidInputDate = "Incident date should be before raised claim date"
        static let logoutSuccess = "Logout Successfull. Please login with registered mobile number."
    }
    
    enum LoginMobileScene {
        static let title = "Login".localized()
        static let skip = "SKIP".localized()
        static let heading = "Hi, Let's get started!".localized()
        static let mobileHeader = "Please verify your mobile number to Login/Signup".localized()
        static let mobileFooter = "We need your mobile number to get things done. We will never share your number with anyone.".localized()
        static let textFieldMobileDescription = "Mobile Number".localized()
        static let textFieldMobilePrefix = "+91".localized()
        static let textFieldMobilePlaceholder = "Enter your mobile number".localized()
        static let mobileAction = "Get OTP".localized()
        
        static let or = "or".localized()
        
        static let voucherHeader = "Do you have an activation voucher?".localized()
        static let voucherAction = "Activate Voucher".localized()
        static let activationCode = "Have a membership activation code?".localized()
        static let activationCodeDescription = "Activate membership in 3 easy steps to stay protected and avail the benefits."
        static let activationCodeHeaderTitle = "ACTIVATE MEMBERSHIP".localized()
        static let activationCodeActionTitle = "Activate Membership".localized()
        static let findAnotherPlan = "OR, FIND ANOTHER PLAN".localized()
        static let whatToDo = "What would you like to do today?".localized()
        static let loginWith = "You're logged in with ".localized()
        
        static let skipAlertTitle = "Are you sure you want to skip?".localized()
        static let skipAlertMessage = "You can login/create account by verifying your mobile number from Account section.".localized()
        static let skipButtonText = "Skip to home".localized()
        
        //
    }
    
    enum iOSFraudDetection {
        static let linkSentSuccess = "Image upload link successfully sent "
        
        static let resendSame = "Resend Link to same number"
        static let resendNew = "Resend Link to new number"
        static let chatForAssistance = "Chat for assistance"
        static let smsSent = "SMS sent successfully"
        static let seeVideoGiude = "See video guide"
        static let taptoClick = "Tap to click"
        static let changelanguage = "Change language"
        static let disablevoice = "Disable voice support"
        static let enablevoice = "Enable voice support"
        
        static let verifyImeiTitle = "IMEI Verification".localized()
        static let sendInspectionLink = "Send Inspection Link".localized()
        static let unPlugCharger = "Please unplug your charger and try again.".localized()
        static let screenShotCaptureTitle = "IMEI not found".localized()
        static let screenShotCaptureSubTitle = "Ensure that you took a Screenshot of IMEI screen while the number was visible. Please try again.".localized()
        static let anotherNumber = "Enter Another Mobile Number".localized()
        static let stopRecording = "Stop Recording and try again".localized()
        static let awsomeText = "Awesome! We have successfully verified your ".localized()
        static let IMEInumberText = " IMEI Number".localized()
        static let IMEInumberTextDesc = "Required to link membership to your device.".localized()
        static let smsHintText = "You will receive SMS with a link to Record IMEI screen and back panel of your ".localized()
        static let smsSentTitle = "SMS sent successfully".localized()
        static let smsSentMessage = "An SMS with document upload link has been successfully sent to ".localized()
        static let uploadErrorMessage = "Please upload the valid format of the document within the permissible size of 5 MB".localized()
        static let imageTextReadError = "Text not recognized".localized()
        static let selectLanguage = "Select Language".localized()
        static let selectRelationship = "Select Relationship".localized()
        static let decleration = "Please select declaration to proceed.".localized()
        static let purchaseAmountText = "Purchase amount (as per device)".localized()
        static let purchaseDateText = "Purchase date (as on invoice)".localized()
        
        enum IMEISteps {
            enum Step1 {
                static let firstText = "Click ".localized()
                static let boldText = "Verify IMEI button ".localized()
                static let lastText = "below to start.\n".localized()
            }
            
            enum Step2 {
                static let firstText = "Then, click ".localized()
                static let boldText = "Call button ".localized()
                static let lastText = "on the popup to get IMEI.\n".localized()
            }
            
            enum Step3 {
                static let firstText = "Make sure you ".localized()
                static let boldText = "take Screenshot of IMEI screen.\n".localized()
            }
            
            enum Step4 {
                static let firstText = "Click ".localized()
                static let boldText = "Dismiss button ".localized()
                static let lastText = "after taking the screenshot.".localized()
            }
            
            enum Step4_13 {
                static let firstText = "Click ".localized()
                static let boldText = "Cancel button ".localized()
                static let lastText = "after taking the screenshot.".localized()
            }
        }
    }
    
    enum VerifyOTPScene {
        static let heading = "Enter 6-digit OTP you received on".localized()
        static let textFieldOtpDescription = "Enter OTP".localized()
        static let textFieldOtpPlaceholder = "Enter verification code".localized()
        static let otpAction = "Submit".localized()
        static let resendAction = "Resend OTP".localized()
        static let resendSuccess = "OTP sent successfully".localized()
        static let resendOtpCounter = "RE-SEND OTP in".localized()
    }
    
    enum PlanDetailCell {
        static let memID = "MEMBERSHIP ID".localized()
        static let validTill = "Valid till".localized()
        static let activateBy = "Activate by".localized()
        static let membershipId = "Membership ID".localized()
        static let boughtOn = "Bought on".localized()
        static let validFor = "Valid from %@ to %@".localized()
        static let abbInfo = "Applicable only if the phone health satisfies the conditions".localized()
    }
    
    enum OfferFilterScene {
        static let reset = "Reset".localized()
        static let apply = "Apply Filter".localized()
        static let header = "What do you want to do today?".localized()
    }
    
    enum MobilePlanRegistrationScene {
        static let title = "Register".localized()
        static let heading = "Please provide additional information for registration".localized()
        static let submitAction = "Submit".localized()
        static let terms = "I agree to the Terms and Conditions".localized()
        static let termsAction = "Terms and Conditions".localized()
        static let imei = "IMEI".localized()
        static let reenterImei = "Re-enter IMEI".localized()
        static let emailId = "Email ID".localized()
        static let dob = "Date of birth".localized()
        static let name = "Full name".localized()
        static let contact = "Mobile number".localized()
        static let uploadInvoice = "Please upload an image of the device purchase invoice".localized()
        static let uploadInvoiceWithIMEI = "Please upload an image of the device purchase invoice with IMEI ".localized()
        static let uploadInvoiceWithoutIMEI = "Please upload an image of the device purchase invoice".localized()
        static let uploadDocumentWithIMEI = "Please upload additional document in which IMEI is mentioned".localized()
        static let uploadDocumentWithIMEIOptional = "Please upload additional document in which IMEI is mentioned %@".localized()
        static let instructionForIMEI = "Usually device purchased from e-com sites like Amazon, Flipkart etc has IMEI on bill attached to the device.".localized()
        static let blurryImage = "Make sure image is not blurry".localized()
        static let withIMEI = "Make sure invoice has IMEI number".localized()
        static let knowMore = "Know More".localized()
    }
    
    enum SuggestAMobilePlanScene {
        static let title = "Mobile & Tablet Details".localized()
        static let headerForPurchase = "Please enter your mobile details".localized()
        static let headerForGift = "Please enter your friend's handset details".localized()
        static let purchaseAmount = "Purchase Amount".localized()
        static let enterInvoiceAmount = "Enter Invoice Amount".localized()
        static let handsetModel = "Device Brand".localized()
        static let enterHandsetModel = "Enter Device Brand".localized()
        static let purchaseDate = "Purchase Date".localized()
        static let enterPurchaseDate = "Select a Purchase Date".localized()
        
        enum Error {
            static let minimumInvoiceValue = "Minimum amount should be Rs. 5000".localized()
            static let maximumInvoiceValue = "Maximum amount exceeded, please contact the customer care.".localized()
            static let emptyInvoiceValue = "Please enter invoice amount.".localized()
            static let emptyDate = "Please select purchase date.".localized()
            static let emptyHandsetModel = "Please enter brand name.".localized()
            static let invalidDate = "We do not provide protection on phone older than %d days".localized()
        }
    }
    
    enum MobileTabScene {
        static let mobileHeading = "MOBILE ASSIST".localized()
        static let mobileTitle = "Mobile & Tablet Protection".localized()
        static let mobileSubtitle = "Protect your mobile phone against damage instantly".localized()
        static let mobileServices = "Covers Damage | Extended Warranty".localized()
        
        static let protectMobileHeading = "Protect your Mobile".localized()
        static let protectMobileSubHeading = "with best mobile plans".localized()
        
        static let protectYourMobile = "Protect your Mobile".localized()
        static let planExpiredError = "Your membership is expiring on %@. Renew it now to stay protected.".localized()
        static let upgradeError = "You currently have limited protection on your mobile. Upgrade now to enjoy all benefits".localized()
        static let myMobileDetails = "My Device Details".localized()
        static let mhc = "Mobile Health Checker".localized()
        static let mhcDescription = "Check your mobile health on 12 key components".localized()
        //static let mhcDescription = "Check your mobile health on %@ key components".localized()
        static let mhcResultDescription = "Your last mobile health score was ".localized()
        static let giftPlan = "Gift Protection Plan".localized()
        static let giftPlanDescription = "Protect your loved one's phone and stay connected always".localized()
        static let giftNow = "Gift now".localized()
        static let freeTrial = "FREE TRIAL".localized()
    }
    
    enum RenewMembershipScene {
        static let title = "Renew Plan".localized()
        static let moreOrLess = "Have more or less no. of appliances?".localized()
        static let chooseAnotherPlan = "Choose another plan".localized()
        
        static let subtitle = "Not eligible for renewal.".localized()
        
        static let appliancesCount = "%@ of your Appliances %@ not eligible for renewal."
        static let reason = "Don’t Worry you can replace %@ at the time of inspection."
        
        static let savedText = "You have saved Rs %@ this year"
        static let memExp = "Your membership is expiring on %@, Renew it now and save high cost of repairs"
    }
    
    enum PEScene {
        enum FraudDetection {
            static let uploadDocuments = "Please upload the mandatory documents before %@ to avoid cancellation"
            static let verifyIMEIAgain = "It has been 2 days since you verified IMEI. For security reasons, we request you to verify your IMEI again"
            static let uploadFrontAndBackPanel = "Click the below button to open IMEI screen.\n\nUpload front & back panel images of %@ mobile on the link sent to %@ via SMS"
            static let reuploadDocuments = "Please re-upload the documents before %@ to avoid cancellation"
            static let pending = "We are verifying your activation details. This process may take upto 2 working day(s)"
            static let memCancelledDesc = "Your membership was cancelled. Refund will be processed within 7 working day(s). For any assistance, you can chat with us."
            static let memRejectedDesc = "Your membership was rejected. Refund will be processed within 7 working day(s). For any assistance, you can chat with us."
            static let memApprovedDesc = "ACTIVATED SUCCESSFULLY"
            static let memQueuedDesc = "AWAITING CONFIRMATION"
            
            static let androidPlan = "This plan is for your %@ Mobile (Android). Please login from any android mobile to start activation."
            static let activationPending = "Complete activation process before %@ to avoid membership cancellation."
            static let frontBackImageAvailable = "Please check and submit the front panel and back panel photos to avoid membership cancellation"
            
            enum Status {
                static let uploadPending = "DOCUMENT UPLOAD PENDING"
                static let reuploadPending = "DOCUMENT RE-UPLOAD PENDING"
                static let verificationPending = "DOCUMENT VERIFICATION PENDING"
                static let memCancelled = "MEMBERSHIP CANCELLED"
                static let memRejected = "MEMBERSHIP REJECTED"
                static let memApproved = "ACTIVATED SUCCESSFULLY"
                static let memQueued = "COMPLETED"
                static let activationPending = "ACTIVATION PENDING"
                /**
                 * Membership status
                 * show cards with action
                 */
                static let strPostDtlPending = "POSTDTLPENDING"
                static let strComplete = "POSTDTLCOMPLETE"
                static let strReupload = "REUPLOAD"
                static let strPending = "PENDING"
                
                /**
                 * show cards with no action
                 */
                static let strCancelled = "CANCELLED"
                static let strRejected = "REJECTED"
                static let strApproved = "APPROVED"
                static let strQueued = "QUEUED"
                
            }
            
            enum PrimaryAction {
                static let getStarted = "Get Started"
                static let startVerification = "Start IMEI Verification"
                static let uploadDocuments = "Upload Documents"
                static let openIMEIScreen = "Open IMEI Screen"
                
                static let resendLink = "Resend image upload link"
                static let activateNow = "Activate Now"
                static let continues = "Continue"
            }
        }
    }
    
    enum HATabScene {
        
        static let protectHAHeading = "Protect your Home Appliances".localized()
        static let protectHASubHeading = "with best extended warranty plans".localized()
        static let protectHAPrimaryAction = "Find a Plan".localized()
        static let protectHASecAction = "View benefits".localized()
        
        static let myCards = "My Cards".localized()
        static let proceedToPay = "Proceed to pay".localized()
        static let chooseAnotherPlan = "Choose another plan".localized()
        // static let planExpiredError = "Your home appliances protection plan is expiring soon. Renew now to stay protected.".localized()
        static let memExpiry = "Your membership is expiring on %@. Renew it now to stay protected.".localized()
        static let graceMessage = "Your membership was expired on %@. You can renew your membership within %@ day(s) of expiry date.".localized()
        static let buyNewPlan = "BUY A NEW PLAN".localized()
        
        static let upgradeToHomeAssist = "UPGRADE TO HOMEASSIST".localized()
        static let upgradeError = "You currently have limited protection on your home appliances. Upgrade now to enjoy all benefits".localized()
        static let viewInspectionDetails = "View Inspection Details".localized()
        
        enum Whc {
            static let whcHeading = "HOME ASSIST".localized()
            static let whcTitle = "HomeAssist - One plan for your old & new appliances alike!".localized()
            static let whcSubtitle = "A smart way to protect up to 10-year-old appliances in a bundle plan.".localized()
            static let whcServices = "Covers Breakdown | Damage | Fire | Burglary".localized()
            
            static let topHeading = "Favouring new appliances over old ones?".localized()
            static let topSubheading = "Not anymore!".localized()
            static let bottomHeading = "Protect old & new appliances alike with".localized()
            static let bottomSubheading = "HomeAssist".localized()
            static let primaryAction = "I'm Interested".localized()
            static let callInfo = "For further help , please call us at".localized()
            static let contact = "%@".localized()
            static let amountSavedText = "You have saved Rs %@ last year on repairs. Renew now & stay protected 👍".localized()
            
            static let onePlan = "One plan for all appliances".localized()
            static let guaranteedResponse = "Guaranteed response within 6 hours".localized()
            static let cashlessRepairs = "Cashless Repairs".localized()
            
            static let viewApplianceDetails = "My Appliance Details".localized()
            static let gadgetServ = "GadgetServ Plan".localized()
            static let whcPlanName = "HomeAssist Plan".localized()
            
            enum Status {
                static let pending = "INSPECTION PENDING".localized()
                static let pendingWithDelay = "INSPECTION PENDING".localized()
                static let awaiting = "AWAITING CONFIRMATION".localized()
                static let scheduled = "INSPECTION SCHEDULED".localized()
                static let failed = "INSPECTION FAILED".localized()
                static let inspectionCancelled = "INSPECTION CANCELLED".localized()
                static let membershipCancelled = "MEMBERSHIP CANCELLED".localized()
                static let renewMembershipCancelled = "RENEWAL REQUEST CANCELLED".localized()
                static let details = ""
                static let detailsActivated = "ACTIVATED SUCCESSFULLY".localized()
                static let inspectionCancelledNonReschedule = "INSPECTION CANCELLED"
                static let pendingEarliest = "INSPECTION PENDING"
            }
            
            enum Info {
                static let pending = "Please schedule an inspection by %@ to activate membership".localized()
                static let renewPending = "Please schedule an inspection before %@ to renew membership".localized()
                static let pendingWithDelay = "Only %@ days left to schedule an inspection or your membership will be cancelled".localized()
                static let renewPendingWithDelay = "Only %@ days left to schedule an inspection or your renewal request will be cancelled".localized()
                static let pendingWillBeCancelled = "Please schedule an inspection immediately or your membership will be cancelled".localized()
                static let renewPendingWillBeCancelled = "Please schedule an inspection immediately or your renewal request will be cancelled".localized()
                static let awaiting = "Your inspection request is under process".localized()
                static let failed = "Inspection failed as no appliances were in working condition. Refund will be processed within 7 working days. For any assistance, chat with us.".localized()
                static let inspectionCancelled = "As your previous inspection was cancelled please reschedule before %@".localized()
                static let membershipCancelled = "As inspection wasn't scheduled, your membership is cancelled. Refund will be processed within 7 working days. For any assistance, chat with us.".localized()
                static let renewMembershipCancelled = "As inspection wasn't scheduled, your renewal request is cancelled. Refund will be processed within 7 working days. For any assistance, chat with us.".localized()
                static let details = ""
                static let inspectionCancelledNonReschedule = "Your previous inspection was cancelled as you were unavailable".localized()
                static let pendingEarliest = "You have not scheduled the inspection yet.Get in touch at the earliest.".localized()
            }
            
            enum PrimaryAction {
                static let pending = "Schedule Inspection".localized()
                static let inspectionCancelled = "Reschedule".localized()
            }
        }
        
        enum EW {
            static let ewHeading = "EXTENDED WARRANTY".localized()
            static let ewTitle = "Bought a new appliance? Buy an extended warranty now".localized()
            static let ewServices = "Covers Electrical & Mechanical Breakdown".localized()
            
            static let heading = "Bought a new appliance within past 2 months?".localized()
            static let subheading = "get covered beyond manufacturer’s warranty".localized()
            static let primaryAction = "Find extended warranty plans".localized()
            static let membershipExp = "Your membership is expiring soon. Upgrade to HomeAssist Plan and protect it from fire, burglary and damages".localized()
        }
    }
    
    enum BookmarkOffer {
        static let noBookmarkAvailable = "No bookmarked offers available.".localized()
        static let noBookmark = "No bookmarked offers available. See all offers".localized()
        static let noCard = "No bookmarked offers available. Add a debit/credit card to see all offers".localized()
        static let noMembership = "A valid Wallet membership is required to use this feature.".localized()
    }
    
    enum WalletTabScene {
        static let walletHeading = "WALLET ASSIST".localized()
        static let walletTitle = "Wallet and Bank Card Protection".localized()
        static let walletSubtitle = "Secure your Credit & Debit cards against fraud & misuse".localized()
        static let walletServices = "Covers Theft, Fraud & Misuse of Credit & Debit Cards".localized()
        
        static let findATM = "Find nearby ATMs".localized()
        static let findBank = "Find your Bank Branch".localized()
        static let offersOnCardHeading = "Offers on your cards".localized()
        static let offersOnCardSubHeading = "Get amazing deals & offers when you add your debit/credit cards".localized()
        static let offersOnCardAction = "Add a card".localized()
        static let bookmarkedOffers = "View bookmarked offers".localized()
        static let moreCardsHeading = "More Cards, More Savings!".localized()
        static let moreCardsSubHeading = "Add more cards and save on your daily spends".localized()
        static let moreCardsAction = "Add a card".localized()
        static let protectWalletHeading = "Protect your Wallet".localized()
        static let protectWalletSubHeading = "with best fraud protection plans".localized()
        static let protectWalletPrimaryAction = "Find a Plan".localized()
        static let protectWalletSecAction = "View benefits".localized()
        static let viewAllOffers = "View all offers".localized()
        
        static let myCards = "My Cards".localized()
        static let planExpiredError = "Your membership is expiring on %@. Renew it now to stay protected.".localized()
        
        static let upgradeError = "You currently have limited protection on your wallet. Upgrade now to enjoy all benefits".localized()
        
        static let offersOnParticularCard = "OFFERS ON YOUR %@ CARD".localized()
        
        
        static let offersOnAnotherCardHeading = "Add another card to get offers".localized()
        static let offersOnAnotherCardSubHeading = "We have 800+ offers for you. Go on and add a card now!".localized()
        static let offersOnAnotherCardAction = "Add another card".localized()
        static let errorMessageOnAnotherCard = "Sorry, there are no offers on your %@ now, we will notify you once there are offers on your card.".localized()
    }
    
    enum PlanBenefits {
        enum Mobile {
            static let benefit1 = "Protection against accidental & liquid damage".localized()
            static let benefit2 = "Cashless claim processing".localized()
            static let benefit3 = "Doorstep pickup & drop service".localized()
            static let benefit4 = "24x7 Customer support".localized()
        }
        
        enum Wallet {
            static let benefit1 = "One call to register and block all your cards".localized()
            static let benefit2 = "Fraud protection on all your bank cards".localized()
            static let benefit3 = "Emergency travel assistance in case you loose your wallet".localized()
            static let benefit4 = "Free ID replacement services in case you loose your wallet – for both Driving Licence and PAN card".localized()
        }
        
        enum HomeAppliances {
            static let benefit1 = "Extended warranty".localized()
            static let benefit2 = "Unlimited 100% cashless repairs".localized()
            static let benefit3 = "Guaranteed resolution within 10 working days".localized()
            static let benefit4 = "Verified service experts".localized()
            static let benefit5 = "490+ cities covered".localized()
        }
    }
    
    enum SelectPlanScene {
        static let title = "Pick a plan".localized()
        static let heading = "RECOMMENDED PLAN".localized()
        static let buyAction = "Buy Plan".localized()
        static let allBenefits = "View all benefits".localized()
        static let perAnnum = "/year only".localized()
        static let description = "(that's an affordable ₹%@/month)".localized()
        
        static let coverAmtTitle = "Cover amount of".localized()
        static let coverAmtDesc = "Higher cover amount provides better risk coverage and benefits for your wallet".localized()
        static let appliance = "Appliance".localized()

        static let appliances = "Appliances".localized()
        static let showingPlans = "Showing Plans for".localized()
    }
    
    enum AddressRegistrationScene {
        static let title = "Address".localized()
        static let presentedTitle = "Add new address".localized()
        static let pincode = "Pin Code".localized()
        static let city = "City".localized()
        static let state = "State".localized()
        static let nextAction = "Next".localized()
        static let address = "Address".localized()
        static let address2 = "Secondary Address".localized()
        static let others = "Other".localized()
    }
    
    enum PaymentWebViewScene {
        static let title = "Payment".localized()
        
        static let paymentFailed = "Payment Unsuccessful".localized()
        
        static let warning = "WARNING".localized()
        static let warningMessage = "UNTRUSTED CERTIFICATE, Want to continue?".localized()
        static let warningCancel = "Do you want to cancel payment?".localized()
        static let stop = "Stop".localized()
        static let `continue` = "Continue".localized()
    }
    
    enum BankMapScene {
        static let increaseRadiusMessage = "Sorry, no branch found nearby you. We are increasing the search area to show nearest branch.".localized()
        enum Error {
            static let locationDisabled = "To enable, Go to settings > Tap on Location > Choose While Using the App".localized()
        }
    }
    
    enum OfflineOffersScene {
        static let km2 = "2 km".localized()
        static let km5 = "5 km".localized()
        static let km10 = "10 km".localized()
        static let km10plus = "10+ km".localized()
    }
    
    enum AllOffersScene {
        static let showing = "Currently showing".localized()
        static let allOffers = "All Offers".localized()
        
        static let endToday = "Ending today".localized()
        static let endTomorrow = "Ending tomorrow".localized()
        static let endingIn = "Ending in".localized()
        static let endsOn = "Ends on".localized()
    }
    
    enum BookmarkedOffersScene {
        static let title = "Bookmarked Offers".localized()
    }
    
    enum OfferDetail {
        static let title = "Offer Details".localized()
        static let termsconds = "Terms and Conditions".localized()
        static let shareText = "Save money every time you open OneAssist App. See most exciting discounts and offers on your credit and debit cards on the go and save BIG. Download OneAssist app here".localized()
        static let offerAvailableOnMoreCards = "Offer available on the following bank cards".localized()
        
        
        enum OnlineOfferDetailScene {
            static let availOffer = "You can avail this offer at".localized()
        }
        
        enum OfflineOfferDetailScene {
            static let address = "Address".localized()
            static let primaryAction = "GET DIRECTIONS".localized()
        }
    }
    
    enum MoreTabScene {
        static let logout = "Logout".localized()
        static let privacyPolicy = "Privacy Policy".localized()
    }
    
    enum VerifyVoucherScene {
        static let title = "Verify Voucher".localized()
    }
    
    enum VerifyVoucherSuccessScene {
        static let continueLoginWith = "Continue to log in with +91 ".localized()
        static let voucherSuccess = "You have validated all the details successfully for your".localized()
        static let device = "device".localized()
        static let uploadDocument = "Please upload required documents before".localized()
        static let avoidCancellation = "to activate your membership and avoid cancellation.".localized()
        static let `continue` = "Continue".localized()
    }
    
    enum ChooseAppliance {
        static let title = "Select any one appliance".localized()
        static let mainCategories = "MOST BOUGHT".localized()
        static let otherCategories = "OTHER APPLIANCES".localized()
    }
    
    enum ApplianceMakeScene {
        static let title = "Device Make".localized()
    }
    enum HAPinScene
    {
        static let enterPinCode = "We need your pincode to check if it is in our serviceable area".localized()
        static let residentialText = "Please enter your residential pincode".localized()
        static let errorText = "Sorry, we are currently not servicing in your residential area".localized()
        static let notify = "We can notify you when once we are available in your area".localized()
        static let pincode = "Pincode".localized()
        static let picodePlaceHolder = "Enter 6 digit pincode".localized()
    }
    enum HASelectAppliance
    {
        static let howManyAppliance = "How many appliances would you like to protect".localized()
        static let headerTitle = "We cover 15 essential Home Appliances!".localized()
        static let uptoText = "Upto how many Appliances?".localized()
//        static let numberOfAppliances = "NUMBER OF APPLIANCES".localized()
        enum Error {
            static let emptyNumberOfAppliance = "Please select the number of appliances".localized()
            
        }
    }
    
    enum WHC
    {
        enum eventTracking
        {
            static let callOATapped = "call OA tapped".localized()
            static let chatFromInspectionTapped = "chat from inspection assesment tapped".localized()
            static let addTocalendarTapped = "add to calendar tapped".localized()
            static let ratingSubmitted = "Rating Submitted".localized()
            static let addNewAddress = "add a new address tapped".localized()
            static let submitRequestInspection = "submit inspection request tapped".localized()
            static let showInspectionDetails = "show inspection details tapped".localized()
            static let callServiceTapped = "call service center tapped".localized()
            static let sendFeedBackTapped = "Send feedback tapped".localized()
            static let whcPlanBenefitTapped = "whc plan benefit tapped".localized()
            static let myAppliancesDetailsTapped = "my appliance details tapped".localized()
            static let rescheduleInspectionTapped = "reschedule inspection tapped".localized()
            static let iAmInterestedTapped = "Find a Plan".localized()
            static let verifyPincodeTapped = "verify pincode tapped".localized()
            static let notifyMeTapped = "notifyme from whc tapped".localized()
            static let seeBestPlanTapped = "see best plans tapped".localized()
            static let getProtectedTapped = "get protected tapped".localized()
            static let coverAmountChange = "cover amount changed".localized()
            static let whcSuccessfullyPurchased = "Plan Purchased".localized()
            static let scheduleInspectionTapped = "Schedule inspection tapped".localized()
            
            
            enum addCard{
                static let addCardTapped = "Add card from addcard screen tapped".localized()
                static let goToWalletTapped = "Go to mywallet from addcard tapped".localized()
            }
            enum myCards{
                static let myCards = "mycards".localized()
                static let addCard = "Add card tapped(plus icon)".localized()
                static let callBankTapped = "Call your bank tapped".localized()
                static let removeCardTapped = "Remove card tapped".localized()
                static let findBankBranchTapped = "Find bank branch".localized()
            }
            
            enum eventTrackingDesc{
                static let desc = "desc".localized()
                static let location = "location".localized()
                static let verifyPincodeTapped = "please enter your residential pincode".localized()
                static let scheduleInspectionTapped = "From WHC FLOW".localized()
                static let submitRequestInspection = "on submit request (inspection)".localized()
                static let whcPlanBenefitTapped = "membership card view benefits button tap".localized()
                
                enum addCard{
                    static let addCardTappedResult = "result".localized()
                    static let addCardTappedSuccess = "success".localized()
                    static let addCardTappedFailure = "failure".localized()
                    static let cardType = "card_type".localized()
                }
                
            }
            
        }
        
        
        
        enum titles{
            static let selectAppliances = "Select no. of appliances".localized()
            static let PinCode = "Pincode".localized()
            static let inspectionDetailsTitles = "Inspection Details".localized()
        }
        enum Pincode
        {
            static let pincodeError = "Please enter your pincode".localized()
            static let notifyTitle = "Notify Me".localized()
            static let continueTitle = "Continue".localized()
            static let title = "Please verify your Residential Pincode".localized()
            static let subTitle = "We need your pincode to check if it is in our Serviceable area".localized()
            static let continueText  = "Continue".localized()
            static let indicator = "Checking pincode serviceability".localized()
            static let errorTitle  = "Sorry, we are currently not servicing in your residential area.".localized()
            static let errorSubtitle = "We apologize for the inconvenience caused. Please feel free to contact us for any queries.".localized()
            static let pincode = "Pincode".localized()
            static let pincodePlaceholder = "Enter 6 digit pincode".localized()
            static let serviceCountString = "appliances repaired in your pincode in last three months".localized()
            
            enum Error{
                static let pincode = "Please enter a valid pincode".localized()
            }
            
        }
        enum suggestPlans{
            static let seeBestPlans = "See Best Plans".localized()
        }
        enum inspection{
            enum inspectionDetailHeaderView{
                static let showDetails = "Show details";
                static let hideDetails = "Hide details";
//                static let notStartedTimeLineImageName = "timelineInactive";
//                static let startedTimeLineImageName = "timelineInProgress";
//                static let completedTimeLineImageName = "timelineDone"
//                static let onHoldTimelineImageName = "pendingInProgress"
            }
            enum inspectionDetails
            {
                static var  ForegroundRefreshRate = 15
                static let TechnicianDetailCellIdentifier = "TechnicianDetailCell".localized()
                static let InspectionDetailsCellIdentifier = "InspectionDetailsCell".localized()
                static let InspectionAssesmentCellIdentifier = "InspectionAssesmentCell".localized()
                static let InspectionDetailHeaderViewIdentifier = "InspectionDetailHeaderView".localized()
                static let  RatingGreenImage = "starGreen".localized()
                static let IsTechnicianReached = "Has the technician reached your place?".localized()
                static let NotYet =  "NOT YET".localized()
                static let Yes =  "YES".localized()
                static let RateOnAppStore =  "Thank you for your feedback. Please take some time out and rate us on app store".localized()
                static let IWillDoLater =  "I'LL DO IT LATER".localized()
                static let YeahSure =  "YEAH, SURE!".localized()
                static let  FirstHeader =  "Inspection scheduled".localized()
                static let  SecondHeader =  "Technician Visit".localized()
                static let  ThirdHeader =  "Inspection assesment".localized()
                static let  FourthHeader =  "Inspection completed".localized()
                static let TechnicianReachedCheckInterval = 15;
                static let InspectionCompleteCheckInterval = 1;
                static let shareOTPFirstPart = "Please share OTP".localized()
                static let shareOTPSecondPart = "at the end of inspection with the technician.".localized()
                static let shareOTPSecondPartService = "at the end of service with the technician.".localized()
                static let inspectionSuccess  = "Inspection successful".localized()
                static let inspectionScheduled = "Inspection scheduled for OneAssist".localized()
                static let timeLineHeaderView = "TimeLineHeaderView"
                static let serviceDetails = "SERVICE DETAILS".localized()
            }
            
        }
        enum selectAddress{
            static let title = "Select Address".localized()
            static let heading = "Is this where you would like the inspection to be done?".localized()
            static let newAddressButtonTitle = "No,I have shifted to a new location".localized()
        }
    }
    
    enum WhcSuccessScene {
        static let heading = "Payment Successful".localized()
        static let midHeading = "We hope that your appliances are in the best of condition!".localized()
        static let bottomHeading = "You’re just a step away from activating your HomeAssist plan".localized()
        static let transparentHeading = "Schedule an Inspection".localized()
        static let transparentSubHeading = "To enjoy the benefits of HomeAssist plan, please schedule an inspection within 10 days of purchase".localized()
        static let primaryAction = "Schedule Inspection".localized()
    }
    
    enum ScheduleVisitScene {
        static let title = "Schedule Visit".localized()
        static let heading = "When would you like the technician to visit ?".localized()
        static let collectionHeading = "Choose a time convenient for you and rest is on us!".localized()
        static let collectionSlotHeading = "Select a Time Slot".localized()
        static let today = "TODAY".localized()
        static let visitError = "Time slots are not available for the selected date. Please choose another date".localized()
        static let selectSlotError = "Please select a slot".localized()
        static let primaryAction = "Submit Request".localized()
        static let scheduleAction = "Schedule Visit".localized()
        static let tellusMore = "Tell us more".localized()
    }
    
    enum NotifyInputFormScene {
        static let descriptionText = "Coming soon to your location. Get notified when our services open in your city.".localized()
        static let successDescriptionText = "Thanks, you will be notified when services open in your city"
        static let mobileTFLabelText = "Enter Mobile Number".localized()
        static let nameTFLabelText = "Enter Name".localized()
        static let nameTFPlaceholder = "Enter name".localized()
        static let mobileTFPlaceholderText = "Enter 10 digit number".localized()
        static let notifyMeBtnText = "Notify Me".localized()
        static let exploreServiceBtnText = "Explore other services".localized()
    }
    
    enum LeadCreationFormScene {
        static let emailTFLabelText = "Enter Email (Optional)"
        static let specialAttention = "Your request needs special attention"
        static let largerNoOfAppl = "You can select a maximum of %@ in each appliance. Our expert will contact you shortly."
        static let okayGreat = "Okay, great!"
        static let notNow = "Not now"
    }
    
    enum ServiceRequest{
        enum ActionKey{
            static let call = "ACTION_CALL"
            static let chat = "ACTION_CHAT" //added for sod
            static let submitDetails = "ACTION_SUBMIT_DETAIL"
            static let viewDetail = "ACTION_VIEW_DETAIL"
            static let whyBer = "WHY_BER"
            static let bankDetails = "ACTION_BANK_DETAIL"
            static let pay = "ACTION_PAY"
            static let addToCalender = "ACTION_ADD_TO_CALENDER"
            static let reshedule = "RESCHEDULE"
            static let seeAll = "SEE_ALL"
            static let optOut = "OPT_OUT"
            static let payLater = "PAY_LATER"
            static let scheduleVisit = "SCHEDULE_VISIT"
        }
        enum navigationAction{
            static let redirectView = "redirectView"
        }
        enum textFieldText{
            static let emailPlaceholder = "Email ID"
            static let emailDescription = "Enter email ID"
            static let mobilePlaceholder = "Mobile no"
            static let mobileDescription = "Alternate Mobile no"
            static let closed = "Closed"
        }
        enum labelText {
            static let noResultFound =  "No results found"
            static let whatCovered = "What's covered?"
            static let whatNotCovered = "What's not covered?"
        }
        enum navigationBarText {
            static let serviceCenterNearYou = "Service Center Near You"
        }
        enum AlertMessage {
            enum title {
                static let oops = "Oops!"
                static let technicalError = "Technical Error!"
            }
            enum message{
                static let cantUpdateStatus = "Can't update your status."
                static let cantShowSR = "Can't Show your Service Request."
                static let notGetLocation = "Can't get your current location."
                static let canNotChoosePayLater = "Can't choose pay later."
            }
        }
        enum buttonName {
            static let add = "Add"
            static let map = "Map"
            static let list = "List"
         }
        enum JavaScript{
            static let functionName = "createTimelineFromStringJSON"
            static let fileName =  "timeline.js"
        }
        
        enum Titles{
            static let raiseARequest = "Raise a Service Request".localized()
            static let serviceDetails = "Service Details".localized()
        }
        enum plans{
            static let title = "Select a plan to raise service request".localized()
            static let alreadyOngoingClaim = "Looks like you cannot raise a service request for this membership. for further help, chat with us.".localized()
        }
        enum Pincode{
            static let title = "Please enter your residential pincode".localized()
            static let peTitle = "Please enter your Pincode".localized()
            static let subTitle = "We need your pincode to check if it is in our Serviceable area"
            static let peSubTitle = "We will need this to align quick pick up for your device"
            static let pincode = "Pincode".localized()
            static let pincodePlaceholder = "Enter 6 digit pincode".localized()
            static let continueText  = "Continue".localized()
            static let addressComplete = "Please enter your complete address".localized()
            static let peCompleteAddress = "Please enter your complete device pickup address".localized()
            static let address = "Address"
            static let enterAddress = "Enter Address".localized()
            static let addNewAddress = "Add new address".localized()
            static let indicator = "Checking pincode serviceability".localized()
            static let errorTitle  = "Sorry, we are currently not servicing in your residential area.".localized()
            static let errorSubtitle = "We apologize for the inconvenience caused.Please feel free to contact us for any queries.".localized()
            static let contactUs =  "Contact Us".localized()
            static let selfRepair = "Continue".localized()
            
            enum Error{
                static let pincode = "Please enter valid pincode".localized()
                static let address = "please enter valid address".localized()
            }
        }
        
        enum HappenedToAppliances{
            static let notWorking = "It's not working".localized()
            static let accidentalDamage = "There is an accidental damage".localized()
            static let stolen = "It got stolen (Burglary)".localized()
            static let caughtFire = "It caught fire".localized()
        }
        
        enum WhatHappened{
            static let noBoardedAssets = "It seems you do not have any appliances in your plan which are eligible for Periodic Maintenance for now."
            static let isServiceTenureActiveText  = "You can raise the service request once the manufacturing warranty ends"
        }
        
        enum WhenHappend{
            static let titleEW = "When did this happen?".localized()
            static let titleBGFR = "When did this happen?".localized()
            static let subtitle = "These details will help in a smooth claim process".localized()
            static let aroundTime = "And around what time?".localized()
            static let pickATime =  "Pick a date & time".localized()
            static let incidentDataCondition = "Note: Incident date cannot be a future date".localized()
        }
        enum onGoingCell{
            static let appoint = "We will appoint a surveyor very soon. This could take upto 7 working days".localized()
            static let furtherHelp = "For further help, please call us".localized()
            static let phone = "1800 123 3330".localized()
            static let serviceStatus = "Service Request has been received".localized()
        }
        enum TellWhatWrong{
            static let lblTitle = "Tell us what went wrong".localized()
        }
        enum uploadPhoto{
            static let title = "Please click a photo of the Police report or FIR and upload".localized()
            static let subtitle1 = "Image should have FIR Complaint number and the police station's address".localized()
            static let subtitle2 = "Make sure that image is not blurry".localized()
        }
        enum OnGoingHistory{
            static let title = "Please answer a few questions to initiate the claim.".localized()
        }
        
        enum ContactSelfRepairAsk{
            static let selfRepair = "GO WITH SELF REPAIR".localized()
            static let dontWorry = "Don't Worry!".localized()
            static let labelDesc = "We still have covered with our Self Repair option.To know more, contact us.".localized()
            static let labelTitle = "122003 (Gurgaon)".localized()
            static let labelSubtitle = "Sorry, the pincode is not in our serviceable area".localized()
        }
        
        enum SelfRepair{
            static let ServiceContactNumberTitle = "Service Center Contact Number".localized()
            static let estimationCelltitle = "Please ask the service center to provide a repair cost break up".localized()
            static let estimationInvoiceVerificationTitle = "Your estimation invoice is in verification process.The process may take up to 7 working days".localized()
            static let finalUploadInvoiceTitle = "You will get the final invoice from the service center when the repair is done".localized()
            static let finalInvoiceVerificationTitle = "Your final invoice is in verification process.The process may take upto 7 working days".localized()
            static let refundTitle = "Your refund will be processed within 7 working days.Please be patient.".localized()
            static let firstHeader = "NEAREST SERVICE CENTER".localized()
            static let secondHeader = "Upload Estimated Invoice".localized()
            static let thirdHeader = "Estimation Invoice Verification".localized()
            static let forthHeader = "Upload Final InVoice".localized()
            static let fifthHeader = "Final Invoice Verification".localized()
            static let sixthHeader = "Refund".localized()
        }
        
        enum UploadDamageImage {
            static let header = "Please upload %d images of your %@ for verification purposes".localized()
        }
        
        static let hi = "Hi ".localized()
        static let dontWorry = "Don’t worry, we have got you covered".localized()
        static let answer = "Please answer a few questions to initiate the claim".localized()
        static let letsBegin = "Let's Begin".localized()
        static let startPlanctivation = "Start Plan Activation".localized()
        static let whatHappend = "So tell us what happened to your appliance?".localized()
        static let whatHappenedService = "What service type do you wish to avail from your plan?".localized()
        static let whatHappendToDevice = "So tell us what happened to your device?"
        static let help = "Help us to narrow things down".localized()
        static let selectServiceType = "Select the type of service".localized()
        static let whichAppliance = "Which of your appliances is not working?".localized()
        static let serviceWhichAppliance = "Choose an appliance which you want to get serviced".localized()
        static let countTF = "0/140".localized()
        static let enable = "This will enable us to send the right technician"
        static let applianceCaughtFire = "Which of your appliances caught fire?".localized()
        static let applianceGotStolen = "Which of your appliances were stolen?".localized()
        static let selectMultipleApplainces = "You can select multiple appliances".localized()
        static let caughtFireTitle = "Please brief us a  bit more on how did this happens".localized()
        static let descTFText = "Briefly describe".localized()
        static let whatHappedSubTitle = "This will help our service experts to carry the right spare parts during the service visit".localized()
        static let pleaseSelectaReason = "Please select a reason".localized()
        static let expectedClouser = "Expected service closure between %@ - %@".localized()
        static let scheduleVisit = "Schedule Visit".localized()
    }
    enum Actions{
        static let whichAppliance = "Select Issue".localized()
        static let applianceCaughtFire = "How did this happen".localized()
    }
    enum BankDetailsScene {
        static let title = "Enter Bank Details".localized()
        static let accountHolderName = "Account holder's name".localized()
        static let accountNumber = "Beneficiary Account Number".localized()
        static let ifscCode = "IFSC Code".localized()
        static let placeHolderTF = "Enter here".localized()
        
    }
    enum nearByBankScene{
        static let selectBank = "Please select the bank you want to visit today".localized()
        static let noBank  = "Couldn't find the bank you are looking for?".localized()
        static let addCard = "To see any bank branch nearby, please add a card issued by that bank".localized()
        static let addFirstCard = "Register your debit and credit cards to see nearby bank branches.".localized()

    }
    enum InspectionSuccessScene {
        static let heading = "We have received your Inspection request for the preferred time slot".localized()
        static let subHeading = "In case our technicians are all occupied, we might push your Inspection request to the next available slot."
        static let secondaryButtonAction = "OK, got it!".localized()
    }
    
    enum ApplianceListScene {
        static let title = "My Appliances".localized()
    }
    
    enum PersonalElectronics {
        enum UploadDocuments {
            static let titleFindPhone = "Please make sure that you have turned off 'Find My iPhone' service.".localized()
            static let descriptionFindPhone = "Yes, I have turn off 'Find My iPhone' service on my device.".localized()
            static let descriptionFindPhoneLink = " How to turn off Find My iPhone service ?".localized()
            static let headerFindViaMobile = "Turn off Find My iPhone via Mobile?".localized()
            static let headerFindViaWeb = "Turn off Find My iPhone via Website?".localized()
        }
    }
    
    enum Alert {
        static let doBetter = "Help us do better".localized()
        static let howWeWorking = "Tell us how’s OneAssist working out for you so far."
        static let feedbackTitle = "Your feedback is important".localized()
        static let feedbackMessage = "Something on your mind? Give us a quick suggestion or feedback.".localized()
    }
    
    enum LoaderMessage {
        static let waitForStatus = "Please wait while we are checking your request status".localized()
        static let pleaseWait = "Please wait"
        static let refreshData = "Please wait while we refresh your data "
    }
    
    enum Button {
        static let awesome = "Awesome".localized()
        static let notGood = "Not Good".localized()
        static let submit = "Submit".localized()
    }
    
    enum SalesRevamp {
        static let rewardMessage = "Reward added to Memberships section."
        static let faq = "FAQ’s".localized()
        static let template = "Template".localized()
        static let renewalInProgress = "RENEWAL IN PROGRESS"
        static let renewalDescription = "We have received your payment. Your membership will be activated in 24 hrs"
        static let scratchViewTitleUnscratched = "Scratch & Win"
        static let shareText = "Hey, I just won ID Fence Free Trial which helps me monitor my bank cards, social media accounts and more. Download OneAssist App and stand a chance to win amazing rewards."
        static let scratchViewSubtitleUnscratched = "Get great deals, free trials, discounts on protection plans & much more."
        static let shareLabelText = "Tell your friends you just won an amazing reward. Share OneAssist and stand a chance to win more."
        static let shareButtonText = "  Tell Your Friends"
        static let scratchViewButtonScratched = "Claim Reward"
        static let scratchCardMessage = "ID Fence 30-Day Free Trial"
        static let scratchViewPrize = "Get First time report covering last 7 years of your personal information breaches."
        static let newText = "New"
        static let idFenceDashboardNotSetup = "Please wait while we setup your ID Fence dashboard. This may take few minutes."
        static let idFenceDashboardCustomerDoesntExist = "We couldn't find your IDFence Membership."
        static let idFenceDashboardSetupInProgress = "We are setting up your Dashboard. You will get notified via SMS/Email when it is ready"
        static let close = "CLOSE"
        static let buyNow = "Buy Now"
    }
    
    enum MembershipRevamp {
        static let renewNow = "RENEW NOW".localized()
        static let upgradeNow = "Upgrade Now".localized()
        static let reactivateNow = "Reactivate Now".localized()
        static let applianceDetails = "View Appliance Details".localized()
        static let billPaymentInfo = "Never miss your credit card bill payment. Set monthly reminders and pay bills easily.".localized()
        static let emptyMembershipText = "You don't have any membership. Buy a plan now.".localized()
        static let emptyMembershipButtonText = "Buy Plan".localized()
        static let loginFromMembershipTabText = "To see your memberships, Please verify mobile number.".localized()
        static let loginFromMembershipButtonText = "Verify mobile number".localized()
    }
    
    enum NumberVerifyAlerts {
        static let buttonText = "Verify mobile number".localized()
        static let loginButtonText = "Login with registered number".localized()
        
        enum buyPlan {
            static let message = "To Buy Plan, Please verify mobile number."
            static let detailMsg = "To Buy Plan, Please verify mobile number."
        }
        
        enum buySODPlan {
            static let message = "To continue booking, please verify your mobile number."
        }
        
        enum chatWithUs {
            static let message = "To chat with customer care, please verify mobile number."
        }
        
        enum activateVoucherLoginAlert {
             static let message = "Have a voucher code? To activate, please verify mobile number."
        }
        
        enum paycreditcardbill {
            static let message = "To pay credit card bills, please verify mobile number."
        }
        
        enum addCard {
            static let message = "To add your cards, please verify mobile number."
        }
        
        enum bookmarkOffer {
            static let message = "To bookmark an offer, please verify mobile number."
        }
        enum bookmarkLoginAlert {
          static let message = "To see your Bookmarks, please verify mobile number."
        }
        
        enum sod {
            static let message = "To continue booking, please verify your mobile number."
        }
        
        enum deeplinkLogin {
            static let message = "No details found. Login with registered mobile number"
            static let detailMsg = "There’s seems to be a mismatch with your account. Please login again."
        }
    }
    
    enum SelectbankToolTip {
        static let title = "Select bank to see related offers".localized()
        static let detail = "Save money on your daily spends. See offes on banks you transact with".localized()
    }
    
    enum MembershipDetailPopupOption {
        static let chat = "Need help? Chat with us".localized()
        static let planInvoice = "Download Plan Invoice".localized()
        static let insuranceCertificate = "Download Insurance Certificate".localized()
        static let benefitsTnC = "Download Benefits and T&Cs".localized()
        static let faq = "Read FAQs".localized()
    }
    
    enum GifterScenario {
        static let nameConfirmation = "Is your appliance purchase invoice under the name of"
        static let reasonHeading =  "Why is the name on appliance purchase invoice different?"
        static let spellingMistake = "These is a spelling mistake/ name is different"
        static let enterName = "Enter name (As on purchase invoice)"
        static let giftedSomeone = "Gifted by someone"
        static let gifterName = "Enter gifter's name"
        static let relationshipGifter = "Relationship with gifter"
        static let loaned = "Loaned/Second-hand"
        static let others = "Others"
        static let pleasespecifyRelationship = "Please specify relationship"
        static let pleasespecify = "Please specify"
        
        enum GifterTypeValue {
            static let spellingMistakeType = "SPELLING_ERROR"
            static let gifterSomeoneType = "GIFTED"
            static let loanedType = "SECOND_HAND"
            static let othersType = "OTHERS"
            static let verified = "VERIFIED"
        }
        
        enum assetAttributeKeyType {
            static let mismatchName = "MISMATCH_NAME"
            static let gifterRelation = "GIFTER_RELATION"
            static let gifterName = "GIFTER_NAME"
            static let otherReason = "OTHERS"
        }
    }
    
    enum RelationShips {
        static let father = "Father"
        static let mother = "Mother"
        static let brother = "Brother"
        static let sister = "Sister"
        static let child = "Child"
        static let husband = "Husband"
        static let wife = "Wife"
        static let friend = "Friend"
        static let other = "Other"
        static let allValues  = [RelationShips.father.uppercased(), RelationShips.mother.uppercased(), RelationShips.brother.uppercased(),
                                 RelationShips.sister.uppercased(), RelationShips.child.uppercased(), RelationShips.husband.uppercased(),
                                 RelationShips.wife.uppercased(), RelationShips.friend.uppercased()]
    }
}

// MARK:- MHC Strings
enum MhcStrings {
    
    static let inProgress = " in progress...".localized()
    static let waitAMoment = "Please wait a moment...".localized()
    
    enum AlertMessage {
        
        static let cancelTestTitle = "Cancel Mobile Health Test?".localized()
        static let cancelTest = "You can come back at a later time and complete the remaining tests.".localized()
        static let vibration = "For accurate results, keep your mobile on flat surface and then press continue.".localized()
        static let vibrationTitle = "Keep your mobile on flat surface".localized()
        static let speaker = "Please fully increase the volume and make sure earphones are not plugged in.".localized()
        static let speakerTitle = "Increase volume to full".localized()
        static let headphoneTitle = "Plug-in your earphones".localized()
        static let chargerTitle = "Plug-in your charger".localized()
        static let charger = "Connect your charger and make sure the mobile is in charging mode before starting this test.".localized()
        static let headphone = "Make sure to connect your earphones before starting this test.".localized()
        static let wifi = "Please enable WiFi and connect it with a strong network before continuing the test.".localized()
        static let wifiTitle = "Connect WiFi to a network".localized()
        static let bluetooth = "Please enable bluetooth before continuing to successfully clear this test.".localized()
        static let bluetoothTitle = "Enable Bluetooth in settings".localized()
        static let touchIdRetryAlert = " Please try again.".localized()
        
        // Setting messages
        static let touchIdTitle = "Register Touch ID in settings".localized()
        static let faceIdTitle = "Register Face ID in settings".localized()
        static let micSetting = "Please allow microphone & speaker permission in settings to successfully clear this test.".localized()
        static let micTitle = "Enable microphone permission".localized()
        static let speechSetting = "Open setting and turn on speech recognition permission to complete the test.".localized()
        static let gpsSetting = "To enable, Go to settings > Tap on Location > Choose While Using the App.".localized()
        static let gpsTitle = "Enable location permission".localized()
        static let cameraSetting = "Please allow camera permission in settings to successfully clear this test.".localized()
        static let cameraTitle = "Enable camera permission".localized()
        static let touchIdSetting = "To test Touch ID feature, please set up your Touch ID in settings.".localized()
        static let faceIdSetting = "To test Face ID feature, please set up your Face ID in settings.".localized()
        static let faceIdPermissionTitle = "Enable Face ID permission".localized()
        static let faceIdPermissionSetting = "Please allow face ID permission in settings to successfully clear this test.".localized()
        
        static let disablePushHeader = "Disable notifications?".localized()
        static let disablePushParagraph = "To disable notifications, Go to settings > Tap on Notifications > Disable".localized()
        
        static let enablePushHeader = "Enable notifications?".localized()
        static let enablePushParagraph = "To Enable notifications, Go to settings > Tap on Notifications > Enable".localized()
        
    }
    
    enum Buttons {
        static let `continue` = "Continue".localized()
        static let skip = "Skip test".localized()
        static let gotIt = "Okay, got it".localized()
        static let cancel = "Cancel".localized()
        static let settings = "Go to settings".localized()
        static let retry = "Retry".localized()
        static let enable = "Enable".localized()
        static let cancelTest = "Yes, cancel test".localized()
        static let dismiss = "Dismiss".localized()
        static let startTest = "Start test".localized()
        static let resumeTest = "Resume test".localized()
        static let startNewTest = "Start new test".localized()
        static let retryTests = "Retry tests".localized()
        static let completeTests = "Complete Tests".localized()
        static let iWillDoItLater = "I'll do it later".localized()
    }
    
    // Test Strings
    
    enum Speaker {
        static let startTitle = "Microphone & Speaker".localized()
        static let startDescription = "A Sound will be played from your speaker and will be detected via your microphone.".localized()
        static let startSubdescription = "A sound will be played through loud speaker and we'll try to capture that sound via receiver speaker.".localized()
        static let playingSound = "Playing sound...".localized()
        static let capturingSound = "Capturing sound...".localized()
    }
    
    enum OtherTest {
        static let startTitle = "Other tests".localized()
        static let retryTitle = "Other".localized()
        static let startDescription = "WiFi, Bluetooth, RAM & Storage tests will be performed automatically.".localized()
    }
    
    enum Vibration {
        static let startTitle = "Vibration".localized()
        static let startDescription = "Make sure vibration is enabled in mobile settings. Keep your mobile on a flat surface for accuracy.".localized()
        static let inProgress = "Vibration in progress...".localized()
    }
    
    enum GPS {
        static let startTitle = "GPS/Location".localized()
        static let startDescription = "Current location/GPS accuracy will be checked. Make sure you are in a good connectivity area.".localized()
        static let fetchingLocation = "Checking GPS accuracy...".localized()
    }
    
    enum Camera {
        static let startTitle = "Camera".localized()
        static let startDescription = "We will capture 2 images using your camera. No images will be saved during the process.".localized()
    }
    
    enum VolumeButton {
        static let startTitle = "Volume buttons".localized()
        static let startDescription = "We will prompt you to press volume up and volume down.".localized()
    }
    
    enum ChargingPort {
        static let startTitle = "Charging port".localized()
        static let startDescription = "Plug in your charger into your mobile and press start. Make sure your mobile is charging.".localized()
    }
    
    enum Headphone {
        static let startTitle = "Earphones jack".localized()
        static let startDescription = "Plug in your wired earphones into your mobile and press start.".localized()
    }
    
    enum MultiTouch {
        static let startTitle = "Touchscreen".localized()
        static let startDescription = "Press start and swipe your finger on the next screen to remove the blue color.".localized()
    }
    
    enum Accelerometer {
        static let startTitle = "Accelerometer & Magnetometer".localized()
        static let startDescription = "Press start then shake your mobile up & down, and sideways when prompted.".localized()
        static let instruction = "Shake your mobile up & down, and sideways for atleast 2 seconds.".localized()
    }
    
    enum Gyroscope {
        static let startTitle = "Gyroscope".localized()
        static let startDescription = "It is mostly used in 3D games motion gaming apps. Press start and tilt your mobile to score a goal.".localized()
        static let tipMessage = "Tap anywhere to start. Tilt your phone to score a goal!"
    }
    
    enum Proximity {
        static let startTitle = "Proximity".localized()
        static let startDescription = "It is used to reduce display power consumption by turning off the LCD backlight when on call.".localized()
        static let instruction = "Cover the top area with your hand as shown above".localized()
    }
    
    enum FingerprintSensor {
        static let startTitle = "Fingerprint".localized()
        static let startDescription = "Press start and place your finger on the fingerprint sensor when promted.".localized()
        static let instruction = "Place your finger on the fingerprint sensor now till you feel a slight vibration.".localized()
        
    }
    
    enum FaceIdSensors {
        static let startTitle = "Face ID/Face detection".localized()
        static let startDescription = "Press start and hold your mobile up right to scan your face when prompted.".localized()
        static let instruction = "Please make sure you have register a face ID in your iPhone".localized()
        
    }
    
    enum NFC {
        static let startTitle = "NFC".localized()
        static let startDescription = "NFC is used to share contacts, images & files from one mobile to another.".localized()
        static let instruction = "Near-Field Communication test".localized()
    }
    
    enum Common {
        static let viewDetails = "View details".localized()
        static let completeRemaingTests = "Complete Remaining Tests".localized()
        static let shareString = "Hey, I just checked the health of my mobile using Mobile health checker on OneAssist app. To know your mobile health right away download the app from here http://goo.gl/UjBqf2".localized()
    }
    
    enum CategoryScreen {
        static let screenTitle = "Mobile Health".localized()
        static let testsRemaining = "TESTS REMAINING".localized()
        static let testCompleted = "TEST COMPLETED".localized()
        static let testFailed = "TEST FAILED".localized()
        static let notYetStarted = "Not yet started.".localized()
        
        static let buybackInitialTitle = "Test your mobile on these components to get best resale offer.".localized()
        static let buybackInitialSubtitle = "Higher the score, better the price.".localized()
        static let buybackPendingTitle = "Get a better score by testing few remaining components.".localized()
        static let remainingTests = "REMAINING TESTS".localized()
        static let buybackLastResultSubtitle = "Based on recent mobile health check".localized()
        static let buybackLastResultTitleExcellent = "Few mobile components are in excellent condition. 👍".localized()
        static let buybackLastResultTitlePoor = "Mobile health score was poor. Test agian.".localized()
        
        static let mhcCompletedTitle = "Know your iPhone’s health in just a few steps.".localized()
        static let mhcCompletedTipTitle = "Tip: Keep your charger and earphones handy for the test.".localized()
        static let mhcSelectionTitle = "* Selected groups will be tested".localized()
        
        static let mhcDeselectButtonTitle = "Deselect all".localized()
        static let mhcSelectButtonTitle = "Select all".localized()
        static let lastCheckedOn = "Last checked on".localized()
        static let mhcLastResultTitleExcellent = "Your iPhone health was Excellent 👍".localized()
        static let mhcLastResultTitleGood = "Your iPhone health was Good.".localized()
        static let mhcLastResultTitleAverage = "Your iPhone health was Average.".localized()
        static let mhcLastResultTitlePoor = "Your iPhone health was Poor.".localized()
        static let mhcPendingTitle = "Finish your mobile health check now! Few more tests to go.".localized()
        static let startNewTestAlertTitle = "Are you sure you want to start all over again? Previous data will be lost.".localized()
        static let startNewTestAlertPrimary = "Yes, start new test".localized()
    }
    
    enum ResultMainScreen {
        static let buybackTitleExcellent = "Great! Mobile health score is Excellent.".localized()
        static let buybackSubtitleExcellent = "Resale price is only a step away. Tell us a little more about your mobile.".localized()
        static let buybackTitleGood = "Great! Mobile health score is Good.".localized()
        static let titleAverage = "Mobile health score is Average.".localized()
        static let titlePoor = "Mobile health score is Poor.".localized()
        static let buybackSubtitlePoor = "Poor score will affect final price! Test remaining components for better score.".localized()
        static let mhcGroupResultTitleExcellent = "Mobile health score is Excellent.".localized()
        static let mhcGroupResultSubtitleExcellent = "Good job! All selected tests are working fine. Move ahead to see a detailed result.".localized()
        static let mhcGroupResultTitleGood = "Mobile health score is Good.".localized()
        static let mhcGroupResultAverageSubtitle = "Your mobile can do better. See detailed result to see what went wrong.".localized()
        static let mhcGroupResultPoorSubtitle = "Don't be disappointed. See detailed result to see what went wrong.".localized()
        static let mhcResultExcellentSubtitle = "You did good! Keep a regular check on your mobile's health every week.".localized()
    }
    
    enum ResultScreen {
        static let screenTitle = "Detailed Result".localized()
        static let topHeaderExcellent = "Your iPhone health is Excellent".localized()
        static let topHeaderGood = "Your iPhone health is Good.".localized()
        static let topHeaderAverage = "Your iPhone health is Average.".localized()
        static let topHeaderPoor = "Your iPhone health is Poor.".localized()
        static let awesomeHeaderTitle = "Awesome 👍\nYou cleared all the component tests.".localized()
        static let awesomeHeaderSubtitle = "Tell your friends about your mobile health score.".localized()
        static let sectionHeaderTitleAverage = "Get a better score by testing remaining components.".localized()
        static let sectionHeaderTitlePoor = "Retake test and make sure your mobile is in a good condition.".localized()
        static let mobileInfoSectionTitle = "MOBILE INFO".localized()
        static let testResultSectionTitle = "TEST RESULT".localized()
        static let cleared = "Cleared".localized()
        static let failed = "Failed".localized()
        static let skipped = "Skipped".localized()
    }
}
