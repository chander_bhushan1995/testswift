//
//  BankListFooterView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 27/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol AddCardButtonDelegate:class
{
    func addCardButtonTapped()
}
class BankListFooterView: UIView {

    @IBOutlet weak var noBankTitle: UILabel!
    @IBOutlet weak var noBankDescription: BodyTextRegularBlackLabel!
    @IBOutlet weak var addCardButton : UIButton!
    weak var delegate:AddCardButtonDelegate?
        /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> BankListFooterView {
        return UINib(nibName: "BankListFooterView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BankListFooterView
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    @IBAction func addCardButtonTapped(_ sender: Any) {
        self.delegate?.addCardButtonTapped()
        }

    func configureView() {
//        self.addCardButton.clipsToBounds = true
//        self.addCardButton.layer.cornerRadius = self.addCardButton.frame.size.height / 2.0
    }

    
    
    class func viewHeight() -> CGFloat {
        return 168.0
    }
}
