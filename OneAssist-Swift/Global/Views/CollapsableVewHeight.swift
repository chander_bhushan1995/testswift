//
//  CollapsableVewHeight.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

protocol CollapsableVewHeight {
    
}

extension CollapsableVewHeight where Self: UIView {
    
    @discardableResult
    func setZero(for attributes: [NSLayoutConstraint.Attribute]) -> [(NSLayoutConstraint,CGFloat)] {
        
        var constraintConstants: [(NSLayoutConstraint,CGFloat)] = []
        for constraint in constraints {
            if attributes.contains(constraint.firstAttribute) || attributes.contains(constraint.secondAttribute) {
                constraintConstants.append((constraint,constraint.constant))
                constraint.constant = 0
            }
        }
        
        layoutIfNeeded()
        return constraintConstants
    }
    
    func resetConstraint(for constraintConstants: [(NSLayoutConstraint, CGFloat)]) {
        constraintConstants.forEach { (data) in
            data.0.constant = data.1
        }
        layoutIfNeeded()
    }
}

extension UIView: CollapsableVewHeight { }
