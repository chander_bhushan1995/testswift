//
//  ReadMoreTextView.swift
//  LinkText
//
//  Created by Varun Dudeja on 13/07/17.
//  Copyright © 2017 Varun. All rights reserved.
//

import UIKit

protocol LinkTextViewDelegate: class {
    
    /// This method used to give callback on click of hyper link in label
    func linkClicked(linkText: String)
}

class LinkTextView: UITextViewFixed, UITextViewDelegate {
    
    weak var linkDelegate: LinkTextViewDelegate?
    
    override func awakeFromNib() {
        delegate = self
        isSelectable = true
    }
    
    var viewFont: UIFont = DLSFont.h3.regular
    
    private var linkText: String = ""
    
    func setLink(completeText: String, linkText: String) {
        
        self.linkText = linkText
        
        let paragraphStyle = NSMutableParagraphStyle()
        let mutableString = NSMutableAttributedString(string: completeText)
        let str = completeText as NSString
        let range = str.range(of: linkText)
        mutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, completeText.count))
        mutableString.addAttribute(NSAttributedString.Key.link, value: "link1", range: range)
        mutableString.addAttribute(NSAttributedString.Key.font, value: viewFont, range: NSMakeRange(0, completeText.count))
        attributedText = mutableString
    }
    
    func setAttributedLink(completeText: NSAttributedString, linkText: String) {
        
        self.linkText = linkText
        
        let paragraphStyle = NSMutableParagraphStyle()
        let mutableString = NSMutableAttributedString(attributedString: completeText)
        let str = completeText.string as NSString
        let range = str.range(of: linkText)
        mutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, completeText.string.count))
        mutableString.addAttribute(NSAttributedString.Key.link, value: "link1", range: range)
        attributedText = mutableString
    }
    
    
    // MARK: - Text View Delegate
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        let str = textView.text as NSString
        let range = str.range(of: linkText)
        if range.location == characterRange.location && range.length == characterRange.length {
            
            linkDelegate?.linkClicked(linkText: linkText)
            return true
        }
        
        return false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let str = textView.text as NSString
        let range = str.range(of: linkText)
        if range.location == characterRange.location && range.length == characterRange.length {
            
            linkDelegate?.linkClicked(linkText: linkText)
            return true
        }
        
        return false
    }

}

class ReadMoreTextView: LinkTextView, LinkTextViewDelegate {

    private var isReadMoreOn: Bool = false
    private var completeText: String = ""
    private var readLinkText: String {
        return isReadMoreOn ? "Read Less" : "...Read More"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        linkDelegate = self
    }
    
    func setText(text: String, charactersLimit: Int = 100) {
        completeText = text
        setText()
    }
    
    func setText() {
		
		let charCount = completeText.count
        
        guard charCount > 100 else {
            attributedText = NSAttributedString(string: completeText)
            return
        }
        
        if isReadMoreOn {
            
            let text2 = completeText + " " + readLinkText
            setLink(completeText: text2, linkText: readLinkText)
        } else {
			
            let index = completeText.index(completeText.startIndex, offsetBy: 100)
            let partialText = completeText.substring(to: index)
            let text2 = partialText + " " + readLinkText
            setLink(completeText: text2, linkText: readLinkText)
        }
    }
    
    // MARK: - Link Delegate Conformance
    
    func linkClicked(linkText: String) {
        
        isReadMoreOn = !isReadMoreOn
        
        DispatchQueue.main.async {
            self.setText()
        }
    }
}
