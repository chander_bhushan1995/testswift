//
//  DropShadowView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 4/10/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class DropShadowView: UIView {
    
    private var hasShadow: Bool = false
    
    @IBInspectable
    var isTopView: Bool = false
    
    fileprivate func addShadow(bool: Bool) {
        
        if bool && !hasShadow {
            
            let shadowPath = UIBezierPath(rect: bounds)
            layer.masksToBounds = false
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOffset = CGSize(width: 0.0, height: (isTopView ? 2.0 : -2.0))
            layer.shadowRadius = 2
            layer.shadowOpacity = 0.5
            layer.shadowPath = shadowPath.cgPath
            hasShadow = true
            
        } else if !bool && hasShadow {
            layer.shadowOpacity = 0.0
            hasShadow = false
        }
    }
}

extension DropShadowView {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if isTopView {
            if scrollView.contentOffset.y > 0 {
                addShadow(bool: true)
            } else {
                addShadow(bool: false)
            }
        } else {
            if scrollView.contentOffset.y + scrollView.frame.height >= scrollView.contentSize.height {
                addShadow(bool: false)
            } else {
                addShadow(bool: true)
            }
        }
    }
}
