//
//  LabelInputAccesoryView.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class LabelInputAccesoryView: UIView {

    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnDone: UIButton!
    
    func setUpdateUserInterface(isValid: Bool, title: String) {
        lblTitle.text = title
        circularView.layer.cornerRadius = circularView.bounds.width / 2
        imgView.image = #imageLiteral(resourceName: "tick").withRenderingMode(.alwaysTemplate)
        self.isValid = isValid
    }
    
    var isValid: Bool = false {
        didSet {
            if isValid {
                lblTitle.textColor = UIColor.forestGreen
                circularView.backgroundColor = UIColor.forestGreen
                imgView.tintColor = UIColor.white
            } else {
                circularView.backgroundColor = UIColor.lightGray
                lblTitle.textColor = UIColor.lightGray
                imgView.tintColor = UIColor.gray
            }
        }
    }
}
