//
//  DLSCurvedView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 19/03/19.
//  Copyright © 2019 Mukesh. All rights reserved.
//

import UIKit

@IBDesignable
class DLSCurvedView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        clipsToBounds = true
        layer.cornerRadius = 6
    }
    
}
