//
//  CurvedView.swift
//  OneAssist-Swift
//
//  Created by Varun on 02/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@IBDesignable
class CurvedView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initializeView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initializeView()
	}
	
	private func initializeView() {
		layer.cornerRadius = 6.0
        layer.borderColor = UIColor.gray2.cgColor
        layer.borderWidth = 1.0
        layer.contentsScale = UIScreen.main.scale
        layer.rasterizationScale = UIScreen.main.scale
		clipsToBounds = true
	}

}
