//
//  DLSShadowView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 27/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class DLSShadowView: UIView {
    
    var isShadowHidden = false {
        didSet {
            if isShadowHidden {
                layer.shadowColor = UIColor.clear.cgColor
                layer.shadowOpacity = 0
            } else {
                layer.shadowColor = UIColor.dlsShadow.cgColor
                layer.shadowOpacity = 1
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        clipsToBounds = false
        layer.cornerRadius = 6
        layer.shadowColor = UIColor.dlsShadow.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowRadius = 4
    }
    
}
