//
//  CustomSegmentControl.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 04/02/21.
//  Copyright © 2021 OneAssist. All rights reserved.
//

import Foundation
class CustomSegmentControl: UISegmentedControl {
    private func stylize() {
        if #available(iOS 13.0, *) {
            selectedSegmentTintColor = tintColor
            let tintColorImage = UIImage(color: tintColor)
            setBackgroundImage(UIImage(color: backgroundColor ?? .clear), for: .normal, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: .selected, barMetrics: .default)
            setBackgroundImage(UIImage(color: tintColor.withAlphaComponent(0.2)), for: .highlighted, barMetrics: .default)
            setBackgroundImage(tintColorImage, for: [.highlighted, .selected], barMetrics: .default)
            setTitleTextAttributes([.foregroundColor: tintColor!, NSAttributedString.Key.font: DLSFont.bodyText.regular], for: .normal)
            
            setDividerImage(tintColorImage, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
            layer.borderWidth = 1
            layer.borderColor = tintColor.cgColor
            self.setTitleTextAttributes([.foregroundColor: UIColor.white, NSAttributedString.Key.font: DLSFont.bodyText.medium], for: .selected)
        }
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        stylize()
    }
}
