//
//  TagStyledView.swift
//  Pods-TagStyledView_Example
//
//  Created by Dave on 08/11/2018.
//

import Foundation
import UIKit

public protocol TagPresentable: class {
    var cell: UICollectionViewCell { get }
}

public extension TagPresentable where Self: UICollectionViewCell {
    var cell: UICollectionViewCell { return self }
}

public protocol TagStyling: TagPresentable {
    var tagLabel: UILabel! { get }
    func updateStyle(_ isViewMore: Bool)
}

extension TagStyling {
    var fittingSize: CGSize {
        return cell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    func layoutIfNeeded() {
        cell.layoutIfNeeded()
    }
    
    func setNeedsLayout() {
        cell.setNeedsLayout()
    }
}

public extension TagStyledView {
    struct Options {
        public let sectionInset: UIEdgeInsets
        public let lineSpacing: CGFloat
        public let interitemSpacing: CGFloat
        public let align: TagStyledView.Options.Alignment
        
        public init(sectionInset: UIEdgeInsets = .zero,
                    lineSpacing: CGFloat = 10.0,
                    interitemSpacing: CGFloat = 10.0,
                    align: TagStyledView.Options.Alignment = .justified) {
            self.sectionInset = sectionInset
            self.lineSpacing = lineSpacing
            self.interitemSpacing = interitemSpacing
            self.align = align
        }
    }
}

public extension TagStyledView.Options {
    enum Alignment {
        case justified
        case left
        case center
        case right
    }
}

public class TagStyledView: UIView {
    
    public var tags: [AnyObject]? {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    public var options: TagStyledView.Options? {
        didSet {
            guard let options = options else { return }
            layout.sectionInset = options.sectionInset
            layout.minimumInteritemSpacing = options.interitemSpacing
            layout.minimumLineSpacing = options.lineSpacing
            layout.align = options.align
            
            collectionView.reloadData()
        }
    }
    
    var maximumTags: Int? = nil
    var bottomSheetTitle: String? = nil
    
    public var cellForItem: (((cell: TagStyling, indexPath: IndexPath)) -> Void)?
    public var didSelectItemAt: ((IndexPath) -> Void)?
    public var containerSize: ((CGSize) -> Void)?
    
    private let layout = TagStyleFlowLayout()
    private var collectionView: WrappingCollectionView!
    
    private var reseource: (cell: TagStyling, identifier: String)?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    public func register(_ nib: UINib, forCellWithReuseIdentifier identifier: String) {
        collectionView.register(nib, forCellWithReuseIdentifier: identifier)
        reseource = (nib.instantiate(withOwner: nil, options: nil).first as! TagStyling, identifier)
    }
    
    private func setup() {
        collectionView = WrappingCollectionView(frame: bounds, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        addSubview(collectionView)
        
        addConstraint(collectionView.topAnchor.constraint(equalTo: topAnchor))
        addConstraint(collectionView.trailingAnchor.constraint(equalTo: trailingAnchor))
        addConstraint(collectionView.leadingAnchor.constraint(equalTo: leadingAnchor))
        addConstraint(collectionView.bottomAnchor.constraint(equalTo: bottomAnchor))
        
        collectionView.containerSize = {[unowned self] size in
            self.containerSize?(size)
        }
    }
    
    private func configureCell(_ cell: TagStyling, forIndexPath indexPath: IndexPath) {
        guard let tagArray = tags else { return }
        let tags = tagArray[indexPath.row]
        if let maximumTags = maximumTags, tagArray.count > maximumTags, indexPath.item == maximumTags - 1 {
            cell.tagLabel.text = "...view more"
            cell.updateStyle(true)
        } else {
            cell.tagLabel.text = titleForTag(tags)
            cell.updateStyle(false)
        }
    }
    
    private func titleForTag(_ tag: AnyObject) -> String? {
        if let cardObject = tag as? UniqueCards {
            return cardObject.cardIssuerName
        } else if let cardObject = tag as? CardDetail {
            return cardObject.tagging ?? cardObject.cardIssuerName
        } else if let tag = tag as? RatingTagVM {
            return tag.tagText
        }else{
            return nil
        }
    }
    
    private func allTagsText() -> String? {
        return tags?.compactMap({titleForTag($0)?.capitalized}).joined(separator: ", ")
    }
}

extension TagStyledView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let tagArray = tags else { return 0 }
        if let maximumTags = maximumTags {
            return min(tagArray.count, maximumTags)
        } else {
            return tagArray.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let resource = reseource,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: resource.identifier, for: indexPath) as? TagStyling else {
                return UICollectionViewCell()
        }
        
        configureCell(cell, forIndexPath: indexPath)
        
        cellForItem?((cell, indexPath))
        
        return cell as! UICollectionViewCell
    }
}

extension TagStyledView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell = reseource?.cell else { return .zero }
        
        configureCell(cell, forIndexPath: indexPath)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell.fittingSize
    }
}

extension TagStyledView: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let tagArray = tags, let maximumTags = maximumTags, tagArray.count > maximumTags, indexPath.item == maximumTags - 1 {
            let model = ExclusiveBenfit(dictionary: [:])
            model.title = bottomSheetTitle
            model.benefitDescription = allTagsText()
            let bottomSheet = ExclusiveBenefitBottomSheetView()
            bottomSheet.setViewModel(model, delegate: nil)
            Utilities.presentPopover(view: bottomSheet, height: ExclusiveBenefitBottomSheetView.height(forModel: model))
        }
        didSelectItemAt?(indexPath)
    }
}

final class WrappingCollectionView: UICollectionView {
    public var containerSize: ((CGSize) -> Void)?
    
    override func reloadData() {
        super.reloadData()
        
        invalidateIntrinsicContentSize()
        superview?.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        self.containerSize?(contentSize)
        return contentSize
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        invalidateIntrinsicContentSize()
        superview?.layoutIfNeeded()
    }
}
