//
//  TableViewSection.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class TableViewDataSource<Cell, Section: TableViewSection<Cell>> {
    var tableHeader: String?
    var tableFooter: String?
    var tableSections: [Section] = []
    
    var numberOfSections: Int {
        return tableSections.count
    }
    
    func numberOfRows(in section: Int) -> Int {
        return tableSections[section].numberOfRows
    }
    
    func sectionModel(for section: Int) -> Section {
        return tableSections[section]
    }
    
    func rowModel(for indexPath: IndexPath) -> Cell {
        return tableSections[indexPath.section].rows[indexPath.row]
    }
}

class Row {
    var title: String!
    var atributedTitle: NSAttributedString?
    var imageName: String!
    var imgUrl : String!
    var type: Any!
}

class TableViewSection<Cell: Row> {
    var title: String?
    var titleType: String?
    var subtitle: String?
    var isSelected: Bool = false
    var isEnabled : Bool = true
    var isCommentTextView: Bool = false
    var rows: [Cell] = []
    var numberOfRows: Int {
        return rows.count
    }
}

class PagingSection<Cell:Row> :TableViewSection<Cell>{
    var pageNumber = 1
    var pageSize = 20
    var boolMoreDataAvailable = false
    var boolDataLoading = false
}
