//
//  OfferFilterSectionHeaderView.swift
//  OneAssist-Swift
//
//  Created by Varun on 08/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class OfferFilterSectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var labelHeader: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        labelHeader.font = DLSFont.bodyText.regular
        labelHeader.textColor = UIColor.gray
    }
    
    func configureView(header: String) {
        labelHeader.text = header
    }
    
}
