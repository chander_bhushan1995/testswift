//
//  RaisedButton.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 24/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class RaisedButton: UIButton {
    
    var gradientLayer: CALayer?
    var enableState :Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDefaultStateTheme()
        
    }
    
    func setDefaultStateTheme() {
        enableState = false
        clipsToBounds = true
        layer.cornerRadius = frame.size.height / 2.0
        //titleLabel?.font = UIFont(name: SF_FONT_SEMIBOLD, size: RAISED_BUTTON_FONT_SIZE)!
        setTitleColor(UIColor.white, for: .normal)
        addGradientLayer()
    }
    
    // Method to set enabled/disabled state
    func setEnabledState(_ enabledState: Bool) {
        enableState = enabledState
        if enabledState {
            setTitleColor(UIColor.white, for: .normal)
            backgroundColor = UIColor.dodgerBlue
            addGradientLayer()
        }
        else {
            removeGradientLayer()
            setTitleColor(UIColor.lightGray, for: .normal)
            backgroundColor = UIColor.gray2
        }
    }
  
    func addGradientLayer() {
        if gradientLayer == nil {
            //gradientLayer = OAUtility.addGradient(withStartColor: UIColor.raiseButtonGradientStartColor, endColor: UIColor.raiseButtonGradientEndColor, toView: self)
        }
    }
    
    // Method to remove gradient from button
    func removeGradientLayer() {
        if gradientLayer != nil {
            gradientLayer?.removeFromSuperlayer()
            gradientLayer = nil
        }
    }
    
    
    
}

