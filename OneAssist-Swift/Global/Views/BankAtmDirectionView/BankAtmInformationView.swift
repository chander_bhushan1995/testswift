//
//  BankAtmInformationView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 20/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@objc protocol BankAtmDirectionDelegate {
    @objc optional func directionsButtonTapped(coordinates : CLLocationCoordinate2D)
}

class BankAtmInformationView: BaseNibView {

    weak var delegate : BankAtmDirectionDelegate?

    var bankCoordinates : CLLocationCoordinate2D!
    @IBOutlet weak var showDirectionButton : UIButton!
    @IBOutlet weak var bankIconImageView : UIImageView!
    @IBOutlet weak var bankNameLabel : UILabel!
    @IBOutlet weak var bankAddressLabel : UILabel!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    func configureView() {
        showDirectionButton.clipsToBounds = true
        showDirectionButton.layer.cornerRadius = showDirectionButton.frame.size.width / 2.0
//        showDirectionButton.dropShadow(color: .black)
    }

    func updateData(bankName : String?, bankAddress : String?, iconUrl : String?) {
        self.bankNameLabel.text = bankName
        self.bankAddressLabel.text = bankAddress
    }

    @IBAction func directionsButtonTapped(_ sender: Any) {
        self.delegate?.directionsButtonTapped?(coordinates: self.bankCoordinates)
    }
}
