//
//  BankDirectionView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@objc protocol BankDirectionViewDelegate {
    @objc optional func callButtonTapped(phoneNumber : String?)
    @objc optional func directionsButtonTapped(coordinates : CLLocationCoordinate2D)
}

class BankDirectionView: BaseNibView {

    weak var delegate : BankDirectionViewDelegate?
    var bankPhoneNumber : String?

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var directionView : BankAtmInformationView!
    @IBOutlet var constraintBankCallViewHeight: NSLayoutConstraint!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        directionView.delegate = self
    }

    func updateData(bankName : String?, bankAddress : String?, iconUrl : String?) {
        directionView.updateData(bankName : bankName, bankAddress : bankAddress, iconUrl : nil)
    }

    @IBAction func callButtonTapped(_ sender: Any) {
        self.delegate?.callButtonTapped?(phoneNumber : self.bankPhoneNumber)
    }
}

extension BankDirectionView : BankAtmDirectionDelegate {
    func directionsButtonTapped(coordinates: CLLocationCoordinate2D) {
        self.delegate?.directionsButtonTapped?(coordinates: coordinates)
    }
}
