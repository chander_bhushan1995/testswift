//
//  UITextViewFixed.swift
//  OneAssist-Swift
//
//  Created by Varun on 19/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class UITextViewFixed: UITextView {

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}
