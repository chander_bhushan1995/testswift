//
//  NotifyBannerView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 05/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class NotifyBannerView: BaseNibView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
