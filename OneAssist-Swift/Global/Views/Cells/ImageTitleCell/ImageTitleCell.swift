//
//  ImageTitleCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/21/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class ImageTitleCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layoutMargins = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    
    func set(text: String?) {
        titleLabel.text = text
        layoutIfNeeded()
    }
}
