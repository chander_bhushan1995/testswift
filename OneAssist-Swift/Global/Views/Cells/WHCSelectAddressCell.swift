//
//  WHCSelectAddressCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
protocol WHCSelectAddressCellDelegate: class {
    func cellSelected(row: Int)
    func editButtonTapped(row: Int)
}

class WHCSelectAddressCell: UITableViewCell {
    
    var delegate: WHCSelectAddressCellDelegate?
    
    @IBOutlet weak var buttonSelect: UIButton!
    @IBOutlet weak var radiobtnImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.font = DLSFont.h3.bold
        addressLabel.font = DLSFont.h3.bold
        addressLabel.textColor = UIColor.gray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setImageForButton(_ selected: Bool){
        if selected {
            radiobtnImageView.image = #imageLiteral(resourceName: "radioButtonActive")
        }
        else {
            radiobtnImageView.image = #imageLiteral(resourceName: "radioButtonInactive")
        }
    }
    
    func setModel(_ model: AddressDetail, isSelected: Bool, isEditable: Bool, name: String) {
        nameLabel.text = name
        addressLabel.text = String(format: "%@ %@\n%@, %@ - %@", model.address, model.address2, model.city, model.state, model.pincode ?? "")
        setImageForButton(isSelected)
        buttonEdit.isHidden = !(isEditable)
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        self.delegate?.cellSelected(row: (sender as! UIButton).tag)
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        self.delegate?.editButtonTapped(row: (sender as! UIButton).tag)
    }
}
