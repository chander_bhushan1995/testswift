//
//  TechnicianDetailCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
import Kingfisher

class TechnicianDetailCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet var containerView: UIView!
    
    @IBOutlet var transparentView: UIView!
    
    @IBOutlet var otpLabel: UILabel!
    
    @IBOutlet var technicianImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var experienceLabel: UILabel!
    
    @IBOutlet weak var technicianDetail: UILabel!
    
    @IBOutlet var contactNumberLabel: UILabel!
    
    var action:Action?
    @IBOutlet weak var serviceCenterContactNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
   
    func initializeView() {
        containerView?.layer.cornerRadius = 9.0
        containerView?.layer.borderColor = UIColor.gray2.cgColor
        containerView?.layer.borderWidth = 1.0
        transparentView.layer.cornerRadius = 8.0
        technicianImageView.layer.cornerRadius = 20
        technicianImageView.clipsToBounds = true
        contactNumberLabel.text = Constants.WHC.InspectionDetailsCell.customerCareNumber
        setUpUI()
        
        
    }
    func setUpUI()
    {
        // otpLabel.font = DLSFont.bodyText.regular
        // technicianDetail.font = DLSFont.tags.regular
        // nameLabel.font = DLSFont.bodyText.regular
        // experienceLabel.font = DLSFont.bodyText.regular
        // serviceCenterContactNumberLabel.font = DLSFont.bodyText.regular
        // contactNumberLabel.font = DLSFont.h3.regular
        // otpLabel.textColor = UIColor.charcoalGrey
        // technicianDetail.textColor = UIColor.gray
        // nameLabel.textColor = UIColor.charcoalGrey
        // experienceLabel.textColor = UIColor.gray
        // serviceCenterContactNumberLabel.textColor = UIColor.gray
        // contactNumberLabel.textColor = UIColor.charcoalGrey
    }
    
    func setViewModel(_ details: TechnicialDetail) {
        let otpText = Constants.WHC.technicianDetailsCell.shareOTP +
            "\(details.startOTP ?? "")" +  Constants.WHC.technicianDetailsCell.shareOTPSecondPart
        let attrString = NSMutableAttributedString(string: otpText)
        let boldRange = NSRange(location: 13, length: 8)
        attrString.addAttribute(NSAttributedString.Key.font, value: DLSFont.supportingText.bold, range: boldRange)
        otpLabel.attributedText = attrString
        
        let modifier = AnyModifier { request in
            var r = request
            r.setValue(AppUserDefaults.sessionToken, forHTTPHeaderField: Constants.headers.key.token)
            r.setValue(Constants.headers.value.appIdValue, forHTTPHeaderField:  Constants.headers.key.appIdKey)
            return r
        }
        let url = URL(string: Constants.WHC.URL.technicianDetail + "\(details.technicianId ?? "")" + Constants.WHC.InspectionDetail.downloadTechnicianPicture)
        technicianImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "technician_profile_pic"), options: [.requestModifier(modifier)])
        
        nameLabel.text = "\(details.firstName ?? "") \(details.middleName ?? "")\(details.lastName ?? "")"
        experienceLabel.text = Constants.WHC.technicianDetailsCell.serviceExperience +  "\(details.experience ?? "")" + Constants.WHC.technicianDetailsCell.years
        contactNumberLabel.text = details.mobileNumber
    }
    
    func setViewModel1(_ model:TimeLineViewModel.TechnicianDetailCellModel) {
        otpLabel.text = model.startOTP
        self.technicianDetail.text =  model.technicianDetail
  
        if self.technicianImageView.image == nil {
            let url = URL(string: Constants.SchemeVariables.baseDomainOld + "\( model.imageUrl ?? "")")
            if let urlvalue = url {
                if (urlvalue.queryItems.count) > 0 {
                    if let storageRefIds = urlvalue.queryItems["storageRefIds"] {
                        let request = GetClaimDocumentRequestDTO(documentId: nil,storageRefIds: storageRefIds, claimDocType: .thumbnail)
                        GetClaimDocumentRequestUseCase.service(requestDTO: request) {[weak self] (usecase, response, error) in
                            self?.technicianImageView.image = response?.image
                        }
                    }
                }
            }
        }
        nameLabel.text = "\( model.firstName ?? "") \( model.middleName ?? "")\( model.lastName ?? "")"
        self.action =  model.action
        experienceLabel.text = "\( model.experience ?? "")"
        contactNumberLabel.text =  model.mobileNumber
    }
    
    @IBAction func clickedCallCustomerCare(_ sender: Any) {
        if action?.id == Strings.ServiceRequest.ActionKey.call{
            EventTracking.shared.eventTracking(name: .callServiceTapped)
            EventTracking.shared.eventTracking(name: .callButtonForTechnicianTapped)
            EventTracking.shared.eventTracking(name: .SRCallTechnician, [.subcategory: CacheManager.shared.eventProductName ?? "", .service: CacheManager.shared.srType?.rawValue ?? ""])
            Utilities.makeCall(to: contactNumberLabel.text?.replacingOccurrences(of: " ", with: "") ?? "")
        }
    }
}
