//
//  AllOffersCollectionCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 18/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class AllOffersCollectionCell: UICollectionViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imageViewObj: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        self.viewContainer.backgroundColor = .white
        self.labelTitle.textColor = UIColor.darkGray
        labelTitle.font = DLSFont.supportingText.bold
        
        labelTitle.text = Strings.WalletTabScene.viewAllOffers
        imageViewObj.image = #imageLiteral(resourceName: "allOffersArrow")
    }
}
