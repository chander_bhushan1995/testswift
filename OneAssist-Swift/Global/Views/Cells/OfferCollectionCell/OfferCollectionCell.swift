//
//  OfferCollectionCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

struct OfferCollectionCellModel {
    var image: UIImage?
    var title: String?
    var subtitle: String?
    var daysLeft: String?
}

class OfferCollectionCell: UICollectionViewCell {
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewStrip: UIView!
    @IBOutlet weak var labelStrip: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubtitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        self.viewBottom.backgroundColor = UIColor.whiteSmoke
        self.viewStrip.backgroundColor = UIColor.darkOrange
        self.labelStrip.textColor = UIColor.white
        self.labelTitle.textColor = UIColor.darkGray
        self.labelSubtitle.textColor = UIColor.lightGray
        
        labelStrip.font = DLSFont.tags.regular
        labelTitle.font = DLSFont.supportingText.bold
        labelSubtitle.font = DLSFont.tags.regular
    }
    
    func set(model: BankCardOffers) {
        
        self.imageView.setImageWithUrlString(model.image, placeholderImage: #imageLiteral(resourceName: "icon_offer_default"))
        self.labelTitle.text = model.merchantName
        self.labelSubtitle.text = model.offerTitle
        
        if let int = model.offerExpiringInDays?.intValue {
            if int > 0 && int <= 5 {
                if int == 1 {
                    self.labelStrip.text = "\(int) day left"
                } else {
                    self.labelStrip.text = "\(int) days left"
                }
                self.viewStrip.isHidden = false
            } else {
                self.labelStrip.text = nil
                self.viewStrip.isHidden = true
            }
        } else {
            self.viewStrip.isHidden = true
        }
    }
}
