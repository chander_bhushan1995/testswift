//
//  InspectionAssesmentCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class InspectionAssesmentCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var otpLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView() {
        containerView.layer.cornerRadius = 9.0
        containerView.layer.borderColor = UIColor.gray2.cgColor
        containerView.layer.borderWidth = 1.0
        transparentView.layer.cornerRadius = 8.0
//        headerLabel.textColor = UIColor.charcoalGrey
//        headerLabel.font = DLSFont.bodyText.regular
//        otpLabel.font = DLSFont.bodyText.regular
//        otpLabel.textColor = UIColor.charcoalGrey
    }

    func setViewModel(_ model: TimeLineViewModel.InspectionAssesmentCellModel){
        otpLabel.text = model.otp
        headerLabel.text = model.headerText
    }
override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
