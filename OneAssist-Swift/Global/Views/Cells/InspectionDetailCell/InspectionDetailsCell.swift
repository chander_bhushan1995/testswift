//
//  InspectionDetailsCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
protocol InspectionDetailsCellDelegate:class{
    func clickedAddToCalendar(indexPath:IndexPath)
    func clickedCall(indexPath:IndexPath)
}

class InspectionDetailsCell: UITableViewCell, ReuseIdentifier, NibLoadableView {

    @IBOutlet weak var inspectionDetailLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    
    @IBOutlet weak var timeSlotLabel: UILabel!
    @IBOutlet var containerView: UIView!
    
    @IBOutlet weak var allBenefitsView: AllBenefitsView!
    
    @IBOutlet var dateLabel: UILabel!
    
    
    @IBOutlet var timeLabel: UILabel!
    
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet var helplineLabel: UILabel!
   
    @IBOutlet weak var furtherHelpLabel: UILabel!
    @IBOutlet weak var addToCalendarButton: UIButton!
    var indexPath:IndexPath!
    weak var delegate :InspectionDetailsCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView() {
        
        containerView?.layer.cornerRadius = 9.0
        containerView?.layer.borderColor = UIColor.gray2.cgColor
        containerView?.layer.borderWidth = 1.0
        helplineLabel.text = Constants.WHC.InspectionDetailsCell.customerCareNumber
        settingColors()
    }
    
    func settingColors() {
        allBenefitsView.bodyColor = UIColor.forestGreen
    }
    
    func setViewModel(_ details: InspectionDetail) {
        let startDate: Date? = DateFormatter.scheduleFormat.date(from: details.serviceScheduleStarttime)
        let endDate: Date? = DateFormatter.scheduleFormat.date(from: details.serviceScheduleEndtime)
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = gregorianCalendar
        dateFormatter.dateFormat = "dd MMM, EEE"
        let scheduledDate: String = dateFormatter.string(from: startDate ?? Date())
        let scheduledStartTime: String = DateFormatter.time12NotAPperiod.string(from: startDate ?? Date())
        let scheduledEndTime: String = DateFormatter.scheduleTimeFormat.string(from: endDate ?? Date())
        dateLabel.text = scheduledDate
        timeLabel.text = "\(scheduledStartTime)-\(scheduledEndTime)"
        
        allBenefitsView.setContent(items: ["Added to your calendar"], image: #imageLiteral(resourceName: "tick"))
        
        if Utilities.isAddedToCalendar(srId: details.serviceId, scheduleStartDate: details.serviceScheduleStarttime) {
            addToCalendarButton.isHidden = true
        } else {
            addToCalendarButton.isHidden = false
        }
        
        allBenefitsView.isHidden = !addToCalendarButton.isHidden
    }
    
    func setViewModel1(_ model:TimeLineViewModel.InspectionDetailsCellModel, _ delegate:InspectionDetailsCellDelegate?) {
        
        self.delegate = delegate
        self.indexPath = model.indexPath
        
        let startDate: Date? = DateFormatter.scheduleFormat.date(from: model.serviceScheduleStarttime)
        let endDate: Date? = DateFormatter.scheduleFormat.date(from: model.serviceScheduleEndtime)
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = gregorianCalendar
        dateFormatter.dateFormat = "dd MMM, EEE"
        let scheduledDate: String = dateFormatter.string(from: startDate ?? Date())
        let scheduledStartTime: String = DateFormatter.time12NotAPperiod.string(from: startDate ?? Date())
        let scheduledEndTime: String = DateFormatter.scheduleTimeFormat.string(from: endDate ?? Date())
        dateLabel.text = scheduledDate
        timeLabel.text = "\(scheduledStartTime)-\(scheduledEndTime)"
        
        allBenefitsView.setContent(items: ["Added to your calendar"], image: #imageLiteral(resourceName: "tick"))
        
        inspectionDetailLabel.text = model.serviceDetailsText//Strings.WHC.inspection.inspectionDetails.serviceDetails

        addToCalendarButton.isEnabled = model.action1?.enable ?? false
        addToCalendarButton.setTitle(model.action1?.name, for: .normal)
        callButton.isEnabled = model.action2?.enable ?? false
        furtherHelpLabel.text = model.action2?.name
        addToCalendarButton.isEnabled = model.action1?.enable ?? false
        addToCalendarButton.setTitle(model.action1?.name, for: .normal)
        if Utilities.isAddedToCalendar(srId: model.serviceId ?? "", scheduleStartDate: model.serviceScheduleStarttime) {
            addToCalendarButton.isHidden = true
        } else {
            addToCalendarButton.isHidden = false
        }
        allBenefitsView.isHidden = !addToCalendarButton.isHidden
        
        if let actionId = model.action3?.id, actionId == Strings.ServiceRequest.ActionKey.chat { // added for SOD
            helplineLabel.isHidden = true
            callButton.setImage(UIImage(named: "chatWithUs"), for: .normal)
        }else{
           helplineLabel.isHidden = false
        }
    }
    
    @IBAction func clickedBtnAddToCalendar(_ sender: Any) {
        delegate?.clickedAddToCalendar(indexPath: self.indexPath)

    }
   
    @IBAction func clickedBtnCall(_ sender: Any) {
        delegate?.clickedCall(indexPath: self.indexPath)
    }

}
