//
//  InspectionDetailViewModel.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation


enum State : Int {
    case notStarted
    case started
    case completed
}


class InspectionDetailViewModel: NSObject {
    var heading = ""
    var subHeadingWithNoDetails:String?
    var detailState: State  = .notStarted
    var isExpanded = false
}
