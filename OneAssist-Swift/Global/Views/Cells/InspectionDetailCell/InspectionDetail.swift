//
//  InspectionDetail.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 23/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class InspectionDetail {
    var serviceId = ""
    var status = ""
    var assignee = ""
    var serviceScheduleStarttime = ""
    var serviceScheduleEndtime = ""
    var assetId = ""
    var custId = ""
    var inspectionStartTime = ""
    var inspectionEndTime = ""
    var cancelationReason = ""
    
    init(data: SearchService?) {
        serviceScheduleStarttime = data?.scheduleSlotStartDateTime ?? ""
        serviceScheduleEndtime = data?.scheduleSlotEndDateTime ?? ""
        serviceId = data?.serviceRequestId?.description ?? ""
    }
    
    init(data: WHCInspectionResponseModel) {
        serviceId = data.serviceRequestId ?? ""
        status = data.status ?? ""
        assignee = data.assignee ?? ""
        serviceScheduleStarttime = data.scheduleSlotStartDateTime ?? ""
        serviceScheduleEndtime = data.scheduleSlotEndDateTime ?? ""
       // assetId = data.assetId ??  ""
       // custId = data.custId ?? ""
        inspectionStartTime = data.actualStartDateTime ?? ""
        inspectionEndTime = data.actualEndDateTime ?? ""
        if let workflowData = data.workflowData{
            let visit = workflowData.visit
            if visit != nil {
                cancelationReason = visit?["serviceCancelReason"] as? String ?? ""
        }
        }
    }
}
