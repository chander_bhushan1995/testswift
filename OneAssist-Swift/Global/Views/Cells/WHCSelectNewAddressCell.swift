//
//  WHCSelectNewAddressCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 05/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol WHCnewAddressRegistration: class{
    func newAddressButtonTapped()
}

class WHCSelectNewAddressCell: UITableViewCell {
    
    weak var delegate:WHCnewAddressRegistration?
    
    @IBOutlet weak var newAddressButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseButton()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func initialiseButton(){
        newAddressButton.setTitle( Strings.WHC.selectAddress.newAddressButtonTitle, for: .normal)
    }
    
    
    @IBAction func newAddressButtonTapped(_ sender: Any) {
        self.delegate?.newAddressButtonTapped()
    }
    
}
