//
//  CardTypeHeader.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/25/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit
protocol CardTypeHeaderDelegate:class{
    func seeAllTapped()
}
class CardTypeHeader: UIView {

    @IBOutlet weak var seeAllButton: UIButton!
    weak var delegate:CardTypeHeaderDelegate?
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    @IBOutlet weak var rightArrow: UIImageView!
    
    override func awakeFromNib() {
        initializeView()
    }
    
    private func initializeView() {
    }
    @IBAction func seeAllTapped(_ sender: Any) {
        delegate?.seeAllTapped()
    }
}
