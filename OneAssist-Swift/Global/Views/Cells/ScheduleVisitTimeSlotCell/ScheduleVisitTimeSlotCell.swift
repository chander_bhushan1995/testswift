//
//  ScheduleVisitTimeSlotCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class ScheduleVisitTimeSlotViewModel {
    var slot = ""
    var selected = false
    var startTime: String?
    var endTime: String?
}

class ScheduleVisitTimeSlotCell: UICollectionViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var labelSlot: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // labelSlot.font = DLSFont.bodyText.regular
        viewContainer.layer.borderColor = UIColor.buttonBlue.cgColor
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.cornerRadius = 4.0
    }

    func set(viewModel: ScheduleVisitTimeSlotViewModel) {
        labelSlot.text = viewModel.slot
        
        if viewModel.selected {
            viewContainer.backgroundColor = UIColor.buttonBlue
            labelSlot.textColor = UIColor.white
        } else {
            viewContainer.backgroundColor = UIColor.white
            labelSlot.textColor = UIColor.buttonBlue
        }
    }
}
