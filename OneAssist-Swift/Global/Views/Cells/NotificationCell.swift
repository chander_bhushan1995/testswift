//
//  NotificationCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var titleLabel: H3BoldLabel!
    
    @IBOutlet weak var descLabel: H3RegularBlackLabel!
    @IBOutlet weak var dateLabel: H3RegularGreyLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initialiseView(_ model:NotificationsCD)
    {
        self.titleLabel.text = model.title
        self.descLabel.text = model.message
        self.dateLabel.text = model.date
        self.setUpUI()
    
    }
    
    func setUpUI()
    {
        self.titleLabel.isEnabled = true
        self.dateLabel.isEnabled = true
    }
    
    func setUpForSelected()
    {
        self.titleLabel.isEnabled = false
        self.dateLabel.isEnabled = false
        self.descLabel.isEnabled = false
    }

}
