//
//  ScheduleVisitCollectionCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 27/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

enum ScheduleVisitCollectionState {
    case normal
    case selected
    case disabled
}

class ScheduleVisitCollectionViewModel {
    var day = ""
    var dateText = ""
    var isToday = false
    var state: ScheduleVisitCollectionState = .normal
    var date = Date.currentDate()
}

class ScheduleVisitCollectionCell: UICollectionViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelToday: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContainer.layer.cornerRadius = 3
        // labelDay.font = UIFont(name: "Lato-Semibold", size: 13)
        // labelDate.font = UIFont(name: "Lato-Semibold", size: 20)
        // labelToday.font = UIFont(name: "Lato-Bold", size: 9)
        labelToday.text = Strings.ScheduleVisitScene.today
        labelToday.textColor = UIColor.buttonBlue
    }
    
    func set(viewModel: ScheduleVisitCollectionViewModel) {
        labelDay.text = viewModel.day
        labelDate.text = viewModel.dateText
        labelToday.isHidden = !viewModel.isToday
        
        switch viewModel.state {
        case .disabled:
            viewContainer.backgroundColor = UIColor.white
            labelDay.textColor = UIColor.lightGray
            labelDate.textColor = UIColor.lightGray
            isUserInteractionEnabled = false
        case .normal:
            viewContainer.backgroundColor = UIColor.white
            labelDay.textColor = UIColor.gray
            labelDate.textColor = UIColor.black
            isUserInteractionEnabled = true
        case .selected:
            viewContainer.backgroundColor = UIColor.dodgerBlue
            labelDay.textColor = UIColor.white
            labelDate.textColor = UIColor.white
            isUserInteractionEnabled = true
        }
    }
}
