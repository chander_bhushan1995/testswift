//
//  HomeApplianceCollectionCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 16/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class HomeApplianceCollectionCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var curvedView: CurvedView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(viewModel: HomeApplianceSubCategories) {
        imageView.setImageWithUrlString(viewModel.subCatImageUrl)
        labelTitle.text = viewModel.subCategoryName
    }
}
