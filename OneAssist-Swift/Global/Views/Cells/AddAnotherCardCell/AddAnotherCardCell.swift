//
//  AddAnotherCardCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 12/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

struct AddAnotherCardCellModel {
    
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var buttonTitle: String?
    var margin: CGFloat = 0
    var errorForCard: String?
    
    init(errorForCard: String?) {
        
        self.title = Strings.WalletTabScene.offersOnAnotherCardHeading
        self.subtitle = Strings.WalletTabScene.offersOnAnotherCardSubHeading
        self.image = #imageLiteral(resourceName: "CardOffers")
        self.buttonTitle = Strings.WalletTabScene.offersOnAnotherCardAction
        self.margin = 10
        self.errorForCard = errorForCard
    }
}

protocol AddAnotherCardCellDelegate: class {
    func addAnotherCardCell(_ cell: AddAnotherCardCell, clickedBtnAddAnother sender: Any)
}

class AddAnotherCardCell: UITableViewCell {
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var baseLayoutView: BaseLayoutView!
    @IBOutlet weak var imageSubtitleView: ImageSubtitleView!
    @IBOutlet weak var buttonGiftPlan: UIButton!
    
    weak var delegate: AddAnotherCardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        layoutIfNeeded()
    }
    
    func set(model: AddAnotherCardCellModel) {
        imageSubtitleView.imageView.image = model.image
        imageSubtitleView.titleLabel.text = model.title
        imageSubtitleView.subtitleLabel.text = model.subtitle
        buttonGiftPlan.setTitle(model.buttonTitle, for: .normal)
        
        baseLayoutView.horizontalMargins = model.margin
        
        errorView.backgroundColor = UIColor.paleWhite
        errorLabel.font = DLSFont.bodyText.regular
        errorLabel.textColor = UIColor.lightGray
        errorLabel.text = String(format: Strings.WalletTabScene.errorMessageOnAnotherCard, model.errorForCard ?? "cards")
    }
    
    @IBAction func clickedBtnGiftAPlan(_ sender: Any) {
        delegate?.addAnotherCardCell(self, clickedBtnAddAnother: sender)
    }
    
}
