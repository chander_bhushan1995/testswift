//
//  FeedbackCell.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 29/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class FeedbackCell: UITableViewCell {
    
    @IBOutlet var radioButton: RadioButton!
    
    @IBOutlet var label: UILabel!
    
    @IBOutlet var textView: TextFieldView!
    
    @IBOutlet var textFieldHeightConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func radioButtonClicked(_ sender: Any) {
        
    }

}
