//
//  TransparentAdCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 14/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

enum TransparentAdCellModelType {
    case moreCards
}

struct TransparentAdCellModel {
    
    var title: String?
    var subtitle: String?
    var buttonTitle: String?
    
    init(type: TransparentAdCellModelType? = nil) {
        
        guard type != nil else {
            return
        }
        
        switch type! {
        case .moreCards:
            self.title = Strings.WalletTabScene.moreCardsHeading
            self.subtitle = Strings.WalletTabScene.moreCardsSubHeading
            self.buttonTitle = Strings.WalletTabScene.moreCardsAction
        }
    }
}

protocol TransparentAdCellDelegate: class {
    func clickedClose(sender: UIButton)
    func clickedPrimaryAction(sender: UIButton)
}

class TransparentAdCell: UITableViewCell {

    weak var delegate: TransparentAdCellDelegate?
    
    @IBOutlet weak var curvedView: TransparentCurvedView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var buttonClose: UIButton!
    
    @IBOutlet weak var seperatorView: SeperatorView!
    @IBOutlet weak var buttonAction: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        labelTitle.font = DLSFont.h3.bold
        labelTitle.textColor = UIColor.darkGray
        
        labelSubtitle.font = DLSFont.bodyText.regular
        labelSubtitle.textColor = UIColor.darkGray
        
        buttonClose.setImage(#imageLiteral(resourceName: "cross"), for: .normal)
        
        self.seperatorView.backgroundColor = UIColor.dodgerBlue
    }
    
    func set(model: TransparentAdCellModel) {
        self.labelTitle.text = model.title
        self.labelSubtitle.text = model.subtitle
        self.buttonAction.setTitle(model.buttonTitle, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedPrimaryAction(_ sender: Any) {
        delegate?.clickedPrimaryAction(sender: sender as! UIButton)
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        delegate?.clickedClose(sender: sender as! UIButton)
    }
}
