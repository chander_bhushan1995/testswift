//
//  AllATMsCell.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 27/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class AllATMsCell: UITableViewCell {

    @IBOutlet weak var issuerNameLabel: UILabel!
    @IBOutlet weak var selectedCellDotView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureView() {
        self.selectedCellDotView.clipsToBounds = true
        self.selectedCellDotView.layer.cornerRadius = self.selectedCellDotView.frame.size.width/2.0
    }

    class func cellHeight() -> CGFloat {
        return 60.0
    }
}
