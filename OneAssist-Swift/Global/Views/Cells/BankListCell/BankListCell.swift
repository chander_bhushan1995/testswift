//
//  BankListCell.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 26/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class BankListCell: UITableViewCell {

    @IBOutlet weak var topSeperator: SeperatorView!
    @IBOutlet weak var issuerLogoImageView: UIImageView!
    @IBOutlet weak var issuerNameLabel: UILabel!
    @IBOutlet weak var selectedCellDotView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureView() {
        self.selectedCellDotView.clipsToBounds = true
        self.selectedCellDotView.layer.cornerRadius = self.selectedCellDotView.frame.size.width/2.0
        topSeperator.isHidden = true
    }

    class func cellHeight() -> CGFloat {
        return 64.0
    }
}
