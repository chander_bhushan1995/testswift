
//
//  OfferListCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol OfferListCellDelegate: class {
    func clickedBookmark(for indexPath: IndexPath?)
}

class OfferListCell: UITableViewCell {

    weak var delegate: OfferListCellDelegate?
    
    @IBOutlet weak var imageViewBrandName: UIImageView!
    @IBOutlet weak var labelBrandName: BodyTextRegularBlackLabel!
    @IBOutlet weak var labelOffer: SupportingTextRegularGreyLabel!
    @IBOutlet weak var imageViewExpiry: UIImageView!
    @IBOutlet weak var labelExpiry: SupportingTextRegularGreyLabel!
    @IBOutlet weak var buttonBookmark: UIButton!
    
    @IBOutlet weak var curvedView: DLSCurvedView!
    @IBOutlet weak var bankTagLabel: TagsBoldGreyLabel!
    @IBOutlet weak var moreBankTagLabel: SupportingTextRegularGreyLabel!
    @IBOutlet weak var curvedViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var curvedViewHeight: NSLayoutConstraint!
    
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        curvedView.layer.cornerRadius = 2.0
        curvedView.layer.borderWidth = 1.0
        curvedView.layer.borderColor = UIColor.buttonBlue.cgColor
        bankTagLabel.textColor = UIColor.buttonBlue
        
        initializeView()
    }
    
    func initializeView() {
        buttonBookmark.setImage(#imageLiteral(resourceName: "bookmark_select_gray"), for: .normal)
        buttonBookmark.setImage(#imageLiteral(resourceName: "bookmark_select_blue"), for: .selected)
        
        let image = #imageLiteral(resourceName: "clock").withRenderingMode(.alwaysTemplate)
        self.imageViewExpiry.image = image
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(model: BankCardOffers?, for indexPath: IndexPath? = nil) {
        
        guard let model = model else {
            return
        }
        
        self.indexPath = indexPath
        self.imageViewBrandName.setImageWithUrlString(model.image, placeholderImage: #imageLiteral(resourceName: "icon_offer_default"))
        
        self.labelBrandName.text = model.merchantName
        self.labelOffer.text = model.offerTitle
        
        
        self.labelExpiry.text = Utilities.getOfferListDateText(dateString: model.endDate, expiryInDays : model.offerExpiringInDays)
        
        let color = Utilities.isOfferEndingSoon(dateString: model.endDate, expiryInDays : model.offerExpiringInDays) ? UIColor.errorTextFieldBorder : UIColor.bodyTextGray
        self.labelExpiry.textColor = color
        self.imageViewExpiry.tintColor = color
        
        self.buttonBookmark.isSelected = model.isBookmarked
        
        var tag: String?
        var tagCount: Int = 0
        
        if let cardDetailList = model.cardDetailsList , cardDetailList.count > 0 {
            let cardDetail = cardDetailList.first
            if let tagName = cardDetail?.tagging {
                tag = tagName
            }else if let tagName = cardDetail?.cardIssuerName {
                tag = tagName
            }
            tagCount = cardDetailList.count - 1
        }
        moreBankTagLabel.text = nil
        
        if let tagText = tag {
            curvedView.isHidden = false
            curvedViewTopConstraint.constant = 7.0
            curvedViewHeight.constant = 20.0
            bankTagLabel.text = tagText
            if tagCount > 0 {
                moreBankTagLabel.text = "+" + "\(tagCount)" + "more"
            }
            
        }else {
            curvedView.isHidden = true
            curvedViewTopConstraint.constant = 0.0
            curvedViewHeight.constant = 0.0
            bankTagLabel.text = nil
        }
    }
    
    @IBAction func clickedBtnBookmark(_ sender: UIButton) {
        buttonBookmark.isSelected = !buttonBookmark.isSelected
        delegate?.clickedBookmark(for: self.indexPath)
    }
}

