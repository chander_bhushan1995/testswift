//
//  OfferLoadMoreCell.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 15/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class OfferLoadMoreCell: UITableViewCell {
    @IBOutlet weak var actIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
