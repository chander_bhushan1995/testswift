//
//  OfferHeaderView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 15/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

class OfferHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: SupportingTextBoldBlackLabel!
    
    override func awakeFromNib() {
         super.awakeFromNib()
        titleLabel.textColor = UIColor.bodyTextGray
    }

}
