//
//  MHCCell.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/3/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

struct MHCCellModel {
    
}

class MHCCell: UITableViewCell {

    @IBOutlet weak var imageSubtitleView: ImageSubtitleView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    private func initializeView() {
        imageSubtitleView.imageView.image = #imageLiteral(resourceName: "mhc")
        imageSubtitleView.titleLabel.text = Strings.MobileTabScene.mhc
        //imageSubtitleView.subtitleLabel.text = Strings.MobileTabScene.mhcDescription

        let mhcHomeObj = MHC_HomeVC()
        let dict = mhcHomeObj.readPlistFile()
        let testBundleArrayData : [Any] = dict!["App_Configuration_Test"] as! [Any]

        if let mhcStatus = UserDefaults.standard.value(forKey: "MHC_RESULT") as? String {
            let imageSubtitleViewFont = imageSubtitleView.subtitleLabel.font
            let mhcDescFont = UIFont.init(name: imageSubtitleViewFont!.familyName, size: imageSubtitleViewFont!.pointSize + 1.0)!

            let mhcMutableAttrString = NSMutableAttributedString()
            let mhcDescAttrString = NSMutableAttributedString(string: Strings.MobileTabScene.mhcResultDescription)
            let mhcStatusAttrString = NSMutableAttributedString(string: mhcStatus, attributes: [NSAttributedString.Key.foregroundColor : UIColor.mhcStatusTextColor, NSAttributedString.Key.font : mhcDescFont])
            mhcMutableAttrString.append(mhcDescAttrString)
            mhcMutableAttrString.append(mhcStatusAttrString)
            imageSubtitleView.subtitleLabel.attributedText = mhcMutableAttrString

        } else {
            imageSubtitleView.subtitleLabel.text =  String(format : Strings.MobileTabScene.mhcDescription, String(describing : testBundleArrayData.count))
        }

        layoutIfNeeded()
    }
    
}
