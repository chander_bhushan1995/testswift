//
//  OfferFilterCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 08/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class OfferFilterCell: UICollectionViewCell {

    @IBOutlet weak var viewCurved: CurvedView!
    @IBOutlet weak var imageViewObj: UIImageView!
    @IBOutlet weak var labelFilterName: SupportingTextRegularBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(viewModel: FilterViewModel) {
        
        labelFilterName.text = viewModel.text
        imageViewObj.image = viewModel.image
        
        let borderColor = viewModel.selected ? UIColor.buttonTitleBlue : UIColor.seperatorView
        let textColor = viewModel.selected ? UIColor.buttonTitleBlue : UIColor.charcoalGrey
        viewCurved.layer.borderColor = borderColor.cgColor
        labelFilterName.textColor = textColor
    }
}
