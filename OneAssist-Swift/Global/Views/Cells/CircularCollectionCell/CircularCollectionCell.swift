//
//  CircularCollectionCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 21/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

struct CircularCollectionViewModel {
    var selected: Bool
    var title: String
    var value: Int
    var eventName: Event.Events
}

class CircularCollectionCell: UICollectionViewCell {

    @IBOutlet weak var viewCircular: UIView!
    @IBOutlet weak var labelTitle: SupportingTextRegularBlackLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeView()
    }
    
    func initializeView() {
        self.viewCircular.layer.cornerRadius = 15
        self.viewCircular.layer.borderColor = UIColor.buttonTitleBlue.cgColor
        self.viewCircular.layer.borderWidth = 1.0
    }
    
    func set(model: CircularCollectionViewModel) {
        self.labelTitle.text = model.title
        self.viewCircular.backgroundColor = model.selected ? .buttonTitleBlue : .white
        self.labelTitle.textColor = model.selected ? .white : UIColor.charcoalGrey
    }
}
