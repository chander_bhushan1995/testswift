//
//  ImageTitleButtonCell.swift
//  OneAssist-Swift
//
//  Created by Varun on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

enum TitleModelType {
    case atm
    case bank
    case bookmarked
}

struct ImageTitleButtonCellModel {
    
    var title: String?
    var subTitle: String?
    var image: UIImage?
    var type: TitleModelType?
    
    init(type: TitleModelType? = nil) {
        
        self.type = type
        guard type != nil else {
            return
        }
        
        switch type! {
        case .atm:
            self.title = Strings.WalletTabScene.findATM
            self.image = #imageLiteral(resourceName: "atmWalletTabCard")
        case .bank:
            self.title = Strings.WalletTabScene.findBank
            self.image = #imageLiteral(resourceName: "bankWalletTabCard")
        case .bookmarked:
            self.title = Strings.WalletTabScene.bookmarkedOffers
            self.image = #imageLiteral(resourceName: "bookmarkedOffersWalletTabCard")
        }
        
    }
}

class ImageTitleButtonCell: UITableViewCell {

    @IBOutlet weak var imageTitleView: ImageTitleView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(model: ImageTitleButtonCellModel) {
        imageTitleView.imageViewObj.image = model.image
        imageTitleView.labelTitle.text = model.title
        layoutIfNeeded()
    }
}
