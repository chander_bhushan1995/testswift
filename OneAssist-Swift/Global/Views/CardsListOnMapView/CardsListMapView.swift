//
//  CardsListMapView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 22/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@objc protocol CardRowTappedDelegate {
    @objc optional func issuerSelected(issuer : WalletCard?)
}

class CardsListMapView: BaseNibView {

    @IBOutlet weak var cardsTableView : UITableView!
    @IBOutlet weak var constraintAddCardViewHeight : NSLayoutConstraint!

    var issuerList : [WalletCard]?
    var selectedRow : Int? = 0
    let maxRowsToShow : Int = 5
    
    weak var delegate : CardRowTappedDelegate?
    weak var carddelegate: AddCardButtonDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }

    func configureView() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 10.0
        cardsTableView.register(nib: Constants.NIBNames.allATMsCell, forCellReuseIdentifier: Constants.CellIdentifiers.allATMsCell)
        cardsTableView.register(nib: Constants.NIBNames.bankListCell, forCellReuseIdentifier: Constants.CellIdentifiers.bankListCell)
        cardsTableView.separatorStyle = .none
        cardsTableView.dataSource = self
        cardsTableView.delegate = self
    }

    func selectTableRow(row : Int) {
        selectedRow = row
        let indexPath : IndexPath = IndexPath(row: row, section: 0)
        cardsTableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
    }
    
    @IBAction func addCardButtonTapped(_ sender: Any) {
      self.carddelegate?.addCardButtonTapped()
    }
}

extension CardsListMapView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let issuerList = issuerList {
            return issuerList.count + 1
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.allATMsCell, for: indexPath) as! AllATMsCell
            //cell.constraintIssuerLogoImageViewWidth.constant = 0
            cell.issuerNameLabel.text = "All ATMs nearby"
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if selectedRow != nil && indexPath.row == selectedRow! {
                cell.selectedCellDotView.isHidden = false
            } else {
                cell.selectedCellDotView.isHidden = true
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.bankListCell, for: indexPath) as! BankListCell
            cell.issuerNameLabel.text = issuerList?[indexPath.row - 1].issuerName
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if selectedRow != nil && indexPath.row == selectedRow! {
                cell.selectedCellDotView.isHidden = false
            } else {
                cell.selectedCellDotView.isHidden = true
            }
            return cell
        }
    }
}

extension CardsListMapView : UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return BankListCell.cellHeight()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if selectedRow! == indexPath.row {
            return
        }

        selectedRow = indexPath.row
        if indexPath.row == 0 {
            let cell = cardsTableView.cellForRow(at: indexPath) as! AllATMsCell
            cell.selectedCellDotView.isHidden = false
            delegate?.issuerSelected?(issuer : nil)
        } else {
            let cell = cardsTableView.cellForRow(at: indexPath) as! BankListCell
            cell.selectedCellDotView.isHidden = false
            delegate?.issuerSelected?(issuer : issuerList?[indexPath.row - 1])
        }
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let cell = cardsTableView.cellForRow(at: indexPath) as? AllATMsCell
            cell?.selectedCellDotView.isHidden = true
        } else {
            let cell = cardsTableView.cellForRow(at: indexPath) as? BankListCell
            cell?.selectedCellDotView.isHidden = true
        }
    }
}
