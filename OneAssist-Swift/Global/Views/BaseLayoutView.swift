//
//  BaseLayoutView.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@IBDesignable
class BaseLayoutView: UIView {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initializeView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initializeView()
	}
    
    private func initializeView() {
        backgroundColor = .clear
        setMargins()
    }
	
	func setMargins() {
		layoutMargins = UIEdgeInsets(top: verticalMargins, left: horizontalMargins, bottom: verticalMargins, right: horizontalMargins)
	}
	
	@IBInspectable var horizontalMargins: CGFloat = 15 {
		didSet {
			setMargins()
		}
	}
    
    @IBInspectable var verticalMargins: CGFloat = 15 {
        didSet {
            setMargins()
        }
    }
}


class EqualMarginedView: BaseLayoutView {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
        
        horizontalMargins = 10
        verticalMargins = 10
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
        
        horizontalMargins = 10
        verticalMargins = 10
	}
}
