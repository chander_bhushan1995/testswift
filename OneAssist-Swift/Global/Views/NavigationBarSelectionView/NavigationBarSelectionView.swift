//
//  NavigationBarSelectionView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 22/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

@objc protocol NavigationBarActionDelegate {
    @objc optional func navigationBarActionButtonTapped()
}

class NavigationBarSelectionView : UIView {

    weak var delegate : NavigationBarActionDelegate?

    @IBOutlet weak var navigationBarTitleLabel : SupportingTextRegularGreyLabel!
    @IBOutlet weak var navigationBarActionButton : UIButton!
    
    class func instanceFromNib() -> NavigationBarSelectionView {
        return UINib(nibName: "NavigationBarSelectionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavigationBarSelectionView
    }

    override var intrinsicContentSize: CGSize {
        return CGSize.init(width: 200.0, height: 44.0)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateUI(title: String, subtitle: String, isButtonEnable: Bool) {
        navigationBarActionButton.setAttributedTitle(nil, for: .normal)
        navigationBarActionButton.setTitle(title + " ", for: .normal)
        navigationBarTitleLabel.text = subtitle
        setupButton(isButtonEnable)
       
    }
    
    func updateUI(title: NSAttributedString, subtitle: String, isButtonEnable: Bool) {
        navigationBarActionButton.setTitle(nil, for: .normal)
        navigationBarActionButton.setAttributedTitle(title, for: .normal)
        navigationBarTitleLabel.text = subtitle
        setupButton(isButtonEnable)
    }
    
    
    func setupButton(_ isButtonEnable: Bool) {
        navigationBarActionButton.setImage(nil, for: .normal)
        navigationBarActionButton.semanticContentAttribute = .forceRightToLeft
       if isButtonEnable {
           navigationBarActionButton.setImage(UIImage(named: "downArrowBlue"), for: .normal)
           navigationBarActionButton.isUserInteractionEnabled = true
           
       }else {
           navigationBarActionButton.isUserInteractionEnabled = false
           
       }
    }

    @IBAction func navigationBarButtonAction(_ sender: Any) {
        delegate?.navigationBarActionButtonTapped?()
    }
}
