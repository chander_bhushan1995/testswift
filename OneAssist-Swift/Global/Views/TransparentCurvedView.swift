//
//  TransparentCurvedView.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class TransparentCurvedView: CurvedView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        self.backgroundColor = UIColor.dodgerBlue.withAlphaComponent(0.1)
        self.layer.borderColor = UIColor.dodgerBlue.cgColor
    }
}
