//
//  SeperatorView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/24/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class SeperatorView: UIView {
	
    /** 
    * true: if your seperator is vertical, it's width will be 1.
    * false: if your seperator is horizontal, it's height will be 1
    * For custom width and height use reverse order and set constraints from Interface Builder. */
    @IBInspectable var isVertical: Bool = false {
        didSet {
            initializeView()
        }
    }
    
	override init(frame: CGRect) {
		super.init(frame: frame)
		initializeView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initializeView()
	}
	
	private func initializeView() {
		backgroundColor = UIColor.seperatorView
		
		for constraint in constraints {
			if !isVertical && constraint.firstAttribute == .height {
				constraint.constant = 1
            } else if isVertical && constraint.firstAttribute == .width {
                constraint.constant = 1
            }
		}
	}
}
