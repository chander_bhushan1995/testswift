//
//  BaseLayoutView2.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 21/03/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

@IBDesignable
class BaseLayoutView2: BaseLayoutView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }

    private func initializeView() {
        backgroundColor = .clear
        setMargins()
    }

    override func setMargins() {
        layoutMargins = UIEdgeInsets(top: 0, left: 11, bottom: 10, right: 11)
    }
}
