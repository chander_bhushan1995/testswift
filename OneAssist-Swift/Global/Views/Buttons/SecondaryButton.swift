//
//  SecondaryButton.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class SecondaryButton: UIButton {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initializeView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initializeView()
	}
	
	private func initializeView() {
		
		setTitleColor(UIColor.dodgerBlue, for: .normal)
		titleLabel?.font = DLSFont.bodyText.regular
	}
}
