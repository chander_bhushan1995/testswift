//
//  ArrowButton.swift
//  OneAssist-Swift
//
//  Created by OneAssist on 1/8/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class ArrowButton: PrimaryButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        initializeArrowImage()
    }
}

extension PrimaryButton {
    
    func initializeArrowImage() {
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        setImage(#imageLiteral(resourceName: "buttonArrow"), for: .normal)
        setImage(#imageLiteral(resourceName: "buttonArrow_disable"), for: .disabled)
        setImage(#imageLiteral(resourceName: "lightArrow"), for: .highlighted)
        transform = CGAffineTransform(scaleX: -1, y: 1.0);
        titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
        imageView?.transform = CGAffineTransform(scaleX: -1, y: 1.0)
    }
}
