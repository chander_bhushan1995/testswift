//
//  PrimaryButton.swift
//  OneAssist-Swift
//
//  Created by Varun on 20/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@IBDesignable
class PrimaryButton: UIButton {
    var type:Any!
//	var fields: [TextFieldView]?
//	
//	var controlEnabled: Bool {
//		set {
//			isEnabled = newValue
//		}
//		get {
//			return isEnabled
//		}
//	}
//	
//	func validateControl() {
//		guard fields != nil else {
//			return
//		}
//		
//		for field in fields! {
//			if field.isInvalid {
//				controlEnabled = false
//				return
//			}
//		}
//		
//		controlEnabled = true
//	}
	
	override var isEnabled: Bool {
		didSet {
			if isEnabled {
				backgroundColor = UIColor.dodgerBlue
			}
			else {
				backgroundColor = UIColor.gray2
			}
		}
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initializeView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initializeView()
	}
	
	private func initializeView() {
		
		backgroundColor = UIColor.dodgerBlue
		setTitleColor(.white, for: .normal)
        setTitleColor(.lightGray, for: .disabled)
        
        setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .highlighted)
		layer.cornerRadius = 22.0
		titleLabel?.font = DLSFont.h3.bold
		
		for constraint in constraints {
			if constraint.firstAttribute == .height {
				constraint.constant = 44
			}
		}
	}
}
