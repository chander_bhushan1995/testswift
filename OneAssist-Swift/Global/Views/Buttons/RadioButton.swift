//
//  RadioButton.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 01/12/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class RadioButton: UIButton {

    var selectedState : Bool = false {
        didSet {
            if selectedState == false {
                self.setImage(#imageLiteral(resourceName: "radioButtonInactive"), for: .normal)
            } else {
                self.setImage(#imageLiteral(resourceName: "radioButtonActive"), for: .normal)
            }
        }
    }
}
