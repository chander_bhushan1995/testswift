//
//  BadgeCount.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 26/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BadgeCount: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        self.layer.cornerRadius = self.frame.height/2
        self.backgroundColor = UIColor(red:1, green:0.67, blue:0, alpha:1)
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.12).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 4
    }

}
//extension BadgeCount {
//   convenience init(size:CGSize){
//    self.init()
//
//    for constraint in constraints {
//        if constraint.firstAttribute == .height {
//            constraint.constant = size.height
//            self.layer.cornerRadius = self.frame.height/2
//        }
//        if constraint.firstAttribute == .width {
//            constraint.constant = size.width
//        }
//    }
//
//    }
//}
