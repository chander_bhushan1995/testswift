//
//  BaseNibViewAutoLayout.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 08/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BaseNibViewAutoLayout: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    func customInit() {
        
        let ownerType = type(of: self)
        let className = String(describing: ownerType)
        let view = Bundle(for: ownerType).loadNibNamed(className, owner: self, options: nil)?.first as! UIView
        addSubview(view)
        // view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.topAnchor),
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            ])
    }

}
