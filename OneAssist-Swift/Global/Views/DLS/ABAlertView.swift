//
//  ABAlertView.swift
//  ABAlertDemo
//
//  Created by Ankur Batham on 31/03/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import UIKit

let kContainerWidth: CGFloat = 300.0
let kCornerRadius: CGFloat   = 3.0
let kShadowOpacity: Float    = 0.15
let kButtonHeight: CGFloat   = 45.0
private let kAlertViewTag   = 12345
private let kMaxLogoDimentionSmall: CGFloat = 50.0
private let kMaxLogoDimentionLarge: CGFloat = 180.0

class ABAlertView: UIView {
    
    private lazy var containerView = UIView()
    private lazy var shadowView = UIView()
    private lazy var titleLabel = UILabel()
    private lazy var messageLabel = UILabel()
    private lazy var logoImageView = UIImageView()
    
    private var buttons = [ABAlertButton]()
    private var isLargeLogo = false
    private var maxLogoDimension: CGFloat { return isLargeLogo ? kMaxLogoDimentionLarge : kMaxLogoDimentionSmall }
    
    // Fonts
    public var titleFont: UIFont?
    public var messageFont: UIFont?
    var closure: (() -> Void)?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public init(title: String?, message: String?, image: UIImage?, isLargeLogo: Bool = false, action: (() -> Void)?) {
        super.init(frame: CGRect.zero)
        
        titleLabel.text = title
        messageLabel.text = message
        logoImageView.image = image
        closure = action
        self.isLargeLogo = isLargeLogo
        
        self.frame            = UIScreen.main.bounds
        self.backgroundColor  = UIColor.black.withAlphaComponent(0.7)
        containerView.layer.cornerRadius = kCornerRadius
        containerView.layer.masksToBounds      = true
        
        shadowView.layer.shadowColor   = UIColor.black.cgColor
        shadowView.layer.shadowOffset  = CGSize.zero
        shadowView.layer.shadowOpacity = kShadowOpacity
        shadowView.layer.shadowRadius  = kCornerRadius
    }
    
    
    // Add a button with a title, a background color, a font color and an action
    public func addButton(title:String, backgroundColor: UIColor, fontColor: UIColor, action:@escaping ()->Void) {
        let btn    = ABAlertButton(title: title, backgroundColor: backgroundColor, fontColor: fontColor)
        btn.action = action
        btn.addTarget(self, action:#selector(ABAlertView.buttonClicked(btn:)), for:.touchUpInside)
        buttons.append(btn)
    }
    
    
    public func show() {
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        if let windowView = UIApplication.shared.keyWindow {
            if windowView.viewWithTag(kAlertViewTag) == nil {
                self.tag = kAlertViewTag
                windowView.addSubview(self)
            }
        } else {
            return
        }
        
        var contentHeight:CGFloat = 32.0
        let xMargin: CGFloat = 16.0
        let labelWidth = kContainerWidth-(xMargin*2.0)
        
        // Dismiss Button
        if closure != nil {
            let dismissBtn = UIButton(type: .custom)
            dismissBtn.frame = CGRect(x: kContainerWidth-50, y: 0.0, width: 50, height: 50)
            dismissBtn.setImage(#imageLiteral(resourceName: "cross_icon"), for: .normal)
            dismissBtn.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.right;
            dismissBtn.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top;
            dismissBtn.imageEdgeInsets = UIEdgeInsets(top: 10, left:10, bottom: 10, right: 10)
            dismissBtn.setTitleColor(UIColor.gray, for: .normal)
            dismissBtn.addTarget(self, action:#selector(ABAlertView.dismissClicked(_:)), for:.touchUpInside)
            containerView.addSubview(dismissBtn)
            
            contentHeight = 48.0
        }
        
        // ImageView Logo
        if let logoImage = logoImageView.image {
            let width = min(logoImage.size.width, maxLogoDimension)
            let height = min(logoImage.size.height, maxLogoDimension)
            
            logoImageView.frame = CGRect(x:  kContainerWidth/2 - (width/2.0), y: contentHeight+5, width: width, height: height)
            containerView.addSubview(logoImageView)
            
            contentHeight = getBottomPos(view: logoImageView) + 32
            
        }
        
        // Title
        if let text = titleLabel.text, text.count > 0 {
            
            // set property
            titleLabel.numberOfLines = 0
            titleLabel.textColor = UIColor.charcoalGrey
            titleLabel.textAlignment = NSTextAlignment.center
            titleLabel.font = (titleFont != nil) ? titleFont : UIFont.boldSystemFont(ofSize: 16)
            
            // set frame
            let titleSize    = titleLabel.sizeThatFits(CGSize(width: labelWidth, height: CGFloat(MAXFLOAT)))
            titleLabel.frame = CGRect(x: xMargin, y: contentHeight, width: labelWidth, height: titleSize.height)
            containerView.addSubview(titleLabel)
            
            contentHeight = getBottomPos(view: titleLabel) + 10
        }
        
        if let text = messageLabel.text, text.count > 0 {
            messageLabel.numberOfLines = 0
            messageLabel.textColor = UIColor.bodyTextGray
            messageLabel.font = (messageFont != nil) ? messageFont : UIFont.systemFont(ofSize: 16)
            messageLabel.textAlignment = NSTextAlignment.center
            
            let messageSize    = messageLabel.sizeThatFits(CGSize(width: labelWidth, height: CGFloat(MAXFLOAT)))
            messageLabel.frame = CGRect(x: xMargin, y: contentHeight, width: labelWidth, height: messageSize.height)
            containerView.addSubview(messageLabel)
            
            contentHeight = getBottomPos(view: messageLabel) + 16
            
            //Margin between message and first button
            contentHeight = contentHeight + 16
        }
        
        // Button
        let buttonSpacing:CGFloat = 10.0
        let buttonWidth = kContainerWidth-(xMargin*2.0)
        
        for i in 0 ..< buttons.count {
            let btn   = buttons[i]
            let y = (i == 0) ? contentHeight: contentHeight + buttonSpacing
            btn.frame = CGRect(x: xMargin, y: y, width: buttonWidth, height: kButtonHeight)
            contentHeight = getBottomPos(view: btn)
            
            containerView.addSubview(btn)
        }
        
        //Bottom Margin
        contentHeight = contentHeight + 16
        
        
        // Shadow View & Container View
        shadowView.frame     = CGRect(x: 0, y: 0, width: kContainerWidth, height: contentHeight)
        shadowView.center    = self.center
        containerView.frame  = CGRect(origin: CGPoint.zero, size: shadowView.frame.size)
        containerView.backgroundColor = UIColor.white
        shadowView.addSubview(containerView)
        self.addSubview(shadowView)
        
        // Apply a fade-in animation
        self.alpha     = 0.0
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.alpha = 1.0
            
        }) { (finished) in
            
        }
    }
    
    private func dismiss(completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: 0.18, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.alpha = 0.0
            
        }) { (finished) in
            self.removeFromSuperview()
            if completion != nil {
                completion!(true)
            }
        }
    }
    
    @objc func dismissClicked(_ sender: UIButton?) {
        dismiss { (status) in
            if self.closure != nil {
                self.closure!()
            }
        }
    }
    
    @objc func buttonClicked(btn:ABAlertButton) {
        dismiss { (status) in
            btn.action()
        }
    }
    
    
    private func getBottomPos(view: UIView) -> CGFloat {
        return view.frame.origin.y + view.frame.height
    }
}


internal final class ABAlertButton: UIButton {
    
    fileprivate var target:AnyObject!
    fileprivate var action:(()->Void)!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(title: String) {
        self.init(title: title, backgroundColor: nil, fontColor: nil)
    }
    
    init(title: String, backgroundColor: UIColor?, fontColor: UIColor?) {
        
        super.init(frame: CGRect.zero)
        self.backgroundColor = (backgroundColor != nil) ? backgroundColor! : self.tintColor
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = DLSFont.h3.regular
        self.setTitleColor((fontColor != nil) ? fontColor! : UIColor.white , for: .normal)
        self.setupCommonProperties()
    }
    
}
