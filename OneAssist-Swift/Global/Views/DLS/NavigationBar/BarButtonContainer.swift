//
//  BarButtonContainer.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 27/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BarButtonContainer: BaseNibView {

    @IBOutlet weak var badgeCount: BadgeCount!{
        didSet{
            badgeCount.isHidden = true
        }
    }
    @IBOutlet weak var badgeCountLabel: UILabel!
    @IBOutlet weak var barIconView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var actionBtn: UIButton!
    
    override func awakeFromNib() {
       super.awakeFromNib()
//        badgeCountLabel.addObserver(self, forKeyPath: "text", options: [.old, .new], context: nil)
    }
//
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "text" {
////            print("old:", change?[.oldKey])
////            print("new:", change?[.newKey])
//            if let countStr = change?[.oldKey] as? String, countStr != "0"{
//                 badgeCount.isHidden = false
//            }
//        }
//    }
//    func setCount(count:String){
//      if  count != "0" {
//        badgeCount.isHidden = false
//        }
//    }

}
