//
//  DottedLineView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 10/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class DottedLineView: UIView {

    override public class var layerClass: Swift.AnyClass {
        return CAShapeLayer.self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        update()
    }
    
    func update(start: CGPoint, end: CGPoint) {
        guard let gradientLayer = layer as? CAShapeLayer else { return }
        gradientLayer.update(start: start, end: end)
    }
    
    func update() {
        update(start: CGPoint(x: frame.width/2, y: 1), end: CGPoint(x: frame.width/2, y: frame.height))
    }
}
