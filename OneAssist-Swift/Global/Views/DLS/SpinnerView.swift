//
//  SpinnerView.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 07/05/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner

class SpinnerView {
    
    var spinnerView: MMMaterialDesignSpinner = MMMaterialDesignSpinner(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
    
    init(view: UIView) {
        spinnerView.lineWidth = 2.0
        spinnerView.tintColor = UIColor.buttonTitleBlue
        
        view.addSubview(spinnerView)
        spinnerView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: spinnerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 32).isActive = true
        NSLayoutConstraint(item: spinnerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 32).isActive = true
        NSLayoutConstraint(item: spinnerView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: spinnerView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1.0, constant: 0).isActive = true
    }
    
    func startAnimating() {
//        spinnerView.isHidden = false
        spinnerView.startAnimating()
    }
    
    func stopAnimating() {
//        spinnerView.isHidden = true
        spinnerView.stopAnimating()
    }
}
