//
//  LayerContainerView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 09/09/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class LayerContainerView: UIView {
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(colors: [UIColor], type: CAGradientLayerType) {
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.update(colors: colors, type: type)
    }
    
    func update(start: CAGradientLayer.Point, end: CAGradientLayer.Point, colors: [UIColor], type: CAGradientLayerType) {
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.update(start: start, end: end, colors: colors, type: type)
    }
}
