//
//  DefaultFormField.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 26/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class DefaultTextField: UITextField {
    
    var textDidSet:(()->())?
    var copyEnabled:Bool = true
    var pasteEnabled:Bool = true
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
       self.font = DLSFont.bodyText.regular
        for constraint in constraints {
            if constraint.firstAttribute == .height {
                constraint.constant = 45
            }
        }
    }
    override var text: String?{
        didSet {
            textDidSet?()
        }
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if (action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:))) && !copyEnabled {
            return false
        }
        if action == #selector(UIResponderStandardEditActions.paste(_:)) && !pasteEnabled {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
