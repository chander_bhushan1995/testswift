//
//  ABOTPView.swift
//  ABAlertDemo
//
//  Created by Ankur Batham on 31/03/19.
//  Copyright © 2019 Ankur Batham. All rights reserved.
//

import UIKit

private let otpFieldTag = 1932

protocol ABOTPViewDelegate: class {
    func validate(enteredOTP:String)
    func isOTPValid(status:Bool)
}

public struct ABOTPConfig{
    public static var maxOTPField:Int = 6
    public static var bgColor:UIColor = UIColor.groupTableViewBackground
    public static var padding:CGFloat = 10
    public static var font:UIFont = UIFont.systemFont(ofSize: 20)
    public static var textColor:UIColor = UIColor.black
    public static var tintColor:UIColor = UIColor.lightGray
    public static var lineColor: UIColor = UIColor.gray2
    public static var leftMargin: CGFloat = 16
    
}

class ABOTPTextField:UITextField {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }

    fileprivate func loadinit() {
        let dummyView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height))
        dummyView.tag = 123
        dummyView.backgroundColor = UIColor.clear
        self.addSubview(dummyView)
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        self.sendActions(for: .valueChanged)
    }
    
    func editingEnable(enable: Bool) {
        let dummyView = self.viewWithTag(123)
        dummyView?.isHidden = enable
    }
}

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = ABOTPConfig.lineColor.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}


class ABOTPView: UIView {

    private var currentOTPTag = 0
    private var arrOTPFields:Array<ABOTPTextField> = Array()
    private var isLastOTPFieldDataCleared = false
    
    //MARK:- Public Variables
    public weak var delegate:ABOTPViewDelegate?
    
    //MARK:- Initilizer Methods
    public override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        //commonInit()
    }
    
    public func resetOTPFields(){
        
        for OTPField in arrOTPFields {
            //OTPField.isEnabled = false
            OTPField.editingEnable(enable: false)
            OTPField.text = ""
        }
        
        let initialOTPField:ABOTPTextField = arrOTPFields[0]
        //initialOTPField.isEnabled = true
        initialOTPField.editingEnable(enable: true)
        initialOTPField.becomeFirstResponder()
        currentOTPTag = initialOTPField.tag
        self.validOTP()
    }
    
    public func commonInit(){
        self.backgroundColor = UIColor.clear
        
        let textFieldWidth = (UIScreen.main.bounds.width - CGFloat(ABOTPConfig.leftMargin*2) - CGFloat(ABOTPConfig.maxOTPField - 1) * CGFloat(ABOTPConfig.padding)) / CGFloat(ABOTPConfig.maxOTPField)
        let textFieldHeight = self.bounds.height
        
        var x:CGFloat = 0
        
        if arrOTPFields.count > 0  , arrOTPFields.count == ABOTPConfig.maxOTPField {
            for OTPField in arrOTPFields {
               // OTPField.isEnabled = false
                OTPField.editingEnable(enable: false)
                var otpFrame = OTPField.frame
                otpFrame.origin.x = CGFloat(x)
                otpFrame.size.width = textFieldWidth
                otpFrame.size.height = textFieldHeight
                 x = x + textFieldWidth + ABOTPConfig.padding
            }
            
        }else {
            for subView in self.subviews {
                subView.removeFromSuperview()
            }
            
            
            arrOTPFields.removeAll()
            for index in 0..<ABOTPConfig.maxOTPField {
                createOTPTextField(CGRect.init(x: CGFloat(x), y: CGFloat(0), width: textFieldWidth, height: textFieldHeight),tag: index)
                x = x + textFieldWidth + ABOTPConfig.padding
            }
        }
        
    }
    
    fileprivate func createOTPTextField(_ frame:CGRect, tag:Int){
        
        let txtField:ABOTPTextField = ABOTPTextField.init(frame: frame)
        
        txtField.backgroundColor = ABOTPConfig.bgColor
        txtField.keyboardType = .numberPad
        txtField.tag = otpFieldTag + tag
        txtField.addTarget(self, action: #selector(ABOTPView.nextOTP(txtField:)), for: .editingChanged)
        txtField.addTarget(self, action: #selector(ABOTPView.previousOTP(txtField:)), for: .valueChanged)
        txtField.textAlignment = .center
        txtField.font = ABOTPConfig.font
        txtField.textColor = ABOTPConfig.textColor
        txtField.tintColor = ABOTPConfig.tintColor
        txtField.editingEnable(enable: false)
        //txtField.isEnabled = false
        txtField.delegate = self
        txtField.setBottomBorder()
        
        self.addSubview(txtField)
        
        arrOTPFields.append(txtField)
    }
    
    @objc fileprivate func nextOTP(txtField:ABOTPTextField){
        if(!(txtField.text?.isEmpty)!){
            if txtField.tag == otpFieldTag + ABOTPConfig.maxOTPField - 1 {
                //      txtField.isEnabled = false
               // txtField.editingEnable(enable: false)
                verifyOTP()
                return
                
            }
            
            for OTPField in arrOTPFields {
               // OTPField.isEnabled = false
                OTPField.editingEnable(enable: false)
            }
            
            let nextOTPField = arrOTPFields[(txtField.tag - otpFieldTag + 1)]
           // nextOTPField.isEnabled = true
            nextOTPField.editingEnable(enable: true)
            nextOTPField.becomeFirstResponder()
            
            nextOTPField.text = ""
            
            isLastOTPFieldDataCleared = false
            validOTP()
        }
    }
    
    @objc fileprivate func previousOTP(txtField:ABOTPTextField) {
        validOTP()
        if txtField.tag <= otpFieldTag {
            return
        }
        
        for OTPField in arrOTPFields {
           // OTPField.isEnabled = false
            OTPField.editingEnable(enable: false)
        }
        
        
        if txtField.tag == otpFieldTag + ABOTPConfig.maxOTPField - 1 && !isLastOTPFieldDataCleared{
            isLastOTPFieldDataCleared = true
            txtField.text = ""
           // txtField.isEnabled = true
            txtField.editingEnable(enable: true)
            txtField.becomeFirstResponder()
            return
            
        }
        
        
        let previousOTPField = arrOTPFields[(txtField.tag - otpFieldTag - 1)]
      //  previousOTPField.isEnabled = true
        previousOTPField.editingEnable(enable: true)
        previousOTPField.becomeFirstResponder()
        
        previousOTPField.text = ""
    }
    
    fileprivate func verifyOTP(){
        var enteredOTP = ""
        for OTPField in arrOTPFields {
            enteredOTP = enteredOTP + OTPField.text!
        }
        delegate?.validate(enteredOTP: enteredOTP)
        print("Final OTP: \(enteredOTP)")
        if enteredOTP.count == ABOTPConfig.maxOTPField {
            delegate?.isOTPValid(status: true)
        }
    }
    
    fileprivate func validOTP() {
        delegate?.isOTPValid(status: false)
    }
}

extension ABOTPView:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 1 // Bool
    }
}
