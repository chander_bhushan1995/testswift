//
//  OACalendarView.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 08/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol OACalendarViewDelegate: class {
    /// will be called when both date and time are selected and valid
    func dateChanged(_ date: Date, view: OACalendarView)
}

class OACalendarView: UIView, NibInstantiable{
    enum SelectionDay:String{
        case Today
        case Tomorrow
        case Yesterday
    }
    
    //MARK:- outlets
    @IBOutlet weak var BtnToday: OAPrimaryButton!{
        didSet{
            BtnToday.setTitle(secondDay.rawValue, for: .normal)
        }
    }
    @IBOutlet weak var BtnYesterday: OAPrimaryButton!{
        didSet{
            BtnYesterday.setTitle(firstDay.rawValue, for: .normal)
        }
    }
    @IBOutlet weak var BtnCalendar: OAPrimaryButton!
    @IBOutlet weak var dateContainerView: UIView!
    @IBOutlet weak var dateContainerStackView: UIStackView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: BodyTextBoldBlackLabel!
    
    //MARK:- public properties
    /// Minimum (oldest) allowed date and time
    public var minimumDate:Date?
    
    /// Maximum allowed date and time
    public var maximumDate:Date?

    /// Note in the date picker footer
    public var dateFooterNote:String?

    /// Initial date and time
    public var date:Date = Date()
    weak var delegate: OACalendarViewDelegate?
    public var firstDay:SelectionDay = .Yesterday{
        didSet{
             BtnYesterday.setTitle(firstDay.rawValue, for: .normal)
        }
    }
    public var secondDay:SelectionDay = .Today{
        didSet{
             BtnToday.setTitle(secondDay.rawValue, for: .normal)
        }
    }
    public var selectedDay:SelectionDay  = .Today{
        didSet{
            setDateLabel(selectedDay)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
}

//MARK:- configurations
extension OACalendarView {
   
    fileprivate func commonInit(){
        loadNibContent()
        configureView()
    }
    
    /// Will set initial date and time text labels
    func setInitialDate(date: Date?){
        if let date = date {
            setDate(date)
            setTime(date.string(with: .time12period))
        }
    }
    
    fileprivate func configureView(){
        self.dateContainerView.isHidden = true
        self.dateContainerStackView.isHidden = false
        setDateLabel(selectedDay)
        lblTime.textColor = UIColor.dodgerBlue
        
        // tag will categories buttons state
        self.BtnToday.tag = OAButtonTagType.Primary.enabledButton.rawValue
        self.BtnYesterday.tag = OAButtonTagType.Primary.outlineButton.rawValue
        self.BtnCalendar.tag = OAButtonTagType.Primary.outlineButton.rawValue
    }
}

//MARK:- OATimePickerDelegate
extension OACalendarView: OATimePickerDelegate{
    
    func timeSelected(_ time: String) {
        setTime(time)
        isDatePickable()
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
    fileprivate func setTime(_ time: String){
        self.lblTime.text = time
    }
}

//MARK:- OADatePickerDelegate
extension OACalendarView: OADatePickerDelegate{
    
    func dateSelected(_ date: Date) {
        setDate(date)
        isDatePickable()
        Utilities.topMostPresenterViewController.dismiss(animated: true)
    }
    
    fileprivate func setDate(_ date: Date){
        self.dateContainerView.isHidden = false
        self.dateContainerStackView.isHidden = true
        self.lblDate.text = date.string(with: .slotDateFormat)
    }
}

//MARK:- Time picker
extension OACalendarView {
    
    @IBAction func clickTimePickerBtn(_ sender: Any) {
        presentTimePicker()
    }
    
    fileprivate func presentTimePicker(){
        let timePicker = OATimePicker()
        timePicker.delegate = self
        timePicker.baseDate = self.lblDate.text
        if let maximumDate = maximumDate {timePicker.maximumDate = maximumDate}
        if let minimumDate = minimumDate {timePicker.minimumDate = minimumDate}
        timePicker.date = date
        Utilities.presentPopover(view: timePicker, height: Utilities.datePickerHeight)
    }
}

//MARK:- Date picker
extension OACalendarView {
   
    @IBAction func clickBtnYesterday(_ sender: Any) { //first btn
        self.BtnYesterday.tag = OAButtonTagType.Primary.enabledButton.rawValue
        self.BtnToday.tag = OAButtonTagType.Primary.outlineButton.rawValue
        setDateLabel(firstDay)
        isDatePickable()
        
    }
    @IBAction func clickBtnToday(_ sender: Any) { //second btn
        self.BtnToday.tag = OAButtonTagType.Primary.enabledButton.rawValue
        self.BtnYesterday.tag = OAButtonTagType.Primary.outlineButton.rawValue
        setDateLabel(secondDay)
        isDatePickable()
    }
    
    @IBAction func clickDatePickerBtn(_ sender: Any) {
        presentDatePicker()
    }
     fileprivate func setDateLabel(_ day:SelectionDay){
        var labelText = Date().string(with: .slotDateFormat)
        switch day {
        case .Today: labelText = Date().string(with: .slotDateFormat)
        case .Tomorrow: labelText = Date.tomorrow.string(with: .slotDateFormat)
        case .Yesterday: labelText = Date.yesterday.string(with: .slotDateFormat)
        }
        lblDate.text = labelText
    }
    fileprivate func presentDatePicker(){
        let datePicker = OADatePicker()
        if let maximumDate = maximumDate {datePicker.maximumDate = maximumDate}
        if let minimumDate = minimumDate {datePicker.minimumDate = minimumDate}
        if let date = DateFormatter.slotDateFormat.date(from: self.lblDate.text ?? ""){
            datePicker.date = date.addingTimeInterval(TimeInterval(19800))
        }
        datePicker.note = dateFooterNote
        //datePicker.configure()
        datePicker.delegate = self
        Utilities.presentPopover(view: datePicker, height: Utilities.datePickerHeight)
    }
}

//MARK:- Util
extension OACalendarView {
    /// to validate date and time when both are selected and valid
    fileprivate func isDatePickable(){
        let dateStr = lblDate.text!+", "+lblTime.text!
        let dateFormatter = DateFormatter.incidentTimeFormat
        dateFormatter.setLocal() //This will habdle the case when device is set to 24 hour format
        if let selectedDate12 = dateFormatter.date(from: dateStr){
            delegate?.dateChanged(selectedDate12, view: self)
        }
    }
}
