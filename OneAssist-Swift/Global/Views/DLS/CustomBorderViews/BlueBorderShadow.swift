//
//  ActiveFormField.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 26/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class BlueBorderShadow: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        super.initDefaultBorder()
        self.layer.cornerRadius = 2
        self.backgroundColor = UIColor.white
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red:0.01, green:0.51, blue:0.94, alpha:1).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowColor = UIColor(red:0.01, green:0.51, blue:0.94, alpha:0.2).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 8
        
        
    }
    
}
