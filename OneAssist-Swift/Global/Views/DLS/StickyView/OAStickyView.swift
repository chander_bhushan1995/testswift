//
//  OAStickyView.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 17/05/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OAStickyView: UIView{
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        layer.shadowColor = UIColor.dlsShadow.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: -2)
  
    }
   
}
