//
//  TransparentH3RegularButton.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 20/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TransparentH3RegularButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        setTitleColor(.buttonTitleBlue, for: .normal)
        titleLabel?.font = DLSFont.h3.regular
    }

}
