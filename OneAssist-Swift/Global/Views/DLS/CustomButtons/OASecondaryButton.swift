//
//  OASecondaryButton.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 29/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OASecondaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override var tag: Int {
        didSet {
            initializeView()
        }
    }
    
    
    private func initializeView() {
        backgroundColor = UIColor.clear
        setTitleColor(.buttonTitleBlue, for: .normal)
        titleLabel?.font = DLSFont.bodyText.regular
        configureButton()
    }
    
    private func configureButton(){
        switch tag {
        case OAButtonTagType.Secondary.H3Regular.rawValue:
            titleLabel?.font = DLSFont.h3.regular
        case OAButtonTagType.Secondary.H3Bold.rawValue:
            titleLabel?.font = DLSFont.h3.bold
        case OAButtonTagType.Secondary.bodyTextBold.rawValue:
            titleLabel?.font = DLSFont.bodyText.bold
        case OAButtonTagType.Secondary.supportingTextRegular.rawValue:
            titleLabel?.font = DLSFont.supportingText.bold
        case OAButtonTagType.Secondary.bodyTextRegularWithRightArrow.rawValue:
            titleLabel?.font = DLSFont.bodyText.bold
            addRightImage(image: #imageLiteral(resourceName: "arrowDLS"), spacing: 8)
        case OAButtonTagType.Secondary.supportingTextRegularWithDownArrow.rawValue:
            titleLabel?.font = DLSFont.supportingText.bold
            addRightImage(image: #imageLiteral(resourceName: "downArrowBlue"), spacing: 4)
        case OAButtonTagType.Secondary.supportingTextRegularWithRightArrow.rawValue:
            titleLabel?.font = DLSFont.supportingText.bold
            addRightImage(image: #imageLiteral(resourceName: "arrowDLS"), spacing: 8)
        default: break
        }
    }
    
    public func addRightImage(image:UIImage = #imageLiteral(resourceName: "arrowDLS"), spacing: CGFloat = 8) {
        let insetAmount = spacing / 2
        imageEdgeInsets = UIEdgeInsets(top: 2, left: insetAmount, bottom: 0, right: -insetAmount)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
        semanticContentAttribute = .forceRightToLeft
        setImage(image, for: .normal)
    }
}

