//
//  TransparentH3BoldButton.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 17/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TransparentH3BoldButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        setTitleColor(.buttonTitleBlue, for: .normal)
        titleLabel?.font = DLSFont.h3.bold
    }
    
}
