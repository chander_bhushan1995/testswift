//
//  TransparentBodyTextBoldButton.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 20/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TransparentBodyTextBoldButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                backgroundColor = UIColor.white
                setTitleColor(.buttonTitleBlue, for: .normal)
            }
            else {
                backgroundColor = UIColor.disabledAndLines
                setTitleColor(.white, for: .normal)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        backgroundColor = UIColor.clear
        setTitleColor(.buttonTitleBlue, for: .normal)
        titleLabel?.font = DLSFont.bodyText.bold
    }
}
