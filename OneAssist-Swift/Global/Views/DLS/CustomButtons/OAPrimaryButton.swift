//
//  OAPrimaryButton.swift
//  OneAssist-Swift
//
//  Created by Chanchal Chauhan on 29/04/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner

/**
 ## Tag loaderButton = 103:  to show loader in left side of the button.
 ## Tag disabledButton = 102: will remove the loader
 ## Tag enabledButton = 101: will remove the loadern.
 */

enum OAButtonTagType {
    
    enum Primary: Int {
        case enabledButton = 101
        case disabledButton = 102
        case loaderButton = 103
        case outlineButton = 104
    }
    
    enum Secondary: Int {
        case H3Regular = 101
        case bodyTextBold = 102
        case supportingTextRegular = 103
        case bodyTextRegularWithRightArrow = 104
        case supportingTextRegularWithDownArrow = 105
        case H3Bold = 106
        case supportingTextRegularWithRightArrow = 107
    }
    
    enum Other: Int {
        case grayButton = 101
        case outlineDeselected = 102
        case outlineSelected = 103
    }
}

class OAPrimaryButton: UIButton {
    
    var spinnerView: MMMaterialDesignSpinner?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? .buttonTitleBlue : .disabledAndLines
        }
    }
    
    override var tag: Int {
        didSet {
            initializeView()
        }
    }
    
    private func initializeView() {
        setupCommonProperties()
        backgroundColor = .buttonTitleBlue
        setTitleColor(.white, for: .normal)
        titleLabel?.font = DLSFont.h3.bold
        for constraint in constraints where constraint.firstAttribute == .height {
            constraint.constant = 48
        }
        configureButton()
        //TODO: Add shadow
    }
    
    private func configureButton() {
        spinnerView?.isHidden = true
        switch tag {
        case OAButtonTagType.Primary.enabledButton.rawValue:
            isEnabled = true
        case OAButtonTagType.Primary.disabledButton.rawValue:
            isEnabled = false
        case OAButtonTagType.Primary.loaderButton.rawValue:
            if spinnerView == nil {
                addLoader()
                spinnerView?.isHidden = true
            }
            spinnerView?.isHidden = false
            isEnabled = false
            backgroundColor = .selectedTextFieldBorderShadow
        case OAButtonTagType.Primary.outlineButton.rawValue:
            setTitleColor(.buttonTitleBlue, for: .normal)
            titleLabel?.font = DLSFont.h3.bold
            backgroundColor = .white
            layer.borderWidth = 1
            layer.borderColor = UIColor.buttonTitleBlue.cgColor
        default:
            break
        }
    }
}

extension OAPrimaryButton {
    public func addLoader() {
        spinnerView = MMMaterialDesignSpinner(frame: CGRect(x: 20, y: frame.height/2 - 15, width: 30, height: 30))
        spinnerView?.lineWidth = 2.0
        spinnerView?.tintColor = .white
        addSubview(spinnerView!)
        setStateOfLoaderButton(isLoading:true)
        spinnerView?.startAnimating()
    }
    
    public func removeLoader(){
        spinnerView?.removeFromSuperview()
        spinnerView?.stopAnimating()
        spinnerView?.isHidden = true
        spinnerView = nil
        setStateOfLoaderButton(isLoading:false)
    }
    
    private func setStateOfLoaderButton(isLoading:Bool) {
        if isLoading {
            isEnabled = false
            backgroundColor = .selectedTextFieldBorderShadow
        } else {
            isEnabled = true
        }
    }
}
