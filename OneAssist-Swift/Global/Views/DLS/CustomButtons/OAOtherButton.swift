//
//  OAOtherButton.swift
//  OneAssist-Swift
//
//  Created by Pankaj Verma on 29/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class OAOtherButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override var tag: Int {
        didSet {
            initializeView()
        }
    }
    
    private func initializeView() {
        setupCommonProperties()
        backgroundColor = UIColor.white
        setTitleColor(UIColor.bodyTextGray, for: .normal)
        titleLabel?.font = DLSFont.tags.bold
        for constraint in constraints where constraint.firstAttribute == .height {
            constraint.constant = 48
        }
        configureButton()
    }
    
    private func configureButton() {
        switch tag {
        case OAButtonTagType.Other.grayButton.rawValue:
            titleLabel?.font = DLSFont.tags.bold
        case OAButtonTagType.Other.outlineDeselected.rawValue:
            titleLabel?.font = DLSFont.bodyText.medium
            setTitleColor(.charcoalGrey, for: .normal)
            backgroundColor = .white
            layer.cornerRadius = 4
            layer.borderWidth = 1
            layer.borderColor = UIColor.gray2.cgColor
            clipsToBounds = true
        case OAButtonTagType.Other.outlineSelected.rawValue:
            titleLabel?.font = DLSFont.bodyText.bold
            setTitleColor(.white, for: .normal)
            backgroundColor = .buttonTitleBlue
            layer.cornerRadius = 4
            clipsToBounds = true
        default: break
        }
    }
}

