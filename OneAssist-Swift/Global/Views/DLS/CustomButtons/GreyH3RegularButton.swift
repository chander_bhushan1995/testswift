//
//  GreyH3RegularButton.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 20/03/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class GreyH3RegularButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        
        backgroundColor = UIColor.disabledAndLines
        setTitleColor(.white, for: .normal)
        titleLabel?.font = DLSFont.h3.regular
        for constraint in constraints {
            if constraint.firstAttribute == .height {
                constraint.constant = 44
            }
        }
        //todo: Add shadow if needed
        
    }


}
