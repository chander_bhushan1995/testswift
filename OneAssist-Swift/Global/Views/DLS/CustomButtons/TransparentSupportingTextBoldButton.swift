//
//  TransparentSupportingTextBoldButton.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 02/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TransparentSupportingTextBoldButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        backgroundColor = UIColor.clear
        setTitleColor(.buttonTitleBlue, for: .normal)
        titleLabel?.font = DLSFont.supportingText.bold
    }
}

