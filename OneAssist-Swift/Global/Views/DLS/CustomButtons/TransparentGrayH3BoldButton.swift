//
//  TransparentGrayH3BoldButton.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 01/04/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class TransparentGrayH3BoldButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        setTitleColor(.bodyTextGray, for: .normal)
        titleLabel?.font = DLSFont.supportingText.bold
    }

}
