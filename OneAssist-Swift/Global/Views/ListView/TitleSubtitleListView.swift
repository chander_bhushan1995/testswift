//
//  TitleSubtitleListView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 09/12/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

struct TitleSubtitleListPointModel {
    var title:String?
    var subtitle: String?
}

struct TitleSubtitleListViewModel {
    var listHeader: String?
    var list: [TitleSubtitleListPointModel] = []
    var textAlignment: NSTextAlignment = .left
    var listHeaderToListSpacing: CGFloat = 16
    var paragraphSpacing: CGFloat = 20
    var titleToSubtitleSpacing: CGFloat = 4
    var headerFont: UIFont = DLSFont.bodyText.regular
    var titleFont: UIFont = DLSFont.supportingText.bold
    var subtitleFont: UIFont = DLSFont.bodyText.regular
    var headerColor: UIColor = .charcoalGrey
    var titleColor: UIColor = .charcoalGrey
    var subtitleColor: UIColor = .bodyTextGray
}

class TitleSubtitleListView: UIView {
    var label: UILabel?
    
    @discardableResult
    func setViewModel(model: TitleSubtitleListViewModel) -> CGFloat? {
        let items = model.list
        let textAlignment = model.textAlignment
        
        // create our NSTextAttachment
        var fullString = NSMutableAttributedString()
        
        
        if let header = model.listHeader {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = textAlignment
            paragraphStyle.paragraphSpacing = model.listHeaderToListSpacing
            
            fullString = NSMutableAttributedString(string: header + "\n", attributes: [.font: model.headerFont, .paragraphStyle: paragraphStyle, .foregroundColor: model.headerColor])
        }
        
        for (index, listPoint) in items.enumerated() {
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            var listIndexString = NSMutableAttributedString()
            if let title = listPoint.title, !title.isEmpty {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = textAlignment
                paragraphStyle.paragraphSpacing = model.titleToSubtitleSpacing
                
                listIndexString = NSMutableAttributedString(string: title + "\n", attributes: [.font: model.titleFont, .paragraphStyle: paragraphStyle, .foregroundColor: model.titleColor])
            }
            
            if var subtitle = listPoint.subtitle, !subtitle.isEmpty {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = textAlignment
                paragraphStyle.paragraphSpacing = model.paragraphSpacing
                
                if index != items.count - 1 {
                    subtitle = subtitle + "\n"
                }
                
                listIndexString.append(NSMutableAttributedString(string: subtitle, attributes: [.font: model.subtitleFont, .paragraphStyle: paragraphStyle, .foregroundColor: model.subtitleColor]))
            }
            
            fullString.append(listIndexString)
        }
        
        // draw the result in a label
        label?.removeFromSuperview()
        label = UILabel()
        label!.numberOfLines = 0
        
        label!.attributedText = fullString
        label!.textAlignment = textAlignment
        
        if textAlignment == .left {
            label!.sizeToFit()
        }
        
        addSubview(label!)
        label!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label!.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            label!.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            label!.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            label!.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            ])
        
        
        var count = 0
        let charCount = Int(UIScreen.main.bounds.width/10)
        for item in items {
            var c = (item.title?.count ?? 0)/(charCount) //asuming 40 character in 1 line
            if c == 0 { c = 1 }
            count +=  c
            
            var d = (item.subtitle?.count ?? 0)/(charCount) //asuming 40 character in 1 line
            if d == 0 { d = 1 }
            count +=  d
        }
        for constraint in constraints {
            if constraint.firstAttribute == .height {
                constraint.constant = count.cgFloat * 36  //label!.frame.height
            }
            if constraint.firstAttribute == .width {
                constraint.constant = label!.frame.width
            }
        }
        return label?.frame.height
    }
}
