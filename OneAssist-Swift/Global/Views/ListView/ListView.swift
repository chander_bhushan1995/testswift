//
//  ListView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 30/10/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

struct ListViewModel {
    var listHeader: String?
    var list: [String] = []
    var image = #imageLiteral(resourceName: "compare_tick")
    var textAlignment: NSTextAlignment = .left
    var bodyColor: UIColor = .charcoalGrey
    var imageSize = CGSize(width: 8, height: 6)
    var paragraphSpacing: CGFloat = 12
    var headerToListSpacing: CGFloat = 24
    var listFont: UIFont = DLSFont.bodyText.regular
    var imageOriginPoint:CGPoint = .zero
    var lineSpacing: CGFloat = 4
}

class ListView: UIView {
    var label: UILabel?
    @discardableResult
    func setViewModel(model: ListViewModel) -> CGFloat? {
        let items = model.list
        let textAlignment = model.textAlignment
        let imageSize = model.imageSize 
        let listFont = model.listFont
        let spaces = "  "
        
        // create our NSTextAttachment
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = model.image
        imageAttachment.bounds = CGRect(origin: model.imageOriginPoint, size: imageSize)
        
        var fullString = NSMutableAttributedString()
        
        if let header = model.listHeader {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = textAlignment
            paragraphStyle.paragraphSpacing = model.headerToListSpacing
            fullString = NSMutableAttributedString(string: header + "\n", attributes: [.font: listFont, .paragraphStyle: paragraphStyle, .foregroundColor: model.bodyColor])
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineSpacing = model.lineSpacing
        paragraphStyle.paragraphSpacing = model.paragraphSpacing
        paragraphStyle.headIndent = imageSize.width + spaces.width(withHeight: .greatestFiniteMagnitude, font: listFont)
        
        for (index, text) in items.enumerated() {
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            let listIndexString = NSMutableAttributedString()
            listIndexString.append(NSAttributedString(attachment: imageAttachment))
            listIndexString.append(NSAttributedString(string: spaces + text))
            if index != items.count - 1 {
                listIndexString.append(NSAttributedString(string: "\n"))
            }
            
            listIndexString.addAttributes([.font: listFont, .paragraphStyle: paragraphStyle, .baselineOffset: NSNumber(value: 0), .foregroundColor: model.bodyColor], range: NSRange(location: 0, length: listIndexString.length))
            
            fullString.append(listIndexString)
        }
        
        // draw the result in a label
        label?.removeFromSuperview()
        label = UILabel()
        label!.numberOfLines = 0
        
        label!.attributedText = fullString
        label!.textAlignment = textAlignment
        
        if textAlignment == .left {
            label!.sizeToFit()
        }
        
        addSubview(label!)
        label!.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label!.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            label!.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            label!.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            label!.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            ])
        
        
        var count = 0
        let charCount = Int(UIScreen.main.bounds.width/10)
        for item in items {
            var c = item.count/(charCount) //asuming 40 character in 1 line
            if c == 0 { c = 1 }
            count +=  c
        }
        for constraint in constraints {
            if constraint.firstAttribute == .height {
                constraint.constant = count.cgFloat * 36  //label!.frame.height
            }
            if constraint.firstAttribute == .width {
                constraint.constant = label!.frame.width
            }
        }
        return label?.frame.height
    }
}


