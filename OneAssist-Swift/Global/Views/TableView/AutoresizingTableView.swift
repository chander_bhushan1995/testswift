//
//  AutoresizingTableView.swift
//  OneAssist-Swift
//
//  Created by Sudhir.Kumar on 21/02/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class AutoresizingTableView: UITableView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.estimatedRowHeight = CGFloat(100)
        self.rowHeight = UITableView.automaticDimension
       
    }
}
