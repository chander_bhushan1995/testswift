//
//  BaseNibView.swift
//  LandingPage
//
//  Created by Mukesh on 7/19/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@IBDesignable
class BaseNibView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    func customInit() {
        
        let ownerType = type(of: self)
        let className = String(describing: ownerType)
        let view = Bundle(for: ownerType).loadNibNamed(className, owner: self, options: nil)?.first as! UIView
        addSubview(view)
        view.frame = bounds
    }
    
}
