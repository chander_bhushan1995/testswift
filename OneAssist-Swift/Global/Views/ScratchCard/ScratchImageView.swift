//
//  ScratchImageView.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 03/12/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

protocol ScratchImageViewDelegate: class {
    func percentScratched(_ percentScratched: Double)
}

class ScratchImageView: UIImageView {
    
    var lineWidth: CGFloat = 40
    weak var delegate: ScratchImageViewDelegate?
    private var lastPoint: CGPoint?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override init(image: UIImage?) {
        super.init(image: image)
        isUserInteractionEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            lastPoint = touch.location(in: self)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let point = lastPoint {
            let currentLocation = touch.location(in: self)
            scratch(from: point, to: currentLocation)
            lastPoint = currentLocation
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let point = lastPoint {
            let currentLocation = touch.location(in: self)
            scratch(from: point, to: currentLocation)
            lastPoint = currentLocation
        }
    }
    
    private func scratch(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(frame.size)
        image?.draw(in: bounds)
        
        let path = CGMutablePath()
        path.move(to: fromPoint)
        path.addLine(to: toPoint)
        
        let context = UIGraphicsGetCurrentContext()!
        context.setLineCap(.round)
        context.setLineWidth(lineWidth)
        context.setBlendMode(.clear)
        context.addPath(path)
        context.strokePath()
        
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let transparentValue = image?.transparentPercentage()
        delegate?.percentScratched(transparentValue ?? 0.0)
    }
}

extension UIImage {
    func transparentPercentage() -> Double {
        guard let image = cgImage, let imageData = image.dataProvider?.data else { return 0.0 }
        
        let width = image.width
        let height = image.height
        let imageDataPointer = CFDataGetBytePtr(imageData)
        var transparentPixelCount = 0
        for i in 0..<(width*height) where imageDataPointer?[4*i + 3] == 0 {
            // check for indices at 4*i+3 bacuase in pixels color space the data is set as 0 = R, 1 = G, 2 = B, 3 = A
            transparentPixelCount += 1
        }
        
        return Double(transparentPixelCount) / Double(width * height)
    }
}
