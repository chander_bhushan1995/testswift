//
//  ImageCaptionView.swift
//  LandingPage
//
//  Created by Mukesh on 7/19/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class ImageCaptionView: BaseNibView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var middleLabel: H3RegularBlackLabel!
    @IBOutlet weak var viewAppOnly: UIView!
    @IBOutlet weak var labelAppOnly: SupportingTextRegularBlackLabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        viewAppOnly.layer.cornerRadius = 3
        labelAppOnly.textColor = UIColor.white
        
    }
    
    func set(text: String, image: UIImage?, hintText: String? = nil, appOnly: Bool = false) {
        imageView.image = image
        
        if image == nil {
            titleLabel.text = nil
            middleLabel.text = nil
            
        }else {
            titleLabel.text = text
            middleLabel.text = hintText
            
            if appOnly {
                viewAppOnly.isHidden = false
            }
        }
        
    }
}
