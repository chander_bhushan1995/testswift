//
//  ImageTitleVIew.swift
//  OneAssist-Swift
//
//  Created by Varun on 13/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

struct ImageTitleViewModel {
    var title: String?
    var image: UIImage?
}

class ImageTitleView: BaseNibView {

    @IBOutlet weak var imageViewObj: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
//        labelTitle.font = DLSFont.h3.bold
//        labelTitle.textColor = UIColor.darkGray
    }
    
    func setViewModel(_ viewModel: ImageTitleViewModel) {
        labelTitle.text = viewModel.title
        imageViewObj.image = viewModel.image
        layoutIfNeeded()
    }
}
