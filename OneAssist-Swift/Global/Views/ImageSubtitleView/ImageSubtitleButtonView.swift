//
//  ImageSubtitleButtonView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/24/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

protocol ImageSubtitleButtonViewDelegate: class {
    func imageSubtitleButton(view: ImageSubtitleButtonView, clickedBtn sender: Any)
}

class ImageSubtitleButtonView: BaseNibView {
    @IBOutlet weak var imageSubtitleView: ImageSubtitleView!
    @IBOutlet weak var button: UIButton!
    
    weak var delegate: ImageSubtitleButtonViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        let leadingConstraint = NSLayoutConstraint(item: button, attribute: .leading, relatedBy: .equal, toItem: imageSubtitleView.subtitleLabel, attribute: .leading, multiplier: 1, constant: 0)
        addConstraint(leadingConstraint)
        
        layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    @IBAction func clickedButton(_ sender: Any) {
        delegate?.imageSubtitleButton(view: self, clickedBtn: sender)
    }
}
