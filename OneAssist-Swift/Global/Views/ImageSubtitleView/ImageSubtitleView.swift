//
//  ImageSubtitleView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/24/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

struct ImageSubtitleViewModel {
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var imageUrlStr: String?
    
    init(title: String?, subtitle: String?, image: UIImage?, imageUrlStr: String? = nil) {
        self.title = title
        self.subtitle = subtitle
        self.image = image
        self.imageUrlStr = imageUrlStr
    }
}

class ImageSubtitleView: BaseNibView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    private func initializeView() {
        titleLabel.font = DLSFont.h3.regular
        titleLabel.textColor = UIColor.darkGray
        
        subtitleLabel.font = DLSFont.bodyText.regular
        subtitleLabel.textColor = UIColor.lightGray
//        layoutMargins = UIEdgeInsets(top: 15, left: 15, bottom: 21, right: 15)
    }
    
    func setViewModel(_ viewModel: ImageSubtitleViewModel) {
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        
        if viewModel.imageUrlStr != nil {
            imageView.setImageWithUrlString(viewModel.imageUrlStr!)
        } else {
            imageView.image = viewModel.image
        }
        
        layoutIfNeeded()
    }
}
