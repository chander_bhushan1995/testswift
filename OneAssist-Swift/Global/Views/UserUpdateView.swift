//
//  UserUpdateView.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 23/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

@objc protocol UserUpdateViewDelegate: NSObjectProtocol {
    func gotitButtonAction()
}

struct UserUpdateModel {
    var title: String?
    var detail: String?
}

class UserUpdateView: UIView {
    
    @IBOutlet var bottomSheetView: UIView!
    @IBOutlet weak var welcomeMessageLabel: H2RegularLabel!
    @IBOutlet weak var generalInfoLabel: H3RegularBlackLabel!
    
    weak var delegate: UserUpdateViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadinit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadinit()
    }
    
    private func loadinit() {
        let bundle = Bundle(for: self.classForCoder)
        bundle.loadNibNamed("UserUpdateView", owner: self, options: nil)
        addSubview(bottomSheetView)
        bottomSheetView.frame = self.bounds
    }
    
    @IBAction func btnPressed(_ sender: Any) {
        delegate?.gotitButtonAction()
    }
    
    func setUpValue(_ model: UserUpdateModel) {
        self.welcomeMessageLabel.text = model.title
        self.generalInfoLabel.text = model.detail
    }
    
    
    class func height(forModel model: UserUpdateModel) -> CGFloat {
        let bottomSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
        let widthConstraints: CGFloat = 40.0
        let imageHeight: CGFloat = 32 + 32 + 24 + 16 + 48 + 48 + 18
        let titleHeight = model.title?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.h2.regular) ?? 0.0
        let descriptionHeight = model.detail?.height(withWidth: screensize.width - widthConstraints, font: DLSFont.h3.regular) ?? 0.0
    
        return  bottomSafeArea + imageHeight + titleHeight + descriptionHeight
    }
}
