//
//  SelectbankToolTipView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 10/03/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

enum ButtonAction: Int {
    case dismiss = 100
    case selectBank = 200
    case cross = 300
}

struct SelectbankToolTipModel {
    var title: String?
    var detail: String?
    var fistButtonText: String?
    var secondButtonText: String?
}

class SelectbankToolTipView: UIView {
    
    open var tapHandler: ((ButtonAction) -> Void)?
    
 //   @IBOutlet var containerView: UIView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var titleLabel: H3BoldLabel!
    @IBOutlet weak var detailLabel: SupportingTextRegularBlackLabel!
    @IBOutlet weak var dismissButton: OASecondaryButton!
    @IBOutlet weak var selectBankButton: OASecondaryButton!
    
    class func loadView() -> SelectbankToolTipView {
        return Bundle.main.loadNibNamed("SelectbankToolTipView", owner: self, options: nil)?[0] as! SelectbankToolTipView
    }
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    func configureView() {
        titleLabel.textColor = UIColor.white
        detailLabel.textColor = UIColor.white.withAlphaComponent(0.75)
        
        dismissButton.backgroundColor = UIColor.clear
        dismissButton.layer.cornerRadius = 2.0
        dismissButton.layer.borderColor = UIColor.white.cgColor
        dismissButton.layer.borderWidth = 1.0
        dismissButton.tintColor = UIColor.white
        dismissButton.setTitleColor(UIColor.white, for: .normal)
        
        selectBankButton.backgroundColor = UIColor.white
        selectBankButton.layer.cornerRadius = 2.0
        selectBankButton.setTitleColor(UIColor.buttonBlue, for: .normal)
        
        crossButton.backgroundColor = UIColor.blueDark
        crossButton.layer.cornerRadius = crossButton.bounds.height/2.0
    }
    
    func setupdetail(model: SelectbankToolTipModel){
        self.titleLabel.text = model.title
        self.detailLabel.text = model.detail
        dismissButton.setTitle(model.fistButtonText, for: .normal)
        selectBankButton.setTitle(model.secondButtonText, for: .normal)
    }
    
    
    class func height(forModel model:SelectbankToolTipModel, with width: CGFloat) -> CGFloat {
        
        let widthConstraints: CGFloat = 12.0
        let collectiveHeigthConstains: CGFloat = 24+4+4+20+40+6
        let titleHeight = model.title?.height(withWidth: width - widthConstraints, font: DLSFont.h3.bold) ?? 0.0
        let descriptionHeight = model.detail?.height(withWidth: width - widthConstraints, font: DLSFont.supportingText.regular) ?? 0.0
        
        return   collectiveHeigthConstains + titleHeight + descriptionHeight
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK: - UIButton Action
    
    @IBAction func onClickCrossAction(_ sender: UIButton?){
        tapHandler?(ButtonAction.cross)
    }
    
    @IBAction func onClickDismissAction(_ sender: UIButton?){
        tapHandler?(ButtonAction.dismiss)
    }
    
    @IBAction func onClickSelectBankAction(_ sender: UIButton?){
        tapHandler?(ButtonAction.selectBank)
        
    }

}
