//
//  BankMarkerView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 18/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class BankMarkerView: GMSMarker {

    var bankName : String?
    var bankAddress : String?
    var bankPhoneNumber : String?
    var bankCoordinates : CLLocationCoordinate2D?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
