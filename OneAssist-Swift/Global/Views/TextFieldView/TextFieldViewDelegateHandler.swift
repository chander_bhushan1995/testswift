//
//  TextFieldViewDelegateHandler.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/23/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation

class TextFieldViewDelegateHandler: NSObject, TextFieldViewDelegate {
    
    var requiredFields: [TextFieldView]
    var validationHandler: (_ isValid: Bool,_ replacingTextWithView: (String,TextFieldView)?)->()
    
    var invalidFieldCount: Int {
        didSet {
            validationHandler(invalidFieldCount == 0, replacingTuple)
        }
    }
    
    func validateValues() {
        validationHandler(invalidFieldCount == 0, replacingTuple)
    }
    
    var replacingTuple: (String,TextFieldView)? = nil
    
    init(requiredFields: [TextFieldView], validationHandler: @escaping (_ isValid: Bool,_ replacingText: (String,TextFieldView)?)->()) {
        self.requiredFields = requiredFields
        self.validationHandler = validationHandler
        invalidFieldCount = requiredFields.count
        
        super.init()
        
        for textFiled in requiredFields {
            textFiled.delegate = self
        }
    }
    
    private var _wasInvalid: Bool = true
    
    func textFieldViewShouldBeginEditing(_ textFieldView: TextFieldView) -> Bool {
        return true
    }
    
    func textFieldViewShouldEndEditing(_ textFieldView: TextFieldView) -> Bool {
        _wasInvalid = textFieldView.isInvalid
        return true
    }
    
    func textFieldViewDidBeginEditing(_ textFieldView: TextFieldView) {
        
    }
    
    func textFieldViewDidEndEditing(_ textFieldView: TextFieldView) {
        
        guard invalidFieldCount >= 1 else {
            return
        }
        
        textFieldView.isInvalid = !(textFieldView.fieldType.validateTextLimit(text: textFieldView.textFieldObj.text))
        
        if _wasInvalid != textFieldView.isInvalid {
            replacingTuple = nil
            invalidFieldCount = (textFieldView.isInvalid) ? invalidFieldCount + 1 : invalidFieldCount - 1
        }
    }
    
    func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        guard invalidFieldCount == 1 || invalidFieldCount == 0  else {
            return true
        }
        
        _wasInvalid = textFieldView.isInvalid
        
        let str = (textFieldView.textFieldObj.text ?? "") as NSString
        let strNew = str.replacingCharacters(in: range, with: string)
        textFieldView.isInvalid = !(textFieldView.fieldType.validateTextLimit(text: strNew))
        
        if _wasInvalid != textFieldView.isInvalid {
            replacingTuple = (strNew,textFieldView)
            
            defer {
                invalidFieldCount = (textFieldView.isInvalid) ? invalidFieldCount + 1 : invalidFieldCount - 1
                _wasInvalid = invalidFieldCount == 0 ? false : true
            }
        }
        
        return true
    }
}
