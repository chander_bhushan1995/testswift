//
//  TextFieldDescriptionView.swift
//  OneAssist-Swift
//
//  Created by Varun on 19/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class TextFieldDescriptionView: BaseNibView {

	@IBOutlet weak var labelDescription: SupportingTextRegularBlackLabel!
	@IBOutlet weak var imageViewObj: UIImageView!
	
	@IBOutlet var constraintLabelDescriptionLeading: NSLayoutConstraint!
	@IBOutlet var constraintImageViewHeight: NSLayoutConstraint!
    
	var hasImage: Bool = false {
		didSet {
			constraintLabelDescriptionLeading.isActive = hasImage
			if !(hasImage) {
                constraintImageViewHeight.constant = 0.0
				imageViewObj.image = nil
            }else {
                constraintImageViewHeight.constant = 29.0
            }
		}
	}
}
