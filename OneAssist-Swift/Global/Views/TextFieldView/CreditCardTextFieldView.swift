//
//  CreditCardTextFieldView.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/26/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import UIKit

class CreditCardTextFieldView: TextFieldView {
    private var previousTextFieldContent: String?
    private var previousSelection: UITextRange?
    
    override var fieldText: String? {
        set {
            textFieldObj.text = newValue
        }
        
        get {
            return textFieldObj.text?.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "")
        }
    }
    
    override func customInit() {
        
        let ownerType = type(of: self)
        let view = Bundle(for: ownerType).loadNibNamed("TextFieldView", owner: self, options: nil)?.first as! UIView
        addSubview(view)
        view.frame = bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textFieldObj.addTarget(self, action: #selector(reformatAsCardNumber), for: .editingChanged)
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        previousTextFieldContent = textField.text
        previousSelection = textField.selectedTextRange
        
        return super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }
    
    @objc private func reformatAsCardNumber(textField: UITextField) {
        
        if fieldType != CardFieldType.self && fieldType != BankCardFieldType.self {
            return
        }
        
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }
        
        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }
        
        if cardNumberWithoutSpaces.count > fieldType.maxCharacterLength {
            textField.text = previousTextFieldContent
            textField.selectedTextRange = previousSelection
            return
        }
        
        let cardNumberWithSpaces = self.insertSpacesEveryFourDigitsIntoString(string: cardNumberWithoutSpaces, andPreserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces
        
        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }
    }
    
    private func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition
        
        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }
        
        return digitsOnlyString
    }
    
    private func insertSpacesEveryFourDigitsIntoString(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        
        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            if i > 0 && (i % 4) == 0 {
                stringWithAddedSpaces.append(" ")
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        
        return stringWithAddedSpaces
    }
}
