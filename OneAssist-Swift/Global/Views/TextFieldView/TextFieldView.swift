//
//  TextFieldView.swift
//  OneAssist-Swift
//
//  Created by Varun on 19/07/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

@objc protocol TextFieldViewDelegate {
    @objc optional func textFieldViewShouldBeginEditing(_ textFieldView: TextFieldView) -> Bool
    @objc optional func textFieldViewShouldEndEditing(_ textFieldView: TextFieldView) -> Bool
    @objc optional func textFieldViewDidBeginEditing(_ textFieldView: TextFieldView)
    @objc optional func textFieldViewDidEndEditing(_ textFieldView: TextFieldView)
    @objc optional func textFieldView(_ textFieldView: TextFieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    @objc optional func textViewBtnClicked(_ textFieldView: TextFieldView)
    @objc optional func textViewRightBtnClicked(_ textFieldView: TextFieldView)
}


enum TextFieldState {
    case selected
    case normal
    case error(String)
}

class TextFieldView: BaseNibView {
    
    // properties
    
    weak var delegate: TextFieldViewDelegate?
    
    var fieldType: FieldType.Type = BaseFieldType.self {
        didSet {
            configureField()
        }
    }
    
    var isInvalid: Bool = true
    var isSetRightImageAction: Bool = false
    
    var descriptionText: String? {
        didSet {
            textFieldDescriptionView.labelDescription.text = descriptionText
        }
    }
    
    var prefixText: String? {
        didSet {
            if prefixText != nil {
                let prefix = UILabel()
                prefix.text =   "  " + prefixText!
                prefix.font = DLSFont.h3.regular
                prefix.textColor = textFieldObj.textColor
                prefix.sizeToFit()
                //                let size    = prefix.sizeThatFits(CGSize(width: textFieldObj.bounds.width, height: textFieldObj.bounds.height))
                //                prefix.textAlignment = .center
                //                prefix.frame = CGRect(x: 5, y: 0, width: size.width+10, height: size.height)
                textFieldObj.leftView = prefix
                textFieldObj.leftViewMode = .always
                let fontAttribute:[NSAttributedString.Key:UIFont] = [NSAttributedString.Key.font: prefix.font]
                let size = prefix.text!.size(withAttributes: fontAttribute)
                placeholderLeading.constant = size.width + 8
            } else {
                textFieldObj.leftView = nil
                placeholderLeading.constant = 8
            }
            
        }
    }
    
    var suffixText: String? {
        didSet {
            if suffixText != nil {
                let suffix = UILabel()
                suffix.text = "  " + suffixText! + " "
                suffix.font = DLSFont.h3.regular
                suffix.textColor = UIColor.bodyTextGray
                suffix.sizeToFit()
                //                let size = suffix.sizeThatFits(CGSize(width: textFieldObj.bounds.width, height: textFieldObj.bounds.height))
                let fontAttribute:[NSAttributedString.Key:UIFont] = [NSAttributedString.Key.font: suffix.font]
                let size = suffix.text!.size(withAttributes: fontAttribute)
                //                prefix.textAlignment = .center
                suffix.frame = CGRect(x: 0, y: 0, width: size.width + 8, height: size.height)
                textFieldObj.rightView = suffix
                textFieldObj.rightViewMode = .always
                //                placeholderLeading.constant = size.width + 8
            }else {
                textFieldObj.rightView = nil
                //                placeholderLeading.constant = 8
            }
            
        }
    }
    
    
    var rightImage: UIImage? {
        didSet {
            if rightImage != nil {
                let imageView = UIImageView()
                imageView.image = rightImage
                imageView.sizeToFit()
                let trailingPadding:CGFloat = 16
                let rightView = UIView(frame: CGRect(x: 0, y: 0, width: imageView.frame.width + trailingPadding, height: imageView.frame.height))
                rightView.addSubview(imageView)
                if isSetRightImageAction {
                    rightView.isUserInteractionEnabled = true
                    let tap = UITapGestureRecognizer(target: self, action: #selector(onClickRightImage))
                    rightView.addGestureRecognizer(tap)
                }
                
                textFieldObj.rightView = rightView
                textFieldObj.rightViewMode = .always
            } else {
                textFieldObj.rightView = nil
            }
        }
    }
    
    
    var rightButton: String? {
        didSet {
            if rightButton != nil {
                let button = UIButton()
                button.setTitle(rightButton, for: .normal)
                button.setTitleColor(.buttonTitleBlue, for: .normal)
                button.titleLabel?.font = DLSFont.bodyText.bold
                button.sizeToFit()
                button.addTarget(self, action: #selector(onClickRightButton), for: .touchUpInside)
                let trailingPadding:CGFloat = 12
                let rightView = UIView(frame: CGRect(x: 0, y: 0, width: button.frame.width + trailingPadding, height: button.frame.height))
                rightView.addSubview(button)
                textFieldObj.rightView = rightView
                textFieldObj.rightViewMode = .always
            } else {
                textFieldObj.rightView = nil
            }
        }
    }
    
    var placeholderFieldText: String? {
        didSet {
            //textFieldObj.placeholder = placeholderFieldText
            //            let attributes = [
            //                NSAttributedString.Key.foregroundColor: UIColor.bodyTextGray,
            //                NSAttributedString.Key.font : DLSFont.tags.regular
            //            ]
            //
            //            textFieldObj.attributedPlaceholder = NSAttributedString(string: placeholderFieldText ?? "", attributes:attributes)
            placeholderLabel.text = placeholderFieldText
        }
    }
    
    var fieldText: String? {
        set {
            textFieldObj.text = newValue
        } get {
            return textFieldObj.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    var imageField: UIImage? {
        didSet {
            hasImage = true
            textFieldDescriptionView.imageViewObj.image = imageField
        }
    }
    
    var invalidImageField: UIImage?
    
    var hasImage: Bool = false {
        didSet {
            textFieldDescriptionView.hasImage = hasImage
        }
    }
    
    var hasDescriptionView: Bool = true {
        didSet {
            hasImage = false
            textFieldDescriptionView.isHidden = !hasDescriptionView
        }
    }
    
    override var isUserInteractionEnabled: Bool  {
        didSet {
            self.textFieldObj.isUserInteractionEnabled = isUserInteractionEnabled
            if !isUserInteractionEnabled {
                self.textFieldObj.textColor = UIColor.dlsShadow
                textFieldDescriptionView.labelDescription.textColor = UIColor.dlsShadow
                self.textFieldObj.styleTextField(with: UIColor.dlsShadow, borderColor: UIColor.dlsShadow, shadowColor: UIColor.dlsShadow)
                labelError.textColor = UIColor.dlsShadow
            }else {
                animateTextField(state: .normal)
                self.textFieldObj.textColor = UIColor.charcoalGrey
                textFieldDescriptionView.labelDescription.textColor = UIColor.charcoalGrey
            }
        }
    }
    
    
    // outlets
    @IBOutlet weak var textFieldDescriptionView: TextFieldDescriptionView!
    @IBOutlet weak var textFieldObj: DefaultTextField!
    @IBOutlet weak var labelError: SupportingTextRegularBlackLabel!
    //    @IBOutlet weak var rightImageView:UIImageView!
    @IBOutlet weak var button:UIButton!
    @IBOutlet weak var placeholderLabel:OALabel!{
        didSet{
            placeholderLabel.font = DLSFont.bodyText.regular
            placeholderLabel.isEnabled = false
        }
    }
    @IBOutlet weak var placeholderLeading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        textFieldObj.delegate = self
        configureView()
    }
    
    private func configureView() {
        hasImage = false
        placeholderLabel.isHidden = !isEmpty()
        configureField()
    }
    
    private func configureField() {
        self.animateTextField(state: .normal)
        textFieldObj.keyboardType = fieldType.keyboardType
        textFieldObj.isSecureTextEntry = fieldType.isSecure
        button.isHidden = true
        textFieldObj.isUserInteractionEnabled = true
        textFieldObj.textDidSet = { [unowned self] in
            self.placeholderLabel.isHidden = !self.isEmpty()
        }
    }
    
    func setError(_ error: String) {
        animateTextField(state: .error(error))
    }
    
    func animateTextField(state: TextFieldState) {
        
        var textfieldBordercolor: UIColor!
        var textfieldBorderShadowcolor: UIColor!
        var image: UIImage?
        var description: String?
        var imageColor: UIColor? = UIColor.errorMessage
        
        switch state {
        case .normal:
            textfieldBordercolor = UIColor.bodyTextGray
            textfieldBorderShadowcolor =  UIColor.activeTextFieldBorderShadow
            image = imageField
            description = nil
        case .selected:
            textfieldBordercolor = UIColor.buttonTitleBlue
            textfieldBorderShadowcolor = UIColor.selectedTextFieldBorderShadow
            image = imageField
            description = nil
        case .error(let error):
            textfieldBordercolor = UIColor.errorTextFieldBorder
            textfieldBorderShadowcolor = UIColor.errorTextFieldBorderShadow
            image = imageField
            description = error
            imageColor = nil
        }
        
        self.textFieldObj.styleTextField(with: UIColor.white, borderColor: textfieldBordercolor, shadowColor: textfieldBorderShadowcolor)
        
        UIView.animate(withDuration: 0.3) {
            
            if let imageColor = imageColor {
                self.textFieldDescriptionView.imageViewObj.image = image?.withRenderingMode(.alwaysTemplate)
                self.textFieldDescriptionView.imageViewObj.tintColor = imageColor
            } else {
                self.textFieldDescriptionView.imageViewObj.image = image
            }
            
            self.labelError.text = description
        }
        
        UIView.transition(with: labelError, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.labelError.textColor = textfieldBordercolor
        }, completion: nil)
    }
    
    @objc func onClickRightImage() {
        self.delegate?.textViewRightBtnClicked?(self)
    }
    
    @objc func onClickRightButton() {
        self.delegate?.textViewBtnClicked?(self)
    }
}

extension TextFieldView: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let bool = delegate?.textFieldViewShouldBeginEditing?(self) , bool == false {
            return false
        }
        
        animateTextField(state: .selected)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if let bool = delegate?.textFieldViewShouldEndEditing?(self) , bool == false {
            return false
        }
        
        animateTextField(state: .normal)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldViewDidBeginEditing?(self)
        //        placeholderLabel.isHidden = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        delegate?.textFieldViewDidEndEditing?(self)
        if textField.text == ""{
            placeholderLabel.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 0 {
            self.placeholderLabel.isHidden = !string.isEmpty
        }
        if let bool = delegate?.textFieldView?(self, shouldChangeCharactersIn: range, replacementString: string) , bool == false {
            return false
        }
        
        let str = (textField.text ?? "") as NSString
        var strNew = str.replacingCharacters(in: range, with: string)
        
        if strNew.count > fieldType.maxCharacterLength {
            return false
        }
        
        //        if fieldType == CardFieldType.self {
        //
        //            let selectedRange = textField.selectedTextRange
        //
        //            strNew = strNew.replacingOccurrences(of: " ", with: "")
        //
        //            let count = strNew.count / 4
        //
        //            var finalString = ""
        //
        //            var offset = string.count
        //
        //            if count > 0 {
        //                for i in 1...count {
        //
        //                    let start = strNew.index(strNew.startIndex, offsetBy: (i-1)*4)
        //                    let end = strNew.index(strNew.startIndex, offsetBy: i*4)
        //                    finalString += strNew.substring(with: start..<end)
        //
        //                    if count < 4 {
        //                        finalString += " "
        //                    }
        //                }
        //
        //                if strNew.count % 4 == 0 && count < 4 {
        //                    offset += 1
        //                }
        //
        //                finalString += strNew.substring(from: strNew.index(strNew.startIndex, offsetBy: count*4))
        //
        //            } else {
        //                finalString = strNew
        //            }
        //
        //            fieldText = finalString
        //
        //            if let newPosition = textField.position(from: (selectedRange?.start)!, offset: offset) {
        //
        //                textField.selectedTextRange = textField.textRange(from: (newPosition), to: (newPosition))
        //            }
        //            return false
        //        }
        
        return true
    }
}

extension TextFieldView {
    public func enableFieldViewButton(_ enable:Bool = true,  showRightImage:Bool = false, rightIconImage: UIImage? = #imageLiteral(resourceName: "downArrowBlack")){
        button.isHidden = !enable
        textFieldObj.isUserInteractionEnabled = !enable
        rightImage = showRightImage ?  rightIconImage : nil
    }
    
    public func enableFieldViewButton(_ enable:Bool = true, buttonText:String?) {
        button.isHidden = !enable
        textFieldObj.isUserInteractionEnabled = !enable
        rightButton = buttonText
    }
    
    func isEmpty()->Bool{
        return !isHidden && (fieldText == nil || fieldText == "")
    }
    
    @IBAction func clickedTextFieldBtn(_ sender:UIButton){
        delegate?.textViewBtnClicked?(self)
    }
}
