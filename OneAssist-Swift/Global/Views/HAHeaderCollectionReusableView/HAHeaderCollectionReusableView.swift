//
//  HAHeaderCollectionReusableView.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 09/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class HAHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func initiliseTitle()
    {
        self.title.text = Strings.HASelectAppliance.headerTitle
    }
}
