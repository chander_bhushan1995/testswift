//
//  BankListHeaderView.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 27/09/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

class BankListHeaderView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var title: H3RegularBlackLabel!
    
    class func instanceFromNib() -> BankListHeaderView {
        return UINib(nibName: "BankListHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BankListHeaderView
    }

    class func viewHeight() -> CGFloat {
        return 72.0
    }
}
