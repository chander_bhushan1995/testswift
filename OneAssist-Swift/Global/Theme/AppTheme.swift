//
//  AppTheme.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 7/28/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import UIKit

final class AppTheme {
    
    static let global = AppTheme()
    
    private init() { }
    
    func setThemes() {
        setNavigationBarTheme()
        setTabBarTheme()
		setTextFieldTheme()
		setTableTheme()
        setSegmentControlTheme()
        setDropDownTheme()
        setDatePickerTheme()
    }
    
    private func setNavigationBarTheme() {
        let navigationAppearance = UINavigationBar.appearance()
        
        navigationAppearance.barTintColor = UIColor.white
        navigationAppearance.tintColor = UIColor.black
        navigationAppearance.isTranslucent = false
        navigationAppearance.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.darkGray,
            NSAttributedString.Key.font: DLSFont.h3.bold
        ]
        
        var backButtonBackgroundImage = #imageLiteral(resourceName: "navigateBack")
        backButtonBackgroundImage = backButtonBackgroundImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        navigationAppearance.backIndicatorImage = backButtonBackgroundImage
        navigationAppearance.backIndicatorTransitionMaskImage = backButtonBackgroundImage
        
        navigationAppearance.shadowImage = UIImage()
        
        // let navigationItemApp = UIBarButtonItem.appearance()
        
        // navigationAppearance.backItem?.backBarButtonItem = nil
        //navigationItemApp.title = ""
        // self.navigationItem.backBarButtonItem?.title = ""
    }
    
    private func setTabBarTheme() {
        let tabApp = UITabBar.appearance()
        tabApp.shadowImage = UIImage()
        tabApp.backgroundImage = UIImage()
        tabApp.barTintColor = UIColor.clear
    }
	
	private func setTextFieldTheme() {
		
		let textFieldObj = UITextField.appearance()
		textFieldObj.font = DLSFont.h3.regular
		textFieldObj.textColor = UIColor.darkGray
		textFieldObj.tintColor = UIColor.dodgerBlue
	}
	
	private func setTableTheme() {
		
		let tableViewObj = UITableView.appearance()
		tableViewObj.tableFooterView = UIView()
		tableViewObj.separatorInset = .zero
        
        let tableCellApp = UITableViewCell.appearance()
        tableCellApp.backgroundColor = UIColor.clear
        tableCellApp.contentView.backgroundColor = UIColor.clear
	}
    
    private func setSegmentControlTheme() {
        
        let segment = UISegmentedControl.appearance()
        segment.tintColor = UIColor.dodgerBlue
        segment.ensureiOS12Style()
    }
    
    private func setDropDownTheme() {
        let appearance = DropDown.appearance()
        appearance.cornerRadius = 4.0
        appearance.shadowColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.25)
        appearance.shadowOpacity = 0.25
        appearance.shadowRadius = 2.0
        appearance.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }

    private func setDatePickerTheme() {
        let appearance = UIDatePicker.appearance()
        appearance.calendar = Calendar(identifier: .gregorian)
    }
}
