//
//  VerticalDashView.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 16/09/19.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import UIKit

class VerticalDashView: UIView {
    
    override func awakeFromNib() {
         super.awakeFromNib()
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = UIColor.dashedViewColor.cgColor
        lineLayer.lineWidth = 1
        lineLayer.lineDashPattern = [3,3]
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0),
                                CGPoint(x: 0, y: self.bounds.height)])
        lineLayer.path = path
        self.layer.addSublayer(lineLayer)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
