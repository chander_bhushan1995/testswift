//
//  BaseResponseDTO.swift
//  OneAssistAlamoFire
//
//  Created by Sudhir.Kumar on 10/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation

class BaseResponseDTO: ParsableModel {
    var message: String?
    var responseMessage: String?
    var status: String?
    var statusCode: String?
    var error : [ApiResponseError]?
    var errors: [ApiResponseError]?
    var invalidData: [ApiResponseError]?
}


