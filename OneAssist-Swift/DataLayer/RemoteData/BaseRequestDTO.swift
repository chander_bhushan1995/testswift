//
//  BaseRequestDTO.swift
//  OneAssistAlamoFire
//
//  Created by Sudhir.Kumar on 10/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation

/// Base request model
class BaseRequestDTO {
    
    /// Convert object to dictionary
    ///
    /// - Returns: Dictionary of current DTO
    func createRequestParameters() -> [String: Any]? {
        return JSONParserSwift.getDictionary(object: self)
    }
}
