//
//  RemoteDataSource.swift
//  OneAssist-Swift
//
//  Created by Varun on 11/08/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseCrashlytics
typealias Authentication = (username: String, password: String)

class RemoteDataSource{
    
    let concurrentQueue = DispatchQueue(label: "remoteDataSource")
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    
    static func apiGatewayHeadersWithToken() -> [String : String] {
       return [
            Constants.headers.key.appIdKey: Constants.headers.value.appIdValue,
            Constants.headers.key.token : AppUserDefaults.sessionToken,
            Constants.headers.key.appVersion : UIApplication.appVersion
        ]
    }
    private func getEncryptionHeader(for request: Parameters?) -> String? {
        guard let parameters = request else {
            return nil
        }

        if JSONSerialization.isValidJSONObject(parameters) {
            if let data = try? JSONSerialization.data(withJSONObject: parameters, options: []), let jsonString = String(data: data, encoding: .utf8) {
                let stringToEncrypt = "PREFH1037H\(jsonString)POSTFH1037H"
                return sha256(string: stringToEncrypt)
            }
        }
        
        return nil
    }
    
    private func sha256(string : String) -> String? {
        guard let data = string.data(using: .utf8) else {
            return nil
        }
        
        var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA256($0, CC_LONG(data.count), &hash)
        }
        
        let finalString = hash.reduce("") { (result, value) -> String in
            result.appendingFormat("%02x", value)
        }
        
        return finalString
    }
    
    private func getHeader(for url: String, and request: Parameters?, with headers: [String: String] = RemoteDataSource.apiGatewayHeadersWithToken()) -> [String: String] {
        var headers = headers
//        if url.contains(Constants.URL.baseUrl) {
            if let checksum = getEncryptionHeader(for: request) {
                headers["checksum"] = checksum
            }
//        }
        
        headers["User-Agent"] = UAString()
        
        return headers
    }
    
    func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: [String: String]? = nil)

        -> DataRequest{
            registerBackgroundTask()
            return AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: HTTPHeaders(headers ?? [:]))
    }
    
    // Helper json get method
    func responseGet(url:String,
                     request: BaseRequestDTO?,
                     headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                     authenticate: Authentication? = nil,
                     completionHandler:@escaping(Data?,HTTPURLResponse?,Error?)->Void) -> Request? {
        
        let urlMain = URL(string: url)!
        let requestJSON = request?.createRequestParameters()
        let headers = getHeader(for: url, and: requestJSON, with: headers ?? [:])
        let dataReq = self.request(urlMain, method: .get, parameters: requestJSON, encoding: URLEncoding.default, headers: headers)
        
        dataReq.responseData(queue: concurrentQueue) { (response) in
            self.printRequestResponse(response: response)
            completionHandler(response.data, response.response, response.error)
        }
        
        return dataReq
    }
    
    // Helper json delete method
    func responseDelete(url:String,
                        request: BaseRequestDTO?,
                        headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                        authenticate: Authentication? = nil,
                        completionHandler:@escaping(Data?,HTTPURLResponse?,Error?)->Void) -> Request? {
        
        let urlMain = URL(string: url)!
        let requestJSON = request?.createRequestParameters()
        let headers = getHeader(for: url, and: requestJSON, with: headers ?? [:])
        let dataReq = self.request(urlMain, method: .delete, parameters: requestJSON, encoding: JSONEncoding.default, headers: headers)
        
        dataReq.responseData(queue: concurrentQueue) { (response) in
            self.printRequestResponse(response: response)

            completionHandler(response.data, response.response, response.error)
        }
        
        return dataReq
    }
    
    
    // Helper json post method
    func responsePost(url:String,

                      request: BaseRequestDTO?,
                      headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                      authenticate: Authentication? = nil,isForNodeApi:Bool? = nil,
                      completionHandler:@escaping(Data?,HTTPURLResponse?,Error?)->Void) -> Request? {
        
        let urlMain = URL(string: url)!
       
        var dataReq:DataRequest
        let requestJSON = request?.createRequestParameters()
        let headers = getHeader(for: url, and: requestJSON, with: headers ?? [:])
        if isForNodeApi == nil {
         dataReq = self.request(urlMain, method: .post, parameters: requestJSON, encoding: JSONEncoding.default, headers: headers)
        }
        else{
            var headers1 :[String:String] = [:]
            headers1 = [Constants.headers.key.contentType:Constants.headers.value.contentTypeValue]
            let parameter = request?.createRequestParameters()
            let jsonData = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            var dict:[String:String] = [:]
            dict["body"] = jsonString
            dataReq = self.request(urlMain, method: .post, parameters: dict , encoding: JSONEncoding.default, headers: headers1)
        }
        
        dataReq.responseData(queue: concurrentQueue) { (response) in
            self.printRequestResponse(response: response)

            completionHandler(response.data, response.response, response.error)
        }
        
        return dataReq 
    }
    
    // Helper json post method
    func responsePost(url:String,
                      body: Parameters?,
                      headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                      authenticate: Authentication? = nil,isForNodeApi:Bool? = nil,
                      completionHandler:@escaping(Data?,HTTPURLResponse?,Error?)->Void) -> Request? {
        
        let urlMain = URL(string: url)!
        
        var dataReq:DataRequest
//        let headers = getHeader(for: url, and: body, with: headers ?? [:])
        if isForNodeApi == nil {
            dataReq = self.request(urlMain, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
        }
        else{
            var headers1 :[String:String] = [:]
            headers1 = [Constants.headers.key.contentType:Constants.headers.value.contentTypeValue]
            let jsonData = try? JSONSerialization.data(withJSONObject: body, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            var dict:[String:String] = [:]
            dict["body"] = jsonString
            dataReq = self.request(urlMain, method: .post, parameters: dict , encoding: JSONEncoding.default, headers: headers1)
        }
        
        dataReq.responseData(queue: concurrentQueue) { (response) in
            self.printRequestResponse(response: response)
            
            completionHandler(response.data, response.response, response.error)
        }
        
        return dataReq
    }

    
    // Helper json put method
    func responsePut(url:String,
                     request: BaseRequestDTO?,
                     headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                     authenticate: Authentication? = nil,
                     completionHandler:@escaping(Data?,HTTPURLResponse?,Error?)->Void) -> Request? {
        
        let urlMain = URL(string: url)!
        let requestJSON = request?.createRequestParameters()
        let headers = getHeader(for: url, and: requestJSON, with: headers ?? [:])
        
        let dataReq = self.request(urlMain, method: .put, parameters: requestJSON, encoding: JSONEncoding.default, headers: headers)
        
        dataReq.responseData(queue: concurrentQueue) { (response) in
            self.printRequestResponse(response: response)

            completionHandler(response.data, response.response, response.error)
            
        }
        return dataReq
    }
    
    func logAPIErrors(response: AFDataResponse<Data>) throws{
        if NetworkReachabilityManager()?.isReachable ?? false {
            guard let error = response.error else {
                if let data = response.data {
                    let dict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
                    if let dicterror = dict?["error"], dicterror != nil {
                        var userInfo : [String: Any] = [:]
                        userInfo["ERROR"] = dicterror.debugDescription
                        if let urlString = response.response?.url?.absoluteString {
                            userInfo["URL"] = urlString
                        }
                        if let request = response.request {
                            if let headerDict = request.allHTTPHeaderFields {
                                userInfo["REQUESTHEADER"] = headerDict
                            }
                        }
                        if let responseHeaderDict = response.response?.allHeaderFields {
                            userInfo["RESPONSEHEADER"] = responseHeaderDict
                        }
                        let mainError = NSError(domain: "API_ERROR" , code: 9999, userInfo: userInfo)
                        Crashlytics.crashlytics().record(error: mainError)
                    }
                }
                return
            }
            
                var userInfo : [String: Any] = [:]
                userInfo["ERROR"] = error
                if let urlString = response.response?.url?.absoluteString {
                    userInfo["URL"] = urlString
                }
                if let request = response.request {
                    if let headerDict = request.allHTTPHeaderFields {
                        userInfo["REQUESTHEADER"] = headerDict
                    }
                }
                if let responseHeaderDict = response.response?.allHeaderFields {
                    userInfo["RESPONSEHEADER"] = responseHeaderDict
                }
                let mainError = NSError(domain: error.domain , code: error.code, userInfo: userInfo)
            Crashlytics.crashlytics().record(error: mainError)
        }
    }
    
    func printRequestResponse(response : AFDataResponse<Data>) {
        endBackgroundTask()
        do {
            try self.logAPIErrors(response: response)
        } catch  {
            
        }
        
        #if DEBUG
            if let urlString = response.response?.url?.absoluteString {
                Swift.print("URL: \(urlString)")
            }
            if let request = response.request {
                Swift.print("Request Method: \(request.httpMethod ?? "")")
                
                if let req = request.httpBody {
                    let dict = try? JSONSerialization.jsonObject(with: req, options: .allowFragments) as AnyObject
                    Swift.print("Request Body: ")
                    Swift.print(dict ?? [:])
                }
                
                if let headerDict = request.allHTTPHeaderFields {
                    Swift.print("Request Headers: ")
                    if let jsonData = try? JSONSerialization.data(withJSONObject: headerDict, options: .prettyPrinted), let decoded = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as AnyObject {
                        Swift.print(decoded)
                    } else {
                        Swift.print(headerDict)
                    }
                }
            }
            
            if let data = response.data {
                let dict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
                Swift.print("Response:")
                Swift.print(dict ?? [:])
            }
            if let responseHeaderDict = response.response?.allHeaderFields {
                Swift.print("Response Headers: ")
                if let jsonData = try? JSONSerialization.data(withJSONObject: responseHeaderDict, options: .prettyPrinted), let decoded = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as AnyObject {
                    Swift.print(decoded)
                } else {
                    Swift.print(responseHeaderDict)
                }
            }
        #endif
    }
    
    func responseMultipart(url:String,
                           request: BaseRequestDTO?,
                           headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                           multipartRequest: @escaping (Request) -> (),
                           completionHandler:@escaping(Data?,HTTPURLResponse?,Error?)->Void) {
        registerBackgroundTask()
        let requestJSON = request?.createRequestParameters()
        
        let headers = getHeader(for: url, and: requestJSON, with: headers ?? [:])
        let url = URL(string: url)!
        
        let request = AF.upload(multipartFormData: { (multipart) in
            
            
            
            // Create multipart request
            requestJSON?.keys.forEach({ (key) in
                if let data = requestJSON?[key] as? Data {
                    // Append the image files
                    multipart.append(data, withName: key, fileName: "sample.jpeg", mimeType: "image/jpeg")
                } else if let jsonDict = requestJSON?[key] as? [String: Any] {
                    // Append json datas
                    if let jsonData = try? JSONSerialization.data(withJSONObject: jsonDict) {
                        multipart.append(jsonData, withName: key)
                    }
                } else if let paramString = requestJSON?[key] as? String, let stringData = paramString.data(using: .utf8) {
                    // Append string params
                    multipart.append(stringData, withName: key)
                }
            })
            
        }, to: url, method: .post, headers: HTTPHeaders(headers))
        request.responseData { [weak self] (response) in
            guard let self = self else { return }
            switch response.result {
            case .success(_):
                
                // Return request
                multipartRequest(request)
                
                // Get response
                request.responseData(queue: self.concurrentQueue, completionHandler: { (response) in
                    self.printRequestResponse(response: response)
                    // Uploaded successfully, read the response.
                    completionHandler(response.data, response.response, response.error)
                })
                
                request.uploadProgress(closure: { (progress) in
                    // Handle upload progress here.
                })
                
            case .failure(let error):
                // Failure in creating request.
                completionHandler(nil, nil, error)
            }
            self.endBackgroundTask()
        }
    }
    
    
    func downloadFile(url:String, destinationUrl: String,
                           request: BaseRequestDTO?,
                           headers: [String: String]? = RemoteDataSource.apiGatewayHeadersWithToken(),
                           completionHandler:@escaping(String?,String?,Error?)->Void) {
        
        registerBackgroundTask()
        
        let requestJSON = request?.createRequestParameters()
        
        let header = getHeader(for: url, and: requestJSON, with: headers ?? [:])
        
        let destination: DownloadRequest.Destination = {_,_ in
            return (URL(string: destinationUrl)!, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        AF.download(url, headers: HTTPHeaders(header),to: destination)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }.responseData(queue: concurrentQueue, completionHandler: { (response) in
                completionHandler(String(response.response?.statusCode ?? 0), destinationUrl, response.error)
                self.endBackgroundTask()
            })
    }
    
}

typealias RemoteBackGroundTask = RemoteDataSource
extension RemoteBackGroundTask {
    
    
    func registerBackgroundTask() {
        print("Background task started.")
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = .invalid
    }
}
