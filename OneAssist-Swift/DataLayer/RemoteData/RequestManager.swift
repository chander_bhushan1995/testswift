//
//  RequestManager.swift
//  OneAssistAlamoFire
//
//  Created by Sudhir.Kumar on 10/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation

import Alamofire

protocol RequestManagerInterface {
    func addRequest(requestingScreenName : String, requestKey: String, requestObject : BaseRequestUseCaseProtocol ,cancelPrevious:Bool)
    func removeRequest(requestingScreenName : String, requestKey: String)
    func cancelRequest(requestingScreenName : String, requestKey: String)
    func cancelAll(requestingScreenName : String)
    
}

class RequestManager : RequestManagerInterface
{
    static let sharedInstance:RequestManagerInterface = RequestManager()
    private var requestDict : Dictionary<String, Dictionary<String, BaseRequestUseCaseProtocol>>?
    
    private  init() {
        self.requestDict = [String : [String : BaseRequestUseCaseProtocol]]()
    }
    
    func addRequest(requestingScreenName : String, requestKey: String, requestObject : BaseRequestUseCaseProtocol ,cancelPrevious:Bool = false)
    {
        if let dictRequest = self.requestDict?[requestingScreenName] {
            // If dict is present for screen name
            var requestDict = dictRequest
            if cancelPrevious == true {
                if let request = requestDict[requestKey] {
                    request.cancel()
                }
                requestDict.updateValue(requestObject, forKey: requestKey)
            }else {
                if requestDict[requestKey] == nil {
                    requestDict.updateValue(requestObject, forKey: requestKey)
                }
            }
            self.requestDict?.updateValue(requestDict, forKey: requestingScreenName)
        }else {
            var dictRequest = [String : BaseRequestUseCaseProtocol]()
            dictRequest.updateValue(requestObject, forKey: requestKey)
            self.requestDict?.updateValue(dictRequest, forKey: requestingScreenName)
        }
    }
    
    func removeRequest(requestingScreenName : String, requestKey: String)
    {
        if let dictRequest = self.requestDict?[requestingScreenName] {
            // If dict is present for screen name
            var requestDict = dictRequest
            if requestDict[requestKey] != nil {
                requestDict.removeValue(forKey: requestKey)
                if requestDict.count == 0 {
                    self.requestDict?.removeValue(forKey: requestingScreenName)
                }else {
                    self.requestDict?.updateValue(requestDict, forKey: requestingScreenName)
                }
            }
        }
    }
    
    func cancelRequest(requestingScreenName : String, requestKey: String)
    {
        if let dictRequest = self.requestDict?[requestingScreenName] {
            // If dict is present for screen name
            var requestDict = dictRequest
            if let request = requestDict[requestKey] {
                request.cancel()
                requestDict.removeValue(forKey: requestKey)
                if requestDict.count == 0 {
                    self.requestDict?.removeValue(forKey: requestingScreenName)
                }else {
                    self.requestDict?.updateValue(requestDict, forKey: requestingScreenName)
                }
            }
        }
        
    }
    
    func cancelAll(requestingScreenName : String)
    {
        let dictRequest = self.requestDict?[requestingScreenName];
        if dictRequest != nil {
            self.requestDict?.removeValue(forKey: requestingScreenName);
            for (_ , request) in dictRequest! {
                request.cancel()
            }
        }
    }
    
    
    
}
