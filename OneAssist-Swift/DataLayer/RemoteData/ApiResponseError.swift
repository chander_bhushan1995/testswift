//
//  ApiResponseError.swift
//  OneAssist-Swift
//
//  Created by Prateek Varshney on 03/04/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

class ApiResponseError : ParsableModel {
    var errorCode : String?
    var message : String?
    var errorMessage: String?
    var attributeCode : String?
}
