//
//  DocumentTypeEntity+CoreDataProperties.swift
//  
//
//  Created by Varun on 29/03/18.
//
//

import Foundation
import CoreData


extension DocumentTypeEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DocumentTypeEntity> {
        return NSFetchRequest<DocumentTypeEntity>(entityName: "DocumentTypeEntity")
    }

    @NSManaged public var srType: String?
    @NSManaged public var docTypeData: NSData?

}
