//
//  Offer+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 08/04/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//
//

import Foundation
import CoreData


extension Offer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Offer> {
        return NSFetchRequest<Offer>(entityName: "Offer")
    }

    @NSManaged public var bookmarked: Int32
    @NSManaged public var bookmarkId: String?
    @NSManaged public var brandImage: String?
    @NSManaged public var discount: String?
    @NSManaged public var endDate: String?
    @NSManaged public var merchantName: String?
    @NSManaged public var offerId: String?
    @NSManaged public var offerProvider: String?
    @NSManaged public var offerReference: String?
    @NSManaged public var outletCode: String?
    @NSManaged public var primaryId: String?
    @NSManaged public var syncStatus: Int32
    @NSManaged public var cardDetail: Issuer?
    @NSManaged public var cardDetailList: NSSet?

}

// MARK: Generated accessors for cardDetailList
extension Offer {

    @objc(addCardDetailListObject:)
    @NSManaged public func addToCardDetailList(_ value: Issuer)

    @objc(removeCardDetailListObject:)
    @NSManaged public func removeFromCardDetailList(_ value: Issuer)

    @objc(addCardDetailList:)
    @NSManaged public func addToCardDetailList(_ values: NSSet)

    @objc(removeCardDetailList:)
    @NSManaged public func removeFromCardDetailList(_ values: NSSet)

}
