//
//  UserCoreDataStore+CoreDataClass.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/30/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData

@objc(UserCoreDataStore)
public class UserCoreDataStore: NSManagedObject {
    
    static var currentUser: UserCoreDataStore? {
        
        let result: [UserCoreDataStore]? = CoreDataStack.sharedStack.fetchObject(predicateString: nil, inManagedObjectContext: CoreDataStack.sharedStack.mainContext)
        return result?.first
    }
    
    var custIds: [String]? {
        
        set(newValue) {
            userCustIds = newValue?.joined(separator: ",")
        } get {
            if userCustIds != nil {
                return userCustIds!.components(separatedBy: ",")
            } else {
                return nil
            }
        }
    }
}
