//
//  NotificationsCD+CoreDataClass.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData

@objc(NotificationsCD)
public class NotificationsCD: NSManagedObject {
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        self.setValue(Date(), forKey: "creationDate")
    }
}
