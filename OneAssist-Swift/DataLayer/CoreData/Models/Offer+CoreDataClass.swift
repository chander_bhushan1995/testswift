//
//  Offer+CoreDataClass.swift
//  
//
//  Created by Varun on 03/10/17.
//
//

import Foundation
import CoreData

@objc(Offer)
public class Offer: NSManagedObject {
    
    var bookmark: NSNumber? {
        set(newValue) {
            bookmarked = newValue?.int32Value ?? 0
        } get {
            return bookmarked as NSNumber
        }
    }
    var distance: String?
    var image: String? {
      return self.brandImage
    }
    var latitude: NSNumber?
    var longitude: NSNumber?
    var merchantLocation: String?
    var offerExpiringInDays: NSNumber?
    var offerCode: String? { return offerReference }
    var offerTitle: String? {
        return discount
    }
    var offerType: String?
}
