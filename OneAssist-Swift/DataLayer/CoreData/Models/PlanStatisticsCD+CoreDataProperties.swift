//
//  PlanStatisticsCD+CoreDataProperties.swift
//  
//
//  Created by Anand Kumar on 02/09/2019.
//
//

import Foundation
import CoreData


extension PlanStatisticsCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlanStatisticsCD> {
        return NSFetchRequest<PlanStatisticsCD>(entityName: "PlanStatisticsCD")
    }

    @NSManaged public var plan_code: String?
    @NSManaged public var rank: Int64
    @NSManaged public var metadata: StatisticsMetaDataCD?
}
