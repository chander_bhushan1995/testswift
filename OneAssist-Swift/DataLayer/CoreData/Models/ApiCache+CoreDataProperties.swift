//
//  ApiCache+CoreDataProperties.swift
//  
//
//  Created by Sudhir.Kumar on 21/08/17.
//
//

import Foundation
import CoreData


extension ApiCache {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ApiCache> {
        return NSFetchRequest<ApiCache>(entityName: "ApiCache")
    }

    @NSManaged public var name: String?
    @NSManaged public var cacheDate: Date?
}
