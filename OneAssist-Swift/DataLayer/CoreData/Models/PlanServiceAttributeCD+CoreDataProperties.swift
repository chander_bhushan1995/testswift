//
//  PlanServiceAttributeCD+CoreDataProperties.swift
//  
//
//  Created by Anand Kumar on 03/10/2019.
//
//

import Foundation
import CoreData


extension PlanServiceAttributeCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlanServiceAttributeCD> {
        return NSFetchRequest<PlanServiceAttributeCD>(entityName: "PlanServiceAttributeCD")
    }

    @NSManaged public var attributeName: String?
    @NSManaged public var attributeValue: String?
    @NSManaged public var serviceName: String?

}
