//
//  WHCServiceList+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData


extension WHCServiceList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WHCServiceList> {
        return NSFetchRequest<WHCServiceList>(entityName: "WHCServiceList")
    }

    @NSManaged public var serviceList: String?
    @NSManaged public var serviceListing: ProductServices?

}
