//
//  NotificationsCD+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 16/10/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData


extension NotificationsCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NotificationsCD> {
        return NSFetchRequest<NotificationsCD>(entityName: "NotificationsCD")
    }

    @NSManaged public var date: String?
    @NSManaged public var message: String?
    @NSManaged public var title: String?
    @NSManaged public var read: NSNumber?
    @NSManaged public var creationDate: Date?
    @NSManaged public var identifier: String?


    @NSManaged public var userInfo: Data?
    
    

}
