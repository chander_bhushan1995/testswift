//
//  OfferCategoryCD+CoreDataProperties.swift
//  
//
//  Created by Varun on 28/09/17.
//
//

import Foundation
import CoreData


extension OfferCategoryCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OfferCategoryCD> {
        return NSFetchRequest<OfferCategoryCD>(entityName: "OfferCategoryCD")
    }

    @NSManaged public var category: String?

}
