//
//  CustomerDetailsCoreDataStore+CoreDataClass.swift
//  
//
//  Created by Varun on 13/12/17.
//
//

import Foundation
import CoreData

@objc(CustomerDetailsCoreDataStore)
public class CustomerDetailsCoreDataStore: NSManagedObject {
    
    static var currentCustomerDetails: CustomerDetailsCoreDataStore? {
        
        let result: [CustomerDetailsCoreDataStore]? = CoreDataStack.sharedStack.fetchObject(predicateString: nil, inManagedObjectContext: CoreDataStack.sharedStack.mainContext)
        return result?.first
    }
}
