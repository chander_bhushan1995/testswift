//
//  PlanBenefitsCD+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData


extension PlanBenefitsCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlanBenefitsCD> {
        return NSFetchRequest<PlanBenefitsCD>(entityName: "PlanBenefitsCD")
    }

    @NSManaged public var benefit: String?
    @NSManaged public var benefitId: Int64
    @NSManaged public var benefitValue: String?
    @NSManaged public var shortName: String?
    @NSManaged public var planCode: Int64
    @NSManaged public var rank: Int64
}
