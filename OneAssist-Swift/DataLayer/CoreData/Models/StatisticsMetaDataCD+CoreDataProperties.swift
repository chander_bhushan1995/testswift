//
//  StatisticsMetaDataCD+CoreDataProperties.swift
//  
//
//  Created by Anand Kumar on 02/09/2019.
//
//

import Foundation
import CoreData


extension StatisticsMetaDataCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StatisticsMetaDataCD> {
        return NSFetchRequest<StatisticsMetaDataCD>(entityName: "StatisticsMetaDataCD")
    }

    @NSManaged public var membershipCount: Int64

}
