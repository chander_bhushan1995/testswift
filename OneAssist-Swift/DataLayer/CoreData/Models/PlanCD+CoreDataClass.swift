//
//  PlanCD+CoreDataClass.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData

@objc(PlanCD)
public class PlanCD: NSManagedObject {

}

extension PlanCD {
    func hasEW() -> Bool {
        if let productServiceArray = productServices?.allObjects as? [ProductServices] {
            for productService in productServiceArray {
                if let serviceList = productService.serviceList?.allObjects as? [WHCServiceList], serviceList.contains(where: {$0.serviceList == Constants.PlanServices.extendedWarranty})  {
                    return true
                }
            }
        }
        return false
    }
    
    func hasCreditScore() -> Bool {
        if let productServiceArray = productServices?.allObjects as? [ProductServices] {
            for productService in productServiceArray {
                if let serviceList = productService.serviceList?.allObjects as? [WHCServiceList], serviceList.contains(where: {$0.serviceList == Constants.PlanServices.creditScoreMonitoring}) {
                    return true
                }
            }
        }
        return false
    }
    
    func serviceList() -> String {
        return servicesListSet().joined(separator: ",")
    }
    
    func servicesListSet() -> Set<String> {
        var services = Set<String>()
        for productService in productServices ?? NSSet() {
            if let productService = productService as? ProductServices {
                for serviceCD in productService.serviceList ?? NSSet() {
                    if let service = serviceCD as? WHCServiceList, let serviceCode = service.serviceList, !serviceCode.isEmpty {
                        services.insert(serviceCode)
                    }
                }
            }
        }
        return services
    }
}
