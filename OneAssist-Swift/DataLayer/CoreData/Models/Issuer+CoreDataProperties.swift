//
//  Issuer+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ankur Batham on 08/04/20.
//  Copyright © 2020 OneAssist. All rights reserved.
//
//

import Foundation
import CoreData


extension Issuer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Issuer> {
        return NSFetchRequest<Issuer>(entityName: "Issuer")
    }

    @NSManaged public var bookmarkCard: Int64
    @NSManaged public var cardIssuerCode: String?
    @NSManaged public var cardIssuerId: String?
    @NSManaged public var cardIssuerName: String?
    @NSManaged public var cardTypeId: String?
    @NSManaged public var helpline1: String?
    @NSManaged public var helpline2: String?
    @NSManaged public var imagePath: String?
    @NSManaged public var landline1: String?
    @NSManaged public var landline2: String?
    @NSManaged public var ranking: Int64
    @NSManaged public var tagging: String?

}
