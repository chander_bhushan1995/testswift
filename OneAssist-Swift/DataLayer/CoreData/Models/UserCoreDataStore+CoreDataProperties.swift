//
//  UserCoreDataStore+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 8/30/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData


extension UserCoreDataStore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserCoreDataStore> {
        return NSFetchRequest<UserCoreDataStore>(entityName: "UserCoreDataStore")
    }

    @NSManaged public var cusId: String?
    @NSManaged public var mobileNo: String?
    @NSManaged public var sessionId: String?
    @NSManaged public var userName: String?
    @NSManaged public var userCustIds: String?
    @NSManaged public var activationCode: String?
}
