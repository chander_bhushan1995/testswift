//
//  PlanCD+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData


extension PlanCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlanCD> {
        return NSFetchRequest<PlanCD>(entityName: "PlanCD")
    }

    @NSManaged public var activationDefferedDays: Int64
    @NSManaged public var allowedMaxQuantity: Int64
    @NSManaged public var allowedUnits: Int64
    @NSManaged public var annuity: Bool
    @NSManaged public var coverAmount: Int64
    @NSManaged public var discountAllowed: Bool
    @NSManaged public var downgradeAllowed: Bool
    @NSManaged public var frequency: String?
    @NSManaged public var planDuration: String?
    @NSManaged public var graceDays: Int64
    @NSManaged public var isPremium: Bool
    @NSManaged public var maxInsuranceValue: Int64
    @NSManaged public var minInsuranceValue: Int64
    @NSManaged public var noOfCustomers: Int64
    @NSManaged public var planCode: Int64
    @NSManaged public var planDescription: String?
    @NSManaged public var planName: String?
    @NSManaged public var planStatus: String?
    @NSManaged public var price: Int64
    @NSManaged public var anchorPrice: Int64
    @NSManaged public var priceType: Int64
    @NSManaged public var refundAllowed: Bool
    @NSManaged public var renewalWindowDays: Int64
    @NSManaged public var taxcode: String?
    @NSManaged public var trial: Bool
    @NSManaged public var upgradeAllowed: Bool
    @NSManaged public var benefits: NSSet?
    @NSManaged public var excludedBenefits: NSSet?
    @NSManaged public var productServices: NSSet?
    @NSManaged public var planStatistics: PlanStatisticsCD?
    @NSManaged public var planServiceAttributes: NSSet?
    
    @NSManaged public var isWhc: Bool
}

// MARK: Generated accessors for benefits
extension PlanCD {

    @objc(addBenefitsObject:)
    @NSManaged public func addToBenefits(_ value: PlanBenefitsCD)

    @objc(removeBenefitsObject:)
    @NSManaged public func removeFromBenefits(_ value: PlanBenefitsCD)

    @objc(addBenefits:)
    @NSManaged public func addToBenefits(_ values: NSSet)

    @objc(removeBenefits:)
    @NSManaged public func removeFromBenefits(_ values: NSSet)

}

// MARK: Generated accessors for excludedBenefits
extension PlanCD {
    
    @objc(addExcludedBenefitsObject:)
    @NSManaged public func addToExcludedBenefits(_ value: PlanBenefitsCD)
    
    @objc(removeExcludedBenefitsObject:)
    @NSManaged public func removeFromExcludedBenefits(_ value: PlanBenefitsCD)
    
    @objc(addExcludedBenefits:)
    @NSManaged public func addToExcludedBenefits(_ values: NSSet)
    
    @objc(removeExcludedBenefits:)
    @NSManaged public func removeFromExcludedBenefits(_ values: NSSet)
    
}

// MARK: Generated accessors for productServices
extension PlanCD {

    @objc(addProductServicesObject:)
    @NSManaged public func addToProductServices(_ value: ProductServices)

    @objc(removeProductServicesObject:)
    @NSManaged public func removeFromProductServices(_ value: ProductServices)

    @objc(addProductServices:)
    @NSManaged public func addToProductServices(_ values: NSSet)

    @objc(removeProductServices:)
    @NSManaged public func removeFromProductServices(_ values: NSSet)

}

// MARK: Generated accessors for planServiceAttributes
extension PlanCD {
    
    @objc(addPlanServiceAttributesObject:)
    @NSManaged public func addToPlanServiceAttributes(_ value: PlanServiceAttributeCD)
    
    @objc(removePlanServiceAttributesObject:)
    @NSManaged public func removeFromPlanServiceAttributes(_ value: PlanServiceAttributeCD)
    
    @objc(addPlanServiceAttributes:)
    @NSManaged public func addToPlanServiceAttributes(_ values: NSSet)
    
    @objc(removePlanServiceAttributes:)
    @NSManaged public func removeFromPlanServiceAttributes(_ values: NSSet)
    
}
