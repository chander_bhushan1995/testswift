//
//  CustomerDetailsCoreDataStore+CoreDataProperties.swift
//  
//
//  Created by Varun on 13/12/17.
//
//

import Foundation
import CoreData


extension CustomerDetailsCoreDataStore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CustomerDetailsCoreDataStore> {
        return NSFetchRequest<CustomerDetailsCoreDataStore>(entityName: "CustomerDetailsCoreDataStore")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var gender: String?
    @NSManaged public var addressLine1: String?
    @NSManaged public var addressLine2: String?
    @NSManaged public var cityName: String?
    @NSManaged public var stateName: String?
    @NSManaged public var pincode: String?
    @NSManaged public var cityCode: String?
    @NSManaged public var stateCode: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var email: String?

}
