//
//  ProductServices+CoreDataProperties.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import CoreData


extension ProductServices {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProductServices> {
        return NSFetchRequest<ProductServices>(entityName: "ProductServices")
    }

    @NSManaged public var productCode: String?
    @NSManaged public var productServices: PlanCD?
    @NSManaged public var serviceList: NSSet?

}

// MARK: Generated accessors for serviceList
extension ProductServices {

    @objc(addServiceListObject:)
    @NSManaged public func addToServiceList(_ value: WHCServiceList)

    @objc(removeServiceListObject:)
    @NSManaged public func removeFromServiceList(_ value: WHCServiceList)

    @objc(addServiceList:)
    @NSManaged public func addToServiceList(_ values: NSSet)

    @objc(removeServiceList:)
    @NSManaged public func removeFromServiceList(_ values: NSSet)

}
