//
//  CoreData+ApiCache.swift
//  TestCoreData
//
//  Created by Sudhir.Kumar on 21/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation
import CoreData

extension CoreDataStack {
    
    func checkDataIsExpiredInCache(apiCacheChecker : ApiCacheChecker) -> Bool {
        
        let arrManagedObject: [ApiCache]? = CoreDataStack.sharedStack.fetchObject(predicateString: "name == '\(apiCacheChecker.getApiName)'" , inManagedObjectContext: CoreDataStack.sharedStack.mainContext)
        
        if arrManagedObject?.count == 1 {
            
            let managedObject = arrManagedObject?[0]
            
            let timeInterval = Date.currentDate().timeIntervalSince(((managedObject?.cacheDate) as Date?)!)

            if  timeInterval > apiCacheChecker.getCacheTimeDuration {
                return true
            }
        }
        
        return false
    }
    
    func removeApiDataPresentInCache(apiCacheChecker : ApiCacheChecker) -> Bool {
        
        let arrManagedObject: [ApiCache]? = CoreDataStack.sharedStack.fetchObject(predicateString: "name == '\(apiCacheChecker.getApiName)'" , inManagedObjectContext: CoreDataStack.sharedStack.mainContext)
        
        if arrManagedObject?.count == 1 {
            
            let managedObject = arrManagedObject?[0]
            CoreDataStack.sharedStack.mainContext.delete(managedObject!)
            CoreDataStack.sharedStack.saveMainContext()
            
        }
        
        return false
    }
    
    func insertApiCacheData(apiCacheChecker : ApiCacheChecker) {
        let arrManagedObject: [ApiCache]? = CoreDataStack.sharedStack.fetchObject(predicateString: "name == '\(apiCacheChecker.getApiName)'" , inManagedObjectContext: CoreDataStack.sharedStack.mainContext)
        
        if arrManagedObject?.count == 1 {
            let managedObject = arrManagedObject?[0]
            managedObject?.cacheDate = Date.currentDate()
        }
        else if arrManagedObject?.count == 0 {            
            let managedObject = ApiCache(context: CoreDataStack.sharedStack.mainContext)
            managedObject.cacheDate = Date.currentDate()
            managedObject.name = apiCacheChecker.getApiName.description()
        }
        
        CoreDataStack.sharedStack.saveMainContext()
    }
    
    func fetchApiCacheData<T: NSManagedObject>(apiCacheChecker : ApiCacheChecker, predicate: String? = nil) -> [T]? {
        
        if let result: [ApiCache] = CoreDataStack.sharedStack.fetchObject(predicateString: "name == '\(apiCacheChecker.getApiName.description())'" , inManagedObjectContext: CoreDataStack.sharedStack.mainContext), result.count > 0, let date = result[0].cacheDate {
            
            let timeInterval = Date.currentDate().timeIntervalSince(date)
            
            if  timeInterval < apiCacheChecker.getCacheTimeDuration {
                
                let entities: [T]? = CoreDataStack.sharedStack.fetchObject(predicateString: predicate, inManagedObjectContext: CoreDataStack.sharedStack.mainContext)
                return entities
            }
        }
        return nil
    }
}
