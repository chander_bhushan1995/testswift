//
//  StringTransformer.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 03/01/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import UIKit

@objc class StringTransformer: ValueTransformer {
    override class func transformedValueClass() -> AnyClass {
        return NSData.self
    }
    
    override class func allowsReverseTransformation() -> Bool {
        return true
    }
    
    override func reverseTransformedValue(_ value: Any?) -> Any? {
        guard let data = value as? Data else {
            return nil
        }
        
        return String(data: data.xor(), encoding: .utf8)
    }
    
    override func transformedValue(_ value: Any?) -> Any? {
        guard let string = value as? String else {
            return nil
        }
        
        return string.data(using: .utf8)?.xor()
    }
}

extension Data {
    func xor() -> Data {
        let xorKey = "1Assist!123".data(using: .utf8) ?? Data()
        var data = self
        for i in 0..<data.count {
            data[i] ^= xorKey[i % xorKey.count]
        }
        
        return data
    }
}
