//
//  ApiCache+Strategy.swift
//  TestCoreData
//
//  Created by Sudhir.Kumar on 21/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation
import CoreData

protocol ApiCacheChecker {
    var getCacheTimeDuration:TimeInterval { get }
    var getApiName:NSManagedObject.Type { get }
}

let CacheTimeDuration:TimeInterval = 24*60*60*2 // 2 days

// TODO: KAG remove issuers
class IssuerCacheChecker: ApiCacheChecker {
    var getCacheTimeDuration: TimeInterval = CacheTimeDuration
    var getApiName: NSManagedObject.Type = Issuer.self
}

class OfferCategoriesCacheChecker: ApiCacheChecker {
    var getCacheTimeDuration: TimeInterval = CacheTimeDuration
    var getApiName: NSManagedObject.Type = OfferCategoryCD.self
}

class BookmarkedOffersCacheChecker: ApiCacheChecker {
    var getCacheTimeDuration: TimeInterval = CacheTimeDuration
    var getApiName: NSManagedObject.Type = Offer.self
}

class SuggestPlanCacheChecker: ApiCacheChecker {
    var getCacheTimeDuration: TimeInterval = 0
    var getApiName: NSManagedObject.Type = PlanCD.self
}

class DocumentTypeCacheChecker: ApiCacheChecker {
    var getCacheTimeDuration:TimeInterval = CacheTimeDuration
    var getApiName:NSManagedObject.Type = DocumentTypeEntity.self
}

class CustomerDetailsCacheChecker: ApiCacheChecker {
    var getCacheTimeDuration:TimeInterval = 60*60 // 1 hour
    var getApiName:NSManagedObject.Type = CustomerDetailsCoreDataStore.self
}
