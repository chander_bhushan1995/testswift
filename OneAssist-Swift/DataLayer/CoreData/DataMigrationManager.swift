//
//  DataMigrationManager.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 06/01/2020.
//  Copyright © 2020 OneAssist. All rights reserved.
//

import CoreData
import FirebaseCrashlytics

class DataMigrationManager {
    
    let modelName: String
    let storeURL: URL
    let modelVersions: [String]
    
    init(modelName: String, storeURL: URL, modelVersions: [String]) {
        self.modelName = modelName
        self.storeURL = storeURL
        self.modelVersions = modelVersions
    }
    
    func checkAndPerformMigration() {
        if !NSManagedObjectModel.isModel(modelName, compatibleWithStoreAt: storeURL) {
            performMigration()
        }
    }
    
    func performMigration() {
        guard let storeModel = NSManagedObjectModel.storeModelForUrl(storeURL, modelName: modelName), let currentModelIndex = modelVersions.firstIndex(where: { storeModel.isVersion(modelName, modelVersionName: $0) }) else {
            let wrappedError = NSError(domain: "CORE_DATA_ERROR", code: 110011, userInfo: [NSLocalizedDescriptionKey: "Store data model is not compatible with any application model"])
            Crashlytics.crashlytics().record(error: wrappedError)
            print("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            handleFailedMigration()
            return
        }
        guard currentModelIndex < modelVersions.count - 1 else {
            print("Does not need migration")
            return
        }
        
        for modelIndex in currentModelIndex...(modelVersions.count-2) {
            migrateStoreAt(URL: storeURL, fromModel: .dataModel(named: modelName, modelVersionName: modelVersions[modelIndex]), toModel: .dataModel(named: modelName, modelVersionName: modelVersions[modelIndex+1]))
        }
    }
    
    func handleFailedMigration() {
        Utilities.logoutAndDeleteDB()
        let targetURL = storeURL.deletingLastPathComponent()
        let shmURL = targetURL.appendingPathComponent(storeURL.lastPathComponent + "-shm")
        let walURL = targetURL.appendingPathComponent(storeURL.lastPathComponent + "-wal")
                
        do {
            let fileManager = FileManager.default
            try fileManager.removeItem(at: storeURL)
            try fileManager.removeItem(at: shmURL)
            try fileManager.removeItem(at: walURL)
        } catch {
            print("Migration failed: \(error)")
        }
    }
    
    private func migrateStoreAt(URL storeURL: URL, fromModel from:NSManagedObjectModel, toModel to:NSManagedObjectModel) {
        // 1
        let migrationManager = NSMigrationManager(sourceModel: from, destinationModel: to)
        var mappingModel = NSMappingModel(from: nil, forSourceModel: from, destinationModel: to)
        if mappingModel == nil {
            do{
               mappingModel = try NSMappingModel.inferredMappingModel(forSourceModel: from, destinationModel: to)
            }catch {
                
            }
        }
        
        let targetURL = storeURL.deletingLastPathComponent()
        let destinationName = storeURL.lastPathComponent + "~1"
        let destinationURL = targetURL.appendingPathComponent(destinationName)
        let shmURL = targetURL.appendingPathComponent(storeURL.lastPathComponent + "-shm")
        let walURL = targetURL.appendingPathComponent(storeURL.lastPathComponent + "-wal")
        
        print("From Model: \(from.entityVersionHashesByName)")
        print("To Model: \(to.entityVersionHashesByName)")
        print("Migrating store \(storeURL) to \(destinationURL)")
        print("Mapping model: \(String(describing: mappingModel))")
        
        // 2
        do {
            try migrationManager.migrateStore(from: storeURL, sourceType: NSSQLiteStoreType, options: nil, with: mappingModel, toDestinationURL: destinationURL, destinationType: NSSQLiteStoreType, destinationOptions: nil)
            let fileManager = FileManager.default
            try fileManager.removeItem(at: storeURL)
            try fileManager.removeItem(at: shmURL)
            try fileManager.removeItem(at: walURL)
            try fileManager.moveItem(at: destinationURL, to: storeURL)
            print("Migration Completed Successfully")
        } catch {
            print("Migration failed: \(error)")
        }
    }
}

extension NSManagedObjectModel {
    
    func isVersion(_ modelName: String, modelVersionName: String) -> Bool { return self == .dataModel(named: modelName, modelVersionName: modelVersionName) }
    
    class func dataModel(named modelName: String, modelVersionName: String) -> NSManagedObjectModel {
        return modelURLs(in: modelName).first(where: { $0.lastPathComponent == "\(modelVersionName).mom" }).flatMap(NSManagedObjectModel.init) ?? NSManagedObjectModel()
    }
    
    class func storeModelForUrl(_ url: URL, modelName: String) -> NSManagedObjectModel? {
        return modelURLs(in: modelName).compactMap(NSManagedObjectModel.init).first(where: { $0.isCompatibleWithStoreAt(url)})
    }
    
    class func isModel(_ modelName: String, compatibleWithStoreAt url: URL) -> Bool {
        return model(named: modelName).isCompatibleWithStoreAt(url)
    }
    
    private func isCompatibleWithStoreAt(_ url: URL) -> Bool {
        let storeMetadata = (try? NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: url, options: nil)) ?? [:]
        return isConfiguration(withName: nil, compatibleWithStoreMetadata: storeMetadata)
    }
    
    private class func modelURLs(in modelFolder: String) -> [URL] {
        return Bundle.main.urls(forResourcesWithExtension: "mom", subdirectory:"\(modelFolder).momd") ?? []
    }
    
    private class func model(named modelName: String) -> NSManagedObjectModel {
        return Bundle.main.url(forResource: modelName, withExtension: "momd").flatMap(NSManagedObjectModel.init) ?? NSManagedObjectModel()
    }
}
