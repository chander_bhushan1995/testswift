//
//  CoreData.swift
//  CoreData
//
//  Created by Sudhir.Kumar on 14/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation
import CoreData
import FirebaseCrashlytics

let cdModelName: String = "OneAssistCoreData"
let cdStoreName: String = "SingleViewCoreData"
let cdModelVersions: [String] = ["OneAssistCoreData", "OneAssistCoreData_v2"]
let cdStoreURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!.appendingPathComponent("\(cdStoreName).sqlite")

class CoreDataStack {
    
    static let sharedStack = CoreDataStack()
    
    private init () {
        if !RemoteConfigManager.shared.migrateDataInAppDelegate {
            DataMigrationManager(modelName: cdModelName, storeURL: cdStoreURL, modelVersions: cdModelVersions).checkAndPerformMigration()
        }
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        print(cdStoreURL)
        let description = NSPersistentStoreDescription(url: cdStoreURL)
        description.shouldMigrateStoreAutomatically = true
        description.shouldInferMappingModelAutomatically = true
        
        let container = NSPersistentContainer(name: cdModelName)
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: [NSLocalizedDescriptionKey: "Failed to initialize the application's saved data", NSLocalizedFailureReasonErrorKey: "There was an error creating or loading the application's saved data.", NSUnderlyingErrorKey: error])
                Crashlytics.crashlytics().record(error: wrappedError)
                print("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var mainContext: NSManagedObjectContext = {
        let viewContext = persistentContainer.viewContext
        viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        viewContext.undoManager = nil
        viewContext.shouldDeleteInaccessibleFaults = true
        viewContext.automaticallyMergesChangesFromParent = true
        
        return persistentContainer.viewContext
    }()
    
    lazy var secondaryContext: NSManagedObjectContext = {
        let tempContext = persistentContainer.newBackgroundContext()
        tempContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        tempContext.undoManager = nil
        return tempContext
    }()
    
    func saveMainContext(completion: (()->Void)? = nil) -> Void {
        self.mainContext.perform {
            var t = clock()
            self.save(manageObjectContext: self.mainContext)
            t = clock() - t
            print("Saving MainContext function takes \(t) ticks, which is \(Double(t) / Double(CLOCKS_PER_SEC)) seconds of CPU time")
            DispatchQueue.main.async {
                completion?()
            }
        }
    }
    
    func saveContext(context:NSManagedObjectContext, completion: (()->Void)? = nil) {
        context.perform {
            var t = clock()
            self.save(manageObjectContext: context)
            t = clock() - t
            print("The function takes \(t) ticks, which is \(Double(t) / Double(CLOCKS_PER_SEC)) seconds of CPU time")
            self.saveMainContext(completion: completion)
        }
    }
    
    private func save(manageObjectContext:NSManagedObjectContext) {
        guard manageObjectContext.hasChanges else { return }
        do {
            print("\(Thread.current)")
            try manageObjectContext.save()
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
    
    func fetchObject<T: NSManagedObject>(predicateString predicate: String?, inManagedObjectContext context: NSManagedObjectContext, argumentList: [Any]? = nil) -> [T]? {
        let fetch = NSFetchRequest<T>(entityName: NSStringFromClass(T.self))
        if let predicate = predicate {
            fetch.predicate = NSPredicate(format: predicate, argumentArray: argumentList)
        }
        var result: [T]?
        do {
            result = try context.fetch(fetch)
        } catch {
            Crashlytics.crashlytics().record(error: NSError(domain: "FETCH_FROM_COREDATA", code: 9997, userInfo: [NSUnderlyingErrorKey: error as NSError]))
        }
        return result
    }
    
    func deleteEntities(_ entities : [String] = []) {
        let entityNames = entities.count == 0 ? persistentContainer.managedObjectModel.entities.map {$0.name!} : entities
        for entity in entityNames {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: entity))
            do {
                try mainContext.execute(deleteRequest)
                try mainContext.save()
            } catch {
                print ("There was an error")
            }
        }
    }
}

extension NSManagedObject {
    class func getCount(predicateString : String?, inManagedObjectContext context: NSManagedObjectContext) -> Int? {
        let fetch = fetchRequest()
        if let predicate = predicateString {
            fetch.predicate = NSPredicate(format: predicate, argumentArray: nil)
        }
        do {
            return try context.count(for: fetch)
        } catch {
            
            Crashlytics.crashlytics().record(error: NSError(domain: "GETCOUNT_FROM_COREDATA", code: 9998, userInfo: [NSUnderlyingErrorKey: error as NSError]))
        }
        return nil;
    }
}
