//
//  DataProviderFactory.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/18/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

final class ResponseDataProviderFactory: DataProviderFactory {
    
    static let shared = ResponseDataProviderFactory()
    
    private init() { }
    
    var authenticationDataProvider = { () -> AuthenticationDataProvider in
        return AuthenticationRemoteDataProvider()
    }
    
    var commonDataProvider = { () -> CommonDataProvider in
        return CommonResponseDataProvider()
    }
    
    var cardGenieDataProvider = { () -> CardGenieDataProvider in
        return CardGenieResponseDataProvider()
    }
    
    var walletDataProvider = { () -> WalletDataProvider in
        return WalletResponseDataProvider()
    }
    
    var whcDataProvider = { () -> WHCDataProvider in
        return WHCResponseDataProvider()
    }
    
    var remoteConfigDataProvider = { () -> RemoteConfigDataProvider in
        return RemoteConfigResponseDataProvider()
    }
    
    var homeAppliancesDataProvider = { () -> HomeAppliancesDataProvider in
        return HomeAppliancesResponseDataProvider()
    }
    
    var claimDataProvider = { () -> ClaimDataProvider in
        return ClaimResponseDataProvider()
    }
}
