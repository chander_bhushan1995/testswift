//
//  WalletResponseDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/5/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

class WalletResponseDataProvider: DataProvider, WalletDataProvider {
    private let remoteDataSource = RemoteDataSource()
    
    func getWalletCards(requestDto: WalletCardsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WalletCardsResponseDTO?, Error?)->Void) {
        
        let url = Constants.WalletTabUrls.walletCards + (requestDto?.customerId ?? "") + "/cards?status=A"
        
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func downloadFile(requestDto: DownloadRequestDTO?, taskCallBack:@escaping (_ task : Request?)-> Void, completionHandlerBlock:@escaping(DownloadResponseDTO?, Error?)->Void) {
        
        let url = requestDto?.downloadUrl ?? ""
        
        remoteDataSource.downloadFile(url: url,destinationUrl: requestDto?.destinationUrl ?? "", request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (statusCode, destinationUrl, error) in
            let downloadRes = DownloadResponseDTO(dictionary: [:])
            downloadRes.statusCode = statusCode
            downloadRes.destinationUrl = destinationUrl
            completionHandlerBlock(downloadRes, error)
        }
    }
    
    func getRaiseClaimDocumentRequest(requestDto: DownloadRequestDTO?, taskCallBack:@escaping (_ task : Request?)-> Void, completionHandlerBlock:@escaping(DownloadResponseDTO?, Error?)->Void) {
        guard let url = requestDto?.downloadUrl else {
            completionHandlerBlock(nil, nil)
            return
        }
        
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            
            if let imageData = data {
                let desc = String(data: imageData, encoding: .utf8)
                let imageData = Data.init(base64Encoded: desc ?? "") ?? Data()
                
               var fileName = requestDto?.fileName ?? "default"
                
                if let responseHeaderDict = urlResponse?.allHeaderFields {
                    var extensions : String = responseHeaderDict["Content-Disposition"] as? String ?? "filename=default.JPG"
                    extensions = extensions.components(separatedBy: "filename=").last?.components(separatedBy: ".").last ?? "JPG"
                    fileName = fileName + "." + extensions
                    
                }else {
                    fileName = fileName + ".JPG"
                }
                
                let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let destinationUrl = documentsUrl.appendingPathComponent(fileName)
                
                do {
                    try imageData.write(to: destinationUrl)
                } catch {
                    print(error.localizedDescription)
                }
                
                let response = GetClaimDocumentResponseDTO(dictionary: [:])
                response.status = Constants.ResponseConstants.success
                response.image = UIImage(data: imageData)
                
                let downloadRes = DownloadResponseDTO(dictionary: [:])
                downloadRes.statusCode = "200"
                downloadRes.destinationUrl = destinationUrl.absoluteString
                
                completionHandlerBlock(downloadRes, nil)
                
            } else {
                completionHandlerBlock(nil, nil)
            }
        }
        taskCallBack(request)
    }
    
    func getSIStatusOfIDFencePremium(requestDto: MembershipSIStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(MembershipSIStatusResponseDTO?, Error?)->Void) {
        
        let url = String(format: "\(Constants.WalletTabUrls.siStatusURL)\(requestDto?.memUUID ?? "")")
        
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getSubscriptionStatusOfIDFence(requestDto: IDFenceSubscriptionStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(IDFenceSubscriptionStatusResponseDTO?, Error?)->Void) {
        
        let url = Constants.WalletTabUrls.subscriptionStatusURL + (requestDto?.memUUID ?? "")
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
}
