//
//  DataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/16/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation

enum ParsingError: LocalizedError {
    case emptyResponse
    
    var errorDescription: String? {
        return "Response is empty"
    }
}

protocol ResponseFilter {
    func filter<Response: BaseResponseDTO>(response: Response) -> Response?
}

protocol DataProvider {
}

extension DataProvider {
    private func parseResponse<ResponseDTOType: BaseResponseDTO>(data: Data?) throws -> ResponseDTOType {
        do {
            if let data = data {
                let parsedResponse: ResponseDTOType = try JSONParserSwift.parse(data: data)
                return parsedResponse
            } else {
                throw ParsingError.emptyResponse
            }
        } catch {
            throw error
        }
    }
   
    func handleResponse<ResponseDTOType: BaseResponseDTO>(data: Data?, urlResponse: HTTPURLResponse?, error: Error?, completionHandlerBlock: @escaping (ResponseDTOType?, Error?)->Void) {
        
        if error != nil {
            DispatchQueue.main.async {
                completionHandlerBlock(nil, error)
            }
            
        } else {
            do {
                
                let response: ResponseDTOType = try self.parseResponse(data: data)
                
                if response is SearchServiceRequestPlanResponseDTO ,let responseDTO = response as? SearchServiceRequestPlanResponseDTO , let data = data {
                    let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dictionary = dictionary as? [String: Any] {
                        if let dataArrray = dictionary["data"] as? [Any],dataArrray.count != 0{
                            if dataArrray[0] is NSNull {  //will check if received data is <null>
                                completionHandlerBlock(nil, ParsingError.emptyResponse)
                                return
                            }
                            print(dataArrray[0])
                            do {
                                let jsonData = try JSONSerialization.data(withJSONObject: dataArrray[0])
                                if let jsonString = String(data: jsonData, encoding: .utf8) {
                                   responseDTO.sendJSData = jsonString
                                    print(jsonString)
                                }
                                print(JSONParserSwiftError.cannotConvertToString)
                            } catch {
                                print(error)
                            }
                        }
                    }
                }
                
                if let token = urlResponse?.allHeaderFields[Constants.headers.key.token] as? String {
                    AppUserDefaults.sessionToken = token
                }
                
               /*
                 if let cookies = urlResponse?.allHeaderFields["Set-Cookie"] as? String {
                    UserDefaultsKeys.cookie.set(value: cookies)
                }
                 */
                
                DispatchQueue.main.async {
                    if let filter = Utilities.getResponseFilter() {
                        if let filteredResponse = filter.filter(response: response) {
                            completionHandlerBlock(filteredResponse, error)
                        }
                    } else {
                        completionHandlerBlock(response, error)
                    }
                }
            } catch {
                DispatchQueue.main.async {
                    completionHandlerBlock(nil, error)
                }
            }
        }
    }
    
    func handleResponse(data: Data?, urlResponse: HTTPURLResponse?, error: Error?, completionHandlerBlock: @escaping (Any?, Error?) -> Void) {
        if error != nil {
            DispatchQueue.main.async {
                completionHandlerBlock(nil, error)
            }
        } else {
            var response: Any? = nil
            if let data = data {
                response = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            }

            if let token = urlResponse?.allHeaderFields[Constants.headers.key.token] as? String {
                AppUserDefaults.sessionToken = token
            }
            
            /*
             if let cookies = urlResponse?.allHeaderFields["Set-Cookie"] as? String {
                UserDefaultsKeys.cookie.set(value: cookies)
            }
 */

            DispatchQueue.main.async {
                completionHandlerBlock(response, error)
            }
        }
    }
    
    func handleResponse<ResponseDTOType: BaseResponseDTO>(data: Data?) -> ResponseDTOType? {
        let reponseObject: ResponseDTOType? = try? self.parseResponse(data: data)
        return reponseObject
    }
}
