//
//  ClaimResponseDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 2/21/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class ClaimResponseDataProvider: DataProvider, ClaimDataProvider {
    
    private let remoteDataSource = RemoteDataSource()
    
    func checkPincodeServiciability(requestDto: WHCPincodeRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandler completionHandlerBlock: @escaping(WHCPincodeResponseDTO?, Error?)->Void){
        let completeUrl = "\(Constants.WHC.URL.pincodeAndAdressURL)\(UserCoreDataStore.currentUser?.cusId ?? "")/address?pinCode=\(requestDto?.pincode ?? "")"
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    func updateAlternateMobileEmail(requestDto: MobileEmailRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (MobileEmailResponseDTO?, Error?) -> Void) {
        
        let completeUrl = Constants.WHC.URL.updateMobileEmail
        let request = remoteDataSource.responsePut(url: completeUrl, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }

    func wantDeviceBack(requestDto: DeviceBackRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (DeviceBackResponseDTO?, Error?) -> Void)
    {
        let completeUrl = Constants.WHC.URL.updateMobileEmail
        let request = remoteDataSource.responsePut(url: completeUrl, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    func payLater(requestDto: PayLaterRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (PayLaterResponseDTO?, Error?) -> Void){
        let completeUrl = Constants.WHC.URL.updateMobileEmail
        let request = remoteDataSource.responsePut(url: completeUrl, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    func getServiceCenterList(requestDto: ServiceCenterListRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ServiceCenterListResponceDTO?, Error?) -> Void) {
        let completeUrl = "\(Constants.WHC.URL.serviceCenterListURL)isWalkIn=Y&pincode=\(requestDto?.pincode ?? "" )"
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    func getcityCode(requestDto: ServiceCenterListRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (CityCodeResponseDTO?, Error?) -> Void) {
        let completeUrl = "\(Constants.WHC.URL.cityCodeURL)" + (requestDto?.pincode ?? "")
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getServiceCenterDistance(requestDto: GoogleDistanceRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (GoogleDistanceResponseDTO?, Error?) -> Void){
        let completeUrl = "\(Constants.Urls.googleMapDistance)" + "?origins=\(requestDto?.origins ?? "")&destinations=\(requestDto?.destinations ?? "")&key=\(requestDto?.key ?? "")"
        let formatedURL = completeUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        
        let request = remoteDataSource.responseGet(url: formatedURL ?? "", request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getJSTimelineCard(data: JavaScriptTimeineRequestDTO?, completionHandler: @escaping (JavaScriptTimeineResponceDTO?, Error?) -> Void) {
        self.handleResponse(data: data?.timeLineData, urlResponse: nil, error: nil, completionHandlerBlock: completionHandler)
    }
    
    func createServiceRequestPlan(requestDto: CreateServiceRequestPlanRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (CreateServiceRequestPlanResponseDTO?, Error?) -> Void) {
        
        let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/v2/create"
        
        let request = remoteDataSource.responsePost(url: completeUrl, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        
        taskCallBack(request)
    }

    func updateServiceRequestPlan(requestDto: UpdateServiceRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (UpdateServiceRequestResponseDTO?, Error?) -> Void) {

        let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/v2/update/submission/sr"

        let request = remoteDataSource.responsePut(url: completeUrl, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }

        taskCallBack(request)
    }
    
    func getMembershipAssetDetails(requestDto: GetMembershipAssetListRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetMembershipAssetListResponseDTO?, Error?) -> Void) {
        //  let completeE = "\(Constants.Claims.URL.membershipAssetDetail)\(requestDto?.membershipId)"
        let completeUrl = "\(Constants.Claims.URL.membershipAssetDetail)\(String(describing: requestDto?.membershipId))"
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func whatHappend(requestDto: WhatHappendRequestDTO, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (WhatHappendResponseDTO?, Error?) -> Void) {
        var completeUrl = "\(Constants.Claims.URL.whatHappend)\(requestDto.productCode ?? "")\(Constants.Claims.taskType)\(requestDto.taskType ?? "")\(Constants.Claims.status)\(requestDto.status )"
        if let productVariantId = requestDto.productVariantId {
            let allVariantIDs = productVariantId
            completeUrl += "\(Constants.Claims.productVariantId)\(allVariantIDs)"
        }else if let productVariantIds = requestDto.productVariantIds{ // in case of sod only
            let allVariantIDs = productVariantIds.joined(separator: ",")
            completeUrl += "\(Constants.Claims.productVarientIds)\(allVariantIDs)"
        } else {
            completeUrl += "\(Constants.Claims.options)\(Strings.Global.distinctByName)"
        }
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }

    
    func getFeedbackRequest(requestDto: SearchServiceRequestPlanRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (SearchServiceRequestPlanResponseDTO?, Error?) -> Void) {
        let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests"
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func searchServiceRequestPlan(requestDto: SearchServiceRequestPlanRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (SearchServiceRequestPlanResponseDTO?, Error?) -> Void) {
        
        let completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests"
        //        if requestDto is HistoryServiceRequestPlanRequestDTO{
        //            //completeUrl = "https://cdn.1atesting.in/serviceplatform/api/servicerequests?        status=CO&referenceNumbers=100199461"
        //            completeUrl = completeUrl + "?status=CO&referenceNumbers=100199461"
        //        }
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
//            var finalData = data
//            if let dat = String(data: data ?? Data(), encoding: .isoLatin1)?.data(using: .utf8) {
//                finalData = dat
//            }
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getPendingFeedback(requestDto: GetPendingFeedbackRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetPendingFeedbackResponseDTO?, Error?) -> Void) {
        //  let completeE = "\(Constants.Claims.URL.membershipAssetDetail)\(requestDto?.membershipId)"
        let completeUrl = "\(Constants.Claims.URL.address)"
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func uploadClaimInvoice(requestDto: UploadClaimInvoiceRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (UploadClaimInvoiceResponseDTO?, Error?) -> Void) {
        
        //        {{HostName}}/serviceplatform/api/servicerequests/{{ServiceRequestId}}/documents?documentId=615f7673f1284d939160515aa92d71cb
        
        if let dta = requestDto?.files,  let err = Utilities.isImageDataApt(dta) {
            self.handleResponse(data: nil, urlResponse: nil, error: err, completionHandlerBlock: completionHandler)
            return
        }
        
        let completeUrl: String
        
        if let id = requestDto?.documentId {
            // update
            completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(requestDto?.serviceRequestId ?? "")/documents?documentTypeId=\(requestDto?.documentTypeId ?? "")&documentId=\(id)"
        } else {
            completeUrl = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(requestDto?.serviceRequestId ?? "")/documents?documentTypeId=\(requestDto?.documentTypeId ?? "")"
        }
        
        remoteDataSource.responseMultipart(url: completeUrl, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken(), multipartRequest: taskCallBack) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
    }
    
    func getMembershipServices(requestDto: MembershipServicesRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (MembershipServicesResponseDTO?, Error?) -> Void) {
        
        //    https://<context>/myaccount/api/membership/{memId}/services
        
        let completeUrl = "\(Constants.SchemeVariables.baseMainDomain)/myaccount/api/membership/\(requestDto?.memId.description ?? "")/services"
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getCheckAvailability(requestDto: CheckAvailabilityRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (CheckAvailabilityResponseDTO?, Error?) -> Void) {
        let url = "\(Constants.SchemeVariables.baseMainDomain)/myaccount/api/claim/checkEligibility"
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getExcessPayment(requestDto: ExcessPaymentRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ExcessPaymentResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.URL.baseMainUrlOasys)doPayment"
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getClaimDocumentRequest(requestDto: GetClaimDocumentRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetClaimDocumentResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.SchemeVariables.servicePlatform)servicerequests/documents"//\(requestDto?.documentId ?? "")"
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            
            if let imageData = data {
                let desc = String(data: imageData, encoding: .utf8)
                let imageData = Data.init(base64Encoded: desc ?? "") ?? Data()
                let response = GetClaimDocumentResponseDTO(dictionary: [:])
                response.status = Constants.ResponseConstants.success
                response.image = UIImage(data: imageData)
                
                DispatchQueue.main.async {
                    let response: GetClaimDocumentResponseDTO?  = response.image != nil ? response : nil
                    completionHandler(response, error)
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(nil, nil)
                }
            }
        }
        taskCallBack(request)
    }
    
    
    func updateSR(requestDto: ClaimUpdateScheduleVisitRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ClaimUpdateScheduleVisitResponseDTO?, Error?) -> Void){
        
        let url = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(requestDto?.serviceID ?? "")/schedule"
        let request = remoteDataSource.responsePut(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func notifyAllDocumentUploaded(requestDto: ClaimNotifyAllDocumentUploadedRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (ClaimNotifyAllDocumentUploadedResponseDTO?, Error?) -> Void){
        
        var eventCode = ""
        if Utilities.isSrTypePE(value: requestDto?.srType) {
            eventCode = "UPDATE_STAGE_COMPLETE"
        } else {
            eventCode = "COMPLETE_DOCUMENT_UPLOAD"
        }
//        let url = "\(Constants.SchemeVariables.servicePlatform)servicerequests/\(requestDto?.serviceID ?? "")/onEvent?eventCode=\(eventCode)"
//        "https://qa1admin.1atesting.in/serviceplatform/api/"
        let url = "\(Constants.SchemeVariables.servicePlatform)servicerequests/v2/\(requestDto?.serviceID ?? "")/onEvent?eventCode=\(eventCode)"
        
        let request = remoteDataSource.responsePut(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getServiceDocumentType(requestDto: GetServiceDocumentTypeRequestDTO?, apiCacheChecker: ApiCacheChecker, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetServiceDocumentTypeResponseDTO?, Error?) -> Void) {
        
        if !Utilities.isSrTypePE(value: requestDto?.serviceRequestTypeId) {
            
            requestDto?.referenceCode = nil
//            requestDto?.insurancePartnerCode = nil
//
//            if let list: [DocumentTypeEntity] =  CoreDataStack.sharedStack.fetchApiCacheData(apiCacheChecker: apiCacheChecker), list.count > 0 {
//
//                if let entity = list.filter({$0.srType == requestDto?.serviceRequestTypeId}).first, let dataObj = entity.docTypeData {
//                    self.handleResponse(data: dataObj as Data, urlResponse: nil, error: nil, completionHandlerBlock: completionHandler)
//                    return
//                }
//            }
            
        }
        
        let url = "\(Constants.SchemeVariables.servicePlatform)servicedocumenttypes/v2/search"
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken())
        { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: GetServiceDocumentTypeResponseDTO?, error) in
                
                do {
                    try Utilities.checkError(response, error: error)
                  
                    let predicate = "srType == '\(requestDto?.serviceRequestTypeId ?? "")'"
                    let context = CoreDataStack.sharedStack.secondaryContext
                    
                    if let entities: [DocumentTypeEntity] = CoreDataStack.sharedStack.fetchObject(predicateString: predicate, inManagedObjectContext: CoreDataStack.sharedStack.mainContext), let ent = entities.first {
                        
                        ent.docTypeData = data as NSData?
                    } else {
                        
                        let ent = DocumentTypeEntity(context: context)
                        ent.srType = requestDto?.serviceRequestTypeId ?? ""
                        ent.docTypeData = data as NSData?
                    }
                    
                    CoreDataStack.sharedStack.insertApiCacheData(apiCacheChecker: apiCacheChecker)
                    CoreDataStack.sharedStack.saveContext(context: context) {}
                    
                    
                } catch {
                    print("service doc response not saved in db")
                }
                
                completionHandler(response, error)
            }
                
            )
        }
        
        taskCallBack(request)
    }
    
    func getDeviceCondition(requestDto: DeviceConditionRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (DeviceConditionResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.SchemeVariables.servicePlatform)generickeysets/PE_DEVICE_CONDITION"
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getBankDetails(requestDto: GetBankDetailsRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetBankDetailsResponseDTO?, Error?) -> Void) {
        let url = "\(Constants.SchemeVariables.baseMainDomain)/myaccount/api/customer/\(requestDto?.custId ?? "")/bankDetails"

        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }

    func updateBankDetails(requestDto: UpdateBankDetailsRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (UploadClaimInvoiceResponseDTO?, Error?) -> Void) {
        
//        https://sit2.1atesting.in/myaccount/api/customer/2869414/bank
        let completeUrl: String = "\(Constants.SchemeVariables.baseMainDomain)/myaccount/api/customer/\(requestDto?.custID ?? "")/bank?srNumber=\(requestDto?.srNumber ?? "")"

        remoteDataSource.responseMultipart(url: completeUrl, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken(), multipartRequest: taskCallBack) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
    }
    
    func getBankDocumentRequest(requestDto: GetClaimDocumentRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetClaimDocumentResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.SchemeVariables.baseMainDomain)//myaccount/api/claim/document/download?mongoId=\(requestDto?.documentIds ?? "")"

        let request = remoteDataSource.responseGet(url: url, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            
            if let imageData = data {
                let desc = String(data: imageData, encoding: .utf8)
                let imageData = Data.init(base64Encoded: desc ?? "") ?? Data()
                let response = GetClaimDocumentResponseDTO(dictionary: [:])
                response.status = Constants.ResponseConstants.success
                response.image = UIImage(data: imageData)
                
                DispatchQueue.main.async {
                    let response: GetClaimDocumentResponseDTO?  = response.image != nil ? response : nil
                    completionHandler(response, error)
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(nil, nil)
                }
            }
        }
        taskCallBack(request)
    }
    
    func getDocumentDetails(requestDto: GetDocumentDetailsRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetDocumentDetailsResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.SchemeVariables.baseDomain)/myaccount/api/customer/documents"
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
        
    }
    
    func getServiceCost(requestDto: GetServiceCostRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetServiceCostResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.SchemeVariables.servicePlatform)service/costs"
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getServicedAsset(requestDto: GetServicedAssetRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (GetServicedAssetResponseDTO?, Error?) -> Void) {
        
        let url = "\(Constants.SchemeVariables.servicePlatform)servicerequests/assets"
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func draftMembership(requestDto: DraftsMembershipRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (DraftsMembershipResponseDTO?, Error?) -> Void){
        
        let url = "\(Constants.SchemeVariables.servicePlatform)/v1/intimation?id=" + requestDto!.referenceNumbers
        
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
}
