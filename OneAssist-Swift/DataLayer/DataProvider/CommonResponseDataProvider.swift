//
//  ResponseDataProvider.swift
//  OneAssistAlamoFire
//
//  Created by Sudhir.Kumar on 09/08/17.
//  Copyright © 2017 Sudhir.Kumar. All rights reserved.
//

import Foundation
import Alamofire

final class CommonResponseDataProvider: DataProvider, CommonDataProvider {
    
    private let remoteDataSource = RemoteDataSource()
    
    func getPlanBenefits(requestDto: PlanBenefitsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(PlanBenefitsResponseDTO?, Error?)->Void) {
        
        let url = Constants.MembershipUrls.getPlanBenefits
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getPlanBenefitsWithRank(requestDto: PlanBenefitsWithRankRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(PlanBenefitsWithRankResponseDTO?, Error?)->Void) {
        
        let url = Constants.WHC.URL.planBenefitsWithRank
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getRenewalPaymentResult(requestDto: RenewalPaymentRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(RenewalPaymentResponseDTO?, Error?)->Void) {
        
        let url = Constants.PaymentURLs.renewalPayment
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func saveDeviceRegistrationDetails(requestDto: DeviceRegistrationDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void) {
        let url = Constants.URL.baseMainUrlOasys + "restservice/v2/wsupdateDeviceRegistrationDetails"
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func saveNotificationStatus(requestDto: NotificationStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void) {
        
        let url = Constants.URL.baseMainUrlOasys + "restservice/v2/wsupdateNotificationStatus"
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getMasterData(requestDto: MasterDataRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(MasterDataResponseDTO?, Error?)->Void) {
        
        let url = Constants.Urls.getMasterData
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func updatePostPaymentDetails(requestDto: UpdatePostPaymentDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UpdatePostPaymentDetailsResponseDTO?, Error?)->Void) {
        
        let url = Constants.Urls.postPaymentDetails
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func uploadInvoice(requestDto: UploadInvoiceRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UploadInvoiceResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.URL.baseMainUrlOasys)restservice/wsUploadAssetDocument"
        
        remoteDataSource.responseMultipart(url: url, request: requestDto, headers:RemoteDataSource.apiGatewayHeadersWithToken(), multipartRequest: taskCallBack) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
    }
    
    func uploadInvoiceFD(requestDto: UploadInvoiceFDRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UploadInvoiceFDResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.URL.baseMainUrlOasys)webservice/rest/uploadAssetDocuments"
        
        remoteDataSource.responseMultipart(url: url, request: requestDto, multipartRequest: taskCallBack) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
    }
    
    func buyNow(requestDto: BuyNowWhcRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BuyNowWhcResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.SchemeVariables.baseDomain)" + "/OASYS/" + "buyNow?planCode=%@&leadSource=" + Constants.SchemeVariables.leadSource
        let completeUrl = String(format: url, requestDto?.planCode ?? "")
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getServicesList(requestDto: GetServicesListRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetServicesListResponseDTO?, Error?)->Void){
        
        var url = Constants.SODUrls.getServices
            if let pcs = requestDto?.productCodes {
                url += "?productCodes=" + pcs.joined(separator: ",")
                if let serviceCodes = requestDto?.serviceCodes, (requestDto?.serviceCodes?.count ?? 0) > 0 {
                    url += "&serviceRequestTypes=" + serviceCodes.joined(separator: ",")
                }
                url += "&pageNo=0&pageSize=100&sortBy=serviceId&sortType=DESC"
            }
        let request = remoteDataSource.responseGet(url: url, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getProductsSpecs(requestDto: GetProductSpecsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void,completionHandlerBlock: @escaping(GetProductSpecsResponseDTO?, Error?)->Void){
        var url = Constants.SODUrls.getProductSpecifications
        if let pcs = requestDto?.productCodes,let isSubscription = requestDto?.isSubscription {
            url += "?productCodes=" + pcs.joined(separator: ",") + "&isSubscription=\(isSubscription)"
        }
        let request = remoteDataSource.responseGet(url: url, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getSRsReportForPincode(requestDto: GetSRsReportForPincodeReqDTO?, taskCallBack: @escaping (_ task: Request?) -> Void,completionHandlerBlock: @escaping(GetSRsReportForPincodeResDTO?, Error?)->Void){
        
        guard let pincode = requestDto?.pinCode else{ return }
        requestDto?.pinCode = nil
        let url = Constants.SODUrls.getSRsReportByPinCode + "/%@/services/reports"
        let completeUrl = String(format: url, pincode)
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getProductCategory(requestDto: ProductCategoryRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(ProductCategoryResponseDTO?, Error?)->Void) {
        
        let url = Constants.MembershipUrls.getSubCategories
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getStateCity(requestDto: StateCityRequestDTO?, taskCallBack:@escaping (_ task : Request?)-> Void, completionHandlerBlock:@escaping(StateCityResponseDTO?, Error?)->Void) {
        
        let url = Constants.Urls.getStateAndCity
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getCustomerMembershipDetails(requestDto: CustomerMembershipDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(CustomerMembershipDetailsResponseDTO?, Error?)->Void) {
        
        let url = Constants.SchemeVariables.baseDomain + "/myaccount/api/customer/memberships"
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getCustDetails(requestDto: GetCustDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetCustDetailsResponseDTO?, Error?)->Void) {
        
        let url = Constants.SchemeVariables.baseMainDomain + "/myaccount/service/customer/getcustdetail"
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getDocsToUpload(requestDto: GetDocsToUploadRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetDocsToUploadResponseDTO?, Error?)->Void) {
        let urlBase64CharacterSet = CharacterSet(charactersIn: "/+=\n").inverted
        let code = requestDto?.activationCode.addingPercentEncoding(withAllowedCharacters: urlBase64CharacterSet)
        
        let url = "\(Constants.URL.baseMainUrlOasys)getDocsToUpload?activationCode=%@"
        let completeUrl = String(format: url, code ?? "")
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func submitDocs(requestDto: SubmitDocRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SubmitDocResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.Urls.submitDocURL)?activityRefId=%@"
        let completeUrl = String(format: url, requestDto?.activityRefId ?? "")
        let request = remoteDataSource.responsePut(url: completeUrl, request: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    
    func updateCustDetails(requestDto: UpdateCustDetailsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void) {
        
        let url = Constants.Urls.updateCustDetails
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getBuyBackStatusData(requestDto: BuyBackStatusRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BuyBackStatusResponseDTO?, Error?)->Void) {
        
        let url = Constants.Urls.buybackStatus
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func postMHCData(requestDto: MHCDataAddUpdateRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(MHCDataAddUpdateResponseDTO?, Error?)->Void) {
        
        let url = Constants.Urls.mhcDataStore
        
        let request = remoteDataSource.responsePost(url: url, body: requestDto?.data, headers: RemoteDataSource.apiGatewayHeadersWithToken()) {(data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getUserGroup(requestDto: UserGroupRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UserGroupResponseDTO?, Error?)->Void) {
        let url = Constants.Urls.userGroup + (requestDto?.custId ?? "")
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: UserGroupResponseDTO?, error) in
                if let userLabels = response?.data?.userLabels {
                    AppUserDefaults.isCatalystUserGroup = userLabels.contains(Constants.AppConstants.catalystGroup)
                }
                completionHandlerBlock(response, error)
            })
        }
        
        taskCallBack(request)
    }
    
    func sendImageUploadLink(requestDto: SendSMSRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SendSMSResponseDTO?, Error?)->Void) {
        let url = Constants.Urls.sendImageUploadLink
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    
    func getIMEIKnowMoreResponse(requestDto: KnowMoreRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(IMEIFoundKnowMoreModel?, Error?)->Void) {
        if let path = Bundle.main.path(forResource: "IMEIFoundKnowMore", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                self.handleResponse(data: data, urlResponse: nil, error: nil, completionHandlerBlock: completionHandlerBlock)
                
            } catch {
                // handle error
            }
        }
    }
    
    func getInvoices(requestDto: DownloadInvoiceRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DownloadInvoiceResponseDTO?, Error?)->Void) {
        let url = Constants.MembershipUrls.getInvoices
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getDocumentUrl(requestDto: DocumentUrlRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DocumentUrlResponseDTO?, Error?)->Void) {
        let url = Constants.MembershipUrls.getDocumentUrl
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func refundOrder(requestDto: RefundOrderRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(RefundOrderResponseDTO?, Error?)->Void) {
        
       // let url = "\(Constants.URL.baseMainUrlOasys)webservice/rest/cancelPendingCustomer"
        //https://uat1.1atesting.in/OASYS/webservice/rest/cancelPendingCustomer
        
        let url = "\(Constants.URL.baseMainUrlOasys)webservice/rest/cancelPendingCustomer?orderId=%@"
        let completeUrl = String(format: url, requestDto?.orderId ?? "")
        
        let request = remoteDataSource.responsePut(url: completeUrl, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    
    func get1oaLongUrl(url: String, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetLongURLResponseDTO?, Error?)->Void) {
        
        let request = remoteDataSource.responseGet(url: url, request: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func changeFDTestType(requestDto: FDTestTypeChangeRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.URL.baseUrlApiGateWay)subscription-service/order/model-type/\(requestDto?.activityReferenceId ?? "")"
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto, headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    
    func getFDHashKey(requestDto: FDHashKeyGenerationReqDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(FDHashKeyGenerationResDTO?, Error?)->Void){
        let url = "\(Constants.URL.baseUrlApiGateWay)subscription-service/order/hash-key/\(requestDto?.activityReferenceId ?? "")"
        
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
}
