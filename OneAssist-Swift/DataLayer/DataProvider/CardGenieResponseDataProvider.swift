//
//  CardGenieResponseDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 9/4/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

final class CardGenieResponseDataProvider: DataProvider, CardGenieDataProvider {
    
    private let remoteDataSource = RemoteDataSource()
    
    func addBookmarkedOffers(requestDto: AddBookmarkedOffersRequestDTO?, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(AddBookmarkedOffersResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.bookmarkOffers
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getCategoryList(requestDto: CategoryListRequestDTO?, apiCacheChecker: ApiCacheChecker, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(CategoryListResponseDTO?, Error?) -> Void) {
        
        if let list: [OfferCategoryCD] =  CoreDataStack.sharedStack.fetchApiCacheData(apiCacheChecker: apiCacheChecker) {
            
            let response = CategoryListResponseDTO()
            response.status = Constants.ResponseConstants.success
            response.categories = list
            completionHandlerBlock(response, nil)
            return
        }
        
        let url = Constants.WalletTabUrls.CardGenieUrls.offerCategories
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
            
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: CategoryListResponseDTO?, error) in
                if let list = response?.data {
                    
                    let context = CoreDataStack.sharedStack.secondaryContext
                    
                    // remove old records if any
                    if let list: [OfferCategoryCD] = CoreDataStack.sharedStack.fetchObject(predicateString: nil, inManagedObjectContext: context) {
                        for entity in list {
                            context.delete(entity)
                        }
                    }
                    //
                    
                    let dbList = list.map({ (apiCategory) -> OfferCategoryCD in
                        let dbCategory = OfferCategoryCD(context: context)
                        dbCategory.category = apiCategory.category
                        return dbCategory
                    })
                    
                    response?.categories = dbList
                    response?.data = nil
                    
                    CoreDataStack.sharedStack.insertApiCacheData(apiCacheChecker: apiCacheChecker)
                    CoreDataStack.sharedStack.saveContext(context: context)
                }
                
                completionHandlerBlock(response, error)
            })
        }
        
        taskCallBack(request)
    }
    
    func getBookmarkedCardOffers(requestDto: BookmarkedCardOffersRequestDTO?, apiCacheChecker: ApiCacheChecker, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BookmarkedCardOffersResponseDTO?, Error?) -> Void) {
        
        if let list: [Offer] =  CoreDataStack.sharedStack.fetchApiCacheData(apiCacheChecker: apiCacheChecker) {
            
            let bookmarkedList = list.filter({ (offer) -> Bool in
                let expiringDate = Date(string: offer.endDate ?? "", formatter: DateFormatter.serverDateFormat) ?? Date.currentDate()
                let order = Date.compareDate(fromdate: expiringDate, todate: Date())
                return offer.bookmarked == 1 && (order != -1)
            })
            
            if bookmarkedList.count > 0 {
                let response = BookmarkedCardOffersResponseDTO()
                response.status = Constants.ResponseConstants.success
                response.dataCD = bookmarkedList
                response.offerCount = NSNumber(integerLiteral: bookmarkedList.count)
                
                let offerList = bookmarkedList.map({ (offer) -> BankCardOffers in
                    let cardOffer = BankCardOffers(dictionary: [:])
                    cardOffer.bookmarkId = offer.bookmarkId
                    cardOffer.bookmark = offer.bookmark
                    cardOffer.endDate = offer.endDate
                    cardOffer.offerId = offer.offerId
                    cardOffer.offerCode = offer.offerCode
                    cardOffer.offerType = offer.offerType
                    cardOffer.offerTitle = offer.offerTitle
                    cardOffer.outletCode = offer.outletCode
                    cardOffer.offerExpiringInDays = offer.offerExpiringInDays
                    cardOffer.distance = offer.discount
                    cardOffer.image = offer.image
                    cardOffer.latitude = offer.latitude
                    cardOffer.longitude = offer.longitude
                    cardOffer.merchantLocation = offer.merchantLocation
                    cardOffer.merchantName = offer.merchantName
                    
                    if let cardDetail = offer.cardDetailList {
                        var existingcardArray: [CardDetail] = []
                        for cardObj in cardDetail {
                            if let bankCard = cardObj as? Issuer {
                                let card = CardDetail(dictionary: [:])
                                card.cardTypeId = bankCard.cardTypeId
                                card.cardIssuerId = bankCard.cardIssuerId
                                card.cardIssuerName = bankCard.cardIssuerName
                                card.cardIssuerCode = bankCard.cardIssuerCode
                                card.imagePath = bankCard.imagePath
                                card.helpline1 = bankCard.helpline1
                                card.helpline2 = bankCard.helpline2
                                card.landline1 = bankCard.landline1
                                card.landline2 = bankCard.landline2
                                card.ranking = bankCard.ranking as NSNumber
                                card.tagging = bankCard.tagging
                                existingcardArray += [card]
                            }
                        }
                        cardOffer.cardDetailsList = existingcardArray
                    }
                    return cardOffer
                })
                response.data = offerList
                completionHandlerBlock(response, nil)
                return
            }
        }
        
        let url = Constants.WalletTabUrls.CardGenieUrls.aggregatedSearchOffers
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)){ (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: BookmarkedCardOffersResponseDTO?, error) in
                if let list = response?.data {
                    let offerDB = OfferSyncUseCase.shared.storeBookMarkOfferInDb(offers: list, apiCacheChecker: apiCacheChecker)
                    response?.dataCD = offerDB
                }
                
                completionHandlerBlock(response, error)
            })
        }
        
        taskCallBack(request)
    }
    
    func getNearestBankBranch(requestDto: NearestBankBranchRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(NearestBankBranchResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.nearestBankBranch
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) {(data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
        
    }
    
    func getNearestATM(requestDto: NearestATMRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(NearestATMResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.nearestATM
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) {(data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
        
    }
    
    func getOnlineCardOffers(request: OnlineCardOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OnlineCardOffersResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.aggregatedSearchOffers
        
        let request = remoteDataSource.responsePost(url: url, request: request,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getOfflineCardOffers(request: OfflineCardOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OfflineCardOffersResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.aggregatedSearchOffers
        
        let request = remoteDataSource.responsePost(url: url, request: request,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
            
        taskCallBack(request)
        
    }
    
    func getOnlineOfferDetail(request: OnlineOfferDetailRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OnlineOfferDetailResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.offers
        
        let request = remoteDataSource.responseGet(url: url, request: request,headers:RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getOfflineOfferDetail(request: OfflineOfferDetailRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(OfflineOfferDetailResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.offers
        
        let request = remoteDataSource.responseGet(url: url, request: request,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func deleteBookmarkedOffers(request: DeleteBookmarkedOffersRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DeleteBookmarkedOffersResponseDTO?, Error?) -> Void) {
        
        let url = Constants.WalletTabUrls.CardGenieUrls.bookmarkOffers
        
        let request = remoteDataSource.responseDelete(url: url, request: request,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getBankCards(request: BankCardRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BankCardResponseDTO?, Error?) -> Void) {
        
        if let responseDTO = DataDiskStogare.getOfferbanksData(), let data = responseDTO.data, data.count>0 {
            completionHandlerBlock(responseDTO, nil)
            
        }else {
            
            let url = Constants.WalletTabUrls.CardGenieUrls.bankCards
            
            let request = remoteDataSource.responseGet(url: url, request: request,headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: (Constants.CardGenieCreds.username, Constants.CardGenieCreds.password)) { (data, urlResponse, error) in
                self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: BankCardResponseDTO?, error) in
                    DataDiskStogare.saveOfferbanksData(data: data)
                    completionHandlerBlock(response, error)
                })
            }
            
            taskCallBack(request)
        }
    }
    
}
