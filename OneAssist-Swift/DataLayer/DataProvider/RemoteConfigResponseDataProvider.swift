//
//  RemoteConfigDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/17/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

class RemoteConfigManager {
    
    static let shared = RemoteConfigManager()
    private init() {}
    
    private var remoteConfig: RemoteConfig!
    private var isDeveloperMode: Bool!
    private var expirationTime: TimeInterval!
    private let forceUpdateKey = "requires_force_update"
    private let forceUpdateTitleKey = "force_update_dialog_title"
    private let forceUpdateMessageKey = "force_update_dialog_msg"
    private let haTopCatKey = "home_appliances_top_subcategories_revamp"
    private let haOtherCatKey = "home_appliances_other_subcategories_revamp"
    private let peCatKey = "pe_devices_list"
    private let whcApplianceList = "whc_appliances_list"
    private let whcActivated = "whc_plans_activated"
    private let currentVersionKey = "Current_Version"
    private let showClaimTabKey  = "sd_enabled"
    private let renewalQuickFactsList = "renewal_quick_facts"
    private let customerCareContactNumber = "oneassist_customer_care_number"
    private let minCharacterIncidentDescriptionLenght = "claim_raise_description_min_length"
    private let screenProtectionEnabled = "screen_protection_enabled"
    private let maxCharacterIncidentDescriptionLenght = "claim_raise_description_max_length"
    private let oaChatEnabled = "oa_chat_v1"
    private let renewMessageHAKey = "renew_message_HA"
    private let forceUpdateJs = "force_update_JS"
    private let jsUpdateInterval = "JS_update_interval"
    private let isShowBuybackComingsoonCard = "is_show_buyback_comingsoon_card"
    private let isShowBuybackCard = "is_show_buyback_card"
    private let isShowDeselectAllMHCCategoryIcon = "is_show_deselect_all_MHC_category_icon"
    private let showModelNotFoundTime = "show_model_not_found_time"
    private let buybackCouponCodeKey = "buyback_coupon_code"
    private let buybackTNCKey = "buyback_tnc"
    private let idFenceKey = "idfenceWeb_dashboard_url"
    private let unlimitedApplianceCountKey = "unlimited_appliances_count"
    private let prePostOnboardingFormFieldsKey = "pre_post_onboarding_form_fields_v2"
    private let imeiScreenShotsend = "imeiScreenShotsend"
    private let invoiceValidationDays = "invoice_validation_days"
    private let fdImageScaleFactor = "fd_image_scale_factor"
    private let fd_barcodeScanner_support  = "fdbarcodeScannersupport"
    private let categoryCouponCodeKey = "category_coupon_code"
    private let categoryExclusiveBenefitsKey = "category_exclusive_benefits"
    private let categoryTestimonialsKey = "category_testimonials"
    private let planDetailFaqKey = "plan_detail_faq"
    private let sodFaqKey = "sod_faq_url"
    //    private let buyPlanRevampKey = "buy_plan_revamp"
    private let buyPlanRevampKey = "buy_plan_revamp_v1"
    private let buyPlanIdFenceUrlKey = "buy_plan_id_fence_url"
    private let productTermsKey = "product_terms"
    private let autoSuggestionDataKey = "auto_suggestion_data"
    private let idFenceDetailPageDataKey = "id_fence_detail_page_data"
    private let reward_feature_on = "reward_feature_on"
    private let fmipDataKey = "fmip_data"
    private let milogoutDataKey = "milogout_data"
    private let idFenceDashboardDetailsKey = "id_fence_dashboard_details"
    private let logChatEventKey = "log_chat_events"
    private let oaPromises = "oa_promises"
    private let safetyGuidlines = "safety_guidlines"
    private let sodAppliances = "sod_appliances"
    private let assuredServices = "oa_assured_services"
    private let whyOA = "why_choose_oa"
    private let platformRatings = "oa_platform_ratings"
    private let mirrorFlowRatingData = "mirror_flow_rating_data"
    private let subscriptionBenefits = "subscription_benefits"
    private let qrCampaignIdSoho = "qr_campaign_id_soho"
    private let migrateDataInAppDelegateKey = "migrate_data_in_app_delegate"
    private let mirrortestvideoguide = "mirror_test_video_guide_v1"
    private let mirrortestsamplephoto = "mirror_test_sample_photo"
    private let mirrorflowdata = "mirror_flow_data_v1"
    private let mirrortestlanguageenable = "mirror_test_language_enable"
    private let mirrortestretrytime = "mirror_test_retry_time"
    private let notificationAlertDaysCapKey = "notification_alert_daysCap"
    private let notificationAlertKey  = "notification_alert"
    private let showNotificationPromptKey =  "show_notification_prompt"
    private let sodProductServiceScreenKey = "sod_product_service_screen"
    private let sodProductScreenKey = "sod_product_screen"
    private let bbQquestionDataVersionKey = "bb_question_data_version"
    private let buybackDeclarationDataKey = "buyback_declaration_data"
    private let cdnDataVersioningKey = "cdn_data_versioning"
    
    private let showCatalystRecommendationsKey = "show_catalyst_recommendations"
    private let showTodayTabKey = "show_today_tab"
    private let recommendationForceUpdateIntervalKey = "recommendation_force_update_interval"
    private let enableFirebaseImageStorageKey = "enable_firebase_image_storage"
    
    private let findPlanVariationsKey = "find_plan_variations"
    private let planListVariationsKey = "plan_list_variations"
    private let moduleflowsequencingkey = "module_flow_sequencing"
    private let haCampaignId = "ha_campaign_id"
    private let qrCampaignIdNonServ = "qr_campaign_id_nonserviceable"
    private let serviceGadgetsKey = "service_gadgets"
    private let productServicesKey = "product_services"
    private let buyTabJSONKey = "buy_tab_json"
    private let sodAppliancesOffers = "sod_appliance_offers"
    private let planTaglineKey = "plan_tagline"
    private let coverAmountInfoKey = "cover_amount_info"

    private let modelDownloadingScreenDataKey = "mirror_modals_download_screen"
    private let smartMirrorFlowModelsInfoKey = "smart_mirror_flow_models_info"
    private let mirrorTestThresoldsKey = "mirror_test_thresolds"
    private let mirrorTestErrorMessagesKey = "mirror_error_msgs"
    private let shortUrlRedirectionPatchkey = "short_url_redirection_patch"
    private let mirrorTestLoggingHashKey = "mirror_log_secure_key"
    private let fdDeviceBrightnessKey = "fd_device_brightness"

    
    
    func setup(isDeveloperMode: Bool, expirationTime: TimeInterval = Constants.RemoteConfig.defaultExpirationTime) {
        self.isDeveloperMode = isDeveloperMode
        self.expirationTime = expirationTime
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.configSettings = RemoteConfigSettings(developerModeEnabled: isDeveloperMode)
        remoteConfig.setDefaults(fromPlist: Constants.RemoteConfig.defaultPlistName)
    }
    
    var contactNumberCustomerCare  : String {
        return remoteConfig.configValue(forKey: customerCareContactNumber).stringValue ?? "1800 123 3330"
    }
    
    var minCharacterIncidentDescription  : String {
        return remoteConfig.configValue(forKey: minCharacterIncidentDescriptionLenght).stringValue ?? "140"
    }
    
    var maxCharacterIncidentDescription  : String {
        return remoteConfig.configValue(forKey: maxCharacterIncidentDescriptionLenght).stringValue ?? "400"
    }
    
    var isScreenProtectionEnabled: Bool {
        return remoteConfig.configValue(forKey: screenProtectionEnabled).boolValue
        
    }
    
    var unlimitedApplianceCount: String {
        return remoteConfig.configValue(forKey: unlimitedApplianceCountKey).stringValue ?? "500"
    }
    
    var requiresForceUpdate: Bool {
        return remoteConfig.configValue(forKey: forceUpdateKey).boolValue
    }
    
    var forceUpdateTitle: String {
        return remoteConfig.configValue(forKey: forceUpdateTitleKey).stringValue ?? ""
    }
    
    
    var forceUpdateMessage: String {
        return remoteConfig.configValue(forKey: forceUpdateMessageKey).stringValue ?? ""
    }
    
    var forceUpdateJS: Bool {
        return remoteConfig.configValue(forKey: forceUpdateJs).boolValue ?? false
    }
    
    var javascriptUpdateInterval: String {
        return remoteConfig.configValue(forKey: jsUpdateInterval).stringValue ?? ""
    }
    
    var homeAppliancesTopCategories: String {
        return remoteConfig.configValue(forKey: haTopCatKey).stringValue ?? ""
    }
    
    var homeAppliancesOtherCategories: String {
        return remoteConfig.configValue(forKey: haOtherCatKey).stringValue ?? ""
    }
    
    var personalElectronicsCategories: String {
        return remoteConfig.configValue(forKey: peCatKey).stringValue ?? ""
    }
    
    var whcCategoryList: String {
        return remoteConfig.configValue(forKey: whcApplianceList).stringValue ?? ""
    }
    
    var renewalQuickFacts: String {
        return remoteConfig.configValue(forKey: renewalQuickFactsList).stringValue ?? ""
    }
    
    var isWHCActivated: Bool {
        return remoteConfig.configValue(forKey: whcActivated).boolValue
    }
    
    var showClaimTab : Bool {
        return remoteConfig.configValue(forKey: showClaimTabKey).boolValue
    }
    var currentVersion: String {
        return remoteConfig.configValue(forKey: currentVersionKey).stringValue ?? ""
    }
    
    var isFreshChatEnabled : Bool {
        return remoteConfig.configValue(forKey: oaChatEnabled).boolValue ?? true
    }
    
    var renewMessageHA:String {
        return remoteConfig.configValue(forKey: renewMessageHAKey).stringValue ?? ""
    }
    
    var isIMEIScreenShotSend : Bool {
        return remoteConfig.configValue(forKey: imeiScreenShotsend).boolValue ?? false
    }
    
    var coverAmountInfo: String {
        return remoteConfig.configValue(forKey: coverAmountInfoKey).stringValue ?? ""
    }
    
    var smartMirrorFlowModelsInfo: String {
        return remoteConfig.configValue(forKey: smartMirrorFlowModelsInfoKey).stringValue ?? ""
    }
    
    var mirrorTestThresolds: String {
        return remoteConfig.configValue(forKey: mirrorTestThresoldsKey).stringValue ?? ""
    }
    
    var mirrorTestErrorMessages: String {
        return remoteConfig.configValue(forKey: mirrorTestErrorMessagesKey).stringValue ?? ""
    }
    
    var mirrorTestLoggingHash: String {
        return remoteConfig.configValue(forKey: mirrorTestLoggingHashKey).stringValue ?? ""
    }
    
    func fetchRemoteData() {
        remoteConfig.fetch(withExpirationDuration: expirationTime) { (status, error) in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate()
            } else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
        }
    }
    
    var getInvoiceValidationDays: Int {
        return remoteConfig.configValue(forKey: invoiceValidationDays).numberValue?.intValue ?? 15
    }
    
    var getFDImageScaleFactor: Int {
        return remoteConfig.configValue(forKey: fdImageScaleFactor).numberValue?.intValue ?? 1600
    }
    
    var getFDbarcodeScannersupport:String {
        return remoteConfig.configValue(forKey: fd_barcodeScanner_support).stringValue ?? ""
    }
    
    var showBuybackComingsoonCard:Bool {
        return remoteConfig.configValue(forKey: isShowBuybackComingsoonCard).boolValue
    }
    
    var showBuybackCard:Bool {
        return remoteConfig.configValue(forKey: isShowBuybackCard).boolValue
    }
    
    var showDeselectAllMHCCategoryIcon:Bool {
        return remoteConfig.configValue(forKey: isShowDeselectAllMHCCategoryIcon).boolValue
    }
    
    var showModelNotFoundTimeInDays:Int {
        return remoteConfig.configValue(forKey: showModelNotFoundTime).numberValue?.intValue ?? 0
    }
    
    var buybackCouponCode: String? {
        return remoteConfig.configValue(forKey: buybackCouponCodeKey).stringValue
    }
    
    var buybackTNCPath: String? {
        return remoteConfig.configValue(forKey: buybackTNCKey).stringValue
    }
    var productTNCPath: String? {
        return remoteConfig.configValue(forKey: productTermsKey).stringValue
    }
    
    var idFencePath: String? {
        return remoteConfig.configValue(forKey: idFenceKey).stringValue
    }
    
    var prePostOnboardingFormFieldsPath: String? {
        return remoteConfig.configValue(forKey: prePostOnboardingFormFieldsKey).stringValue
    }
    
    var categoryCouponCode: String {
        return remoteConfig.configValue(forKey: categoryCouponCodeKey).stringValue ?? ""
    }
    
    var categoryExclusiveBenefits: String {
        return remoteConfig.configValue(forKey: categoryExclusiveBenefitsKey).stringValue ?? ""
    }
    
    var categoryTestimonials: String {
        return remoteConfig.configValue(forKey: categoryTestimonialsKey).stringValue ?? ""
    }
    
    var planTagline: String {
        return remoteConfig.configValue(forKey: planTaglineKey).stringValue ?? ""
    }
    
    var planDetailFaq: String? {
        return remoteConfig.configValue(forKey: planDetailFaqKey).stringValue
    }
    
    var sodPlanDetailFaq: String? {
        return remoteConfig.configValue(forKey: sodFaqKey).stringValue
    }
    
    var buyPlanRevamp: String {
        return remoteConfig.configValue(forKey: buyPlanRevampKey).stringValue ?? ""
    }
    
    var buyPlanIdFenceUrl: String {
        return remoteConfig.configValue(forKey: buyPlanIdFenceUrlKey).stringValue ?? ""
    }
    
    var autoSuggestionData: String? {
        return remoteConfig.configValue(forKey: autoSuggestionDataKey).stringValue
    }
    
    var idFenceDetailPageData: String {
        return remoteConfig.configValue(forKey: idFenceDetailPageDataKey).stringValue ?? ""
    }
    
    var rewardFeatureOn:Bool {
        return remoteConfig.configValue(forKey: reward_feature_on).boolValue
    }
    
    var fmipData: String {
        return remoteConfig.configValue(forKey: fmipDataKey).stringValue ?? ""
    }
    
    var milogoutData: String {
        return remoteConfig.configValue(forKey: milogoutDataKey).stringValue ?? ""
    }
    
    var idFenceDashboardData: String {
        return remoteConfig.configValue(forKey: idFenceDashboardDetailsKey).stringValue ?? ""
    }
    
    var isLogChatEvent: Bool {
        return remoteConfig.configValue(forKey: logChatEventKey).boolValue
    }
    
    var oaPromisesData: String {
        return remoteConfig.configValue(forKey: oaPromises).stringValue ?? ""
    }
    
    var safetyGuidlinesData: String {
        return remoteConfig.configValue(forKey: safetyGuidlines).stringValue ?? ""
    }
    
    var sodAppliancesData: String {
        return remoteConfig.configValue(forKey: sodAppliances).stringValue ?? ""
    }
    
    var oaAssuredServices:String{
        return remoteConfig.configValue(forKey: assuredServices).stringValue ?? ""
    }
    
    var whyChooseOA:String{
        return remoteConfig.configValue(forKey: whyOA).stringValue ?? ""
    }
    
    var oaPlatformRatings:String{
        return remoteConfig.configValue(forKey: platformRatings).stringValue ?? ""
    }
    
    var oaSODAppliancesOffers: String{
        return remoteConfig.configValue(forKey: sodAppliancesOffers).stringValue ?? ""
    }
    
    var oaMirrorFlowRatingData: String{
        return remoteConfig.configValue(forKey: mirrorFlowRatingData).stringValue ?? ""
    }
    
    var sodProductServiceScreen: String {
        return remoteConfig.configValue(forKey: sodProductServiceScreenKey).stringValue ?? ""
    }
    
    var sodProductScreen: String {
        return remoteConfig.configValue(forKey: sodProductScreenKey).stringValue ?? ""
    }
    
    var oaSubscriptionBenefits: String {
        return remoteConfig.configValue(forKey: subscriptionBenefits).stringValue ?? ""
    }
    
    var oaHACampaignId: String{
        return remoteConfig.configValue(forKey: haCampaignId).stringValue ?? ""
    }
    
    var oaQRCampaignIdSoho: String{
        return remoteConfig.configValue(forKey: qrCampaignIdSoho).stringValue ?? ""
    }
    
    var oaQRCampaignIdNonServ: String{
        return remoteConfig.configValue(forKey: qrCampaignIdNonServ).stringValue ?? ""
    }
    
    var migrateDataInAppDelegate: Bool {
        return remoteConfig.configValue(forKey: migrateDataInAppDelegateKey).boolValue
    }
    
    var mirrorTestVideoGuide: String {
        return remoteConfig.configValue(forKey: mirrortestvideoguide).stringValue ?? ""
    }
    
    var mirrorTestSamplePhotos: String {
        return remoteConfig.configValue(forKey: mirrortestsamplephoto).stringValue ?? ""
    }
    
    var mirrorFlowData: String {
        return remoteConfig.configValue(forKey: mirrorflowdata).stringValue ?? ""
    }
    
    var mirrorLanguageEnable: Bool {
        return remoteConfig.configValue(forKey: mirrortestlanguageenable).boolValue
    }
    
    var mirrorTestRetryTime:Int {
        return remoteConfig.configValue(forKey: mirrortestretrytime).numberValue?.intValue ?? 60
    }
    
    var notificationAlertDaysCap: String {
        return remoteConfig.configValue(forKey: notificationAlertDaysCapKey).stringValue ?? "7"
    }
    
    var notificationAlert: String {
        return remoteConfig.configValue(forKey: notificationAlertKey).stringValue ?? ""
    }
    
    var showNotificationPrompt: Bool {
        return remoteConfig.configValue(forKey: showNotificationPromptKey).boolValue
    }
    
    var moduleflowsequencing: String {
        return remoteConfig.configValue(forKey: moduleflowsequencingkey).stringValue ?? ""
    }
    
    var findPlanVariations: String{
        return remoteConfig.configValue(forKey: findPlanVariationsKey).stringValue ?? ""
    }
    var planListVariations: String{
        return remoteConfig.configValue(forKey: planListVariationsKey).stringValue ?? ""
    }
    
    var bbQquestionDataVersion: String {
        return remoteConfig.configValue(forKey: bbQquestionDataVersionKey).stringValue ?? ""
    }
    
    var buybackDeclarationData: String {
        return remoteConfig.configValue(forKey: buybackDeclarationDataKey).stringValue ?? ""
    }
    var cdnDataVersioning: String{
        return remoteConfig.configValue(forKey: cdnDataVersioningKey).stringValue ?? ""
    }
    
    var modelDownloadingScreenData: String{
        return remoteConfig.configValue(forKey: modelDownloadingScreenDataKey).stringValue ?? ""
    }
    
    var serviceGadgets: String {
        return remoteConfig.configValue(forKey: serviceGadgetsKey).stringValue ?? ""
    }
    
    var productServices: String {
        return remoteConfig.configValue(forKey: productServicesKey).stringValue ?? ""
    }
    
    var buyTabJSON: String {
        return remoteConfig.configValue(forKey: buyTabJSONKey).stringValue ?? ""
    }
    
    var showCatalystRecommendations: Bool {
        return remoteConfig.configValue(forKey: showCatalystRecommendationsKey).boolValue
    }
    
    var showTodayTab: Bool {
        return remoteConfig.configValue(forKey: showTodayTabKey).boolValue
    }
    
    var recommendationForceUpdateInterval: Int {
        return remoteConfig.configValue(forKey: recommendationForceUpdateIntervalKey).numberValue?.intValue ?? 240
    }
    
    var fdDeviceBrightness: Float {
        return remoteConfig.configValue(forKey: fdDeviceBrightnessKey).numberValue?.floatValue ?? 0.5
    }
    
    var shortUrlRedirectionPatch: [[String:String]] {
        let jsonText = remoteConfig.configValue(forKey: shortUrlRedirectionPatchkey).stringValue
        if let data = jsonText?.data(using: .utf8){
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String:String]] {
                    return json
                }
            }catch _ as NSError {
                return [[:]]
            }
        }
        return [[:]]
    }

    func getFirebaseDataWith(key: String) -> String?{
        var firebaseData:String? = nil
        switch key {
        case RemoteConfigManager.shared.serviceGadgetsKey:
            firebaseData =  RemoteConfigManager.shared.serviceGadgets
        case RemoteConfigManager.shared.buyTabJSONKey:
            firebaseData = RemoteConfigManager.shared.buyTabJSON
        case RemoteConfigManager.shared.haCampaignId:
            firebaseData = RemoteConfigManager.shared.oaHACampaignId
        case RemoteConfigManager.shared.qrCampaignIdNonServ:
            firebaseData = RemoteConfigManager.shared.oaQRCampaignIdNonServ
        case RemoteConfigManager.shared.sodAppliancesOffers:
            firebaseData = RemoteConfigManager.shared.oaSODAppliancesOffers
        case RemoteConfigManager.shared.moduleflowsequencingkey:
            firebaseData = RemoteConfigManager.shared.moduleflowsequencing
        case RemoteConfigManager.shared.planTaglineKey:
            firebaseData = RemoteConfigManager.shared.planTagline
        case RemoteConfigManager.shared.findPlanVariationsKey:
            firebaseData = RemoteConfigManager.shared.findPlanVariations
        case RemoteConfigManager.shared.planListVariationsKey:
            firebaseData = RemoteConfigManager.shared.planListVariations
        case RemoteConfigManager.shared.coverAmountInfoKey:
            firebaseData = RemoteConfigManager.shared.coverAmountInfo
        case RemoteConfigManager.shared.renewMessageHAKey:
            firebaseData = RemoteConfigManager.shared.renewMessageHA
        case RemoteConfigManager.shared.haTopCatKey:
            firebaseData = RemoteConfigManager.shared.homeAppliancesTopCategories
        case RemoteConfigManager.shared.haOtherCatKey:
            firebaseData = RemoteConfigManager.shared.homeAppliancesOtherCategories
        case RemoteConfigManager.shared.peCatKey:
            firebaseData = RemoteConfigManager.shared.personalElectronicsCategories
        case RemoteConfigManager.shared.productServicesKey:
            firebaseData =  RemoteConfigManager.shared.productServices
        default:
            firebaseData = nil
        }
        return firebaseData
    }
    var enableFirebaseImageStorage: Bool {
        return remoteConfig.configValue(forKey: enableFirebaseImageStorageKey).boolValue
    }
}

class RemoteConfigResponseDataProvider: DataProvider, RemoteConfigDataProvider {
    func getBuyPlanCategory(completionHandler: @escaping (BuyPlanResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.buyPlanRevamp.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getOAPromises(completionHandler: @escaping (OAPromisesResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.oaPromisesData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSafetyGuidLines(completionHandler: @escaping (SafetyGuidesResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.safetyGuidlinesData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSODAppliances(completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.sodAppliancesData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getHomeApplianceCategory(completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.homeAppliancesTopCategories.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getHomeApplianceOtherCategory(completionHandler: @escaping (HomeApplianceCategoryResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.homeAppliancesOtherCategories.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getWHCApplianceList(completionHandler: @escaping (WHCApplianceListResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.whcCategoryList.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getQuickFactsList(completionHandler: @escaping (RenewalFactsResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.renewalQuickFacts.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getFDBarcodeSupportList(completionHandler: @escaping (FDbarcodeScannerResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.getFDbarcodeScannersupport.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getCustomerTestimonials(completionHandler: @escaping (CustomerTestimonialsResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.categoryTestimonials.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getCategoryCouponCodes(completionHandler: @escaping (CouponsResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.categoryCouponCode.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getCategoryExclusiveBenefits(completionHandler: @escaping (ExclusiveBenfitsResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.categoryExclusiveBenefits.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getPlanTagline(completionHandler: @escaping (PlanTaglineResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.planTagline.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getIDFenceDetailPageData(completionHandler: @escaping (PlanDetailPageData?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.idFenceDetailPageData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getOAAssuredServices(completionHandler: @escaping (OAAssuredServicesResponse?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.oaAssuredServices.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getWhyChooseOA(completionHandler: @escaping (WhyChooseOAResponse?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.whyChooseOA.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getPlatformRatings(completionHandler: @escaping (PlatformRatingsResponse?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.oaPlatformRatings.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSubscriptionBenefits(completionHandler: @escaping (SubscriptionBenefitsResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.oaSubscriptionBenefits.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSODProductsOffers(completionHandler: @escaping (SODOffersResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.oaSODAppliancesOffers.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getMirrorTestVideoGuide(completionHandler: @escaping (MirrorTestVideoGuideResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.mirrorTestVideoGuide.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getMirrorTestSamplePhotos(completionHandler: @escaping (MirrorTestSamplePhotoDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.mirrorTestSamplePhotos.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getMirrorTestLanguage(completionHandler: @escaping (MirrorTestLanguageModelDTO?, Error?) -> Void) {
        handleLanguageResponse(data: RemoteConfigManager.shared.mirrorFlowData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getPhotoHelpPoints(completionHandler: @escaping (PhotoClickHelpPointsDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.mirrorFlowData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getMirrorFlowRatingData(completionHandler: @escaping (MirrorFlowRatingRespModel?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.oaMirrorFlowRatingData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getNotificationPrompt(completionHandler: @escaping (NotificationAlertResponseDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.notificationAlert.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSODProductServiceScreen(completionHandler: @escaping (SODProductServiceScreenResDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.sodProductServiceScreen.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSODProductScreen(completionHandler: @escaping (SODProductServiceScreenResDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.sodProductScreen.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getCDNDataVersioning(completionHandler: @escaping (CDNDataVersioningDTO?, Error?) -> Void){
        handleResponse(data: RemoteConfigManager.shared.cdnDataVersioning.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getModelDownloadingStatusScreenData(completionHandler: @escaping (ModelDownloadingStatusScreenData?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.modelDownloadingScreenData.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getSmartMirrorFlowModelsInfo(completionHandler: @escaping (MirrorFlowModelsInfoResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.smartMirrorFlowModelsInfo.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getMirrorTestThresolds(completionHandler: @escaping (MirrorTestThresoldsResponseDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.mirrorTestThresolds.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func getMirrorTestErrorMessages(completionHandler: @escaping (MirrorTestErrorMessagesResDTO?, Error?) -> Void) {
        handleResponse(data: RemoteConfigManager.shared.mirrorTestErrorMessages.data(using: .utf8), completionHandlerBlock: completionHandler)
    }
    
    func handleResponse<ResponseDTOType: BaseResponseDTO>(data: Data?, completionHandlerBlock: @escaping (ResponseDTOType?, Error?)->Void) {
        do {
            if let data = data {
                let response: ResponseDTOType = try JSONParserSwift.parse(data: data)
                DispatchQueue.main.async {
                    completionHandlerBlock(response, nil)
                }
            } else {
                DispatchQueue.main.async {
                    completionHandlerBlock(nil, ParsingError.emptyResponse)
                }
            }
        } catch {
            DispatchQueue.main.async {
                completionHandlerBlock(nil, error)
            }
        }
    }
    
    func handleLanguageResponse(data: Data?, completionHandlerBlock: @escaping (MirrorTestLanguageModelDTO?, Error?)->Void) {
        do {
            if let data = data {
                let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictionary = dictionary as? [String: Any] {
                    let languageModelDTO = MirrorTestLanguageModelDTO(dictionary: [:])
                    if let lang_data = dictionary["lang_data"] as? [String: Any] {
                        var langdata: [LanguageData] = []
                        for key in lang_data.keys {
                            if let value = lang_data[key] as? [String: Any]  {
                                let languageData: LanguageData  =   JSONParserSwift.parse(dictionary: value)
                                languageData.language = key
                                langdata.append(languageData)
                            }
                        }
                        languageModelDTO.lang_data = langdata
                    }
                    DispatchQueue.main.async {
                        completionHandlerBlock(languageModelDTO, nil)
                    }
                } else {
                    throw JSONParserSwiftError.cannotParseJsonString
                }
            } else {
                DispatchQueue.main.async {
                    completionHandlerBlock(nil, ParsingError.emptyResponse)
                }
            }
        } catch {
            DispatchQueue.main.async {
                completionHandlerBlock(nil, error)
            }
        }
    }
}
