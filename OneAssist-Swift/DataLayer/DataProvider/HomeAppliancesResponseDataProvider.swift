//
//  HomeAppliancesDataProvider.swift
//  OneAssist-Swift
//
//  Created by OneAssist on 1/17/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

class HomeAppliancesResponseDataProvider: DataProvider, HomeAppliancesDataProvider {
    private let remoteDataSource = RemoteDataSource()
    
    func getAssetMake(requestDto: AssetMakeRequestDTO? , taskCallBack: @escaping (_ task : Request?)-> Void, completionHandler: @escaping (AssetMakeResponseDTO?, Error?) -> Void) {
        
        let subCategoryCodeList = requestDto?.subCategoryCodeList
        let url = Constants.Urls.getAssets
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    completionHandler(nil, error)
                }
                return
            }
            
            if let data = data, let jsonDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                let response = AssetMakeResponseDTO(dictionary: [:])
                response.status = jsonDict["status"] as? String
                response.message = jsonDict["message"] as? String
                response.assetMakeDtl = AssetMakeDtl(dictionary: [:])
                response.assetMakeDtl?.subCategoryMap = [:]
                
                if let subjsonDict = jsonDict["assetMakeDtl"] as? [String: Any], let subCatCode = subCategoryCodeList {
                    for code in subCatCode {
                        if let list = subjsonDict[code] as? [Any] {
                            var subCategories: [SubCategory] = []
                            for dictJson in list {
                                if let dict = dictJson as? [String: Any] {
                                    subCategories.append(SubCategory(dictionary: dict))
                                }
                            }
                            
                            response.assetMakeDtl!.subCategoryMap![code] = subCategories
                        }
                    }
                }
                DispatchQueue.main.async {
                    completionHandler(response, nil)
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(nil, ParsingError.emptyResponse)
                }
            }
        }
        
        taskCallBack(request)
    }
    
    func suggestPlans(request: SuggestPlanRequestDTO?, apiCacheChecker: ApiCacheChecker, taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SuggestPlanResponseDTO?, Error?) -> Void) {
        
        let isWhc = request?.isWhc ?? false
        
        if let list: [PlanCD] = CoreDataStack.sharedStack.fetchApiCacheData(apiCacheChecker: apiCacheChecker, predicate: "isWhc == \(NSNumber(value: isWhc))") {
            
            let response = SuggestPlanResponseDTO()
            response.status = Constants.ResponseConstants.success
            
            response.dataCD = list
            completionHandlerBlock(response, nil)
            return
        }
        
        let url = Constants.WHC.URL.suggestPlan
        
        let request = remoteDataSource.responsePost(url: url, request: request,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: SuggestPlanResponseDTO?, error) in
                
                if let list = response?.data {
                    
                    if let list: [PlanCD] =  CoreDataStack.sharedStack.fetchObject(predicateString: "isWhc == '\(NSNumber(value: isWhc))'", inManagedObjectContext: CoreDataStack.sharedStack.mainContext) {
                        for ent in list {
                            CoreDataStack.sharedStack.mainContext.delete(ent)
                        }
                    }
                    
                    let context = CoreDataStack.sharedStack.secondaryContext
                    
                    let dbList = list.map({ (plan) -> PlanCD in
                        let newDetails = PlanCD(context: context)
                        newDetails.planCode = plan.planCode?.int64Value ?? 0
                        newDetails.planName = plan.planName
                        newDetails.planDescription = plan.planDescription
                        newDetails.frequency = plan.frequency
                        newDetails.planDuration = plan.planDuration
                        newDetails.annuity = plan.annuity?.boolValue ?? false
                        newDetails.price = plan.price?.int64Value ?? 0
                        newDetails.taxcode = plan.taxcode
                        newDetails.discountAllowed = plan.discountAllowed?.boolValue ?? false
                        newDetails.trial = plan.trial?.boolValue ?? false
                        newDetails.noOfCustomers = plan.noOfCustomers?.int64Value ?? 0
                        newDetails.planStatus = plan.planStatus
                        newDetails.anchorPrice = plan.anchorPrice?.int64Value ?? 0
                        newDetails.renewalWindowDays = plan.renewalWindowDays?.int64Value ?? 0
                        newDetails.minInsuranceValue = plan.minInsuranceValue?.int64Value ?? 0
                        newDetails.maxInsuranceValue = plan.maxInsuranceValue?.int64Value ?? 0
                        newDetails.priceType = (plan.priceType?.int64Value)!
                        newDetails.allowedMaxQuantity = plan.allowedMaxQuantity?.int64Value ?? 0
                        newDetails.allowedUnits = plan.allowedUnits?.int64Value ?? 0 
                        newDetails.downgradeAllowed = plan.downgradeAllowed?.boolValue ?? false
                        newDetails.upgradeAllowed = plan.upgradeAllowed?.boolValue ?? false
                        newDetails.activationDefferedDays = plan.activationDefferedDays?.int64Value ?? 0
                        newDetails.graceDays = (plan.graceDays?.int64Value)!
                        newDetails.refundAllowed = plan.refundAllowed?.boolValue ?? false
                        newDetails.isPremium = plan.isPremium?.boolValue ?? false
                        newDetails.coverAmount = plan.coverAmount?.int64Value ?? 0
                        
                        newDetails.isWhc = isWhc
                        
                        if let benefits = plan.benefits, !benefits.isEmpty {
                            
                            var benefitsCD: [PlanBenefitsCD] = []
                            
                            for benefit in benefits {
                                let newplanBenefit = PlanBenefitsCD(context: context)
                                newplanBenefit.benefitId = benefit.benefitId?.int64Value ?? 0
                                newplanBenefit.benefit = benefit.benefit
                                newplanBenefit.benefitValue = benefit.benefitValue
                                newplanBenefit.shortName = benefit.shortName
                                newplanBenefit.planCode = benefit.planCode?.int64Value ?? 0
                                newplanBenefit.rank = benefit.rank?.int64Value ?? 0
                                benefitsCD.append(newplanBenefit)
                            }
                            
                            newDetails.addToBenefits(Set(benefitsCD) as NSSet)
                        }
                        
                        if let excludedBenefits = plan.excludedBenefits, !excludedBenefits.isEmpty {
                            
                            var excludedBenefitsCD: [PlanBenefitsCD] = []
                            
                            for excludedBenefits in excludedBenefits {
                                let newplanExcludedBenefit = PlanBenefitsCD(context: context)
                                newplanExcludedBenefit.benefitId = excludedBenefits.benefitId?.int64Value ?? 0
                                newplanExcludedBenefit.benefit = excludedBenefits.benefit
                                newplanExcludedBenefit.benefitValue = excludedBenefits.benefitValue
                                newplanExcludedBenefit.shortName = excludedBenefits.shortName
                                newplanExcludedBenefit.planCode = excludedBenefits.planCode?.int64Value ?? 0
                                newplanExcludedBenefit.rank = excludedBenefits.rank?.int64Value ?? 0
                                excludedBenefitsCD.append(newplanExcludedBenefit)
                            }
                            
                            newDetails.addToExcludedBenefits(Set(excludedBenefitsCD) as NSSet)
                        }
                        
                        var services: [ProductServices] = []
                        
                        for count in 0..<(plan.productServices?.count)!{
                            let productService = ProductServices(context: context)
                            productService.productCode = plan.productServices?[count].productCode
                            
                            for ele in (plan.productServices?[count].serviceList)!
                            {
                                let productServiceForWHC = WHCServiceList(context: context)
                                productServiceForWHC.serviceList = ele
                                productServiceForWHC.serviceListing = productService
                            }
                            productService.productServices = newDetails
                            services.append(productService)
                        }
                        
                        if let planServiceAttributes = plan.planServiceAttributes, !planServiceAttributes.isEmpty {
                            
                            var planServiceAttributesCD: [PlanServiceAttributeCD] = []
                            
                            for planServiceAttribute in planServiceAttributes {
                                let newplanServiceAttribute = PlanServiceAttributeCD(context: context)
                                newplanServiceAttribute.attributeName = planServiceAttribute.attributeName
                                newplanServiceAttribute.attributeValue = planServiceAttribute.attributeValue
                                newplanServiceAttribute.serviceName = planServiceAttribute.serviceName
                                planServiceAttributesCD.append(newplanServiceAttribute)
                            }
                            
                            newDetails.addToPlanServiceAttributes(Set(planServiceAttributesCD) as NSSet)
                        }
                        
                        if let planStatistics = plan.planStatistics {
                            let planStatisticsCD = PlanStatisticsCD(context: context)
                            planStatisticsCD.plan_code = planStatistics.plan_code
                            planStatisticsCD.rank = planStatistics.rank?.int64Value ?? 0
                            
                            if let metadata = planStatistics.metadata {
                                let statisticsMetaDataCD = StatisticsMetaDataCD(context: context)
                                statisticsMetaDataCD.membershipCount = metadata.membershipCount?.int64Value ?? 0
                                planStatisticsCD.metadata = statisticsMetaDataCD
                            }
                            newDetails.planStatistics = planStatisticsCD
                        }
              
                        newDetails.addToProductServices(Set(services) as NSSet)
                        return newDetails
                    })
                    
                    response?.dataCD = dbList
                    
                    if isWhc  {
                        CoreDataStack.sharedStack.insertApiCacheData(apiCacheChecker: apiCacheChecker)
                    }
                    
                    CoreDataStack.sharedStack.saveContext(context: context) {}
                }
                
                completionHandlerBlock(response, error)
            })
        }
        
        taskCallBack(request)
    }
}
