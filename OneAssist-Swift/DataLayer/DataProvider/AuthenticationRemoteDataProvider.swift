//
//  AuthenticationDataProvider.swift
//  OneAssist-Swift
//
//  Created by Mukesh on 1/17/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

class AuthenticationRemoteDataProvider: DataProvider, AuthenticationDataProvider {
    private let remoteDataSource = RemoteDataSource()
    
    func getCustomerDetails(request: GetPermanentCustomerDetailsRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetPermanentCustomerDetailsResponseDTO?, Error?) -> Void) {
        
        let url = Constants.MembershipUrls.customerDetails
        
        let headers = RemoteDataSource.apiGatewayHeadersWithToken()
        
        let request = remoteDataSource.responsePost(url: url, request: request, headers: headers) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    
    func loginUser(request: LoginMobileRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(LoginMobileResponseDTO?, Error?) -> Void) {
        
        let url = Constants.Urls.SignUp.authGenerateOTP
        
        let request = remoteDataSource.responsePost(url: url, request: request, headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func logoutUser(request: LogoutRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(LogoutResponseDTO?, Error?) -> Void) {
        
        let url = Constants.Urls.SignUp.logoutUser
        
        let request = remoteDataSource.responseGet(url: url, request: request, headers: RemoteDataSource.apiGatewayHeadersWithToken()){ (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func verifyOTP(request: VerifyOTPRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(VerifyOTPResponseDTO?, Error?) -> Void) {
        
        let url = Constants.Urls.SignUp.login
        
        let request = remoteDataSource.responsePost(url: url, request: request, headers: RemoteDataSource.apiGatewayHeadersWithToken(), authenticate: nil) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func updateCustomerDetails(request: UpdateCustomerDetailsRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(UpdateCustomerDetailsResponseDTO?, Error?) -> Void) {
        
        let url = Constants.MembershipUrls.updateCustomerDetails
        
        let request = remoteDataSource.responsePost(url: url, request: request,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func duplicateIMEICheck(request: DuplicateIMEIRequestDTO? , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(DuplicateIMEIResponseDTO?, Error?) -> Void) {
        
        let url = Constants.Urls.SignUp.IMEIDedupeCheck
        
        let request = remoteDataSource.responsePost(url: url, request: request,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getQuestionAnswerList(request: GetQuestionAnswerRequestDTO , taskCallBack:@escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(GetQuestionAnswerResponseDTO?, Error?) -> Void) {
        
        let url = (Constants.Urls.SignUp.questionAnswer) + request.custId + "/question-answer?categoryCode=" + request.categoryCode + "&questionCode=" + request.questionCode
        
        let request = remoteDataSource.responseGet(url: url, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()){ (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: { (response: GetQuestionAnswerResponseDTO?, error) in
                DataDiskStogare.saveDatawithFile(data: data, fileName: questionFilename)
                completionHandlerBlock(response, error)
            })
        }
        taskCallBack(request)
    }
}
