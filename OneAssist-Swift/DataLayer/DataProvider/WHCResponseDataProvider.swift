//
//  WHCResponseDataProvider.swift
//  OneAssist-Swift
//
//  Created by Ashish Mittal on 20/11/17.
//  Copyright © 2017 Mukesh. All rights reserved.
//

import Foundation
import Alamofire

final class WHCResponseDataProvider: DataProvider, WHCDataProvider {
    
    private let remoteDataSource = RemoteDataSource()
    
    func getWHCInspectionDetails(requestDto: WHCInspectionRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WHCInspectionResponseDTO?, Error?)->Void){
        
        let completeUrl = "\(Constants.WHC.URL.inspectionDetailURL)\(requestDto?.requestId ?? "")"
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
        
    }
    
    func getCustomerAddress(requestDto: WHCPincodeRequestDTO?, taskCallBack: @escaping (Request?) -> Void, completionHandler: @escaping (WHCPincodeResponseDTO?, Error?) -> Void) {
        let completeUrl = "\(Constants.WHC.URL.pincodeAndAdressURL)\(UserCoreDataStore.currentUser?.cusId ?? "")/address?addressType=\(requestDto?.addrType ?? "")"
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil, headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandler)
        }
        taskCallBack(request)
    }
    
    func getWHCFeedbacks(requestDto: WHCFeedbackRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WHCFeedbackResponseDTO?, Error?)->Void) {
        let completeUrl = String(format: "\(Constants.WHC.URL.getFeedbackURL)\(requestDto?.rating ?? "")", requestDto?.ratingType ?? "")
//        "\(Constants.WHC.URL.getFeedbackURL)\(requestDto?.rating ?? "")"
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
   
        func getWHCPinCodeResponse(requestDto: WHCPincodeRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(WHCPincodeResponseDTO?, Error?)->Void) {
//        if requestDto?.type == nil{
//         //   UserCoreDataStore.currentUser?.cusId
//            //\(UserCoreDataStore.currentUser?.cusId ?? "")
//            let completeUrl = "\(Constants.WHC.URL.pincodeAndAdressURL)\(UserCoreDataStore.currentUser?.cusId ?? "")/address?pinCode=\(requestDto?.pincode ?? "")"
//            let request = remoteDataSource.responseGet(url: completeUrl, request: nil) { (data, urlResponse, error) in
//                self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
//        }
//             taskCallBack(request)
//        }
//        else{
        let completeUrl = "\(Constants.WHC.URL.pincodeURL)\(requestDto?.pincode ?? "")\(requestDto?.serviceRequestSourceType ?? "")"
            let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
                self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
             taskCallBack(request)
            //}
       // (Constants.WHC.serViceRequestType)
//       // let request = remoteDataSource.responseGet(url: completeUrl, request: nil) { (data, urlResponse, error) in
//            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
       
        
       
    }
    
    func getWHCNotify(requestDto: WHCPinVerificationRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(BaseResponseDTO?, Error?)->Void) {
        
        let completeUrl = "\(Constants.WHC.URL.notifyURL)last_name=\(requestDto?.name ?? "")&email_c=\(requestDto?.email ?? "")&phone_mobile=\(requestDto?.mobileNumber ?? "")&whc_postalcode=\(requestDto?.pincode ?? "")&lead_source=\(Constants.SchemeVariables.leadSource)&lead_source_description=Mobile_description&BUSS_PARTNER_NAME_C=\(Constants.SchemeVariables.bussPartnerName)&BUS_UNIT_NAME_C=\(Constants.SchemeVariables.busUnitName)&customer_status_c=lead&campaign_id=\(requestDto?.campaignId ?? "")&assigned_user_id=1&req_id=last_name".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "\(Constants.WHC.URL.notifyURL)"
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getTechnicianDetail(requestDto: TechnicianDetailRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(TechnicianDetailResponseDTO?, Error?)->Void) {
        
        let completeUrl = "\(Constants.WHC.URL.technicianDetail)\(requestDto?.technicianId ?? "")"
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getStartEndOTP(requestDto: InspectionStartEndOTPRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(InspectionStartEndOTPResponseDTO?, Error?)->Void) {
        
        let completeUrl = String(format : Constants.WHC.URL.startEndOTP, requestDto?.serviceRequestId ?? "")
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getTimeSlots(requestDto: InspectionTimeSlotsRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(InspectionTimeSlotsResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.SchemeVariables.servicePlatform)masterData/availableServiceSlots"
        
        let request = remoteDataSource.responseGet(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        taskCallBack(request)
    }
    
    func scheduleInspection(requestDto: ScheduleInspectionRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(ScheduleInspectionResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.URL.baseMainUrlOasys)v2/submitPostPaymentDetails"
        
        let request = remoteDataSource.responsePost(url: url, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, response, error) in
            self.handleResponse(data: data, urlResponse: response, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func rescheduleInspection(requestDto: RescheduleInspectionRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(RescheduleInspectionResponseDTO?, Error?)->Void) {
        
        let url = "\(Constants.SchemeVariables.servicePlatform)servicerequests/%@/schedule"
        let completeUrl = String(format: url, requestDto?.serviceReqId ?? "")
        
        let request = remoteDataSource.responsePut(url: completeUrl, request: requestDto,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func getTempCustomer(requestDto: TemporaryCustomerInfoRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(TemporaryCustomerInfoResponseDTO?, Error?)->Void) {
        
        let urlBase64CharacterSet = CharacterSet(charactersIn: "/+=\n").inverted
        let code = requestDto?.activationCode.addingPercentEncoding(withAllowedCharacters: urlBase64CharacterSet)
        
        let url = "\(Constants.URL.baseMainUrlOasys)getTempCustomerInfo?activationCode=%@"
        let completeUrl = String(format: url, code ?? "")
        
        let request = remoteDataSource.responseGet(url: completeUrl, request: nil,headers: RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
    
    func submitFeedback(requestDto: SubmitFeedbackRequestDTO?, taskCallBack: @escaping (_ task: Request?) -> Void, completionHandlerBlock: @escaping(SubmitFeedbackResponseDTO?, Error?)->Void) {
        let url = Constants.WHC.URL.submitFeedbackURL
        let completeUrl = String(format: url, requestDto?.serviceReqId ?? "")
        
        let request = remoteDataSource.responsePut(url: completeUrl, request: requestDto,headers:RemoteDataSource.apiGatewayHeadersWithToken()) { (data, urlResponse, error) in
            self.handleResponse(data: data, urlResponse: urlResponse, error: error, completionHandlerBlock: completionHandlerBlock)
        }
        
        taskCallBack(request)
    }
}
