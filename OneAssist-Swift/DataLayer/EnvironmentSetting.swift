//
//  EnvironmentSetting.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 17/08/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

private let keyCurrentEnviornment = "debugEnvironment"
private let keyPreviousEnviornment = "previousEnvironment"
private let keyNodeServerIP = "nodeServerIP"
private let keyUseNodeServer = "useNodeServer"

enum EnvironmentType : Int {
    case prod = 1, qa1, qa2, qa3, qa4, sit1, sit2, sit3, sit4, sit5, sit6, sit7, sit8, sit9, sit10, uat1, uat2, aws

    func toStringValue() -> String {
        switch self {
        case .prod: return "prod"
        case .qa1: return "qa1"
        case .qa2: return "qa2"
        case .qa3: return "qa3"
        case .qa4: return "qa4"
        case .sit1: return "sit1"
        case .sit2: return "sit2"
        case .sit3: return "sit3"
        case .sit4: return "sit4"
        case .sit5: return "sit5"
        case .sit6: return "sit6"
        case .sit7: return "sit7"
        case .sit8: return "sit8"
        case .sit9: return "sit9"
        case .sit10: return "sit10"
        case .uat1: return "uat1"
        case .uat2: return "uat2"
        case .aws: return "aws"
        }
    }
}

private let defaultEnviornment = EnvironmentType.prod

class EnvironmentSetting {
    private init() {}
    
    class var currentEnviornment: EnvironmentType {
        get {
            if let env = UserDefaults.standard.value(forKey: keyCurrentEnviornment) as? Int, let enviornment = EnvironmentType(rawValue: env) {
                return enviornment
            } else {
                self.currentEnviornment = defaultEnviornment
                return defaultEnviornment
            }
        } set {
            UserDefaults.standard.setValue(newValue.rawValue, forKey: keyCurrentEnviornment)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var previousEnviornment: EnvironmentType {
        get {
            if let env = UserDefaults.standard.value(forKey: keyPreviousEnviornment) as? Int, let enviornment = EnvironmentType(rawValue: env) {
                return enviornment
            } else {
                self.previousEnviornment = defaultEnviornment
                return defaultEnviornment
            }
        } set {
            UserDefaults.standard.setValue(newValue.rawValue, forKey: keyPreviousEnviornment)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var nodeServerIP: String? {
        get {
            return UserDefaults.standard.value(forKey: keyNodeServerIP) as? String
        } set {
            UserDefaults.standard.setValue(newValue, forKey: keyNodeServerIP)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var useNodeServer: Bool {
        get {
            return UserDefaults.standard.bool(forKey: keyUseNodeServer)
        } set {
            UserDefaults.standard.set(newValue, forKey: keyUseNodeServer)
            UserDefaults.standard.synchronize()
        }
    }
}
