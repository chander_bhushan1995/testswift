//
//  XMPPManager.swift
//  OneAssist-Swift
//
//  Created by Anand Kumar on 28/08/18.
//  Copyright © 2018 Mukesh. All rights reserved.
//

import Foundation
import XMPPFramework
import Alamofire
import FirebaseCrashlytics

var userMobileNumber: String {
    if let mobileNo = CacheManager.shared.mobileNo {
        return mobileNo
    }else if let mobileNo = UserCoreDataStore.currentUser?.mobileNo {
        CacheManager.shared.mobileNo = mobileNo
        return mobileNo
    }else {
        return ""
    }
}

var chatUserName: String {
    return UserCoreDataStore.currentUser?.userName ?? "User"
}

private let hostPort: UInt16    = 5222 // Not needed?


class XMPPManager: NSObject {
    
    static let shared = XMPPManager()
    
    fileprivate var stream = XMPPStream()
    
    var userJIDString: String {
        return "visitor.\(userMobileNumber).chat@\(Constants.SchemeVariables.chatHostName)"
    }
    
    var connectionCallback: ((Bool) -> Void)? = nil
    
    var xmppRoster = XMPPRoster(rosterStorage: XMPPRosterCoreDataStorage())
    var slotRequest:((XMPPSlot?) -> Void)?

    fileprivate var password:String?
    
    private override init() {
        super.init()
    }
    
    func createXMPPConnection(withPassword password:String, _ callback: @escaping (Bool) -> Void) {
        connectionCallback = callback
        createXMPPConnection(withPassword: password, forceNewConnection: true)
    }
    
    func reconnect() {
        print("Stream: reconnect")
        if let password = self.password {
            createXMPPConnection(withPassword: password)
        }
    }
    
    func disconnect() {
        if RemoteConfigManager.shared.isLogChatEvent {
            EventTracking.shared.eventTracking(name: .websocketConnection, [.connection : "disconnect", .dateTime: Date().stringInEventFormat()])
        }
        password = nil
        tearDownStream()
    }
    
    func reuqestSlotForDocumentUpload(fileName: String, size: UInt, contentType: String, _ completionHandler: @escaping (XMPPSlot?) -> Void) {
        
        let iq = XMPPIQ(type: "get", to: XMPPJID(string: "upload.\(Constants.SchemeVariables.chatHostName)"))
        iq.addAttribute(withName: "xmlns", stringValue: "jabber:client")
        
        let request = DDXMLElement.element(withName: "request") as! DDXMLElement
        request.addAttribute(withName: "xmlns", stringValue: "urn:xmpp:http:upload:0")
        request.addAttribute(withName: "filename", stringValue: fileName)
        request.addAttribute(withName: "size", stringValue: "\(size)")
        request.addAttribute(withName: "content-type", stringValue: contentType)
        
        iq.addChild(request)
        
        slotRequest = completionHandler
        stream.send(iq)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            self.slotRequest?(nil)
            self.slotRequest = nil
        })
    }
    
    private func createXMPPConnection(withPassword password:String, forceNewConnection: Bool = false) {
        if self.password != password {
            self.password = password
        }
        if !stream.isDisconnected && !forceNewConnection{
            return
        }
        tearDownStream()
        xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = true
        xmppRoster.autoFetchRoster = true
        stream.removeDelegate(self)
        stream.addDelegate(self, delegateQueue: .main)
        xmppRoster.removeDelegate(self)
        xmppRoster.addDelegate(self, delegateQueue: .main)
        xmppRoster.activate(stream)
        stream.hostName = Constants.SchemeVariables.chatHostName
        stream.hostPort = hostPort
        print("user namr is =====  \(userJIDString)")
        stream.myJID = XMPPJID(string: userJIDString, resource: UIDevice.uuid.replacingOccurrences(of: "-", with: ""))
        connect(forceNewConnection:forceNewConnection)
    }
    
    private func connect(forceNewConnection: Bool = false) {
        print("Stream: Connect")
        if !stream.isDisconnected && !forceNewConnection {
            return
        }
        do {
            try stream.connect(withTimeout: XMPPStreamTimeoutNone)
        } catch {
            print("error occured in connecting")
        }
    }
    
    private func tearDownStream() {
        print("Stream: tearDownStream")
        xmppRoster.deactivate()
        stream.disconnect()
    }
    
    fileprivate func sendAckForMessage(_ message: XMPPMessage) {
        if let from = message.from, let msgId = message.children?.first(where: { $0.name == "ejb-msg-id" })?.child(at: 0)?.stringValue {
            
            let iq = XMPPIQ(type: "result", to: from)
            iq.addAttribute(withName: "id", stringValue: "msg-ack-" + msgId)
            iq.addAttribute(withName: "from", stringValue: userJIDString)
            
            let request = DDXMLElement.element(withName: "query") as! DDXMLElement
            request.setXmlns("ejb-msg-ack")
            request.addChild(DDXMLNode.text(withStringValue: msgId) as! DDXMLNode)
            
            iq.addChild(request)
            
            print("Stream: Send message Ack : \(iq)")
            
            stream.send(iq)
        }
    }
    
    func uploadDocument(_ imageUri:String, _ completionHandler: @escaping (String, Int) -> Void) {
        print(" ==== \(imageUri)")
        guard let url = URL(string: imageUri), let data = try? Data(contentsOf: url) else {
            return
        }
        let filename = imageUri.components(separatedBy: "/").last ?? ""
        let contentType = "image/jpeg"
        reuqestSlotForDocumentUpload(fileName: filename, size: UInt(data.count), contentType: contentType) { (slot) in
            if let slot = slot {
                let headers = ["Content-Type": contentType]
                AF.upload(data, to: slot.putURL, method: .put, headers: HTTPHeaders(headers)).response { response in
                    if let data = response.data, !data.isEmpty {
                        print("====!==== success")
                        completionHandler(slot.putURL.absoluteString, 1)
                    } else {
                        completionHandler("", -1)
                    }
                }
            } else {
                completionHandler("", -1)
            }
        }
    }
}

extension XMPPManager: XMPPStreamDelegate {
    
    func xmppStreamWillConnect(_ sender: XMPPStream) {
        print("Stream: Will Connect")
    }
    
    func xmppStreamConnectDidTimeout(_ sender: XMPPStream) {
        connectionCallback?(false)
        connectionCallback = nil
        print("Stream: Timeout")
    }
    
    func xmppStreamDidConnect(_ sender: XMPPStream) {
        print("Stream: Connected")
        print("Stream: Will Authenticate, username: \(userJIDString), passowrd: \(password ?? "")")
        if RemoteConfigManager.shared.isLogChatEvent {
            EventTracking.shared.eventTracking(name: .websocketConnection, [.connection : "done", .dateTime: Date().stringInEventFormat()])
        }
        try? stream.authenticate(withPassword: password!)
    }
    
    func xmppStreamDidDisconnect(_ sender: XMPPStream, withError error: Error?) {
        print("Stream: Disconnected, Error :\(error?.localizedDescription ?? "NIL")")
        if RemoteConfigManager.shared.isLogChatEvent {
            if let errorDescription = error?.localizedDescription {
                EventTracking.shared.eventTracking(name: .websocketConnection, [.connectionClosedOnError: errorDescription, .dateTime: Date().stringInEventFormat()])
            } else {
                EventTracking.shared.eventTracking(name: .websocketConnection, [.connection : "Closed", .dateTime: Date().stringInEventFormat()])
            }
        }
        if error != nil && (NetworkReachabilityManager()?.isReachable ?? false) {
            // reconnect socket connection.
            tearDownStream()
            reconnect()
            var userInfo : [String: Any] = [:]
            userInfo["hash"] = self.password ?? "unknow"
            userInfo["userJIDString"] = userJIDString
            userInfo["error"] = error ?? "unknow error"
            let mainError = NSError(domain: error?.domain ?? "CHAT_DOMAIN" , code: error?.code ?? 999, userInfo: userInfo)
            Crashlytics.crashlytics().record(error: mainError)
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Stream: Did Not Authenticated \(error)")
        connectionCallback?(false)
        connectionCallback = nil
    }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        connectionCallback?(true)
        connectionCallback = nil
        self.stream.send(XMPPPresence())
       // print("Stream: Sending : \(sender) \(XMPPPresence())")
        print("Stream: Authenticated")
    }
    
    func xmppStream(_ sender: XMPPStream, didSend iq: XMPPIQ) {
        print("Stream: Sent iq : \(iq)")
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive iq: XMPPIQ) -> Bool {
        print("Stream: Received iq : \(iq)")
        if let slot = XMPPSlot(iq: iq) {
            slotRequest?(slot)
            slotRequest = nil
        }
        return true
    }
    
    func xmppStream(_ sender: XMPPStream, didSend presence: XMPPPresence) {
        print("Stream: Sent presence : \( presence.type ?? "unknown")")
    }
    
    func xmppStream(_ sender: XMPPStream, didReceiveCustomElement element: DDXMLElement) {
        print("Stream: Received Custom Element : \(element)")
    }
    
    func xmppStream(_ sender: XMPPStream, didReceiveError error: DDXMLElement) {
        print("Stream: Received Error : \(error)")
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive presence: XMPPPresence) {
        print("Stream: Received presence : \( presence)")
        print("Stream: Received presence : \( presence.type ?? "unknown")")
        let presenceType = presence.type
        let username = sender.myJID?.user
        let presenceFromUser = presence.from?.user
        
        if presenceFromUser != username, presenceType == "subscribe" {
            xmppRoster.subscribePresence(toUser: presence.from!)
        }
    }
    
    func xmppStreamDidSendClosingStreamStanza(_ sender: XMPPStream) {
        print("Stream: Sent closing stream stanza")
    }
    
    func xmppStream(_ sender: XMPPStream, didSend message: XMPPMessage) {
       // print("Stream: Sent message : \(message.body ?? "")")
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
//        print("Stream: Received message : \( message)")
        let from = message.from?.bare ?? ""
        if message.isChatMessageWithBody {
            if let messageReceived = message.body, !messageReceived.isEmpty {
                //print("Stream: Received message : \(messageReceived)")
                if RemoteConfigManager.shared.isLogChatEvent {
                    EventTracking.shared.eventTracking(name: .agentMessageReceived, [.stanzaId: message.stanzaIds.first?.value ?? ""])
                }
                let chatId = message.children?.first(where: { $0.name == "chat-id" })?.child(at: 0)?.stringValue ?? ""
                let msgId = message.children?.first(where: { $0.name == "ejb-msg-id" })?.child(at: 0)?.stringValue ?? ""
                ChatBridge.receivedMessage(messageReceived, from: from, chatId: chatId, msgId: msgId)
                sendAckForMessage(message)
            }
        } else {
            if !message.elements(forName: "composing").isEmpty {
                ChatBridge.receivedStatus("composing", from: from)
            } else if !message.elements(forName: "paused").isEmpty {
                ChatBridge.receivedStatus("paused", from: from)
            } else if !message.elements(forName: "gone").isEmpty  {
                ChatBridge.receivedStatus("gone", from: from)
            } else if !message.elements(forName: "inactive").isEmpty {
                ChatBridge.receivedStatus("inactive", from: from)
            }
        }
    }
}

extension XMPPManager: XMPPRosterDelegate {
    func xmppRoster(_ sender: XMPPRoster, didReceiveRosterItem item: DDXMLElement) {
        print("Roster: Received RosterItem : \(item.description)")
    }
}
