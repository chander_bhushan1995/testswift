//
//  CloudFirestoreManager.swift
//  OneAssist-Swift
//
//  Created by Himanshu Dagar on 07/11/2019.
//  Copyright © 2019 OneAssist. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseStorage
import FirebaseDatabase

class CloudFirestoreManager {
    
    private init() {}
    
    static let shared = CloudFirestoreManager()
    
    var db: Firestore!
    var realtimeDB: DatabaseReference!
    
    func setup() {
        db = Firestore.firestore()
        realtimeDB = Database.database().reference()
    }
    
    func writeUserData(mobileNo: String, emailId: String, deviceModel: String, orderID: String, OCRText: String, imeis: [String], barcodeScanText: String?) {
        
        var docData: [String: Any] = [
                        "deviceModel": deviceModel,
                        "timeStamp": Timestamp(date: Date()),
                        "iOS Version": UIDevice.current.systemVersion,
                        "OCRText": OCRText ]
        
        if imeis.count > 0 {
            docData["imei"] = imeis[0]
            if imeis.count > 1 {
                docData["imei2"] = imeis[1]
            }
        }
        
        if let barcodeScanText = barcodeScanText {
            docData["barcodeScanText"] = barcodeScanText
        }
        
        db.collection("fraud_detection_users").document(mobileNo).setData([
            "emailID" : emailId,
            
            "\(orderID) - \(Timestamp(date: Date()))" : docData], merge: true) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
        }
    }
    
    func writeMirrorTestLogs(data: [String: Any]) {
        if let tempUserId = data["tempUserId"] as? String {
            self.realtimeDB.child("MirrorTest/\(tempUserId)_\(RemoteConfigManager.shared.mirrorTestLoggingHash)").childByAutoId().setValue(data)
        }
    }
    
    func uploadFile(_ image: UIImage, fileName: String, customerId: String) {
        // Create a root reference
        if let imageData = image.jpegData(compressionQuality: 0.9) {
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let filePath = "Images/" + customerId + "/" + fileName
            
            // Create a reference to the file you want to upload
            let riversRef = storageRef.child(filePath)
            
            // Upload the file to the path "images/rivers.jpg"
            _ = riversRef.putData(imageData, metadata: nil) { (metadata, error) in
              guard let metadata = metadata else {
                print(error ?? "something wrong")
                return
              }
                print(metadata)
            }
        }
    }
    
    func uploadFile(_ image: UIImage, at uploadPath: String, completionHandler: (()->())?) {
        if let imageData = image.jpegData(compressionQuality: 1) {
            let storage = Storage.storage()
            let storageRef = storage.reference()
            
            let riversRef = storageRef.child(uploadPath)
            _ = riversRef.putData(imageData, metadata: nil, completion: { metaData, error in
                guard error == nil else {
                    print("error while uploading image on Firebase Storage \(String(describing: error))")
                    return
                }
                completionHandler?()
            })
        }
    }
}
